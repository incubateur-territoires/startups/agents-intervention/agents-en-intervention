import { app } from './bootstrap.js';
import Carousel from '@stimulus-components/carousel'

app.register('carousel', Carousel);

/*
 * Welcome to your app's main JavaScript file!
 *
 * This file will be included onto the page via the importmap() Twig function,
 * which should already be in your base.html.twig.
 */
import 'swiper/css/bundle'
import './styles/home.scss';
