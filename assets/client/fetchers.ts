import * as reactQuery from '@tanstack/react-query';

import {
  ApiCategoriesGetCollectionError,
  ApiCategoriesGetCollectionVariables,
  ApiEmployersEmployerIdactiveAgentsGetCollectionError,
  ApiEmployersEmployerIdactiveAgentsGetCollectionPathParams,
  ApiEmployersEmployerIdactiveAgentsGetCollectionQueryParams,
  ApiEmployersEmployerIdactiveAgentsGetCollectionVariables,
  ApiEmployersEmployerIdinterventionsGetCollectionError,
  ApiEmployersEmployerIdinterventionsGetCollectionPathParams,
  ApiEmployersEmployerIdinterventionsGetCollectionQueryParams,
  ApiEmployersEmployerIdinterventionsGetCollectionVariables,
  ApiEmployersIdGetError,
  ApiEmployersIdGetPathParams,
  ApiEmployersIdGetVariables,
  ApiInterventionsIdGetError,
  ApiInterventionsIdGetPathParams,
  ApiInterventionsIdGetVariables,
  ApiInterventionsIdPatchError,
  ApiInterventionsIdPatchPathParams,
  ApiInterventionsIdPatchVariables,
  ApiTypesGetCollectionError,
  ApiTypesGetCollectionVariables,
} from '@aei/src/client/generated/components';
import { Context, useContext } from '@aei/src/client/generated/context';
import { fetch } from '@aei/src/client/generated/fetcher';
import type * as Fetcher from '@aei/src/client/generated/fetcher';
import { getApiUrl } from '@aei/src/utils/url';
import {
  CalendarEvent,
  Category,
  EmployerEmployerview,
  IndisponibiliteAgent,
  Intervention,
  InterventionInterventionview,
  Type,
  UserUserview,
} from '@aei/types';

export type ApiEmployersEmployerIdinterventionsGetCollectionResponse = InterventionInterventionview[];

export const fetchApiEmployersEmployerIdinterventionsGetCollection = (
  variables: ApiEmployersEmployerIdinterventionsGetCollectionVariables,
  signal?: AbortSignal
) =>
  fetch<
    ApiEmployersEmployerIdinterventionsGetCollectionResponse,
    ApiEmployersEmployerIdinterventionsGetCollectionError,
    undefined,
    {},
    ApiEmployersEmployerIdinterventionsGetCollectionQueryParams,
    ApiEmployersEmployerIdinterventionsGetCollectionPathParams
  >({
    url: '/employers/{employerId}/interventions',
    method: 'get',
    ...variables,
    signal,
  });

export const fetchApiEmployersEmployerIdinterventionsGetCollectionRaw = (
  variables: ApiEmployersEmployerIdinterventionsGetCollectionVariables,
  signal?: AbortSignal
) =>
  fetch<
    any,
    ApiEmployersEmployerIdinterventionsGetCollectionError,
    undefined,
    {},
    ApiEmployersEmployerIdinterventionsGetCollectionQueryParams,
    ApiEmployersEmployerIdinterventionsGetCollectionPathParams
  >({
    url: '/employers/{employerId}/interventions',
    method: 'get',
    ...variables,
    signal,
    raw: true,
  });

export enum ExportFormat {
  CSV = 'text/csv',
  // Excel = 'application/vnd.ms-excel',
  Excel = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
  // XLSX = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
}

export const fetchApiEmployersEmployerIdinterventionsExportGetCollection = ({
  pathParams,
  queryParams = {},
  format = ExportFormat.CSV,
  ...rest
}: ApiEmployersEmployerIdinterventionsGetCollectionVariables & { format: ExportFormat }) => {
  const url = new URL(getApiUrl() + `/employers/${pathParams.employerId}/interventions/export`);
  if (queryParams) {
    // @ts-ignore
    Object.keys(queryParams).forEach((key) => url.searchParams.append(key, queryParams[key]));
  }
  // @ts-ignore
  const token = window.JWT;

  return window.fetch(url.toString(), {
    method: 'get',
    ...rest,
    headers: { Accept: format, Authorization: `Bearer ${token}`, ...rest.headers },
  });
};

/**
 * Retrieves a Intervention resource.
 */
export const fetchApiInterventionsIdGet = (variables: ApiInterventionsIdGetVariables, signal?: AbortSignal) =>
  fetch<InterventionInterventionview, ApiInterventionsIdGetError, undefined, {}, {}, ApiInterventionsIdGetPathParams>({
    url: '/interventions/{id}',
    method: 'get',
    ...variables,
    signal,
  });

/**
 * Retrieves a Intervention resource.
 */
export const useApiInterventionsIdGet = <TData = InterventionInterventionview>(
  variables: ApiInterventionsIdGetVariables,
  options?: Omit<reactQuery.UseQueryOptions<InterventionInterventionview, ApiInterventionsIdGetError, TData>, 'queryKey' | 'queryFn'>
) => {
  const { fetcherOptions, queryOptions, queryKeyFn } = useContext(options);
  return reactQuery.useQuery<InterventionInterventionview, ApiInterventionsIdGetError, TData>({
    queryKey: queryKeyFn({ path: '/interventions/{id}', operationId: 'apiInterventionsIdGet', variables }),
    queryFn: ({ signal }) => fetchApiInterventionsIdGet({ ...fetcherOptions, ...variables }, signal),
    ...options,
    ...queryOptions,
  });
};

/**
 * Updates the Intervention resource.
 */
export const fetchApiInterventionsIdPatch = (variables: ApiInterventionsIdPatchVariables, signal?: AbortSignal) =>
  fetch<InterventionInterventionview, ApiInterventionsIdPatchError, undefined, {}, {}, ApiInterventionsIdPatchPathParams>({
    url: '/interventions/{id}',
    method: 'patch',
    ...variables,
    signal,
  });

/**
 * Updates the Intervention resource.
 */
export const useApiInterventionsIdPatch = (
  options?: Omit<
    reactQuery.UseMutationOptions<InterventionInterventionview, ApiInterventionsIdPatchError, ApiInterventionsIdPatchVariables>,
    'mutationFn'
  >
) => {
  const { fetcherOptions } = useContext();
  return reactQuery.useMutation<InterventionInterventionview, ApiInterventionsIdPatchError, ApiInterventionsIdPatchVariables>({
    mutationFn: (variables: ApiInterventionsIdPatchVariables) => fetchApiInterventionsIdPatch({ ...fetcherOptions, ...variables }),
    ...options,
  });
};

export type ApiEmployersEmployerIdactiveAgentsGetCollectionResponse = UserUserview[];

/**
 * Retrieves the collection of User resources.
 */
export const fetchApiEmployersEmployerIdactiveAgentsGetCollection = (
  variables: ApiEmployersEmployerIdactiveAgentsGetCollectionVariables,
  signal?: AbortSignal
) =>
  fetch<
    ApiEmployersEmployerIdactiveAgentsGetCollectionResponse,
    ApiEmployersEmployerIdactiveAgentsGetCollectionError,
    undefined,
    {},
    ApiEmployersEmployerIdactiveAgentsGetCollectionQueryParams,
    ApiEmployersEmployerIdactiveAgentsGetCollectionPathParams
  >({
    url: '/employers/{employerId}/active-agents',
    method: 'get',
    ...variables,
    signal,
  });

/**
 * Retrieves the collection of User resources.
 */
export const useApiEmployersEmployerIdactiveAgentsGetCollection = <TData = ApiEmployersEmployerIdactiveAgentsGetCollectionResponse>(
  variables: ApiEmployersEmployerIdactiveAgentsGetCollectionVariables,
  options?: Omit<
    reactQuery.UseQueryOptions<ApiEmployersEmployerIdactiveAgentsGetCollectionResponse, ApiEmployersEmployerIdactiveAgentsGetCollectionError, TData>,
    'queryKey' | 'queryFn'
  >
) => {
  const { fetcherOptions, queryOptions, queryKeyFn } = useContext(options);
  return reactQuery.useQuery<ApiEmployersEmployerIdactiveAgentsGetCollectionResponse, ApiEmployersEmployerIdactiveAgentsGetCollectionError, TData>({
    queryKey: queryKeyFn({
      path: '/employers/{employerId}/active-agents',
      operationId: 'apiEmployersEmployerIdactiveAgentsGetCollection',
      variables,
    }),
    queryFn: ({ signal }) => fetchApiEmployersEmployerIdactiveAgentsGetCollection({ ...fetcherOptions, ...variables }, signal),
    ...options,
    ...queryOptions,
  });
};

/**
 * Retrieves a Employer resource.
 */
export const fetchApiEmployersIdGet = (variables: ApiEmployersIdGetVariables, signal?: AbortSignal) =>
  fetch<EmployerEmployerview, ApiEmployersIdGetError, undefined, {}, {}, ApiEmployersIdGetPathParams>({
    url: '/employers/{id}',
    method: 'get',
    ...variables,
    signal,
  });

/**
 * Retrieves a Employer resource.
 */
export const useApiEmployersIdGet = <TData = EmployerEmployerview>(
  variables: ApiEmployersIdGetVariables,
  options?: Omit<reactQuery.UseQueryOptions<EmployerEmployerview, ApiEmployersIdGetError, TData>, 'queryKey' | 'queryFn'>
) => {
  const { fetcherOptions, queryOptions, queryKeyFn } = useContext(options);
  return reactQuery.useQuery<EmployerEmployerview, ApiEmployersIdGetError, TData>({
    queryKey: queryKeyFn({ path: '/employers/{id}', operationId: 'apiEmployersIdGet', variables }),
    queryFn: ({ signal }) => fetchApiEmployersIdGet({ ...fetcherOptions, ...variables }, signal),
    ...options,
    ...queryOptions,
  });
};

export type ApiCategoriesGetCollectionResponse = Category[];

/**
 * Retrieves the collection of Category resources.
 */
export const fetchApiCategoriesGetCollection = (variables: ApiCategoriesGetCollectionVariables, signal?: AbortSignal) =>
  fetch<ApiCategoriesGetCollectionResponse, ApiCategoriesGetCollectionError, undefined, {}, {}, {}>({
    url: '/categories',
    method: 'get',
    ...variables,
    signal,
  });

/**
 * Retrieves the collection of Category resources.
 */
export const useApiCategoriesGetCollection = <TData = ApiCategoriesGetCollectionResponse>(
  variables: ApiCategoriesGetCollectionVariables,
  options?: Omit<reactQuery.UseQueryOptions<ApiCategoriesGetCollectionResponse, ApiCategoriesGetCollectionError, TData>, 'queryKey' | 'queryFn'>
) => {
  const { fetcherOptions, queryOptions, queryKeyFn } = useContext(options);
  return reactQuery.useQuery<ApiCategoriesGetCollectionResponse, ApiCategoriesGetCollectionError, TData>({
    queryKey: queryKeyFn({ path: '/categories', operationId: 'apiCategoriesGetCollection', variables }),
    queryFn: ({ signal }) => fetchApiCategoriesGetCollection({ ...fetcherOptions, ...variables }, signal),
    ...options,
    ...queryOptions,
  });
};

export type ApiTypesGetCollectionResponse = Type[];

/**
 * Retrieves the collection of Type resources.
 */
export const fetchApiTypesGetCollection = (variables: ApiTypesGetCollectionVariables, signal?: AbortSignal) =>
  fetch<ApiTypesGetCollectionResponse, ApiTypesGetCollectionError, undefined, {}, {}, {}>({
    url: '/types',
    method: 'get',
    ...variables,
    signal,
  });

/**
 * Retrieves the collection of Type resources.
 */
export const useApiTypesGetCollection = <TData = ApiTypesGetCollectionResponse>(
  variables: ApiTypesGetCollectionVariables,
  options?: Omit<reactQuery.UseQueryOptions<ApiTypesGetCollectionResponse, ApiTypesGetCollectionError, TData>, 'queryKey' | 'queryFn'>
) => {
  const { fetcherOptions, queryOptions, queryKeyFn } = useContext(options);
  return reactQuery.useQuery<ApiTypesGetCollectionResponse, ApiTypesGetCollectionError, TData>({
    queryKey: queryKeyFn({ path: '/types', operationId: 'apiTypesGetCollection', variables }),
    queryFn: ({ signal }) => fetchApiTypesGetCollection({ ...fetcherOptions, ...variables }, signal),
    ...options,
    ...queryOptions,
  });
};

export type ApiInterventionsRestartPostPathParams = {
  /**
   * Intervention identifier
   */
  id: string;
};

export type ApiInterventionsRestartPostError = Fetcher.ErrorWrapper<undefined>;

export type ApiInterventionsRestartPostVariables = {
  body: {};
  pathParams: ApiInterventionsRestartPostPathParams;
} & Context['fetcherOptions'];

export type InterventionRestartInput = {};

/**
 * Restarts an Intervention.
 */
export const fetchApiInterventionsRestartPost = (variables: ApiInterventionsRestartPostVariables, signal?: AbortSignal) =>
  fetch<Intervention, ApiInterventionsRestartPostError, InterventionRestartInput, {}, {}, ApiInterventionsRestartPostPathParams>({
    url: '/interventions/{id}/restart',
    method: 'post',
    ...variables,
    signal,
  });

export type ApiIndisponibilitePostVariables = {
  body: IndisponibiliteAgent;
  pathParams: {};
} & Context['fetcherOptions'];
export type ApiIndisponibilitePostPathParams = {};
export type ApiIndisponibilitePostError = Fetcher.ErrorWrapper<undefined>;

export const fetchApiIndisponibilitePost = (variables: ApiIndisponibilitePostVariables, signal?: AbortSignal) =>
  fetch<CalendarEvent, ApiIndisponibilitePostError, IndisponibiliteAgent, {}, {}, ApiIndisponibilitePostPathParams>({
    url: '/indisponibilite/agent',
    method: 'post',
    ...variables,
    signal,
  });
