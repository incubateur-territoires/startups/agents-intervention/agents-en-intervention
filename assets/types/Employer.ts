import { User } from './User';

export type Employer = {
  id: number;
  siren: any;
  name: any;
  longitude: any;
  latitude: any;
  users: User[];
  timezone: string;
};
