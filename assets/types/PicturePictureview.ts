import { Picture } from './Picture';

export type PicturePictureview = Pick<Picture, 'id' | 'fileName' | 'url' | 'tag' | 'small' | 'medium' | 'original'>;
