import { Location } from './Location';

export type LocationLocationmap = Pick<Location, 'street' | 'rest' | 'postcode' | 'city' | 'longitude' | 'latitude'>;
