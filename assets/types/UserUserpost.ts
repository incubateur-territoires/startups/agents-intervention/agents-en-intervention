import { User } from './User';

export type UserUserpost = Pick<User, 'login' | 'password' | 'firstname' | 'lastname' | 'email' | 'phoneNumber' | 'picture' | 'employer' | 'roles'>;
