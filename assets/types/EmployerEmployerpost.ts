import { Employer } from './Employer';

export type EmployerEmployerpost = Pick<Employer, 'siren' | 'name' | 'longitude' | 'latitude'>;
