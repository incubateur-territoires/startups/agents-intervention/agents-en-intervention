import { Type } from './Type';

export type TypeTypeget = Pick<Type, 'id' | 'name' | 'category' | 'picture' | 'description' | 'position'>;
