import { Picture } from './Picture';

export type PictureInterventionget = Pick<Picture, 'id' | 'fileName' | 'url' | 'tag' | 'small' | 'medium' | 'original'>;
