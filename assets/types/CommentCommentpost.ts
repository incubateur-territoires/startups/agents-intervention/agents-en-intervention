import { Comment } from './Comment';

export type CommentCommentpost = Pick<Comment, 'message' | 'intervention'>;
