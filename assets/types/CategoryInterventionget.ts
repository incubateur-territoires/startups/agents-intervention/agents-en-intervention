import { Category } from './Category';

export type CategoryInterventionget = Pick<Category, 'id' | 'name' | 'picture' | 'description'>;
