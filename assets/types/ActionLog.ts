
export type ActionLog = {
  id: any;
  timestamp: Date;
  type: string;
  entityClass: string;
  entityId: number | null;
  changes: any | null;
  userIdentifier: number | null;
};
