import { Category } from './Category';

export type CategoryInterventionget_collection = Pick<Category, 'id' | 'name' | 'picture' | 'description'>;
