import { Status } from './Status';
import { Priority } from './Priority';
import { Type } from './Type';
import { User } from './User';
import { Employer } from './Employer';
import { Location } from './Location';
import { Comment } from './Comment';
import { Picture } from './Picture';
import { Frequency } from './Frequency';
import { Category } from './Category';

export type Intervention = {
  id: number;
  logicId: number;
  createdAt: Date;
  description: any | null;
  status: Status;
  priority: Priority;
  type: Type;
  author: User;
  employer: Employer;
  location: Location;
  endedAt: Date | null;
  startedAt: Date | null;
  participants: User[];
  comments: Comment[];
  pictures: Picture[];
  startAt: Date | null;
  dueDate: Date | null;
  recurrenceReference: Intervention;
  generatedInterventions: Intervention[];
  endAt: Date | null;
  frequency: Frequency;
  triggerAt: string | null;
  exceptions: any | null;
  frequencyInterval: number;
  title: string;
  recurring: boolean;
  category: Category;
};
