import { Intervention } from './Intervention';

export type InterventionInterventionexport = Pick<Intervention, 'logicId' | 'createdAt' | 'description' | 'status' | 'priority' | 'type' | 'author' | 'location' | 'endedAt' | 'startedAt' | 'title'>;
