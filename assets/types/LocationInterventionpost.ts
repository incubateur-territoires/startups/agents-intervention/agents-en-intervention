import { Location } from './Location';

export type LocationInterventionpost = Pick<Location, 'street' | 'rest' | 'postcode' | 'city' | 'longitude' | 'latitude'>;
