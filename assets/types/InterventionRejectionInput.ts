
export type InterventionRejectionInput = {
  reason: string;
  details: string;
};
