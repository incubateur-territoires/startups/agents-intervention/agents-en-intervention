import { User } from './User';

export type UserUserpatch = Pick<User, 'password'>;
