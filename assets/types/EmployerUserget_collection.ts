import { Employer } from './Employer';

export type EmployerUserget_collection = Pick<Employer, 'id' | 'name'>;
