
export type Category = {
  id: number;
  name: any;
  picture: any;
  description: any | null;
  position: number | null;
};
