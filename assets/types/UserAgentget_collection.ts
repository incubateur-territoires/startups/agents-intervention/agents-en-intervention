import { User } from './User';

export type UserAgentget_collection = Pick<User, 'id' | 'firstname' | 'lastname' | 'email' | 'phoneNumber'>;
