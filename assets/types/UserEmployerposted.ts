import { User } from './User';

export type UserEmployerposted = Pick<User, 'id' | 'login' | 'firstname' | 'lastname' | 'email' | 'phoneNumber' | 'employer' | 'roles'>;
