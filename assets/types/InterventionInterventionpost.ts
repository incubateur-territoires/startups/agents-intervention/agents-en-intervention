import { Intervention } from './Intervention';

export type InterventionInterventionpost = Pick<Intervention, 'description' | 'status' | 'priority' | 'type' | 'employer' | 'location' | 'endedAt' | 'startedAt' | 'participants' | 'pictures' | 'startAt' | 'dueDate' | 'endAt' | 'frequency' | 'frequencyInterval' | 'title'>;
