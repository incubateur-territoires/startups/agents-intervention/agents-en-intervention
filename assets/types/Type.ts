import { Category } from './Category';

export type Type = {
  id: number;
  name: any;
  category: Category;
  picture: any;
  description: any | null;
  position: number | null;
};
