import { Employer } from './Employer';

export type EmployerEmployerput = Pick<Employer, 'id' | 'siren' | 'name' | 'longitude' | 'latitude'>;
