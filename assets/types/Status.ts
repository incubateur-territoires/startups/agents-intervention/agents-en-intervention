export enum Status {
  ToValidate = "to-validate",
  Blocked = "blocked",
  Finished = "finished",
  InProgress = "in-progress",
  ToDo = "to-do",
  Recurring = "recurring",
  Draft = "draft",
  Rejected = "rejected",
}
