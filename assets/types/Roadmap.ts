
export type Roadmap = {
  id: number;
  title: string;
  description: any;
  expirationDate: any | null;
  estimatedDate: string;
  position: number;
  published: boolean;
};
