import { Employer } from './Employer';

export type InterventionSequences = {
  employer: Employer;
  seq: number;
};
