import { Picture } from './Picture';

export type PictureUserget = Pick<Picture, 'id' | 'fileName' | 'url' | 'tag'>;
