export enum Role {
  Agent = "ROLE_AGENT",
  AdminEmployer = "ROLE_ADMIN_EMPLOYER",
  Director = "ROLE_DIRECTOR",
  Elected = "ROLE_ELECTED",
  Admin = "ROLE_ADMIN",
}
