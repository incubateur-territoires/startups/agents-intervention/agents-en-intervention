import { User } from './User';
import { Intervention } from './Intervention';

export type Comment = {
  id: number;
  message: any;
  createdAt: Date;
  author: User;
  intervention: Intervention;
};
