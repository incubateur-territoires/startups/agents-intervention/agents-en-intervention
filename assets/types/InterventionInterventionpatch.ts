import { Intervention } from './Intervention';

export type InterventionInterventionpatch = Pick<Intervention, 'description' | 'status' | 'priority' | 'type' | 'location' | 'endedAt' | 'startedAt' | 'participants' | 'startAt' | 'dueDate' | 'endAt' | 'frequency' | 'frequencyInterval' | 'title'>;
