import { Employer } from './Employer';

export type EmployerEmployerposted = Pick<Employer, 'id' | 'siren' | 'name' | 'longitude' | 'latitude'>;
