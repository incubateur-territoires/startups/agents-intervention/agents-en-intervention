import { Location } from './Location';

export type LocationInterventionview = Pick<Location, 'fullAddress'>;
