import { Location } from './Location';

export type LocationInterventionget = Pick<Location, 'id' | 'street' | 'rest' | 'postcode' | 'city' | 'longitude' | 'latitude'>;
