
export type Location = {
  id: number;
  street: any | null;
  rest: any | null;
  postcode: any | null;
  city: any | null;
  longitude: any;
  latitude: any;
  fullAddress: string;
};
