import { Intervention } from './Intervention';

export type InterventionInterventionget = Pick<Intervention, 'startAt' | 'dueDate' | 'endAt' | 'frequency' | 'frequencyInterval' | 'title'>;
