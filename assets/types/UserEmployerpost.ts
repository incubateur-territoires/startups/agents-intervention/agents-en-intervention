import { User } from './User';

export type UserEmployerpost = Pick<User, 'login' | 'password' | 'firstname' | 'lastname' | 'email' | 'phoneNumber' | 'employer'>;
