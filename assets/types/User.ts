import { Picture } from './Picture';
import { Employer } from './Employer';

export type User = {
  id: number;
  login: any;
  password: any;
  firstname: any;
  lastname: any;
  email: any | null;
  phoneNumber: any | null;
  createdAt: Date;
  picture: Picture;
  employer: Employer;
  active: boolean;
  connectedAt: Date | null;
  roles: any;
  proConnectToken: any | null;
  role: string;
  connectWithProConnect: boolean;
};
