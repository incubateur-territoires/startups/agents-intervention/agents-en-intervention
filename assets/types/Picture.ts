import { PictureTag } from './PictureTag';
import { Intervention } from './Intervention';

export type Picture = {
  id: number;
  fileName: any;
  url?: string | null;
  urls: any[];
  createdAt: Date;
  tag: PictureTag | null;
  intervention: Intervention;
  small: string | null;
  medium: string | null;
  original: string | null;
};
