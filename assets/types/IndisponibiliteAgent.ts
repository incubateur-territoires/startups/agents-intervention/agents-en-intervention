import { Frequency } from './Frequency';

export type IndisponibiliteAgent = {
  title: string;
  frequency: Frequency;
  frequencyInterval: number;
  startDate: Date;
  endDate: Date;
  recEndDate?: Date | null;
  participant: string;
};
