import { Employer } from './Employer';

export type EmployerEmployerview = Pick<Employer, 'id' | 'siren' | 'name' | 'longitude' | 'latitude'>;
