import { Location } from './Location';

export type LocationInterventionpatch = Pick<Location, 'street' | 'rest' | 'postcode' | 'city' | 'longitude' | 'latitude'>;
