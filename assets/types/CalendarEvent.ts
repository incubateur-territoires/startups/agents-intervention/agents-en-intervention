
export type CalendarEvent = {
  id: any;
  calendarData: any;
  calendarId: string;
  lastModified: Date;
  etag: string;
  size: number;
  componentType: string;
  firstOccurence: Date;
  lastOccurence: Date | null;
};
