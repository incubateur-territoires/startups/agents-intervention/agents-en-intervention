import { User } from './User';

export type UserInterventionget_collection = Pick<User, 'id' | 'firstname' | 'lastname' | 'email' | 'phoneNumber' | 'picture'>;
