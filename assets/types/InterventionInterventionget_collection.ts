import { Intervention } from './Intervention';

export type InterventionInterventionget_collection = Pick<Intervention, 'recurring' | 'category'>;
