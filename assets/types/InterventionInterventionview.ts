import { Intervention } from './Intervention';

export type InterventionInterventionview = Pick<Intervention, 'id' | 'logicId' | 'createdAt' | 'description' | 'status' | 'priority' | 'type' | 'author' | 'employer' | 'location' | 'endedAt' | 'startedAt' | 'participants' | 'comments' | 'pictures' | 'startAt' | 'dueDate' | 'endAt' | 'frequency' | 'frequencyInterval' | 'title' | 'recurring' | 'category'>;
