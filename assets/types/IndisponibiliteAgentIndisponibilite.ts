import { IndisponibiliteAgent } from './IndisponibiliteAgent';

export type IndisponibiliteAgentIndisponibilite = Pick<IndisponibiliteAgent, 'title' | 'frequency' | 'frequencyInterval' | 'startDate' | 'endDate' | 'recEndDate' | 'participant'>;
