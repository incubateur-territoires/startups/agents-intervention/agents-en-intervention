import { User } from './User';

export type UserUserget_collection = Pick<User, 'id' | 'firstname' | 'lastname' | 'email' | 'phoneNumber' | 'picture'>;
