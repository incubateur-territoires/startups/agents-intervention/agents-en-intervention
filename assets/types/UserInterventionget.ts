import { User } from './User';

export type UserInterventionget = Pick<User, 'id' | 'firstname' | 'lastname' | 'email' | 'phoneNumber' | 'picture' | 'role'>;
