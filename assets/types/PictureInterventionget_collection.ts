import { Picture } from './Picture';

export type PictureInterventionget_collection = Pick<Picture, 'id' | 'fileName' | 'url' | 'tag' | 'small' | 'medium' | 'original'>;
