import { Employer } from './Employer';

export type RecurringInterventionSequences = {
  employer: Employer;
  seq: number;
};
