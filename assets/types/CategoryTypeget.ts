import { Category } from './Category';

export type CategoryTypeget = Pick<Category, 'id' | 'name' | 'picture' | 'description' | 'position'>;
