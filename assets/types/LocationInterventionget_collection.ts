import { Location } from './Location';

export type LocationInterventionget_collection = Pick<Location, 'id' | 'street' | 'rest' | 'postcode' | 'city' | 'longitude' | 'latitude'>;
