import { User } from './User';

export type UserUserview = Pick<User, 'id' | 'login' | 'firstname' | 'lastname' | 'email' | 'phoneNumber' | 'createdAt' | 'picture' | 'employer' | 'active' | 'connectedAt' | 'roles' | 'role' | 'connectWithProConnect'>;
