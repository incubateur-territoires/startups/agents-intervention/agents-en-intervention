import { Type } from './Type';

export type TypeInterventionget_collection = Pick<Type, 'name' | 'category' | 'picture'>;
