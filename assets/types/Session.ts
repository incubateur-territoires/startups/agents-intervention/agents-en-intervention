
export type Session = {
  id: string;
  data: any;
  lifetime: number;
  time: number;
  unserializedData?: any | null;
};
