import { User } from './User';

export type UserUserget = Pick<User, 'id' | 'firstname' | 'lastname' | 'email' | 'phoneNumber' | 'picture'>;
