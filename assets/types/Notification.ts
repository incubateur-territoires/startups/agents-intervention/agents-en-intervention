import { User } from './User';
import { Intervention } from './Intervention';

export type Notification = {
  id: number;
  user: User;
  intervention: Intervention;
  title: string;
  message: any | null;
  isRead: boolean;
  createdAt: Date;
};
