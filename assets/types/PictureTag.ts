export enum PictureTag {
  After = "after",
  Before = "before",
  Avatar = "avatar",
}
