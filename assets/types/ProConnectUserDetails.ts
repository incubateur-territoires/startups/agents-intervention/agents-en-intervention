
export type ProConnectUserDetails = {
  email: string;
  siret: string;
  given_name?: string | null;
  usual_name?: string | null;
  phone_number?: string | null;
};
