import { Comment } from './Comment';

export type CommentInterventionget_collection = Pick<Comment, 'message' | 'createdAt' | 'author'>;
