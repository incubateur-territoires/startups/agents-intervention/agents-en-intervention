import { Type } from './Type';

export type TypeInterventionget = Pick<Type, 'id' | 'name' | 'category' | 'picture' | 'description'>;
