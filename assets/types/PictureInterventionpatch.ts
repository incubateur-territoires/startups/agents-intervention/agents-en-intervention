import { Picture } from './Picture';

export type PictureInterventionpatch = Pick<Picture, 'intervention'>;
