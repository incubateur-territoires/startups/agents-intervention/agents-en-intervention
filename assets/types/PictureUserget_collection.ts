import { Picture } from './Picture';

export type PictureUserget_collection = Pick<Picture, 'id' | 'fileName' | 'url' | 'tag'>;
