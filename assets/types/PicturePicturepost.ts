import { Picture } from './Picture';

export type PicturePicturepost = Pick<Picture, 'fileName' | 'tag' | 'intervention'>;
