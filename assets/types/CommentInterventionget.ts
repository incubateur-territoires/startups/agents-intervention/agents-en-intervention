import { Comment } from './Comment';

export type CommentInterventionget = Pick<Comment, 'message' | 'createdAt' | 'author'>;
