import { Icon, Map } from 'leaflet';
import { useEffect, useMemo, useRef, useState } from 'react';
import React from 'react';
import { MapContainer, Marker, TileLayer } from 'react-leaflet';
import { useAsyncFn, useAsyncRetry } from 'react-use';

import {
  ApiEmployersEmployerIdinterventionsGetCollectionError,
  ApiEmployersEmployerIdinterventionsGetCollectionPathParams,
  ApiEmployersEmployerIdinterventionsGetCollectionQueryParams,
} from '@aei/src/client/generated/components';
import { fetch } from '@aei/src/client/generated/fetcher';
import { Fab } from '@aei/src/components';
import { InterventionCard } from '@aei/src/components/InterventionCard';
import { InterventionViewAndMutateMode, useInterventionViewAndMutate } from '@aei/src/components/InterventionViewAndMutateProvider';
import { ErrorAlert } from '@aei/src/ui/ErrorAlert';
import { LoadingArea } from '@aei/src/ui/LoadingArea';
import { bluePinIcon, fabButtonSpacing, getCurrentPosition, greenPinIcon, redPinIcon, tileLayerDefaultProps } from '@aei/src/utils/map';
import useEmployerId from '@aei/src/utils/useEmployerId';
import { EmployerEmployerview } from '@aei/types/EmployerEmployerview';
import { Intervention } from '@aei/types/Intervention';
import { InterventionInterventionview } from '@aei/types/InterventionInterventionview';
import { Priority } from '@aei/types/Priority';
import { Status } from '@aei/types/Status';

export const INITIAL_ZOOM = 6;
export const WHEN_ZOOMED_IN = 18;
export const FLY_TO_OPTIONS = { animate: false };

const interventionIcon = (intervention: InterventionInterventionview): Icon => {
  if (intervention.priority === Priority.Urgent) {
    return redPinIcon;
  } else if (intervention.status === Status.ToValidate) {
    return greenPinIcon;
  } else {
    return bluePinIcon;
  }
};

export interface InterventionMapPageProps {
  employer: EmployerEmployerview;
  url: string;
}

export function InterventionMapPage({ employer, url }: InterventionMapPageProps) {
  const { setContext } = useInterventionViewAndMutate();
  const employerId = useEmployerId();
  const containerRef = useRef<HTMLInputElement>(null);
  const [map, setMap] = useState<Map | null>(null);
  // const [position, setPosition] = useState<LatLngExpression>([46.603354, 1.888334]); // Default to the center of France
  const [selectedInterventionId, setSelectedIntervention] = useState<Intervention['id'] | null>(null);

  const [getCurrentPositionState, _getCurrentPosition] = useAsyncFn(getCurrentPosition);

  const {
    value: interventions = [],
    loading,
    error,
    retry,
  } = useAsyncRetry(() =>
    fetch<
      InterventionInterventionview[],
      ApiEmployersEmployerIdinterventionsGetCollectionError,
      undefined,
      {},
      ApiEmployersEmployerIdinterventionsGetCollectionQueryParams,
      ApiEmployersEmployerIdinterventionsGetCollectionPathParams
    >({
      url: url,
      method: 'get',
    })
  );

  // Désactivation de la géolocalisation automatique (à supprimer si cela ne sert plus)
  // useEffect(() => {
  //   getCurrentPosition().then((userGeolocation) => {
  //     if (userGeolocation) {
  //       map?.flyTo([userGeolocation.coords.latitude, userGeolocation.coords.longitude], WHEN_ZOOMED_IN, FLY_TO_OPTIONS); // Set a scale to see the city and around
  //     }
  //   });
  // }, [map]);

  const selectedIntervention = useMemo(() => interventions.find((it) => it.id === selectedInterventionId), [interventions, selectedInterventionId]);

  /**
   * Préviens les bandes grises sur le côté de la carte
   */
  useEffect(() => {
    const container = containerRef.current;
    const resizeObserver = new ResizeObserver(() => {
      if (map) {
        map.invalidateSize();
      }
    });

    if (container) {
      resizeObserver.observe(container);

      return () => {
        resizeObserver.unobserve(container);
      };
    }
  }, [map]);

  if (error) {
    return <ErrorAlert errors={[error]} refetchs={retry} />;
  }

  if (loading) {
    return <LoadingArea ariaLabelTarget="page" />;
  }

  // TODO: ideally we could take all the markers to position the view to see them all
  console.log('center', employer ? [parseFloat(employer.latitude), parseFloat(employer.longitude)] : [46.603354, 1.888334]);
  return (
    <div ref={containerRef} style={{ display: 'flex', height: '100%', width: '100%', position: 'relative' }}>
      <MapContainer
        center={employer ? [parseFloat(employer.latitude), parseFloat(employer.longitude)] : [46.603354, 1.888334]}
        zoom={employer ? WHEN_ZOOMED_IN : INITIAL_ZOOM}
        attributionControl={false}
        ref={setMap}
        style={{ flex: 1, minHeight: '300px' }}
      >
        <TileLayer {...tileLayerDefaultProps} />
        {interventions?.map((intervention) => {
          return (
            <Marker
              key={intervention.id}
              position={[parseFloat(intervention.location.latitude), parseFloat(intervention.location.longitude)]}
              icon={interventionIcon(intervention)}
              eventHandlers={{
                click: (event) => {
                  if (intervention.id === selectedIntervention?.id) {
                    setSelectedIntervention(null);
                  } else {
                    setSelectedIntervention(intervention.id);

                    // Center it on map to not be hidden by the "dialog"
                    map?.flyTo(event.latlng, WHEN_ZOOMED_IN, FLY_TO_OPTIONS);
                  }
                },
              }}
            />
          );
        })}
      </MapContainer>
      {selectedIntervention && (
        <div
          style={{
            position: 'absolute',
            width: '90%',
            maxWidth: '600px',
            bottom: 'calc(5vh + 60px)',
            left: '50%',
            transform: 'translateX(-50%)',
            zIndex: 1000,
          }}
        >
          <a
            href={`#intervention-${selectedIntervention.id}`}
            onClick={() => {
              setContext({
                mode: InterventionViewAndMutateMode.View,
                interventionId: selectedIntervention.id,
                retry,
              });
            }}
          >
            <InterventionCard intervention={selectedIntervention} />
          </a>
        </div>
      )}

      <Fab
        href={`/${employerId}/interventions`}
        right={fabButtonSpacing}
        top={fabButtonSpacing}
        color="secondary"
        size="small"
        aria-label="voir la liste des interventions"
        label="Voir la liste des interventions"
      >
        <span className="fr-icon-list-unordered" aria-hidden="true"></span>
      </Fab>
      <Fab
        onClick={() =>
          setContext({
            mode: InterventionViewAndMutateMode.Create,
            retry,
          })
        }
        left="calc(50% - 28px)"
        bottom={fabButtonSpacing}
        color="primary"
        size="large"
        aria-label="ajouter une intervention"
        label="ajouter une intervention"
      >
        <span className="fr-icon-add-line" aria-hidden="true"></span>
      </Fab>
      <Fab
        size="small"
        onClick={() => {
          _getCurrentPosition().then((userGeolocation) => {
            if (userGeolocation) {
              map?.flyTo([userGeolocation.coords.latitude, userGeolocation.coords.longitude], WHEN_ZOOMED_IN, FLY_TO_OPTIONS); // Set a scale to see the city and around
            }
          });
        }}
        right={fabButtonSpacing}
        bottom={fabButtonSpacing}
        aria-label="se géolocaliser"
        color="secondary"
        loading={getCurrentPositionState.loading}
        label="Se géolocaliser"
      >
        <span className="fr-icon-send-plane-line" aria-hidden="true"></span>
      </Fab>
    </div>
  );
}
