import { startReactDsfr } from '@codegouvfr/react-dsfr/spa';
import { QueryClient, QueryClientProvider } from '@tanstack/react-query';
import React, { ReactNode } from 'react';
import { I18nextProvider } from 'react-i18next';

import InterventionViewAndMutateProvider from '@aei/src/components/InterventionViewAndMutateProvider';
import { i18n } from '@aei/src/i18n';
import { ModalProvider } from '@aei/src/ui/modal/ModalProvider';

startReactDsfr({ defaultColorScheme: 'system' });

const queryClient = new QueryClient({
  defaultOptions: {
    queries: {
      retry: false, // cacheTime: 0,
    },
  },
});

export interface ApplicationProps {
  children: ReactNode;
}

const Application = ({ children }: ApplicationProps) => (
  <React.StrictMode>
    <QueryClientProvider client={queryClient}>
      <I18nextProvider i18n={i18n}>
        <ModalProvider>
          <InterventionViewAndMutateProvider>{children}</InterventionViewAndMutateProvider>
        </ModalProvider>
      </I18nextProvider>
    </QueryClientProvider>
  </React.StrictMode>
);

export default Application;
