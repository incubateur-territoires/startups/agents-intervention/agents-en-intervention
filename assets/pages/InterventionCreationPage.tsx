import {
  useApiCategoriesGetCollection,
  useApiEmployersEmployerIdactiveAgentsGetCollection,
  useApiEmployersIdGet,
  useApiTypesGetCollection,
} from '@aei/client/fetchers';
import { useApiInterventionsPost } from '@aei/src/client/generated/components';
import { InterventionViewAndMutateContextDefaults, useInterventionViewAndMutate } from '@aei/src/components/InterventionViewAndMutateProvider';
import { MutateInterventionForm } from '@aei/src/components/MutateInterventionForm';
import useEmployerId from '@aei/src/utils/useEmployerId';
import { Frequency, Priority, Status } from '@aei/types';

interface InterventionCreationPageProps {
  defaults?: InterventionViewAndMutateContextDefaults;
  onSuccess?: () => void;
}

export function InterventionCreationPage({ defaults = {}, onSuccess }: InterventionCreationPageProps) {
  const { resetContext } = useInterventionViewAndMutate();
  const employerId = useEmployerId();

  const { data: agents = [] } = useApiEmployersEmployerIdactiveAgentsGetCollection({ pathParams: { employerId: `${employerId}` } });
  const { data: categories = [] } = useApiCategoriesGetCollection({});
  const { data: employer } = useApiEmployersIdGet({ pathParams: { id: `${employerId}` } });
  const { data: types = [] } = useApiTypesGetCollection({});

  const createIntervention = useApiInterventionsPost();

  return (
    <MutateInterventionForm
      agents={agents}
      types={types}
      categories={categories}
      employer={employer}
      onSuccess={(input) =>
        createIntervention
          .mutateAsync({
            body: {
              title: input.title,
              description: input.description || '',
              priority: input.priority ? Priority.Urgent : Priority.Normal,
              type: `api/types/${input.typeId}`,
              location: input.location,
              participants: input.participantsIds ? input.participantsIds.map((pId) => `api/users/${pId}`) : undefined,
              pictures: [
                // TODO: it could be merged into a single array but the UI should be update to take in account all cases (creation: only before && edit: both)
                ...(input.beforePicture ? [`api/pictures/${input.beforePicture.id}`] : []),
                ...(input.afterPicture ? [`api/pictures/${input.afterPicture.id}`] : []),
              ],
              employer: `api/employers/${employerId}`,
              startAt: input.priority ? new Date() : input.startAt,
              endAt: input.priority ? new Date(new Date().getTime() + 1.5 * 3600 * 1000) : input.endAt,
              endedAt: input.frequency !== Frequency.ONE_TIME ? input.endedAt : null,
              frequency: input.frequency,
              frequencyInterval: input.frequencyInterval,
              status: Status.ToDo,
            } as any,
          })
          .then(async () => {
            if (onSuccess) {
              return onSuccess();
            }
          })
          .then(async () => {
            return resetContext();
          })
      }
      prefill={{
        startAt: defaults?.startAt,
        endAt: defaults?.endAt,
        endedAt: defaults?.endedAt,
      }}
    />
  );
}
