import {
  useApiCategoriesGetCollection,
  useApiEmployersEmployerIdactiveAgentsGetCollection,
  useApiInterventionsIdGet,
  useApiTypesGetCollection,
} from '@aei/client/fetchers';
import { useApiInterventionsIdPatch } from '@aei/src/client/generated/components';
import { useInterventionViewAndMutate } from '@aei/src/components/InterventionViewAndMutateProvider';
import { MutateInterventionForm } from '@aei/src/components/MutateInterventionForm';
import { ErrorAlert } from '@aei/src/ui/ErrorAlert';
import { LoadingArea } from '@aei/src/ui/LoadingArea';
import { AggregatedQueries } from '@aei/src/utils/react-query';
import useEmployerId from '@aei/src/utils/useEmployerId';
import { Priority } from '@aei/types';

export interface InterventionEditPageProps {
  params: { interventionId: string };
  onSuccess?: () => void;
}

export function InterventionEditPage({ params: { interventionId }, onSuccess }: InterventionEditPageProps) {
  const { resetContext } = useInterventionViewAndMutate();
  const employerId = useEmployerId();

  const { data: agents = [] } = useApiEmployersEmployerIdactiveAgentsGetCollection({ pathParams: { employerId: `${employerId}` } });
  const { data: categories = [] } = useApiCategoriesGetCollection({});
  const { data: types = [] } = useApiTypesGetCollection({});

  const updateIntervention = useApiInterventionsIdPatch();

  const getIntervention = useApiInterventionsIdGet({
    pathParams: {
      id: interventionId,
    },
  });

  const aggregatedQueries = new AggregatedQueries(getIntervention);

  const intervention = getIntervention.data;

  if (aggregatedQueries.hasError) {
    return <ErrorAlert errors={aggregatedQueries.errors} refetchs={aggregatedQueries.refetchs} />;
  } else if (aggregatedQueries.isLoading) {
    return <LoadingArea ariaLabelTarget="page" />;
  } else if (!intervention) {
    return <p>Not found</p>;
  }

  const beforeImages = intervention.pictures?.filter((p) => p.tag === 'before');
  const afterImages = intervention.pictures?.filter((p) => p.tag === 'after');

  return (
    <MutateInterventionForm
      agents={agents}
      types={types}
      categories={categories}
      onSuccess={async (input) => {
        // `body: any` needed due to missing types (ref: https://github.com/fabien0102/openapi-codegen/issues/198)
        await updateIntervention.mutateAsync({
          pathParams: {
            id: `${intervention.id}`,
          },
          headers: {
            'Content-Type': 'application/merge-patch+json',
          },
          ...{
            body: {
              title: input.title,
              description: input.description,
              priority: input.priority ? Priority.Urgent : Priority.Normal,
              type: `api/types/${input.typeId}`,
              location: input.location,
              participants: input.participantsIds ? input.participantsIds.map((pId) => `api/users/${pId}`) : undefined,
              pictures: [
                // TODO: it could be merged into a single array but the UI should be update to take in account all cases (creation: only before && edit: both)
                ...(input.beforePicture ? [`api/pictures/${input.beforePicture.id}`] : []),
                ...(input.afterPicture ? [`api/pictures/${input.afterPicture.id}`] : []),
              ],
              startAt: input.startAt ? new Date(input.startAt) : null, // data.startAt?new Date(data.startAt) : new Date()
              endAt: input.endAt ? new Date(input.endAt) : null,
              endedAt: input.endedAt ? new Date(input.endedAt) : null,
              frequency: input.frequency,
              frequencyInterval: input.frequencyInterval,
            } as any,
          },
        });

        if (onSuccess) {
          onSuccess();
        }
        resetContext();
      }}
      intervention={intervention}
      prefill={{
        title: intervention.title,
        beforePicture: beforeImages.length > 0 ? beforeImages[0] : null,
        afterPicture: afterImages.length > 0 ? afterImages[0] : null,
        typeId: intervention.type.id.toString(),
        categoryId: intervention.category?.id.toString() || undefined,
        description: intervention.description,
        location: intervention.location,
        priority: intervention.priority === Priority.Urgent,
        participantsIds: intervention.participants.map((p) => `${p.id}`),
        startAt: intervention.startAt ? new Date(intervention.startAt) : null,
        endAt: intervention.endAt ? new Date(intervention.endAt) : null,
        endedAt: intervention.endedAt ? new Date(intervention.endedAt) : undefined,
        frequency: intervention.frequency,
        frequencyInterval: intervention.frequencyInterval,
      }}
    />
  );
}
