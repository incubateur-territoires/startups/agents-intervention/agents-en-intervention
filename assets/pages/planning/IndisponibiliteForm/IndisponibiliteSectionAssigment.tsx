import RadioButtons from '@codegouvfr/react-dsfr/RadioButtons';
import { useFormContext } from 'react-hook-form';
import { useTranslation } from 'react-i18next';

import { Role } from '@aei/types';
import { UserUserview } from '@aei/types/UserUserview';

export interface AssignmentFormProps {
  agents: UserUserview[];
}

const roles = [Role.Agent, Role.Director, Role.AdminEmployer];

function IndisponibiliteSectionAssigment({ agents }: AssignmentFormProps) {
  const {
    register,
    formState: { errors },
  } = useFormContext();
  const { t } = useTranslation('common');

  return roles.map((role) => (
    <RadioButtons
      id={`assignment-${role}`}
      key={`assignment-${role}`}
      stateRelatedMessage={errors.participant?.message?.toString() || ''}
      state={errors.participant && 'error'}
      legend={
        <span
          style={{
            color: '#666',
          }}
        >
          {t(`model.user.roles.${role}`).toUpperCase()}
        </span>
      }
      // name="agent"
      options={agents
        .filter((it) => it.role === role)
        .map((participant) => ({
          id: `assignment-${participant.id}`,
          label: `${participant.firstname} ${participant.lastname}`,
          // @ts-ignore
          nativeInputProps: { value: participant.id, ...register('participant') },
        }))}
    />
  ));
}

export default IndisponibiliteSectionAssigment;
