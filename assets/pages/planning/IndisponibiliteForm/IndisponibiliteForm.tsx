import React, { useCallback, useEffect, useMemo } from 'react';
import { FormProvider, useForm } from 'react-hook-form';
import { useAsyncFn } from 'react-use';
import z from 'zod';

import { fetchApiIndisponibilitePost, useApiEmployersEmployerIdactiveAgentsGetCollection } from '@aei/client/fetchers';
import IndisponibiliteSectionAssigment from '@aei/pages/planning/IndisponibiliteForm/IndisponibiliteSectionAssigment';
import IndisponibiliteSectionIndispo from '@aei/pages/planning/IndisponibiliteForm/IndisponibiliteSectionIndispo';
import { LoadingButton } from '@aei/src/components';
import MyDrawerDSFR from '@aei/src/components/MyDrawerDSFR';
import { useUser } from '@aei/src/utils/auth';
import useEmployerId from '@aei/src/utils/useEmployerId';
import useMobileFormat from '@aei/src/utils/useMobileFormat';
import { Frequency, Role } from '@aei/types';

export enum Section {
  Indispo = 'inspo',
  Assignment = 'assignment',
}

export const IndisponibiliteFormSchema = z.object({
  title: z.string(),

  start: z.coerce.date(),
  end: z.coerce.date(),
  recEnd: z.coerce.date().optional(),

  frequency: z.enum([Frequency.ONE_TIME, Frequency.DAILY, Frequency.WEEKLY, Frequency.MONTHLY, Frequency.YEARLY]),
  frequencyInterval: z.number().optional(),

  participant: z.string(),

  intervention_type: z.enum(['onetime', 'recurring']),
  recurring: z.coerce.number().optional(),
});

const validations = {
  [Section.Indispo]: IndisponibiliteFormSchema.omit({
    participant: true,
  }),
  [Section.Assignment]: IndisponibiliteFormSchema,
};

export type IndisponibiliteFormSchemaType = z.infer<typeof IndisponibiliteFormSchema>;

export interface IndisponibiliteFormProps {
  onClose?: () => void;
  onSubmitError?: (error: any) => void | Promise<void>;
  onSubmitSuccess?: () => void | Promise<void>;
  open?: boolean;
}

const IndisponibiliteForm = ({ onClose, onSubmitError, onSubmitSuccess, open = false }: IndisponibiliteFormProps) => {
  const { hasRole, user } = useUser();
  const mobileFormat = useMobileFormat();
  const employerId = useEmployerId();
  const methods = useForm<IndisponibiliteFormSchemaType>({
    defaultValues: {
      // @ts-ignore
      start: new Date().toISOString().substring(0, 16), // @ts-ignore
      end: new Date(new Date().getTime() + 1.5 * 3600 * 1000).toISOString().substring(0, 16),
      intervention_type: 'onetime',
      frequency: Frequency.ONE_TIME,
    },
  });
  const { control, handleSubmit } = methods;
  const { data: agents = [] } = useApiEmployersEmployerIdactiveAgentsGetCollection({ pathParams: { employerId: `${employerId}` } });

  const sectionFlow: Section[] = useMemo(() => {
    const values = Object.values(Section);
    if (!hasRole(Role.AdminEmployer) && !hasRole(Role.Director) && !hasRole(Role.Admin)) {
      return values.filter((val) => val !== Section.Assignment);
    }
    return values;
  }, [hasRole]);

  const context = React.useRef({ step: Section.Indispo, submitter: '' });
  const setStep = useCallback((step: Section) => {
    context.current.step = step ?? Section.Indispo;
  }, []);

  useEffect(() => {
    if (open) {
      setStep(Section.Indispo);
      methods.reset();
    }
    return () => setStep(Section.Indispo);
  }, [open, setStep, methods.reset]);

  const _onClose = useCallback(() => {
    methods.reset();
    setStep(Section.Indispo);
    if (onClose) {
      onClose();
    }
  }, [methods, onClose, setStep]);

  const displayedSection = context.current.step;

  const nextSection = useMemo(() => {
    const currentIndex = sectionFlow.indexOf(displayedSection);
    return sectionFlow[currentIndex + 1] || null;
  }, [displayedSection, sectionFlow]);

  const prevSection = useMemo(() => {
    const currentIndex = sectionFlow.indexOf(displayedSection);

    return sectionFlow[currentIndex - 1] || null;
  }, [displayedSection, sectionFlow]);

  const [onSuccessState, onSuccess] = useAsyncFn(
    async (args: IndisponibiliteFormSchemaType) =>
      fetchApiIndisponibilitePost({
        pathParams: { employerId: `${employerId}` },
        body: {
          endDate: args.end, //.toISOString(),
          frequency: args.frequency as Frequency,
          frequencyInterval: args.frequencyInterval ?? 1,
          recEndDate: args.recEnd, //?.toISOString(),
          startDate: args.start, //.toISOString(),
          title: args.title,
          participant: args.participant ?? user?.id.toString(),
        },
      })
        .then(() => {
          if (onSubmitSuccess) {
            onSubmitSuccess();
          }
          _onClose();
        })
        .catch((error) => {
          console.error(error);
          if (onSubmitError) {
            onSubmitError(error);
          }
          return Promise.reject(error);
        }),
    [setStep, methods.reset, onSubmitError, onSubmitSuccess, user?.id, employerId]
  );

  const onSubmit = (e: React.FormEvent) => {
    const submitter = context.current.submitter;
    const doHandleSubmit = handleSubmit(
      // onValid callback
      (data) => {
        const currentIndex = sectionFlow.indexOf(displayedSection);
        const prevSection = sectionFlow[currentIndex - 1] || null;
        const nextSection = sectionFlow[currentIndex + 1] || null;
        const newStep = submitter === 'prev' ? prevSection : nextSection;

        if (!nextSection && submitter !== 'prev') {
          return onSuccess(data);
        } else {
          setStep(newStep);
          context.current.step = newStep;
        }
      },

      // onInvalid callback
      () => {}
    );
    doHandleSubmit(e).catch((e) => {
      console.error(e);
    });
  };

  console.log(control._formValues, validations[context.current.step].safeParse(control._formValues));

  return (
    <MyDrawerDSFR onClose={_onClose} open={open} title="Indisponibilité">
      <FormProvider {...methods}>
        <form onSubmit={onSubmit} className="aei-stack aei-stack-large-padding">
          {!mobileFormat && prevSection && (
            <button
              className="fr-btn fr-btn--icon-left fr-btn--tertiary fr-btn--tertiary-no-outline fr-icon-arrow-left-line"
              onClick={() => (context.current.submitter = 'prev')}
              type="submit"
            >
              Retour
            </button>
          )}

          <div className="aei-stack aei-stack-no-padding">
            {displayedSection === Section.Indispo && <IndisponibiliteSectionIndispo />}
            {displayedSection === Section.Assignment && <IndisponibiliteSectionAssigment agents={agents} />}

            <div className="aei-stack aei-stack-large-gap aei-stack-no-padding">
              <LoadingButton
                loading={onSuccessState.loading}
                className="fr-btn fr-btn--lg fr-btn--wide"
                onClick={() => (context.current.submitter = 'next')}
                type="submit"
                disabled={!validations[context.current.step].safeParse(control._formValues).success}
              >
                {nextSection ? 'Suivant' : 'Valider'}
              </LoadingButton>

              {prevSection && mobileFormat && (
                <LoadingButton
                  disabled={onSuccessState.loading}
                  className="fr-btn fr-btn--lg fr-btn--wide fr-btn--secondary"
                  onClick={() => (context.current.submitter = 'prev')}
                  type="submit"
                >
                  Retour
                </LoadingButton>
              )}
            </div>
          </div>
        </form>
      </FormProvider>
    </MyDrawerDSFR>
  );
};

export default IndisponibiliteForm;
