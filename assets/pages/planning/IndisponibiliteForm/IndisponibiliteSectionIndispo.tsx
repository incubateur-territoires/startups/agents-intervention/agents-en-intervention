import { Input } from '@codegouvfr/react-dsfr/Input';
import { Select } from '@codegouvfr/react-dsfr/Select';
import React, { useEffect } from 'react';
import { useFormContext } from 'react-hook-form';
import { useTranslation } from 'react-i18next';

import { IndisponibiliteFormSchemaType } from '@aei/pages/planning/IndisponibiliteForm/IndisponibiliteForm';
import { computeRecurring } from '@aei/src/components/MutateInterventionForm/InterventionType/RecurrenceType';
import { MutateInterventionSchemaType } from '@aei/src/models/actions/intervention';
import { getWeekOfMonth } from '@aei/src/models/getWeekOfMonth';
import { Frequency } from '@aei/types';

const IndisponibiliteSectionIndispo = () => {
  const { t } = useTranslation('common');
  const methods = useFormContext<IndisponibiliteFormSchemaType>();
  const {
    formState: { errors },
    register,
    setValue,
  } = methods;
  const { intervention_type, recEnd, start, recurring } = methods.watch();

  useEffect(() => {
    if (intervention_type === 'onetime') {
      setValue('frequency', Frequency.ONE_TIME);
      setValue('frequencyInterval', 1);
      return;
    }

    const data: Partial<MutateInterventionSchemaType> = {
      frequency: Frequency.DAILY,
      frequencyInterval: 1,
    };
    switch (parseInt(String(recurring))) {
      case 1:
        console.log('daily, frequency 1');
        data.frequency = Frequency.DAILY;
        data.frequencyInterval = 1;
        break;
      case 2:
        console.log('weekly, frequency 1');
        data.frequency = Frequency.WEEKLY;
        data.frequencyInterval = 1;
        break;
      case 3:
        console.log('weekly, frequency 2');
        data.frequency = Frequency.WEEKLY;
        data.frequencyInterval = 2;
        break;
      case 4:
        console.log('monthly, frequency 1');
        data.frequency = Frequency.MONTHLY;
        data.frequencyInterval = 1;
        break;
      case 5:
        console.log('monthly, frequency 3');
        data.frequency = Frequency.MONTHLY;
        data.frequencyInterval = 3;
        break;
      case 6:
        console.log('monthly, frequency 6');
        data.frequency = Frequency.MONTHLY;
        data.frequencyInterval = 6;
        break;
      case 7:
        console.log('yearly, frequency 7');
        data.frequency = Frequency.YEARLY;
        data.frequencyInterval = 1;
        break;
      default:
        data.frequency = Frequency.DAILY;
        data.frequencyInterval = 1;
        setValue('recurring', computeRecurring(data.frequency, data.frequencyInterval));
    }
    setValue('frequency', data.frequency);
    setValue('frequencyInterval', data.frequencyInterval || 1);
  }, [intervention_type, recurring, setValue]);

  const weekday = start ? new Intl.DateTimeFormat('fr-FR', { weekday: 'long' }).format(new Date(start)) : '-';
  const weekOfMonth = start ? getWeekOfMonth(new Date(start)) : 0;

  return (
    <>
      <Input
        label="Titre de l'intervention"
        state={errors.title ? 'error' : 'default'}
        stateRelatedMessage={errors.title?.message}
        nativeInputProps={register('title')}
      />

      <fieldset className="fr-fieldset" id="radio-hint" aria-labelledby="radio-hint-legend radio-hint-messages">
        <legend className="fr-fieldset__legend--regular fr-fieldset__legend" id="radio-hint-legend">
          Cette intervention est :
        </legend>

        <div className="fr-fieldset__element">
          <div className="fr-radio-group">
            <input type="radio" id="radio-hint-1" value="onetime" {...methods.register('intervention_type')} />
            <label className="fr-label" htmlFor="radio-hint-1">
              Ponctuelle
            </label>
          </div>
        </div>

        {intervention_type === 'onetime' && (
          <div className="aei-stack aei-stack-large-padding aei-stack-no-padding aei-stack-fill-parent">
            <Input
              label="Début"
              // hintText="Date à laquelle l’intervention est prévue (facultatif)."
              nativeInputProps={{
                'aria-label': 'Date de début',
                type: 'datetime-local',
                ...methods.register('start'),
              }}
            />

            <Input
              label="Fin"
              // hintText="Date à laquelle l’intervention est prévue (facultatif)."
              nativeInputProps={{
                'aria-label': 'Date de fin',
                type: 'datetime-local',
                ...methods.register('end'),
              }}
            />
          </div>
        )}

        <div className="fr-fieldset__element">
          <div className="fr-radio-group">
            <input type="radio" id="radio-hint-2" value="recurring" {...methods.register('intervention_type')} />
            <label className="fr-label" htmlFor="radio-hint-2">
              Récurrente
            </label>
          </div>
        </div>

        {intervention_type === 'recurring' && (
          <div className="aei-stack aei-stack-large-padding aei-stack-no-padding aei-stack-fill-parent">
            <Input
              label="Début"
              // hintText="Date à laquelle l’intervention est prévue (facultatif)."
              nativeInputProps={{
                'aria-label': 'Date de début',
                type: 'datetime-local',
                ...methods.register('start'),
              }}
            />

            <Input
              label="Fin"
              // hintText="Date à laquelle l’intervention est prévue (facultatif)."
              nativeInputProps={{
                'aria-label': 'Date de fin',
                type: 'datetime-local',
                ...methods.register('end'),
              }}
            />

            <Select label="Récurrence" nativeSelectProps={methods.register('recurring')}>
              <option value={1}>Tous les jours</option>
              <option value={2}>Toutes les semaines, le {weekday}</option>
              <option value={3}>Toutes les deux semaines, le {weekday}</option>
              <option value={4}>
                Tous les mois, le {weekOfMonth}
                {weekOfMonth === 1 ? 'er' : 'ème'} {weekday}
              </option>
              <option value={5}>
                Tous les 3 mois, le {weekOfMonth}
                {weekOfMonth === 1 ? 'er' : 'ème'} {weekday}
              </option>
              <option value={6}>
                Tous les 6 mois, le {weekOfMonth}
                {weekOfMonth === 1 ? 'er' : 'ème'} {weekday}
              </option>
              <option value={7}>Tous les ans, le {t('date.dayMonth', { date: new Date(start || 'now') })}</option>
            </Select>

            <Input
              label="Date de fin de la récurrence"
              nativeInputProps={{
                'aria-label': 'Date de fin de la récurrence',
                type: 'date',
                ...register('recEnd'),
                value: new Date(recEnd || 'now')?.toLocaleDateString('fr').split('/').reverse().join('-'),
                onChange: (event) => setValue('recEnd', new Date(event.target.value)),
              }}
            />
          </div>
        )}
      </fieldset>
    </>
  );
};

export default IndisponibiliteSectionIndispo;
