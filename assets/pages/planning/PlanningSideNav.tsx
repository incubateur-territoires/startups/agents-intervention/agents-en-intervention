import Checkbox from '@codegouvfr/react-dsfr/Checkbox';
import * as React from 'react';
import { FormProvider, useFormContext, useWatch } from 'react-hook-form';
import { useToggle } from 'react-use';

import useRetrieveFilterParams from '@aei/pages/planning/useRetrieveFilterParams';
import { useUser } from '@aei/src/utils/auth';
import { Role } from '@aei/types';

import { PlanningSearch } from './PlanningPage';

interface EntryProps {
  name: 'status' | 'agents' | 'categories';
  label: string;
  options: { value: any; label: string }[];
}

const Abc = ({
  display = false,
  name,
  options,
}: {
  display: boolean;
  name: 'status' | 'agents' | 'categories';
  options: { value: any; label: string }[];
}) => {
  const methods = useFormContext<PlanningSearch>();

  return (
    <div style={{ display: display ? 'flex' : 'none', flexDirection: 'column', marginLeft: '1rem' }}>
      {options.map((opt) => (
        <Checkbox
          key={opt.value}
          options={[
            {
              label: opt.label,
              nativeInputProps: { ...methods.register(name), value: opt.value },
            },
          ]}
        />
      ))}
    </div>
  );
};

const Entry = ({ name, label, options }: EntryProps) => {
  const methods = useFormContext<PlanningSearch>();
  const value = useWatch({ name });
  const [display, toggle] = useToggle(false);

  const handleChange1 = (event: React.ChangeEvent<HTMLInputElement>) => {
    methods.setValue(name, event.target.checked ? options.map((o) => `${o.value}`) : []);
  };

  const allChecked = value.length === options.length;

  return (
    <div className="planning-side-nav">
      <Checkbox
        options={[
          {
            label: (
              <div
                style={{
                  minWidth: '100%',
                  display: 'flex',
                  justifyContent: 'space-between',
                  alignItems: 'center',
                  fontWeight: 'bold',
                }}
              >
                <span>{label}</span>
                <span
                  className={`fr-icon ${display ? 'fr-icon-arrow-up-s-line' : 'fr-icon-arrow-down-s-line'}`}
                  onClick={(e) => {
                    e.preventDefault();
                    toggle();
                  }}
                  onKeyDown={(e) => {
                    if (e.key === 'Space' || e.key === 'Enter') {
                      e.preventDefault();
                      toggle();
                    }
                  }}
                />
              </div>
            ),
            nativeInputProps: {
              className: value.length > 0 && value.length < options.length ? 'checkbox-intermediate' : '',
              checked: allChecked,
              onChange: handleChange1,
            },
          },
        ]}
      />
      <Abc display={display} name={name} options={options} />
    </div>
  );
};

export interface PlanningSideNavProps {}

const PlanningSideNav = () => {
  const { hasRole } = useUser();
  const { value: params, loading: _loading } = useRetrieveFilterParams(true);
  const methods = useFormContext<PlanningSearch>();

  if (_loading || !params) return null;

  return (
    <FormProvider {...methods}>
      {!hasRole(Role.Agent) && (
        <Entry
          name="agents"
          label="Assignée à"
          options={[
            ...params.agents.map((a) => ({
              value: a.id,
              label: `${a.firstname} ${a.lastname}`,
            })),
            { value: 0, label: 'Non assigné' },
          ]}
        />
      )}
      <Entry name="status" label="Statuts" options={Object.entries(params.statuses).map(([s, label]) => ({ value: s, label }))} />
      <Entry name="categories" label="Catégories" options={params.categories.map((c) => ({ value: c.id, label: c.name }))} />
    </FormProvider>
  );
};

export default PlanningSideNav;
