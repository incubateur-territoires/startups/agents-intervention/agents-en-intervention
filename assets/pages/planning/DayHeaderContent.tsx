import { DayHeaderContentArg } from '@fullcalendar/core';

const style = { fontSize: '2rem', fontWeight: 'lighter', lineHeight: '3rem' };

function DayHeaderContent({ text }: DayHeaderContentArg) {
  console.log('DayHeaderContent render', text);
  const [day, dayNumber] = text.split(' ');
  const dayUpper = day.charAt(0).toUpperCase() + day.slice(1);
  return (
    <span>
      {dayUpper} <br />
      <span style={style}>{dayNumber}</span>
    </span>
  );
}

export default DayHeaderContent;
