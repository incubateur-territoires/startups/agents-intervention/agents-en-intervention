import { useMemo } from 'react';
import { useTranslation } from 'react-i18next';
import { useAsyncRetry } from 'react-use';

import {
  ApiCategoriesGetCollectionResponse,
  ApiEmployersEmployerIdactiveAgentsGetCollectionResponse,
  ApiEmployersEmployerIdusersGetCollectionResponse,
  fetchApiCategoriesGetCollection,
  fetchApiEmployersEmployerIdactiveAgentsGetCollection,
  fetchApiEmployersEmployerIdusersGetCollection,
} from '@aei/src/client/generated/components';
import { useUser } from '@aei/src/utils/auth';
import useEmployerId from '@aei/src/utils/useEmployerId';
import { Role } from '@aei/types';

/**
 * @deprecated use Twig instead
 * @param listAgents
 */
const useRetrieveFilterParams = (listAgents = false) => {
  const { t } = useTranslation();

  const employerId = useEmployerId();
  const { isCoordinateurOrAdminEmployer } = useUser();
  const statuses = useMemo<Record<string, string>>(() => {
    const statuses: Record<string, string> = {
      blocked: t('model.intervention.status.enum.blocked'),
      finished: t('model.intervention.status.enum.finished'),
      'in-progress': t('model.intervention.status.enum.in-progress'),
      'to-do': t('model.intervention.status.enum.to-do'),
    };

    if (isCoordinateurOrAdminEmployer) {
      statuses['rejected'] = t('model.intervention.status.enum.rejected');
    }

    return statuses;
  }, [t]);

  const ret = useAsyncRetry(() =>
    Promise.all([
      fetchApiEmployersEmployerIdusersGetCollection({
        pathParams: {
          employerId: `${employerId}`,
        }, // @ts-ignore
        // queryParams: { active: true, 'roles[0]': 'ROLE_AGENT', 'roles[1]': 'ROLE_DIRECTOR' },
      }),
      fetchApiCategoriesGetCollection({}),
      listAgents
        ? fetchApiEmployersEmployerIdactiveAgentsGetCollection({
            pathParams: {
              employerId: `${employerId}`,
            },
          })
        : [],
    ]).then(([users = [], categories = [], agentsActives = []]) => ({
      agents: users.filter((u) => u.roles?.includes(Role.Agent) || u.roles?.includes(Role.AdminEmployer) || u.roles?.includes(Role.Director)),
      agentsActives,
      categories,
      statuses,
      users,
    }))
  );

  return {
    ...ret,
    value: ret.value || {
      agents: [] as ApiEmployersEmployerIdusersGetCollectionResponse,
      agentsActives: [] as ApiEmployersEmployerIdactiveAgentsGetCollectionResponse,
      categories: [] as ApiCategoriesGetCollectionResponse,
      statuses: {} as Record<string, string>,
      users: [] as ApiEmployersEmployerIdusersGetCollectionResponse,
    },
  };
};

export default useRetrieveFilterParams;
