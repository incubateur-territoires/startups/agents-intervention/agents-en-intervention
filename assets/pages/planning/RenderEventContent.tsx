import { EventContentArg } from '@fullcalendar/core';
import React from 'react';

import { Avatar } from '@aei/src/components';
import { InterventionPriorityChip } from '@aei/src/components/InterventionPriorityChip';
import { InterventionStatusChip } from '@aei/src/components/InterventionStatusChip';
import { User } from '@aei/types';
import { Priority } from '@aei/types/Priority';

const RenderEventContent = (eventInfo: EventContentArg) => {
  if (eventInfo.event.source?.format === 'ics') {
    const agent: User = eventInfo.event.source?.internalEventSource?._raw.extraParams.agent;
    return (
      <div className={`fc-custom-event`}>
        <span className="fc-custom-event-hour" style={{ display: 'inline-flex' }}>
          {eventInfo.event.start?.toLocaleTimeString('FR-fr', {
            hour: '2-digit',
            minute: '2-digit',
          })}{' '}
          {agent && (
            <div className="fc-custom-event-participant">
              <Avatar fontSize={8} fullName={`${agent.firstname} ${agent.lastname}`} size={12} />
            </div>
          )}
        </span>
        <span className="fc-custom-event-title">{eventInfo.event.title}</span>
      </div>
    );
  }

  return (
    <div className={`fc-custom-event`}>
      <span className="fc-custom-event-hour" style={{ display: 'inline-flex' }}>
        {eventInfo.event.start?.toLocaleTimeString('FR-fr', {
          hour: '2-digit',
          minute: '2-digit',
        })}{' '}
        {eventInfo.event.extendedProps.intervention?.participants?.map((participant: any) => {
          return (
            <div key={participant.id} className="fc-custom-event-participant">
              <Avatar fontSize={8} fullName={`${participant.firstname} ${participant.lastname}`} size={12} />
            </div>
          );
        })}
      </span>
      <span className="fc-custom-event-title">{eventInfo.event.title}</span>
      <div>
        {eventInfo.event.extendedProps.intervention && (
          <InterventionStatusChip
            status={eventInfo.event.extendedProps.intervention.status}
            participants={eventInfo.event.extendedProps.intervention.participants}
          />
        )}
        {eventInfo.event.extendedProps.intervention?.priority === Priority.Urgent && <InterventionPriorityChip priority={Priority.Urgent} />}
      </div>
    </div>
  );
};

export default RenderEventContent;
