import Alert from '@codegouvfr/react-dsfr/Alert';
import {
  CustomButtonInput,
  DateSelectArg,
  EventClickArg,
  EventContentArg,
  EventDropArg,
  EventSourceFuncArg,
  EventSourceInput,
  FormatterInput,
} from '@fullcalendar/core';
import frLocale from '@fullcalendar/core/locales/fr';
import dayGridPlugin from '@fullcalendar/daygrid';
import iCalendarPlugin from '@fullcalendar/icalendar';
import interactionPlugin, { DateClickArg, EventResizeDoneArg } from '@fullcalendar/interaction';
import FullCalendar from '@fullcalendar/react';
import timeGridPlugin from '@fullcalendar/timegrid';
import React, { useCallback, useEffect, useMemo, useRef, useState } from 'react';
import ReactDOM, { createPortal } from 'react-dom';
import { FormProvider, useForm } from 'react-hook-form';
import { useToggle } from 'react-use';

import { fetchApiEmployersEmployerIdinterventionsGetCollection, useApiInterventionsIdPatch } from '@aei/client/fetchers';
import IndisponibiliteForm from '@aei/pages/planning/IndisponibiliteForm';
import PlanningSideNav from '@aei/pages/planning/PlanningSideNav';
import useRetrieveFilterParams from '@aei/pages/planning/useRetrieveFilterParams';
import { Fab } from '@aei/src/components';
import { InterventionViewAndMutateMode, useInterventionViewAndMutate } from '@aei/src/components/InterventionViewAndMutateProvider';
import LinearProgress from '@aei/src/components/LinearProgress/LinearProgress';
import Snackbar from '@aei/src/components/Snackbar/Snackbar';
import { InterventionStatusSchema } from '@aei/src/models/entities/intervention';
import { useUser } from '@aei/src/utils/auth';
import useEmployerId from '@aei/src/utils/useEmployerId';
import useMobileFormat from '@aei/src/utils/useMobileFormat';
import { InterventionInterventionview } from '@aei/types';

import RenderEventContent from './RenderEventContent';

const fabButtonSpacing = '5vh';
const plugins = [dayGridPlugin, interactionPlugin, timeGridPlugin, iCalendarPlugin];

const mobileHeaderToolbar = {
  left: 'calendar prev,next title',
  center: '', // user can switch between the two
  right: '',
};

const slotLabelFormat: FormatterInput = {
  hour: 'numeric',
  minute: '2-digit',
  omitZeroMinute: false,
};

const locales = [frLocale];

const dayHeaderFormat: FormatterInput = { day: 'numeric', weekday: 'long' };

const views = {
  timeGridWeek: {
    allDaySlot: false, // slotDuration: "01:30"
  },
};

interface MyEvent {
  id: string;
  title: string;
  start?: Date;
  end?: Date;
  intervention: InterventionInterventionview;
}

interface State {
  open: boolean;
  success: boolean;
}

export interface PlanningSearch {
  agents: string[];
  categories: string[];
  status: string[];
}

const eventClassNames = (eventInfo: EventContentArg) => {
  if (eventInfo.event.source?.format === 'ics') return 'fc-v-event-indispo';
  if (eventInfo.event.extendedProps.intervention && !eventInfo.event.extendedProps.intervention?.participants?.length)
    return 'fc-v-event-not-affected';
  return '';
};

const PlanningPage = () => {
  const { isAgent, user } = useUser();
  const employerId = useEmployerId();
  const { value: params, loading: _loading } = useRetrieveFilterParams(true);
  const { setContext } = useInterventionViewAndMutate();
  const [loading, setLoading] = useState<boolean>(false);
  const calendar = useRef<FullCalendar | null>(null);
  const calendarPicker = useRef<HTMLInputElement | null>(null);
  const [state, setState] = React.useState<State>({
    open: false,
    success: true,
  });
  const { open, success } = state;
  const [displayIndisponibiliteForm, toggleIndisponibiliteForm] = useToggle(false);
  const mobileFormat = useMobileFormat();

  const headerToolbar = useMemo(
    () => ({
      left: 'calendar today prev,next title',
      center: 'timeGridWeek,timeGridDay', // user can switch between the two
      right: 'print newIntervention' + (isAgent ? '' : ' more'),
    }),
    [isAgent]
  );

  const handleClose = () => {
    setState({ ...state, open: false });
  };

  const updateIntervention = useApiInterventionsIdPatch();

  const methods = useForm<PlanningSearch>({
    defaultValues: {
      agents: [],
      categories: [],
      status: [],
    },
    context: { values: {} },
  });
  const values = methods.watch();

  const loadEvent = useCallback(
    async (event: EventSourceFuncArg): Promise<MyEvent[]> => {
      if (!event.start || !event.end) {
        return [];
      }

      const queryParams: Record<string, any> = {
        'exists[startAt]': true,
        'startAt[after]': event.start.toISOString(),
        'startAt[strictly_before]': event.end.toISOString(),
      };

      if (values.status.length > 0) {
        values.status?.map((status, index) => {
          queryParams[`status[${index}]`] = status;
        });
      } else {
        queryParams['status[0]'] = InterventionStatusSchema.Values['to-do'];
        queryParams['status[1]'] = InterventionStatusSchema.Values.blocked;
        queryParams['status[2]'] = InterventionStatusSchema.Values['in-progress'];
        queryParams['status[3]'] = InterventionStatusSchema.Values['finished'];
      }

      values.agents?.map((agent, index) => {
        if (agent != '0') {
          queryParams[`participants[${index}]`] = agent;
        } else {
          queryParams[`participants[${index}]`] = 'null';
        }
      });

      values.categories?.map((category, index) => {
        queryParams[`type.category[${index}]`] = `categories/${category}`;
      });

      return fetchApiEmployersEmployerIdinterventionsGetCollection({
        pathParams: { employerId: `${employerId}` },
        queryParams,
      }).then((value) =>
        value
          .filter((i) => !i.recurring)
          .map(
            (i): MyEvent => ({
              id: `${i.id}`,
              title: `#${i.logicId} - ${i.title}`,
              start: (i.startAt && new Date(i.startAt)) || undefined,
              end: (i.endAt && new Date(i.endAt)) || undefined,
              intervention: i,
            })
          )
      );
    },
    [employerId, values.agents, values.categories, values.status]
  );

  const retry = useCallback(() => {
    if (calendar.current) {
      calendar.current?.getApi().refetchEvents();
    }
  }, [calendar]);

  const customButtons: Record<string, CustomButtonInput> = useMemo(
    () => ({
      calendar: {
        icon: 'calendar',
        text: 'Calendrier',
        click: function () {
          calendarPicker.current && calendarPicker.current.showPicker();
        },
      },
      newIntervention: {
        text: 'Créer une intervention',
        click: () => setContext({ mode: InterventionViewAndMutateMode.Create }),
      },
      print: {
        icon: 'print',
        click: function () {
          window.print();
        },
      },
      more: {
        icon: 'more fr-icon-add-line',
        click: function (event: MouseEvent) {
          const el = event.target as HTMLElement;
          let button: HTMLButtonElement;
          if (el.tagName === 'button') {
            button = el as HTMLButtonElement;
          } else {
            button = el.closest('button') as HTMLButtonElement;
          }

          // @ts-ignore
          let rect = button?.getBoundingClientRect();
          let dropdownMenu = document.getElementById('dropdownMenuPlanning');

          // @ts-ignore
          dropdownMenu.style.display = 'block';
          // @ts-ignore
          dropdownMenu.style.right = window.innerWidth - rect.right + 'px';
          // @ts-ignore
          dropdownMenu.style.top = rect.bottom + window.scrollY + 'px';
        },
      },
    }),
    [setContext]
  );

  const createIntervention = useCallback(
    (select: DateSelectArg) => {
      console.log('createIntervention');
      setContext({
        mode: InterventionViewAndMutateMode.Create,
        defaults: { startAt: select.start, endAt: select.end },
        retry,
        onClose: () => calendar.current?.getApi().unselect(),
      });
    },
    [retry, setContext]
  );

  const dateClick = useCallback(
    (dateClick: DateClickArg) => {
      console.log('dateClick');
      const end = new Date(dateClick.date.getTime() + 1.5 * 3600 * 1000);
      calendar.current?.getApi().addEvent(
        {
          id: `${-Math.floor(Math.random() * 100000)}`,
          start: dateClick.date,
          end,
        },
        false
      );
      setContext({
        mode: InterventionViewAndMutateMode.Create,
        defaults: { startAt: dateClick.date, endAt: end },
        retry,
        onClose: () => calendar.current?.getApi().removeAllEvents(),
      });
    },
    [retry, setContext]
  );

  const eventClick = useCallback(
    (info: EventClickArg) => {
      if (info.event.source?.internalEventSource._raw.extraParams?.source === 'ics') {
        return;
      }

      if (!info.event.id || parseInt(info.event.id) < 0) {
        calendar.current?.getApi().getEventById(info.event.id)?.remove();
        return;
      }
      setContext({
        mode: InterventionViewAndMutateMode.View,
        interventionId: parseInt(info.event.id),
        onClose: () => calendar.current?.getApi().unselect(),
      });
    },
    [setContext]
  );

  const eventDropOrResize = useCallback(
    (info: EventDropArg | EventResizeDoneArg) =>
      updateIntervention
        .mutateAsync({
          pathParams: {
            id: `${info.event.id}`,
          },
          headers: {
            'Content-Type': 'application/merge-patch+json',
          },
          ...({
            body: {
              startAt: info.event.start,
              endAt: info.event.end,
            },
          } as any),
        })
        .then(retry)
        .catch(info.revert),
    [retry, updateIntervention]
  );

  const onSuccess = useCallback(() => {
    setContext({ mode: undefined, interventionId: undefined });
    setState({ open: true, success: true });
    retry();
  }, [retry, setContext]);

  useEffect(() => {
    setContext({ retry, onSuccess });
  }, [onSuccess, retry, setContext]);

  const eventSources = useMemo<EventSourceInput[]>(() => {
    const eventSources: EventSourceInput[] = [loadEvent];

    if (isAgent && user) {
      eventSources.push({
        url: `/calendar/agent/${user.id}/indisponibilite.ics`,
        id: `/calendar/agent/${user.id}/indisponibilite.ics`,
        format: 'ics',
        extraParams: {
          agent: user,
          cachebuster: new Date().valueOf(),
          source: 'ics',
        },
      });
    } else {
      eventSources.push(
        ...params.agentsActives.map((agent) => ({
          url: `/calendar/agent/${agent.id}/indisponibilite.ics`,
          id: `/calendar/agent/${agent.id}/indisponibilite.ics`,
          format: 'ics',
          extraParams: {
            agent,
            cachebuster: new Date().valueOf(),
            source: 'ics',
          },
        }))
      );
    }

    // return [
    //   loadEvent,
    //   ...,
    //   // ,
    //   // {
    //   //   url: `/${employerId}/indisponibilite.ics`,
    //   //   id: `/${employerId}/indisponibilite.ics`,
    //   //   format: 'ics',
    //   //   extraParams: {
    //   //     cachebuster: new Date().valueOf(),
    //   //     michel: 'michel',
    //   //   },
    //   // },
    // ];

    return eventSources;
  }, [loadEvent, params.agentsActives]);

  const footer = document.getElementById('aei-planning-search');

  return (
    <>
      <div style={{ display: 'block', width: '100%' }} className="planning">
        {loading || _loading ? <LinearProgress /> : <div style={{ height: '4px' }}></div>}
        <div style={{ height: 0, overflow: 'hidden' }}>
          <input
            type="date"
            ref={calendarPicker}
            style={{ visibility: 'hidden' }}
            onChange={(event) => calendar.current && calendar.current?.getApi().gotoDate(event.target.value)}
          />
        </div>
        <FullCalendar
          ref={calendar}
          allDaySlot={false}
          customButtons={customButtons}
          dateClick={dateClick}
          // dayHeaderContent={DayHeaderContent}
          dayHeaderFormat={dayHeaderFormat}
          // defaultTimedEventDuration="01:30"
          editable={!isAgent}
          eventClassNames={eventClassNames}
          eventClick={eventClick}
          eventContent={RenderEventContent}
          eventDrop={eventDropOrResize}
          eventOrder="title"
          eventResize={eventDropOrResize}
          eventSources={eventSources}
          firstDay={1}
          viewClassNames="michel"
          headerToolbar={mobileFormat ? mobileHeaderToolbar : headerToolbar}
          height="calc(100vh - 72px)"
          initialView={mobileFormat ? 'timeGridDay' : 'timeGridWeek'}
          // lazyFetching={false}
          loading={setLoading}
          locale="fr"
          locales={locales}
          nowIndicator
          plugins={plugins}
          // @ts-ignore
          schedulerLicenseKey={window.FULL_CALENDAR_LICENCE_KEY}
          select={createIntervention}
          selectAllow={(selectInfo) => selectInfo.start.getDay() === selectInfo.end.getDay()}
          selectMinDistance={10}
          selectMirror
          selectable={!isAgent}
          slotLabelFormat={slotLabelFormat}
          unselectAuto={false}
          views={views}
          weekNumbers={!mobileFormat}
          weekText="S"
        />
      </div>
      <Snackbar anchorOrigin={{ vertical: 'top', horizontal: 'center' }} autoHideDuration={6000} onClose={handleClose} open={open}>
        {success ? (
          <Alert
            small
            closable
            onClose={handleClose}
            severity="success"
            style={{ width: '100%' }}
            description="Votre intervention a bien été sauvegardée"
          />
        ) : (
          <Alert small closable onClose={handleClose} severity="error" style={{ width: '100%' }} description="Une erreur est survenue" />
        )}
      </Snackbar>
      {mobileFormat && (
        <Fab
          position="fixed"
          onClick={() => setContext({ mode: InterventionViewAndMutateMode.Create, retry })}
          color="primary"
          size="large"
          aria-label="ajouter une intervention"
          label="ajouter une intervention"
          left="calc(50% - 28px)"
          bottom={fabButtonSpacing}
        >
          <span className="fr-icon-add-line" aria-hidden="true"></span>
        </Fab>
      )}
      {footer &&
        ReactDOM.createPortal(
          <FormProvider {...methods}>
            <PlanningSideNav />
          </FormProvider>,
          footer
        )}
      {!isAgent &&
        createPortal(
          <div id="dropdownMenuPlanning" className="dropdown-menu-planning michel">
            <button
              onClick={() => {
                toggleIndisponibiliteForm();
                let dropdownMenu = document.getElementById('dropdownMenuPlanning');
                // @ts-ignore
                dropdownMenu.style.display = 'none';
              }}
            >
              Créer une indisponibilité
            </button>
          </div>,
          document.getElementById('body')!
        )}
      <IndisponibiliteForm onClose={toggleIndisponibiliteForm} open={displayIndisponibiliteForm} onSubmitSuccess={retry} />
    </>
  );
};

export default React.memo(PlanningPage);
