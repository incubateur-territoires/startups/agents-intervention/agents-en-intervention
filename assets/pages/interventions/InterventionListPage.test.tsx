import { fireEvent, render, screen } from '@testing-library/react';
import React from 'react';
import { useAsyncRetry } from 'react-use';

import { InterventionViewAndMutateMode, useInterventionViewAndMutate } from '@aei/src/components/InterventionViewAndMutateProvider';
import { useUser } from '@aei/src/utils/auth';
import { useMediumFormat, useMobileFormat } from '@aei/src/utils/useMobileFormat';

import { InterventionListPage, InterventionListPageProps } from './InterventionListPage';

jest.mock('@aei/src/utils/auth', () => ({
  useUser: jest.fn(),
}));

jest.mock('@aei/src/utils/useMobileFormat', () => ({
  useMediumFormat: jest.fn(),
  useMobileFormat: jest.fn(),
}));

jest.mock('@aei/src/components/InterventionViewAndMutateProvider', () => ({
  useInterventionViewAndMutate: jest.fn(),
  InterventionViewAndMutateMode: jest.requireActual('@aei/src/components/InterventionViewAndMutateProvider').InterventionViewAndMutateMode,
}));

const mockRetry = jest.fn();

jest.mock('react-use', () => ({
  ...jest.requireActual('react-use'),
  useAsyncRetry: jest.fn(() => ({ loading: false, retry: mockRetry, error: null })),
}));

const mockProps: InterventionListPageProps = {
  agents: { '1': 'Agent 1' },
  agentsActifs: [],
  authors: { '1': 'Author 1' },
  categories: { '1': 'Category 1' },
  statuses: { '1': 'Status 1' },
  isRecurrence: false,
};

describe('InterventionListPage', () => {
  beforeEach(() => {
    (useUser as jest.Mock).mockReturnValue({ user: { roles: [] }, hasRole: jest.fn() });
    (useMediumFormat as jest.Mock).mockReturnValue(false);
    (useMobileFormat as jest.Mock).mockReturnValue(false);
    (useInterventionViewAndMutate as jest.Mock).mockReturnValue({ setContext: jest.fn() });
    (useAsyncRetry as jest.Mock).mockReturnValue({ loading: false, retry: jest.fn(), error: null });
  });

  it('should render without crashing', () => {
    render(<InterventionListPage {...mockProps} />);
    expect(screen.getByText("Recherche d'intervention")).toBeInTheDocument();
  });

  it('should display an error alert when there is an error', () => {
    (useAsyncRetry as jest.Mock).mockReturnValue({ loading: false, retry: jest.fn(), error: { message: 'Error message' } });
    render(<InterventionListPage {...mockProps} />);
    expect(screen.getByText('Erreur de récupération des données')).toBeInTheDocument();
    expect(screen.getByText('Error message')).toBeInTheDocument();
  });

  it('should call setContext with correct mode when add button is clicked', () => {
    const setContextMock = jest.fn();
    (useInterventionViewAndMutate as jest.Mock).mockReturnValue({ setContext: setContextMock });
    render(<InterventionListPage {...mockProps} />);
    fireEvent.click(screen.getByLabelText('ajouter une intervention'));
    expect(setContextMock).toHaveBeenCalledWith({ mode: InterventionViewAndMutateMode.Create, retry: expect.any(Function) });
  });

  // Add more tests as needed
});
