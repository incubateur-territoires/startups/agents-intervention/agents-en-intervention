import Alert from '@codegouvfr/react-dsfr/Alert';
import Button from '@codegouvfr/react-dsfr/Button';
import { Input } from '@codegouvfr/react-dsfr/Input';
import queryString from 'query-string';
import React, { useEffect, useMemo, useRef } from 'react';
import ReactDOM from 'react-dom';
import { FormProvider, useForm } from 'react-hook-form';
import { useTranslation } from 'react-i18next';
import { useReactToPrint } from 'react-to-print';
import { useAsyncRetry, useSetState, useToggle } from 'react-use';

import { fetchApiEmployersEmployerIdinterventionsGetCollectionRaw } from '@aei/client/fetchers';
import FilterTag from '@aei/pages/interventions/FilterTag';
import { ApiEmployersEmployerIdinterventionsGetCollectionQueryParams } from '@aei/src/client/generated/components';
import { Fab } from '@aei/src/components';
import { DataTableSearchParams } from '@aei/src/components/DataTable/DataTableConfiguration';
import { SortDirection } from '@aei/src/components/DataTable/SortLink';
import ExportDialog from '@aei/src/components/ExportDialog';
import { InterventionViewAndMutateMode, useInterventionViewAndMutate } from '@aei/src/components/InterventionViewAndMutateProvider';
import { InterventionList, InterventionsDataTableConfiguration } from '@aei/src/components/InterventionViewer/InterventionListDSFR';
import { useUser } from '@aei/src/utils/auth';
import { fabButtonSpacing } from '@aei/src/utils/map';
import useCtrlPListener from '@aei/src/utils/useCtrlPListener';
import useEmployerId from '@aei/src/utils/useEmployerId';
import { useMediumFormat, useMobileFormat } from '@aei/src/utils/useMobileFormat';
import { InterventionInterventionview, Priority, Role, UserUserview } from '@aei/types';

import BasicPopover from './Filter';

export interface InterventionSearch {
  sort_by: string;
  order: SortDirection;
  agents: string[];
  categories: string[];
  nonAssignatedOnly: boolean;
  priorities: string[];
  status: string[];
  authors: string[];
  term: string;
}

export interface InterventionListPageProps {
  agents: Record<string, string>;
  agentsActifs: UserUserview[];
  authors: Record<string, string>;
  categories: Record<string, string>;
  isRecurrence?: boolean;
  statuses: Record<string, string>;
}

export function InterventionListPage({
  agents,
  agentsActifs,
  authors,
  categories,
  statuses,
  isRecurrence = false,
}: Readonly<InterventionListPageProps>) {
  const breakpoint = useMediumFormat();
  const { setContext } = useInterventionViewAndMutate();
  const { user, hasRole } = useUser();
  const mobile = useMobileFormat();
  const [showExportDialog, toggleShowExportDialog] = useToggle(false);
  const searchParams = useMemo(
    () =>
      queryString.parse(window.location.search, {
        arrayFormat: 'index',
        parseBooleans: true,
      }),
    [window.location.search]
  );
  const contentRef = useRef<HTMLDivElement>(null);
  const reactToPrintFn = useReactToPrint({
    contentRef,
    nonce: document.querySelector('meta[name="csp-nonce"]')?.getAttribute('content') ?? '',
  });
  useCtrlPListener(reactToPrintFn);

  const { t } = useTranslation('common');
  const employerId = useEmployerId();
  const [{ rowCount, interventions: _interventions }, setState] = useSetState<{
    rowCount: number;
    interventions: InterventionInterventionview[];
  }>({
    rowCount: 0,
    interventions: [],
  });

  const priorities = useMemo<Record<Priority, string>>(
    () => ({
      [Priority.Normal]: t('model.intervention.priority.enum.normal'),
      [Priority.Urgent]: t('model.intervention.priority.enum.urgent'),
    }),
    [t]
  );

  const methods = useForm<InterventionSearch>({
    defaultValues: {
      sort_by: (searchParams['sort_by'] as string) || 'logicId',
      order: (searchParams['order'] as SortDirection) || 'desc',
      agents: (searchParams['agents'] as []) || [],
      categories: (searchParams['categories'] as []) || [],
      nonAssignatedOnly: searchParams['not-assignated'] === '1' || false,
      priorities: (searchParams['priorities'] as []) || [],
      status: (searchParams['status'] as []) || [],
      authors: (searchParams['authors'] as []) || [],
      term: (searchParams['term'] as string) || '',
    },
    context: { values: {} },
  });
  const values = methods.watch();

  useEffect(() => {
    const handlePopState = () => {
      const searchParams = queryString.parse(window.location.search, {
        arrayFormat: 'index',
        parseBooleans: true,
      });
      methods.setValue('sort_by', (searchParams['sort_by'] as string) || 'logicId');
      methods.setValue('order', (searchParams['order'] as SortDirection) || 'desc');
    };

    window.addEventListener('popstate', handlePopState);
    return () => {
      window.removeEventListener('popstate', handlePopState);
    };
  }, []);

  const [paginationModel, setPaginationModel] = useSetState<{ pageSize: number; page: number }>({
    pageSize: 20,
    page: 0,
  });

  useEffect(() => {
    setPaginationModel({ page: 0 });
  }, [mobile]);

  const listSearchParams = useMemo<DataTableSearchParams<InterventionsDataTableConfiguration>>(
    () => ({
      // @ts-ignore
      sort_by: values.sort_by || 'logicId',
      order: (values.order as SortDirection) || 'desc',
    }),
    [values.sort_by, values.order]
  );

  const queryParams = useMemo<ApiEmployersEmployerIdinterventionsGetCollectionQueryParams>(() => {
    const ret: any = {
      page: paginationModel.page + 1,
      itemsPerPage: paginationModel.pageSize,
      hidegeneratedafter: 1,
      recurring: isRecurrence,
      pagination: 'true',
    };

    if (values.sort_by && values.order) {
      ret[`order[${values.sort_by}]`] = values.order;
    } else {
      ret['order[logicId]'] = 'desc';
    }

    const urlParams: any = {
      page: ret.page,
      sort_by: values.sort_by || 'logicId',
      order: values.order || 'desc',
    };

    if (values.term) {
      ret['term'] = values.term;
      urlParams['term'] = values.term;
    }

    if (!isRecurrence) {
      values.status?.map((status, index) => {
        ret[`status[${index}]`] = status;
        urlParams[`status[${index}]`] = status;
      });
    }
    values.agents?.map((agent, index) => {
      ret[`participants[${index + 1}]`] = agent;
      urlParams[`participants[${index + 1}]`] = agent;
    });
    values.categories?.map((category, index) => {
      ret[`type.category[${index}]`] = `api/categories/${category}`;
      urlParams[`type.category[${index}]`] = `api/categories/${category}`;
    });
    values.priorities?.map((priority, index) => {
      ret[`priority[${index}]`] = priority;
      urlParams[`priority[${index}]`] = priority;
    });
    values.authors?.map((author, index) => {
      ret[`author[${index}]`] = `api/users/${author}`;
      urlParams[`author[${index}]`] = `api/users/${author}`;
    });

    if (values.nonAssignatedOnly) {
      ret['participants[0]'] = 'null';
      urlParams['not-assignated'] = '1';
    }

    window.history.pushState(urlParams, '', `?${queryString.stringify(urlParams)}`);
    let queryStringChange = new PopStateEvent('popstate', { state: urlParams });
    window.dispatchEvent(queryStringChange);

    return ret;
  }, [
    values.sort_by,
    values.order,
    values.term,
    values.agents,
    values.authors,
    values.categories,
    values.nonAssignatedOnly,
    values.priorities,
    values.status,
    paginationModel.page,
    paginationModel.pageSize,
  ]);

  const { loading, retry, error } = useAsyncRetry(
    () =>
      fetchApiEmployersEmployerIdinterventionsGetCollectionRaw({
        pathParams: {
          employerId: `${employerId}`,
        },
        queryParams,
      }).then((value) => {
        if (!value) return;
        if (mobile) {
          const newIds = value['hydra:member']?.map((intervention: any) => intervention.id);
          setState({
            rowCount: parseInt(value['hydra:totalItems']),
            interventions:
              [..._interventions.filter((existing) => !newIds?.includes(existing.id)), ...(value['hydra:member'] || [])].sort(
                (a: any, b: any) => b.logicId - a.logicId
              ) || [],
          });
        } else {
          setState({ rowCount: parseInt(value['hydra:totalItems']), interventions: value['hydra:member'] || [] });
        }
      }),
    [queryParams, mobile]
  );

  const addButton = useMemo(() => {
    const el = document.getElementById('intervention-add-button');
    if (!breakpoint && el) {
      return ReactDOM.createPortal(
        <Button
          priority="secondary"
          onClick={() =>
            setContext({
              mode: InterventionViewAndMutateMode.Create,
              retry,
            })
          }
        >
          Créer une intervention
        </Button>,
        el
      );
    } else {
      return (
        <Fab
          position="fixed"
          onClick={() => setContext({ mode: InterventionViewAndMutateMode.Create, retry })}
          color="primary"
          size="large"
          aria-label="ajouter une intervention"
          label="ajouter une intervention"
          left="calc(50% - 28px)"
          bottom={fabButtonSpacing}
        >
          <span className="fr-icon-add-line" aria-hidden="true"></span>
        </Fab>
      );
    }
  }, [breakpoint, retry, setContext]);

  return (
    <>
      <div className="fr-mt-2w">
        {error && (
          <div className="fr-mb-2w">
            <Alert severity="error" title="Erreur de récupération des données" description={error.message} />
          </div>
        )}

        <div
          className="aei-stack aei-stack-small-gap fr-no-print"
          style={{
            backgroundColor: 'var(--aei-background-white)',
            borderRadius: '8px',
            border: '1px solid var(--aei-border-card-color)',
            padding: '24px 16px 24px 16px',
          }}
        >
          <FormProvider {...methods}>
            <div className="aei-stack aei-stack-small-gap aei-stack-columns" style={{ alignItems: 'flex-end' }}>
              <form style={{ flexGrow: '1' }}>
                <Input
                  hideLabel
                  label="Recherche d'intervention"
                  nativeInputProps={{
                    ...methods.register('term'),
                    'aria-label': 'recherche intervention',
                    placeholder: 'Rechercher une adresse, un identifiant, un agent',
                  }}
                  addon={<Button type="button" iconId="fr-icon-search-line" onClick={function noRefCheck() {}} title="Rechercher" />}
                />
              </form>
              <Button className="fr-hidden fr-unhidden-sm" iconId="fr-icon-download-line" onClick={() => toggleShowExportDialog()}>
                Exporter
              </Button>
              <Button priority="secondary" title="Imprimer" iconId={'fr-icon-printer-line'} onClick={() => reactToPrintFn()} />
            </div>

            <div className="aei-stack aei-stack-small-gap aei-stack-columns">
              {!isRecurrence && (
                <div style={{ marginTop: '0.3rem' }}>
                  <BasicPopover key="filter_status" name="status" label="Statut" entries={statuses} />
                </div>
              )}
              <div style={{ marginTop: '0.3rem' }}>
                <BasicPopover key="filter_categories" name="categories" label="Catégorie" entries={categories} />
              </div>
              <div style={{ marginTop: '0.3rem' }}>
                <BasicPopover key="filter_priorities" name="priorities" label="Priorité" entries={priorities} />
              </div>
              <div style={{ marginTop: '0.3rem' }}>
                <BasicPopover key="filter_agents" name="agents" label="Assignée à" entries={agents} />
              </div>
              {!hasRole(Role.Agent) && (
                <div style={{ marginTop: '0.3rem' }}>
                  <FilterTag
                    activated={values.nonAssignatedOnly}
                    label="Non assignées"
                    onClick={() => methods.setValue('nonAssignatedOnly', !values.nonAssignatedOnly)}
                  />
                </div>
              )}
              <div style={{ marginTop: '0.3rem' }}>
                <BasicPopover key="filter_authors" name="authors" label="Créé par" entries={authors} />
              </div>
            </div>
          </FormProvider>
        </div>
        {(_interventions && _interventions?.length > 0) || loading ? (
          <InterventionList
            assignableAgents={agentsActifs}
            canMutate={user?.roles.includes(Role.Director) || user?.roles.includes(Role.AdminEmployer)}
            interventions={_interventions}
            loading={loading}
            refetch={retry}
            paginationModel={paginationModel}
            setPaginationModel={setPaginationModel}
            ref={contentRef}
            rowCount={rowCount}
            searchParams={listSearchParams}
            sortable
            useHistoryApi
          />
        ) : (
          <div
            className="fr-mt-2w"
            style={{
              padding: '3rem 6rem',
              backgroundColor: 'white',
              border: '1px solid #ddd',
              display: 'flex',
              flexDirection: 'column',
              textAlign: 'center',
              alignItems: 'center',
              justifyContent: 'center',
            }}
          >
            <span className="fr-icon-file-line"></span>
            <p className="fr-pt-2w">
              <b>Aucune intervention n'a encore été créée</b>
            </p>

            <p>
              Pour créer une intervention,
              <br />
              <a
                href="#"
                style={{ textDecoration: 'underline' }}
                onClick={(e) => {
                  e.preventDefault();
                  setContext({
                    mode: InterventionViewAndMutateMode.Create,
                    retry,
                  });
                }}
              >
                cliquez ici
              </a>
              .
            </p>
          </div>
        )}
        {addButton}
      </div>
      {showExportDialog && <ExportDialog onClose={toggleShowExportDialog} queryParams={queryParams} />}
    </>
  );
}
