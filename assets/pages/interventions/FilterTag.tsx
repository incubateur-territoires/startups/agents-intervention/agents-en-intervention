import Tag from '@codegouvfr/react-dsfr/Tag';
import React, { Ref, forwardRef } from 'react';

interface FilterTagProps {
  activated: boolean;
  children?: React.ReactNode;
  disabled?: boolean;
  label: string;
  onClick: (e: React.MouseEvent<HTMLElement>) => void;
}

const FilterTag = forwardRef<HTMLButtonElement, FilterTagProps>(({ activated, children, disabled = false, label, onClick }, ref) => {
  const _onClick: React.MouseEventHandler<HTMLElement> = (e) => {
    e.preventDefault();
    e.stopPropagation();
    return onClick(e);
  };
  return (
    <Tag
      ref={ref}
      nativeButtonProps={{ className: `fr-tag--aria-pressed-${activated ? 'true' : 'false'}`, onClick: disabled ? undefined : _onClick }}
      // pressed={activated}
    >
      {label}
    </Tag>
  );
});

export default FilterTag;
