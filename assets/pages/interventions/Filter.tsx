import Checkbox from '@codegouvfr/react-dsfr/Checkbox';
import * as React from 'react';
import { useFormContext } from 'react-hook-form';

import FilterTag from '@aei/pages/interventions/FilterTag';
import DropdownMenu from '@aei/src/components/DropdownMenu/DropdownMenu';

import { InterventionSearch } from './InterventionListPage';

interface BasicPopover {
  name: keyof InterventionSearch;
  label: string;
  entries: Record<string, string>;
}

export default function BasicPopover({ name, label, entries }: BasicPopover) {
  const [anchorEl, setAnchorEl] = React.useState<HTMLElement | null>(null);
  const methods = useFormContext<InterventionSearch>();
  const rawValue = methods.watch(name);

  const value = React.useMemo<string[]>(() => {
    if (typeof rawValue === 'boolean') {
      return [];
    }
    return Array.isArray(rawValue) ? rawValue : [rawValue];
  }, [rawValue]);

  const handleClick = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const open = Boolean(anchorEl);
  const id = open ? 'simple-popover' : undefined;

  return (
    <DropdownMenu
      anchorOrigin={{ vertical: 'bottom', horizontal: 'center' }}
      button={
        <FilterTag
          disabled={Object.keys(entries).length === 0}
          label={label}
          aria-describedby={id}
          activated={value.length > 0}
          onClick={handleClick}
        />
      }
      dropdownClasses="aei-filter-popover"
      keepOpen
      noNotch
    >
      <div className="fr-p-1w">
        <Checkbox
          legend={`${label} :`}
          options={Object.keys(entries).map((key) => ({
            key: key,
            label: entries[key],
            nativeInputProps: {
              ...methods.register(name),
              value: key,
            },
          }))}
          state="default"
        />
      </div>
    </DropdownMenu>
  );
}
