import Tag from '@codegouvfr/react-dsfr/Tag';
import { MouseEventHandler, useState } from 'react';
import { createReducerContext, useEffectOnce } from 'react-use';

import { TeamList } from '@aei/src/components';
import MyDrawerDSFR from '@aei/src/components/MyDrawerDSFR';
import UserForm from '@aei/src/components/TeamList/UserForm';
import { LoadingArea } from '@aei/src/ui/LoadingArea';
import { useUser } from '@aei/src/utils/auth';
import { UserUserview } from '@aei/types';
import { Role } from '@aei/types/Role';

type Action = {
  type: 'openForm' | 'closeForm' | 'setUsers' | 'removeUser';
  payload?: any;
};

type State = {
  formOpened: boolean;
  loading: boolean;
  userToModify: UserUserview | null;
  users: UserUserview[];
};

function reducer(state: State, action: Action): State {
  switch (action.type) {
    case 'closeForm':
      const index = action.payload?.user ? state.users.findIndex((user) => action.payload.user.id === user.id) : -1;
      const _users = state.users;

      if (index !== -1) {
        _users[index] = action.payload.user;
      } else if (action.payload?.user) {
        _users.push(action.payload.user);
      }

      return {
        ...state,
        formOpened: false,
        userToModify: null,
        users: _users,
      };
    case 'openForm':
      return { ...state, formOpened: true, userToModify: action.payload.user };
    case 'setUsers':
      return { ...state, users: action.payload.users, loading: state.loading ? false : state.loading };
    case 'removeUser':
      return { ...state, users: state.users.filter((user) => action.payload.user.id !== user.id) };
    default:
      throw new Error('Unknown action in TeamList reducer');
  }
}

export const [useTeamList, TeamListProvider] = createReducerContext(reducer, { formOpened: false, loading: true, userToModify: null, users: [] });

const TeamListPageInner = (props: TeamListPageProps) => {
  const [{ formOpened, loading, users, userToModify }, dispatch] = useTeamList();

  const { user } = useUser();
  const [filter, setFilter] = useState<Role | null>(null);

  useEffectOnce(() => {
    dispatch({ type: 'setUsers', payload: { ...props, loading: false } });
  });

  if (!user || loading) return <LoadingArea ariaLabelTarget="page" />;

  const handleClick =
    (type: Role | null): MouseEventHandler<Element> =>
    () => {
      if (type === filter) {
        setFilter(null);
      } else {
        setFilter(type);
      }
    };

  const _users = users.filter((user) => filter === null || user.roles?.at(0) === filter);

  return (
    <div style={{ width: '100%', padding: '2rem' }}>
      <div style={{ paddingBottom: '2rem' }}>
        <div className="aei-stack aei-stack-columns aei-stack-small-gap">
          <Tag
            nativeButtonProps={{
              onClick: handleClick(Role.AdminEmployer),
            }}
            pressed={Role.AdminEmployer === filter}
          >
            Administrateur
          </Tag>
          <Tag
            nativeButtonProps={{
              onClick: handleClick(Role.Director),
            }}
            pressed={Role.Director === filter}
          >
            Coordinateur
          </Tag>
          <Tag
            nativeButtonProps={{
              onClick: handleClick(Role.Agent),
            }}
            pressed={Role.Agent === filter}
          >
            Agent
          </Tag>
          <Tag
            nativeButtonProps={{
              onClick: handleClick(Role.Elected),
            }}
            pressed={Role.Elected === filter}
          >
            Demandeur
          </Tag>
        </div>
      </div>

      {_users ? (
        <TeamList
          users={_users}
          modify={(user) => {
            dispatch({ type: 'openForm', payload: { user } });
          }}
        />
      ) : (
        <p>Pas d&apos;utilisateur trouvé</p>
      )}
      <MyDrawerDSFR
        open={formOpened}
        onClose={() => dispatch({ type: 'closeForm' })}
        title={userToModify ? 'Modifier les informations du collaborateur' : 'Inviter un nouveau collaborateur'}
      >
        <UserForm />
      </MyDrawerDSFR>
    </div>
  );
};

export interface TeamListPageProps {
  users: Array<UserUserview>;
}

export const TeamListPage = ({ users }: TeamListPageProps) => (
  <TeamListProvider>
    <TeamListPageInner users={users} />
  </TeamListProvider>
);
