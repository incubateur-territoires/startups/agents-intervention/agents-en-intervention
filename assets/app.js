import './bootstrap.js';
import '@codegouvfr/react-dsfr/dsfr/dsfr.module';
/*
 * Welcome to your app's main JavaScript file!
 *
 * This file will be included onto the page via the importmap() Twig function,
 * which should already be in your base.html.twig.
 */
import './styles/app.scss';
import './styles/public.scss';
import './styles/stats.scss';
import './styles/avatar.scss';

console.log('This log comes from assets/app.js - welcome to AssetMapper! 🎉');
