import { Application } from '@hotwired/stimulus';
import { clearDOM, mountDOM } from '@symfony/stimulus-testing';

import PasswordValidationController from '../../controllers/password_validation_controller';

describe('PasswordValidationController', () => {
  let container: HTMLElement | null;

  beforeAll(() => {
    const application = Application.start();
    application.register('password-validation', PasswordValidationController);
  });

  beforeEach(() => {
    container = mountDOM(
      '<div data-controller="password-validation"><input data-password-validation-target="input" data-action="input->password-validation#validate" name="password" type="password" /></div>'
    );
  });

  afterEach(() => {
    clearDOM();
    container = null;
  });

  it('connects', () => {
    if (!container) {
      fail('Container is null');
    }

    const criteriaElements = container.querySelectorAll('[data-password-validation-target="criteria"]');
    expect(criteriaElements.length).toBe(5);
  });

  it('validates password correctly', () => {
    if (!container) {
      fail('Container is null');
    }

    const input: HTMLInputElement | null = container.querySelector('[data-password-validation-target="input"]');
    if (input) {
      input.value = 'Password1!';
      input.dispatchEvent(new Event('input'));
    }

    const criteriaElements = container.querySelectorAll('[data-password-validation-target="criteria"]');
    expect(criteriaElements[0].classList.contains('fr-valid-text')).toBe(true); // 10 caractères minimum
    expect(criteriaElements[1].classList.contains('fr-valid-text')).toBe(true); // 1 caractère spécial minimum
    expect(criteriaElements[2].classList.contains('fr-valid-text')).toBe(true); // 1 chiffre minimum
    expect(criteriaElements[3].classList.contains('fr-valid-text')).toBe(true); // 1 lettre minuscule minimum
    expect(criteriaElements[4].classList.contains('fr-valid-text')).toBe(true); // 1 lettre majuscule minimum
  });

  it('validates password wrongly', () => {
    if (!container) {
      fail('Container is null');
    }

    const input: HTMLInputElement | null = container.querySelector('[data-password-validation-target="input"]');
    if (input) {
      input.value = 'Pass';
      input.dispatchEvent(new Event('input'));
    }

    const criteriaElements = container.querySelectorAll('[data-password-validation-target="criteria"]');
    expect(criteriaElements[0].classList.contains('fr-valid-text')).toBe(false); // 10 caractères minimum
    expect(criteriaElements[1].classList.contains('fr-valid-text')).toBe(false); // 1 caractère spécial minimum
    expect(criteriaElements[2].classList.contains('fr-valid-text')).toBe(false); // 1 chiffre minimum
    expect(criteriaElements[3].classList.contains('fr-valid-text')).toBe(true); // 1 lettre minuscule minimum
    expect(criteriaElements[4].classList.contains('fr-valid-text')).toBe(true); // 1 lettre majuscule minimum
  });
});
