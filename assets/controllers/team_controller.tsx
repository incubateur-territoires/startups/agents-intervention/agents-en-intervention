import React, { ReactNode } from 'react';

import ApplicationController from '@aei/controllers/ApplicationController';
import { User } from '@aei/types/User';

import { TeamListPage } from '../pages/team/TeamListPage';

/*
 * The following line makes this controller "lazy": it won't be downloaded until needed
 * See https://github.com/symfony/stimulus-bridge#lazy-controllers
 */
/* stimulusFetch: 'lazy' */
export default class extends ApplicationController {
  declare readonly usersValue: Array<User>;
  static values = {
    ...ApplicationController.values,
    users: Array,
  };

  connect() {
    super.connect();
    console.log(this.usersValue);
  }

  render(): ReactNode {
    return <TeamListPage users={this.usersValue} />;
  }
}
