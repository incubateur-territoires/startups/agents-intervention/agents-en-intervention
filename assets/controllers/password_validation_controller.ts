import { Controller } from '@hotwired/stimulus';

// Connects to data-controller="password-validation"
/* stimulusFetch: 'lazy' */
export default class extends Controller {
  declare readonly inputTarget: HTMLInputElement;
  static readonly targets = ['input'];

  connect(): void {
    this.injectCriteria();
    this.updateCriteria();
  }

  validate(): void {
    this.updateCriteria();
  }

  injectCriteria(): void {
    const criteriaHtml = `
      <p class="fr-error-text" data-password-validation-target="criteria">10 caractères minimum</p>
      <p class="fr-error-text" data-password-validation-target="criteria">1 caractère spécial minimum</li>
      <p class="fr-error-text" data-password-validation-target="criteria">1 chiffre minimum</p>
      <p class="fr-error-text" data-password-validation-target="criteria">1 lettre minuscule minimum</p>
      <p class="fr-error-text" data-password-validation-target="criteria">1 lettre majuscule minimum</p>
    `;

    this.element.insertAdjacentHTML('beforeend', criteriaHtml);
  }

  updateCriteria(): void {
    const password: string = this.inputTarget.value;
    const required: boolean = this.inputTarget.required;
    const criteriaElements: NodeListOf<HTMLLIElement> = this.element.querySelectorAll("[data-password-validation-target='criteria']");

    if (!required && password === '') {
      criteriaElements.forEach((criteriaElement, index) => {
        criteriaElement.classList.remove('fr-error-text', 'fr-valid-text');
        criteriaElement.classList.add('fr-info-text');
      });
      this.inputTarget.setCustomValidity('');
      return;
    }

    this.inputTarget.setCustomValidity('');

    const requirements: { regex: RegExp; message: string }[] = [
      {
        regex: /.{10,}/,
        message: '10 caractères minimum',
      },
      { regex: /[^0-9a-zA-Z]/, message: '1 caractère spécial minimum' },
      {
        regex: /\d/,
        message: '1 chiffre minimum',
      },
      { regex: /[a-z]/, message: '1 lettre minuscule minimum' },
      {
        regex: /[A-Z]/,
        message: '1 lettre majuscule minimum',
      },
    ];

    criteriaElements.forEach((criteriaElement, index) => {
      const requirement = requirements[index];
      const isValid = requirement.regex.test(password);
      criteriaElement.classList.remove('fr-error-text', 'fr-valid-text', 'fr-info-text');
      criteriaElement.classList.add(isValid ? 'fr-valid-text' : 'fr-error-text');
      if (!isValid) {
        this.inputTarget.setCustomValidity('Veuillez respecter les critères de mot de passe');
      }
    });
  }
}
