import ApplicationController from '@aei/controllers/ApplicationController';
import { SortableColumn } from '@aei/src/components/DataTable/DataTableConfiguration';
import { SortDirection } from '@aei/src/components/DataTable/SortLink';
import { InterventionList, InterventionsDataTableConfiguration } from '@aei/src/components/InterventionViewer/InterventionListDSFR';
import { UserUserview } from '@aei/types';
import { InterventionInterventionview } from '@aei/types/InterventionInterventionview';

/*
 * The following line makes this controller "lazy": it won't be downloaded until needed
 * See https://github.com/symfony/stimulus-bridge#lazy-controllers
 */
/* stimulusFetch: 'lazy' */
export default class extends ApplicationController {
  declare readonly agentsValue: Array<UserUserview>;
  declare readonly interventionsValue: Array<InterventionInterventionview>;
  declare readonly orderValue: SortDirection;
  declare readonly sortByValue: SortableColumn<InterventionsDataTableConfiguration>['name'];

  static values = {
    agents: Array<Object>,
    interventions: Array<Object>,
    order: String,
    sortBy: String,
  };

  private loading: boolean = false;
  private nextUrl: string = '';

  connect() {
    super.connect();

    // Capture le clic sur les liens pour connaître l'URL de destination
    document.addEventListener('click', (event) => {
      if (!event) return;
      // @ts-ignore
      const target: any = event.target?.closest('a'); // Gère le cas où le clic est sur un enfant de `<a>`
      if (target && target.href) {
        this.nextUrl = target.href;
      }
    });

    window.addEventListener('beforeunload', (event) => {
      console.log(event, this.nextUrl);
      if (this.nextUrl && this.nextUrl.includes('/dashboard?')) {
        console.log('La page est en cours de navigation vers une nouvelle URL');
        this.loading = true;
        this.renderApplication();
      }
    });
  }

  render() {
    return (
      <InterventionList
        assignableAgents={this.agentsValue}
        canMutate={true}
        cols={['logicId', 'title', 'location', 'author', 'createdAt']}
        forceEditOnClick
        interventions={this.interventionsValue}
        loading={this.loading}
        refetch={() => {
          window.location.reload();
        }}
        searchParams={{ order: this.orderValue, sort_by: this.sortByValue || 'logicId' }}
        sortable
      />
    );
  }
}
