import ApplicationController from '@aei/controllers/ApplicationController';
import { InterventionListPage } from '@aei/pages/interventions/InterventionListPage';
import { Category, UserUserview } from '@aei/types';

/*
 * The following line makes this controller "lazy": it won't be downloaded until needed
 * See https://github.com/symfony/stimulus-bridge#lazy-controllers
 */
/* stimulusFetch: 'lazy' */
export default class extends ApplicationController {
  declare readonly agentsValue: UserUserview[];
  declare readonly agentsActifsValue: UserUserview[];
  declare readonly categoriesValue: Category[];
  declare readonly recurrenceValue: boolean;
  declare readonly statusesValue: Record<string, string>;

  static values = {
    agents: Array<Object>,
    agentsActifs: Array<Object>,
    categories: Array<Object>,
    recurrence: Boolean,
    statuses: Object,
  };

  connect() {
    super.connect();
  }

  render() {
    const agents: Record<string, string> = {};
    this.agentsValue.map((it) => {
      agents[it.id] = `${it.firstname} ${it.lastname}`;
    });

    const categories: Record<string, string> = {};
    this.categoriesValue.map((it) => {
      categories[it.id] = it.name;
    });

    return (
      <InterventionListPage
        agents={agents}
        agentsActifs={this.agentsActifsValue}
        authors={agents}
        categories={categories}
        isRecurrence={this.recurrenceValue}
        statuses={this.statusesValue}
      />
    );
  }
}
