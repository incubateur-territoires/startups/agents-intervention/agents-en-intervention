import { Controller } from '@hotwired/stimulus';

export default class extends Controller {
  private menuTarget: HTMLElement | null = null;
  // static targets = ["menu"];

  overlay: HTMLDivElement | null = null;

  connect() {}

  toggle() {
    if (this.menuTarget?.style.display === 'block') {
      this.closeMenu();
    } else {
      this.openMenu();
    }
  }

  openMenu() {
    this.menuTarget = document.getElementById('dropdownMenu') as HTMLElement;
    this.menuTarget.getElementsByTagName('button')[0]?.addEventListener('click', (e) => {
      e.stopPropagation();
      this.closeMenu();
    });

    this.overlay = document.createElement('div');
    this.overlay.classList.add('sidenav-overlay');
    this.overlay.style.display = 'block';
    this.overlay.style.zIndex = '1001';
    this.overlay.addEventListener('click', () => {
      this.closeMenu();
    });
    document.body.appendChild(this.overlay);

    /*
     * On sort l'élément du flux pour le mettre en bas du body pour qu'il reste bien au-dessus des autres éléments.
     */
    document.body.appendChild(this.menuTarget);

    const rect = this.element.getBoundingClientRect();
    if (this.menuTarget) {
      this.menuTarget.style.top = `${rect.bottom + window.scrollY}px`;
      this.menuTarget.style.right = `${window.innerWidth - rect.right + window.scrollX}px`;
      this.menuTarget.style.display = 'block';
    }
  }

  closeMenu() {
    if (this.menuTarget) {
      this.menuTarget.style.display = 'none';
    }
    if (this.overlay) {
      document.body.removeChild(this.overlay);
    }
  }
}
