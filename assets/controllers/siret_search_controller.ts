import { Controller } from '@hotwired/stimulus';
import axios from 'axios';

interface SiretResponse {
  nom_raison_sociale: string;
  siege: {
    adresse: string;
    geo_adresse: string;
    libelle_voie: string;
    code_postal: string;
    libelle_commune: string;
    latitude: string;
    longitude: string;
  };
  siren: string;
}

/* stimulusFetch: 'lazy' */
export default class extends Controller {
  static readonly targets = ['results', 'latitude', 'longitude', 'siren', 'submit', 'name'];

  resultsTarget!: HTMLElement;
  latitudeTarget!: HTMLInputElement;
  longitudeTarget!: HTMLInputElement;
  sirenTarget!: HTMLInputElement;
  nameTarget!: HTMLInputElement;
  submitTarget!: HTMLButtonElement;

  connect() {
    super.connect();
    this.submitTarget.disabled = true;
  }

  search(event: Event) {
    const inputElement = event.target as HTMLInputElement;
    const siret = inputElement.value;

    // Vérifiez si le SIRET contient exactement 14 caractères
    if (siret.length === 14) {
      this.fetchSiretDetails(siret);
    } else {
      // Réinitialiser les résultats si l'utilisateur supprime du texte
      this.clearResults();
    }
  }

  sanitizePaste(event: ClipboardEvent) {
    event.preventDefault();

    const clipboardData = event.clipboardData || (window as any).clipboardData;
    const pastedData = clipboardData.getData('text');
    const sanitizedData = pastedData.replace(/[^0-9]/g, '');

    const inputElement = event.target as HTMLInputElement;
    inputElement.value = sanitizedData;

    // Déclencher l'événement input manuellement
    let inputEvent = new Event('input', { bubbles: true });
    inputElement.dispatchEvent(inputEvent);
  }

  async fetchSiretDetails(siret: string) {
    try {
      const response = await axios.get<{
        results: SiretResponse[];
      }>(`https://recherche-entreprises.api.gouv.fr/search?q=${siret}&page=1&per_page=10&est_collectivite_territoriale=true`);

      if (response.data && response.data.results && response.data.results.length > 0) {
        this.displayResults(response.data.results);
      } else {
        this.displayNoResults();
      }
    } catch (error) {
      console.error('Erreur lors de la recherche SIRET:', error);
      this.displayError();
    }
  }

  clearResults() {
    this.resultsTarget.innerHTML = '';
    this.submitTarget.disabled = true;
  }

  displayResults(data: SiretResponse[]) {
    // Exemple d'affichage du résultat
    this.resultsTarget.innerHTML = data
      .map(
        (it, index) => `
      <div class="result-item ${index === 0 ? 'result-item--selected' : ''}"
          data-siret-search-id-param="${index}"
           data-siret-search-name-param="${it.nom_raison_sociale}"
           data-siret-search-siren-param="${it.siren}"
           data-siret-search-latitude-param="${it.siege.latitude ?? ''}"
           data-siret-search-longitude-param="${it.siege.longitude ?? ''}"
           data-action="click->siret-search#selectSearchItem">
        <strong>${it.nom_raison_sociale}</strong>
        ${it.siege.geo_adresse ? it.siege.geo_adresse : it.siege.adresse}
      </div>
    `
      )
      .join('\n');

    if (data.length > 0) {
      this.fillHiddenFields(data[0].nom_raison_sociale, data[0].siren, data[0].siege.latitude, data[0].siege.longitude);
    }
  }

  selectSearchItem(event: PointerEvent) {
    const elements = this.element.querySelectorAll('.result-item');

    elements.forEach((it) => {
      it.classList.remove('result-item--selected');
    });

    // @ts-ignore
    const params = event.params;
    elements[params.id].classList.add('result-item--selected');
    this.fillHiddenFields(params.name, params.siren, params.latitude, params.longitude);
  }

  fillHiddenFields(name: string, siret: string, latitude: string, longitude: string) {
    this.nameTarget.value = name;
    this.sirenTarget.value = siret;
    this.latitudeTarget.value = latitude;
    this.longitudeTarget.value = longitude;
    this.submitTarget.disabled = false;
  }

  displayNoResults() {
    this.resultsTarget.innerHTML = `<div class="result-item">Aucun résultat trouvé.</div>`;
    this.submitTarget.disabled = true;
  }

  displayError() {
    this.resultsTarget.innerHTML = `<div class="result-item">Erreur lors de la recherche.</div>`;
    this.submitTarget.disabled = true;
  }
}
