import { Controller } from '@hotwired/stimulus';
import { ReactNode } from 'react';
import { Root, createRoot } from 'react-dom/client';

import Application from '@aei/pages/Application';

class ApplicationController<ElementType extends Element = Element> extends Controller<ElementType> {
  protected root?: Root;

  connect() {
    super.connect();
    this.root = createRoot(this.element);
    this.renderApplication();
  }

  renderApplication() {
    this.root?.render(
      <Application
      // agentsActifs={this.agentsActifsValue}
      // categories={this.categoriesValue}
      // employer={this.employerValue}
      // types={this.typesValue}
      >
        {this.render()}
      </Application>
    );
  }

  render(): ReactNode {
    return <></>;
  }
}

export default ApplicationController;
