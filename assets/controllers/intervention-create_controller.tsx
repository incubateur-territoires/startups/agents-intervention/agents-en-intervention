import React from 'react';

import ApplicationController from '@aei/controllers/ApplicationController';
import NewInterventionButton from '@aei/src/components/NewInterventionButton';

export default class extends ApplicationController {
  render() {
    return <NewInterventionButton />;
  }
}
