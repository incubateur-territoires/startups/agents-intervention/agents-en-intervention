import { Controller } from '@hotwired/stimulus';

/*
 * The following line makes this controller "lazy": it won't be downloaded until needed
 * See https://github.com/symfony/stimulus-bridge#lazy-controllers
 */
/* stimulusFetch: 'eager' */
export default class extends Controller {
  declare navTarget: HTMLDivElement;
  declare navButtonTarget: HTMLButtonElement;
  static targets = ['nav', 'navButton'];

  overlay: HTMLDivElement | null = null;

  // Méthode appelée lorsque le controller est connecté au DOM
  connect() {
    this.overlay = document.createElement('div');
    this.overlay.classList.add('sidenav-overlay');
    this.overlay.addEventListener('click', () => {
      this.navTarget.classList.toggle('sidenav-open');
      if (this.overlay) this.overlay.style.display = 'none';
    });
    document.body.appendChild(this.overlay);

    if (this.navButtonTarget) {
      this.navButtonTarget.addEventListener('click', () => {
        console.log('click navButtonTarget');
        if (this.overlay) this.overlay.style.display = 'block';
        if (this.navTarget) {
          this.navTarget.classList.toggle('sidenav-open');
        }
      });
    }
  }
}
