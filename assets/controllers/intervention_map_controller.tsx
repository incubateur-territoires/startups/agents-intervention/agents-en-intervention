import { Controller } from '@hotwired/stimulus';
import { Root, createRoot } from 'react-dom/client';

import Application from '@aei/pages/Application';
import { InterventionMapPage } from '@aei/pages/InterventionMapPage';
import { EmployerEmployerview, InterventionInterventionview } from '@aei/types';

/* stimulusFetch: 'lazy' */
export default class extends Controller {
  declare readonly employerValue: EmployerEmployerview;
  declare readonly interventionsValue: InterventionInterventionview[];
  declare readonly urlValue: string;

  static values = {
    employer: Object,
    // interventions: Array,
    url: String,
  };

  private root?: Root;

  connect() {
    this.root = createRoot(this.element);
    this.renderComponent(); // Initial rendering
  }

  renderComponent() {
    // console.log(interventions);
    this.root?.render(
      <Application>
        <InterventionMapPage employer={this.employerValue} url={this.urlValue} />
      </Application>
    );
  }

  // Méthode retry qui permet de recharger les interventions
  // retry() {
  //   // Exemple de rechargement dynamique des données d'interventions
  //   // Vous pourriez récupérer ces données via une requête API par exemple
  //   fetch(this.urlValue) // Utiliser une vraie URL d'API pour charger les données
  //     .then((response) => response.json())
  //     .then((data) => {
  //       this.renderComponent(data); // Re-rendre avec les nouvelles données
  //     })
  //     .catch((error) => {
  //       console.error('Erreur lors du rechargement des interventions:', error);
  //     });
  // }
}
