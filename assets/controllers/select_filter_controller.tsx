import { Controller } from '@hotwired/stimulus';
import { createRoot } from 'react-dom/client';

import SearchFilter from '@aei/src/components/SearchFilter';
import { Category } from '@aei/src/components/SearchFilter/filter';

/*
 * The following line makes this controller "lazy": it won't be downloaded until needed
 * See https://github.com/symfony/stimulus-bridge#lazy-controllers
 */
/* stimulusFetch: 'eager' */
export default class extends Controller {
  connect() {
    super.connect();
    console.log("oh oh oh c'est un select filter");

    const category: Category = {
      multiple: true,
      id: '_',
      label: 'Catégorie',
      options: {
        aa: [
          {
            name: 'Jean',
            value: 'jean',
          },
          {
            name: 'Paul',
            value: 'paul',
          },
          {
            name: 'Mich',
            value: 'mich',
          },
        ],
        ab: [
          {
            name: 'Jean',
            value: 'jean',
          },
          {
            name: 'Paul',
            value: 'paul',
          },
          {
            name: 'Mich',
            value: 'mich',
          },
        ],
      },
    };

    createRoot(this.element).render(
      <SearchFilter category={category} onSelect={console.log} onUnselect={console.log} selected={new Set<string>()} />
    );
  }
}
