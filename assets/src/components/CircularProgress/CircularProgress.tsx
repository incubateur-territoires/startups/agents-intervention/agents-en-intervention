import React from 'react';

import './CircularProgress.css';

interface CircularProgressProps {
  size?: number; // Taille du composant (largeur/hauteur en pixels)
  thickness?: number; // Épaisseur de la barre de progression
}

const CircularProgress: React.FC<CircularProgressProps> = ({ size = 20, thickness = 4 }) => {
  const radius = (size - thickness) / 2;
  const circumference = 2 * Math.PI * radius;

  return (
    <div className="circular-progress" style={{ width: size, height: size }}>
      <svg width={size} height={size} viewBox={`0 0 ${size} ${size}`}>
        <circle className="circular-progress-background" cx={size / 2} cy={size / 2} r={radius} strokeWidth={thickness} />
        <circle
          className="circular-progress-bar"
          cx={size / 2}
          cy={size / 2}
          r={radius}
          strokeWidth={thickness}
          strokeDasharray={circumference}
          strokeDashoffset={circumference * 0.75}
        />
      </svg>
    </div>
  );
};

export default CircularProgress;
