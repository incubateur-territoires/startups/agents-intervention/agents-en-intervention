import { useCallback } from 'react';

import { useTeamList } from '@aei/pages/team/TeamListPage';
import { Avatar } from '@aei/src/components';

export interface AddUserProps {
  onClick: React.MouseEventHandler<HTMLDivElement>;
}

const AddUser = () => {
  const [, dispatch] = useTeamList();
  const onClick: React.MouseEventHandler<HTMLDivElement> = useCallback(() => {
    dispatch({ type: 'openForm', payload: { user: null } });
  }, [dispatch]);
  const onKeyDown: React.KeyboardEventHandler<HTMLDivElement> = useCallback(() => {
    dispatch({ type: 'openForm', payload: { user: null } });
  }, [dispatch]);
  return (
    <div
      onClick={onClick}
      onKeyDown={onKeyDown}
      style={{
        cursor: 'pointer',
        width: 160,
        maxWidth: 160, // marginBottom: '1.5rem',
        // marginLeft: '1rem',
        boxShadow: 'none !important',
        border: '1px solid var(--aei-border-card-color)',
        borderRadius: '5px',
        position: 'relative',
        backgroundColor: 'var(--aei-background-white)',
      }}
    >
      <div className="aei-stack">
        <div style={{ display: 'flex', justifyContent: 'center', margin: 'auto', paddingTop: '1rem' }}>
          <div style={{ alignItems: 'center' }}>
            <Avatar fullName="+" size={60} />
          </div>
        </div>
        <div style={{ flex: '1 0 auto', padding: '0.5rem', textAlign: 'center' }}>
          <h5 className="fr-m-0">Ajouter un utilisateur</h5>
        </div>
      </div>
    </div>
  );
};

export default AddUser;
