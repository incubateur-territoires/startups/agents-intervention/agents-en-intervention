import { Select } from '@codegouvfr/react-dsfr/Select';
import { useFormContext } from 'react-hook-form';

import { useApiEmployersGetCollection } from '@aei/src/client/generated/components';
import { LoadingArea } from '@aei/src/ui/LoadingArea';

export type EmployerInputProps = {
  defaultValue?: number;
  name: string;
};

const EmployerInput = ({ defaultValue, name }: EmployerInputProps) => {
  const {
    register,
    formState: { errors },
  } = useFormContext();
  const { data: employers, isFetching } = useApiEmployersGetCollection({});

  if (isFetching) {
    return <LoadingArea ariaLabelTarget="employers input" />;
  }

  if (!employers) {
    return null;
  }

  return (
    <Select
      label="Commune *"
      state={errors[name] ? 'error' : 'default'}
      stateRelatedMessage={errors[name]?.message?.toString()}
      nativeSelectProps={{
        ...register(name),
        required: true,
        'aria-required': 'true',
      }}
    >
      {employers.map((employer) => (
        <option key={employer.id} value={employer.id} selected={employer.id === defaultValue}>
          {employer.name}
        </option>
      ))}
    </Select>
  );
};

export default EmployerInput;
