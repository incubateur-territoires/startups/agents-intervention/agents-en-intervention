import Input from '@codegouvfr/react-dsfr/Input';
import { Select } from '@codegouvfr/react-dsfr/Select';
import { zodResolver } from '@hookform/resolvers/zod';
import { useEffect } from 'react';
import { FormProvider, SubmitHandler, useForm } from 'react-hook-form';
import { useTranslation } from 'react-i18next';
import z from 'zod';

import { useTeamList } from '@aei/pages/team/TeamListPage';
import { useApiUserPatch, useApiUsersPost } from '@aei/src/client/generated/components';
import LoadingButton from '@aei/src/components/LoadingButton';
import { PictureType } from '@aei/src/models/entities/picture';
import { UserRoleSchema } from '@aei/src/models/entities/user';
import { useUser } from '@aei/src/utils/auth';
import { PictureTag, Role } from '@aei/types';

import { PasswordInput } from '../../PasswordInput';
import { PictureInput } from '../../form';
import EmployerInput from './EmployerInput';

const UserFormSchema = (creation: boolean) =>
  z
    .object({
      firstname: z.string().min(1, 'Veuillez entrer un nom'),
      lastname: z.string().min(1, 'Veuillez entrer un prénom'),
      login: z.string().min(1, 'Veuillez entrer un identifiant'),
      email: z.string().email().optional().or(z.literal('')),
      phoneNumber: z.string().optional().or(z.literal('')),
      employer: z.any().optional(),
      role: UserRoleSchema,
      picture: PictureType.nullable(),
      password: creation
        ? z.string().min(1, "Un mot de passe est obligatoire lors de la création d'un utilisateur")
        : z.string().optional().or(z.literal('')),
      password_confirmation: z.string().optional().or(z.literal('')),
    })
    .superRefine(({ password_confirmation, password }, ctx) => {
      if (password_confirmation !== password) {
        ctx.addIssue({
          code: 'custom',
          path: ['password_confirmation'],
          message: 'Le mot de passe et sa confirmation doivent être égaux',
        });
      }
    });

type UserFormType = z.infer<ReturnType<typeof UserFormSchema>>;

const UserForm = () => {
  const { t } = useTranslation('common');
  const [{ userToModify }, dispatch] = useTeamList();
  const { hasRole, isAdmin, user } = useUser();

  const form = useForm<UserFormType>({
    resolver: zodResolver(UserFormSchema(!userToModify)),
    reValidateMode: 'onChange',
  });
  const {
    handleSubmit,
    reset,
    register,
    setError,
    setValue,
    watch,
    formState: { isSubmitting, errors },
  } = form;
  const picture = watch('picture');
  console.log(watch(), UserFormSchema(!userToModify).safeParse(form.control._defaultValues));

  const role = userToModify?.roles?.at(0);
  useEffect(() => {
    if (userToModify) {
      reset({
        lastname: userToModify.lastname,
        firstname: userToModify.firstname,
        login: userToModify.login,
        email: userToModify.email || undefined,
        phoneNumber: userToModify.phoneNumber || undefined,
        password: undefined,
        role,
        picture: userToModify.picture || null,
      });
    } else {
      reset({
        lastname: undefined,
        firstname: undefined,
        login: undefined,
        password: undefined,
        email: undefined,
        phoneNumber: undefined,
        role: undefined,
        picture: null,
      });
    }
  }, [userToModify, reset, role]);

  const { mutateAsync: create } = useApiUsersPost();
  const { mutateAsync: update } = useApiUserPatch();

  if (!user) return <p>Erreur</p>;
  const onSubmit: SubmitHandler<UserFormType> = async ({ role, picture, ...data }) =>
    Promise.resolve({
      ...data,
      picture: (picture && `api/pictures/${picture.id}`) || null,
      roles: [role],
      employer: isAdmin ? `api/employers/${data.employer}` : user.employer,
    })
      .then(async (body) => {
        if (userToModify) {
          return update({
            pathParams: { id: `${userToModify.id}` },
            ...({ body } as any),
          });
        } else {
          return create({
            body,
          });
        }
      })
      .then((user) => {
        dispatch({ type: 'closeForm', payload: { user } });
        reset({
          lastname: undefined,
          firstname: undefined,
          login: undefined,
          password: undefined,
          role: undefined,
        });
      })
      .catch((err) => {
        if (err.stack['@type'] === 'hydra:Error') {
          if (
            err.stack['hydra:description'] ===
            'Le mot de passe doit contenir au moins 10 caractères, une majuscule, une minuscule, un chiffre et un caractère spécial.'
          ) {
            setError('password', {
              message: err.stack['hydra:description'],
            });
          } else {
            setError('login', { message: err.stack['hydra:description'] });
          }
        } else if (err.stack.violations) {
          console.error(err.stack.detail);
          err.stack.violations.map((violation: any) => setError(violation.propertyPath, { message: violation.message }));
        } else if (err.stack.detail === 'ANCT employees must have ROLE_ADMIN role') {
          setError('role', { message: "Les employés de l'ANCT doivent avoir le rôle d'administrateur" });
        } else if (err.stack.detail === '') {
          setError('role', { message: "Les employés de l'ANCT doivent avoir le rôle d'administrateur" });
        } else {
          setError('login', { message: "Une erreur s'est produite" });
        }
      });

  return (
    <FormProvider {...form}>
      <form onSubmit={handleSubmit(onSubmit)}>
        <div className="fr-p-2w">
          <div className="aei-stack">
            <div className="fr-alert fr-alert--info fr-mb-2w">
              {userToModify?.connectWithProConnect ? (
                <p>Cet utilisateur est connecté avec Pro Connect, vous ne pouvez pas modifier son identifiant, email ni mot de passe.</p>
              ) : (
                <p>
                  Pour permettre à cet utilisateur de se connecter avec ProConnect, veuillez renseigner son email de connexion dans les champs
                  Identifiant et Email.
                </p>
              )}
            </div>

            <Input
              label="Nom *"
              stateRelatedMessage={errors.lastname?.message}
              state={errors.lastname ? 'error' : 'default'}
              nativeInputProps={register('lastname')}
            />

            <Input
              label="Prénom *"
              stateRelatedMessage={errors.firstname?.message}
              state={errors.firstname ? 'error' : 'default'}
              nativeInputProps={register('firstname')}
            />

            <Input
              label="Identifiant *"
              disabled={userToModify?.connectWithProConnect}
              stateRelatedMessage={errors.login?.message}
              state={errors.login ? 'error' : 'default'}
              nativeInputProps={register('login')}
            />

            {isAdmin && <EmployerInput name="employer" defaultValue={userToModify?.employer.id} />}

            <Select
              label="Rôle *"
              state={errors.role ? 'error' : 'default'}
              stateRelatedMessage={errors.role?.message?.toString()}
              nativeSelectProps={{
                ...register('role'),
                required: true,
                'aria-required': 'true',
              }}
            >
              <option value={Role.Agent}>{t('model.user.roles.ROLE_AGENT')}</option>
              {userToModify?.role === Role.AdminEmployer && (hasRole(Role.Admin) || hasRole(Role.AdminEmployer)) && (
                <option value={Role.AdminEmployer}>{t('model.user.roles.ROLE_ADMIN_EMPLOYER')}</option>
              )}
              <option value={Role.Director}>{t('model.user.roles.ROLE_DIRECTOR')}</option>
              <option value={Role.Elected}>{t('model.user.roles.ROLE_ELECTED')}</option>
              {isAdmin && <option value={Role.Admin}>{t('model.user.roles.ROLE_ADMIN')}</option>}
            </Select>

            {!userToModify?.connectWithProConnect && (
              <>
                <PasswordInput label="Mot de passe" name="password" withPasswordConstraints required={!userToModify} />
                <PasswordInput label="Confirmation du mot de passe" name="password_confirmation" required={!userToModify} />
              </>
            )}

            {isAdmin && (
              <Input
                label="Email"
                disabled={userToModify?.connectWithProConnect}
                stateRelatedMessage={errors.email?.message}
                state={errors.email ? 'error' : 'default'}
                nativeInputProps={{ ...register('email'), type: 'email' }}
              />
            )}

            {isAdmin && (
              <Input
                label="Numéro de téléphone"
                stateRelatedMessage={errors.phoneNumber?.message}
                state={errors.phoneNumber ? 'error' : 'default'}
                nativeInputProps={register('phoneNumber')}
              />
            )}

            <PictureInput
              name="picture"
              onChange={(picture) => setValue('picture', picture || null)}
              tag={PictureTag.Avatar}
              value={picture || undefined}
              withCompression
            />
          </div>
        </div>
        <hr className="fr-m-0" />
        <div className="fr-p-0-2w">
          <div className="aei-stack aei-stack-small-gap aei-stack-columns" style={{ justifyContent: 'flex-end' }}>
            <LoadingButton type="button" priority="secondary" onClick={() => dispatch({ type: 'closeForm' })}>
              Annuler
            </LoadingButton>
            <LoadingButton
              loading={isSubmitting}
              disabled={isSubmitting} // || !isValid
              type="submit"
            >
              {userToModify ? 'Modifier' : 'Créer le compte'}
            </LoadingButton>
          </div>
        </div>
      </form>
    </FormProvider>
  );
};

export default UserForm;
