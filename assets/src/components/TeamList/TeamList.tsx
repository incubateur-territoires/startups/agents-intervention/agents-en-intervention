import { useTranslation } from 'react-i18next';

import { useTeamList } from '@aei/pages/team/TeamListPage';
import { Avatar } from '@aei/src/components';
import { DropdownMenuItem } from '@aei/src/components/DropdownMenu';
import DropdownMenu from '@aei/src/components/DropdownMenu/DropdownMenu';
import { useSingletonConfirmationDialog } from '@aei/src/ui/modal/useModal';
import { UserUserview } from '@aei/types';

import { useApiUsersIdDelete } from '../../client/generated/components';
import AddUser from './AddUser';

export interface TeamListProps {
  modify: (user: UserUserview) => void;
  users: UserUserview[];
}

const TeamList = ({ modify, users }: TeamListProps) => {
  const { t } = useTranslation('common');
  const [, dispatch] = useTeamList();
  const deleteUser = useApiUsersIdDelete();
  const { showConfirmationDialog } = useSingletonConfirmationDialog();

  const deleteUserAction = async (user: UserUserview) => {
    showConfirmationDialog({
      description: <>Êtes-vous sûr de vouloir supprimer l&apos;utilisateur {user.id} ?</>,
      onConfirm: async () => {
        await deleteUser
          .mutateAsync({
            pathParams: {
              id: `${user.id}`,
            },
          })
          .then(() => dispatch({ type: 'removeUser', payload: { user } }));
      },
    });
  };

  return (
    <div className="aei-stack aei-stack-columns aei-stack-large-gap">
      {users.map((user) => (
        <div
          key={user.id}
          style={{
            cursor: 'pointer',
            width: 160,
            maxWidth: 160, // marginBottom: '1.5rem',
            // marginLeft: '1rem',
            boxShadow: 'none !important',
            border: '1px solid var(--aei-border-card-color)',
            borderRadius: '5px',
            position: 'relative',
            backgroundColor: 'var(--aei-background-white)',
          }}
          onClick={() => modify(user)}
          onKeyDown={() => modify(user)}
        >
          <div className="aei-stack">
            <div style={{ display: 'flex', justifyContent: 'center', margin: 'auto', paddingTop: '1rem' }}>
              <div style={{ alignItems: 'center' }}>
                <Avatar src={user.picture?.url || undefined} fullName={`${user.firstname} ${user.lastname}`} size={60} />
              </div>
            </div>
            <div style={{ flex: '1 0 auto', padding: '0.5rem', textAlign: 'center' }}>
              <h5 className="fr-m-0">
                {user.firstname} {user.lastname}
              </h5>
              <p className="fr-m-0">{user.login}</p>
              <p className="fr-m-0" style={{ color: 'gray' }}>
                <span>{user.roles && t(`model.user.roles.${user.roles[0]}`)}</span>
              </p>
            </div>
            <div style={{ position: 'absolute', right: 0 }}>
              <DropdownMenu>
                <DropdownMenuItem onClick={() => modify(user)}>Modifier</DropdownMenuItem>
                <DropdownMenuItem onClick={() => deleteUserAction(user)}>Supprimer</DropdownMenuItem>
              </DropdownMenu>
            </div>
          </div>
        </div>
      ))}
      <AddUser key="add" />
    </div>
  );
};

export default TeamList;
