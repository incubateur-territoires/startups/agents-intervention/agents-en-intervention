import Pagination from '@codegouvfr/react-dsfr/Pagination';
import classNames from 'classnames';
import React, { ComponentType } from 'react';

import { InterventionListProps } from '@aei/src/components/InterventionViewer/InterventionListDSFR';
import LinearProgress from '@aei/src/components/LinearProgress/LinearProgress';

import './DataTable.scss';
import { DataTableConfiguration, DataTableRow, DataTableSearchParams } from './DataTableConfiguration';
import SortLink from './SortLink';
import { createSortLinkProps } from './createSortLinkProps';

export type DataTableClasses = {
  wrapper?: string;
  container?: string;
  content?: string;
  table?: string;
  thead?: string;
  th?: string;
  tbody?: string;
  tr?: string;
};

interface DataTableProps<Data extends DataTableRow, Configuration extends DataTableConfiguration<Data>> {
  rows: Data[];
  configuration: Configuration;
  className?: string;
  classes?: DataTableClasses;
  loading?: boolean;
  searchParams: DataTableSearchParams<Configuration>;
  baseHref: string;
  rowButtonComponent?: ComponentType<{ row: Data }>;
  rowCount?: number;
  paginationModel?: InterventionListProps['paginationModel'];
  setPaginationModel?: InterventionListProps['setPaginationModel'];
}

const DataTable = <Data extends DataTableRow, Configuration extends DataTableConfiguration<Data>>({
  configuration,
  rows,
  className,
  classes,
  loading = false,
  searchParams,
  baseHref,
  rowButtonComponent: RowButtonComponent,
  rowCount,
  paginationModel,
  setPaginationModel,
}: DataTableProps<Data, Configuration>) => {
  const sortLinkProps = (sortParams: DataTableSearchParams<Configuration>, isDefault = false, defaultSortableDirection?: 'asc' | 'desc') =>
    createSortLinkProps({
      searchParams,
      sortParams,
      isDefault,
      defaultSortableDirection,
      baseHref,
      useHistoryApi: configuration.useHistoryApi,
    });

  return (
    <>
      <div className={classNames('fr-table', className)} data-fr-js-table="true">
        {loading ? (
          <div style={{ position: 'sticky', top: 0, zIndex: 500 }}>
            <LinearProgress />
          </div>
        ) : (
          <div style={{ height: '4px' }}></div>
        )}
        <div className={classNames('fr-table__wrapper', classes?.wrapper)}>
          <div className={classNames('fr-table__container', classes?.container)}>
            <div className={classNames('fr-table__content', classes?.content)}>
              <table className={classNames('aei-table-fixed', classes?.table)} data-fr-js-table-element="true">
                <thead className={classNames(classes?.thead)}>
                  <tr className={classNames(classes?.tr)}>
                    {configuration.columns.map(
                      ({ name, header, sortInMemory, sortable, defaultSortable, defaultSortableDirection, headerClassName, orderBy }) => (
                        <th scope="col" key={name} className={classNames(headerClassName, classes?.th)}>
                          {header}
                          {!!sortable && (!!defaultSortable || !!sortInMemory || !!orderBy) && (
                            <SortLink
                              {...sortLinkProps(
                                {
                                  sort_by: name,
                                } as DataTableSearchParams<Configuration>,
                                defaultSortable,
                                defaultSortableDirection
                              )}
                            />
                          )}
                        </th>
                      )
                    )}
                  </tr>
                </thead>
                <tbody className={classNames(classes?.tbody)}>
                  {rows.map((row) => (
                    <tr
                      key={configuration.rowKey(row)}
                      onClick={(e) => configuration.onRowClick && configuration.onRowClick(row, e)}
                      className={classNames(
                        classes?.tr,
                        !!configuration.rowLink && 'fr-enlarge-link',
                        (!!configuration.rowLink || !!configuration.onRowClick) && 'rowWithLink',
                        !!RowButtonComponent && 'fr-enlarge-button',
                        !!RowButtonComponent && 'rowWithButton'
                      )}
                    >
                      {configuration.columns.map(({ name, cellAsTh, cell, cellClassName }) => {
                        if (!cell) {
                          return null;
                        }
                        const Component = cellAsTh ? 'th' : 'td';

                        return (
                          <Component className={classNames(cellClassName)} key={name}>
                            {cell(row)}
                          </Component>
                        );
                      })}
                      {!!configuration.rowLink && (
                        <td className="rowLinkCell">
                          <a {...configuration.rowLink(row)} />
                        </td>
                      )}
                      {!!RowButtonComponent && (
                        <td className="rowButtonCell">
                          <RowButtonComponent row={row} />
                        </td>
                      )}
                    </tr>
                  ))}
                  {rows.length === 0 && (
                    <tr>
                      <td colSpan={configuration.columns.length}>Aucun résultat</td>
                    </tr>
                  )}
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      {paginationModel && setPaginationModel && rowCount !== undefined && rowCount !== 0 && (
        <Pagination
          count={Math.ceil(rowCount / paginationModel.pageSize)}
          defaultPage={paginationModel.page + 1}
          getPageLinkProps={(pageNumber) => ({
            href: '#',
            onClick: (e) => {
              e.preventDefault();
              window.scrollTo({
                top: 0,
                behavior: 'smooth', // Optionnel : pour un défilement en douceur
              });
              setPaginationModel((prev) => ({ page: pageNumber - 1, pageSize: prev.pageSize }));
            },
          })}
          showFirstLast
        />
      )}
    </>
  );
};

export default DataTable;
