import type { SortDirection } from './SortLink';

export const createSortLinkProps = <T extends { sort_by?: string; order?: SortDirection }, V extends { sort_by?: string; order?: SortDirection }>({
  sortParams,
  searchParams,
  defaultSortableDirection = 'asc',
  isDefault = false,
  baseHref,
  useHistoryApi = false,
}: {
  searchParams: T;
  sortParams: V;
  isDefault?: boolean;
  defaultSortableDirection?: SortDirection;
  baseHref: string;
  useHistoryApi?: boolean;
}) => {
  const params = {
    ...searchParams,
    ...sortParams,
  };

  const isActiveByDefault = !searchParams.sort_by && isDefault;

  const isActive = isActiveByDefault || searchParams.sort_by === sortParams.sort_by;

  const order =
    isActiveByDefault && !searchParams.order
      ? defaultSortableDirection === 'desc'
        ? 'asc'
        : 'desc'
      : isActive
        ? searchParams.order === 'desc'
          ? 'asc'
          : 'desc'
        : 'asc';

  const props = {
    ...params,
    isActive,
    order,
  };

  const href = `${baseHref}?${new URLSearchParams({
    ...params,
    order,
  }).toString()}`;

  return {
    ...props,
    href,
    onClick: (e: React.MouseEvent) => {
      if (!useHistoryApi) return;

      e.preventDefault();
      e.stopPropagation();
      window.history.replaceState({ href }, '', href);
      window.dispatchEvent(new PopStateEvent('popstate', { state: href }));
    },
  };
};
