export type SortDirection = 'asc' | 'desc';

export type SortLinkProps = {
  order: SortDirection;
  isActive?: boolean;
  accessibilityTitle?: string;
  href: string;
  onClick?: (e: React.MouseEvent) => void;
};

const SortLink = ({ order, isActive, accessibilityTitle, href, onClick }: SortLinkProps) => {
  const isDesc = order === 'desc';
  const isAsc = !isDesc;

  const icon = isActive && isAsc ? 'fr-icon-arrow-up-line' : 'fr-icon-arrow-down-line';

  const title = accessibilityTitle ? `Trier par ${accessibilityTitle}, ordre ${isActive && isAsc ? 'décroissant' : 'croissant'}` : undefined;

  return (
    <a
      className={`fr-btn fr-ml-2v fr-btn--tertiary-no-outline fr-btn--sm ${icon}`}
      title={title}
      href={href}
      onClick={onClick}
      style={{
        color: isActive ? undefined : 'var(--text-disabled-grey)',
      }}
    />
  );
};

export default SortLink;
