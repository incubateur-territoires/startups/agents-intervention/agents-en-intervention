import Input from '@codegouvfr/react-dsfr/Input';
import { Fragment, ReactNode } from 'react';
import { useFormContext } from 'react-hook-form';

const DivPasswordValidation = ({ children }: { children: ReactNode }) => (
  <div className="fr-mb-4w" data-controller="password-validation">
    {children}
  </div>
);

const PasswordInput = ({
  label,
  name,
  required = false,
  withPasswordConstraints = false,
}: {
  label: string;
  name: string;
  required?: boolean;
  withPasswordConstraints?: boolean;
}) => {
  const {
    register,
    setValue,
    formState: { errors },
  } = useFormContext();

  const Container = withPasswordConstraints ? DivPasswordValidation : Fragment;

  return (
    <Container>
      <Input
        className="react-password-field"
        label={label + (required ? ' *' : '')}
        state={errors[name] ? 'error' : 'default'}
        stateRelatedMessage={errors[name]?.message?.toString()}
        nativeInputProps={{
          ...register(name),
          type: 'password',
          required,
          autoComplete: 'new-password',
          ...(withPasswordConstraints
            ? {
                'data-action': 'input->password-validation#validate',
                'data-password-validation-target': 'input',
              }
            : {}), // @ts-ignore
          'data-controller': 'symfony--ux-toggle-password--toggle-password',
          'data-symfony--ux-toggle-password--toggle-password-hidden-label-value': 'Masquer',
          'data-symfony--ux-toggle-password--toggle-password-visible-label-value': 'Afficher',
          'data-symfony--ux-toggle-password--toggle-password-button-classes-value': '["toggle-password-button"]',
          onChange: (event) => {
            setValue(name, event.target.value.replace('’', "'"));
          },
        }}
      />
    </Container>
  );
};

export default PasswordInput;
