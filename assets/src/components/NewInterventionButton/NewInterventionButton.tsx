import Button from '@codegouvfr/react-dsfr/Button';
import React from 'react';

import { InterventionViewAndMutateMode, useInterventionViewAndMutate } from '@aei/src/components/InterventionViewAndMutateProvider';
import { useUser } from '@aei/src/utils/auth';
import useEmployerId from '@aei/src/utils/useEmployerId';

const NewInterventionButton = () => {
  const { isAgent } = useUser();
  const employerId = useEmployerId();
  const { setContext } = useInterventionViewAndMutate();
  return (
    <Button
      priority="secondary"
      onClick={() =>
        setContext({
          mode: InterventionViewAndMutateMode.Create,
          retry: () => (isAgent ? window.location.reload() : window.location.replace(`/${employerId}/interventions`)),
        })
      }
    >
      Créer une intervention
    </Button>
  );
};

export default NewInterventionButton;
