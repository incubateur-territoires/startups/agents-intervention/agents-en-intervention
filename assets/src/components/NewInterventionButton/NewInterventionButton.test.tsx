import { fireEvent, screen } from '@testing-library/dom';
import { render } from '@testing-library/react';
import React from 'react';

import { InterventionViewAndMutateMode, useInterventionViewAndMutate } from '@aei/src/components/InterventionViewAndMutateProvider';
import { useUser } from '@aei/src/utils/auth';
import useEmployerId from '@aei/src/utils/useEmployerId';

import NewInterventionButton from './NewInterventionButton';

jest.mock('@aei/src/components/InterventionViewAndMutateProvider');
jest.mock('@aei/src/utils/auth');
jest.mock('@aei/src/utils/useEmployerId');

const employerIdMock = '12345';

describe('NewInterventionButton', () => {
  it('renders the button with correct text', () => {
    const setContextMock = jest.fn();
    const isAgentMock = true;

    (useInterventionViewAndMutate as jest.Mock).mockReturnValue({ setContext: setContextMock });
    (useUser as jest.Mock).mockReturnValue({ isAgent: isAgentMock });
    (useEmployerId as jest.Mock).mockReturnValue(employerIdMock);
    render(<NewInterventionButton />);
    expect(screen.getByRole('button')).toHaveTextContent('Créer une intervention');
  });

  it('calls setContext with correct parameters on button click', () => {
    const setContextMock = jest.fn();
    const isAgentMock = jest.fn();

    (useInterventionViewAndMutate as jest.Mock).mockReturnValue({ setContext: setContextMock });
    (useUser as jest.Mock).mockReturnValue({ isAgent: isAgentMock });
    (useEmployerId as jest.Mock).mockReturnValue(employerIdMock);

    render(<NewInterventionButton />);
    fireEvent.click(screen.getByRole('button'));

    expect(setContextMock).toHaveBeenCalledWith({
      mode: InterventionViewAndMutateMode.Create,
      retry: expect.any(Function),
    });
  });

  it('retry function reloads the page is user is agent', () => {
    const setContextMock = jest.fn();
    const employerIdMock = '12345';

    (useInterventionViewAndMutate as jest.Mock).mockReturnValue({ setContext: setContextMock });
    (useUser as jest.Mock).mockReturnValue({ isAgent: true });
    (useEmployerId as jest.Mock).mockReturnValue(employerIdMock);

    render(<NewInterventionButton />);
    fireEvent.click(screen.getByRole('button'));

    const retryFunction = setContextMock.mock.calls[0][0].retry;

    // Test when user is an agent
    Object.defineProperty(window, 'location', {
      value: { reload: jest.fn() },
      writable: true,
    });
    retryFunction();
    expect(window.location.reload).toHaveBeenCalled();
  });

  it('retry function replace the page is user is not agent', () => {
    const setContextMock = jest.fn();
    const employerIdMock = '12345';

    (useInterventionViewAndMutate as jest.Mock).mockReturnValue({ setContext: setContextMock });
    (useUser as jest.Mock).mockReturnValue({ isAgent: false });
    (useEmployerId as jest.Mock).mockReturnValue(employerIdMock);

    render(<NewInterventionButton />);
    fireEvent.click(screen.getByRole('button'));

    const retryFunction = setContextMock.mock.calls[0][0].retry;

    // Test when user is not an agent
    (useUser as jest.Mock).mockReturnValue({ isAgent: false });
    Object.defineProperty(window, 'location', {
      value: { replace: jest.fn() },
      writable: true,
    });
    retryFunction();
    expect(window.location.replace).toHaveBeenCalledWith(`/${employerIdMock}/interventions`);
  });
});
