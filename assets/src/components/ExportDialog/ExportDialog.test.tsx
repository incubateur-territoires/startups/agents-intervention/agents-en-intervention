import { fireEvent, render, screen, waitFor } from '@testing-library/react';

import { fetchApiEmployersEmployerIdinterventionsExportGetCollection } from '@aei/client/fetchers';
import useEmployerId from '@aei/src/utils/useEmployerId';

import ExportDialog from './ExportDialog';

jest.mock('@aei/client/fetchers');
jest.mock('@aei/src/utils/useEmployerId');

const mockUseEmployerId = useEmployerId as jest.Mock;
const mockFetchApi = fetchApiEmployersEmployerIdinterventionsExportGetCollection as jest.Mock;

describe('ExportDialog', () => {
  beforeEach(() => {
    mockUseEmployerId.mockReturnValue('123');
    mockFetchApi.mockResolvedValue({ text: jest.fn().mockResolvedValue('csv content') });
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  const renderComponent = (props = {}) => {
    return render(<ExportDialog onClose={jest.fn()} {...props} />);
  };

  it('renders the component', () => {
    renderComponent();
    expect(screen.getByText('Exporter des interventions')).toBeInTheDocument();
  });

  it('displays validation errors when form is submitted empty', async () => {
    renderComponent();

    // Remove the required attribute to simulate form submission
    screen.getByLabelText('Date de début').removeAttribute('required');

    fireEvent.click(screen.getByText("Télécharger l'export"));

    await waitFor(() => {
      expect(screen.getByText('La date de début est requise')).toBeInTheDocument();
    });
  });

  it('submits the form and calls the API', async () => {
    renderComponent();
    fireEvent.change(screen.getByLabelText('Date de début'), { target: { value: '2023-01-01' } });
    fireEvent.change(screen.getByLabelText('Date de fin'), { target: { value: '2023-12-31' } });
    fireEvent.click(screen.getByText("Télécharger l'export"));

    await waitFor(() => {
      expect(mockFetchApi).toHaveBeenCalledWith({
        pathParams: { employerId: '123' },
        queryParams: {
          'createdAt[after]': '2023-01-01',
          'createdAt[before]': '2023-12-31',
          pagination: false,
        },
        format: 'text/csv',
      });
    });
  });

  it('displays an error alert if the API call fails', async () => {
    mockFetchApi.mockRejectedValue(new Error('API error'));
    renderComponent();
    fireEvent.change(screen.getByLabelText('Date de début'), { target: { value: '2023-01-01' } });
    fireEvent.change(screen.getByLabelText('Date de fin'), { target: { value: '2023-12-31' } });
    fireEvent.click(screen.getByText("Télécharger l'export"));

    await waitFor(() => {
      expect(screen.getByText('API error')).toBeInTheDocument();
    });
  });

  it('submits the form with the withFilter checkbox checked', async () => {
    renderComponent();
    fireEvent.change(screen.getByLabelText('Date de début'), { target: { value: '2023-01-01' } });
    fireEvent.change(screen.getByLabelText('Date de fin'), { target: { value: '2023-12-31' } });
    fireEvent.click(screen.getByLabelText('Prendre en compte les filtres activés'));
    fireEvent.click(screen.getByText("Télécharger l'export"));

    await waitFor(() => {
      expect(mockFetchApi).toHaveBeenCalledWith({
        pathParams: { employerId: '123' },
        queryParams: {
          'createdAt[after]': '2023-01-01',
          'createdAt[before]': '2023-12-31',
          pagination: false, // Ajoutez ici les filtres supplémentaires si nécessaire
        },
        format: 'text/csv',
      });
    });
  });

  it('submits the form and calls the API with Excel format', async () => {
    renderComponent();
    fireEvent.change(screen.getByLabelText('Date de début'), { target: { value: '2023-01-01' } });
    fireEvent.change(screen.getByLabelText('Date de fin'), { target: { value: '2023-12-31' } });
    fireEvent.click(screen.getByLabelText('Excel'));
    fireEvent.click(screen.getByText("Télécharger l'export"));

    await waitFor(() => {
      expect(mockFetchApi).toHaveBeenCalledWith({
        pathParams: { employerId: '123' },
        queryParams: {
          'createdAt[after]': '2023-01-01',
          'createdAt[before]': '2023-12-31',
          pagination: false,
        },
        format: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
      });
    });
  });
});
