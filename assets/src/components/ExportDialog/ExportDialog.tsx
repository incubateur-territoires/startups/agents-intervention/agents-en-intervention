import Checkbox from '@codegouvfr/react-dsfr/Checkbox';
import { Input } from '@codegouvfr/react-dsfr/Input';
import RadioButtons from '@codegouvfr/react-dsfr/RadioButtons';
import { zodResolver } from '@hookform/resolvers/zod';
import { saveAs } from 'file-saver';
import { useRef } from 'react';
import { FormProvider, useForm } from 'react-hook-form';
import { useAsyncFn } from 'react-use';
import z from 'zod';

import { ExportFormat, fetchApiEmployersEmployerIdinterventionsExportGetCollection } from '@aei/client/fetchers';
import { ApiEmployersEmployerIdinterventionsGetCollectionQueryParams } from '@aei/src/client/generated/components';
import { BaseForm } from '@aei/src/components/BaseForm';
import { ErrorAlert } from '@aei/src/ui/ErrorAlert';
import Modal from '@aei/src/ui/modal';
import useEmployerId from '@aei/src/utils/useEmployerId';

const ExportDialogSchema = z.object({
  startDate: z.string().min(1, 'La date de début est requise'),
  endDate: z.string().min(1, 'La date de fin est requise'),
  withFilter: z.coerce.string().optional(),
  format: z.nativeEnum(ExportFormat).default(ExportFormat.CSV),
});

type ExportDialogForm = z.infer<typeof ExportDialogSchema>;

interface ExportDialogProps {
  onClose: () => void;
  queryParams?: ApiEmployersEmployerIdinterventionsGetCollectionQueryParams;
}

const ExportDialog = ({ onClose, queryParams }: ExportDialogProps) => {
  const employerId = useEmployerId();
  const formRef = useRef<HTMLFormElement | null>(null);
  const methods = useForm<ExportDialogForm>({
    resolver: zodResolver(ExportDialogSchema),
    defaultValues: {
      format: ExportFormat.CSV,
      endDate: new Date().toISOString().split('T')[0],
    },
  });
  const { handleSubmit, control } = methods;

  const [state, onSubmit] = useAsyncFn(
    async (data: ExportDialogForm) =>
      fetchApiEmployersEmployerIdinterventionsExportGetCollection({
        pathParams: { employerId: `${employerId}` },
        queryParams: {
          ...(data.withFilter === 'withFilter' ? queryParams : {}),
          'createdAt[after]': data.startDate,
          'createdAt[before]': data.endDate,
          pagination: false,
        },
        format: data.format,
      })
        .then((response) => response.blob())
        .then((responseBlob) => {
          if (data.format === ExportFormat.CSV) {
            const blob = new Blob([responseBlob], { type: 'text/csv;charset=utf-8;' });
            saveAs(blob, 'interventions.csv');
          } else {
            const blob = new Blob([responseBlob], { type: ExportFormat.Excel });
            saveAs(blob, 'interventions.xlsx', { autoBom: true });
          }
        }),
    [queryParams]
  );

  return (
    <Modal
      autoCloseOnConfirm={false}
      iconId="fr-icon-arrow-right-line"
      loading={state.loading}
      onCancel={async () => onClose()}
      onClose={onClose}
      onConfirm={async () => formRef.current?.requestSubmit()}
      open
      primaryButtonText="Télécharger l'export"
      title="Exporter des interventions"
    >
      <FormProvider {...methods}>
        <BaseForm handleSubmit={handleSubmit} onSubmit={onSubmit} control={control} innerRef={formRef} ariaLabel="assigner des agents">
          {state.error && <ErrorAlert errors={[state.error]} />}

          <Checkbox
            id="withFilter"
            stateRelatedMessage={methods.formState.errors.withFilter?.message?.toString() || ''}
            state={methods.formState.errors.withFilter && 'error'}
            options={[
              {
                label: 'Prendre en compte les filtres activés',
                nativeInputProps: { value: 'withFilter', ...methods.register('withFilter') },
              },
            ]}
          />

          <RadioButtons
            legend="Format :"
            name="format"
            options={Object.entries(ExportFormat).map(([label, value]) => ({
              label,
              nativeInputProps: { value, ...methods.register('format') },
            }))}
            orientation="horizontal"
          />

          {/*<Select*/}
          {/*//     nativeSelectProps={{...methods.register('format'),*/}
          {/*//     'aria-label': 'Format',*/}
          {/*//     required: true,}}*/}
          {/*//     >*/}
          {/*//   {Object.entries(ExportFormat).map(([name, format]) => (*/}
          {/*//     <option key={format} value={format}>*/}
          {/*//   {name}*/}
          {/*// </option>*/}
          {/*))}*/}
          {/*// </RadioButtons>*/}

          <p>Choisir la période à exporter :</p>

          <div className="aei-stack aei-stack-columns aei-stack-large-gap aei-stack-no-padding aei-stack-autogrow">
            <Input
              label="Date de début"
              stateRelatedMessage={methods.formState.errors.startDate?.message?.toString() || ''}
              state={methods.formState.errors.startDate && 'error'}
              nativeInputProps={{
                'aria-label': 'Date de début',
                type: 'date',
                required: true,
                ...methods.register('startDate'),
              }}
            />

            <Input
              label="Date de fin"
              stateRelatedMessage={methods.formState.errors.endDate?.message?.toString() || ''}
              state={methods.formState.errors.endDate && 'error'}
              nativeInputProps={{
                'aria-label': 'Date de fin',
                type: 'date',
                required: true,
                ...methods.register('endDate'),
              }}
            />
          </div>
        </BaseForm>
      </FormProvider>
    </Modal>
  );
};

export default ExportDialog;
