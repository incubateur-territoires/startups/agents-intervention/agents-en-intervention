import Tag from '@codegouvfr/react-dsfr/Tag';
import { useColors } from '@codegouvfr/react-dsfr/useColors';
import { useTranslation } from 'react-i18next';

import { Priority } from '@aei/types/Priority';

export interface InterventionPriorityChipProps {
  priority: Priority;
}

export function InterventionPriorityChip(props: InterventionPriorityChipProps) {
  const { t } = useTranslation('common');

  let priorityColor = 'black';
  let priorityText = t(`model.intervention.priority.enum.${props.priority}`);
  let textColor = '#fff';

  const theme = useColors();

  switch (props.priority) {
    case Priority.Normal:
      priorityColor = theme.decisions.background.actionHigh.blueFrance.default;
      break;
    case Priority.Urgent:
      priorityColor = '#FFE8E5';
      textColor = '#B34000';
      break;
  }

  return (
    <Tag
      iconId={props.priority === Priority.Urgent ? 'fr-icon-warning-line' : undefined}
      nativeSpanProps={{ style: { backgroundColor: priorityColor, color: textColor } }}
      small
    >
      {priorityText}
    </Tag>
  );
}
