import Alert from '@codegouvfr/react-dsfr/Alert';
import Tag from '@codegouvfr/react-dsfr/Tag';
import 'leaflet/dist/leaflet.css';
import { useRef, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { MapContainer, Marker, TileLayer } from 'react-leaflet';
import { useAsyncRetry, useToggle } from 'react-use';

import { fetchApiInterventionsIdGet, useApiEmployersEmployerIdactiveAgentsGetCollection, useApiInterventionsIdPatch } from '@aei/client/fetchers';
import { WHEN_ZOOMED_IN } from '@aei/pages/InterventionMapPage';
import '@aei/src/assets/map/leaflet.scss';
import DropdownMenu from '@aei/src/components/DropdownMenu/DropdownMenu';
import { InterventionCommentArea } from '@aei/src/components/InterventionCommentArea';
import AssignmentMenuItem from '@aei/src/components/InterventionViewer/AssignmentMenuItem';
import DeleteMenuItem from '@aei/src/components/InterventionViewer/DeleteMenuItem';
import InterventionViewParticipants from '@aei/src/components/InterventionViewer/InterventionViewParticipants';
import InterventionViewerBottomActions from '@aei/src/components/InterventionViewer/InterventionViewerBottomActions';
import InterventionViewerStatus from '@aei/src/components/InterventionViewer/InterventionViewerStatus';
import ModifyMenuItem from '@aei/src/components/InterventionViewer/ModifyMenuItem';
import PrintMenuItem from '@aei/src/components/InterventionViewer/PrintMenuItem';
import RejectMenuItem from '@aei/src/components/InterventionViewer/RejectMenuItem';
import RouteMenuItem from '@aei/src/components/InterventionViewer/RouteMenuItem';
import SetBlockedMenuItem from '@aei/src/components/InterventionViewer/SetBlockedMenuItem';
import { InterventionViewerProperty } from '@aei/src/components/InterventionViewerProperty';
import MyDrawer from '@aei/src/components/MyDrawerDSFR';
import QuickStatusBadgeIndicator from '@aei/src/components/QuickStatusBadgeIndicator';
import { LoadingArea } from '@aei/src/ui/LoadingArea';
import { useSingletonConfirmationDialog } from '@aei/src/ui/modal/useModal';
import { addressFormatterFormat } from '@aei/src/utils/address';
import { useUser } from '@aei/src/utils/auth';
import { bluePinIcon, redPinIcon, tileLayerDefaultProps } from '@aei/src/utils/map';
import useEmployerId from '@aei/src/utils/useEmployerId';
import { Intervention, InterventionInterventionview, PictureTag, Priority, Role, Status } from '@aei/types';

import { PictureInput } from './form';

export enum Section {
  ToValidate = 'to-validate',
  ToDo = 'todo',
  InProgress = 'in-progress',
  Done = 'done',
}

export const sectionFlow: Section[] = Object.values(Section);

export interface InterventionViewerInnerProps {
  interventionId: Intervention['id'];
  onClose?: () => void;
  modify: () => void;
  refetch: () => void | Promise<void>;
}

export function InterventionViewer({ interventionId, onClose = () => null, refetch, modify }: Readonly<InterventionViewerInnerProps>) {
  const { t } = useTranslation('common');
  const { hasRole, loading, user } = useUser();
  const employerId = useEmployerId();
  const [intervention, setIntervention] = useState<InterventionInterventionview | null>(null);
  const ref = useRef<HTMLDivElement>(null);
  const [showAfterPicturePicker, toggleShowAfterPicturePicker] = useToggle(false);
  const canMutate = !hasRole(Role.Elected) && intervention?.status !== Status.Rejected;

  const { data: agentsActifs } = useApiEmployersEmployerIdactiveAgentsGetCollection({
    pathParams: {
      employerId: `${employerId}`,
    },
  });

  const { retry } = useAsyncRetry(
    () =>
      fetchApiInterventionsIdGet({ pathParams: { id: `${interventionId}` } }).then((data) => {
        setIntervention(data);
      }),
    [interventionId]
  );

  const updateIntervention = useApiInterventionsIdPatch({
    onSuccess: () => {
      // Needed to refetch manually to get the new comment from top parent component
      return refetch();
    },
  });
  const { showConfirmationDialog } = useSingletonConfirmationDialog();

  if (loading) {
    return <LoadingArea ariaLabelTarget="contenu" />;
  } else if (!user) {
    throw new Error('must be authenticated to use the viewer');
  }

  const onReject = () => {
    if (!intervention) return;
    setIntervention({ ...intervention, status: Status.Rejected });
    refetch();
  };

  const setBlocked = async (intervention: InterventionInterventionview) => {
    showConfirmationDialog({
      description: <>Êtes-vous sûr de vouloir mettre en attente l&apos;intervention #{intervention.logicId} ?</>,
      onConfirm: async () =>
        updateInterventionHelper(intervention, {
          status: 'blocked',
        }),
    });
  };

  const updateInterventionHelper = async (intervention: InterventionInterventionview, body: any) => {
    // `body: any` needed due to missing types (ref: https://github.com/fabien0102/openapi-codegen/issues/198)
    return await updateIntervention
      .mutateAsync({
        pathParams: {
          id: `${intervention.id}`,
        },
        headers: {
          'Content-Type': 'application/merge-patch+json',
        },
        ...{
          body: body,
        },
      })
      .then((inter) => setIntervention(inter))
      .then(() => refetch());
  };

  return (
    <MyDrawer
      open
      onClose={onClose}
      title={intervention ? `${intervention.recurring ? 'Récurrence' : 'Intervention'} #${intervention?.logicId}` : 'Chargement'}
      tooltip={
        canMutate &&
        intervention && (
          <DropdownMenu>
            <RouteMenuItem intervention={intervention} />
            <SetBlockedMenuItem intervention={intervention} setBlocked={setBlocked} />
            <AssignmentMenuItem assignableAgents={agentsActifs} intervention={intervention} refetch={refetch} />
            <RejectMenuItem intervention={intervention} onSuccess={onReject} />
            <ModifyMenuItem intervention={intervention} onModify={modify} />
            <DeleteMenuItem
              intervention={intervention}
              onSuccess={async () => {
                setIntervention(null);
                refetch && (await refetch());
                if (onClose) onClose();
              }}
            />
            <PrintMenuItem toPrint={ref} />
          </DropdownMenu>
        )
      }
    >
      {!!intervention && (
        <div ref={ref} className="aei-intervention-viewer">
          <InterventionViewerStatus canMutate={canMutate} intervention={intervention} refetch={refetch} setIntervention={setIntervention} />
          {showAfterPicturePicker ? (
            <div className="aei-stack aei-stack-large-padding aei-stack-large-gap">
              <PictureInput
                name="afterpicture"
                onChange={(afterPicture) => {
                  retry();
                  // const pictures = intervention.pictures.filter((it) => it.tag === 'before').map((it) => `api/pictures/${it.id}`);
                  // if (afterPicture) {
                  //     pictures.push(`api/pictures/${afterPicture.id}`);
                  //     updateInterventionHelper(intervention, {
                  //         pictures,
                  //     });
                  // }
                }}
                tag={PictureTag.After}
                intervention={interventionId}
                withCompression
              />
              <button
                className="fr-btn fr-btn--wide fr-btn--lg"
                onClick={() => {
                  if (intervention) {
                    toggleShowAfterPicturePicker();
                  }
                }}
              >
                Suivant
              </button>
              <button
                className="fr-btn fr-btn--secondary fr-btn--wide fr-btn--lg"
                onClick={() => {
                  toggleShowAfterPicturePicker();
                }}
              >
                Annuler
              </button>
            </div>
          ) : (
            <>
              <div>
                <InterventionViewParticipants intervention={intervention} />
                <QuickStatusBadgeIndicator intervention={intervention} />

                <div className="aei-stack aei-stack-large-padding">
                  <div style={{ width: '100%', overflowY: intervention.pictures.length > 1 ? 'scroll' : 'hidden' }}>
                    <div
                      style={{
                        width: intervention.pictures.length > 1 ? 'fit-content' : '100%',
                        display: 'flex',
                        flexDirection: 'row',
                        gap: '1rem',
                        paddingBottom: '1rem',
                      }}
                    >
                      {intervention.pictures.length === 0 && (
                        <div
                          key={`pic-category`}
                          style={{
                            position: 'relative',
                            width: '100vw',
                            height: '100vw',
                            maxHeight: 400,
                            backgroundImage: `url(${intervention.type.picture})`,
                            backgroundSize: 'cover',
                            backgroundPosition: 'center',
                            backgroundRepeat: 'no-repeat',
                            backgroundColor: '#fcfcfc',
                          }}
                        ></div>
                      )}
                      {intervention.pictures
                        .sort((a) => (a.tag === 'before' ? 1 : -1))
                        .map((pic) => (
                          <div
                            key={`pic-${pic.id}`}
                            style={{
                              position: 'relative',
                              width: intervention.pictures.length > 1 ? 'min(30vh,50vw)' : '100vw',
                              height: intervention.pictures.length > 1 ? 'min(30vh,50vw)' : '100vw',
                              maxHeight: 400,
                              backgroundImage: `url(${pic?.url ? pic.url : intervention.type.picture})`,
                              backgroundSize: 'contain',
                              backgroundPosition: 'center',
                              backgroundRepeat: 'no-repeat',
                              backgroundColor: '#fcfcfc',
                            }}
                          >
                            {pic.tag && intervention.pictures.length > 1 && (
                              <Tag
                                style={{
                                  position: 'absolute',
                                  zIndex: 100,
                                  backgroundColor: 'white',
                                  color: 'black',
                                  top: '1rem',
                                  left: '1rem',
                                }}
                              >
                                {t(`model.picture.tag.${pic.tag}`) || '-'}
                              </Tag>
                            )}
                          </div>
                        ))}
                    </div>
                  </div>

                  <h5>{intervention.title || intervention.type.name}</h5>

                  <InterventionViewerProperty
                    value={addressFormatterFormat({
                      street: intervention.location.street || undefined,
                      city: intervention.location.city || undefined,
                      postcode: intervention.location.postcode || undefined,
                    })}
                    icon="fr-icon-map-pin-2-line"
                  />

                  <InterventionViewerProperty
                    value={`Publiée le ${t('date.short', { date: new Date(intervention.createdAt) })} par ${intervention.author.firstname} ${intervention.author.lastname}`}
                    icon="fr-icon-calendar-event-line"
                  />

                  <InterventionViewerProperty icon="fr-icon-draft-line" value={intervention.type.name} />

                  {intervention.recurring && (
                    <Alert
                      severity="info"
                      small
                      style={{ marginTop: '16px', marginRight: '16px' }}
                      description="Dans le Planning, chaque intervention sera visible jusqu'à la date de fin de la récurrence. Dans la page Interventions, chaque
                        intervention sera publiée la veille de sa date de réalisation."
                    />
                  )}
                  <p style={{ whiteSpace: 'pre-line' }}>{intervention.description}</p>

                  <MapContainer
                    center={[parseFloat(intervention.location.latitude), parseFloat(intervention.location.longitude)]}
                    zoom={WHEN_ZOOMED_IN}
                    attributionControl={false}
                    style={{
                      width: '100%',
                      height: '30vh',
                      maxHeight: '300px',
                    }}
                  >
                    <TileLayer {...tileLayerDefaultProps} />
                    <Marker
                      key={intervention.id}
                      position={[parseFloat(intervention.location.latitude), parseFloat(intervention.location.longitude)]}
                      icon={intervention.priority === Priority.Urgent ? redPinIcon : bluePinIcon}
                    />
                  </MapContainer>
                </div>

                <hr className="fr-mt-2w" />

                <InterventionCommentArea currentUserId={user.id} intervention={intervention} retry={retry} />
              </div>
              <InterventionViewerBottomActions
                canMutate={canMutate}
                intervention={intervention}
                updateInterventionHelper={updateInterventionHelper}
                toggleShowAfterPicturePicker={toggleShowAfterPicturePicker}
              />
            </>
          )}
        </div>
      )}
    </MyDrawer>
  );
}
