import Tag from '@codegouvfr/react-dsfr/Tag';
import { useColors } from '@codegouvfr/react-dsfr/useColors';
import { useTranslation } from 'react-i18next';

import { InterventionInterventionview } from '@aei/types/InterventionInterventionview';
import { Status } from '@aei/types/Status';

export interface InterventionStatusChipProps {
  participants: InterventionInterventionview['participants'];
  status: Status;
}

export function InterventionStatusChip(props: InterventionStatusChipProps) {
  const { t } = useTranslation('common');
  const theme = useColors();

  let statusColor: string = theme.options.brownCafeCreme._925_125.default;
  let statusText = t(`model.intervention.status.enum.${props.status}`);

  switch (props.status) {
    case Status.ToDo:
      if (props.participants.length > 0) {
        statusColor = theme.options.greenTilleulVerveine._950_100.default;
      } else {
        statusColor = theme.options.greenTilleulVerveine._950_100.default;
        // statusText = t('model.intervention.status.enum.not-assigned'); // FIXME
        statusText = 'À faire';
      }
      break;
    case Status.InProgress:
      statusColor = theme.options.blueFrance._925_125.default;
      break;
    case Status.Blocked:
      statusColor = theme.options.pinkTuile._950_100.default;
      break;
    case Status.Finished:
      statusColor = theme.options.greenEmeraude._925_125.default;
      break;
  }

  return (
    <Tag className="chip-status" small nativeSpanProps={{ style: { backgroundColor: statusColor } }}>
      {statusText}
    </Tag>
  );
}
