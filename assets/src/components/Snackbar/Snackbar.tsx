import React, { ReactNode, useEffect } from 'react';

interface SnackbarProps {
  message?: string; // Message optionnel si l'on souhaite utiliser children
  open: boolean;
  autoHideDuration?: number; // Durée avant de cacher automatiquement la Snackbar (en ms)
  anchorOrigin?: {
    vertical: 'top' | 'bottom';
    horizontal: 'left' | 'center' | 'right';
  };
  onClose: () => void;
  children?: ReactNode; // Possibilité d'utiliser des enfants pour personnaliser le contenu
}

const Snackbar: React.FC<SnackbarProps> = ({
  message,
  open,
  autoHideDuration = 3000,
  anchorOrigin = { vertical: 'bottom', horizontal: 'center' },
  onClose,
  children,
}) => {
  useEffect(() => {
    if (open) {
      const timer = setTimeout(() => {
        onClose();
      }, autoHideDuration);
      return () => clearTimeout(timer);
    }
  }, [open, autoHideDuration, onClose]);

  if (!open) return null;

  const getPositionStyles = (): React.CSSProperties => {
    const { vertical, horizontal } = anchorOrigin;
    return {
      position: 'fixed',
      [vertical]: '20px',
      [horizontal]: horizontal === 'center' ? '50%' : '20px',
      transform: horizontal === 'center' ? 'translateX(50%)' : 'none',
    };
  };

  return <div style={{ ...styles.snackbar, ...getPositionStyles() }}>{children ? children : message}</div>;
};

const styles: { [key: string]: React.CSSProperties } = {
  snackbar: {
    backgroundColor: 'white',
    padding: '0',
    boxShadow: '0px 4px 6px rgba(0,0,0,0.1)',
    zIndex: 1000,
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    minWidth: 'min(200px, 90vw)',
    maxWidth: 'min(400px, 90vw)',
    position: 'fixed',
    top: '20px',
    left: 'calc(50% - min(400px, 90vw))',
  },
  closeButton: {
    marginLeft: '10px',
    background: 'none',
    border: 'none',
    cursor: 'pointer',
    fontSize: '16px',
  },
};

export default Snackbar;
