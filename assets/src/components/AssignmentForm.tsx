import Checkbox from '@codegouvfr/react-dsfr/Checkbox';
import { useFormContext } from 'react-hook-form';
import { FieldValues } from 'react-hook-form/dist/types';
import { useTranslation } from 'react-i18next';

import { Role } from '@aei/types';
import { UserUserview } from '@aei/types/UserUserview';

export interface AssignmentFormProps {
  agents: UserUserview[];
}

const roles = [Role.Agent, Role.Director, Role.AdminEmployer];

function AssignmentForm<T extends FieldValues & { participantsIds: any[] }>({ agents }: AssignmentFormProps) {
  const {
    register,
    formState: { errors },
  } = useFormContext<T>();
  const { t } = useTranslation('common');

  return roles.map((role) => (
    <Checkbox
      id={`assignment-${role}`}
      stateRelatedMessage={errors.participantsIds?.message?.toString() || ''}
      state={errors.participantsIds && 'error'}
      legend={
        <span
          style={{
            color: '#666',
          }}
        >
          {t(`model.user.roles.${role}`).toUpperCase()}
        </span>
      }
      options={agents
        .filter((it) => it.role === role)
        .map((participant) => ({
          id: `assignment-${participant.id}`,
          label: `${participant.firstname} ${participant.lastname}`,
          // @ts-ignore
          nativeInputProps: { value: participant.id, ...register('participantsIds') },
        }))}
    />
  ));
}

export default AssignmentForm;
