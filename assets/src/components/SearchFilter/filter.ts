export type FilterKey = '_';

export type SelectOption<T extends string = string> = {
  name: string;
  value: T;
  disabled?: boolean;
  hidden?: boolean;
};

export type Category =
  | { multiple: false; id: FilterKey; label: string; options: SelectOption[] }
  | {
      multiple: true;
      id: FilterKey;
      label: string;
      options: { [key in string]: SelectOption[] };
    };
