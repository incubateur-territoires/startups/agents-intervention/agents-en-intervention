import Button from '@codegouvfr/react-dsfr/Button';
import React, { useRef, useState } from 'react';
import { useClickAway } from 'react-use';

import { FilterCategory } from './FilterCategory';
import { Category, FilterKey, SelectOption } from './filter';

export { Category };

const SearchFilter = ({
  category,
  onSelect,
  onUnselect,
  selected,
}: {
  category: Category;
  onSelect: (option: SelectOption, category: FilterKey) => void;
  onUnselect: (option: SelectOption, category: FilterKey) => void;
  selected: Set<string>;
}) => {
  const [open, setOpen] = useState(false);
  const optionsRef = useRef(null);
  useClickAway(optionsRef, () => setOpen(false));

  return (
    <div className="search-filter" ref={optionsRef}>
      <Button
        className={'button' + (open ? 'buttonOpen' : '')}
        priority="tertiary"
        iconId={`fr-icon-arrow-${open ? 'up' : 'down'}-s-line`}
        iconPosition="right"
        onClick={() => setOpen(!open)}
      >
        {category.label}
        {selected.size > 0 && <span className="buttonCount">{selected.size}</span>}
      </Button>
      {open && (
        <div className="options">
          <FilterCategory category={category} onSelect={onSelect} onUnselect={onUnselect} selected={selected} />
        </div>
      )}
    </div>
  );
};

export default SearchFilter;
