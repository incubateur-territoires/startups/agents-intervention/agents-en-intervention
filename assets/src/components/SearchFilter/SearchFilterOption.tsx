import React from 'react';

import { SelectOption } from './filter';

const SearchFilterOption = ({
  option,
  onSelect,
  selected,
}: {
  option: SelectOption;
  selected?: boolean;
  onSelect: (option: SelectOption) => void;
}) => (
  <>
    <button
      key={option.value}
      type="button"
      className="option"
      onClick={() => {
        onSelect(option);
      }}
    >
      <div>{option.name}</div>
      <span className={'fr-icon--sm fr-icon-check-line fr-text-title--blue-france' + (!selected ? 'fr-hidden' : '')} />
    </button>
    <hr className="separator" />
  </>
);

export default SearchFilterOption;
