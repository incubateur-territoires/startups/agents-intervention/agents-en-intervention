import React from 'react';

import AssignmentForm from '@aei/src/components/AssignmentForm';
import { MutateInterventionSchemaType } from '@aei/src/models/actions/intervention';
import { UserUserview } from '@aei/types/UserUserview';

interface InterventionAssignmentProps {
  agents: UserUserview[];
}

const InterventionAssignment = (props: InterventionAssignmentProps) => <AssignmentForm<MutateInterventionSchemaType> agents={props.agents} />;

export default InterventionAssignment;
