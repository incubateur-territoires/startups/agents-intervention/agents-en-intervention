import DSFRInput from '@codegouvfr/react-dsfr/Input';
import { LatLng, LatLngExpression, Map } from 'leaflet';
import 'leaflet/dist/leaflet.css';
import debounce from 'lodash.debounce';
import { PropsWithChildren, useCallback, useEffect, useMemo, useRef, useState } from 'react';
import { useForm } from 'react-hook-form';
import { MapContainer, Marker, TileLayer } from 'react-leaflet';
import { useAsyncFn, useToggle } from 'react-use';

import { FLY_TO_OPTIONS, INITIAL_ZOOM, WHEN_ZOOMED_IN } from '@aei/pages/InterventionMapPage';
import '@aei/src/assets/map/leaflet.scss';
import { InterventionLocationSchemaType } from '@aei/src/models/entities/intervention';
import { addressFormatterFormat } from '@aei/src/utils/address';
import { bluePinIcon, fabButtonSpacing, getCurrentPosition, tileLayerDefaultProps } from '@aei/src/utils/map';
import { search, searchByCoordinates } from '@aei/src/utils/national-address-base';
import useMobileFormat from '@aei/src/utils/useMobileFormat';

import Fab from '../Fab';

export interface MapAddressPickerProps {
  defaultAddress?: InterventionLocationSchemaType;
  defaultPosition?: LatLng;
  error?: string;
  onAddressChanged: (address: InterventionLocationSchemaType | null) => void;
}

const computeInitialCenter = (defaultPosition?: LatLng, defaultAddress?: InterventionLocationSchemaType): LatLng | null => {
  if (defaultPosition) {
    return defaultPosition;
  } else if (defaultAddress) {
    return new LatLng(parseFloat(defaultAddress.latitude), parseFloat(defaultAddress.longitude));
  } else {
    return null;
  }
};

/**
 * Permet de sélectionner un point ou une adresse.
 *
 * Doit permettre de :
 * - se positionner sur l'emplacement de la commune par défaut
 * - récupérer l'adresse depuis la position configurée
 * - changer la position en entrant une nouvelle adresse dans la barre de recherche
 * - la barre de recherche doit faire de l'autocomplétion
 * - le marker doit être déplacable pour sélectionner une nouvelle position
 *    => l'adresse du marker doit être trouvée automatiquement
 *    => le marker doit garder la position que l'utilisateur lui a donné
 * - l'utilisateur doit pouvoir se géolocaliser à une position
 *    => elle deviendra la position sélectionnée
 *    => l'adresse doit être trouvée pour cette position
 *    => le marker doit rester à la position détectée après géocodage
 *
 * @param props PropsWithChildren<MapAddressPickerProps>
 * @returns MapAddressPicker
 */
const MapAddressPicker = (props: PropsWithChildren<MapAddressPickerProps>) => {
  const addressQueryRef = useRef<HTMLInputElement | null>(null);
  const [map, setMap] = useState<Map | null>(null);
  const [showSearchResults, toggleSearchResults] = useToggle(false);
  const mobileFormat = useMobileFormat();

  // Permet de récupérer la position depuis le navigateur de l'utilisateur
  const [getCurrentPositionState, _getCurrentPosition] = useAsyncFn(getCurrentPosition);

  const methods = useForm<{ address: string }>();

  useEffect(() => {
    if (props.defaultAddress) {
      methods.reset({
        address: addressFormatterFormat({
          street: props.defaultAddress.street || undefined,
          city: props.defaultAddress.city,
          postcode: props.defaultAddress.postcode,
          fallback: `${props.defaultAddress.latitude}, ${props.defaultAddress.longitude}`,
        }),
      });
    } else {
      methods.reset({ address: '' });
    }
  }, [methods, props.defaultAddress]);

  /**
   * C'est la position choisie par l'utilisateur
   * ou par le géocodage dans le cas d'une recherche par adresse
   *
   * Il ne doit pas changer en cas de changement de la `defaultAddress`
   * excepté au premier rendering.
   */
  const [position, _setPosition] = useState<LatLng | null>(computeInitialCenter(props.defaultPosition, props.defaultAddress) || null);

  /**
   * Permet de changer la position actuelle
   * Cette fonction peut aussi effectuer le géocodage de l'adresse à partir de cette position
   */
  const setPosition = useCallback(
    (_position: LatLngExpression | null) => {
      if (_position === null) {
        props.onAddressChanged(null);
        return;
      }

      let latlng: LatLng | null = null;
      if (Array.isArray(_position)) {
        latlng = new LatLng(_position[0], _position[1]);
      } else if (_position instanceof LatLng) {
        latlng = _position;
      }

      _setPosition(latlng);
    },
    [props.onAddressChanged]
  );

  useEffect(() => {
    map?.addEventListener('click', (e) => {
      console.log(e.target, map, e.target === map);
      const newPosition = e.latlng as LatLng;
      setPosition(newPosition);
    });
  }, [map, setPosition]);

  /**
   * Modifie la position de la carte lorsque la position change
   */
  useEffect(() => {
    if (position) {
      map?.flyTo(position, WHEN_ZOOMED_IN, FLY_TO_OPTIONS);
      searchByCoordinates(position).then((address) => {
        if (!address) return;
        if (addressQueryRef.current) {
          addressQueryRef.current.value = address ? address.properties.display_name : '';
        }
        props.onAddressChanged(
          address
            ? {
                latitude: position.lat.toString(), // address.geometry.coordinates[1].toString(),
                longitude: position.lng.toString(), // address.geometry.coordinates[0].toString(),
                street: address.properties.name || address.properties.address.street || address.properties.address.hamlet || '',
                rest: '',
                postcode: address.properties.address.postcode,
                city: address.properties.address.city || address.properties.address.town || address.properties.address.municipality || ' ',
                fallback: address.properties.display_name,
              }
            : null
        );
      });
    }
  }, [map, position, props.onAddressChanged]);

  const [{ value: searchAddressQuerySuggestions = [] }, handleSearchAddressQueryChange] = useAsyncFn((event: React.ChangeEvent<HTMLInputElement>) => {
    toggleSearchResults(true);
    return search(event.target.value);
  }, []);

  const debounedHandleClearAddressQuery = useMemo(() => debounce(handleSearchAddressQueryChange, 500), [handleSearchAddressQueryChange]);
  useEffect(() => {
    return () => {
      debounedHandleClearAddressQuery.cancel();
    };
  }, [debounedHandleClearAddressQuery]);

  return (
    <div className="map-address-picker">
      <div style={{ position: 'relative' }}>
        <DSFRInput
          label="Lieu de l'intervention"
          hideLabel={mobileFormat}
          nativeInputProps={methods.register('address', {
            onChange: handleSearchAddressQueryChange,
          })}
        />

        {searchAddressQuerySuggestions && showSearchResults && (
          <div className="search-container">
            {searchAddressQuerySuggestions.map((option) => (
              <button
                className="search-item"
                onClick={() => {
                  setPosition(new LatLng(option.geometry.coordinates[1], option.geometry.coordinates[0]));
                  toggleSearchResults();
                }}
              >
                {addressFormatterFormat({
                  street: option.properties.name || undefined,
                  city: option.properties.address.city || undefined,
                  postcode: option.properties.address.postcode || undefined,
                  fallback: option.properties.display_name,
                })}
              </button>
            ))}
          </div>
        )}
      </div>

      <MapContainer
        center={position || [46.603354, 1.888334]}
        zoom={INITIAL_ZOOM}
        attributionControl={false}
        ref={setMap}
        style={{ flex: 1, width: '100%', minHeight: '50vh' }}
      >
        <TileLayer {...tileLayerDefaultProps} />
        {position && (
          <Marker
            position={position}
            icon={bluePinIcon}
            draggable
            eventHandlers={{
              dragend: (event) => {
                const newPosition = event.target._latlng as LatLng;
                setPosition(newPosition); // Async
              },
            }}
          />
        )}

        <Fab
          disabled={getCurrentPositionState.loading}
          color="secondary"
          size="small"
          onClick={(e) => {
            e.stopPropagation();
            e.preventDefault();
            _getCurrentPosition().then((userGeolocation) => {
              if (userGeolocation) {
                map?.flyTo([userGeolocation.coords.latitude, userGeolocation.coords.longitude], 20, FLY_TO_OPTIONS); // Set a scale to see the city and around
                setPosition([userGeolocation.coords.latitude, userGeolocation.coords.longitude]);
              }
            });
          }}
          aria-label="se géolocaliser"
          label="se géolocaliser"
          right={fabButtonSpacing}
          bottom={fabButtonSpacing}
          loading={getCurrentPositionState.loading}
        >
          <span className="fr-icon-send-plane-line" aria-hidden="true"></span>
        </Fab>
      </MapContainer>
    </div>
  );
};

export default MapAddressPicker;
