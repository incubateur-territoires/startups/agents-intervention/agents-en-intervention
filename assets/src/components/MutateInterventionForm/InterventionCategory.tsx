import Accordion from '@codegouvfr/react-dsfr/Accordion';
import React from 'react';
import { useFormContext } from 'react-hook-form';

import { MutateInterventionSchemaType } from '@aei/src/models/actions/intervention';
import { Category } from '@aei/types/Category';
import { Type } from '@aei/types/Type';

const sortCategories = (a: Category, b: Category) => ((a.position || 0) > (b.position || 0) ? 1 : -1);

interface InterventionCategoryProps {
  categories: Category[];
  types: Type[];
}

const InterventionCategory = (props: InterventionCategoryProps) => {
  const { watch, register, resetField } = useFormContext<MutateInterventionSchemaType>();
  const { typeId } = watch();

  return (
    <>
      {/*{typeId && (<div>*/}
      {/*    <button className="fr-tag fr-tag--dismiss"*/}
      {/*            aria-label={`Retirer ${props.types.find((type) => `${type.id}` === typeId)?.name || ''}`}*/}
      {/*            onClick={() => resetField('typeId')}>{props.types.find((type) => `${type.id}` === typeId)?.name || ''}*/}
      {/*    </button>*/}
      {/*    /!*<Chip sx={{ mb: 4 }} label={props.types.find((type) => `${type.id}` === typeId)?.name || ''}*!/*/}
      {/*    /!*      onDelete={() => resetField('typeId')}/>*!/*/}
      {/*</div>)}*/}

      <fieldset
        className="fr-fieldset"
        id="radio-hint"
        aria-labelledby="radio-hint-legend radio-hint-messages"
        style={{ marginRight: 0, marginLeft: 0 }}
      >
        <legend className="fr-fieldset__legend--regular fr-fieldset__legend" id="radio-hint-legend">
          Catégorie de l'intervention
        </legend>

        {typeId && (
          <div style={{ marginBottom: '2rem' }}>
            <button
              className="fr-tag fr-tag--dismiss"
              aria-label={`Retirer ${props.types.find((type) => `${type.id}` === typeId)?.name || ''}`}
              onClick={() => resetField('typeId')}
            >
              {props.types.find((type) => `${type.id}` === typeId)?.name || ''}
            </button>
          </div>
        )}

        <div className="fr-accordions-group type-select">
          {props.categories.sort(sortCategories).map((category) => (
            <Accordion label={category.name}>
              {props.types
                .filter((t) => t.category.id === category.id)
                .sort(sortCategories)
                .map((type) => (
                  <div key={type.id} className="fr-fieldset__element">
                    <div className="fr-radio-group">
                      <input type="radio" id={`radio-hint-${type.id}`} value={type.id} {...register('typeId')} />
                      <label className="fr-label" htmlFor={`radio-hint-${type.id}`}>
                        {type.name}
                      </label>
                    </div>
                  </div>
                ))}
            </Accordion>
          ))}
        </div>
      </fieldset>
    </>
  );
};
export default InterventionCategory;
