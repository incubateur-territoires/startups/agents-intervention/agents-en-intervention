import { Input } from '@codegouvfr/react-dsfr/Input';
import React from 'react';
import { useFormContext } from 'react-hook-form';

import InterventionType from '@aei/src/components/MutateInterventionForm/InterventionType';
import { MutateInterventionSchemaType } from '@aei/src/models/actions/intervention';
import { useUser } from '@aei/src/utils/auth';
import { Role } from '@aei/types';

/**
 * Permet de récolter les informations de base sur l'intervention
 * - Titre
 * - Description
 * - Type et horaires
 *
 * @constructor
 */
const InterventionDefinition = () => {
  const { hasRole } = useUser();
  const {
    register,
    formState: { errors },
  } = useFormContext<MutateInterventionSchemaType>();

  return (
    <div>
      <Input
        label="Titre de l'intervention"
        state={errors.title ? 'error' : 'default'}
        stateRelatedMessage={errors.title?.message}
        nativeInputProps={register('title')}
      />

      <Input
        label="Description"
        hintText="Facultatif"
        state={errors.description ? 'error' : 'default'}
        stateRelatedMessage={errors.description?.message}
        textArea
        nativeTextAreaProps={{ ...register('description'), style: { resize: 'vertical' } }}
      />

      {(hasRole(Role.Director) || hasRole(Role.AdminEmployer) || hasRole(Role.Admin)) && <InterventionType />}
    </div>
  );
};

export default InterventionDefinition;
