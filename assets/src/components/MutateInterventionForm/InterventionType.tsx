import { useForm, useFormContext } from 'react-hook-form';

import OnetimeType from '@aei/src/components/MutateInterventionForm/InterventionType/OnetimeType';
import RecurrenceType from '@aei/src/components/MutateInterventionForm/InterventionType/RecurrenceType';
import { MutateInterventionSchemaType } from '@aei/src/models/actions/intervention';
import { Frequency } from '@aei/types';

interface InterventionTypeForm {
  intervention_type: 'onetime' | 'recurring';
}

const InterventionType = () => {
  const { watch } = useFormContext<MutateInterventionSchemaType>();
  const frequency = watch('frequency');
  const methods = useForm<InterventionTypeForm>({
    defaultValues: {
      intervention_type: (frequency || Frequency.ONE_TIME) === 'onetime' ? 'onetime' : 'recurring',
    },
  });

  const { intervention_type } = methods.watch();

  return (
    <fieldset className="fr-fieldset" id="radio-hint" aria-labelledby="radio-hint-legend radio-hint-messages">
      <legend className="fr-fieldset__legend--regular fr-fieldset__legend" id="radio-hint-legend">
        Cette intervention est :
      </legend>

      <div className="fr-fieldset__element">
        <div className="fr-radio-group">
          <input type="radio" id="radio-hint-1" value="onetime" {...methods.register('intervention_type')} />
          <label className="fr-label" htmlFor="radio-hint-1">
            Ponctuelle
          </label>
        </div>
      </div>

      {intervention_type === 'onetime' && <OnetimeType />}

      <div className="fr-fieldset__element">
        <div className="fr-radio-group">
          <input type="radio" id="radio-hint-2" value="recurring" {...methods.register('intervention_type')} />
          <label className="fr-label" htmlFor="radio-hint-2">
            Récurrente
          </label>
        </div>
      </div>

      {intervention_type === 'recurring' && <RecurrenceType />}
    </fieldset>
  );
};

export default InterventionType;
