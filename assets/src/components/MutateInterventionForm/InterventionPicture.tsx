import { useFormContext } from 'react-hook-form';

import { PictureInput } from '@aei/src/components/form';
import { MutateInterventionSchemaType } from '@aei/src/models/actions/intervention';
import { InterventionInterventionview, PictureTag } from '@aei/types';

export interface InterventionPictureProps {
  intervention?: InterventionInterventionview;
}

const InterventionPicture = ({ intervention }: InterventionPictureProps) => {
  const { watch, setValue } = useFormContext<MutateInterventionSchemaType>();
  const { beforePicture } = watch();

  return (
    <div style={{ maxWidth: '100%' }}>
      <PictureInput
        name="beforePicture"
        onChange={(picture) => setValue('beforePicture', picture || null)}
        withCompression
        tag={PictureTag.Before}
        value={beforePicture || undefined}
        intervention={intervention?.id}
      />
    </div>
  );
};

export default InterventionPicture;
