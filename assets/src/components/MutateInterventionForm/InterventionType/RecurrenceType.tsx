import { Input } from '@codegouvfr/react-dsfr/Input';
import { Select } from '@codegouvfr/react-dsfr/Select';
import { useEffect } from 'react';
import { useForm, useFormContext } from 'react-hook-form';
import { useTranslation } from 'react-i18next';

import { MutateInterventionSchemaType } from '@aei/src/models/actions/intervention';
import { getWeekOfMonth } from '@aei/src/models/getWeekOfMonth';
import { Frequency } from '@aei/types';

const startDefault = new Date();
startDefault.setDate(startDefault.getDate() + 1);
startDefault.setHours(8);
startDefault.setMinutes(30);
startDefault.setSeconds(0);

const endDefault = new Date(startDefault);
endDefault.setHours(10);
endDefault.setMinutes(0);

export const computeRecurring = (frequency = Frequency.ONE_TIME, frequencyInterval = 1) => {
  if (frequency === Frequency.DAILY) {
    return 1;
  }

  if (frequency === Frequency.WEEKLY) {
    if (frequencyInterval === 1) {
      return 2;
    }
    if (frequencyInterval === 2) {
      return 3;
    }
  }

  if (frequency === Frequency.MONTHLY) {
    if (frequencyInterval === 1) {
      return 4;
    }
    if (frequencyInterval === 3) {
      return 5;
    }
    if (frequencyInterval === 6) {
      return 6;
    }
  }

  if (frequency === Frequency.YEARLY) {
    if (frequencyInterval === 1) {
      return 7;
    }
  }

  return 0;
};

interface RecurringInterventionTypeForm {
  recurring: number;
  date: string;
  hour_start: string;
  hour_end: string;
}

const RecurrenceType = () => {
  const { t } = useTranslation('common');
  const { setValue, watch, register, formState } = useFormContext<MutateInterventionSchemaType>();
  const { startAt, endAt, endedAt, frequency, frequencyInterval } = watch();

  const methods = useForm<RecurringInterventionTypeForm>({
    defaultValues: {
      recurring: computeRecurring(frequency, frequencyInterval),
      date: (startAt || startDefault)?.toLocaleDateString('fr').split('/').reverse().join('-'),
      hour_start: (startAt || startDefault)?.toLocaleTimeString('en-GB'),
      hour_end: (endAt || endDefault)?.toLocaleTimeString('en-GB'),
    },
  });
  const { recurring, hour_start, date, hour_end } = methods.watch();

  const weekday = startAt ? new Intl.DateTimeFormat('fr-FR', { weekday: 'long' }).format(new Date(startAt)) : '-';
  const weekOfMonth = startAt ? getWeekOfMonth(new Date(startAt)) : 0;

  useEffect(() => {
    if (date && !endedAt) {
      const d = new Date(date);
      d.setFullYear(d.getFullYear() + 1);
      setValue('endedAt', d);
    }
    console.log(formState);
  }, [date]);

  useEffect(() => {
    if (date && hour_start) {
      setValue('startAt', new Date(`${date}T${hour_start}`));
    }
  }, [date, hour_start, setValue]);

  useEffect(() => {
    if (date && hour_end) {
      setValue('endAt', new Date(`${date}T${hour_end}`));
    }
  }, [date, hour_end, setValue]);

  useEffect(() => {
    const data: Partial<MutateInterventionSchemaType> = {
      frequency: Frequency.DAILY,
      frequencyInterval: 1,
    };
    switch (parseInt(String(recurring))) {
      case 1:
        console.log('daily, frequency 1');
        data.frequency = Frequency.DAILY;
        data.frequencyInterval = 1;
        break;
      case 2:
        console.log('weekly, frequency 1');
        data.frequency = Frequency.WEEKLY;
        data.frequencyInterval = 1;
        break;
      case 3:
        console.log('weekly, frequency 2');
        data.frequency = Frequency.WEEKLY;
        data.frequencyInterval = 2;
        break;
      case 4:
        console.log('monthly, frequency 1');
        data.frequency = Frequency.MONTHLY;
        data.frequencyInterval = 1;
        break;
      case 5:
        console.log('monthly, frequency 3');
        data.frequency = Frequency.MONTHLY;
        data.frequencyInterval = 3;
        break;
      case 6:
        console.log('monthly, frequency 6');
        data.frequency = Frequency.MONTHLY;
        data.frequencyInterval = 6;
        break;
      case 7:
        console.log('yearly, frequency 7');
        data.frequency = Frequency.YEARLY;
        data.frequencyInterval = 1;
        break;
      default:
        data.frequency = Frequency.DAILY;
        data.frequencyInterval = 1;
        methods.setValue('recurring', computeRecurring(data.frequency, data.frequencyInterval));
    }
    setValue('frequency', data.frequency);
    setValue('frequencyInterval', data.frequencyInterval || 1);
  }, [recurring, setValue]);

  return (
    <div className="aei-stack">
      <Input
        label="Date de la première intervention :"
        nativeInputProps={{
          'aria-label': 'Date de la première intervention',
          type: 'date',
          ...methods.register('date'),
        }}
      />

      <div className="aei-stack aei-stack-columns aei-stack-large-gap aei-stack-autogrow fr-p-0">
        <Input
          label="Début"
          nativeInputProps={{
            ...methods.register('hour_start'),
            type: 'time',
            'aria-label': 'Heure de début',
            list: 'start_times',
          }}
        />
        <datalist id="start_times">
          {Array(24 * 4)
            .fill(1)
            .map((val, index) => (
              <option
                key={`rec_start_times_${index}`}
                value={`${Math.trunc(index / 4)}`.padStart(2, '0') + ':' + `${(index % 4) * 15}`.padStart(2, '0') + ':00'}
              />
            ))}
        </datalist>

        <Input
          label="Fin"
          nativeInputProps={{
            ...methods.register('hour_end'),
            type: 'time',
            'aria-label': 'Heure de fin',
            list: 'end_times',
          }}
        />
        <datalist id="end_times">
          {Array(24 * 4)
            .fill(1)
            .map((val, index) => (
              <option
                key={`rec_end_times_${index}`}
                value={`${Math.trunc(index / 4)}`.padStart(2, '0') + ':' + `${(index % 4) * 15}`.padStart(2, '0') + ':00'}
              />
            ))}
        </datalist>
      </div>
      <br />
      <Select label="Récurrence" nativeSelectProps={methods.register('recurring')}>
        <option value={1}>Tous les jours</option>
        <option value={2}>Toutes les semaines, le {weekday}</option>
        <option value={3}>Toutes les deux semaines, le {weekday}</option>
        <option value={4}>
          Tous les mois, le {weekOfMonth}
          {weekOfMonth === 1 ? 'er' : 'ème'} {weekday}
        </option>
        <option value={5}>
          Tous les 3 mois, le {weekOfMonth}
          {weekOfMonth === 1 ? 'er' : 'ème'} {weekday}
        </option>
        <option value={6}>
          Tous les 6 mois, le {weekOfMonth}
          {weekOfMonth === 1 ? 'er' : 'ème'} {weekday}
        </option>
        <option value={7}>Tous les ans, le {t('date.dayMonth', { date: new Date(startAt || date || 'now') })}</option>
      </Select>

      <Input
        label="Date de fin de la récurrence"
        nativeInputProps={{
          'aria-label': 'Date de fin de la récurrence',
          type: 'date',
          ...register('endedAt'),
          value: new Date(endedAt || 'now')?.toLocaleDateString('fr').split('/').reverse().join('-'),
          onChange: (event) => setValue('endedAt', new Date(event.target.value)),
        }}
      />

      <div className="fr-alert fr-alert--info fr-alert--sm">
        <p>
          Dans le Planning, chaque intervention sera visible jusqu'à la date de fin de la récurrence. Dans la page Interventions, chaque intervention
          sera publiée la veille de sa date de réalisation.
        </p>
      </div>
    </div>
  );
};

export default RecurrenceType;
