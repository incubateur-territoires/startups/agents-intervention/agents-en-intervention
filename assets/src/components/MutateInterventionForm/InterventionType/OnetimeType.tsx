import { Checkbox as DSFRCheckbox } from '@codegouvfr/react-dsfr/Checkbox';
import { Input } from '@codegouvfr/react-dsfr/Input';
import { useEffect } from 'react';
import { useForm, useFormContext } from 'react-hook-form';

import { MutateInterventionSchemaType } from '@aei/src/models/actions/intervention';
import { Frequency } from '@aei/types';

interface InterventionTypeForm {
  date: string;
  hour_start: string;
  hour_end: string;
}

const OnetimeType = () => {
  const { setValue, watch, register } = useFormContext<MutateInterventionSchemaType>();
  const { endAt, priority, startAt } = watch();

  // @ts-ignore
  const _priority = priority === true || priority === 'true';

  const methods = useForm<InterventionTypeForm>({
    defaultValues: {
      date: (startAt || new Date())?.toLocaleDateString('fr').split('/').reverse().join('-'),
      hour_start: startAt?.toLocaleTimeString('en-GB'),
      hour_end: endAt?.toLocaleTimeString('en-GB'),
    },
  });

  const { hour_start, date, hour_end } = methods.watch();

  useEffect(() => {
    if (date && hour_start) {
      setValue('startAt', new Date(`${date}T${hour_start}`));
    } else {
      setValue('startAt', null);
    }
  }, [date, hour_start, setValue]);

  useEffect(() => {
    if (date && hour_end) {
      setValue('endAt', new Date(`${date}T${hour_end}`));
    } else {
      setValue('endAt', null);
    }
  }, [date, hour_end, setValue]);

  useEffect(() => {
    setValue('frequency', Frequency.ONE_TIME);
    setValue('frequencyInterval', 1);
  }, [setValue]);

  return (
    <div style={{ display: 'flex', flexDirection: 'column', gap: '.5rem', padding: '.5rem' }}>
      <Input
        label=""
        hintText="Date à laquelle l’intervention est prévue (facultatif)."
        nativeInputProps={{
          'aria-label': 'Date de début',
          type: 'date',
          ...methods.register('date'),
        }}
        disabled={_priority}
      />

      <DSFRCheckbox
        options={[
          {
            hintText: 'Si l’intervention doit être réalisée dès que possible.',
            label: 'Intervention urgente',
            nativeInputProps: { ...register('priority'), value: 'true' },
          },
        ]}
      />

      <div className="aei-stack aei-stack-columns aei-stack-large-gap aei-stack-autogrow fr-p-0">
        <Input
          label="Début"
          nativeInputProps={{
            ...methods.register('hour_start'),
            type: 'time',
            'aria-label': 'Heure de début',
            list: 'start_times',
          }}
          disabled={_priority}
        />
        <datalist id="start_times">
          {Array(24 * 4)
            .fill(1)
            .map((val, index) => (
              <option
                key={`onetime_start_times_${index}`}
                value={`${Math.trunc(index / 4)}`.padStart(2, '0') + ':' + `${(index % 4) * 15}`.padStart(2, '0') + ':00'}
              />
            ))}
        </datalist>

        <Input
          label="Fin"
          nativeInputProps={{
            ...methods.register('hour_end'),
            type: 'time',
            'aria-label': 'Heure de fin',
            list: 'end_times',
          }}
          disabled={_priority}
        />
        <datalist id="end_times">
          {Array(24 * 4)
            .fill(1)
            .map((val, index) => (
              <option
                key={`onetime_end_times_${index}`}
                value={`${Math.trunc(index / 4)}`.padStart(2, '0') + ':' + `${(index % 4) * 15}`.padStart(2, '0') + ':00'}
              />
            ))}
        </datalist>
      </div>

      <div className="fr-alert fr-alert--info fr-alert--sm">
        <p>La durée d’intervention sera visible sur la page de l’intervention ainsi que sur le planning.</p>
      </div>
    </div>
  );
};

export default OnetimeType;
