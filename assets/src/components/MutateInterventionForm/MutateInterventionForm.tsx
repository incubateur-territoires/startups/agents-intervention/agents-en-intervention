import classNames from 'classnames';
import { LatLng } from 'leaflet';
import React, { useCallback, useEffect, useMemo } from 'react';
import { FormProvider, Resolver, useForm } from 'react-hook-form';
import { useAsyncFn } from 'react-use';
import z from 'zod';

import { LoadingButton } from '@aei/src/components';
import InterventionAssignment from '@aei/src/components/MutateInterventionForm/InterventionAssignment';
import InterventionCategory from '@aei/src/components/MutateInterventionForm/InterventionCategory';
import InterventionDefinition from '@aei/src/components/MutateInterventionForm/InterventionDefinition';
import InterventionPicture from '@aei/src/components/MutateInterventionForm/InterventionPicture';
import MapAddressPicker from '@aei/src/components/MutateInterventionForm/MapAddressPicker';
import { MutateInterventionPrefillSchemaType, MutateInterventionSchemaType } from '@aei/src/models/actions/intervention';
import { InterventionLocationSchemaType, InterventionPictureSchema, InterventionSchema } from '@aei/src/models/entities/intervention';
import { addressFormatterFormat } from '@aei/src/utils/address';
import useMobileFormat from '@aei/src/utils/useMobileFormat';
import { Category, EmployerEmployerview, Frequency, InterventionInterventionview, Role, Type, UserUserview } from '@aei/types';

import { usePrivateLayoutContext } from '../../app/(private)/PrivateLayoutProvider';
import { useUser } from '../../utils/auth';

export enum Section {
  Title = 'title',
  Location = 'location',
  Type = 'type',
  Photo = 'photo',
  Assignment = 'assignment',
}

const validations = {
  [Section.Title]: z.object({
    title: z.string().min(1),
    description: z.string(),

    // choix du type d'intervention
    frequency: z.nativeEnum(Frequency),

    // pour les interventions ponctuelles
    startAt: z.coerce.date().optional(),
    endAt: z.coerce.date().optional(),
    priority: z.coerce.boolean().optional(),

    // pour les interventions récurrentes
    recStartAt: z.coerce.date().optional(),
    recEndAt: z.coerce.date().optional(),
    frequencyInterval: z.number(),
  }),
  [Section.Location]: z.object({ location: InterventionSchema.shape.location }),
  [Section.Type]: z.object({ typeId: z.string().min(1) }),
  [Section.Photo]: z.object({
    beforePicture: z.nullable(InterventionPictureSchema),
    afterPicture: z.nullable(InterventionPictureSchema),
  }),
  [Section.Assignment]: z.object({ participantsIds: z.array(z.string()).min(0) }),
};

export interface MutateInterventionFormProps {
  agents: UserUserview[];
  categories: Category[];
  forceSection?: Section;
  intervention?: InterventionInterventionview;
  employer?: EmployerEmployerview;
  onSuccess: (input: MutateInterventionPrefillSchemaType) => Promise<void>;
  prefill?: MutateInterventionPrefillSchemaType;
  types: Type[];
}

type ResolverContext = { step: Section; submitter: string };

// @ts-ignore
const resolver: Resolver<MutateInterventionSchemaType> = async (data, context) => {
  if (!context)
    return {
      values: data,
      errors: {},
    };

  const ctx: ResolverContext = (context as any).current;

  return validations[ctx.step]
    .safeParseAsync(data)
    .then(() => ({ values: data, errors: {} }))
    .catch((e) => {
      console.error(e);
      return {
        values: {},
        errors: { step2: { type: 'required', message: 'Step 2 is required' } },
      };
    });
};

export function MutateInterventionForm(props: MutateInterventionFormProps) {
  const { hasRole } = useUser();

  const sectionFlow: Section[] = useMemo(() => {
    const values = Object.values(Section);
    if (!hasRole(Role.AdminEmployer) && !hasRole(Role.Director) && !hasRole(Role.Admin)) {
      return values.filter((val) => val !== Section.Assignment);
    }
    return values;
  }, [hasRole]);

  const [step, setStep] = React.useState(Section.Title);
  useEffect(() => {
    return () => setStep(Section.Title);
  }, []);
  const context = React.useRef({ step, submitter: '' });
  const mobileFormat = useMobileFormat();

  const displayedSection = context.current.step;

  const nextSection = useMemo(() => {
    const currentIndex = sectionFlow.indexOf(displayedSection);
    return sectionFlow[currentIndex + 1] || null;
  }, [displayedSection, sectionFlow]);

  const prevSection = useMemo(() => {
    const currentIndex = sectionFlow.indexOf(displayedSection);

    return sectionFlow[currentIndex - 1] || null;
  }, [displayedSection, sectionFlow]);

  const methods = useForm<MutateInterventionSchemaType>({
    shouldUnregister: false,
    context,
    resolver,
    mode: 'onChange',
    defaultValues: {
      // @ts-ignore
      beforePicture: null, // @ts-ignore
      afterPicture: null,
      participantsIds: [],
      priority: false, // @ts-ignore
      startAt: props.prefill?.startAt?.toISOString().substring(0, 16), // @ts-ignore
      endAt: props.prefill?.endAt?.toISOString().substring(0, 16),
      endedAt: props.prefill?.endedAt || undefined,
      frequency: Frequency.ONE_TIME,
      frequencyInterval: 1,
      ...props.prefill,
    },
    reValidateMode: 'onChange',
  });
  const {
    handleSubmit, // Garder isValidating pour avoir le re-rendu du composant à la validation
    formState: { errors },
    setValue,
    control,
    watch,
  } = methods;

  const { mainTitle, useSetMainTitle } = usePrivateLayoutContext();
  useSetMainTitle(
    mobileFormat && control._formValues.location
      ? addressFormatterFormat({
          street: control._formValues.location.street || undefined,
          city: control._formValues.location.city || undefined,
          postcode: control._formValues.location.postcode || undefined,
        })
      : mainTitle
  );

  if (hasRole(Role.Agent) || hasRole(Role.Elected)) {
    watch();
  }

  const [onSuccessState, onSuccess] = useAsyncFn(props.onSuccess, []);

  const onSubmit = (e: React.FormEvent) => {
    const submitter = context.current.submitter;
    const doHandleSubmit = handleSubmit(
      // onValid callback
      (data) => {
        const currentIndex = sectionFlow.indexOf(displayedSection);
        const prevSection = sectionFlow[currentIndex - 1] || null;
        const nextSection = sectionFlow[currentIndex + 1] || null;
        const newStep = submitter === 'prev' ? prevSection : nextSection;

        if (!nextSection && submitter !== 'prev') {
          return onSuccess(data);
        } else {
          setStep(newStep);
          context.current.step = newStep;
        }
      },

      // onInvalid callback
      () => {}
    );
    doHandleSubmit(e).catch((e) => {
      console.error(e);
    });
  };

  const onAddressChanged = useCallback(
    (address: InterventionLocationSchemaType | null) => {
      setValue('location', address || (undefined as any), { shouldValidate: true });
    },
    [setValue]
  );

  const defaultPosition = useMemo(() => {
    if (control._formValues.location) {
      return new LatLng(parseFloat(control._formValues.location.latitude), parseFloat(control._formValues.location.longitude));
    }
    return props.employer ? new LatLng(parseFloat(props.employer.latitude), parseFloat(props.employer.longitude)) : undefined;
  }, [control._formValues.location, props.employer]);

  try {
    console.info(validations[context.current.step].parse(control._formValues));
  } catch (e) {
    console.error(e);
  }

  return (
    <div className="aei-stack aei-stack-large-padding">
      <FormProvider {...methods}>
        <form onSubmit={onSubmit} className="mutate-intervention-form">
          <>
            {!mobileFormat && prevSection && (
              <button
                className="fr-btn fr-btn--icon-left fr-btn--tertiary fr-btn--tertiary-no-outline fr-icon-arrow-left-line"
                onClick={() => (context.current.submitter = 'prev')}
                type="submit"
              >
                Retour
              </button>
            )}
            {displayedSection === Section.Photo && mobileFormat && <div style={{ flex: 1 }} />}

            {displayedSection === Section.Title && <InterventionDefinition />}
            {displayedSection === Section.Location && (
              <MapAddressPicker
                defaultAddress={control._formValues.location || undefined}
                defaultPosition={defaultPosition}
                error={errors?.location?.message}
                onAddressChanged={onAddressChanged}
              />
            )}
            {displayedSection === Section.Photo && <InterventionPicture intervention={props.intervention} />}
            {displayedSection === Section.Type && <InterventionCategory categories={props.categories} types={props.types} />}
            {displayedSection === Section.Assignment && <InterventionAssignment agents={props.agents} />}

            {mobileFormat && <div style={{ flex: 1 }} />}
          </>
          <div
            className={classNames(
              'aei-stack aei-stack-large-gap aei-stack-no-padding',
              displayedSection === Section.Type && 'mutate-intervention-form-stick-submit'
            )}
          >
            <LoadingButton
              loading={onSuccessState.loading}
              className="fr-btn fr-btn--lg fr-btn--wide"
              onClick={() => (context.current.submitter = 'next')}
              type="submit"
              disabled={!validations[context.current.step].safeParse(control._formValues).success}
            >
              {nextSection
                ? 'Suivant'
                : hasRole(Role.AdminEmployer) || hasRole(Role.Director) || hasRole(Role.Admin)
                  ? 'Publier'
                  : 'Envoyer ma demande'}
            </LoadingButton>

            {prevSection && mobileFormat && (
              <LoadingButton
                disabled={onSuccessState.loading}
                className="fr-btn fr-btn--lg fr-btn--wide fr-btn--secondary"
                onClick={() => (context.current.submitter = 'prev')}
                type="submit"
              >
                Retour
              </LoadingButton>
            )}
          </div>
        </form>
      </FormProvider>
    </div>
  );
}
