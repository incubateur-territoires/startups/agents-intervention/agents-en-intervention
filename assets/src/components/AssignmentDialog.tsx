import { zodResolver } from '@hookform/resolvers/zod';
import { useEffect, useRef } from 'react';
import { FormProvider, useForm } from 'react-hook-form';

import { useApiInterventionsIdPatch } from '@aei/src/client/generated/components';
import AssignmentForm from '@aei/src/components/AssignmentForm';
import { BaseForm } from '@aei/src/components/BaseForm';
import { AssignInterventionParticipantsSchema, AssignInterventionParticipantsSchemaType } from '@aei/src/models/actions/intervention';
import Modal from '@aei/src/ui/modal/Modal';
import { InterventionInterventionview } from '@aei/types/InterventionInterventionview';
import { UserUserview } from '@aei/types/UserUserview';

export interface AssignmentDialogProps {
  intervention: InterventionInterventionview;
  agents: UserUserview[];
  prefill?: AssignInterventionParticipantsSchemaType;
  open: boolean;
  onClose: () => void;
  onChange?: (intervention: InterventionInterventionview) => void;
}

export const AssignmentDialog = (props: AssignmentDialogProps) => {
  const formRef = useRef<HTMLFormElement | null>(null);
  const updateInteventionParticipants = useApiInterventionsIdPatch();

  const methods = useForm<AssignInterventionParticipantsSchemaType>({
    resolver: zodResolver(AssignInterventionParticipantsSchema),
    defaultValues: {
      participantsIds: [],
      ...props.prefill,
    },
  });
  const { handleSubmit, setValue, control, watch } = methods;

  const values = watch();

  useEffect(() => {
    setValue(
      'participantsIds',
      props.intervention.participants.map((p) => `${p.id}`)
    );
  }, [setValue, props.intervention.participants]);

  const onSubmit = async (input: AssignInterventionParticipantsSchemaType) =>
    updateInteventionParticipants
      .mutateAsync({
        pathParams: {
          id: `${props.intervention.id}`,
        },
        headers: {
          'Content-Type': 'application/merge-patch+json',
        },
        ...{
          body: {
            participants: input.participantsIds.map((pId) => `api/users/${pId}`),
          } as any, // Needed due to missing types (ref: https://github.com/fabien0102/openapi-codegen/issues/198)
        },
      })
      .then((intervention) => {
        if (props.onChange) {
          // @ts-ignore
          props.onChange(intervention);
        }
        props.onClose();
      });

  return (
    <Modal
      title="Assigner cette intervention"
      iconId="fr-icon-arrow-right-line"
      open
      primaryButtonText="Confirmer"
      onConfirm={async () => formRef.current?.requestSubmit()}
      onCancel={async () => props.onClose()}
      onClose={props.onClose}
    >
      <FormProvider {...methods}>
        <BaseForm handleSubmit={handleSubmit} onSubmit={onSubmit} control={control} innerRef={formRef} ariaLabel="assigner des agents">
          {props.agents.length > 0 ? (
            <>
              <p>L&apos;intervention sera ajoutée dans la liste d&apos;interventions du ou des agent(s) séléctionné(s).</p>
              <AssignmentForm<AssignInterventionParticipantsSchemaType> agents={props.agents} />
            </>
          ) : (
            <p>Vous n&apos;avez actuellement aucun agent dans votre collectivité.</p>
          )}
        </BaseForm>
      </FormProvider>
    </Modal>
  );
};
