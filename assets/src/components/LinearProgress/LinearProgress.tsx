import './_LinearProgress.scss';

const LinearProgress = () => {
  return (
    <div className="linear-progress">
      <span className="linear-progress--bar1"></span>
      <span className="linear-progress--bar2"></span>
    </div>
  );
};

export default LinearProgress;
