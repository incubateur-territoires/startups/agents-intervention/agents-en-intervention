import Button from '@codegouvfr/react-dsfr/Button';
import { Input } from '@codegouvfr/react-dsfr/Input';
import { zodResolver } from '@hookform/resolvers/zod';
import { useQueryClient } from '@tanstack/react-query';
import { useMemo } from 'react';
import { useForm } from 'react-hook-form';
import { useTranslation } from 'react-i18next';

import { useApiCommentsPost } from '@aei/src/client/generated/components';
import { BaseForm } from '@aei/src/components/BaseForm';
import { Comment, CommentMessage } from '@aei/src/components/Comment';
import { AddInterventionCommentSchema, AddInterventionCommentSchemaType } from '@aei/src/models/actions/intervention';
import { InterventionInterventionview, UserUserview } from '@aei/types';

export interface InterventionCommentAreaProps {
  currentUserId: number;
  intervention: InterventionInterventionview;
  retry: () => void;
}

export interface CommentGroup {
  author: UserUserview;
  messages: CommentMessage[];
}

export function InterventionCommentArea(props: Readonly<InterventionCommentAreaProps>) {
  const { t } = useTranslation('common');
  const queryClient = useQueryClient();

  const sortedComments = useMemo(() => {
    return props.intervention.comments.sort((a, b) => new Date(a.createdAt).getTime() - new Date(b.createdAt).getTime());
  }, [props.intervention]);

  const groupedComments = useMemo(() => {
    const grouped: { [key: string]: CommentGroup[] } = {};

    sortedComments.forEach(
      (message) => {
        const dateStr = new Date(message.createdAt).toISOString().split('T')[0];
        if (!grouped[dateStr]) {
          grouped[dateStr] = [];
        }

        const lastMessageGroup = grouped[dateStr][grouped[dateStr].length - 1];

        if (lastMessageGroup && lastMessageGroup.author.id === message.author.id) {
          lastMessageGroup.messages.push({
            content: message.message,
            date: message.createdAt,
          });
        } else {
          grouped[dateStr].push({
            author: message.author,
            messages: [
              {
                content: message.message,
                date: message.createdAt,
              },
            ],
          });
        }
      },
      [sortedComments]
    );

    return Object.entries(grouped).map(([date, messageGroups]) => ({ date, messageGroups }));
  }, [sortedComments]);

  const addComment = useApiCommentsPost({
    onSuccess: () => {
      // Needed to refetch manually to get the new comment from top parent component
      props.retry && props.retry();
      return queryClient.refetchQueries({ stale: true });
    },
  });

  const {
    register,
    handleSubmit,
    formState: { errors, isSubmitting },
    control,
    reset,
  } = useForm<AddInterventionCommentSchemaType>({
    resolver: zodResolver(AddInterventionCommentSchema),
  });

  const onSubmit = async (input: AddInterventionCommentSchemaType) =>
    addComment
      .mutateAsync({
        body: {
          intervention: `api/interventions/${props.intervention.id}`,
          message: input.content,
        },
      })
      .then(() => reset());

  return (
    <div className="aei-stack aei-stack-large-padding fr-pt-0">
      <h6>Commentaires</h6>

      {groupedComments.map(({ date, messageGroups }) => (
        <div key={date}>
          <div
            style={{
              textAlign: 'center',
              paddingTop: '.5rem',
              paddingBottom: '1rem',
            }}
          >
            <p className="fr-tag">{t('date.ago', { date: new Date(date) })}</p>
          </div>
          {messageGroups.map((group) => (
            <Comment
              key={`${date}_${group.messages.map((m) => m.date).join('_')}_`}
              author={group.author}
              messages={group.messages}
              side={group.author.id === props.currentUserId ? 'right' : 'left'}
            />
          ))}
        </div>
      ))}
      <BaseForm handleSubmit={handleSubmit} onSubmit={onSubmit} className="fr-no-print" control={control} ariaLabel="ajouter un commentaire">
        <Input
          label="Écrire un commentaire"
          hideLabel
          disabled={isSubmitting}
          textArea
          nativeTextAreaProps={{ ...register('content'), placeholder: 'Écrire un message' }}
          state={errors.content ? 'error' : 'default'}
          stateRelatedMessage={errors.content?.message}
          addon={
            <Button
              disabled={isSubmitting}
              style={{
                position: 'absolute',
                borderRadius: '5px',
                rotate: '46deg',
                background: 'none',
                color: '#000091',
                right: 0,
              }}
              iconId="fr-icon-send-plane-fill"
              type="submit"
              title="Envoyer le commentaire"
            />
          }
        />
      </BaseForm>
    </div>
  );
}
