import React from 'react';

interface DropdownMenuItemProps {
  children: React.ReactNode;
  onClick: () => void;
}

const DropdownMenuItem = ({ children, onClick }: DropdownMenuItemProps) => {
  return (
    <div
      className="dropdown-item"
      onClick={() => {
        onClick();
      }}
      onKeyDown={() => {
        onClick();
      }}
    >
      {children}
    </div>
  );
};

export default DropdownMenuItem;
