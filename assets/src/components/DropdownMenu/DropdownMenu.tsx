import Button from '@codegouvfr/react-dsfr/Button';
import React, { MouseEventHandler, ReactElement, cloneElement, useEffect, useRef, useState } from 'react';
import { createPortal } from 'react-dom';

interface DropdownMenuProps {
  /**
   * Positionnement du menu par rapport au bouton
   */
  anchorOrigin?: { vertical: 'top' | 'bottom'; horizontal: 'left' | 'right' | 'center' };

  /**
   * Bouton personnalisé pour ouvrir le menu
   */
  button?: ReactElement;

  /**
   * Contenu du menu
   */
  children: React.ReactNode;

  /**
   * Classes CSS à ajouter au menu
   */
  dropdownClasses?: string;

  /**
   * Garder ouvert après un clic sur un élément du menu
   */
  keepOpen?: boolean;

  /**
   * Cache la flèche qui pointe vers le bouton
   */
  noNotch?: boolean;
}

const DropdownMenu: React.FC<DropdownMenuProps> = ({
  anchorOrigin = { vertical: 'bottom', horizontal: 'left' },
  button,
  children,
  dropdownClasses = '',
  keepOpen = false,
  noNotch = false,
}) => {
  const [isOpen, setIsOpen] = useState(false);
  const [position, setPosition] = useState<{ top: number; left: number }>({ top: 0, left: 0 });
  const buttonRef = useRef<HTMLButtonElement | null>(null);
  const dropdownRef = useRef<HTMLDivElement | null>(null);

  const toggleDropdown: MouseEventHandler<HTMLElement> = (e) => {
    e.preventDefault();
    e.stopPropagation();
    if (buttonRef.current) {
      const buttonRect = buttonRef.current.getBoundingClientRect();
      const viewportWidth = window.innerWidth;
      const viewportHeight = window.innerHeight;
      let left = buttonRect.left + window.scrollX - 180;
      let top = buttonRect.bottom + window.scrollY;

      // Utilisation de anchorOrigin pour ajuster le positionnement
      if (anchorOrigin.vertical === 'top') {
        top = buttonRect.top + window.scrollY - (dropdownRef.current?.offsetHeight ?? 200); // Positionner en haut du bouton
      }

      if (anchorOrigin.horizontal === 'right') {
        left = buttonRect.right + window.scrollX - (dropdownRef.current?.offsetWidth ?? 200); // Positionner à droite du bouton
      } else if (anchorOrigin.horizontal === 'center') {
        left = buttonRect.left + window.scrollX + buttonRect.width / 2 - (dropdownRef.current?.offsetWidth ?? 200) / 2; // Centrer horizontalement
      }

      // Ajuster les débordements hors du viewport
      if (left + 200 > viewportWidth) {
        left = viewportWidth - 220;
      }
      if (left < 0) {
        left = 10;
      }
      if (top + 200 > viewportHeight) {
        top = buttonRect.top + window.scrollY - 210;
      }

      setPosition({
        top: top,
        left: left,
      });
    }
    setIsOpen(!isOpen);
  };

  useEffect(() => {
    const handleClickOutside = (event: MouseEvent) => {
      if (dropdownRef.current && !dropdownRef.current.contains(event.target as Node) && !buttonRef.current?.contains(event.target as Node)) {
        setIsOpen(false);
      }
    };
    document.addEventListener('mousedown', handleClickOutside);
    return () => {
      document.removeEventListener('mousedown', handleClickOutside);
    };
  }, []);

  const dropdownContent = (
    <>
      <div
        className="dropdown-overlay"
        onClick={() => setIsOpen(false)}
        onKeyUp={(e) => {
          if (e.key === 'Escape') {
            setIsOpen(false);
          }
        }}
      ></div>
      <div
        onClick={(e) => {
          if (!keepOpen) {
            toggleDropdown(e);
            e.stopPropagation();
          }
        }}
        onKeyUp={(e) => {
          if (e.key === 'Escape') {
            setIsOpen(false);
          }
        }}
        ref={dropdownRef}
        className={`dropdown-menu ${noNotch ? 'dropdown-menu--no-notch' : ''} ${dropdownClasses}`}
        style={{ position: 'absolute', top: position.top, left: position.left }}
      >
        {children}
      </div>
    </>
  );

  return (
    <div style={{ position: 'relative', display: 'inline-block' }}>
      {button ? (
        cloneElement(button, {
          // @ts-ignore
          ref: (instance: HTMLButtonElement) => {
            buttonRef.current = instance;
          },
          onClick: (e: React.MouseEvent<HTMLElement>) => {
            // @ts-ignore
            if (button?.props?.onClick) {
              // @ts-ignore
              button.props.onClick(e);
            }
            toggleDropdown(e);
          },
        })
      ) : (
        <Button
          priority="tertiary no outline"
          ref={buttonRef}
          title="Ouvrir le menu"
          iconId="fr-icon-more-line"
          onClick={toggleDropdown}
          nativeButtonProps={{ style: { rotate: '90deg' } }}
          aria-label="options"
          aria-controls={isOpen ? 'user-menu' : undefined}
          aria-haspopup="true"
          aria-expanded={isOpen ? 'true' : undefined}
        />
      )}
      {isOpen && createPortal(dropdownContent, document.body)}
    </div>
  );
};

export default DropdownMenu;
