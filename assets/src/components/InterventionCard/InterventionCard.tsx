import { JSX } from 'react';
import { useTranslation } from 'react-i18next';

import { InterventionPriorityChip } from '@aei/src/components/InterventionPriorityChip';
import { InterventionStatusChip } from '@aei/src/components/InterventionStatusChip';
import { addressFormatterFormat } from '@aei/src/utils/address';
import { InterventionInterventionview } from '@aei/types/InterventionInterventionview';

import './InterventionCard.css';

export interface InterventionCardProps {
  intervention: InterventionInterventionview;
  upperRightActions?: JSX.Element;
}

export function InterventionCard(props: Readonly<InterventionCardProps>) {
  const { t } = useTranslation('common');

  return (
    <div className="aei-intervention-card" style={{ height: '147px' }}>
      <div className="aei-stack">
        <div className="fr-p-1w">
          <p className="aei-intervention-card--title">{props.intervention.title}</p>
          <span className="aei-intervention-card--posted-at">
            Posté le {t('date.shortWithTime', { date: new Date(props.intervention.createdAt) })}
          </span>
          <p className="aei-intervention-card--address">
            {addressFormatterFormat({
              street: props.intervention.location.street || undefined,
              city: props.intervention.location.city || undefined,
              postcode: props.intervention.location.postcode || undefined,
            })}
          </p>
        </div>
      </div>
      <div
        className="aei-intervention-card--image"
        style={{
          backgroundImage: `url(${!!props.intervention.pictures[0] ? props.intervention.pictures[0].medium : props.intervention.type.picture})`,
        }}
      ></div>
      <div style={{ position: 'absolute', right: 5, top: 5 }}>
        <div className="aei-stack aei-stack-columns aei-stack-small-gap">
          <InterventionPriorityChip priority={props.intervention.priority} />
          <InterventionStatusChip participants={props.intervention.participants} status={props.intervention.status} />
        </div>
      </div>
      {!!props.upperRightActions && <div style={{ position: 'absolute', right: 0 }}>{props.upperRightActions}</div>}
    </div>
  );
}
