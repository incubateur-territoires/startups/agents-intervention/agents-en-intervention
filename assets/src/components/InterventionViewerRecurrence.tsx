import { InterventionViewerProperty } from '@aei/src/components/InterventionViewerProperty';
import { InterventionSchemaType } from '@aei/src/models/entities/intervention';
import { getWeekOfMonth } from '@aei/src/models/getWeekOfMonth';

interface InterventionViewerRecurrenceProps {
  intervention: InterventionSchemaType;
}

export const InterventionViewerRecurrence = ({ intervention }: InterventionViewerRecurrenceProps) => {
  if (!intervention.recurring) return null;

  const weekday = intervention?.startAt ? new Intl.DateTimeFormat('fr-FR', { weekday: 'long' }).format(new Date(intervention.startAt)) : '-';
  const weekOfMonth = intervention?.startAt ? getWeekOfMonth(new Date(intervention.startAt)) : 0;

  let text =
    intervention.frequency === 'weekly'
      ? `Toutes les ${intervention.frequencyInterval > 1 ? intervention.frequencyInterval : ''} semaines, le ${weekday}`
      : `Tous les ${intervention.frequencyInterval > 1 ? intervention.frequencyInterval : ''} ${
          intervention.frequency === 'daily' ? 'jours' : 'mois'
        }`;

  if (intervention.frequency === 'monthly') {
    text += ` le ${weekOfMonth}eme ${weekday}`;
  }

  return <InterventionViewerProperty icon="fr-icon-time-line" value={text} />;
};
