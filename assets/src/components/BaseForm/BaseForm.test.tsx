import { fireEvent, render, screen, waitFor } from '@testing-library/react';
import { Control } from 'react-hook-form';

import { BaseForm, BaseFormProps } from './BaseForm';

interface FormSchema {
  name: string;
}

const renderBaseForm = (props: Partial<BaseFormProps<FormSchema>> = {}) => {
  const defaultProps: BaseFormProps<FormSchema> = {
    handleSubmit: jest.fn(),
    onSubmit: jest.fn().mockResolvedValue(undefined),
    control: { _formState: { errors: {} } } as Control<FormSchema>,
    ariaLabel: 'test-form',
    ...props,
  };

  return render(<BaseForm {...defaultProps}>Form Content</BaseForm>);
};

describe('BaseForm Component', () => {
  test('renders BaseForm component', () => {
    renderBaseForm();
    expect(screen.getByLabelText('test-form')).toBeInTheDocument();
  });

  test('calls onSubmit when form is submitted', async () => {
    const handleSubmit = jest.fn((fn) => (e: any) => {
      e.preventDefault();
      fn({ name: 'test' });
    });
    const onSubmit = jest.fn().mockResolvedValue(undefined);

    // @ts-ignore
    renderBaseForm({ handleSubmit, onSubmit });

    fireEvent.submit(screen.getByLabelText('test-form'));

    await waitFor(() => {
      expect(onSubmit).toHaveBeenCalledWith({ name: 'test' });
    });
  });

  test.skip('displays validation errors', async () => {
    const control = {
      _formState: {
        errors: {
          name: { message: 'Name is required' },
        },
      },
    } as Control<FormSchema>;

    renderBaseForm({ control });

    expect(screen.getByText('components.BaseForm.form_contains_errors')).toBeInTheDocument();
    expect(screen.getByText('Name is required')).toBeInTheDocument();
  });

  test.skip('displays submission error', async () => {
    const onSubmit = jest.fn().mockRejectedValue(new Error('Submission failed'));

    renderBaseForm({ onSubmit });

    fireEvent.submit(screen.getByLabelText('test-form'));

    await waitFor(() => {
      expect(screen.getByText('Submission failed')).toBeInTheDocument();
    });
  });
});
