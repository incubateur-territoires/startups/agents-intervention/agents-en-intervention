import Alert from '@codegouvfr/react-dsfr/Alert';
import { CSSProperties, FormEventHandler, PropsWithChildren, RefObject, useRef, useState } from 'react';
import { Control, FieldErrorsImpl, FieldValues, UseFormHandleSubmit } from 'react-hook-form';
import { useTranslation } from 'react-i18next';

import { ErrorAlert } from '@aei/src/ui/ErrorAlert';

export interface BaseFormProps<FormSchemaType extends FieldValues> {
  ariaLabel: string;
  className?: string;
  control: Control<any>;
  flex?: boolean;
  handleSubmit: UseFormHandleSubmit<FormSchemaType>;
  innerRef?: RefObject<HTMLFormElement | null>;
  onSubmit: (input: FormSchemaType) => Promise<void>;
  style?: CSSProperties;
}

export function BaseForm<FormSchemaType extends FieldValues>(props: PropsWithChildren<BaseFormProps<FormSchemaType>>) {
  const { t } = useTranslation('common');
  const [validationErrors, setValidationErrors] = useState<Partial<FieldErrorsImpl<any>>>(props.control._formState.errors);
  const [onSubmitError, setOnSubmitError] = useState<Error | null>(null);
  const formRef = useRef<HTMLFormElement | null>(null); // This is used to scroll to the error messages

  const setMultipleRefs = (element: HTMLFormElement) => {
    formRef.current = element;

    if (props.innerRef) {
      props.innerRef.current = element;
    }
  };

  const onSubmit: FormEventHandler<HTMLFormElement> = async (...args) => {
    const submitChain = props.handleSubmit(
      async (input: FormSchemaType) => {
        try {
          // Validation has passed, reset validation errors
          setValidationErrors({});

          await props.onSubmit(input);

          setOnSubmitError(null);
        } catch (err: unknown) {
          if (err instanceof Error) {
            setOnSubmitError(err);
          } else {
            setOnSubmitError(err as any); // The default case is good enough for now
          }

          if (formRef.current?.scrollIntoView) {
            formRef.current.scrollIntoView({ behavior: 'smooth' });
          }
        }
      },
      (errors) => {
        // Only triggered on inputs validation errors
        setValidationErrors(errors);

        if (formRef.current?.scrollIntoView) {
          formRef.current.scrollIntoView({ behavior: 'smooth' });
        }
      }
    );

    await submitChain(...args);
  };

  return (
    <form
      onSubmit={onSubmit}
      onKeyDown={(event) => {
        if (event.key === 'Enter') {
          event.preventDefault();
        }
      }}
      aria-label={props.ariaLabel}
      className={props.className}
      style={{
        ...(props.flex
          ? {
              display: 'flex',
              flexDirection: 'column',
              flex: 1,
              gap: '1rem',
            }
          : {}),
        ...props.style,
      }}
      ref={setMultipleRefs}
    >
      {onSubmitError || Object.keys(validationErrors).length > 0 ? (
        <div className="aei-stack aei-stack-small-gap">
          {!!onSubmitError && (
            <div className="fr-p-2w">
              <ErrorAlert errors={[onSubmitError]} />
            </div>
          )}

          {Object.keys(validationErrors).length > 0 && (
            <div className="fr-p-2w">
              <Alert
                severity="error"
                small
                description={
                  validationErrors[''] !== undefined
                    ? (validationErrors['']?.message as string)
                    : t('components.BaseForm.form_contains_errors', { count: Object.keys(validationErrors).length })
                }
              />
            </div>
          )}
        </div>
      ) : null}

      {props.children}
    </form>
  );
}
