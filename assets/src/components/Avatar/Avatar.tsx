import React from 'react';

import './Avatar.css';

export function extractInitials(fullName: string): string {
  const parts = fullName.split(/[ -]/);
  let initials = '';

  for (const part of parts) {
    initials += part.charAt(0);
  }

  if (initials.length > 3 && initials.search(/[A-Z]/) !== -1) {
    initials = initials.replace(/[a-z]+/g, '');
  }

  initials = initials.substring(0, 2).toUpperCase();

  return initials;
}

export function stringToColor(string: string) {
  let hash = 0;
  let i;

  for (i = 0; i < string.length; i += 1) {
    hash = string.charCodeAt(i) + ((hash << 5) - hash);
  }

  let color = '#';

  for (i = 0; i < 3; i += 1) {
    const value = (hash >> (i * 8)) & 0xff;
    color += `00${value.toString(16)}`.slice(-2);
  }

  return color;
}

interface AvatarProps {
  src?: string;
  fontSize?: number;
  fullName: string;
  size?: number;
  backgroundColor?: string;
  textColor?: string;
}

const Avatar: React.FC<AvatarProps> = ({ src, fontSize, fullName, size = 24, backgroundColor, textColor }) => {
  const styles: React.CSSProperties = {
    width: size,
    height: size,
    backgroundColor: backgroundColor || stringToColor(fullName) || '#ccc',
    color: textColor || '#fff',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: '50%',
    fontSize: `${fontSize || size / 2}px`,
    overflow: 'hidden',
  };

  return (
    <div style={styles} className="avatar">
      {src ? (
        <img src={src} alt={fullName} style={{ width: '100%', height: '100%', objectFit: 'cover' }} />
      ) : (
        <span>{extractInitials(fullName)}</span>
      )}
    </div>
  );
};

export default Avatar;
