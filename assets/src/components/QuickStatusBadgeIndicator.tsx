import Tag from '@codegouvfr/react-dsfr/Tag';
import { useTranslation } from 'react-i18next';

import { InterventionPriorityChip } from '@aei/src/components/InterventionPriorityChip';
import { getWeekOfMonth } from '@aei/src/models/getWeekOfMonth';
import { Frequency } from '@aei/types';
import { InterventionInterventionview } from '@aei/types/InterventionInterventionview';
import { Priority } from '@aei/types/Priority';
import { Status } from '@aei/types/Status';

export interface QuickStatusBadgeIndicatorProps {
  intervention: InterventionInterventionview;
}

const QuickStatusBadgeIndicator = ({ intervention }: QuickStatusBadgeIndicatorProps) => {
  const { t } = useTranslation();
  let child = null;
  let label = '';

  const startAt = intervention.startAt ? new Date(intervention.startAt) : null;
  const endAt = intervention.endAt ? new Date(intervention.endAt) : null;
  const endedAt = intervention.endedAt ? new Date(intervention.endedAt) : null;

  if (intervention.recurring) {
    const weekday = intervention?.startAt ? new Intl.DateTimeFormat('fr-FR', { weekday: 'long' }).format(new Date(intervention.startAt)) : '-';
    const weekOfMonth = intervention?.startAt ? getWeekOfMonth(new Date(intervention.startAt)) : 0;

    let label;
    if (intervention.frequency === Frequency.YEARLY) {
      label = 'Tous les ans, le ' + t('date.dayMonth', { date: startAt });
    } else {
      label =
        intervention.frequency === Frequency.WEEKLY
          ? `Toutes les ${intervention.frequencyInterval > 1 ? intervention.frequencyInterval : ''} semaines, le ${weekday}`
          : `Tous les ${intervention.frequencyInterval > 1 ? intervention.frequencyInterval : ''} ${intervention.frequency === Frequency.DAILY ? 'jours' : 'mois'}`;

      if (intervention.frequency === Frequency.MONTHLY) {
        label += ` le ${weekOfMonth}eme ${weekday}`;
      }
      label += ', de ' + t('date.time', { date: startAt }) + ' à ' + t('date.time', { date: endAt });
    }
    child = (
      <Tag iconId="fr-icon-time-line" nativeSpanProps={{ style: { backgroundColor: '#E3E3FD', color: '#000091' } }}>
        {label}
      </Tag>
    );
  } else {
    if (intervention.priority === Priority.Urgent) {
      child = <InterventionPriorityChip priority={intervention.priority} />;
    } else if (intervention.status === Status.Finished) {
      child = (
        <Tag
          iconId="fr-icon-check-line"
          className="chip-priority"
          nativeSpanProps={{ style: { backgroundColor: '#B3F7C3', color: '#161616' } }}
        >{`Terminée le ${t('date.shortWithTime', { date: endedAt })}`}</Tag>
      );
    } else if (intervention.status === Status.Rejected) {
      child = (
        <Tag iconId="fr-icon-close-line" className="chip-priority" nativeSpanProps={{ style: { backgroundColor: '#F1E4CA', color: '#161616' } }}>
          Intervention refusée
        </Tag>
      );
    } else if (intervention.startAt) {
      label = 'À faire le ';
      if (intervention.endAt) {
        label += t('date.shortWithTimeFrom', { date: startAt }) + ' à ' + t('date.time', { date: endAt });
      } else {
        label += t('date.shortWithTime', { date: startAt });
      }
      child = (
        <Tag iconId="fr-icon-time-line" className="chip-priority" nativeSpanProps={{ style: { backgroundColor: '#E3E3FD', color: '#000091' } }}>
          {label}
        </Tag>
      );
    }
  }

  return <div className="fr-px-2w fr-pt-2w fr-pb-0 quick-status-badge-indicator">{child}</div>;
};

export default QuickStatusBadgeIndicator;
