import Button, { ButtonProps } from '@codegouvfr/react-dsfr/Button';
import React from 'react';

import CircularProgress from '../CircularProgress';

type LoadingButtonProps = ButtonProps & {
  disabled?: boolean;
  loading?: boolean;
};

const LoadingButton: React.FC<LoadingButtonProps> = ({ children, loading = false, disabled = false, nativeButtonProps, ...props }) => {
  return (
    // @ts-ignore
    <Button {...props} nativeButtonProps={{ ...nativeButtonProps, disabled: disabled || loading }}>
      {loading ? (
        <>
          <CircularProgress />
          {/*{children && <span>Chargement...</span>}*/}
        </>
      ) : (
        children
      )}
    </Button>
  );
};

export default LoadingButton;
