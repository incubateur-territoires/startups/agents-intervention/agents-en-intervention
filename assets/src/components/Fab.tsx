import React, { JSX, useMemo } from 'react';

import CircularProgress from './CircularProgress';

interface FabProps {
  bottom?: string;
  children: JSX.Element;
  color: 'primary' | 'secondary';
  disabled?: boolean;
  href?: string;
  label: string;
  left?: string;
  loading?: boolean;
  onClick?: (e: any) => void;
  position?: 'absolute' | 'fixed';
  right?: string;
  size: 'large' | 'small';
  top?: string;
}

const Fab = ({ position = 'absolute', ...props }: FabProps) => {
  const fabStyle: React.CSSProperties = useMemo(
    () => ({
      position: position,
      width: props.size === 'large' ? '56px' : '40px',
      height: props.size === 'large' ? '56px' : '40px',
      padding: props.size === 'large' ? '16px' : '9px',
      borderRadius: '45%',
      boxShadow: '0px 2px 10px rgba(0, 0, 0, 0.3)',
      backgroundColor: props.color === 'primary' ? '#000091' : 'rgb(236, 236, 254)',
      backgroundImage: 'none',
      color: props.color === 'primary' ? 'white' : 'var(--text-action-high-blue-france)',
      right: props.right,
      bottom: props.bottom,
      left: props.left,
      top: props.top,
      zIndex: 1000,
    }),
    [props]
  );

  if (props.loading) {
    return (
      <div style={fabStyle}>
        <CircularProgress size={props.size === 'large' ? 24 : 21} />
      </div>
    );
  }

  if (props.href && !props.disabled) {
    return (
      <a href={props.href} aria-disabled={props.disabled} style={fabStyle} aria-label={props.label} onClick={props.onClick}>
        {props.children}
      </a>
    );
  }

  return (
    <button disabled={props.disabled} style={fabStyle} aria-label={props.label} onClick={props.onClick}>
      {props.children}
    </button>
  );
};

export default Fab;
