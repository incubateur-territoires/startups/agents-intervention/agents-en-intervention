export { Comment, CommentMessage } from './Comment';
export type { CommentProps, CommentSide } from './Comment';
