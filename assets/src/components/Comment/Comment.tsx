import { useTranslation } from 'react-i18next';

import Avatar from '@aei/src/components/Avatar';
import { UserUserview } from '@aei/types';

import './_Comment.scss';

export type CommentSide = 'left' | 'right';

export interface CommentMessage {
  content: string;
  date: Date;
}

export interface CommentProps {
  author: UserUserview;
  messages: CommentMessage[];
  side: CommentSide;
}

export function Comment(props: CommentProps) {
  const { t } = useTranslation('common');

  return (
    <div className={`comment-wrapper comment-wrapper--${props.side}`}>
      {/*{props.side === 'left' && (*/}
      {/*  <Grid item>*/}
      {/*    <Tooltip title={`${props.author.firstname} ${props.author.lastname}`}>*/}
      {/*      <div style={{ paddingBottom: '1rem' }}>*/}
      {/*        /!* Avatar only for other people *!/*/}
      {/*        <Avatar fullName={`${props.author.firstname} ${props.author.lastname}`} />*/}
      {/*      </div>*/}
      {/*    </Tooltip>*/}
      {/*  </Grid>*/}
      {/*)}*/}
      <div className="comment-wrapper__comments">
        {props.messages.map((message, i) => {
          return (
            <div key={i}>
              <div
                style={{
                  display: 'flex',
                  alignItems: 'center',
                  marginBottom: 1,
                  textAlign: props.side === 'left' ? 'left' : 'right',
                  flexDirection: props.side === 'right' ? 'row-reverse' : undefined,
                }}
              >
                {/*<Tooltip title={t('date.longWithTime', { date: new Date(message.date) })}>*/}
                <p
                  className={`comment comment--${props.side} ${i === 0 ? ' comment-first ' : ''} ${i === props.messages.length - 1 ? ' comment-last ' : ''}`}
                >
                  {message.content.replace('\\n', '<br/>')}
                </p>
                {/*</Tooltip>*/}
              </div>
            </div>
          );
        })}
      </div>
      <div className="comment-wrapper__user">
        {/*<Tooltip title={`${props.author.firstname} ${props.author.lastname}`}>*/}
        <div style={{ paddingBottom: '1rem' }}>
          <Avatar fullName={`${props.author.firstname} ${props.author.lastname}`} />
        </div>
        {/*</Tooltip>*/}
      </div>
    </div>
  );
}
