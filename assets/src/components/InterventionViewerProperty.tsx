import { FrIconClassName } from '@codegouvfr/react-dsfr';
import React from 'react';

interface InterventionViewerPropertyProps {
  icon: FrIconClassName;
  value: string;
}

export const InterventionViewerProperty = ({ icon, value }: InterventionViewerPropertyProps) => {
  return (
    <div className="aei-stack aei-stack-columns aei-stack-small-gap aei-stack-no-padding" style={{ flexWrap: 'nowrap' }}>
      <span className={`${icon} fr-icon--sm`} aria-hidden="true"></span>
      <span>{value}</span>
    </div>
  );
};
