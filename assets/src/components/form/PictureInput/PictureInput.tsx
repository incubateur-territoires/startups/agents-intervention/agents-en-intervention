import { useCallback, useEffect, useRef, useState } from 'react';
import { useAsyncFn } from 'react-use';

import { useApiPicturesIdDelete } from '@aei/src/client/generated/components';
import LoadingButton from '@aei/src/components/LoadingButton';
import { useUser } from '@aei/src/utils/auth';
import { useMobileFormat } from '@aei/src/utils/useMobileFormat';
import { Intervention, PicturePictureview, PictureTag } from '@aei/types';

import compress from './compress';
import uploadPicture from './uploadPicture';

export type PictureInputProps = {
  name: string;

  /**
   *
   * When the picture change (after compression)
   *
   * @param picture the Picture instance
   * @returns void
   */
  onChange: (picture?: PicturePictureview) => void;

  tag: PictureTag;

  intervention?: Intervention['id'];

  /**
   * Need compression
   */
  withCompression?: boolean;

  value?: PicturePictureview;
};

/**
 * Enhance input for picture (only one)
 */
function PictureInput({ name, onChange, tag, value, intervention, withCompression = false }: PictureInputProps) {
  const { token } = useUser();
  const mobileFormat = useMobileFormat();

  const fileInputRef = useRef<HTMLInputElement>(null);
  const [previewUrl, setPreviewUrl] = useState<string | null>(null);
  const remove = useApiPicturesIdDelete();
  const [picture, setPicture] = useState<PicturePictureview | null>(null);

  useEffect(() => {
    setPreviewUrl(value?.url || null);
  }, [setPreviewUrl, value]);

  const [{ loading }, _onChange] = useAsyncFn(
    (event) =>
      compress(withCompression, event)
        .then(async ({ file, preview }) => {
          const picture = await uploadPicture(file, token || '', tag, intervention);
          setPreviewUrl(preview);
          setPicture(picture);
          return picture;
        })
        .then(onChange)
        .catch((e) => {
          console.error(e);
          debugger;
        }),
    [onChange, setPreviewUrl, token]
  );

  const removePicture = useCallback(() => {
    if (picture) {
      remove.mutateAsync({ pathParams: { id: `${picture.id}` } });
    }
    return onChange(undefined);
  }, [onChange]);

  return (
    <>
      {mobileFormat && <div style={{ flex: 1 }} />}
      <div style={{ textAlign: mobileFormat ? 'center' : undefined }}>
        {!mobileFormat && <p style={{ fontWeight: 'bold' }}>Ajouter une photo</p>}
        {!!previewUrl ? (
          <div
            className="aei-stack aei-stack-columns"
            style={{
              width: '100%',
              justifyContent: 'center',
              flexWrap: 'nowrap',
            }}
          >
            <img
              src={previewUrl}
              alt="logo de la collectivité"
              style={{
                display: 'block',
                maxHeight: '200px',
                maxWidth: 'calc(100% - 40px)',
              }}
            />
            <LoadingButton
              type="button"
              priority="tertiary no outline"
              iconId="fr-icon-delete-bin-fill"
              onClick={removePicture}
              title="supprimer l'image"
            />
          </div>
        ) : (
          <div>
            {/* Unable to nest the `<input>` into a `<Button component="label">` due to a11y limitations, so using a ref */}
            <LoadingButton
              type="button"
              priority="secondary"
              loading={loading}
              onClick={(e) => {
                e.stopPropagation();
                fileInputRef.current?.click();
              }}
              style={{ paddingRight: 2, paddingLeft: 2, flexDirection: 'column', width: '100%' }}
            >
              <span className="fr-icon-camera-fill fr-icon--lg" aria-hidden="true"></span>
              <p>Associer une photo</p>
            </LoadingButton>
            <input ref={fileInputRef} type="file" accept="image/*" hidden onChange={_onChange} />
          </div>
        )}
      </div>
    </>
  );
}

export default PictureInput;
