import imageCompression from 'browser-image-compression';

export type compressReturn = {
  file: File;
  preview: string;
};

const compress = async (withCompression: boolean, event: React.ChangeEvent<HTMLInputElement>): Promise<compressReturn> => {
  if (!event.target.files?.length) {
    throw new Error('compress::file is mandatory');
  }

  try {
    let imageFile = event.target.files[0];

    if (withCompression) {
      const controller = typeof AbortController !== 'undefined' ? new AbortController() : undefined;

      // Compress the picture first for a shorter upload
      imageFile = await imageCompression(imageFile, {
        maxSizeMB: 0.3,
        maxWidthOrHeight: 1920,
        useWebWorker: true,
        signal: controller?.signal, // Not used for now
      });
    }

    return imageCompression.getDataUrlFromFile(imageFile).then((preview) => ({ file: imageFile, preview }));
  } catch (error) {
    console.log(error);
    throw error;
  }
};

export default compress;
