import { getApiUrl } from '@aei/src/utils/url';
import { Intervention, Picture, PictureTag } from '@aei/types';

const uploadPicture = (picture: File, token: string, tag: PictureTag, interventionId?: Intervention['id']): Promise<Picture> =>
  Promise.resolve()
    .then(() => {
      console.log('DO UPLOAD', picture);

      const pictureFormData = new FormData();
      pictureFormData.append('fileName', picture, picture.name);
      pictureFormData.append('tag', tag);

      if (interventionId) {
        pictureFormData.append('intervention', `api/interventions/${interventionId}`);
      }

      return pictureFormData;
    })
    .then((pictureFormData: FormData) =>
      fetch(getApiUrl() + '/pictures', {
        // Pour l'envoi direct à l'API.
        headers: { Authorization: 'Bearer ' + token },
        method: 'POST',
        body: pictureFormData,
      })
        .then((response) => response.json())
        .then((res) => {
          console.log('La photo a bien été envoyé');
          return res;
        })
        .catch((err) => {
          console.error(err);
        })
    );

export default uploadPicture;
