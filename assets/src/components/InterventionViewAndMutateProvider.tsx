import React, { ReactNode, createContext, useCallback, useContext, useEffect, useMemo } from 'react';
import { useEffectOnce, useSetState } from 'react-use';

import { InterventionCreationPage } from '@aei/pages/InterventionCreationPage';
import { InterventionEditPage } from '@aei/pages/InterventionEditPage';
import { InterventionViewer } from '@aei/src/components/InterventionViewer';
import MyDrawer from '@aei/src/components/MyDrawerDSFR';
import { useUser } from '@aei/src/utils/auth';
import { useHashChange } from '@aei/src/utils/url';
import { Role } from '@aei/types';

export enum InterventionViewAndMutateMode {
  View = 'view',
  Edit = 'edit',
  Create = 'create',
}

export interface InterventionViewAndMutateContextDefaults {
  startAt?: Date;
  endAt?: Date;
  endedAt?: Date;
}

export interface InterventionViewAndMutateContextParam {
  mode?: InterventionViewAndMutateMode;
  interventionId?: number;
  retry: () => any;
  left?: boolean;
  defaults?: InterventionViewAndMutateContextDefaults;
  onSuccess: () => any;
  onClose: () => any;
}

export interface InterventionViewAndMutateContextType {
  setContext: (
    patch:
      | Partial<InterventionViewAndMutateContextParam>
      | ((prevState: InterventionViewAndMutateContextParam) => Partial<InterventionViewAndMutateContextParam>)
  ) => void;
  resetContext: () => void;
}

export const InterventionViewAndMutateContext = createContext<InterventionViewAndMutateContextType>({
  setContext: () => null,
  resetContext: () => null,
});

const initialState = {
  mode: undefined,
  interventionId: undefined,
  retry: () => null,
  left: true,
  defaults: {},
  onSuccess: () => null,
  onClose: () => null,
};

export interface InterventionViewAndMutateProviderProps {
  children: ReactNode;
}

const InterventionViewAndMutateProvider = ({ children }: InterventionViewAndMutateProviderProps) => {
  const { hasRole } = useUser();
  const { hash, setHash } = useHashChange();
  const [ctx, setContext] = useSetState<InterventionViewAndMutateContextParam>(initialState);
  const resetContext = useCallback(async () => {
    if (ctx.mode !== InterventionViewAndMutateMode.View && ctx.retry) {
      await ctx.retry();
    }
    if (ctx.onClose) {
      await ctx.onClose();
    }
    setHash('');
    setContext(initialState);
  }, [ctx, setContext]);

  const modify = useCallback(() => {
    if (ctx.interventionId) {
      setContext({
        mode: InterventionViewAndMutateMode.Edit,
      });
    }
  }, [ctx.interventionId, setContext]);

  useEffectOnce(() => {
    if (hash) {
      if (hash.startsWith('intervention-edit-')) {
        setContext({
          interventionId: parseInt(hash.replace('intervention-edit-', '')),
          mode: InterventionViewAndMutateMode.Edit,
        });
      } else {
        setContext({
          interventionId: parseInt(hash.replace('intervention-', '')),
          mode: InterventionViewAndMutateMode.View,
        });
      }
    }
  });

  useEffect(() => {
    if (ctx.interventionId) {
      setHash(ctx.mode === InterventionViewAndMutateMode.Edit ? `intervention-edit-${ctx.interventionId}` : `intervention-${ctx.interventionId}`);
    }
  }, [ctx.interventionId, ctx.mode]);

  const title = useMemo(() => {
    // Certains rôles ne peuvent pas créer directement des interventions, mais seulement des demandes.
    if (hasRole(Role.Agent) || hasRole(Role.Elected)) {
      return "Demande d'intervention";
    }
    return InterventionViewAndMutateMode.Create ? 'Créer une intervention' : `Modification #${ctx.interventionId}`;
  }, [ctx.interventionId, hasRole]);

  const value = useMemo(
    () => ({
      setContext,
      resetContext,
    }),
    [setContext, resetContext]
  );

  return (
    <InterventionViewAndMutateContext.Provider value={value}>
      {children}
      {ctx.mode === InterventionViewAndMutateMode.View && ctx.interventionId && (
        <InterventionViewer interventionId={ctx.interventionId} refetch={ctx.retry} onClose={resetContext} modify={modify} />
      )}
      {ctx.mode && ctx.mode !== InterventionViewAndMutateMode.View && (
        <MyDrawer narrowView left open={true} onClose={resetContext} title={title}>
          {ctx.mode === InterventionViewAndMutateMode.Create ? (
            <InterventionCreationPage defaults={ctx.defaults} onSuccess={ctx.onSuccess} />
          ) : (
            <InterventionEditPage onSuccess={ctx.onSuccess} params={{ interventionId: `${ctx.interventionId}` }} />
          )}
        </MyDrawer>
      )}
    </InterventionViewAndMutateContext.Provider>
  );
};

export const useInterventionViewAndMutate = () => useContext(InterventionViewAndMutateContext);

export default InterventionViewAndMutateProvider;
