import Button from '@codegouvfr/react-dsfr/Button';
import React, { ReactNode, useCallback, useMemo } from 'react';
import ReactDOM from 'react-dom';

import useMobileFormat from '@aei/src/utils/useMobileFormat';

interface MyDrawerProps {
  narrowView?: boolean;
  left?: boolean;
  open: boolean;
  onClose: () => void;
  children: ReactNode;
  title?: string;
  tooltip?: ReactNode;
}

const MyDrawer = ({ left = false, narrowView = false, open, onClose, children, title = '', tooltip = null }: MyDrawerProps) => {
  const mobileFormat = useMobileFormat();
  const style = useMemo(
    () => ({
      width: mobileFormat ? '100vw' : '90%',
      maxWidth: mobileFormat ? undefined : narrowView ? '400px' : '500px',
      marginLeft: left ? 0 : 'auto',
      marginRight: !left ? 0 : 'auto',
    }),
    [mobileFormat, narrowView]
  );

  const _onClose = useCallback(() => onClose(), [onClose]);

  if (!open) {
    return null;
  }

  return ReactDOM.createPortal(
    <dialog
      id="fr-drawer"
      className="fr-modal--opened fr-drawer"
      role="dialog"
      onClick={(e) => {
        if (e.target === e.currentTarget) {
          _onClose();
        }
      }}
      onKeyUp={(e) => {
        if (e.key === 'Escape') {
          _onClose();
        }
      }}
    >
      <div className="fr-modal__body" style={style}>
        <div className="fr-modal__header">
          {mobileFormat && (
            <Button
              priority="tertiary no outline"
              title="Fermer la visualisation"
              iconId="fr-icon-arrow-left-line"
              onClick={_onClose}
              nativeButtonProps={{ 'aria-label': 'fermer la visualisation' }}
            />
          )}
          <h6 style={{ flexGrow: 1, margin: 0 }}>{title}</h6>
          {tooltip}
          {!mobileFormat && (
            <Button
              type="button"
              priority="tertiary no outline"
              title="Fermer la visualisation"
              iconId="fr-icon-close-line"
              // iconPosition="right"
              onClick={_onClose}
              nativeButtonProps={{ 'aria-label': 'fermer la visualisation' }}
            />
          )}
        </div>
        {children}
      </div>
    </dialog>,
    document.body
  );
};

export default MyDrawer;
