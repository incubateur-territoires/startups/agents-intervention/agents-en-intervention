import { AssignmentDialog } from '@aei/src/components/AssignmentDialog';
import { DropdownMenuItem } from '@aei/src/components/DropdownMenu';
import { useSingletonModal } from '@aei/src/ui/modal/useModal';
import { useUser } from '@aei/src/utils/auth';
import { InterventionInterventionview, Role, UserUserview } from '@aei/types';

type AssignmentMenuItemProps = {
  assignableAgents?: UserUserview[];
  refetch?: () => void;
  intervention: InterventionInterventionview;
};

const AssignmentMenuItem = ({ assignableAgents, refetch, intervention }: AssignmentMenuItemProps) => {
  const { showModal } = useSingletonModal();
  const { hasRole } = useUser();

  if (hasRole(Role.Agent) || !assignableAgents) {
    return null;
  }

  return (
    <DropdownMenuItem
      onClick={() => {
        showModal((modalProps) => (
          <AssignmentDialog
            {...modalProps}
            intervention={intervention}
            agents={assignableAgents}
            onChange={() => {
              refetch && refetch();
            }}
          />
        ));
      }}
    >
      <span className="fr-icon-user-add-line" aria-hidden="true"></span>
      Assigner
    </DropdownMenuItem>
  );
};

export default AssignmentMenuItem;
