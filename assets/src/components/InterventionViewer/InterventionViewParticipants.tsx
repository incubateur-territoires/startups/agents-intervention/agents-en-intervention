import { Avatar } from '@aei/src/components';
import { InterventionInterventionview } from '@aei/types';

const InterventionViewParticipants = ({ intervention }: { intervention: InterventionInterventionview }) => {
  if (!intervention.participants || intervention.participants.length === 0) return null;

  return (
    <div style={{ backgroundColor: 'var(--background-alt-overlap-grey)', padding: '1rem' }}>
      <div style={{ display: 'flex' }}>
        {intervention.participants.map((participant, index) => {
          return (
            <div key={participant.id} style={{ marginLeft: index > 0 ? '-12px' : '0' }}>
              <Avatar fullName={`${participant.firstname} ${participant.lastname}`} />
            </div>
          );
        })}
        <span style={{ paddingLeft: '0.5rem' }}>
          {intervention.participants.map((participant) => `${participant.firstname} ${participant.lastname}`).join(', ')}
        </span>
      </div>
    </div>
  );
};

export default InterventionViewParticipants;
