import { DropdownMenuItem } from '@aei/src/components/DropdownMenu';
import { useUser } from '@aei/src/utils/auth';

export interface ModifyMenuItemProps {
  intervention: any;
  onModify: () => void;
}

const ModifyMenuItem = ({ intervention, onModify }: ModifyMenuItemProps) => {
  const { isAgent } = useUser();

  if (isAgent) {
    return null;
  }

  return (
    <DropdownMenuItem onClick={onModify}>
      <span className="fr-icon-edit-line" aria-hidden="true"></span>
      Modifier
    </DropdownMenuItem>
  );
};

export default ModifyMenuItem;
