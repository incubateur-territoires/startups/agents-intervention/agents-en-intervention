import Tooltip from '@codegouvfr/react-dsfr/Tooltip';
import { forwardRef, useMemo } from 'react';
import { useTranslation } from 'react-i18next';

import { Avatar } from '@aei/src/components';
import DataTable from '@aei/src/components/DataTable/DataTable';
import { DataTableConfiguration, DataTableSearchParams } from '@aei/src/components/DataTable/DataTableConfiguration';
import DropdownMenu from '@aei/src/components/DropdownMenu/DropdownMenu';
import { InterventionCard } from '@aei/src/components/InterventionCard';
import { InterventionStatusChip } from '@aei/src/components/InterventionStatusChip';
import { InterventionViewAndMutateMode, useInterventionViewAndMutate } from '@aei/src/components/InterventionViewAndMutateProvider';
import AssignmentMenuItem from '@aei/src/components/InterventionViewer/AssignmentMenuItem';
import DeleteMenuItem from '@aei/src/components/InterventionViewer/DeleteMenuItem';
import RejectMenuItem from '@aei/src/components/InterventionViewer/RejectMenuItem';
import LoadingButton from '@aei/src/components/LoadingButton';
import { addressFormatterFormat } from '@aei/src/utils/address';
import { useLargeFormat } from '@aei/src/utils/useMobileFormat';
import { Status } from '@aei/types';
import { InterventionInterventionview } from '@aei/types/InterventionInterventionview';
import { UserUserview } from '@aei/types/UserUserview';

export type InterventionsDataTableConfiguration = DataTableConfiguration<InterventionInterventionview>;

export interface InterventionListProps {
  assignableAgents: UserUserview[];
  canMutate?: boolean;
  cols?: string[];
  forceEditOnClick?: boolean;
  interventions: InterventionInterventionview[];
  loading?: boolean;
  refetch: () => void;
  paginationModel?: { pageSize: number; page: number };
  setPaginationModel?: (
    patch:
      | Partial<{ pageSize: number; page: number }>
      | ((prevState: { pageSize: number; page: number }) => Partial<{ pageSize: number; page: number }>)
  ) => void;
  rowCount?: number;
  searchParams: DataTableSearchParams<InterventionsDataTableConfiguration>;
  sortable?: boolean;
  useHistoryApi?: boolean;
}

export const InterventionList = forwardRef(function InterventionList(props: InterventionListProps, ref) {
  const { t } = useTranslation('common');
  const { cols = ['logicId', 'title', 'location', 'status', 'author', 'createdAt', 'startAt', 'endAt', 'participants'] } = props;

  const breakpointUp = useLargeFormat();
  const { setContext } = useInterventionViewAndMutate();

  const columns: InterventionsDataTableConfiguration['columns'] = useMemo(() => {
    const _columns: InterventionsDataTableConfiguration['columns'] = [];

    if (cols.includes('logicId')) {
      _columns.push({
        name: 'logicId',
        header: 'ID',
        cell: ({ logicId }) => <>{logicId}</>,
        sortable: props.sortable || false,
        defaultSortable: true,
        defaultSortableDirection: 'desc',
      });
    }

    if (cols.includes('title')) {
      _columns.push({
        name: 'title',
        header: 'Title',
        cell: ({ title, type }) => {
          let label = title;
          if (!label || label === 'Intervention') {
            label = type.name;
          }
          return <>{label}</>;
        },
        sortable: props.sortable || false,
        defaultSortableDirection: 'asc',
        orderBy: (direction) => [
          {
            title: direction,
          },
        ],
      });
    }

    if (cols.includes('location')) {
      _columns.push({
        name: 'location',
        header: 'Adresse',
        cell: ({ location }) => (
          <>
            {addressFormatterFormat(
              {
                street: location.street || undefined,
                city: location.city || undefined,
                postcode: location.postcode || undefined,
              },
              true
            )}
          </>
        ),
      });
    }

    if (cols.includes('status')) {
      _columns.push({
        name: 'status',
        header: 'Statut',
        cell: ({ participants, status }) => <InterventionStatusChip status={status} participants={participants} />,
        headerClassName: 'fr-text--right',
        cellClassName: 'fr-text--right',
      });
    }

    if (cols.includes('author')) {
      _columns.push({
        name: 'author',
        header: 'Créateur',
        cell: ({ author }) => (
          <div style={{ display: 'inline-grid' }}>
            <Tooltip title={`${author.firstname} ${author.lastname}`} kind="hover">
              <Avatar size={24} fullName={`${author.firstname} ${author.lastname}`} />
            </Tooltip>
          </div>
        ),
        headerClassName: 'fr-text--right',
        cellClassName: 'fr-text--right',
        sortable: props.sortable || false,
        defaultSortableDirection: 'asc',
      });
    }

    if (cols.includes('createdAt')) {
      _columns.push({
        name: 'createdAt',
        header: 'Créé le',
        cell: ({ createdAt }) => (
          <>
            {t('date.date', { date: new Date(createdAt) })}
            <br />
            {t('date.time', { date: new Date(createdAt) })}
          </>
        ),
        sortable: props.sortable || false,
        defaultSortableDirection: 'desc',
        orderBy: (direction) => [
          {
            createdAt: direction,
          },
        ],
      });
    }

    if (cols.includes('startAt')) {
      _columns.push({
        name: 'startAt',
        header: 'Prévue le',
        cell: ({ startAt, priority, status }) => (
          <>
            {priority === 'urgent' ? (
              <span style={{ color: '#B34000' }}>Urgent</span>
            ) : startAt ? (
              <>
                {t('date.date', { date: new Date(startAt) })}
                <br />
                {t('date.time', { date: new Date(startAt) })}
              </>
            ) : status !== Status.Finished ? (
              'À planifier'
            ) : (
              '-'
            )}
          </>
        ),
        sortable: props.sortable || false,
        defaultSortableDirection: 'desc',
        orderBy: (direction) => [
          {
            createdAt: direction,
          },
        ],
      });
    }

    if (cols.includes('endAt')) {
      _columns.push({
        name: 'endedAt',
        header: 'Terminée le',
        sortable: props.sortable || false,
        defaultSortableDirection: 'desc',
        cell: ({ endedAt }) => (
          <>
            {endedAt ? (
              <>
                {t('date.date', { date: new Date(endedAt) })}
                <br />
                {t('date.time', { date: new Date(endedAt) })}
              </>
            ) : (
              '-'
            )}
          </>
        ),
        orderBy: (direction) => [
          {
            createdAt: direction,
          },
        ],
      });
    }

    if (cols.includes('participants')) {
      _columns.push({
        name: 'participants',
        header: 'Assignée à',
        cell: ({ participants }) => (
          <div style={{ display: 'inline-flex', gap: '2px', flexWrap: 'wrap' }}>
            {participants.map((participant) => (
              <Tooltip key={`participants-${participant.id}`} title={`${participant.firstname} ${participant.lastname}`} kind="hover">
                <Avatar size={24} fullName={`${participant.firstname} ${participant.lastname}`} />
              </Tooltip>
            ))}
          </div>
        ),
      });
    }

    if (props.canMutate) {
      _columns.push({
        name: 'actions',
        header: '',
        cell: (row) => (
          <DropdownMenu>
            <AssignmentMenuItem assignableAgents={props.assignableAgents} intervention={row} refetch={props.refetch} />
            <RejectMenuItem onSuccess={props.refetch} intervention={row} />
            <DeleteMenuItem intervention={row} />
          </DropdownMenu>
        ),
      });
    }

    return _columns;
  }, [cols, props.canMutate, t]);

  const InterventionsDataTable = {
    rowKey: ({ id }) => `intervention-${id}`, // rowLink: ({ id }) => ({ href: `#intervention-${id}` }),
    onRowClick: (row) => {
      setContext({
        interventionId: row.id,
        mode: props.forceEditOnClick && props.canMutate ? InterventionViewAndMutateMode.Edit : InterventionViewAndMutateMode.View,
        retry: props.refetch,
      });
    },
    columns,
    useHistoryApi: props.useHistoryApi,
  } satisfies InterventionsDataTableConfiguration;

  return breakpointUp ? ( // @ts-ignore
    <div ref={ref}>
      <DataTable
        baseHref=""
        className="fr-table--lg"
        configuration={InterventionsDataTable}
        loading={props.loading}
        paginationModel={props.paginationModel}
        rowCount={props.rowCount}
        rows={props.interventions}
        searchParams={props.searchParams}
        setPaginationModel={props.setPaginationModel}
      />
    </div>
  ) : (
    <div aria-label="liste des interventions" className="aei-stack aei-stack-large-gap" style={{ paddingLeft: 0, paddingRight: 0 }}>
      {props.interventions.map((intervention) => (
        <a
          key={intervention.id}
          href={`#intervention-${intervention.id}`}
          onClick={() => {
            setContext({
              interventionId: intervention.id,
              mode: InterventionViewAndMutateMode.View,
              retry: props.refetch,
            });
          }}
        >
          <InterventionCard intervention={intervention} />
        </a>
      ))}
      {props.rowCount !== props.interventions.length && (
        <LoadingButton
          loading={props.loading}
          onClick={() =>
            props.setPaginationModel &&
            props.setPaginationModel((prev) => ({
              page: prev.page + 1,
            }))
          }
          className="fr-btn--wide fr-mb-5w"
          title="Voir plus"
          priority="secondary"
        >
          Voir plus
        </LoadingButton>
      )}
    </div>
  );
});
