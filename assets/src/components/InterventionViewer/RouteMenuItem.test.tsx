import { fireEvent, render } from '@testing-library/react';
import platform from 'platform';
import React from 'react';

import { InterventionInterventionview } from '@aei/types';

import RouteMenuItem from './RouteMenuItem';

jest.mock('platform', () => ({
  os: { family: 'iOS' },
}));

// @ts-ignore
const mockIntervention: InterventionInterventionview = {
  location: {
    id: 1,
    rest: ' ',
    latitude: 48.8566,
    longitude: 2.3522,
    street: 'Champs-Élysées',
    city: 'Paris',
    postcode: '75008',
    fullAddress: 'Champs-Élysées, 75008 Paris',
  },
};

describe('RouteMenuItem', () => {
  it('should open the correct URL for iOS', () => {
    const { getByText } = render(<RouteMenuItem intervention={mockIntervention} />);
    const menuItem = getByText('Itinéraire');
    window.open = jest.fn();

    fireEvent.click(menuItem);

    expect(window.open).toHaveBeenCalledWith('maps://?ll=48.8566,2.3522&q=Champs-Élysées, 75008 Paris', '_blank');
  });

  it('should open the correct URL for Android', () => {
    // @ts-ignore
    platform.os.family = 'Android';
    const { getByText } = render(<RouteMenuItem intervention={mockIntervention} />);
    const menuItem = getByText('Itinéraire');
    window.open = jest.fn();

    fireEvent.click(menuItem);

    expect(window.open).toHaveBeenCalledWith('geo:48.8566,2.3522?q=48.8566,2.3522(Champs-Élysées, 75008 Paris)', '_blank');
  });

  it('should open the correct URL for other platforms', () => {
    // @ts-ignore
    platform.os.family = 'Windows';
    const { getByText } = render(<RouteMenuItem intervention={mockIntervention} />);
    const menuItem = getByText('Itinéraire');
    window.open = jest.fn();

    fireEvent.click(menuItem);

    expect(window.open).toHaveBeenCalledWith('https://www.google.com/maps/dir/?api=1&destination=48.8566,2.3522', '_blank');
  });
});
