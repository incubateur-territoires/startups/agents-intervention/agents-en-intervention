import platform from 'platform';

import { DropdownMenuItem } from '@aei/src/components/DropdownMenu';
import { addressFormatterFormat } from '@aei/src/utils/address';
import { InterventionInterventionview } from '@aei/types';

interface RouteMenuItemProps {
  intervention: InterventionInterventionview;
}

const RouteMenuItem = ({ intervention }: RouteMenuItemProps) => {
  // Open the itinerary either on native app when mobile or fallback to Google Maps
  let url = `https://www.google.com/maps/dir/?api=1&destination=${intervention.location.latitude},${intervention.location.longitude}`;

  if (platform.os?.family === 'iOS') {
    url = `maps://?ll=${intervention.location.latitude},${intervention.location.longitude}&q=${addressFormatterFormat({
      street: intervention.location.street || undefined,
      city: intervention.location.city || undefined,
      postcode: intervention.location.postcode || undefined,
    })}`;
  } else if (platform.os?.family === 'Android') {
    url = `geo:${intervention.location.latitude},${intervention.location.longitude}?q=${intervention.location.latitude},${intervention.location.longitude}(${addressFormatterFormat(
      {
        street: intervention.location.street || undefined,
        city: intervention.location.city || undefined,
        postcode: intervention.location.postcode || undefined,
      }
    )})`;
  }

  return (
    <DropdownMenuItem onClick={() => window?.open(url, '_blank')?.focus()}>
      <span className="fr-icon-road-map-line" aria-hidden="true"></span>
      Itinéraire
    </DropdownMenuItem>
  );
};

export default RouteMenuItem;
