import { useAsyncFn } from 'react-use';

import { useApiInterventionsIdrejectPost } from '@aei/src/client/generated/components';
import { DropdownMenuItem } from '@aei/src/components/DropdownMenu';
import { useSingletonRejectDialog } from '@aei/src/components/InterventionViewer/RejectInterventionDialog';
import { useUser } from '@aei/src/utils/auth';
import { InterventionInterventionview } from '@aei/types/InterventionInterventionview';
import { Status } from '@aei/types/Status';

export interface RejectMenuItemProps {
  intervention: InterventionInterventionview | null;
  onSuccess: () => void | Promise<void>;
}

const RejectMenuItem = ({ intervention, onSuccess }: RejectMenuItemProps) => {
  const { showRejectDialog } = useSingletonRejectDialog();
  const reject = useApiInterventionsIdrejectPost();
  const { isCoordinateurOrAdminEmployer } = useUser();

  const [, onConfirm] = useAsyncFn(
    (body) =>
      intervention
        ? reject
            .mutateAsync({
              pathParams: { id: `${intervention.id}` },
              body,
            })
            .then(onSuccess)
        : Promise.reject('Intervention is not accessible'),
    [reject]
  );

  if (!isCoordinateurOrAdminEmployer || !intervention || intervention.status !== Status.ToValidate) {
    return null;
  }

  return (
    <DropdownMenuItem
      onClick={() => {
        showRejectDialog({ onConfirm });
      }}
    >
      <span className="fr-icon-close-circle-line" aria-hidden="true"></span>
      Refuser
    </DropdownMenuItem>
  );
};

export default RejectMenuItem;
