import React from 'react';
import { useReactToPrint } from 'react-to-print';

import { DropdownMenuItem } from '@aei/src/components/DropdownMenu';

interface PrintMenuItemProps {
  toPrint?: React.RefObject<HTMLDivElement | null>;
}

const PrintMenuItem = ({ toPrint }: PrintMenuItemProps) => {
  const print = useReactToPrint({
    contentRef: toPrint,
    nonce: document.querySelector('meta[name="csp-nonce"]')?.getAttribute('content') ?? '',
  });

  return (
    <DropdownMenuItem onClick={() => print()}>
      <span className="fr-icon-printer-line" aria-hidden="true"></span>
      Imprimer
    </DropdownMenuItem>
  );
};

export default PrintMenuItem;
