import { ReactNode } from 'react';

import Button from '@aei/src/components/LoadingButton';
import { useSingletonConfirmationDialog } from '@aei/src/ui/modal/useModal';
import { InterventionInterventionview, PictureTag, Status } from '@aei/types';

export interface InterventionViewerBottomActionsProps {
  canMutate: boolean;
  intervention: InterventionInterventionview;
  updateInterventionHelper: (intervention: InterventionInterventionview, body: any) => Promise<void>;
  toggleShowAfterPicturePicker: (nextValue?: any) => void;
}

const InterventionViewerBottomActions = ({
  canMutate,
  intervention,
  updateInterventionHelper,
  toggleShowAfterPicturePicker,
}: InterventionViewerBottomActionsProps) => {
  const { showConfirmationDialog } = useSingletonConfirmationDialog();

  const startInterventionAction = async (intervention: InterventionInterventionview) => {
    showConfirmationDialog({
      description: <>Êtes-vous sûr de vouloir commencer l&apos;intervention #{intervention.logicId} ?</>,
      onConfirm: async () =>
        updateInterventionHelper(intervention, {
          status: 'in-progress',
        }),
    });
  };

  const endInterventionAction = async (intervention: InterventionInterventionview) => {
    showConfirmationDialog({
      description: <>Êtes-vous sûr de vouloir valider la fin de l&apos;intervention #{intervention.logicId} ?</>,
      onConfirm: async () =>
        updateInterventionHelper(intervention, {
          status: 'finished',
        }),
    });
  };

  if (intervention.status === Status.ToValidate || !canMutate || intervention.recurring || intervention.status === Status.Blocked) {
    return null;
  }

  if (intervention.status === Status.Finished) {
    return (
      <BottomActionsGrid>
        <Button disabled size="large" className="fr-btn--wide">
          Intervention terminée
        </Button>
      </BottomActionsGrid>
    );
  }

  if (intervention.status === Status.ToDo) {
    return (
      <BottomActionsGrid>
        <Button
          onClick={() => {
            if (intervention) {
              startInterventionAction(intervention);
            }
          }}
          size="large"
          className="fr-btn--wide"
        >
          Commencer l&apos;intervention
        </Button>
      </BottomActionsGrid>
    );
  }

  return (
    <BottomActionsGrid>
      {intervention.pictures.filter((it) => it.tag === PictureTag.After).length >= 1 ? (
        <Button
          onClick={() => {
            if (intervention) {
              endInterventionAction(intervention);
            }
          }}
          size="large"
          className="fr-btn--wide"
        >
          Finaliser l&apos;intervention
        </Button>
      ) : (
        <>
          <Button
            onClick={() => {
              toggleShowAfterPicturePicker();
            }}
            size="large"
            className="fr-btn--wide"
          >
            Prendre une photo
          </Button>
          <Button
            priority="tertiary no outline"
            onClick={() => {
              if (intervention) {
                endInterventionAction(intervention);
              }
            }}
            size="large"
            className="fr-btn--wide"
          >
            Terminer sans photo
          </Button>
        </>
      )}
    </BottomActionsGrid>
  );
};

const BottomActionsGrid = ({ children }: { children: ReactNode }) => (
  <div
    style={{
      position: 'sticky',
      bottom: '0',
      zIndex: 1000,
      backgroundColor: 'var(--background-lifted-grey)',
      padding: '0.5rem 1rem',
    }}
    className="fr-no-print"
  >
    {children}
  </div>
);

export default InterventionViewerBottomActions;
