import { DropdownMenuItem } from '@aei/src/components/DropdownMenu';
import { InterventionInterventionview, Status } from '@aei/types';

export interface SetBlockedMenuItemProps {
  intervention: InterventionInterventionview;
  setBlocked: (intervention: InterventionInterventionview) => void;
}

const SetBlockedMenuItem: React.FC<SetBlockedMenuItemProps> = ({ intervention, setBlocked }) =>
  intervention.status !== Status.Recurring && intervention.status !== Status.Finished && intervention.status !== Status.Blocked ? (
    <DropdownMenuItem onClick={() => setBlocked(intervention)}>
      <span className="fr-icon-pause-circle-line" aria-hidden="true"></span>
      Mettre en attente
    </DropdownMenuItem>
  ) : null;

export default SetBlockedMenuItem;
