import { Input } from '@codegouvfr/react-dsfr/Input';
import { useRef } from 'react';

import Modal from '@aei/src/ui/modal/Modal';
import { useSingletonModal } from '@aei/src/ui/modal/useModal';

export interface RejectInterventionDialogProps {
  onConfirm: (body: any) => Promise<void>;
  onClose: () => void;
}

export const RejectInterventionDialog = (props: RejectInterventionDialogProps) => {
  const sumbitRef = useRef<HTMLButtonElement | null>(null);

  return (
    <Modal
      title="Refuser cette intervention"
      iconId="fr-icon-arrow-right-line"
      primaryButtonText="Envoyer"
      open
      onConfirm={async () => sumbitRef.current?.click()}
      onClose={props.onClose}
      onCancel={async () => props.onClose()}
    >
      <form
        onSubmit={(event: React.FormEvent<HTMLFormElement>) => {
          event.preventDefault();
          const formData = new FormData(event.currentTarget);
          const formJson = Object.fromEntries((formData as any).entries());
          props
            .onConfirm(formJson)
            .then(() => props.onClose())
            .catch(alert);
        }}
      >
        <div className="aei-stack">
          <div className="fr-select-group">
            <label className="fr-label" htmlFor="select">
              Raison*
            </label>
            <select className="fr-select" id="reason" name="reason" required>
              <option value="Déjà signalée">Déjà signalée</option>
              <option value="En attente d'intervention">En attente d'intervention</option>
              <option value="Besoin de plus d'informations">Besoin de plus d'informations</option>
            </select>
          </div>

          <Input
            label="Commentaire"
            textArea
            nativeTextAreaProps={{
              id: 'details',
              name: 'details',
            }}
          />
        </div>

        <button type="submit" aria-hidden ref={sumbitRef} />
      </form>
    </Modal>
  );
};

export type ShowRejectInterventionDialogProps = Pick<RejectInterventionDialogProps, 'onConfirm'>;

export const useSingletonRejectDialog = () => {
  const { showModal } = useSingletonModal();

  return {
    showRejectDialog(rejectInterventionDialogProps: ShowRejectInterventionDialogProps) {
      showModal((modalProps) => <RejectInterventionDialog {...modalProps} {...rejectInterventionDialogProps} />);
    },
  };
};
