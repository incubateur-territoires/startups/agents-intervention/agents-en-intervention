import { Stepper } from '@codegouvfr/react-dsfr/Stepper';
import { useColors } from '@codegouvfr/react-dsfr/useColors';
import { useMemo } from 'react';
import { useTranslation } from 'react-i18next';

import { fetchApiInterventionsRestartPost } from '@aei/client/fetchers';
import { Section, sectionFlow } from '@aei/src/components/InterventionViewer';
import Button from '@aei/src/components/LoadingButton';
import { useSingletonConfirmationDialog } from '@aei/src/ui/modal/useModal';
import { InterventionInterventionview, Status } from '@aei/types';

export interface InterventionViewerStatusProps {
  canMutate: boolean;
  refetch: () => void | Promise<void>;
  intervention: InterventionInterventionview;
  setIntervention: (intervention: InterventionInterventionview) => void;
}

const InterventionViewerStatus = ({ intervention, setIntervention, canMutate, refetch }: InterventionViewerStatusProps) => {
  const { t } = useTranslation('common');
  const theme = useColors();
  const { showConfirmationDialog } = useSingletonConfirmationDialog();

  const displayedSection = useMemo<Section>(() => {
    switch (intervention?.status) {
      case Status.Finished:
        return Section.Done;
      case Status.ToValidate:
        return Section.ToValidate;
      case Status.InProgress:
      case Status.Blocked:
        return Section.InProgress;
      default:
        return Section.ToDo;
    }
  }, [intervention]);

  const restartInterventionAction = async () => {
    if (intervention)
      showConfirmationDialog({
        description: <>Êtes-vous sûr de vouloir reprendre l&apos;intervention #{intervention.logicId} ?</>,
        onConfirm: () =>
          intervention
            ? fetchApiInterventionsRestartPost({
                pathParams: { id: `${intervention.id}` },
                body: {},
              }).then((intervention) => {
                setIntervention(intervention);
                refetch();
              })
            : Promise.resolve(),
      });
  };

  if (intervention.recurring || intervention.status === Status.Rejected) return null;

  return (
    <div
      className="fr-p-2w"
      style={{
        backgroundColor: theme.decisions.background.alt.blueFrance.default,
      }}
    >
      {intervention.status === Status.Blocked ? (
        <div>
          <p>Cette intervention est mise en attente.</p>
          {canMutate && (
            <div style={{ display: 'flex', justifyContent: 'center' }}>
              <Button onClick={restartInterventionAction}>Reprendre l&apos;intervention</Button>
            </div>
          )}
        </div>
      ) : (
        <Stepper
          title={t(`components.InterventionViewer.section.enum.${displayedSection}`)}
          currentStep={sectionFlow.indexOf(displayedSection)}
          stepCount={sectionFlow.length - 1}
          style={{ marginBottom: 0 }}
        />
      )}
    </div>
  );
};

export default InterventionViewerStatus;
