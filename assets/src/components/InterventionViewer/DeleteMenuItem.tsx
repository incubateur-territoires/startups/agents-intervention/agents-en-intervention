import { useCallback } from 'react';

import { useApiInterventionsIdDelete } from '@aei/src/client/generated/components';
import { DropdownMenuItem } from '@aei/src/components/DropdownMenu';
import { useSingletonConfirmationDialog } from '@aei/src/ui/modal/useModal';
import { useUser } from '@aei/src/utils/auth';
import { InterventionInterventionview } from '@aei/types/InterventionInterventionview';

export interface DeleteMenuItemProps {
  intervention: InterventionInterventionview | null;
  onSuccess?: () => void | Promise<void>;
}

const DeleteMenuItem = ({ intervention, onSuccess }: DeleteMenuItemProps) => {
  const deleteIntervention = useApiInterventionsIdDelete();
  const { showConfirmationDialog } = useSingletonConfirmationDialog();
  const { isAgent } = useUser();

  if (isAgent) {
    return null;
  }

  const deleteInterventionAction = useCallback(async () => {
    if (!intervention) return;
    showConfirmationDialog({
      description: intervention.recurring ? (
        <>Vous allez supprimer cette récurrence ainsi que toutes les interventions qui y sont associées. En êtes-vous sûr ?</>
      ) : (
        <>Êtes-vous sûr de vouloir supprimer l&apos;intervention #{intervention.logicId} ?</>
      ),
      onConfirm: () =>
        deleteIntervention
          .mutateAsync({
            pathParams: {
              id: `${intervention.id}`,
            },
          })
          .then(() => {
            onSuccess && onSuccess();
          }),
    });
  }, []);

  return (
    <DropdownMenuItem onClick={deleteInterventionAction}>
      <span className="fr-icon-delete-line" aria-hidden="true"></span>
      Supprimer
    </DropdownMenuItem>
  );
};

export default DeleteMenuItem;
