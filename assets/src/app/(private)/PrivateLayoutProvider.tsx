import { ReactNode, createContext, useContext } from 'react';

export interface PrivateLayoutContextType {
  mainTitle: string | null;
  useSetMainTitle: (title: string | null) => void;
  sideNavFooter: ReactNode;
  setSideNavFooter: (footer: ReactNode) => void;
}

export const PrivateLayoutContext = createContext<PrivateLayoutContextType>({
  mainTitle: null,
  useSetMainTitle: () => {},
  sideNavFooter: null,
  setSideNavFooter: () => {},
});

export const usePrivateLayoutContext = () => {
  return useContext(PrivateLayoutContext);
};
