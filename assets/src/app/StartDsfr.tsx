import { startReactDsfr } from '@codegouvfr/react-dsfr/next-appdir';

startReactDsfr({ defaultColorScheme: 'light' });

export function StartDsfr() {
  return null;
}

export default StartDsfr;
