import {
  InterventionCommentAuthorSchema,
  InterventionCommentAuthorSchemaType,
  InterventionCommentSchema,
  InterventionCommentSchemaType,
  InterventionLocationSchema,
  InterventionLocationSchemaType,
  InterventionParticipantSchema,
  InterventionParticipantSchemaType,
  InterventionPictureSchema,
  InterventionPictureSchemaType,
  InterventionPrioritySchema,
  InterventionSchema,
  InterventionSchemaType,
  InterventionStatusSchema,
  InterventionTypeCategorySchema,
  InterventionTypeCategorySchemaType,
  InterventionTypeSchema,
  InterventionTypeSchemaType,
} from '@aei/src/models/entities/intervention';

export const categories: InterventionTypeCategorySchemaType[] = [
  InterventionTypeCategorySchema.parse({
    id: 0,
    name: 'Espaces verts',
    description: 'Vitae itaque eaque.',
    picture: 'https://via.placeholder.com/300x150',
    position: 1,
  }),
  InterventionTypeCategorySchema.parse({
    id: 1,
    name: 'Omnis',
    description: 'Quis eum dolor rerum voluptatem.',
    picture: 'https://via.placeholder.com/300x150',
    position: 2,
  }),
  InterventionTypeCategorySchema.parse({
    id: 2,
    name: 'Maborum distinctio eos',
    description: 'Explicabo enim corporis reprehenderit accusamus laboriosam vero quis in necessitatibus.',
    picture: 'https://via.placeholder.com/300x150',
    position: 3,
  }),
];

export const types: InterventionTypeSchemaType[] = [
  InterventionTypeSchema.parse({
    id: 0,
    name: 'Eveniet',
    description: 'Deserunt qui totam dolore similique qui.',
    picture: 'https://via.placeholder.com/300x150',
    category: categories[0],
    position: 1,
  }),
  InterventionTypeSchema.parse({
    id: 2,
    name: 'Perspiciatis',
    description: 'Temporibus eius vel dicta aut tenetur iste commodi quisquam.',
    picture: 'https://via.placeholder.com/300x150',
    category: categories[0],
    position: 2,
  }),
  InterventionTypeSchema.parse({
    id: 3,
    name: 'Provident',
    description: 'Voluptas porro voluptatem.',
    picture: 'https://via.placeholder.com/300x150',
    category: categories[1],
    position: 1,
  }),
];

export const locations: InterventionLocationSchemaType[] = [
  InterventionLocationSchema.parse({
    street: '262 Gertrude Delesseux',
    rest: null,
    postcode: '29200',
    city: 'Brest',
    latitude: '48.8885',
    longitude: '2.386151',
  }),
  InterventionLocationSchema.parse({
    street: '79422 Hermine Vaneau',
    rest: null,
    postcode: '35000',
    city: 'Rennes',
    latitude: '48.1146',
    longitude: '-1.677801',
  }),
  InterventionLocationSchema.parse({
    street: '10965 Adel de Provence',
    rest: null,
    postcode: '75019',
    city: 'Paris',
    latitude: '48.389711',
    longitude: '-4.485278',
  }),
];

export const participants: InterventionParticipantSchemaType[] = [
  InterventionParticipantSchema.parse({
    id: 1,
    firstname: 'Romain',
    lastname: 'Garcia',
    picture: null,
  }),
  InterventionParticipantSchema.parse({
    id: 2,
    firstname: 'Aminte',
    lastname: 'Bertrand',
    picture: null,
  }),
  InterventionParticipantSchema.parse({
    id: 3,
    firstname: 'Agathon',
    lastname: 'Louis',
    picture: null,
  }),
];

export const pictures: InterventionPictureSchemaType[] = [
  InterventionPictureSchema.parse({
    id: 1,
    fileName: 'interventions/fake.jpg',
    url: 'https://via.placeholder.com/300x150',
    tag: 'before',
  }),
  InterventionPictureSchema.parse({
    id: 2,
    fileName: 'interventions/fake.jpg',
    url: 'https://via.placeholder.com/300x150',
    tag: 'before',
  }),
  InterventionPictureSchema.parse({
    id: 3,
    fileName: 'interventions/fake.jpg',
    url: 'https://via.placeholder.com/300x150',
    tag: 'before',
  }),
];

export const commentsAuthors: InterventionCommentAuthorSchemaType[] = [
  InterventionCommentAuthorSchema.parse({
    id: 1,
    firstname: 'Arsinoé',
    lastname: 'Lopez',
  }),
  InterventionCommentAuthorSchema.parse({
    id: 2,
    firstname: 'Clara',
    lastname: 'Richard',
  }),
  InterventionCommentAuthorSchema.parse({
    id: 3,
    firstname: 'Balthazar',
    lastname: 'Fournier',
  }),
];

export const comments: InterventionCommentSchemaType[] = [
  InterventionCommentSchema.parse({
    message: 'Et voluptatibus qui ducimus error. Earum veritatis culpa sint magni asperiores veritatis. Qui ipsa temporibus sed. Et neque id.',
    author: commentsAuthors[0],
    createdAt: new Date('December 17, 2022 03:24:00 UTC'),
  }),
  InterventionCommentSchema.parse({
    message: `Officiis ipsa velit non et nemo a. Labore est cupiditate porro magnam eum consequuntur sit sed qui. Quia accusamus dolores quia et est non sed consequatur autem. Sed dolores suscipit sed autem recusandae laudantium. Aspernatur asperiores quam ducimus molestias non. Nulla facilis sed culpa omnis dolore praesentium voluptatem doloremque qui.

Amet dolorum accusantium voluptatum aut. Consequatur illum sed nostrum est. Quo earum necessitatibus nisi cupiditate. Et perferendis vero ipsam ut ab soluta mollitia aliquid sed.

Qui officia expedita architecto suscipit qui aut. Ut minima iusto qui autem nobis sint accusamus. Molestiae expedita fugiat optio enim a esse quod. Quia est ut labore. Placeat in harum modi quisquam repudiandae. Ut libero atque esse deleniti ea.`,
    author: commentsAuthors[1],
    createdAt: new Date('December 18, 2022 03:24:00 UTC'),
  }),
  InterventionCommentSchema.parse({
    message:
      'Quis sint incidunt quod rerum omnis eum quo. Quam ratione deserunt voluptas dolores ea alias aut nulla. Natus eos quas magni ab reprehenderit et et.',
    author: commentsAuthors[2],
    createdAt: new Date('December 19, 2022 03:24:00 UTC'),
  }),
];

export const interventions: InterventionSchemaType[] = [
  InterventionSchema.parse({
    id: 1,
    logicId: 1,
    type: types[0],
    category: categories[0],
    description: 'Architecto voluptatem quas dolorem impedit totam iure rerum. Qui at minima. Nihil voluptas unde ipsa eum dolores optio eius.',
    location: locations[0],
    priority: InterventionPrioritySchema.Values.normal,
    status: InterventionStatusSchema.Values['to-do'],
    endedAt: null,
    participants: [participants[0], participants[1]],
    pictures: [pictures[0]],
    comments: [comments[0], comments[2], comments[2]],
    createdAt: new Date('December 17, 2022 03:24:00 UTC'),
  }),
  InterventionSchema.parse({
    id: 2,
    logicId: 2,
    type: types[1],
    category: categories[1],
    description:
      'Sit id qui animi. Dolor nihil accusamus repellendus quae officiis vero. Est necessitatibus est alias quam sint occaecati deserunt odio aut. Nam dolorem nemo qui fuga iure.',
    location: locations[1],
    priority: InterventionPrioritySchema.Values.urgent,
    status: InterventionStatusSchema.Values['in-progress'],
    endedAt: null,
    participants: [participants[1]],
    pictures: [pictures[0], pictures[1]],
    comments: [comments[2], comments[2]],
    createdAt: new Date('December 17, 2022 03:24:00 UTC'),
  }),
  InterventionSchema.parse({
    id: 3,
    logicId: 3,
    type: types[2],
    category: categories[2],
    description:
      'Dolor voluptatem distinctio ratione omnis incidunt. Sunt libero nisi nostrum qui fugiat fuga ex perferendis quo. Reprehenderit qui quae aut nostrum. Explicabo inventore accusamus. Vel est quod numquam perferendis maiores odio suscipit.',
    location: locations[2],
    priority: InterventionPrioritySchema.Values.normal,
    status: InterventionStatusSchema.Values['blocked'],
    endedAt: null,
    participants: [participants[2]],
    pictures: [pictures[2]],
    comments: [comments[2]],
    createdAt: new Date('December 17, 2022 03:24:00 UTC'),
  }),
  InterventionSchema.parse({
    id: 4,
    logicId: 4,
    type: types[2],
    category: categories[2],
    description:
      'Dolor voluptatem distinctio ratione omnis incidunt. Sunt libero nisi nostrum qui fugiat fuga ex perferendis quo. Reprehenderit qui quae aut nostrum. Explicabo inventore accusamus. Vel est quod numquam perferendis maiores odio suscipit.',
    location: locations[2],
    priority: InterventionPrioritySchema.Values.normal,
    status: InterventionStatusSchema.Values['finished'],
    endedAt: null,
    participants: [participants[2]],
    pictures: [pictures[2]],
    comments: [comments[2]],
    createdAt: new Date('December 17, 2022 03:24:00 UTC'),
  }),
];
