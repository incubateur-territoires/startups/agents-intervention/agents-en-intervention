import { EmployerEmployerViewEmployerGet } from '../client/generated/schemas';

export const employers: EmployerEmployerViewEmployerGet[] = [
  {
    id: 1,
    name: 'commune',
    siren: 'SIREN',
    latitude: '48.5',
    longitude: '3',
  },
];
