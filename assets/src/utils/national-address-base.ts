import { LatLng } from 'leaflet';

export const baseUrl = 'https://nominatim.openstreetmap.org';

export interface LocationApiPoint {
  coordinates: [number, number];
}

export interface LocationApiProperties {
  place_id: string;
  name: string; // Corresponds to `number + street`
  display_name: string;

  address: {
    street?: string;
    road?: string;
    house_number?: string;
    hamlet?: string;
    postcode: string;
    city?: string;
    village?: string;
    town?: string;
    municipality?: string;
  };
}

export interface LocationApiFeature {
  geometry: LocationApiPoint;
  properties: LocationApiProperties;
}

export interface LocationApiSearchByCoordinatesResponse {
  features: LocationApiFeature[];
}

export interface LocationApiSearchResponse {
  features: LocationApiFeature[];
}

export async function searchByCoordinates(coordinates: LatLng): Promise<LocationApiFeature | null> {
  // Limit to 1 since addresses are sorted on distance already
  const response = await window.fetch(`${baseUrl}/reverse?lat=${coordinates.lat}&lon=${coordinates.lng}&limit=1&format=geojson`);

  if (response.ok) {
    const data = (await response.json()) as LocationApiSearchByCoordinatesResponse;

    if (data.features.length) {
      const address = data.features[0].properties.address;

      let street = undefined;
      if (address.house_number || address.street || address.road || address.hamlet) {
        street = (address.house_number ? address.house_number + ' ' : '') + (address.street || address.road || address.hamlet || '');
      }
      return {
        ...data.features[0],
        properties: {
          ...data.features[0].properties,
          address: {
            ...address,
            street,
            city: address.city || address.village,
          },
        },
      };
    }

    return null;
  } else {
    throw await response.json();
  }
}

export async function search(value: string): Promise<LocationApiFeature[]> {
  if (value.replace(' ', '').length < 3) {
    return [];
  }

  // Wanted to add `lat` and `lon` as query parameter to return address around this position
  // but after multiple tests it does not seem to work well
  const url = new URL('https://api-adresse.data.gouv.fr/search');
  url.searchParams.append('q', value);

  const response = await window.fetch(url);

  if (response.ok) {
    const data = (await response.json().then((results) => {
      results.features = results.features.map((result: any) => {
        result.properties.display_name = result.properties.label;
        result.properties.address = {
          street: result.properties.street,
          hamlet: result.properties.hamlet,
          postcode: result.properties.postcode,
          city: result.properties.city,
        };
        /** @var LocationApiFeature */
        return result;
      });
      return results;
    })) as LocationApiSearchResponse;

    return data.features;
  } else {
    throw await response.json();
  }
}
