import { addressFormatterFormat } from './address';

describe('addressFormatterFormat', () => {
  it('should format address in one line', () => {
    const address = { street: '123 Main St', city: 'Anytown', postcode: '12345' };
    const result = addressFormatterFormat(address, false);
    expect(result).toBe('123 Main St, 12345 Anytown');
  });

  it('should format address in two lines', () => {
    const address = { street: '123 Main St', city: 'Anytown', postcode: '12345' };
    const result = addressFormatterFormat(address, true);
    expect(result).toBe('123 Main St\n12345 Anytown');
  });

  it('should handle missing street', () => {
    const address = { city: 'Anytown', postcode: '12345' };
    const result = addressFormatterFormat(address, false);
    expect(result).toBe('12345 Anytown');
  });

  it('should handle missing city', () => {
    const address = { street: '123 Main St', postcode: '12345' };
    const result = addressFormatterFormat(address, false);
    expect(result).toBe('123 Main St, 12345');
  });

  it('should handle missing postcode', () => {
    const address = { street: '123 Main St', city: 'Anytown' };
    const result = addressFormatterFormat(address, false);
    expect(result).toBe('123 Main St, Anytown');
  });

  it('should handle all fields missing', () => {
    const address = {};
    const result = addressFormatterFormat(address, false);
    expect(result).toBe('Adresse introuvable');
  });

  it('should handle fallback', () => {
    const address = { fallback: 'Fallback address' };
    const result = addressFormatterFormat(address, false);
    expect(result).toBe('Fallback address');
  });
});
