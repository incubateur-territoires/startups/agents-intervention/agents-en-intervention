import { useEffect, useState } from 'react';

export function getBaseUrl() {
  return window.location.origin;
}

export function getApiUrl() {
  return window.location.origin + '/api';
}

export function removeHashPrefix(hash: string) {
  return hash.substring(1);
}

export function useHashChange() {
  const [hash, setHash] = useState(() => removeHashPrefix(window.location.hash));

  useEffect(() => {
    const handleHashChange = (event: HashChangeEvent) => {
      const newUrl = new URL(event.newURL);
      const pureHash = removeHashPrefix(newUrl.hash);

      setHash(pureHash);
    };

    window.addEventListener('hashchange', handleHashChange);

    return () => {
      window.removeEventListener('hashchange', handleHashChange);
    };
  }, []);

  return {
    hash,
    setHash: (hash: string) => {
      window.location.hash = hash;
    },
  };
}
