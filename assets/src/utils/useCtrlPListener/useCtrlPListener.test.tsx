import { fireEvent } from '@testing-library/dom';
import { render } from '@testing-library/react';
import React from 'react';

import useCtrlPListener from './useCtrlPListener';

const TestComponent = () => {
  const [message, setMessage] = React.useState('Press Ctrl+P or Cmd+P');

  useCtrlPListener(() => {
    setMessage('Ctrl+P or Cmd+P pressed!');
  });

  return <p>{message}</p>;
};

describe('useCtrlPListener', () => {
  test('should update message when Ctrl+P is pressed', () => {
    const { getByText } = render(<TestComponent />);
    const messageElement = getByText('Press Ctrl+P or Cmd+P');

    fireEvent.keyDown(document, { key: 'p', ctrlKey: true });
    expect(messageElement).toHaveTextContent('Ctrl+P or Cmd+P pressed!');
  });

  test('should update message when Cmd+P is pressed', () => {
    const { getByText } = render(<TestComponent />);
    const messageElement = getByText('Press Ctrl+P or Cmd+P');

    fireEvent.keyDown(document, { key: 'p', metaKey: true });
    expect(messageElement).toHaveTextContent('Ctrl+P or Cmd+P pressed!');
  });
});
