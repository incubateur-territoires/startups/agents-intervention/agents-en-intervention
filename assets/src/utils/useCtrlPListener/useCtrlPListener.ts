import { useEffect } from 'react';

const useCtrlPListener = (callback: () => void) => {
  useEffect(() => {
    const listener = (e: KeyboardEvent) => {
      if ((e.ctrlKey || e.metaKey) && e.key === 'p') {
        callback();
        e.preventDefault();
        e.stopPropagation();
      }
      return;
    };
    document.addEventListener('keydown', listener);
    return () => {
      document.removeEventListener('keydown', listener);
    };
  }, []);
};

export default useCtrlPListener;
