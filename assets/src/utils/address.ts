export function addressFormatterFormat(
  address: { street?: string | null; city?: string | null; postcode?: string | null; fallback?: string },
  twoLines: boolean = false
): string {
  if (!address.street && !address.city && !address.postcode) {
    return address.fallback || 'Adresse introuvable';
  }

  return `${address.street ? address.street : ''}${twoLines ? '\n' : ''}${!twoLines && address.street && (address.city || address.postcode) ? ', ' : ''}${address.postcode ? address.postcode + ' ' : ''}${address.city || ''}`.trim();
}
