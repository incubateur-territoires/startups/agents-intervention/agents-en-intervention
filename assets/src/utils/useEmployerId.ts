const useEmployerId = (): number => {
  return parseInt(window.location.pathname.split('/')[1]);
};

export default useEmployerId;
