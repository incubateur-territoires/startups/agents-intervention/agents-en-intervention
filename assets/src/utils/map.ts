import { Icon } from 'leaflet';
import type { TileLayer } from 'leaflet';
import type { RefAttributes } from 'react';
import type { TileLayerProps } from 'react-leaflet';

import bluePin from '@aei/src/assets/map/blue-pin.svg';
import greenPin from '@aei/src/assets/map/green-pin.svg';
import redPin from '@aei/src/assets/map/red-pin.svg';

// For now all environments use the same token, so we harcode it
const jawgAccessToken = '8wqzwUmgq5OKhGlDMEhxWrqJce7Bof84Fn8yfs3TiJtXHOdMaID1p8eAeqcdMt2t';

export const tileLayerDefaultProps: TileLayerProps & RefAttributes<TileLayer> = {
  url: `https://{s}.tile.jawg.io/jawg-sunny/{z}/{x}/{y}{r}.png?access-token=${jawgAccessToken}`,
  // @ts-ignore: property does not exist in the types but still, works
  'layer-type': 'base',
  // @ts-ignore: property does not exist in the types but still, works
  name: 'Jawg.Streets',
};

export const fabButtonSpacing = '5vh';

export const redPinIcon = new Icon({
  iconUrl: redPin,
  iconRetinaUrl: redPin,
  popupAnchor: [-0, -0],
  iconSize: [32, 32],
});

export const bluePinIcon = new Icon({
  iconUrl: bluePin,
  iconRetinaUrl: bluePin,
  popupAnchor: [-0, -0],
  iconSize: [32, 32],
});

export const greenPinIcon = new Icon({
  iconUrl: greenPin,
  iconRetinaUrl: greenPin,
  popupAnchor: [-0, -0],
  iconSize: [32, 32],
});

export async function getCurrentPosition(): Promise<GeolocationPosition | null> {
  return new Promise((resolve, reject) => {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(
        (location) => {
          resolve(location);
        },
        (error) => {
          console.error('Cannot get the custom geolocation:', error);
          navigator.permissions
            .query({ name: 'geolocation' })
            .then((status) => {
              if (status.state === 'denied') {
                window.alert('Veuillez autoriser la géolocalisation dans les paramètres de votre navigateur pour utiliser cette fonctionnalité');
              }
            })
            .catch(console.error);
          resolve(null);
        }
      );
    } else {
      console.log('Geolocation is not available in this browser');

      resolve(null);
    }
  });
}
