import { useMedia } from 'react-use';

/**
 * Breakpoints du DSFR
 */
export const breakpoints = {
  sm: 576,
  md: 768,
  lg: 992,
  xl: 1248,
};

const useMobileFormat = () => {
  return useMedia(`(max-width: ${breakpoints.sm}px)`);
};

const useMediumFormat = () => {
  return useMedia(`(max-width: ${breakpoints.md}px)`);
};

const useLargeFormat = () => {
  return useMedia(`(min-width: ${breakpoints.md}px)`);
};

export default useMobileFormat;
export { useMobileFormat, useMediumFormat, useLargeFormat };
