import * as Sentry from '@sentry/react';
import { useCallback, useEffect, useState } from 'react';

import { shouldTargetMock } from '@aei/src/client/mock/environment';
import { TokenUserSchemaType } from '@aei/src/models/entities/user';
import { isBrowser } from '@aei/src/utils/platform';
import { getApiUrl } from '@aei/src/utils/url';
import { Role } from '@aei/types/Role';

const _parseJwt = (token: string) => {
  var base64Url = token.split('.')[1];
  var base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
  var jsonPayload = decodeURIComponent(
    window
      .atob(base64)
      .split('')
      .map(function (c) {
        return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
      })
      .join('')
  );

  return JSON.parse(jsonPayload);
};

const getUser = () => {
  const user =
    shouldTargetMock() || window === undefined
      ? parseJwt(
          'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJpYXQiOjE2OTM0ODM4MjcsImV4cCI6MTY5MzUxOTgyNywicm9sZXMiOlsiUk9MRV9ESVJFQ1RPUiJdLCJ1c2VybmFtZSI6ImRpcmVjdGV1ciIsInVzZXJJZCI6MSwiZmlyc3RuYW1lIjoiRWxsaW90dCIsImxhc3RuYW1lIjoiVG9ycGh5IiwiZW1wbG95ZXJJZCI6MX0.Xvlgdbkz9wRE0qJgwkjxDR595XtuhoQBYYz_dMn443MsMWT7XdH-9aAvdrleYR99Rq2-NL6Y1f3E-VNJpBIaLNzXKsPiqJZU-p6egFMPHpIUe9Ti1-_UF4sWwKzSw-X095HjXL6oW7V7ICXqKZffWVbLlxxAg_xuAFuce_k7kYX4VLx9KIQUSvFNFj1gM-PLFqMDpQ7qwRofm-dY6VwXbQ6L5MY3SkBrLktKNzkiBkp75-8Ve3seckZ5DpOCfD4yBVkOe86708U8rEQGRdfV9lXUih1BDdqP4lOhhEW1DJjBn0NaSESIC69GY2jfj8dwJxhpAokv2njwDLfddKQq6g'
        )
      : // @ts-ignore
        JSON.parse(window.USER);

  if (user) {
    return user;
  } else {
    return null;
  }
};

let logout: () => void,
  parseJwt: Function,
  useUser: () => {
    loading: boolean;
    goToSignIn: Function;
    signIn: (credentials: { login: string; password: string }) => Promise<TokenUserSchemaType>;
    token: string | null;
    user: TokenUserSchemaType | null;
    hasRole: (role: Role) => boolean;
    isAdmin: boolean;
    isAgent: boolean;
    isCoordinateurOrAdminEmployer: boolean;
  };

// :poop: but is like next works
if (!isBrowser) {
  // Since we manage only a JWT with no business server logic in Node.js, we can mock it
  logout = () => {};
  parseJwt = () => Promise.reject('should not be called on server side');
  useUser = () => ({
    loading: false,
    goToSignIn: () => null,
    signIn: () => Promise.reject('should not be called on server side'),
    token: null,
    user: null,
    hasRole: () => false,
    isAdmin: false,
    isAgent: false,
    isCoordinateurOrAdminEmployer: false,
  });
} else {
  logout = () => {
    window.location.replace('/deconnexion');
    // window.WonderPush?.push(['unsetUserId']);
    Sentry.setUser(null);
  };

  parseJwt = _parseJwt;

  useUser = () => {
    const [loading, setLoading] = useState<boolean>(true);
    const [token, setToken] = useState<string | null>(null);
    const [user, setUser] = useState<TokenUserSchemaType | null>(null);

    // Init of values should be in a useEffect to not mess with `user` changing at init while rendering components
    useEffect(() => {
      const tokenFromStorage =
        shouldTargetMock() || window === undefined
          ? 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJpYXQiOjE2OTM0ODM4MjcsImV4cCI6MTY5MzUxOTgyNywicm9sZXMiOlsiUk9MRV9ESVJFQ1RPUiJdLCJ1c2VybmFtZSI6ImRpcmVjdGV1ciIsInVzZXJJZCI6MSwiZmlyc3RuYW1lIjoiRWxsaW90dCIsImxhc3RuYW1lIjoiVG9ycGh5IiwiZW1wbG95ZXJJZCI6MX0.Xvlgdbkz9wRE0qJgwkjxDR595XtuhoQBYYz_dMn443MsMWT7XdH-9aAvdrleYR99Rq2-NL6Y1f3E-VNJpBIaLNzXKsPiqJZU-p6egFMPHpIUe9Ti1-_UF4sWwKzSw-X095HjXL6oW7V7ICXqKZffWVbLlxxAg_xuAFuce_k7kYX4VLx9KIQUSvFNFj1gM-PLFqMDpQ7qwRofm-dY6VwXbQ6L5MY3SkBrLktKNzkiBkp75-8Ve3seckZ5DpOCfD4yBVkOe86708U8rEQGRdfV9lXUih1BDdqP4lOhhEW1DJjBn0NaSESIC69GY2jfj8dwJxhpAokv2njwDLfddKQq6g'
          : // @ts-ignore
            window.JWT;

      setToken(tokenFromStorage);

      // @ts-ignore
      if (window.USER) {
        // @ts-ignore
        setUser(JSON.parse(window.USER));
      } else if (tokenFromStorage) {
        setUser(parseJwt(tokenFromStorage));
      }

      setLoading(false);
    }, []);

    const signIn = useCallback((credentials: { login: string; password: string }): Promise<TokenUserSchemaType> => {
      // TODO: parse with zod SignInSchema
      if (!credentials.login || !credentials.password) {
        throw new Error('credentials_required');
      }
      return fetch(getApiUrl() + '/authentication', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(credentials),
      })
        .then((response) => response.json())
        .then((res) => {
          if (res.token) {
            window.localStorage.setItem('auth-token', res.token);

            setToken(res.token);
            const parsedToken = parseJwt(res.token);
            setUser(parsedToken);
            // FIXME window.WonderPush?.push(['setUserId', parsedToken.userId]);

            return parsedToken;
          } else {
            throw new Error(res.message);
          }
        });
    }, []);

    const goToSignIn = useCallback(() => window.location.replace('/connexion?redirect=' + encodeURI(window.location.href)), []);

    const hasRole = useCallback((role: Role) => !!user?.roles.includes(role), [user?.roles]);

    return {
      loading,
      goToSignIn,
      signIn,
      token: token,
      user: user,
      hasRole,
      isAdmin: hasRole(Role.Admin),
      isAdminEmployer: hasRole(Role.AdminEmployer),
      isAgent: hasRole(Role.Agent),
      isCoordinateurOrAdminEmployer: hasRole(Role.Director) || hasRole(Role.AdminEmployer) || hasRole(Role.Admin),
    };
  };
}

export { logout, getUser, useUser };
