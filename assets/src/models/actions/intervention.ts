import z from 'zod';

import { InterventionSchema } from '@aei/src/models/entities/intervention';
import { PictureType } from '@aei/src/models/entities/picture';
import { Frequency } from '@aei/types';

export const MutateInterventionSchema = z.object({
  title: z.string(),
  beforePicture: z.nullable(PictureType),
  afterPicture: z.nullable(PictureType),
  typeId: z.string(),
  categoryId: z.string(),
  description: InterventionSchema.shape.description,
  location: InterventionSchema.shape.location,
  priority: z.boolean(),
  participantsIds: z.array(z.string()),

  startAt: z.coerce.date().optional().nullable(),
  endAt: z.coerce.date().optional().nullable(),
  endedAt: z.coerce.date().optional(),
  dueDate: z.coerce.date().optional(),
  frequency: z.enum([Frequency.ONE_TIME, Frequency.DAILY, Frequency.WEEKLY, Frequency.MONTHLY, Frequency.YEARLY]),
  frequencyInterval: z.number(),
});
export type MutateInterventionSchemaType = z.infer<typeof MutateInterventionSchema>;

export const MutateInterventionPrefillSchema = MutateInterventionSchema.deepPartial();
export type MutateInterventionPrefillSchemaType = z.infer<typeof MutateInterventionPrefillSchema>;

export const AssignInterventionParticipantsSchema = z.object({
  participantsIds: z.array(z.string()),
});
export type AssignInterventionParticipantsSchemaType = z.infer<typeof AssignInterventionParticipantsSchema>;

export const AddInterventionCommentSchema = z.object({
  content: z.string().min(1),
});
export type AddInterventionCommentSchemaType = z.infer<typeof AddInterventionCommentSchema>;
