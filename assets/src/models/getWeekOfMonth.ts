export function getWeekOfMonth(date: Date) {
  const day = date.getDate();
  const dayOfWeek = date.getDay();

  // Trouver le premier jour de la semaine du mois
  const firstDayOfMonth = new Date(date.getFullYear(), date.getMonth(), 1);
  let firstDayOfWeek = firstDayOfMonth.getDay();

  // Trouver la première occurrence du jour de la semaine dans le mois
  let firstOccurrenceDate;
  if (firstDayOfWeek <= dayOfWeek) {
    firstOccurrenceDate = dayOfWeek - firstDayOfWeek + 1;
  } else {
    firstOccurrenceDate = 7 - (firstDayOfWeek - dayOfWeek) + 1;
  }

  // Calculer combien de fois ce jour de la semaine est apparu jusqu'à la date donnée
  return Math.ceil((day - firstOccurrenceDate + 1) / 7);
}
