import z from 'zod';

import { Role } from '@aei/types';

export const UserRoleSchema = z.enum([Role.Agent, Role.Director, Role.Elected, Role.Admin, Role.AdminEmployer]);

export const UserStatusSchema = z.enum(['REGISTERED', 'CONFIRMED', 'DISABLED']);

export const UserSchema = z
  .object({
    // TODO: add professional phone for colleagues?
    id: z.number().positive(),
    username: z.string(),
    firstname: z.string().min(1),
    lastname: z.string().min(1),
    email: z.string().email(),
    status: UserStatusSchema,
    profilePicture: z.string().url().nullable(),
    lastActivityAt: z.date().nullable(),
    createdAt: z.date(),
    updatedAt: z.date(),
    deletedAt: z.date().nullable(),
    employerId: z.number().positive(),
    roles: z.array(UserRoleSchema),
  })
  .strict();

// This is a lite version of the user entity to be available on the frontend
export const TokenUserSchema = z
  .object({
    username: UserSchema.shape.username,
    firstname: UserSchema.shape.firstname,
    lastname: UserSchema.shape.lastname,
    email: UserSchema.shape.email,
    profilePicture: UserSchema.shape.profilePicture,
    id: UserSchema.shape.id,
    employer: z.string(),
    roles: UserSchema.shape.roles,
  })
  .strict();
export type TokenUserSchemaType = z.infer<typeof TokenUserSchema>;
