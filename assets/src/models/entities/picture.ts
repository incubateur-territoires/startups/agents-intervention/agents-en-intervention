import z from 'zod';

import { PicturePictureview, PictureTag } from '@aei/types';

export const PictureType = z.object({
  id: z.number().min(1),
  fileName: z.string(),
  url: z.string().optional().nullable(),
  tag: z.enum([PictureTag.After, PictureTag.Before, PictureTag.Avatar]).nullable(),
  small: z.string().nullable(),
  medium: z.string().nullable(),
  original: z.string().nullable(),
});

export type Picture = PicturePictureview;
