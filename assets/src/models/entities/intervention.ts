import z from 'zod';

export const InterventionPrioritySchema = z.enum(['normal', 'urgent']);

export const InterventionStatusSchema = z.enum(['blocked', 'finished', 'in-progress', 'to-do', 'recurring', 'to-validate', 'rejected', 'draft']);

export const InterventionTypeCategorySchema = z
  .object({
    id: z.number(),
    name: z.string(),
    description: z.string().nullish(),
    picture: z.string(),
    position: z.number().nullable().or(z.undefined()),
  })
  .strict();
export type InterventionTypeCategorySchemaType = z.infer<typeof InterventionTypeCategorySchema>;

export const InterventionTypeSchema = z
  .object({
    id: z.number(),
    name: z.string(),
    description: z.string().nullish(),
    picture: z.string(),
    category: InterventionTypeCategorySchema,
    position: z.number(),
  })
  .strict();
export type InterventionTypeSchemaType = z.infer<typeof InterventionTypeSchema>;

export const InterventionLocationSchema = z
  .object({
    street: z.string().nullish(),
    rest: z.string().nullish(),
    postcode: z.string().nullish(),
    city: z.string().nullish(),
    longitude: z.string(),
    latitude: z.string(),
    fallback: z.string().nullish(),
  })
  .strict();
export type InterventionLocationSchemaType = z.infer<typeof InterventionLocationSchema>;

export const InterventionParticipantSchema = z
  .object({
    id: z.number(),
    firstname: z.string(),
    lastname: z.string(),
    picture: z.string().nullish(),
    role: z.string().nullish(),
  })
  .strict();
export type InterventionParticipantSchemaType = z.infer<typeof InterventionParticipantSchema>;

export const InterventionPictureTagSchema = z.enum(['before', 'after', 'avatar']);
export type InterventionPictureTagSchemaType = z.infer<typeof InterventionPictureTagSchema>;

export const InterventionPictureSchema = z.object({
  id: z.number(),
  url: z.string().nullish(),
  fileName: z.string(),
  tag: InterventionPictureTagSchema.nullish(),
});
// .strict();
export type InterventionPictureSchemaType = z.infer<typeof InterventionPictureSchema>;

export const InterventionCommentAuthorSchema = z
  .object({
    id: z.number(),
    firstname: z.string(),
    lastname: z.string(),
  })
  .strict();
export type InterventionCommentAuthorSchemaType = z.infer<typeof InterventionCommentAuthorSchema>;

export const InterventionCommentSchema = z
  .object({
    message: z.string(),
    author: InterventionCommentAuthorSchema,
    createdAt: z.date(),
  })
  .strict();
export type InterventionCommentSchemaType = z.infer<typeof InterventionCommentSchema>;

export const InterventionSchema = z
  .object({
    id: z.number(),
    logicId: z.number(),
    title: z.string(),
    type: InterventionTypeSchema,
    category: InterventionTypeCategorySchema,
    description: z.string(),
    location: InterventionLocationSchema,
    priority: InterventionPrioritySchema,
    status: InterventionStatusSchema,
    endedAt: z.date().nullable(),
    participants: z.array(InterventionParticipantSchema),
    pictures: z.array(InterventionPictureSchema),
    comments: z.array(InterventionCommentSchema),
    createdAt: z.date(),
    author: z.object({
      firstname: z.string(),
      lastname: z.string(),
    }),
    frequency: z.enum(['onetime', 'daily', 'weekly', 'monthly', 'yearly']),
    frequencyInterval: z.number(),
    startAt: z.date().optional(),
    dueDate: z.date(),
    endAt: z.date().optional(),
    recurring: z.boolean(),
  })
  .strict();
export type InterventionSchemaType = z.infer<typeof InterventionSchema>;
