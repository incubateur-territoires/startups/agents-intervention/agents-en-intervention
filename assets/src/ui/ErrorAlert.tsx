import Alert from '@codegouvfr/react-dsfr/Alert';
import Button from '@codegouvfr/react-dsfr/Button';

// import { QueryObserverResult, RefetchOptions } from '@tansack/query-core';

export interface ErrorAlertProps {
  errors: (Error | any)[] | Error;
  refetchs?: ((options?: any) => Promise<any>)[] | Function;
}

export function ErrorAlert(props: ErrorAlertProps) {
  const _errors = Array.isArray(props.errors) ? props.errors : [props.errors];

  if (_errors.length === 0) {
    return <></>;
  }

  const _r = Array.isArray(props.refetchs) ? props.refetchs : [props.refetchs];

  const retry = props.refetchs
    ? () =>
        Promise.all(
          _r?.map((refetch) => {
            return refetch && refetch();
          })
        )
    : null;

  // TODO: if an error has a 5xx HTTP code we allow retrying
  const containsServerError: boolean = true;

  let errors: string[] = _errors.map((error) => {
    // TODO: if there is a code error, try to translate
    return error?.message ?? error.toString();
  });

  // Remove duplicates since it has no value
  // @ts-ignore
  errors = [...new Set(errors)];

  return (
    <Alert
      severity="error"
      title={errors.length > 1 ? 'Plusieurs erreurs ont été rencontrées :' : 'Une erreur est survenue :'}
      description={
        <div className="aei-stack aei-stack-large-gap">
          {errors.length === 1 ? (
            <>{errors[0]}</>
          ) : (
            <ul>
              {errors.map((error) => {
                return <li key={error}>{error}</li>;
              })}
            </ul>
          )}
          {retry && containsServerError && (
            <Button
              onClick={retry}
              size="large"
              iconId="fr-icon-refresh-line"
              style={{
                display: 'flex',
                marginTop: 2,
                marginLeft: 'auto',
                marginRight: 'auto',
                color: 'white',
                backgroundColor: '#ce0500',
              }}
              title="Réessayer"
            >
              Réessayer
            </Button>
          )}
        </div>
      }
    />
  );
}
