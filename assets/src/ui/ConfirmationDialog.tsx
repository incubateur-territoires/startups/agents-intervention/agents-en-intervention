import React, { JSX, useCallback, useRef, useState } from 'react';

import { ErrorAlert } from '@aei/src/ui/ErrorAlert';
import Modal from '@aei/src/ui/modal/Modal';

export interface ConfirmationDialogProps {
  title?: string;
  description?: string | JSX.Element;
  onConfirm: () => Promise<void>;
  onCancel?: () => Promise<void>;
  onClose: () => void;
}

export const ConfirmationDialog = (props: ConfirmationDialogProps) => {
  const [actionError, setActionError] = useState<Error | null>(null);
  const dialogContentRef = useRef<HTMLDivElement | null>(null); // This is used to scroll to the error messages

  const closeCallback = () => {
    props.onClose();
    setActionError(null);
  };

  const _onCancel = useCallback(async () => {
    if (props.onCancel) {
      await props.onCancel();
    }
    closeCallback();
  }, [props.onCancel, closeCallback]);

  const _onConfirm = useCallback(async () => {
    try {
      await props.onConfirm();

      closeCallback();
    } catch (err) {
      if (err instanceof Error) {
        setActionError(err);
      } else {
        setActionError(err as any); // The default case is good enough for now
      }

      dialogContentRef.current?.scrollIntoView({ behavior: 'smooth' });
    }
  }, [props.onConfirm, closeCallback, setActionError]);

  return (
    <Modal
      title={props.title ?? 'Confirmation'}
      iconId="fr-icon-arrow-right-line"
      primaryButtonText="Confirmer"
      open
      onClose={closeCallback}
      onCancel={_onCancel}
      onConfirm={_onConfirm}
    >
      <div className="aei-stack">
        {!!actionError && <ErrorAlert errors={[actionError]} />}
        <p>{props.description || 'Êtes-vous sûr de vouloir continuer ?'}</p>
      </div>
    </Modal>
  );
};
