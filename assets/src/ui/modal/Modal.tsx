import { useAsyncFn } from 'react-use';

import LoadingButton from '@aei/src/components/LoadingButton';

interface ModalProps {
  autoCloseOnConfirm?: boolean;
  children: React.ReactNode;
  onClose?: () => void;
  open?: boolean;
  title: string;
  iconId: string;
  loading?: boolean;

  onConfirm: () => Promise<void>;
  onCancel?: () => Promise<void>;
  primaryButtonText?: string;
  secondaryButtonText?: string;
}

const Modal = ({
  autoCloseOnConfirm = true,
  children,
  iconId,
  loading = false,
  open = false,
  onCancel,
  onConfirm,
  onClose,
  title,
  primaryButtonText = 'Envoyer',
  secondaryButtonText = 'Annuler',
}: ModalProps) => {
  const [state, _onConfirm] = useAsyncFn(() => onConfirm().finally(autoCloseOnConfirm ? onClose : () => null), [onConfirm]);
  const _loading = state.loading || loading;
  return (
    <dialog aria-labelledby="fr-modal-2-title" id="fr-modal" className={`fr-modal ${open ? ' fr-modal--opened' : ''}`} role="dialog">
      <div className="fr-container fr-container--fluid fr-container-md">
        <div className="fr-grid-row fr-grid-row--center">
          <div className="fr-col-12 fr-col-md-8 fr-col-lg-6">
            <div className="fr-modal__body">
              <div className="fr-modal__header">
                <button className="fr-btn--close fr-btn" aria-controls="fr-modal-2" onClick={() => onClose && onClose()}>
                  Fermer
                </button>
              </div>
              <div className="fr-modal__content">
                <h1 id="fr-modal-2-title" className="fr-modal__title">
                  <span className={`${iconId} fr-icon--lg`}></span>
                  {title}
                </h1>
                {children}
              </div>
              <div className="fr-modal__footer">
                <div className="fr-btns-group fr-btns-group--lg fr-btns-group--right fr-btns-group--inline-reverse fr-btns-group--inline-lg fr-btns-group--icon-left">
                  <LoadingButton size="large" loading={_loading} priority="primary" onClick={() => _onConfirm()}>
                    {primaryButtonText}
                  </LoadingButton>
                  {onCancel && (
                    <LoadingButton size="large" disabled={_loading} priority="secondary" onClick={() => onCancel()}>
                      {secondaryButtonText}
                    </LoadingButton>
                  )}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </dialog>
  );
};

export default Modal;
