import React, { Fragment, PropsWithChildren, useCallback, useState } from 'react';
import ReactDOM from 'react-dom';

import { ModalContext, ShowModalFactory } from '@aei/src/ui/modal/ModalContext';

export const ModalProvider = ({ children }: PropsWithChildren) => {
  const [render, setRender] = useState<ShowModalFactory | null>(null);

  const onClose = useCallback(() => {
    setRender(null);
  }, []);

  const showModal = useCallback((render: ShowModalFactory) => {
    setRender(() => render);
  }, []);

  return (
    <Fragment>
      <>
        <ModalContext.Provider
          value={{
            showModal: showModal,
          }}
        >
          {children}
        </ModalContext.Provider>
        {/* @ts-ignore */}
        {render && ReactDOM.createPortal(<>{render({ open: true, onClose })}</>, document.body)}
      </>
    </Fragment>
  );
};
