variable "scaleway_config" {
  type = object({
    access_key                        = string
    mailer_dsn                        = string
    mattermost_webhook_url            = string
    matomo_site_id                    = string
    matomo_url                        = string
    organization_id                   = string
    pro_connect_client_id_dev         = string
    pro_connect_client_id_preprod     = string
    pro_connect_client_id_prod        = string
    pro_connect_client_secret_dev     = string
    pro_connect_client_secret_preprod = string
    pro_connect_client_secret_prod    = string
    project_id                        = string
    full_calendar_licence_key         = string
    secret_key                        = string
    sentry_public_key                 = string
    timezonedb_api_key                = string
    wonderpush_access_token_dev       = string
    wonderpush_access_token_preprod   = string
    wonderpush_access_token_prod      = string
    wonderpush_application_id_dev     = string
    wonderpush_application_id_preprod = string
    wonderpush_application_id_prod    = string
    wonderpush_webkey_dev             = string
    wonderpush_webkey_preprod         = string
    wonderpush_webkey_prod            = string
    zapier_webhook_url_dev            = string
    zapier_webhook_url_preprod        = string
    zapier_webhook_url_prod           = string
  })
  sensitive = true
}

variable "scaleway_s3_config" {
  type = object({
    organization_id   = string
    project_id        = string
    access_key        = string
    secret_key        = string
    sentry_public_key = string
  })
  sensitive = true
}

variable "scaleway_tem_config" {
  type = object({
    secret_key = string
  })
  sensitive = true
}

variable "db_admins" {
  type = map(string)
}

variable "sentry" {
  type = object({
    auth_token = string
  })
  sensitive = true
}
