# Provisionning infrastructure with Terraform

This project uses Terraform to provision its infrastructure on Scaleway.
Terraform isn't used to deploy each application update, this is done separately by updating just the container image.

It is recommended to use Terraform 1.5.x.

## Running Terraform locally

- Clone the repository
- `cd agents-en-intervention/terraform`
- Go to GitLab's repository [Terraform states](https://gitlab.com/incubateur-territoires/startups/agents-intervention/agents-en-intervention/-/terraform) page
- Under Actions, copy the Terraform init command and run it after providing your [Personal Access Token](https://gitlab.com/-/profile/personal_access_tokens)
- In [Scaleway's Secret Manager](https://console.scaleway.com/secret-manager/secrets), copy the latest version's value of the secret under `terraform/variables` in a `terraform.auto.tfvars.json` file in the `/terraform` folder. If you don't see this secret, check that you are in the `Agents En Intervention` project.
- Run `terraform plan -out=plan.tfplan`

## Fetching database credentials

- Run `terraform refresh`
- Run `terraform output <env>_db_credentials`

## Connection to the database

The database is accessible using a SSH tunnel on a dedicated VM.
To start the tunnel, run `ssh -N -L <local port>:<db instance ip>:<db instance port> root@<tunnel vm ip>` in a shell.
Then, you'll be able to connect to the database by running `psql -h localhost -p <local port> -U <user> <database>`.

These command can be found mostly preconfigured when fetching database credentials.

## Giving another user access to the database

The `terraform.auto.tfvars.json` file lists the SSH keys that are configured to allow a user to access the database through the SSH tunnel.
In order to configure another user's SSH key:

- add it to the list in the `terraform.auto.tfvars.json` file
- run `terraform plan -out=plan.tfplan`
- if the plan shows no other changes than adding the SSH key, run `terraform apply plan.tfplan`
- connect to the tunnel VM using `ssh root@<tunnel vm ip>`. This needs to be done by someone that has already access to the VM
- run `scw-fetch-ssh-keys --upgrade` on the VM
- remember to update the secret in the Scaleway Secrets Manager with the new value
