resource "random_password" "db_password" {
  length  = 64
  special = true
}

resource "scaleway_rdb_instance" "database" {
  name          = local.fullname
  node_type     = "DB-DEV-S"
  engine        = "PostgreSQL-15"
  is_ha_cluster = true
  encryption_at_rest = true
  init_settings = {}
  tags          = []
}
# Whitelist IPs from http://as12876.net/ according to https://www.scaleway.com/en/docs/faq/serverless-containers/#can-i-whitelist-the-ips-of-my-containers
locals {
  scw_ip_list = [
    "62.210.0.0/16",
    "195.154.0.0/16",
    "212.129.0.0/18",
    "62.4.0.0/19",
    "212.83.128.0/19",
    "212.83.160.0/19",
    "212.47.224.0/19",
    "163.172.0.0/16",
    "51.15.0.0/16",
    "151.115.0.0/16",
    "51.158.0.0/15",
  ]
}
resource "scaleway_rdb_acl" "database" {
  instance_id = scaleway_rdb_instance.database.id
  dynamic "acl_rules" {
    for_each = concat(
      [for idx, ip in local.scw_ip_list : { ip = ip, desc = "Scaleway IP range ${idx}" }],
      [{ ip = "${var.tunnel_ip}/32", desc = "Database SSH tunnel IP" }],
    )
    content {
      ip          = acl_rules.value.ip
      description = acl_rules.value.desc
    }
  }
}

resource "scaleway_rdb_database" "app" {
  instance_id = scaleway_rdb_instance.database.id
  name        = "app"
}

resource "scaleway_rdb_user" "app" {
  instance_id = scaleway_rdb_instance.database.id
  name        = "aei"
  password    = random_password.db_password.result
}
resource "scaleway_rdb_privilege" "app" {
  instance_id   = scaleway_rdb_instance.database.id
  user_name     = scaleway_rdb_user.app.name
  database_name = scaleway_rdb_database.app.name
  permission    = "all"
}
