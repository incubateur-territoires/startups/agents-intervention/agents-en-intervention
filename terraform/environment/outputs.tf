output "container_domain_name" {
  value = scaleway_container.app.domain_name
}
output "db_credentials" {
  value = {
    user           = scaleway_rdb_user.app.name
    password       = random_password.db_password.result
    certificate    = scaleway_rdb_instance.database.certificate
    ssh_tunnel_cmd = "ssh -N -L <local port>:${scaleway_rdb_instance.database.load_balancer[0].ip}:${scaleway_rdb_instance.database.load_balancer[0].port} root@${var.tunnel_ip}"
    psql_cmd       = "psql -h localhost -p <local port> -U ${scaleway_rdb_user.app.name} ${scaleway_rdb_database.app.name}"
  }
  sensitive = true
}
