variable "environment" {
  type = string
}
locals {
  fullname = "agents-intervention-${var.environment}"
  # app_env  = var.environment == "dev" ? "dev" : "prod"
  # app_mode = var.environment == "dev" ? "dev" : "prod"
  app_env  = "prod"
  app_mode = "prod"
}

variable "scaleway_organization_id" {
  type = string
}
variable "scaleway_access_key" {
  type = string
}
variable "scaleway_mattermost_channel" {
  type = string
}
variable "scaleway_mattermost_webhook_url" {
  type = string
}
variable "scaleway_secret_key" {
  type      = string
  sensitive = true
}
variable "scaleway_sentry_public_key" {
  type      = string
  sensitive = true
}
variable "scaleway_full_calendar_licence_key" {
  type      = string
  sensitive = true
}
variable "scaleway_matomo_url" {
  type = string
}
variable "scaleway_matomo_site_id" {
  type = string
}
variable "scaleway_wonderpush_webkey" {
  type      = string
  sensitive = true
}
variable "scaleway_wonderpush_application_id" {
  type      = string
  sensitive = true
}
variable "scaleway_wonderpush_access_token" {
  type      = string
  sensitive = true
}
variable "scaleway_wonderpush_nouvelle_demande" {
  type = string
}
variable "scaleway_wonderpush_intervention_bloquee" {
  type = string
}
variable "scaleway_wonderpush_intervention_terminee" {
  type = string
}
variable "scaleway_wonderpush_intervention_terminee_elu" {
  type = string
}
variable "scaleway_wonderpush_intervention_refusee" {
  type = string
}
variable "scaleway_wonderpush_nouveau_commentaire" {
  type = string
}

variable "scaleway_zapier_webhook_url" {
  type = string
}

variable "scaleway_timezonedb_api_key" {
  type = string
  sensitive = true
}

variable "scaleway_pro_connect_client_id" {
  type = string
  sensitive = true
}
variable "scaleway_pro_connect_client_secret" {
  type = string
  sensitive = true
}
variable "scaleway_pro_connect_api_url" {
  type = string
}

variable "sentry_auth_token" {
  type = string
  sensitive = true
}

variable "scaleway_mailer_dsn" {
  type = string
  sensitive = true
}

variable "domain_name" {
  type = string
}
locals {
  app_url = "https://${var.domain_name}"
  api_url = "https://${var.domain_name}/api"
}
variable "additional_domain_names" {
  type = set(string)
  default = []
}

variable "tunnel_ip" {
  type = string
}

variable "db_admins" {
  type = set(string)
}
