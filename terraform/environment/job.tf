# resource "scaleway_job_definition" "app" {
#   name = "app_job"
#   cpu_limit = 140
#   memory_limit = 256
#   image_uri = scaleway_container.app.registry_image
#   command = "php api/bin/console app:generate-interventions"
#   timeout = "10m"
#
#   env = {
#     APP_ENV                  = local.app_env
#     APP_MODE                 = local.app_mode
#     CORS_ALLOW_ORIGIN        = "^https?://(localhost|127\\.0\\.0\\.1)(:[0-9]+)?$"
#
#     S3_ACCESS_KEY                  = var.scaleway_access_key
#     S3_ENDPOINT                    = "https://s3.fr-par.scw.cloud",
#     S3_NAME                        = scaleway_object_bucket.app.name
#     S3_REGION                      = "fr-par"
#
#     MATOMO_URL     = var.scaleway_matomo_url
#     MATOMO_SITE_ID = var.scaleway_matomo_site_id
#     APP_SECRET            = random_password.app_secret.result
#     JWT_PASSPHRASE        = random_password.jwt_passphrase.result
#     JWT_PUBLIC_KEY_BASE64 = base64encode(tls_private_key.jwt.public_key_pem)
#     JWT_SECRET_KEY_BASE64 = base64encode(tls_private_key.jwt.private_key_pem)
#     S3_SECRET_KEY         = var.scaleway_secret_key
#     DATABASE_URL          = "postgresql://${urlencode(scaleway_rdb_user.app.name)}:${urlencode(random_password.db_password.result)}@${scaleway_rdb_instance.database.load_balancer[0].ip}:${scaleway_rdb_instance.database.load_balancer[0].port}/${scaleway_rdb_database.app.name}"
#     SENTRY_DSN            = "https://${var.scaleway_sentry_public_key}@sentry.anct.cloud-ed.fr/2"
#   }
#
#   cron {
#     schedule = "0 6 * * *"
#     timezone = "Europe/Paris"
#   }
# }
