locals {
  projet_id = 48567730
}
resource "gitlab_project_variable" "variable" {
  for_each = {
    APP_ENV                  = local.app_env
    APP_MODE                 = local.app_mode
    API_URL                  = local.api_url
  }
  raw               = false
  project           = local.projet_id
  environment_scope = var.environment
  protected         = true
  masked            = false
  key               = each.key
  value             = each.value
}

resource "gitlab_project_variable" "masked_variable" {
  for_each = {
    SCW_CONTAINER_ID            = split("/", scaleway_container.app.id)[1]
    SCW_DEFAULT_ORGANIZATION_ID = var.scaleway_organization_id
    SCW_ACCESS_KEY              = var.scaleway_access_key
    SCW_SECRET_KEY              = var.scaleway_secret_key
    SENTRY_AUTH_TOKEN           = var.sentry_auth_token
  }
  raw               = false
  project           = local.projet_id
  environment_scope = var.environment
  protected         = true
  masked            = true
  key               = each.key
  value             = each.value
}
