resource "scaleway_container_namespace" "app" {
  name        = local.fullname
  description = "Application container namespace (managed by Terraform)"
}

resource "random_password" "app_secret" {
  length  = 64
  special = false
}
resource "tls_private_key" "jwt" {
  algorithm = "RSA"
  rsa_bits  = 4096
}
resource "random_password" "jwt_passphrase" {
  length  = 64
  special = false
}
resource "scaleway_container" "app" {
  name            = local.fullname
  description     = "Application container (managed by Terraform)"
  namespace_id    = scaleway_container_namespace.app.id
  port            = 80
  cpu_limit       = 1120
  memory_limit    = 2048
  min_scale       = 2
  max_scale       = 2
  max_concurrency = 80
  deploy          = true
  sandbox         = "v1"

  environment_variables = {
    API_URL                             = local.api_url
    APP_ENV                             = local.app_env
    APP_MODE                            = local.app_mode
    CORS_ALLOW_ORIGIN                   = "^https?://(localhost|127\\.0\\.0\\.1)(:[0-9]+)?$"
    CORS_ALLOW_ORIGIN                   = "^https?://(localhost|127\\.0\\.0\\.1)(:[0-9]+)?$"
    FRONT_URL                           = local.app_url
    MATTERMOST_CHANNEL                  = var.scaleway_mattermost_channel
    MATTERMOST_WEBHOOK_URL              = var.scaleway_mattermost_webhook_url
    MATOMO_SITE_ID                      = var.scaleway_matomo_site_id
    MATOMO_URL                          = var.scaleway_matomo_url
    PRO_CONNECT_CALLBACK_URL            = "${local.app_url}/pro_connect/complete"
    PRO_CONNECT_CLIENT_ID               = var.scaleway_pro_connect_client_id
    PRO_CONNECT_API_URL                 = var.scaleway_pro_connect_api_url
    S3_ACCESS_KEY                       = var.scaleway_access_key
    S3_ENDPOINT                         = "https://s3.fr-par.scw.cloud",
    S3_NAME                             = scaleway_object_bucket.app.name
    S3_REGION                           = "fr-par"
    SENTRY_ENVIRONMENT                  = var.environment
    WONDERPUSH_INTERVENTION_BLOQUEE     = var.scaleway_wonderpush_intervention_bloquee
    WONDERPUSH_INTERVENTION_REFUSEE     = var.scaleway_wonderpush_intervention_refusee
    WONDERPUSH_INTERVENTION_TERMINEE    = var.scaleway_wonderpush_intervention_terminee
    WONDERPUSH_INTERVENTION_TERMINE_ELU = var.scaleway_wonderpush_intervention_terminee_elu
    WONDERPUSH_NOUVEAU_COMMENTAIRE      = var.scaleway_wonderpush_nouveau_commentaire
    WONDERPUSH_NOUVELLE_DEMANDE         = var.scaleway_wonderpush_nouvelle_demande
    ZAPIER_WEBHOOK_URL                  = var.scaleway_zapier_webhook_url
  }

  secret_environment_variables = {
    APP_SECRET                    = random_password.app_secret.result
    DATABASE_URL                  = "postgresql://${urlencode(scaleway_rdb_user.app.name)}:${urlencode(random_password.db_password.result)}@${scaleway_rdb_instance.database.load_balancer[0].ip}:${scaleway_rdb_instance.database.load_balancer[0].port}/${scaleway_rdb_database.app.name}"
    JWT_PASSPHRASE                = random_password.jwt_passphrase.result
    JWT_PUBLIC_KEY_BASE64 = base64encode(tls_private_key.jwt.public_key_pem)
    JWT_SECRET_KEY_BASE64 = base64encode(tls_private_key.jwt.private_key_pem)
    MAILER_DSN                    = var.scaleway_mailer_dsn
    PGSSLROOTCERT                 = scaleway_rdb_instance.database.certificate
    PRO_CONNECT_CLIENT_SECRET     = var.scaleway_pro_connect_client_secret
    S3_SECRET_KEY                 = var.scaleway_secret_key
    SCALEWAY_DATABASE_CERTIFICATE = scaleway_rdb_instance.database.certificate
    SENTRY_DSN                    = "https://${var.scaleway_sentry_public_key}@sentry.anct.cloud-ed.fr/2"
    TIMEZONE_DB_API_KEY           = var.scaleway_timezonedb_api_key
    WONDERPUSH_ACCESS_TOKEN       = var.scaleway_wonderpush_access_token
    WONDERPUSH_APPLICATION_ID     = var.scaleway_wonderpush_application_id
    WONDERPUSH_WEBKEY             = var.scaleway_wonderpush_webkey
    FULL_CALENDAR_LICENCE_KEY     = var.scaleway_full_calendar_licence_key
  }

  lifecycle {
    ignore_changes = [
      registry_image # edited by the deployment CI
    ]
  }
}

resource "scaleway_container_domain" "app" {
  hostname     = var.domain_name
  container_id = scaleway_container.app.id
}

resource "scaleway_container_domain" "app_extra" {
  for_each     = var.additional_domain_names
  hostname     = each.value
  container_id = scaleway_container.app.id
}
