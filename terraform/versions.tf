terraform {
  required_version = "~> 1.10.2"
  required_providers {
    scaleway = {
      source  = "scaleway/scaleway"
      version = "~> 2.48.0"
    }
    random = {
      source  = "hashicorp/random"
      version = "~> 3.6.3"
    }
    gitlab = {
      source  = "gitlabhq/gitlab"
      version = "~> 17.6.1"
    }
    tls = {
      source  = "hashicorp/tls"
      version = "~> 4.0.6"
    }
  }
}
