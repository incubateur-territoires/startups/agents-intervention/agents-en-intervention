resource "scaleway_iam_ssh_key" "vm_access" {
  for_each   = var.db_admins
  name       = each.key
  public_key = each.value
}
resource "scaleway_instance_ip" "ssh_tunnel_instance" {}
resource "scaleway_instance_server" "ssh_tunnel_instance" {
  name  = "db-tunnel"
  type  = "DEV1-S"
  image = "ubuntu_jammy"
  ip_id = scaleway_instance_ip.ssh_tunnel_instance.id
}

output "tunnel_ip" {
  value = scaleway_instance_ip.ssh_tunnel_instance.address
}
