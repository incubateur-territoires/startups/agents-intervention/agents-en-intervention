provider "scaleway" {
  region          = "fr-par"
  zone            = "fr-par-1"
  organization_id = var.scaleway_config.organization_id
  project_id      = var.scaleway_config.project_id
  access_key      = var.scaleway_config.access_key
  secret_key      = var.scaleway_config.secret_key
}
