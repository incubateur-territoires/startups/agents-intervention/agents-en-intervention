locals {
  zone = "agentsenintervention.anct.gouv.fr"
}
resource "scaleway_domain_record" "dev" {
  dns_zone = local.zone
  name     = "dev"
  type     = "CNAME"
  data     = "${module.dev.container_domain_name}."
}
locals {
  dev_domain = "dev.${local.zone}"
}

resource "scaleway_domain_record" "preprod" {
  dns_zone = local.zone
  name     = "preprod"
  type     = "CNAME"
  data     = "${module.preprod.container_domain_name}."
}
locals {
  preprod_domain = "preprod.${local.zone}"
}


resource "scaleway_domain_record" "prod" {
  dns_zone = local.zone
  name     = ""
  type     = "ALIAS"
  data     = "${module.prod.container_domain_name}."
}
resource "scaleway_domain_record" "www" {
  dns_zone = local.zone
  name     = "www"
  type     = "CNAME"
  data     = "${module.prod.container_domain_name}."
}
locals {
  prod_domain     = local.zone
  prod_domain_www = "www.${local.zone}"
}

resource "scaleway_tem_domain" "main" {
  name       = local.zone
  accept_tos = true
  autoconfig = true
}
