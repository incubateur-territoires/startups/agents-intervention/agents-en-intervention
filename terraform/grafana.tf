resource "scaleway_cockpit_grafana_user" "developer" {
  for_each = toset(keys(var.db_admins))
  login    = each.value
  role     = "editor"
}

output "grafana_credentials" {
  value     = { for k, user in scaleway_cockpit_grafana_user.developer : k => user.password }
  sensitive = true
}
