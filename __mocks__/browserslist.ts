// __mocks__/browserslist.ts
const browserslist = {
  findConfigFile: jest.fn(() => '/path/to/config/file'),
  loadConfig: jest.fn(() => ({
    defaults: ['> 1%', 'last 2 versions', 'not dead'],
  })),
};

module.exports = browserslist;
