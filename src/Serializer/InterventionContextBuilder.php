<?php

declare(strict_types=1);

namespace App\Serializer;

use ApiPlatform\State\SerializerContextBuilderInterface;
use App\Entity\Intervention;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

final readonly class InterventionContextBuilder implements SerializerContextBuilderInterface
{
    public function __construct(
        private SerializerContextBuilderInterface $decorated,
        private AuthorizationCheckerInterface     $authorizationChecker
    ) {
    }

    #[\Override]
    public function createFromRequest(
        Request $request,
        bool $normalization,
        ?array $extractedAttributes = null
    ): array {
        $context = $this->decorated->createFromRequest($request, $normalization, $extractedAttributes);
        $resourceClass = $context['resource_class'] ?? null;

        if (
            $resourceClass === Intervention::class &&
            isset($context['groups']) &&
            $this->authorizationChecker->isGranted('ROLE_ADD_PARTICIPANTS_TO_INTERVENTION') &&
            false === $normalization
        ) {
            $context['groups'] = [];
            $context['groups'][] = Intervention::GROUP_INTERVENTION_PATCH;
            $context['groups'][] = Intervention::GROUP_INTERVENTION_POST;
        }

        return $context;
    }
}
