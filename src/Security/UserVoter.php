<?php

declare(strict_types=1);

namespace App\Security;

use App\Entity\Role;
use App\Entity\User;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

/**
 * Le voteur pour les interventions.
 */
class UserVoter extends Voter
{
  /**
   * La possibilité de voir les données.
   */
  const string CREATE = 'CREATE';
  const string DELETE = 'DELETE';
  const string EDIT = 'EDIT';
  const string VIEW = 'VIEW';

  public function __construct(
    private readonly LoggerInterface $logger,
    private readonly Security        $security
  )
  {
  }

  /**
   * Détermine si ce voteur doit voter.
   * @param string $attribute l'attribut.
   * @param mixed $subject l'objet.
   * @return bool si ce voteur doit voter.
   */
  #[\Override]
  protected function supports(string $attribute, mixed $subject): bool
  {
    if (!in_array($attribute, [self::CREATE, self::VIEW, self::EDIT, self::DELETE])) {
      return false;
    }

    if (($subject instanceof User) === false) {
      return false;
    }

    return true;
  }

  /**
   * Détermine si l'utilisateur a ce droit sur l'objet.
   * @param string $attribute l'attribut.
   * @param mixed $subject l'objet.
   * @param TokenInterface $token le jeton.
   * @return bool si l'utilisateur a ce droit sur l'objet.
   */
  #[\Override]
  protected function voteOnAttribute(string $attribute, mixed $subject, TokenInterface $token): bool
  {
    /** @var User $securityUser l'utilisateur connecté. */
    $securityUser = $token->getUser();

    /** @var User $user */
    $user = $subject;

    return match ($attribute) {
      self::CREATE => $this->canCreate($user, $securityUser),
      self::VIEW => $this->canView($user, $securityUser),
      self::EDIT, self::DELETE => $this->canEdit($user, $securityUser),
      default => throw new \LogicException('This code should not be reached!')
    };
  }

  private function canCreate(User $user, User $securityUser): bool
  {
    if ($securityUser->hasRole(Role::Admin)) {
      $ret = true;
    } // Seul un admin peut créer un admin
    else if ($user->hasRole(Role::Admin)) {
      return false;
    } else {
      $ret = $this->security->isGranted('ROLE_EDIT_USERS') && $user->getEmployer()->getId() === $securityUser->getEmployer()->getId();
    }
    $this->logger->info(sprintf('UserVoter :: access CREATE by user %d resulting %b', $user->getId(), $ret));//, ['intervention employer' => $intervention->getEmployer()->getId()]);
    return $ret;
  }

  /**
   * Détermine si l'utilisateur peut voir les données d'un utilisateur.
   * @param User $user l'utilisateur demandé.
   * @param User $securityUser l'utilisateur connecté.
   * @return bool si l'utilisateur peut voir les données d'un utilisateur.
   */
  private function canView(User $user, User $securityUser): bool
  {
    $ret = $user->getEmployer()->getId() === $securityUser->getEmployer()->getId();
    $this->logger->info(sprintf('UserVoter :: access VIEW user %d by user %d resulting %b', $user->getId(), $securityUser->getId(), $ret), ['requested user employer' => $user->getEmployer()->getId()]);
    return $ret;
  }

  private function canEdit(User $user, User $securityUser): bool
  {
    if ($securityUser->hasRole(Role::Admin)) {
      return true;
    }

    // Seul un admin peut modifier un admin
    if ($user->hasRole(Role::Admin)) {
      return false;
    }

    return $this->canView($user, $securityUser) && $securityUser->hasRole(Role::AdminEmployer);
  }
}
