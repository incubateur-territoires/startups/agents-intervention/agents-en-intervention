<?php

declare(strict_types=1);

namespace App\Security;

use App\Entity\User;
use App\Event\ConnexionMode;
use App\Event\UserConnectedEvent;
use Doctrine\ORM\EntityManagerInterface;
use Psr\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Http\Authenticator\AbstractLoginFormAuthenticator;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\CsrfTokenBadge;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\UserBadge;
use Symfony\Component\Security\Http\Authenticator\Passport\Credentials\PasswordCredentials;
use Symfony\Component\Security\Http\Authenticator\Passport\Passport;
use Symfony\Component\Security\Http\SecurityRequestAttributes;
use Symfony\Component\Security\Http\Util\TargetPathTrait;

class UserAuthenticator extends AbstractLoginFormAuthenticator
{
  use TargetPathTrait;

  public const string LOGIN_ROUTE = 'app_login';

  public function __construct(
    private readonly EventDispatcherInterface $eventDispatcher,
    private readonly EntityManagerInterface   $entityManager,
    private readonly UrlGeneratorInterface    $urlGenerator,
  )
  {
  }

  #[\Override]
  public function authenticate(Request $request): Passport
  {
    $username = $request->request->getString('username', '');

    $request->getSession()->set(SecurityRequestAttributes::LAST_USERNAME, $username);

    return new Passport(
      new UserBadge($username),
      new PasswordCredentials($request->request->getString('password', '')),
      [
        new CsrfTokenBadge('authenticate', $request->request->getString('_csrf_token')),
      ]
    );
  }


  /**
   * @param Request $request
   * @param TokenInterface $token
   * @param string $firewallName
   * @return Response|null
   */
  #[\Override]
  public function onAuthenticationSuccess(Request $request, TokenInterface $token, string $firewallName): ?Response
  {
    $user = $token->getUser();

    if ($user instanceof User) {
      $user->setConnectedAt(new \DateTimeImmutable());
      $this->entityManager->persist($user);
      $this->entityManager->flush();
      $this->eventDispatcher->dispatch(new UserConnectedEvent($user, ConnexionMode::Form));
    }

    if ($targetPath = $this->getTargetPath($request->getSession(), $firewallName)) {
      return new RedirectResponse($targetPath);
    }

    return new RedirectResponse($this->urlGenerator->generate('easyadmin_dashboard'));
  }

  #[\Override]
  protected function getLoginUrl(Request $request): string
  {
    return $this->urlGenerator->generate(self::LOGIN_ROUTE);
  }
}
