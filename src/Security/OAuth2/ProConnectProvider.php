<?php

declare(strict_types=1);

namespace App\Security\OAuth2;

use Lcobucci\JWT\Encoding\JoseEncoder;
use Lcobucci\JWT\Token\Parser;
use Lcobucci\JWT\Token\Plain;
use League\OAuth2\Client\Provider\AbstractProvider;
use League\OAuth2\Client\Provider\GenericResourceOwner;
use League\OAuth2\Client\Token\AccessToken;
use League\OAuth2\Client\Tool\BearerAuthorizationTrait;
use Psr\Http\Message\ResponseInterface;

class ProConnectProvider extends AbstractProvider
{
  use BearerAuthorizationTrait;

  #[\Override]
  protected function getScopeSeparator(): string
  {
    return ' ';
  }

  /**
   * @param array<string, mixed> $options
   * @return array<string, mixed>
   */
  #[\Override]
  protected function getAuthorizationParameters(array $options): array
  {
    $options['acr_values'] = 'eidas1';
    $params = parent::getAuthorizationParameters($options);
    unset($params['approval_prompt']);

    return $params;
  }

  /**
   * @return string
   */
  public function getBaseAuthorizationUrl(): string
  {
    return $_ENV['PRO_CONNECT_API_URL'] . '/api/v2/authorize';
  }

  /**
   * @param array<string, mixed> $params
   * @return string
   */
  public function getBaseAccessTokenUrl(array $params): string
  {
    return $_ENV['PRO_CONNECT_API_URL'] . '/api/v2/token';
  }

  public function getResourceOwnerDetailsUrl(AccessToken $token): string
  {
    return $_ENV['PRO_CONNECT_API_URL'] . '/api/v2/userinfo';
  }

  protected function getDefaultScopes(): array
  {
    return [
      'openid',
      'email',
      'siret',
//      'siren',
      'phone_number',
      'given_name',
      'usual_name',
    ];
  }

  protected function checkResponse(ResponseInterface $response, $data): void
  {
    // do nothing ?
  }

  protected function createResourceOwner(array $response, AccessToken $token): GenericResourceOwner
  {
    return new GenericResourceOwner($response, $token->getResourceOwnerId() ?? '');
  }

  #[\Override]
  protected function parseResponse(ResponseInterface $response): array|string
  {
    /** @var non-empty-string $content */
    $content = (string) $response->getBody();
    $type = $this->getContentType($response);

    if (str_contains($type, 'application/jwt')) {
      $parser = new Parser(new JoseEncoder());
      /** @var Plain $token */
      $token = $parser->parse($content);

      if ('RS256' !== $token->headers()->get('alg')) {
        throw new \UnexpectedValueException('Jwt signature algorithm not supported. Expected RS256');
      }

      return $token->claims()->all();
    }

    return parent::parseResponse($response);
  }
}
