<?php

namespace App\Security\Voter;

use App\Entity\Role;
use App\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

final class PermissionVoter extends Voter
{
  public const string PRIVATE_MENU_SHOW_COMMUNES = 'PRIVATE_MENU_SHOW_COMMUNES';
  public const string PRIVATE_MENU_SHOW_DASHBOARD = 'PRIVATE_MENU_SHOW_DASHBOARD';
  public const string PRIVATE_MENU_SHOW_PLANNING = 'PRIVATE_MENU_SHOW_PLANNING';
  public const string PRIVATE_MENU_SHOW_RECURRENCES = 'PRIVATE_MENU_SHOW_RECURRENCES';
  public const string PRIVATE_MENU_SHOW_TEAM = 'PRIVATE_MENU_SHOW_TEAM';

  #[\Override]
  protected function supports(string $attribute, mixed $subject): bool
  {
    return in_array($attribute, [
      self::PRIVATE_MENU_SHOW_COMMUNES,
      self::PRIVATE_MENU_SHOW_DASHBOARD,
      self::PRIVATE_MENU_SHOW_PLANNING,
      self::PRIVATE_MENU_SHOW_RECURRENCES,
      self::PRIVATE_MENU_SHOW_TEAM,
    ]);
  }

  #[\Override]
  protected function voteOnAttribute(string $attribute, mixed $subject, TokenInterface $token): bool
  {
    $user = $token->getUser();

    // if the user is anonymous, do not grant access
    if (!$user instanceof User) {
      return false;
    }

    // ... (check conditions and return true to grant permission) ...
    return match ($attribute) {
      self::PRIVATE_MENU_SHOW_COMMUNES => $user->hasRole(Role::Admin),
      self::PRIVATE_MENU_SHOW_DASHBOARD => !$user->hasRole(Role::Agent),
      self::PRIVATE_MENU_SHOW_PLANNING => !$user->hasRole(Role::Elected),
      self::PRIVATE_MENU_SHOW_RECURRENCES => $user->hasRole(Role::Admin) || $user->hasRole(Role::AdminEmployer) || $user->hasRole(Role::Director),
      self::PRIVATE_MENU_SHOW_TEAM => $user->hasRole(Role::Admin) || $user->hasRole(Role::AdminEmployer),
      default => false
    };
  }
}
