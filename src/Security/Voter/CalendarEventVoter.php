<?php

namespace App\Security\Voter;

use App\Dto\IndisponibiliteAgent;
use App\Entity\Employer;
use App\Entity\Role;
use App\Entity\User;
use App\Repository\UserRepository;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

final class CalendarEventVoter extends Voter
{
    public const string CREATE_INDISPONIBILITE_AGENT = 'CREATE_INDISPONIBILITE_AGENT';
    public const string LIST_INDISPONIBILITE_COLLECTIVITE = 'LIST_INDISPONIBILITE_COLLECTIVITE';
    public const string LIST_INDISPONIBILITE_AGENT = 'LIST_INDISPONIBILITE_AGENT';

    public function __construct(
        private readonly UserRepository $userRepository,
    )
    {
    }

    protected function supports(string $attribute, mixed $subject): bool
    {
        // replace with your own logic
        // https://symfony.com/doc/current/security/voters.html
        return ($attribute == self::CREATE_INDISPONIBILITE_AGENT
                && $subject instanceof IndisponibiliteAgent) ||
            ($attribute == self::LIST_INDISPONIBILITE_COLLECTIVITE
                && $subject instanceof Employer)||
            ($attribute == self::LIST_INDISPONIBILITE_AGENT
                && $subject instanceof User);
    }

    protected function voteOnAttribute(string $attribute, mixed $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();

        // if the user is anonymous, do not grant access
        if (!$user instanceof User) {
            return false;
        }

        // ... (check conditions and return true to grant permission) ...
        return match ($attribute) {
            self::CREATE_INDISPONIBILITE_AGENT => $this->canCreate($user, $subject),
            self::LIST_INDISPONIBILITE_COLLECTIVITE => $this->canListForEmployer($user, $subject),
            self::LIST_INDISPONIBILITE_AGENT => $this->canListForAgent($user, $subject),
            default => false,
        };
    }

    private function canCreate(User $user, mixed $dto): bool
    {
        if ($dto instanceof IndisponibiliteAgent) {
            if ($user->hasRole(Role::Admin)) {
                return true;
            }

            if ($user->hasRole(Role::AdminEmployer) || $user->hasRole(Role::Director)) {
                /** @var User|null $participant */
                $participant = $this->userRepository->find(intval($dto->getParticipant()));
                if ($participant && $participant->getEmployer()->getId() === $user->getEmployer()->getId()) {
                    return true;
                }
            }
        }

        return false;
    }

    private function canListForEmployer(User $user, mixed $employer): bool
    {
        if (!$employer instanceof Employer) {
            return false;
        }

        return $user->hasRole(Role::Admin) || (($user->hasRole(Role::AdminEmployer) || $user->hasRole(Role::Director)) && $employer->getId() === $user->getEmployer()->getId());
    }

    private function canListForAgent(User $user, mixed $agent): bool
    {
        if (!$agent instanceof User) {
            return false;
        }

        return $user->hasRole(Role::Admin)
            || (($user->hasRole(Role::AdminEmployer) || $user->hasRole(Role::Director)) && $agent->getEmployer()->getId() === $user->getEmployer()->getId())
            || $user->getId() === $agent->getId();
    }

}
