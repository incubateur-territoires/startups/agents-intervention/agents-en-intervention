<?php

declare(strict_types=1);

namespace App\Security;

use App\Entity\Employer;
use App\Entity\Role;
use App\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

/**
 * Le voteur pour les employeurs.
 */
class EmployerVoter extends Voter
{
    /**
     * La possibilité de voir les données.
     */
    const VIEW = 'VIEW';
    const EDIT = 'EDIT';
    const EMPLOYER_LIST_USERS = 'EMPLOYER_LIST_USERS';

    /**
     * Détermine si ce voteur doit voter.
     * @param string $attribute l'attribut.
     * @param mixed $subject l'objet.
     * @return bool si ce voteur doit voter.
     */
    #[\Override]
    protected function supports(string $attribute, mixed $subject): bool
    {
        if (!in_array($attribute, [self::VIEW, self::EDIT, self::EMPLOYER_LIST_USERS])) {
            return false;
        }

        if (($subject instanceof Employer) === false) {
            return false;
        }

        return true;
    }

    /**
     * Détermine si l'utilisateur a ce droit sur l'objet.
     * @param string $attribute l'attribut.
     * @param mixed $subject l'objet.
     * @param TokenInterface $token le jeton.
     * @return bool si l'utilisateur a ce droit sur l'objet.
     */
    #[\Override]
    protected function voteOnAttribute(string $attribute, mixed $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();

        if ($user instanceof User === false) {
            return false;
        }

         /** @var Employer $employer l'employeur. */
        $employer = $subject;

        return match ($attribute) {
            self::VIEW, self::EMPLOYER_LIST_USERS => $this->canView($employer, $user),
            self::EDIT => $this->canEdit($employer, $user),
            default => throw new \LogicException('This code should not be reached!'),
        };
    }

    /**
     * Détermine si l'utilisateur peut voir l'employeur.
     * @param Employer $employer l'employeur.
     * @param User $user l'utilisateur.
     * @return bool si l'utilisateur peut voir l'employeur.
     */
    private function canView(Employer $employer, User $user): bool
    {
        if ($user->hasRole(Role::Admin)) {
            return true;
        }

        if ($employer->getId() !== $user->getEmployer()->getId()) {
            return false;
        }

        return true;
    }

    private function canEdit(Employer $employer, User $user): bool
    {
        return $user->hasRole(Role::Admin);
    }
}
