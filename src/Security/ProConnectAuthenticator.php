<?php

declare(strict_types=1);

namespace App\Security;

use App\Dto\ProConnectUserDetails;
use App\Entity\User;
use App\Event\ConnexionMode;
use App\Event\UserConnectedEvent;
use App\Repository\UserRepository;
use App\Service\ProConnectDataService;
use Doctrine\ORM\EntityManagerInterface;
use KnpU\OAuth2ClientBundle\Client\ClientRegistry;
use League\OAuth2\Client\Provider\Exception\IdentityProviderException;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use LogicException;
use Psr\EventDispatcher\EventDispatcherInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Http\Authenticator\AbstractAuthenticator;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\UserBadge;
use Symfony\Component\Security\Http\Authenticator\Passport\Passport;
use Symfony\Component\Security\Http\Authenticator\Passport\SelfValidatingPassport;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class ProConnectAuthenticator extends AbstractAuthenticator
{
  private const string LOGIN_ROUTE = 'app_security_pro_connect_callback';

  public function __construct(
    private readonly ClientRegistry           $clientRegistry,
    private readonly EntityManagerInterface   $entityManager,
    private readonly EventDispatcherInterface $eventDispatcher,
    private readonly JWTTokenManagerInterface $tokenManager,
    private readonly LoggerInterface          $logger,
    private readonly ProConnectDataService    $dataService,
    private readonly UrlGeneratorInterface    $urlGenerator,
    private readonly UserRepository           $userRepository,
  )
  {
  }

  public function supports(Request $request): ?bool
  {
    return self::LOGIN_ROUTE === $request->attributes->get('_route');
  }

  /**
   * @param Request $request
   * @return Passport
   * @throws IdentityProviderException
   * @throws ClientExceptionInterface
   * @throws RedirectionExceptionInterface
   * @throws ServerExceptionInterface
   * @throws TransportExceptionInterface
   */
  public function authenticate(Request $request): Passport
  {
    $code = $request->query->get('code');

    if (null === $code) {
      $content = 'No code provided';
      if (isset($_GET['error_description'])) {
        $content .= ' - Error: ' . $_GET['error_description'];
      }
      throw new LogicException($content);
    }

    $oidcClient = $this->clientRegistry->getClient('pro_connect');
    $accessToken = $oidcClient->getAccessToken();
    $resourceOwnerResponse = $oidcClient->fetchUserFromToken($accessToken);

    $_arr = $resourceOwnerResponse->toArray();
    $userDetails = new ProConnectUserDetails(
      $_arr['email'],
      $_arr['siret'],
      $_arr['given_name'],
      $_arr['usual_name'],
      array_key_exists('phone_number', $_arr) ? $_arr['phone_number'] : null,
    );

    $user = $this->dataService->sync($userDetails);
    $this->userRepository->saveProConnectToken($user, $accessToken->getToken());

    return new SelfValidatingPassport(new UserBadge($user->getLogin(), fn($identifier) => $this->userRepository->getByEmail($identifier)));
  }

  public function onAuthenticationSuccess(Request $request, TokenInterface $token, string $firewallName): ?Response
  {
    $user = $token->getUser();

    if (!$user instanceof User) {
      return new RedirectResponse($this->urlGenerator->generate('app_login'));
    }

    $user->setConnectedAt(new \DateTimeImmutable());
    $this->entityManager->persist($user);
    $this->entityManager->flush();

    // Ajouter une information personnalisée dans la session
    $this->logger->info('Generate jwt for user', ['user' => $user->getId()]);
    $request->getSession()->set('jwt', $this->tokenManager->create($user));

    $this->eventDispatcher->dispatch(new UserConnectedEvent($user, ConnexionMode::ProConnect));

    if (!$user->isComplete()) {
      return new RedirectResponse($this->urlGenerator->generate('app_my_account'));
    }

    return new RedirectResponse($this->urlGenerator->generate('login_success'));
  }

  public function onAuthenticationFailure(Request $request, AuthenticationException $exception): ?Response
  {
    return null;
  }
}
