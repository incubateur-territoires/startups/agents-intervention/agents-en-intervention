<?php

declare(strict_types=1);

namespace App\Security;

use App\Entity\Intervention;
use App\Entity\Role;
use App\Entity\User;
use Psr\Log\LoggerInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

/**
 * Le voteur pour les interventions.
 */
class InterventionVoter extends Voter
{
  /**
   * La possibilité de voir les données.
   */
  const VIEW = 'VIEW';
  const EDIT = 'EDIT';

  const CREATE = 'CREATE';
  const CREATE_REQUEST = 'CREATE_REQUEST';
  const CREATE_ONETIME = 'CREATE_ONETIME';
  const CREATE_RECURRING = 'CREATE_RECURRING';

  const VALIDATE_INTERVENTION = 'VALIDATE_INTERVENTION';
  const REJECT_INTERVENTION = 'REJECT_INTERVENTION';

  public function __construct(
    private readonly LoggerInterface $logger
  )
  {
  }

  /**
   * Détermine si ce voteur doit voter.
   * @param string $attribute l'attribut.
   * @param mixed $subject l'objet.
   * @return bool si ce voteur doit voter.
   */
  #[\Override]
  protected function supports(string $attribute, mixed $subject): bool
  {
    if (!in_array($attribute, [self::VIEW, self::EDIT, self::CREATE_ONETIME, self::CREATE_RECURRING, self::CREATE_REQUEST, self::VALIDATE_INTERVENTION, self::REJECT_INTERVENTION])) {
      return false;
    }

    if (($subject instanceof Intervention) === false) {
      return false;
    }

    return true;
  }

  /**
   * Détermine si l'utilisateur a ce droit sur l'objet.
   * @param string $attribute l'attribut.
   * @param mixed $subject l'objet.
   * @param TokenInterface $token le jeton.
   * @return bool si l'utilisateur a ce droit sur l'objet.
   */
  #[\Override]
  protected function voteOnAttribute(string $attribute, mixed $subject, TokenInterface $token): bool
  {
    $user = $token->getUser();

    if ($user instanceof User === false) {
      return false;
    }

    /** @var Intervention $intervention l'intervention. */
    $intervention = $subject;

    return match ($attribute) {
      self::VIEW => $this->canView($intervention, $user),
      self::EDIT => $this->canEdit($intervention, $user),
      self::CREATE_RECURRING, self::CREATE_ONETIME => $this->canCreateIntervention($intervention, $user),
      self::CREATE_REQUEST => $this->canCreateInterventionRequest($intervention, $user),
      self::VALIDATE_INTERVENTION, self::REJECT_INTERVENTION => $this->canValidateOrRejectInterventionRequest($intervention, $user),
      default => throw new \LogicException('This code should not be reached!'),
    };
  }

  /**
   * Détermine si l'utilisateur peut voir l'intervention.
   * @param Intervention $intervention l'intervention.
   * @param User $user l'utilisateur.
   * @return bool si l'utilisateur peut voir l'intervention.
   */
  private function canView(Intervention $intervention, User $user): bool
  {
    $ret = $user->hasRole(Role::Admin) || $intervention->getEmployer()->getId() === $user->getEmployer()->getId();
    $this->logger->info(sprintf('InterventionVoter :: access VIEW to intervention %d by user %d resulting %b', $intervention->getId(), $user->getId(), $ret), ['intervention employer' => $intervention->getEmployer()->getId()]);

    return $ret;
  }

  private function canEdit(Intervention $intervention, User $user): bool
  {
    $isAllow = $user->hasRole(Role::Director) || $user->hasRole(Role::AdminEmployer) || $user->hasRole(Role::Agent);
    $ret = $user->hasRole(Role::Admin) || ($isAllow && $intervention->getEmployer()->getId() === $user->getEmployer()->getId());
    $this->logger->info(sprintf('InterventionVoter :: access EDIT to intervention %d by user %d resulting %b', $intervention->getId(), $user->getId(), $ret), ['intervention employer' => $intervention->getEmployer()->getId()]);
    return $ret;
  }

  private function canCreateIntervention(Intervention $intervention, User $user): bool
  {
    $isAllow = $user->hasRole(Role::Director) || $user->hasRole(Role::AdminEmployer);
    $ret = $user->hasRole(Role::Admin) || ($isAllow && $intervention->getEmployer()->getId() === $user->getEmployer()->getId());

    $this->logger->info(sprintf('InterventionVoter :: access CREATE_INTERVENTION to intervention %d by user %d resulting %b', $intervention->getId(), $user->getId(), $ret), ['intervention employer' => $intervention->getEmployer()->getId()]);

    return $ret;

  }
  private function canCreateInterventionRequest(Intervention $intervention, User $user): bool
  {
    $isAllow = $user->hasRole(Role::Agent) || $user->hasRole(Role::Elected);
    $ret = $isAllow && $intervention->getEmployer()->getId() === $user->getEmployer()->getId();

    $this->logger->info(sprintf('InterventionVoter :: access CREATE_INTERVENTION_REQUEST to intervention %d by user %d resulting %b', $intervention->getId(), $user->getId(), $ret), ['intervention employer' => $intervention->getEmployer()->getId()]);

    return $ret;
  }

  private function canValidateOrRejectInterventionRequest(Intervention $intervention, User $user): bool
  {
    $isAllow = $user->hasRole(Role::AdminEmployer) || $user->hasRole(Role::Director);
    $ret = $user->hasRole(Role::Admin) || $isAllow && $intervention->getEmployer()->getId() === $user->getEmployer()->getId();

    $this->logger->info(sprintf('InterventionVoter :: access VALIDATE_INTERVENTION or REJECT_INTERVENTION to intervention %d by user %d resulting %b', $intervention->getId(), $user->getId(), $ret), ['intervention employer' => $intervention->getEmployer()->getId()]);

    return $ret;
  }
}
