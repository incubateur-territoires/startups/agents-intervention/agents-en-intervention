<?php

namespace App\Bridge\WonderPush;

use App\Event\CommentAddedEvent;
use App\Event\InterventionBlockedEvent;
use App\Event\InterventionFinishedEvent;
use App\Event\InterventionRejectedEvent;
use App\Event\RequestInterventionCreatedEvent;
use App\Message\SendPushNotification;
use App\Service\WonderPushLogger;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\Attribute\Autowire;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;
use WonderPush\Obj\Notification;
use WonderPush\Obj\NotificationAlert;
use WonderPush\Obj\NotificationAlertWeb;
use WonderPush\Params\DeliveriesCreateParams;
use WonderPush\WonderPush;

readonly class WonderPushMessageHandler
{
  private WonderPush $wonderpush;

  /**
   * @var array<class-string, string>
   */
  private array $typeToCampaignId;

  public function __construct(
    #[Autowire('%env(WONDERPUSH_ACCESS_TOKEN)%')] public string              $accessToken,
    #[Autowire('%env(WONDERPUSH_APPLICATION_ID)%')] public string            $appId,
    #[Autowire('%env(WONDERPUSH_NOUVELLE_DEMANDE)%')] private string         $nouvelleDemandeCampaignId,
    #[Autowire('%env(WONDERPUSH_INTERVENTION_BLOQUEE)%')] private string     $interventionBloqueeCampaignId,
    #[Autowire('%env(WONDERPUSH_INTERVENTION_TERMINEE)%')] private string    $interventionTermineeCampaignId,
//    #[Autowire('%env(WONDERPUSH_INTERVENTION_TERMINE_ELU)%')] private string $interventionTermineeEluCampaignId,
    #[Autowire('%env(WONDERPUSH_INTERVENTION_REFUSEE)%')] private string     $interventionRefuseeCampaignId,
    #[Autowire('%env(WONDERPUSH_NOUVEAU_COMMENTAIRE)%')] private string      $nouveauCommentaireCampaignId,
    WonderPushLogger                                                         $wonderPushLogger,
    private LoggerInterface                                                  $logger,
  )
  {
    $this->wonderpush = new WonderPush($accessToken, $appId);
    $this->wonderpush->setLogger($wonderPushLogger);

    $this->typeToCampaignId = [
      RequestInterventionCreatedEvent::class => $this->nouvelleDemandeCampaignId,
      InterventionBlockedEvent::class => $this->interventionBloqueeCampaignId,
      InterventionFinishedEvent::class => $this->interventionTermineeCampaignId,
      InterventionRejectedEvent::class => $this->interventionRefuseeCampaignId,
      CommentAddedEvent::class => $this->nouveauCommentaireCampaignId,
    ];
  }

  private function getCampaignIdForEvent(string $event): ?string
  {
    return $this->typeToCampaignId[$event] ?? null;
  }

  public function __invoke(SendPushNotification $message): void
  {
    if (empty($message->userIds)) {
      return;
    }

    $campaignId = $this->getCampaignIdForEvent($message->event);
    if ($campaignId === null) {
      $this->logger->error('No campaign ID found for event', ['event' => $message->event]);
      return;
    }

    /** @var DeliveriesCreateParams $dcp */
    $dcp = DeliveriesCreateParams::_new();

    /** @var NotificationAlertWeb $notifWeb */
    $notifWeb = NotificationAlertWeb::_new();
    $notifWeb->setTargetUrl($message->url);

    /** @var NotificationAlert $alert */
    $alert = NotificationAlert::_new([
      'text' => $message->text . ($message->detail ? "\n$message->detail" : ''),
    ]);
    $alert->setWeb($notifWeb);

    /** @var Notification $notif */
    $notif = Notification::_new();
    $notif->setAlert($alert);

    $response = $this->wonderpush->deliveries()->create(
      $dcp
        ->setTargetUserIds($message->userIds)
        ->setCampaignId($campaignId)
        ->setNotification($notif)
    );

    if (!$response->isSuccess()) {
      $this->logger->error('notification push not send', ['response' => $response, 'to' => $message->userIds]);
    } else {
      $this->logger->info('notification push send', ['response' => $response, 'to' => $message->userIds]);
    }
  }
}
