<?php

namespace App\Bridge\Mattermost;

readonly class Message
{
  public function __construct(
    public string $text,
  )
  {
  }
}
