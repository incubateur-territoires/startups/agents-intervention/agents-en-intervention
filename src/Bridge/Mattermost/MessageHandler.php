<?php

namespace App\Bridge\Mattermost;

use Symfony\Component\DependencyInjection\Attribute\Autowire;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

#[AsMessageHandler]
class MessageHandler
{
  public function __construct(
    protected HttpClientInterface $client,
    #[Autowire(env: 'default::MATTERMOST_WEBHOOK_URL')]
    protected ?string             $webhookUrl = null,
    #[Autowire(env: 'default::MATTERMOST_CHANNEL')]
    protected ?string             $channel = null,
  )
  {
  }

  /**
   * Effectue l'envoi du message vers Mattermost
   *
   * @param Message $message
   * @return void
   * @throws TransportExceptionInterface
   */
  public function __invoke(Message $message): void
  {
    if (null === $this->webhookUrl) {
      return;
    }

    $data = ['text' => $message->text];
    if ($this->channel) {
      $data['channel'] = $this->channel;
    }

    $this->client->request(
      method: 'POST',
      url: $this->webhookUrl,
      options: [
        'headers' => [
          'Content-Type' => 'application/json',
        ],
        'body' => json_encode($data),
      ]
    );
  }
}
