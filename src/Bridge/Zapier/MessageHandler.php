<?php

namespace App\Bridge\Zapier;

use Symfony\Component\Messenger\Attribute\AsMessageHandler;
use Symfony\Contracts\HttpClient\HttpClientInterface;

#[AsMessageHandler]
readonly class MessageHandler
{
  public function __construct(
    protected HttpClientInterface $client,
  ) {
  }

  public function __invoke(Message $message): void
  {
    $this->client->request(
      method: 'POST',
      url: $message->endpoint,
      options: [
        'headers' => [
          'Accept' => 'application/json',
          'Content-Type' => 'application/json',
        ],
        'body' => json_encode($message->data),
      ]
    );
  }

}
