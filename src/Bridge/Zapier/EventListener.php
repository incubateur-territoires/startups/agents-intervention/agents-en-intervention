<?php

declare(strict_types=1);

namespace App\Bridge\Zapier;

use App\Entity\Employer;
use App\Entity\User;

use App\Event\EmployerCreatedEvent;
use App\Event\InterventionFinishedEvent;
use App\Event\RecurringInterventionCreatedEvent;
use App\Event\RequestInterventionCreatedEvent;
use App\Event\SimpleInterventionCreatedEvent;
use App\Event\UserConnectedEvent;
use App\Event\UserCreatedEvent;
use Psr\EventDispatcher\EventDispatcherInterface;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\DependencyInjection\Attribute\Autowire;
use Symfony\Component\EventDispatcher\Attribute\AsEventListener;
use Symfony\Component\Messenger\Exception\ExceptionInterface;
use Symfony\Component\Messenger\MessageBusInterface;

readonly class EventListener
{
  public function __construct(
    protected EventDispatcherInterface $eventDispatcher,
    protected MessageBusInterface      $messageBus,
    protected LoggerInterface          $logger,
    protected Security                 $security,
    #[Autowire(env: 'default::ZAPIER_ENDPOINT')]
    protected ?string                  $endpoint = null,
  )
  {
  }

  /**
   *
   * Avec à chaque fois les données suivantes :
   *
   * - ID user
   * - Siren
   * - Nom de commune
   * - Horodatage
   *
   * @param array<string, mixed> $data
   * @return void
   * @throws ExceptionInterface
   */
  protected function send(array $data): void
  {
    if (null === $this->endpoint) {
      $this->logger->info('Zapier endpoint not configured, data not dispatched ' . json_encode($data));

      return;
    }

    /** @var User|null $user */
    $user = $this->security->getUser();

    /** @var ?Employer $employer */
    $employer = $data['employer'] ?? null;

    $this->messageBus->dispatch(new Message(
      endpoint: $this->endpoint,
      data: array_replace([
        'user_id' => $user?->getId(),
        'siren' => $employer ? $employer->getSiren() : $user?->getEmployer()->getSiren(),
        'commune' => $employer ? $employer->getName() : $user?->getEmployer()->getName(),
        'timestamp' => time(),
        'datetime' => (new \DateTimeImmutable())->format('c')
      ], $data)
    ));
  }

  #[AsEventListener(event: EmployerCreatedEvent::class)]
  public function onEmployerCreated(EmployerCreatedEvent $event): void
  {
    $this->send([
      'action' => 'creation_collectivite',
      'collectivite_id' => $event->employer->getId(),
      'collectivite_name' => $event->employer->getName(),
    ]);
  }

  #[AsEventListener(event: UserConnectedEvent::class)]
  public function onUserConnected(UserConnectedEvent $event): void
  {
    $user = $event->user;

    $this->send([
      'action' => UserConnectedEvent::getType(),
      'user_id' => $user->getId(),
    ]);
  }

  #[AsEventListener(event: UserCreatedEvent::class)]
  public function onUserCreated(UserCreatedEvent $event): void
  {
    $user = $event->user;

    $this->send([
      'action' => 'user_created',
      'user_id' => $user->getId(),
    ]);
  }

  #[AsEventListener(event: RequestInterventionCreatedEvent::class)]
  public function onRequestCreated(RequestInterventionCreatedEvent $event): void
  {
    $intervention = $event->getIntervention();
    $this->send([
      'action' => 'creation_demande_intervention',
      'intervention_id' => $intervention->getId()
    ]);
  }

  #[AsEventListener(event: RecurringInterventionCreatedEvent::class)]
  public function onRecurringInterventionCreated(RecurringInterventionCreatedEvent $event): void
  {
    $intervention = $event->getIntervention();
    $this->send([
      'action' => 'creation_intervention_recurrente',
      'intervention_id' => $intervention->getId()
    ]);
  }

  #[AsEventListener(event: SimpleInterventionCreatedEvent::class)]
  public function onSimpleInterventionCreated(SimpleInterventionCreatedEvent $event): void
  {
    $intervention = $event->getIntervention();
    $this->send([
      'action' => 'creation_intervention_simple',
      'intervention_id' => $intervention->getId()
    ]);
  }

  #[AsEventListener(InterventionFinishedEvent::class)]
  public function onInterventionFinished(InterventionFinishedEvent $event): void
  {
    $this->send([
      'action' => 'intervention_finalisee',
      'intervention_id' => $event->intervention->getId()
    ]);
  }

}
