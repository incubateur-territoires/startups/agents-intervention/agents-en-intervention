<?php

namespace App\Bridge\Zapier;

readonly class Message
{
  /**
   * @param string $endpoint
   * @param array<string, mixed> $data
   */
  public function __construct(
    public string $endpoint,
    public array $data,
  ) {
  }
}
