<?php

namespace App\Service;

use App\Entity\DashboardMetrics;
use App\Entity\Intervention;
use App\Entity\Metrics;
use App\Entity\Priority;
use App\Entity\Role;
use App\Entity\Status;
use App\Entity\User;
use Doctrine\DBAL\ArrayParameterType;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;

readonly class MetricsService
{
  /**
   * @var array<string>
   */
  const array TO_EXCLUDE = ['ANCT', 'ANSM', 'SILAB', 'Arcie-en-Val / GIP RECIA', 'SIDEC du Jura'];

  public function __construct(
    private EntityManagerInterface $em,
  )
  {
  }

  public function getForMetricsPage(): Metrics
  {
    $metrics = new Metrics();
    $metrics->nbCollectivites = $this->nbCollectivites();
    $metrics->nbCollectivitesActives = $this->nbCollectivitesActives();
    $metrics->nbUtilisateurs = $this->nbUtilisateurs();
    $metrics->nbInterventions = $this->nbInterventions();

    return $metrics;
  }

  public function getMetricsForDashboard(User $user, int $employerId): DashboardMetrics
  {
    $hideGeneratedInterventionsAfterDays = 1;

    $metrics = new DashboardMetrics();
    $dql = 'SELECT i, p FROM App\Entity\Intervention i JOIN i.participants p WHERE i.employer = :employerId AND (i.recurrenceReference IS NULL OR (i.recurrenceReference IS NOT NULL AND i.startAt <= :afterDate))';

    if ($user->hasRole(Role::Elected)) {
      $dql .= ' and i.author = :userId ';
    }

    $query = $this->em->createQuery($dql);
    $query->setParameter(':employerId', $employerId);
    $query->setParameter('afterDate', new \DateTime("+$hideGeneratedInterventionsAfterDays days"));

    if ($user->hasRole(Role::Elected)) {
      $query->setParameter(':userId', $user->getId());
    }

    /** @var Intervention[] $interventions */
    $interventions = $query->getResult();

    foreach ($interventions as $intervention) {
      switch ($intervention->getStatus()) {
        case Status::Blocked:
          $metrics->status_blocked++;
          break;
        case Status::InProgress:
          $metrics->status_in_progress++;
          break;
        case Status::ToDo:
          if ($intervention->getParticipants()->count() === 0) {
            $metrics->status_not_assigned++;
          } else {
            $metrics->status_todo++;
          }
          break;
        case Status::Finished:
          $metrics->status_finished++;
          break;
        default:
          break;
      }

      if (
        $intervention->getPriority() === Priority::Urgent
        && $intervention->getStatus() !== Status::Finished
      ) {
        $metrics->urgent++;
      }
    }

    return $metrics;
  }

  private function nbCollectivitesActives(): int
  {
    $date = new \DateTime();
    $date->modify('-30 days');

    $dql = 'SELECT DISTINCT e.id FROM App\Entity\Intervention i JOIN i.employer e WHERE i.createdAt >= :date GROUP BY e.id, i.id HAVING count(i.id) <=3';
    $query = $this->em->createQuery($dql);
    $query->setParameter(':date', $date);
    return count($query->getResult());
  }

  /**
   * @throws NonUniqueResultException
   * @throws NoResultException
   */
  private function nbCollectivites(): int
  {
    $dql = 'SELECT count(e.id) FROM App\Entity\Employer e WHERE e.name NOT IN (:names)';
    $query = $this->em->createQuery($dql);
    /* @SuppressWarnings("php:S1192") */
    $query->setParameter(':names', self::TO_EXCLUDE, ArrayParameterType::STRING);
    return intval($query->getSingleScalarResult());
  }

  private function nbUtilisateurs(): int
  {
    $dql = 'SELECT count(u.id) FROM App\Entity\User u JOIN u.employer e  WHERE e.name NOT IN (:names)';
    $query = $this->em->createQuery($dql);
    /* @SuppressWarnings("php:S1192") */
    $query->setParameter(':names', self::TO_EXCLUDE, ArrayParameterType::STRING);
    return intval($query->getSingleScalarResult());
  }

  /**
   * @throws NonUniqueResultException
   * @throws NoResultException
   */
  private function nbInterventions(): int
  {
    $dql = 'SELECT count(i.id) FROM App\Entity\Intervention i JOIN i.employer e  WHERE e.name NOT IN (:names)';
    $query = $this->em->createQuery($dql);
    /* @SuppressWarnings("php:S1192") */
    $query->setParameter(':names', self::TO_EXCLUDE, ArrayParameterType::STRING);
    return intval($query->getSingleScalarResult());
  }
}
