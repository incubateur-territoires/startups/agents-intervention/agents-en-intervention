<?php

namespace App\Service;

use App\Entity\Intervention;
use App\Entity\Status;
use App\Model\Recurrence;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Psr\Log\LoggerInterface;

readonly class RecurrenceServiceImpl implements RecurrenceService
{
  public function __construct(
    private EntityManagerInterface $entityManager,
    private LoggerInterface        $logger,
  )
  {
    $this->logger->debug(self::class . ' __construct');
  }

  #[\Override]
  public function removeGeneratedInterventionsFor(
    Intervention $intervention,
    bool $removeAllReferences = true,
    \DateTimeImmutable $now = new \DateTimeImmutable('now', new \DateTimeZone('UTC'))): void
  {
    $this->logger->debug(self::class . ' removeGeneratedInterventionsFor');
    foreach ($intervention->getGeneratedInterventions() as $it) {
      if ($it->getStartAt() <= $now) {
        // Enlever la référence à l'événement supprimé dans les événements générés passés
        if ($removeAllReferences) {
          $this->logger->debug(self::class . ' removeGeneratedInterventionsFor:: remove recurrence reference for intervention ' . $it->getId());
          $it->setRecurrenceReference(null);
        }
      } else {
        // Supprimer l'ensemble des évènements à venir qui ont été générés par cette récurrence
        $this->logger->info(self::class . ' removeGeneratedInterventionsFor:: remove intervention ' . $it->getId());
        $this->entityManager->remove($it);
      }
    }

    $this->entityManager->flush();
  }

  /**
   * @throws Exception
   */
  #[\Override]
  public function addGeneratedInterventionsFor(Intervention $intervention, string $date = 'now'): void
  {
    $start = new \DateTimeImmutable($date, new \DateTimeZone('UTC'));
    $commune = $intervention->getEmployer();

    $rec = new Recurrence($intervention, $start, $intervention->getEndedAt(), new \DateTimeZone($commune->getTimezone()));
    foreach ($rec->getIterator() as $_) {
      /** @var Intervention $_intervention */
      $_intervention = $_;
      $intervention->addGeneratedIntervention($_intervention);
      $this->entityManager->persist($_intervention);
      $this->logger->debug(self::class . ' addGeneratedInterventionsFor:: persisted intervention ' . $intervention->getId());
    }

    $this->entityManager->flush();
  }

  #[\Override]
  public function modifyGeneratedInterventionsFor(Intervention $intervention): void
  {
    $now = new \DateTimeImmutable('now', new \DateTimeZone('UTC'));
    foreach ($intervention->getGeneratedInterventions() as $it) {
      $this->logger->debug(self::class . ' modifyGeneratedInterventionsFor:: check if intervention ' . $it->getId() . ' is candidate for modification ' . $it->getStartAt()?->format('c') . ' ' . $now->format('c') . ' => ' . ($it->getStartAt() > $now));
      if ($it->getStartAt() > $now) {
        $it
          ->setDescription($intervention->getDescription())
          ->setPriority($intervention->getPriority())
          ->setType($intervention->getType())
          ->setLocation($intervention->getLocation())
          ->setTitle($intervention->getTitle())
          ->setStatus(Status::ToDo)
          ->setParticipants($intervention->getParticipants());
        $this->logger->debug(self::class . ' modifyGeneratedInterventionsFor:: modify intervention ' . $it->getId());
        $this->entityManager->persist($it);
      }
    }
    $this->entityManager->flush();
  }
}
