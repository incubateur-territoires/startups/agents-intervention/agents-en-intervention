<?php

namespace App\Service;

use App\Entity\Notification;
use App\Entity\User;
use App\Repository\NotificationRepository;
use Doctrine\ORM\EntityManagerInterface;

readonly class NotificationServiceImpl implements NotificationService
{
  public function __construct(
    private EntityManagerInterface $entityManager,
    private NotificationRepository $notificationRepository,
  )
  {
  }

  /**
   * Crée une notification pour un utilisateur concernant une intervention.
   *
   * @param Notification $notification La notification à créer
   */
  #[\Override]
  public function createNotification(Notification $notification, bool $flush = false): void
  {
    $this->entityManager->persist($notification);
    if ($flush) {
      $this->entityManager->flush();
    }
  }

  /**
   * Marque une notification comme lue.
   *
   * @param Notification $notification La notification à marquer comme lue
   */
  #[\Override]
  public function markAsRead(Notification $notification): void
  {
    $notification->setIsRead(true);
    $this->entityManager->flush();
  }

  /**
   * Marque toutes les notifications non lues d'un utilisateur comme lues.
   *
   * @param User $user L'utilisateur pour lequel marquer les notifications comme lues
   */
  #[\Override]
  public function markAllAsRead(User $user): void
  {
    $notifications = $this->notificationRepository->getUnreadNotifications($user);
    foreach ($notifications as $notification) {
      $notification->setIsRead(true);
    }
    $this->entityManager->flush();
  }
}
