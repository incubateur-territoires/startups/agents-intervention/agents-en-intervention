<?php

namespace App\Service;

use App\Entity\Employer;
use stdClass;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

readonly class EntrepriseService
{

  public function __construct(
    private HttpClientInterface $httpClient,
    private TimezoneService     $timezoneService,
  )
  {
  }

  public function getEmployerFromSiret(string $siret): Employer
  {
    $orgDetails = $this->request($siret);

    $employer = new Employer();
    $employer->setName($orgDetails->nom_raison_sociale);
    $employer->setSiren($orgDetails->siren);

    $employer->setLatitude(floatval($orgDetails->siege->latitude));
    $employer->setLongitude(floatval($orgDetails->siege->longitude));
    $employer->setTimezone($this->timezoneService->getTimezoneFromCoordinates(
      $employer->getLatitude(),
      $employer->getLongitude()
    ));

    return $employer;
  }

  /**
   * @param string $siret
   * @return bool
   * @throws ClientExceptionInterface
   * @throws RedirectionExceptionInterface
   * @throws ServerExceptionInterface
   * @throws TransportExceptionInterface
   */
  public function isCollectiviteTerritoriale(string $siret): bool
  {
    $result = $this->request($siret);
    return $result->complements->collectivite_territoriale !== null || $result->complements->est_service_public === true;
  }

  /**
   * @param string $siret
   * @return object{
   *    'siren': string,
   *    'nom_raison_sociale': string,
   *    'siege': object{
   *     'libelle_commune': string,
   *     'latitude': string,
   *     'longitude': string,
   *    },
   *    'complements': object{
   *      'collectivite_territoriale': object{'code': string}|null,
   *      'est_service_public': bool|null,
   *    }
   *  }
   * @throws ClientExceptionInterface
   * @throws RedirectionExceptionInterface
   * @throws ServerExceptionInterface
   * @throws TransportExceptionInterface
   */
  private function request(string $siret): object
  {
    $siren = substr($siret, 0, 9);

    $responseJson = $this->httpClient->request(
      'GET',
      'https://recherche-entreprises.api.gouv.fr/search?page=1&per_page=25&est_collectivite_territoriale=true&q=' . $siren,
      [
        'headers' => [
          'Accept: application/json',
        ],
      ],
    )->getContent();

    /** @var object{'results': object{
     *   'siren': string,
     *   'nom_raison_sociale': string,
     *   'siege': stdClass&object{
     *    'libelle_commune': string,
     *    'latitude': string,
     *    'longitude': string,
     *   },
     *   'complements': object{
     *     'collectivite_territoriale': object{'code': string}|null,
     *     'est_service_public': bool|null,
     *   }
     *  }[]} $response
     */
    $response = json_decode($responseJson);
    assert(is_object($response));

    $results = $response->results;

    $orgDetails = null;
    foreach ($results as $result) {
      if ($result->siren === $siren) {
        $orgDetails = $result;
        break;
      }
    }

    if ($orgDetails === null) {
      throw new \LogicException('Erreur: SIREN introuvable');
    }

    // Si les coordonnées sont nulles, on cherche sur l'API Nominatim
    $nominatimResponse = $this->httpClient->request(
      'GET',
      'https://nominatim.openstreetmap.org/search?format=geojson&countrycodes=fr&q=' . $orgDetails->siege->libelle_commune,
      [
        'headers' => [
          'Accept: application/json',
        ],
      ],
    )->getContent();

    /**
     * @var object{'features': object{
     *   'geometry': object{
     *   'coordinates': array{0: float, 1: float}
     *   }
     *   }[]} $nominatim
     */
    $nominatim = json_decode($nominatimResponse);
    $features = $nominatim->features;

    if (!empty($features)) {
      $orgDetails->siege->latitude = $features[0]->geometry->coordinates[1];
      $orgDetails->siege->longitude = $features[0]->geometry->coordinates[0];
    }

    return $orgDetails;
  }
}
