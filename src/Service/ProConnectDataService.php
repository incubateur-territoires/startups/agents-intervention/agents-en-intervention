<?php

declare(strict_types=1);

namespace App\Service;

use App\Dto\ProConnectUserDetails;
use App\Entity\Employer;
use App\Entity\Role;
use App\Entity\User;
use App\Event\EmployerCreatedEvent;
use App\Repository\EmployerRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Psr\EventDispatcher\EventDispatcherInterface;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

readonly class ProConnectDataService
{
  public function __construct(
    private EmployerRepository       $employerRepository,
    private EntityManagerInterface   $entityManager,
    private EntrepriseService        $entrepriseService,
    private EventDispatcherInterface $eventDispatcher,
    private UserRepository           $userRepository,
  )
  {
  }

  /**
   * @param ProConnectUserDetails $userDetails
   * @return User
   */
  public function createUser(ProConnectUserDetails $userDetails): User
  {
    $user = new User();
    $user->setEmail($userDetails->email);
    $user->setLogin($userDetails->email);
    $user->setRoles([Role::Agent]);
    $user->setPassword('-'); // Insertion d'un faux mot de passe
    $user->setActive(true);
    $user->setConnectedAt(new \DateTimeImmutable());
    $user->setFirstName($userDetails->given_name ?? '');
    $user->setLastName($userDetails->usual_name ?? '');
    $user->setPhoneNumber($userDetails->phone_number ?? null);

    $this->entityManager->persist($user);

    return $user;
  }

  /**
   * @param ProConnectUserDetails $userDetails
   * @param Employer $employer
   * @return User
   */
  public function findOrCreateUser(ProConnectUserDetails $userDetails, Employer $employer): User
  {
    $user = $this->userRepository->getByEmail($userDetails->email);
    if (null === $user) {
      $user = $this->createUser($userDetails);

      /*
       * Si c'est le premier utilisateur de l'employeur, on lui donne le rôle
       * d'administrateur de l'employeur.
       */
      if ($employer->getSiren() === '130026032') {
        // Cas spécial pour l'ANCT
        $user->setRoles([Role::Admin]);
      } elseif ($employer->getUsers()->isEmpty()) {
        $user->setRoles([Role::AdminEmployer]);
      } else {
        $user->setRoles([Role::Elected]);
        // TODO Notify the admin of the employer that a new user has been created and it should need other role
      }
      $user->setEmployer($employer);
    }

    return $user;
  }

  /**
   * @param ProConnectUserDetails $userDetails
   * @return Employer
   * @throws ClientExceptionInterface
   * @throws RedirectionExceptionInterface
   * @throws ServerExceptionInterface
   * @throws TransportExceptionInterface
   */
  public function createEmployer(ProConnectUserDetails $userDetails): Employer
  {
    $employer = $this->entrepriseService->getEmployerFromSiret($userDetails->siret);

    $this->entityManager->persist($employer);
    $this->eventDispatcher->dispatch(new EmployerCreatedEvent($employer, false));

    return $employer;
  }

  /**
   * @param ProConnectUserDetails $userDetails
   * @return Employer
   * @throws ClientExceptionInterface
   * @throws RedirectionExceptionInterface
   * @throws ServerExceptionInterface
   * @throws TransportExceptionInterface
   */
  public function findOrCreateEmployer(ProConnectUserDetails $userDetails): Employer
  {
    /** @var Employer|null $employer */
    $employer = $this->employerRepository->findOneBy(['siren' => substr($userDetails->siret, 0, 9)]);
    if (null === $employer) {
      $employer = $this->createEmployer($userDetails);
    }

    return $employer;
  }

  /**
   * Synchronise les données entre ProConnect et Agents en intervention
   * puis retourne l'utilisateur.
   *
   * @throws ClientExceptionInterface
   * @throws RedirectionExceptionInterface
   * @throws ServerExceptionInterface
   * @throws TransportExceptionInterface
   */
  public function sync(ProConnectUserDetails $userDetails): User
  {
    $employer = $this->findOrCreateEmployer($userDetails);
    $user = $this->findOrCreateUser($userDetails, $employer);
    $user->setEmployer($employer);

    $this->entityManager->flush();

    return $user;
  }
}
