<?php

namespace App\Service;

use App\Validator\EventValidatorInterface;

class EventProcessor
{
  /** @var array<string, EventValidatorInterface> */
  private array $validators = [];

  /**
   * @param iterable<EventValidatorInterface> $validators
   */
  public function __construct(
    iterable $validators,
  )
  {
    foreach ($validators as $validator) {
      /** @var EventValidatorInterface $validator */
      $this->validators[$validator->getEventType()] = $validator;
    }
  }

  /**
   * @param string $type
   * @param array<string, mixed> $data
   * @return array
   */
  public function process(string $type, array $data): array
  {
    if (!isset($this->validators[$type])) {
      return [
        'valid' => false,
        'errors' => ['type' => 'Unsupported event type', 'type_value' => $type],
        'type' => $type,
        'raw' => $data,
      ];
    }

    return $this->validators[$type]->validate($data);
  }
}
