<?php

namespace App\Service;

use App\Entity\Comment;
use App\Entity\Intervention;
use App\Entity\Notification;
use App\Entity\User;
use App\Service\NotificationService;
use WonderPush\Obj\DeliveriesCreateResponse;

class NotificationServiceTest implements NotificationService
{
  #[\Override]
  public function createNotification(Notification $notification): void
  {
    // TODO: Implement createNotification() method.
  }

  #[\Override]
  public function markAsRead(Notification $notification): void
  {
    // TODO: Implement markAsRead() method.
  }

  #[\Override]
  public function markAllAsRead(User $user): void
  {
    // TODO: Implement markAllAsRead() method.
  }
}
