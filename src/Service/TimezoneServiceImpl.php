<?php

namespace App\Service;

use LogicException;
use Psr\Log\LoggerInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Throwable;

/**
 * @inheritDoc
 */
readonly class TimezoneServiceImpl implements TimezoneService
{
  public function __construct(
    private HttpClientInterface $httpClient,
    private LoggerInterface $logger,
  )
  {
  }

  /**
   * @inheritDoc
   */
    public function getTimezoneFromCoordinates(float $latitude, float $longitude): string
    {
      try {
        $response = $this->httpClient->request(
            'GET',
            'https://api.timezonedb.com/v2.1/get-time-zone',
            [
                'query' => [
                    'key' => $_ENV['TIMEZONE_DB_API_KEY'],
                    'format' => 'json',
                    'by' => 'position',
                    'lat' => $latitude,
                    'lng' => $longitude,
                    'fields' => 'zoneName',
                ],
            ],
        );

        $content = $response->toArray();
      } catch (Throwable $e) {
        $this->logger->error('Erreur lors de la récupération du fuseau horaire', [
            'exception' => $e,
        ]);
        throw new LogicException('Erreur ', -1, $e);
      }
        return $content['zoneName'];
    }
}
