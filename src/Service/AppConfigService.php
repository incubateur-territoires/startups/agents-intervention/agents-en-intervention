<?php

// src/Service/AppConfigService.php
namespace App\Service;

use App\Entity\AppConfig;
use App\Repository\AppConfigRepository;
use Doctrine\ORM\EntityManagerInterface;
use JsonException;
use Psr\Cache\InvalidArgumentException;
use Symfony\Contracts\Cache\CacheInterface;

readonly class AppConfigService
{
  public function __construct(
    private AppConfigRepository $repository,
    private CacheInterface      $cache,
    private EntityManagerInterface $em,
  )
  {
  }

  /**
   * @param string $key
   * @return AppConfig|null
   * @throws InvalidArgumentException
   */
  public function getConfig(string $key): ?AppConfig
  {
    // Utilisation du cache pour éviter des requêtes répétées
    /** @var AppConfig|null */
    return $this->cache->get('config_' . $key, fn() => $this->repository->find($key));
  }

  /**
   * @param array<string> $keys
   * @return array<string, AppConfig>
   * @throws InvalidArgumentException
   */
  public function getConfigs(array $keys): array
  {
    $configs = [];
    foreach ($keys as $key) {
      $config = $this->getConfig($key);
      if($config) {
        $configs[$key] = $config;
      }
    }
    return $configs;
  }

  /**
   * @param array<string> $keys
   * @return array<string, mixed>
   * @throws JsonException|InvalidArgumentException
   */
  public function getConfigsValues(array $keys): array
  {
    $configs = $this->getConfigs($keys);
    return array_map(fn ($config) => $config->getTypedValue(), $configs);
  }

  /**
   * @param string $key
   * @param mixed $value
   * @return void
   * @throws JsonException
   * @throws InvalidArgumentException
   */
  public function setConfig(string $key, mixed $value): void
  {
    $config = $this->repository->find($key) ?? (new AppConfig())->setKeyName($key);
    if (!$config instanceof AppConfig) {
      return;
    }

    $config->setKeyName($key);
    $config->setTypedValue($value);

    $this->em->persist($config);
    $this->em->flush();

    // Invalidate cache
    $this->cache->delete('config_' . $key);
  }

  /**
   * @param array<string, mixed> $getData
   * @return void
   * @throws InvalidArgumentException
   * @throws JsonException
   */
  public function setConfigs(array $getData): void
  {
    foreach ($getData as $key => $value) {
      $this->setConfig($key, $value);
    }
  }
}
