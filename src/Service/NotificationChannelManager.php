<?php

namespace App\Service;

use App\Entity\Notification;

//use App\Message\SendEmailNotification;
//use App\Message\SendSmsNotification;
use App\Entity\User;
use App\Message\SendPushNotification;
use Symfony\Component\Messenger\Exception\ExceptionInterface;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Contracts\EventDispatcher\Event;

readonly class NotificationChannelManager
{
  public function __construct(
    private MessageBusInterface $bus,
    private NotificationService $notificationService
  )
  {
  }

  /**
   * Envoie la notification sur les canaux préférés de l'utilisateur.
   *
   * @param Notification $notification
   * @param Event $event
   * @param string $url
   * @param User[]|null $users
   * @throws ExceptionInterface
   */
  public function sendNotification(Notification $notification, Event $event, string $url, ?array $users): void
  {
//    $preferences = $user->getNotificationPreferences();

//    if (in_array('email', $preferences[$type] ?? [])) {
//      $this->sendEmail($user, $message);
//    }
//
//    if (in_array('sms', $preferences[$type] ?? [])) {
//      $this->sendSms($user, $message);
//    }

//    if (in_array('push', $preferences[$type] ?? [])) {;
    if (is_array($users)) {
      $this->sendPushNotification($notification, $event, $url, $users);
    }
//    }

    // Tu pourrais aussi ajouter la notification interne ici :
    $this->sendInternalNotification($notification, $users);
  }

//  private function sendEmail(User $user, string $message): void
//  {
//    $this->bus->dispatch(new SendEmailNotification($user->getEmail(), 'Nouvelle notification', $message));
//  }
//
//  private function sendSms(User $user, string $message): void
//  {
//    $this->bus->dispatch(new SendSmsNotification($user->getPhoneNumber(), $message));
//  }

  /**
   * @param Notification $notification
   * @param Event $event
   * @param User[] $users
   * @return void
   * @throws ExceptionInterface
   */
  private function sendPushNotification(Notification $notification, Event $event, string $url, array $users): void
  {
    $this->bus->dispatch(new SendPushNotification(
      $event::class,
      // Ne pas envoyer les Users directement, car ils ne sont pas nécessaires
      // pour le handler et les données seront sérialisées.
      array_map(fn($user) => $user->getId(), $users),
      $url,
      $notification->getTitle(),
      $notification->getMessage() ?? ''
    ));
  }

  /**
   * @param Notification $notification
   * @param User[]|null $users
   * @return void
   */
  private function sendInternalNotification(Notification $notification, ?array $users): void
  {
    if (is_array($users)) {
      foreach ($users as $user) {
        $_notification = clone $notification;
        $_notification->setUser($user);
        $this->notificationService->createNotification($_notification);
      }
    } else {
      $this->notificationService->createNotification($notification);
    }

  }
}
