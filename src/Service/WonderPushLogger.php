<?php

namespace App\Service;

use Psr\Log\LoggerInterface;
use WonderPush\Util\Logger;

readonly class WonderPushLogger implements Logger
{
  public function __construct(
    private LoggerInterface $logger
  )
  {
  }

  /**
   * @param mixed $level
   * @param mixed $message
   * @param array<string, mixed> $context
   * @return void
   */
  #[\Override]
  public function log(mixed $level, mixed $message, array $context = []): void
  {
    if (is_string($message)) {
      $this->logger->log($level, $message, $context);
    }
  }
}
