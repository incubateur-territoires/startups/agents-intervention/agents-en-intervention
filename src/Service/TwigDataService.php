<?php

namespace App\Service;

use App\Entity\ActionLog;
use App\Entity\ActionLoggableInterface;
use App\Entity\Employer;
use App\Entity\User;
use App\Repository\EmployerRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\SecurityBundle\Security;

readonly class TwigDataService
{
  public function __construct(
    private EmployerRepository     $employerRepository,
    private EntityManagerInterface $entityManager,
    private Security               $security,
    private UserRepository         $userRepository
  )
  {
  }

  /**
   * @return array<object>
   */
  public function getCommunes(): array
  {
    $ret = [];
    if ($this->security->isGranted('ROLE_ADMIN')) {
      $ret = $this->employerRepository->findBy([], ['name' => 'asc']);
    }

    return $ret;
  }

  /**
   * @param int $employerId
   * @return array<User>
   */
  public function getActivesAgents(int $employerId): array
  {
    return $this->userRepository->getActiveAgents($employerId);
  }

  /**
   * @param object $entity
   * @return array<ActionLog>
   */
  public function getLogsForEntity(object $entity): array
  {
    if(!method_exists($entity, 'getId') || !$entity instanceof ActionLoggableInterface) {
      return [];
    }

    return $this->entityManager->getRepository(ActionLog::class)
      ->findBy([
        'entityClass' => str_replace('Proxies\__CG__\\', '', $entity::class),
        'entityId' => $entity->getId(),
      ], ['timestamp' => 'DESC']);
  }
}
