<?php

declare(strict_types=1);

namespace App\Service;

use Aws\S3\S3Client;
use Symfony\Component\HttpFoundation\File\UploadedFile;

interface S3Service
{
  /**
   * Renvoie true si le fichier existe dans le seau.
   * @param string $key la clé (le nom) du fichier.
   * @return bool si le fichier existe dans le seau.
   */
  public function fileExist(string $key): bool;

  /**
   * Supprime un fichier.
   * @param string $key la clé.
   */
  public function deleteFile(string $key): void;

  /**
   * Renvoie l'URL signée.
   * @param string $key la clé.
   * @return string l'URL signée.
   */
  public function getSignedURL(string $key): string;

  /**
   * Ajoute un fichier.
   * @param UploadedFile $file le fichier.
   * @param string $key la clé.
   */
  public function putFile(UploadedFile $file, string $key): void;

  /**
   * Ajoute un fichier.
   * @param mixed $content le contenu du fichier.
   * @param string $key la clé.
   */
  public function putFileContent(mixed $content, string $key): void;

  /**
   * Récupère un fichier.
   * @param string $key la clé.
   */
  public function getFile(string $key): mixed;
}
