<?php

namespace App\Service;

use App\Entity\Intervention;
use App\Entity\Notification;
use App\Entity\User;

interface NotificationService
{
  public function createNotification(Notification $notification): void;
  public function markAsRead(Notification $notification): void;
  public function markAllAsRead(User $user): void;
}
