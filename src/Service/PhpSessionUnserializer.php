<?php

namespace App\Service;

use Exception;
use RuntimeException;

/**
 * Service permettant de désérialiser une session PHP / Symfony.
 * Prévient les erreurs remontées par le unserialize de PHP.
 *
 * @package App\Service
 *
 */
class PhpSessionUnserializer
{
  private int $offset = 0;
  private string $alreadyRead = '';

  /**
   * @param array<string> $array
   * @param array<string>|string $keywords
   * @return string
   */
  private function readUntil(array &$array, array|string $keywords): string
  {
    if (is_string($keywords)) {
      $keywords = [$keywords];
    }
    $value = '';
    while (count($array) && !in_array($array[0], $keywords)) {
      $value .= array_shift($array);
      $this->offset++;
    }
    $this->alreadyRead .= $value;
    $this->alreadyRead .= array_shift($array);
    $this->offset++;

    return $value;
  }

  /**
   * @param array<string> $array
   * @param int $length
   * @return string
   */
  public function readChars(array &$array, int $length): string
  {
    $value = '';

    for ($i = 0; $i < $length; $i++) {
      $value .= array_shift($array);
      $this->offset++;
    }
    $this->alreadyRead .= $value;
    return $value;
  }

  /**
   * @param array<string> $array
   * @return string
   */
  private function readString(array &$array): string
  {
    $size = $this->readUntil($array, ':');
    $this->readUntil($array, '"');
    $string = $this->readChars($array, intval($size));
    $this->readUntil($array, ';');
    return $string;
  }

  /**
   * @param array<string> $array
   * @return float|int
   * @throws Exception
   */
  private function readNumber(array &$array): float|int
  {
    $numberString = $this->readUntil($array, ';');
    if (!is_numeric($numberString)) {
      throw new RuntimeException("Parse error: \"$numberString\" is not a number.");
    }
    return str_contains($numberString, '.') ? (float)$numberString : (int)$numberString;
  }

  /**
   * @param array<string> $array
   * @return bool
   */
  private function readBoolean(array &$array): bool
  {
    $booleanString = $this->readUntil($array, ';');
    if ($booleanString !== '0' && $booleanString !== '1') {
      throw new RuntimeException("Parse error: \"$booleanString\" is not a boolean number.");
    }
    return (bool)$booleanString;
  }

  private function readNull(): null
  {
    return null;
  }

  /**
   * @param array<string> $array
   * @param array<string> $breadcrumbs
   * @return array<string,mixed>
   * @throws Exception
   */
  private function readArray(array &$array, array $breadcrumbs): array
  {
    $length = (int)$this->readUntil($array, ':');
    $resultArray = [];
    $this->readUntil($array, '{');
    for ($i = 0; $i < $length; $i++) {
      $key = $this->readValue($array, $breadcrumbs);
      $value = $this->readValue($array, array_merge($breadcrumbs, ["$key"]));

      if ($key === '_security_main' && is_string($value)) {
        $value =  unserialize(trim($value,'"'));
      }

      $resultArray[$key] = $value;
    }
    $this->readUntil($array, '}');
    return $resultArray;
  }

  /**
   * @param array<string> $array
   * @param array<string> $breadcrumbs
   * @return object
   * @throws Exception
   */
  private function readObject(array &$array, array $breadcrumbs): object
  {
    $this->readUntil($array, ':');
    $this->readUntil($array, '"');
    $objectName = $this->readUntil($array, '"');
    $this->readUntil($array, ':');

    $resultObject = new \stdClass();
    $insideResultObject = $resultObject->$objectName = new \stdClass();

    $length = (int)$this->readUntil($array, ':');
    $this->readUntil($array, '{');
    for ($i = 0; $i < $length; $i++) {
      $key = $this->readValue($array, $breadcrumbs);
      $value = $this->readValue($array, array_merge($breadcrumbs, ["$key"]));
      if (str_starts_with((string) $key, "\0")) {
        $splitted = explode("\0", (string) $key);
        $key = array_pop($splitted);
        $insideResultObject->{$key} = $value;
      } else {
        $insideResultObject->{$key} = $value;
      }
    }
    $this->readUntil($array, '}');
    return $resultObject;
  }

  /**
   * @param array<string> $array
   * @param array<string> $breadcrumbs
   * @return object
   * @throws Exception
   */
  private function readClass(array &$array, array $breadcrumbs): object
  {
    $this->readUntil($array, ':');
    $this->readUntil($array, '"');
    $className = $this->readUntil($array, '"');
    $this->readUntil($array, ':');

    $resultClass = new \stdClass();
    $insideResultClass = $resultClass->$className = [];

    $length = (int)$this->readUntil($array, ':');
    $this->readUntil($array, '{');
    for ($i = 0; $i < $length; $i++) {
      $value = $this->readValue($array, $breadcrumbs);
      $insideResultClass[] = $value;
    }
    $this->readUntil($array, '}');
    return $resultClass;
  }

  /**
   * @param array<string> $array
   * @return mixed
   */
  private function readEnum(array &$array): mixed
  {
    $this->readUntil($array, ':');
    $this->readUntil($array, '"');
    $enumName = $this->readUntil($array, '"');
    $this->readUntil($array, ';');

    return $enumName;
  }

  /**
   * @param array<string> $array
   * @param array<string> $breadcrumbs
   * @return mixed
   * @throws Exception
   */
  private function readValue(array &$array, array $breadcrumbs): mixed
  {
    $type = $this->readUntil($array, [':', ';']);
    switch (strtolower($type)) {
      case 's':
        return $this->readString($array);
      case 'i':
      case 'd':
      case 'r':
        return $this->readNumber($array);
      case 'a':
        return $this->readArray($array, $breadcrumbs);
      case 'o':
        return $this->readObject($array, $breadcrumbs);
      case 'c':
        return $this->readClass($array, $breadcrumbs);
      case 'b':
        return $this->readBoolean($array);
      case 'n':
        return $this->readNull();
      case 'e': // enum
        return $this->readEnum($array);
      case '':
        return null;
      default:
        dump($this->alreadyRead);
        throw new RuntimeException("Unknown type: \"$type\" at offset $this->offset");
    }
  }

  /**
   * @param string $text
   * @return array<string,mixed>
   * @throws Exception
   */
  public function unserialize(string $text): array
  {
    $result = [];
    $chars = preg_split('/([^;}]+)\|/', $text, -1, PREG_SPLIT_DELIM_CAPTURE | PREG_SPLIT_NO_EMPTY) ?: [];

    for ($i = 0; $i < count($chars); $i += 2) {
      $key = $chars[$i];
      $value = $chars[$i + 1];
      $result[$key] = $this->unserializeValue($key, $value);
    }
    return $result;
  }

  /**
   * @param string $key
   * @param string $value
   * @return array<string>
   */
  public function unserializeValue(string $key, string $value): array
  {
    $result = [];

    $array = preg_split('//u', $value, -1, PREG_SPLIT_NO_EMPTY) ?: [];
    do {
      try {
        $val = $this->readValue($array, ["$key >>"]);
        if (!is_array($val)) {
          $val = [$val];
        }
        $result = array_merge($result, $val);
      } catch (Exception $e) {
        throw new RuntimeException($e->getMessage() . ', Left text: \"' . implode('', $array ?: null) . '\"');
      }
    } while (count($array));

    return $result;
  }
}

/*
_sf2_attributes|
a:8:{
  s:6:"signup";
  a:5:{
    s:5:"siret";
    s:14:"21590122400018";
    s:4:"name";
    s:7:"CAMBRAI";
    s:5:"siren";
    s:9:"215901224";
    s:9:"longitude";
    s:8:"3.234031";
    s:8:"latitude";
    s:7:"50.1754";
  }
  s:10:"csrf-token";
  i:1;
  s:11:"signup_user";
  a:9:{
    s:5:"siret";
    s:14:"21590122400018";
    s:4:"name";
    s:7:"CAMBRAI";
    s:5:"siren";
    s:9:"215901224";
    s:9:"longitude";
    s:8:"3.234031";
    s:8:"latitude";
    s:7:"50.1754";
    s:9:"firstname";
    s:3:"qdf";
    s:8:"lastname";
    s:3:"sdf";
    s:5:"email";
    s:16:"gvg@sylvainlg.fr";
    s:11:"phoneNumber";
    s:10:"0987654321";
  }
  s:23:"_security.last_username";
  s:3:"slg";
  s:3:"jwt";
  s:506:"eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJpYXQiOjE3MzM5MDI2ODEsImV4cCI6MTczMzkzODY4MSwicm9sZXMiOlsiUk9MRV9BRE1JTiIsIlJPTEVfU1VQRVJfQURNSU4iXSwidXNlcm5hbWUiOiJzbGcifQ.LlZbq3VvcDCAIVvibul99y550qDbcOQNU3dQTRJEC53QvcvtQJ7a8zm9iSY7Oetk0Zh8qJ0Sbuq5f9FM62E6bRpoCVl6Xyg0mDep1Q2lIjNbL4aeeR0ZB5GAjbr0q9fqU-zr6YmlAF8pdCPJ_Neetdj2A9PFMnGWaOShr6-CuHKorhKC-gMhcKokL6R8gL1dWyeqgMWzoV04M4X7eKhSprYsj7iV5ntzlPS_Vop78EolVknhRHvCV1vymKV8yw3BZHaLG59e91EAr9q5LHuLQy8XmOwibLBr3su3aYoUgQlBv-iqsjadu8wK-LeZ-2lVx5_UfNyLaDC8Ry1feDpcaA";
  s:14:"_security_main";
  s:8597:"O:74:"Symfony\\Component\\Security\\Core\\Authentication\\Token\\UsernamePasswordToken":3:{i:0;N;i:1;s:4:"main";i:2;a:5:{i:0;O:15:"App\\Entity\\User":14:{s:19:"\000App\\Entity\\User\000id";i:141;s:22:"\000App\\Entity\\User\000login";s:3:"slg";s:25:"\000App\\Entity\\User\000password";s:60:"$2y$13$E6VvyIGxH3d7ULe0JshleObeozPnTefoSLRKJFA63ZumcOO90jDci";s:26:"\000App\\Entity\\User\000firstname";s:7:"Sylvain";s:25:"\000App\\Entity\\User\000lastname";s:8:"LE GLEAU";s:22:"\000App\\Entity\\User\000email";s:29:"sylvain.le.gleau@beta.gouv.fr";s:28:"\000App\\Entity\\User\000phoneNumber";N;s:26:"\000App\\Entity\\User\000createdAt";O:17:"DateTimeImmutable":3:{s:4:"date";s:26:"2024-04-12 10:04:09.000000";s:13:"timezone_type";i:3;s:8:"timezone";s:3:"UTC";}s:24:"\000App\\Entity\\User\000picture";O:18:"App\\Entity\\Picture":7:{s:22:"\000App\\Entity\\Picture\000id";i:72;s:28:"\000App\\Entity\\Picture\000fileName";s:147:"users/user-141-5fcd971ca789d0b05dc402bd661d780e58a3db11b1d6df64fa3165e95fcb367b262188b6d5d6f35121a4be12784e00d226db5f8d44f92db6955f713c18a37b68.jpg";s:23:"\000App\\Entity\\Picture\000url";s:504:"https://dev-aei.cellar-c2.services.clever-cloud.com/users/user-141-5fcd971ca789d0b05dc402bd661d780e58a3db11b1d6df64fa3165e95fcb367b262188b6d5d6f35121a4be12784e00d226db5f8d44f92db6955f713c18a37b68.jpg?X-Amz-Content-Sha256=UNSIGNED-PAYLOAD&X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=FKAFOID6SUCPYPPKI6ZC%2F20241211%2Fus-west-1%2Fs3%2Faws4_request&X-Amz-Date=20241211T092616Z&X-Amz-SignedHeaders=host&X-Amz-Expires=3600&X-Amz-Signature=fc592322db1c7adfaf61a6cacc94993e2bfb6bdb4c5d169a353afb91a38caee7";s:24:"\000App\\Entity\\Picture\000urls";a:3:{s:5:"small";s:510:"https://dev-aei.cellar-c2.services.clever-cloud.com/users/small/user-141-5fcd971ca789d0b05dc402bd661d780e58a3db11b1d6df64fa3165e95fcb367b262188b6d5d6f35121a4be12784e00d226db5f8d44f92db6955f713c18a37b68.png?X-Amz-Content-Sha256=UNSIGNED-PAYLOAD&X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=FKAFOID6SUCPYPPKI6ZC%2F20241211%2Fus-west-1%2Fs3%2Faws4_request&X-Amz-Date=20241211T092616Z&X-Amz-SignedHeaders=host&X-Amz-Expires=3600&X-Amz-Signature=543db032f0d0f2556047e739d28abf092630e3f5d263ff284867cc59b1111d51";s:8:"original";s:513:"https://dev-aei.cellar-c2.services.clever-cloud.com/users/original/user-141-5fcd971ca789d0b05dc402bd661d780e58a3db11b1d6df64fa3165e95fcb367b262188b6d5d6f35121a4be12784e00d226db5f8d44f92db6955f713c18a37b68.png?X-Amz-Content-Sha256=UNSIGNED-PAYLOAD&X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=FKAFOID6SUCPYPPKI6ZC%2F20241211%2Fus-west-1%2Fs3%2Faws4_request&X-Amz-Date=20241211T092616Z&X-Amz-SignedHeaders=host&X-Amz-Expires=3600&X-Amz-Signature=09c0977c3434397b79771e839ff9aa5997fff2457b660948f43e47dbdca37ef1";s:6:"medium";s:511:"https://dev-aei.cellar-c2.services.clever-cloud.com/users/medium/user-141-5fcd971ca789d0b05dc402bd661d780e58a3db11b1d6df64fa3165e95fcb367b262188b6d5d6f35121a4be12784e00d226db5f8d44f92db6955f713c18a37b68.png?X-Amz-Content-Sha256=UNSIGNED-PAYLOAD&X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=FKAFOID6SUCPYPPKI6ZC%2F20241211%2Fus-west-1%2Fs3%2Faws4_request&X-Amz-Date=20241211T092616Z&X-Amz-SignedHeaders=host&X-Amz-Expires=3600&X-Amz-Signature=6c5f8308379ed969cbbeba8ec5f054a9e5169d088efa2c3326abb691169f1f88";}s:29:"\000App\\Entity\\Picture\000createdAt";O:17:"DateTimeImmutable":3:{s:4:"date";s:26:"2024-10-22 18:07:31.000000";s:13:"timezone_type";i:3;s:8:"timezone";s:3:"UTC";}s:23:"\000App\\Entity\\Picture\000tag";E:28:"App\\Entity\\PictureTag:Avatar";s:32:"\000App\\Entity\\Picture\000intervention";N;}s:25:"\000App\\Entity\\User\000employer";O:34:"Proxies\\__CG__\\App\\Entity\\Employer":7:{s:23:"\000App\\Entity\\Employer\000id";i:54;s:26:"\000App\\Entity\\Employer\000siren";s:9:"130026032";s:25:"\000App\\Entity\\Employer\000name";s:4:"ANCT";s:30:"\000App\\Entity\\Employer\000longitude";d:3.223203;s:29:"\000App\\Entity\\Employer\000latitude";d:44.324594;s:26:"\000App\\Entity\\Employer\000users";O:33:"Doctrine\\ORM\\PersistentCollection":2:{s:13:"\000*\000collection";O:43:"Doctrine\\Common\\Collections\\ArrayCollection":1:{s:53:"\000Doctrine\\Common\\Collections\\ArrayCollection\000elements";a:6:{i:0;r:5;i:1;O:15:"App\\Entity\\User":14:{s:19:"\000App\\Entity\\User\000id";i:184;s:22:"\000App\\Entity\\User\000login";s:40:"sylvain.legleau.prestataire@anct.gouv.fr";s:25:"\000App\\Entity\\User\000password";s:1:"-";s:26:"\000App\\Entity\\User\000firstname";s:7:"Sylvain";s:25:"\000App\\Entity\\User\000lastname";s:8:"LE GLEAU";s:22:"\000App\\Entity\\User\000email";s:40:"sylvain.legleau.prestataire@anct.gouv.fr";s:28:"\000App\\Entity\\User\000phoneNumber";N;s:26:"\000App\\Entity\\User\000createdAt";O:17:"DateTimeImmutable":3:{s:4:"date";s:26:"2024-12-02 12:18:50.000000";s:13:"timezone_type";i:3;s:8:"timezone";s:3:"UTC";}s:24:"\000App\\Entity\\User\000picture";N;s:25:"\000App\\Entity\\User\000employer";r:31;s:23:"\000App\\Entity\\User\000active";b:1;s:28:"\000App\\Entity\\User\000connectedAt";O:17:"DateTimeImmutable":3:{s:4:"date";s:26:"2024-12-09 11:38:34.000000";s:13:"timezone_type";i:3;s:8:"timezone";s:3:"UTC";}s:22:"\000App\\Entity\\User\000roles";a:1:{i:0;s:10:"ROLE_ADMIN";}s:32:"\000App\\Entity\\User\000proConnectToken";s:43:"XMSR-bG3gjnSQWodivX8TktGncGKI4QznJAkeklYwws";}i:2;O:15:"App\\Entity\\User":14:{s:19:"\000App\\Entity\\User\000id";i:172;s:22:"\000App\\Entity\\User\000login";s:16:"Laetitia Lefloch";s:25:"\000App\\Entity\\User\000password";s:60:"$2y$13$CTZo5Jy7vO1TxNvUctj7b.Ud8475tCfQ5BqvrPmJuMUDXtkIuNRmS";s:26:"\000App\\Entity\\User\000firstname";s:9:"La\303\253titia";s:25:"\000App\\Entity\\User\000lastname";s:9:"Le Floc'h";s:22:"\000App\\Entity\\User\000email";s:0:"";s:28:"\000App\\Entity\\User\000phoneNumber";s:0:"";s:26:"\000App\\Entity\\User\000createdAt";O:17:"DateTimeImmutable":3:{s:4:"date";s:26:"2024-11-25 18:33:21.000000";s:13:"timezone_type";i:3;s:8:"timezone";s:3:"UTC";}s:24:"\000App\\Entity\\User\000picture";N;s:25:"\000App\\Entity\\User\000employer";r:31;s:23:"\000App\\Entity\\User\000active";b:1;s:28:"\000App\\Entity\\User\000connectedAt";N;s:22:"\000App\\Entity\\User\000roles";a:1:{i:0;s:10:"ROLE_ADMIN";}s:32:"\000App\\Entity\\User\000proConnectToken";N;}i:3;O:15:"App\\Entity\\User":14:{s:19:"\000App\\Entity\\User\000id";i:167;s:22:"\000App\\Entity\\User\000login";s:5:"boumm";s:25:"\000App\\Entity\\User\000password";s:60:"$2y$13$.b1I865hXk8MKlzXDC0mHemc27X7D7U4lKG8buOtiZT7YxxveG6FK";s:26:"\000App\\Entity\\User\000firstname";s:3:"Bam";s:25:"\000App\\Entity\\User\000lastname";s:3:"Bim";s:22:"\000App\\Entity\\User\000email";s:0:"";s:28:"\000App\\Entity\\User\000phoneNumber";s:0:"";s:26:"\000App\\Entity\\User\000createdAt";O:17:"DateTimeImmutable":3:{s:4:"date";s:26:"2024-09-20 07:49:40.000000";s:13:"timezone_type";i:3;s:8:"timezone";s:3:"UTC";}s:24:"\000App\\Entity\\User\000picture";N;s:25:"\000App\\Entity\\User\000employer";r:31;s:23:"\000App\\Entity\\User\000active";b:1;s:28:"\000App\\Entity\\User\000connectedAt";N;s:22:"\000App\\Entity\\User\000roles";a:1:{i:0;s:10:"ROLE_ADMIN";}s:32:"\000App\\Entity\\User\000proConnectToken";N;}i:4;O:15:"App\\Entity\\User":14:{s:19:"\000App\\Entity\\User\000id";i:154;s:22:"\000App\\Entity\\User\000login";s:9:"jean.mich";s:25:"\000App\\Entity\\User\000password";s:60:"$2y$13$GJaNSgaF./2b7mblK/VNkez2TJbjK0jbdPlYvRyQtrr2TZD7h6BQC";s:26:"\000App\\Entity\\User\000firstname";s:4:"Much";s:25:"\000App\\Entity\\User\000lastname";s:9:"Jean Mich";s:22:"\000App\\Entity\\User\000email";s:0:"";s:28:"\000App\\Entity\\User\000phoneNumber";s:0:"";s:26:"\000App\\Entity\\User\000createdAt";O:17:"DateTimeImmutable":3:{s:4:"date";s:26:"2024-08-22 14:35:05.000000";s:13:"timezone_type";i:3;s:8:"timezone";s:3:"UTC";}s:24:"\000App\\Entity\\User\000picture";N;s:25:"\000App\\Entity\\User\000employer";r:31;s:23:"\000App\\Entity\\User\000active";b:1;s:28:"\000App\\Entity\\User\000connectedAt";N;s:22:"\000App\\Entity\\User\000roles";a:1:{i:0;s:10:"ROLE_ADMIN";}s:32:"\000App\\Entity\\User\000proConnectToken";N;}i:5;O:15:"App\\Entity\\User":14:{s:19:"\000App\\Entity\\User\000id";i:131;s:22:"\000App\\Entity\\User\000login";s:14:"administrateur";s:25:"\000App\\Entity\\User\000password";s:60:"$2y$13$fVKvFoewSS3OfvEx6lgEsugNFiCldon2lOl6SKpo9AaVGaFfqsD16";s:26:"\000App\\Entity\\User\000firstname";s:4:"Nyah";s:25:"\000App\\Entity\\User\000lastname";s:8:"Thompson";s:22:"\000App\\Entity\\User\000email";s:0:"";s:28:"\000App\\Entity\\User\000phoneNumber";s:12:"+12813156861";s:26:"\000App\\Entity\\User\000createdAt";O:17:"DateTimeImmutable":3:{s:4:"date";s:26:"2024-02-19 17:46:40.000000";s:13:"timezone_type";i:3;s:8:"timezone";s:3:"UTC";}s:24:"\000App\\Entity\\User\000picture";N;s:25:"\000App\\Entity\\User\000employer";r:31;s:23:"\000App\\Entity\\User\000active";b:1;s:28:"\000App\\Entity\\User\000connectedAt";N;s:22:"\000App\\Entity\\User\000roles";a:1:{i:0;s:10:"ROLE_ADMIN";}s:32:"\000App\\Entity\\User\000proConnectToken";N;}}}s:14:"\000*\000initialized";b:1;}s:29:"\000App\\Entity\\Employer\000timezone";s:12:"Europe/Paris";}s:23:"\000App\\Entity\\User\000active";b:1;s:28:"\000App\\Entity\\User\000connectedAt";O:17:"DateTimeImmutable":3:{s:4:"date";s:26:"2024-04-12 10:04:17.000000";s:13:"timezone_type";i:3;s:8:"timezone";s:3:"UTC";}s:22:"\000App\\Entity\\User\000roles";a:2:{i:0;s:10:"ROLE_ADMIN";i:1;s:16:"ROLE_SUPER_ADMIN";}s:32:"\000App\\Entity\\User\000proConnectToken";N;}i:1;b:1;i:2;N;i:3;a:0:{}i:4;a:2:{i:0;s:10:"ROLE_ADMIN";i:1;s:16:"ROLE_SUPER_ADMIN";}}}";
  s:20:"_csrf/https-delete61";
  s:43:"OOQBFYA7dhkRf8zYI75Zx2Tj0YjrWRUO0t8on2RsvsE";
  s:20:"_csrf/https-delete74";
  s:43:"txJH6TqJEK6fg7jzfXah_XscY0RMLXTBH2fXdDG43cI";
}
_sf2_meta|
a:3:{
  s:1:"u";
  i:1733909176;
  s:1:"c";
  i:1733848912;
  s:1:"l";
  i:0;
}
 */

/*
O:74:"Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken":3:{i:0;N;i:1;s:4:"main";i:2;a:5:{i:0;O:15:"App\Entity\User":13:{s:19:" App\Entity\User id";i:141;s:22:" App\Entity\User login";s:3:"slg";s:25:" App\Entity\User password";s:60:"$2y$13$E6VvyIGxH3d7ULe0JshleObeozPnTefoSLRKJFA63ZumcOO90jDci";s:26:" App\Entity\User firstname";s:7:"Sylvain";s:25:" App\Entity\User lastname";s:8:"LE GLEAU";s:22:" App\Entity\User email";s:29:"sylvain.le.gleau@beta.gouv.fr";s:28:" App\Entity\User phoneNumber";N;s:26:" App\Entity\User createdAt";O:17:"DateTimeImmutable":3:{s:4:"date";s:26:"2024-04-12 10:04:09.000000";s:13:"timezone_type";i:3;s:8:"timezone";s:3:"UTC";}s:24:" App\Entity\User picture";O:18:"App\Entity\Picture":6:{s:22:" App\Entity\Picture id";i:72;s:28:" App\Entity\Picture fileName";s:18:"users/user-141.png";s:23:" App\Entity\Picture url";s:375:"https://dev-aei.cellar-c2.services.clever-cloud.com/users/user-141.png?X-Amz-Content-Sha256=UNSIGNED-PAYLOAD&X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=FKAFOID6SUCPYPPKI6ZC%2F20241112%2Fus-west-1%2Fs3%2Faws4_request&X-Amz-Date=20241112T111642Z&X-Amz-SignedHeaders=host&X-Amz-Expires=3600&X-Amz-Signature=ec3115a829eed2b2640f643ed06c6b2b4f233bf76c3e45dd9d8bc74878dd1adf";s:29:" App\Entity\Picture createdAt";O:17:"DateTimeImmutable":3:{s:4:"date";s:26:"2024-10-22 18:07:31.000000";s:13:"timezone_type";i:3;s:8:"timezone";s:3:"UTC";}s:23:" App\Entity\Picture tag";N;s:32:" App\Entity\Picture intervention";N;}s:25:" App\Entity\User employer";O:34:"Proxies\__CG__\App\Entity\Employer":7:{s:23:" App\Entity\Employer id";i:54;s:26:" App\Entity\Employer siren";s:9:"098765456";s:25:" App\Entity\Employer name";s:4:"ANCT";s:30:" App\Entity\Employer longitude";d:3.223203;s:29:" App\Entity\Employer latitude";d:44.324594;s:26:" App\Entity\Employer users";O:33:"Doctrine\ORM\PersistentCollection":2:{s:13:" * collection";O:43:"Doctrine\Common\Collections\ArrayCollection":1:{s:53:" Doctrine\Common\Collections\ArrayCollection elements";a:0:{}}s:14:" * initialized";b:0;}s:29:" App\Entity\Employer timezone";s:12:"Europe/Paris";}s:23:" App\Entity\User active";b:1;s:28:" App\Entity\User connectedAt";O:17:"DateTimeImmutable":3:{s:4:"date";s:26:"2024-04-12 10:04:17.000000";s:13:"timezone_type";i:3;s:8:"timezone";s:3:"UTC";}s:22:" App\Entity\User roles";a:2:{i:0;s:10:"ROLE_ADMIN";i:1;s:16:"ROLE_SUPER_ADMIN";}}i:1;b:1;i:2;N;i:3;a:0:{}i:4;a:2:{i:0;s:10:"ROLE_ADMIN";i:1;s:16:"ROLE_SUPER_ADMIN";}}}
 */
