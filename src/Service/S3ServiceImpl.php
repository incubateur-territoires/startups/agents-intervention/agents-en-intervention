<?php

declare(strict_types=1);

namespace App\Service;

use Aws\S3\S3Client;
use Psr\Http\Message\StreamInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Une classe pour accéder au S3.
 */
class S3ServiceImpl implements S3Service
{
    /**
     * @var S3Client le client S3.
     */
    protected S3Client $s3Client;

    /**
     * Le constructeur.
     * @param string $endPoint l'URL du point d'accès.
     * @param string $accessKey la clé d'accès.
     * @param string $secretKey la clé secrète.
     * @param string $region la région.
     * @param string $bucket le seau.
     */
    public function __construct(string $endPoint, string $accessKey, string $secretKey, string $region, protected string $bucket)
    {
        $this->s3Client = new S3Client([
            'version' => 'latest',
            'endpoint' => $endPoint,
            'credentials' => [
                'key' => $accessKey,
                'secret' => $secretKey,
            ],
            'region' => $region,
        ]);
    }

    /**
     * Renvoie true si le fichier existe dans le seau.
     * @param string $key la clé (le nom) du fichier.
     * @return bool si le fichier existe dans le seau.
     */
    #[\Override]
    public function fileExist(string $key): bool
    {
        return $this->s3Client->doesObjectExistV2($this->bucket, $key);
    }

    /**
     * Supprime un fichier.
     * @param string $key la clé.
     */
    #[\Override]
    public function deleteFile(string $key): void
    {
        $this->s3Client->deleteObject([
            'Bucket' => $this->bucket,
            'Key' => $key,
        ]);
    }

    /**
     * Renvoie l'URL signée.
     * @param string $key la clé.
     * @return string l'URL signée.
     */
    #[\Override]
    public function getSignedURL(string $key): string
    {
        $getObjectCommand = $this->s3Client->getCommand('GetObject', [
            'Bucket' => $this->bucket,
            'Key' => $key,
        ]);

        $request = $this->s3Client->createPresignedRequest($getObjectCommand, '+1 hour');

        return (string) $request->getUri();
    }

    /**
     * Ajoute un fichier.
     * @param UploadedFile $file le fichier.
     * @param string $key la clé.
     */
    #[\Override]
    public function putFile(UploadedFile $file, string $key): void
    {
        $this->putFileContent($file->getContent(), $key);
    }


  /**
   * Ajoute un fichier depuis son contenu.
   * @param mixed $content le fichier.
   * @param string $key la clé.
   */
  #[\Override]
  public function putFileContent(mixed $content, string $key): void
  {
    $this->s3Client->putObject([
      'Bucket' => $this->bucket,
      'Body' => $content,
      'Key' => $key,
    ]);
  }

  public function getFile(string $key): mixed
  {
    $result = $this->s3Client->getObject([
      'Bucket' => $this->bucket,
      'Key' => $key,
    ]);

    /** @var StreamInterface $body */
    $body = $result->get('Body');
    $body->rewind();

    return $body->getContents();
  }
}
