<?php

namespace App\Service\Admin;

use App\Entity\Employer;
use App\Repository\EmployerRepository;
use App\Repository\InterventionSequencesRepository;
use App\Repository\RecurringInterventionSequencesRepository;

readonly class SequenceChecker
{

  public function __construct(
    private EmployerRepository                       $employerRepository,
    private InterventionSequencesRepository          $interventionSeqRepository,
    private RecurringInterventionSequencesRepository $recurringInterventionSeqRepository
  ) {
  }

  /**
   * @return array{'employer': Employer, 'sequenceType': string, 'missing': bool}[]
   */
  public function findMissingSequences(): array
  {
    /** @var Employer[] $employers */
    $employers = $this->employerRepository->findAll();

    $missingSequences = [];

    foreach ($employers as $employer) {
      // Vérification pour les interventions simples
      $interventionSeq = $this->interventionSeqRepository->findOneBy(['employer' => $employer->getId()]);
      if (!$interventionSeq) {
        $missingSequences[] = [
          'employer' => $employer,
          'sequenceType' => 'Intervention',
          'missing' => true,
        ];
      }

      // Vérification pour les interventions récurrentes
      $recurringInterventionSeq = $this->recurringInterventionSeqRepository->findOneBy(['employer' => $employer->getId()]);
      if (!$recurringInterventionSeq) {
        $missingSequences[] = [
          'employer' => $employer,
          'sequenceType' => 'Intervention récurrente',
          'missing' => true,
        ];
      }
    }

    return $missingSequences;
  }
}
