<?php

namespace App\Service;

/**
 * TimezoneService regroupe les opérations autour des fuseaux horaires.
 *
 * @package App\Service
 */
interface TimezoneService
{
  /**
   * Récupérer le fuseau horaire à partir des coordonnées géographiques GPS WGS84.
   *
   * @param float $latitude
   * @param float $longitude
   * @return string
   */
  public function getTimezoneFromCoordinates(float $latitude, float $longitude): string;
}
