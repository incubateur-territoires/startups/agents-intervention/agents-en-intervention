<?php

namespace App\Service;

use App\Entity\ActionLog;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\SecurityBundle\Security;

readonly class AuditTrailLogger
{
  private ?int $currentUser;

  public function __construct(
    private EntityManagerInterface $entityManager,
    private EventProcessor         $eventProcessor,
    private LoggerInterface        $logger,
    public Security                $security
  )
  {
    /** @var User|null $user */
    $user = $security->getUser();
    $this->currentUser = $user?->getId();
  }

  /**
   * @param string $type
   * @param string $entityClass
   * @param int|null $entityId
   * @param array<string, mixed> $changes
   * @param bool $flush
   * @return ActionLog
   */
  public function log(
    string $type,
    string $entityClass,
    ?int   $entityId,
    array  $changes,
    bool   $flush = false
  ): ActionLog
  {
    // Valider les données de l'événement
    $result = $this->eventProcessor->process($type, $changes);

    $this->logger->debug('Event validation result', $result);

    if (!$result['valid']) {
      // Tracer l'erreur dans les logs
      $this->logger->error('Invalid event data: ' . json_encode($result['errors']));
    }

    // Les événements validés sont automatiquement enregistrés dans l’audit trail
    $log = ActionLog::new(
      type: $type,
      entityClass: str_replace('Proxies\__CG__\\', '', $entityClass),
      entityId: $entityId,
      changes: json_encode($result) ?: null,
      userIdentifier: $this->currentUser
    );

    $this->entityManager->persist($log);
    if ($flush) {
      $this->entityManager->flush();
    }
    return $log;
  }
}
