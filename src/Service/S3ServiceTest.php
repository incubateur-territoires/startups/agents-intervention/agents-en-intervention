<?php

declare(strict_types=1);

namespace App\Service;

use Aws\S3\S3Client;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Une classe pour accéder au S3.
 */
class S3ServiceTest implements S3Service
{

    /**
     * Renvoie true si le fichier existe dans le seau.
     * @param string $key la clé (le nom) du fichier.
     * @return bool si le fichier existe dans le seau.
     */
    #[\Override]
    public function fileExist(string $key): bool
    {
        return false;
    }

    /**
     * Supprime un fichier.
     * @param string $key la clé.
     */
    #[\Override]
    public function deleteFile(string $key): void
    {

    }

    /**
     * Renvoie l'URL signée.
     * @param string $key la clé.
     * @return string l'URL signée.
     */
    #[\Override]
    public function getSignedURL(string $key): string
    {
        return 'uri';
    }

    /**
     * Ajoute un fichier.
     * @param UploadedFile $file le fichier.
     * @param string $key la clé.
     */
    #[\Override]
    public function putFile(UploadedFile $file, string $key): void
    {

    }

  public function getFile(string $key): mixed
  {
    return 'file';
  }

  public function putFileContent(mixed $content, string $key): void
  {
    // Testing do nothing
  }
}
