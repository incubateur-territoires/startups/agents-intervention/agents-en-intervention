<?php

namespace App\Service;

use App\Entity\Intervention;
use DateMalformedStringException;
use DateTimeImmutable;

/**
 * Permet de faire des opérations sur des interventions :
 * - transformations de récurrence en ponctuel et inversement
 * - régénération des interventions générées en fonction des données de récurrence
 * - gestion des interventions générées suite à une suppression d'intervention récurrente
 */
interface RecurrenceService
{
  /**
   * Supprime les futures interventions générées par cette récurrence
   *
   * @param Intervention $intervention
   * @param bool $removeAllReferences
   * @param DateTimeImmutable $now testing only
   * @return void
   * @throws DateMalformedStringException
   */
  public function removeGeneratedInterventionsFor(
    Intervention $intervention,
    bool $removeAllReferences = true,
    DateTimeImmutable $now = new DateTimeImmutable('now', new \DateTimeZone('UTC'))
  ): void;

  /**
   * Génère les interventions pour cette récurrence
   *
   * @param Intervention $intervention
   * @return void
   */
  public function addGeneratedInterventionsFor(Intervention $intervention): void;

  /**
   * Modifie les futures interventions générées par cette récurrence
   *
   * @param Intervention $intervention
   * @return void
   */
  public function modifyGeneratedInterventionsFor(Intervention $intervention): void;
}
