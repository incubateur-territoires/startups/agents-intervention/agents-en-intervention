<?php
// src/Command/GenerateTypescriptCommand.php
namespace App\Command;

use Symfony\Component\Serializer\Annotation\Groups;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\ClassMetadata;
use ReflectionClass;
use ReflectionEnum;
use ReflectionMethod;
use ReflectionNamedType;
use ReflectionUnionType;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactoryInterface;

#[AsCommand(
  name: 'generate:typescript',
  description: 'Génère des types Typescript pour les entités Symfony',
)]
class GenerateTypescriptCommand extends Command
{
  public function __construct(
    private readonly EntityManagerInterface        $entityManager,
    private readonly ClassMetadataFactoryInterface $classMetadataFactory
  )
  {
    parent::__construct();
  }

  #[\Override]
  protected function configure()
  {
    $this
      ->addArgument('output-dir', InputArgument::REQUIRED, 'Répertoire de sortie des fichiers TypeScript');
  }

  #[\Override]
  protected function execute(InputInterface $input, OutputInterface $output): int
  {
    $outputDir = $input->getArgument('output-dir');

    // Supprimer le dossier de destination s'il existe, puis le recréer
    $this->resetOutputDirectory($outputDir, $output);

    $metaDataFactory = $this->entityManager->getMetadataFactory();
    $allMetadata = $metaDataFactory->getAllMetadata();

    // Collecte des Enums à générer
    $enums = [];

    foreach ($allMetadata as $meta) {
      $className = $meta->getName();
      $output->writeln("Génération des types pour : $className");

      // Générer le fichier TypeScript complet pour l'entité
      $this->generateCompleteTypeForEntity($className, $meta, $outputDir, $enums);

      // Générer les types TypeScript pour chaque groupe
      $this->generateTypesForEntityGroups($className, $meta, $outputDir, $enums);
    }

    // Récupération des dto
    $allDto = $this->getClassesInNamespace('App\\Dto');
    foreach ($allDto as $className) {
      $output->writeln("Génération des types pour : $className");

      // Générer le fichier TypeScript complet pour l'entité
      $this->generateCompleteTypeForEntity($className, null, $outputDir, $enums);

      // Générer les types TypeScript pour chaque groupe
      $this->generateTypesForEntityGroups($className, null, $outputDir, $enums);
    }

    // Récupérer toutes les classes et Enums du namespace App\Entity
    $allClasses = $this->getClassesInNamespace('App\\Entity');

    // Générer les Enums TypeScript manquantes
    foreach ($allClasses as $class) {
      if (enum_exists($class) && !isset($enums[$class])) {
        $enums[$class] = $class;
      }
    }

    // Générer les Enums TypeScript
    foreach ($enums as $enumClass) {
      $output->writeln("Génération des types pour l'enum : $enumClass");
      $this->generateEnum($enumClass, $outputDir);
    }

    // Générer le fichier index.ts
    $this->generateIndexFile($outputDir, $output);

    return Command::SUCCESS;
  }

  private function resetOutputDirectory(string $dir, OutputInterface $output)
  {
    $filesystem = new Filesystem();

    try {
      if ($filesystem->exists($dir)) {
        $filesystem->remove($dir);
        $output->writeln("Le dossier '$dir' a été supprimé.");
      }

      // Recréer le dossier vide
      $filesystem->mkdir($dir);
      $output->writeln("Le dossier '$dir' a été recréé.");
    } catch (IOExceptionInterface $exception) {
      $output->writeln("Erreur lors de la manipulation du dossier : " . $exception->getMessage());
    }
  }

  private function generateCompleteTypeForEntity(
    string         $className,
    ?ClassMetadata $meta,
    string         $outputDir,
    array          &$enums
  )
  {
    $reflectionClass = new ReflectionClass($className);
    $properties = $reflectionClass->getProperties();
    $methods = $reflectionClass->getMethods(ReflectionMethod::IS_PUBLIC); // Récupérer toutes les méthodes publiques

    $entityName = $reflectionClass->getShortName();

    $tsType = "export type $entityName = {\n";

    $imports = []; // Collecter les entités à importer

    // Ajouter les propriétés standard
    foreach ($properties as $property) {
      $propertyType = $this->resolveType($meta, $property, $imports, $entityName, $enums);
      $tsType .= "  $propertyType;\n";
    }

    // Ajouter les méthodes annotées avec #[Groups]
    foreach ($methods as $method) {
      // Vérifier si la méthode est annotée avec #[Groups]
      $groups = $method->getAttributes(Groups::class);
      if (!empty($groups)) {
        $methodName = $method->getName();
        $methodReturnType = $this->getTypeFromReflectionType($method->getReturnType(), $enums, $imports, $entityName);
        $isNullable = $this->isNullable($method->getReturnType());

        // Convertir le nom de la méthode en nom de propriété pour TypeScript
        $propertyName = $this->methodNameToPropertyName($methodName);

        $tsType .= "  $propertyName: $methodReturnType" . ($isNullable ? ' | null' : '') . ";\n";
      }
    }

    $tsType .= "};\n";

    // Supprimer les doublons dans les imports et éviter l'auto-import de l'entité elle-même
    $uniqueImports = array_unique($imports);
    $uniqueImports = array_filter($uniqueImports, fn($import) => $import !== $entityName);

    // Ajouter les imports au début du fichier
    $importStatements = "";
    foreach ($uniqueImports as $import) {
      $importStatements .= "import { $import } from './$import';\n";
    }

    file_put_contents("$outputDir/$entityName.ts", $importStatements . "\n" . $tsType);
  }

  private function generateTypesForEntityGroups(
    string         $className,
    ?ClassMetadata $meta,
    string         $outputDir,
    array          &$enums
  )
  {
    $reflectionClass = new ReflectionClass($className);
    $entityName = $reflectionClass->getShortName();

    // Récupérer les groupes pour chaque attribut (propriété ou méthode)
    $classMetadata = $this->classMetadataFactory->getMetadataFor($className);
    $groupsMap = [];

    foreach ($classMetadata->getAttributesMetadata() as $attributeMetadata) {
      $attributeName = $attributeMetadata->getName();
      $groups = $attributeMetadata->getGroups();

      foreach ($groups as $group) {
        $normalizedGroup = $this->normalizeGroupName($group);
        if (!isset($groupsMap[$normalizedGroup])) {
          $groupsMap[$normalizedGroup] = [];
        }
        $groupsMap[$normalizedGroup][] = $attributeName;
      }
    }

    // Générer les types TypeScript pour chaque groupe avec Pick
    foreach ($groupsMap as $group => $groupAttributes) {
      $typeName = $entityName . ucfirst($group);

      // Générer un type Pick<Type, 'property1' | 'property2' | ...>
      $propertiesList = implode(' | ', array_map(fn($prop) => "'$prop'", $groupAttributes));
      $tsType = "export type $typeName = Pick<$entityName, $propertiesList>;\n";

      // Ajouter l'import du type de base avec un retour à la ligne supplémentaire après l'import
      $importStatement = "import { $entityName } from './$entityName';\n\n";

      // Sauvegarder le fichier TypeScript
      file_put_contents("$outputDir/{$typeName}.ts", $importStatement . $tsType);
    }
  }

  protected function generateIndexFile(string $outputDir, OutputInterface $output): void
  {
    // Récupérer tous les fichiers TypeScript dans le répertoire
    $files = scandir($outputDir);
    $types = [];

    foreach ($files as $file) {
      // Ignorer les fichiers non TypeScript
      if (pathinfo($file, PATHINFO_EXTENSION) !== 'ts' || $file === 'index.ts') {
        continue;
      }

      // Récupérer le nom du fichier sans l'extension
      $typeName = pathinfo($file, PATHINFO_FILENAME);

      // Vérifier s'il y a des conflits de noms
      if (isset($types[$typeName])) {
        $output->writeln("<error>Conflit détecté pour le type '$typeName'. Annulation de la génération de index.ts.</error>");
        return;
      }

      // Ajouter à la liste des types à exporter
      $types[$typeName] = $file;
    }

    // Générer le fichier index.ts avec les exports
    $indexContent = '';
    foreach ($types as $typeName => $file) {
      $indexContent .= "export * from './$typeName';\n";
    }

    // Écrire le fichier index.ts
    file_put_contents("$outputDir/index.ts", $indexContent);

    $output->writeln("Fichier index.ts généré avec succès.");
  }

  private function resolveType(
    ?ClassMetadata      $meta,
    \ReflectionProperty $property,
    array               &$imports,
    string              $currentEntityName,
    array               &$enums
  ): string
  {
    $propertyName = $property->getName();
    $isNullable = false;
    $isOptional = false;
    $tsType = 'any';

    // Si la propriété est mappée dans Doctrine
    if (isset($meta) && $meta->hasField($propertyName)) {
      $fieldMapping = $meta->getFieldMapping($propertyName);
      $isNullable = $fieldMapping['nullable'] ?? false;

      // Vérifier si le champ utilise un enum (comme PictureTag)
      if (isset($fieldMapping['enumType'])) {
        $enumClass = $fieldMapping['enumType'];
        if (enum_exists($enumClass)) {
          // Récupérer le nom de l'enum et ajouter à la liste des imports
          $enumName = $this->getEntityShortName($enumClass);
          if ($enumName !== $currentEntityName) {
            $imports[] = $enumName;
          }
          $enums[$enumClass] = $enumClass; // Collecter l'enum pour la génération
          $tsType = $enumName; // Utiliser le nom de l'enum comme type
        }
      } else {
        // Si ce n'est pas une enum, mapper le type Doctrine à un type TypeScript
        $tsType = $this->convertDoctrineTypeToTsType($fieldMapping['type']);
      }
    } elseif (isset($meta) && $meta->hasAssociation($propertyName)) {
      // Gérer les associations
      $associationMapping = $meta->getAssociationMapping($propertyName);
      $targetEntity = $this->getEntityShortName($associationMapping['targetEntity']);

      if ($targetEntity !== $currentEntityName) {
        $imports[] = $targetEntity;
      }

      if ($associationMapping['type'] & ClassMetadata::TO_ONE) {
        $tsType = $targetEntity;
      } else {
        $tsType = $targetEntity . '[]';
      }
    } else {
      // Gestion des propriétés typées avec un type PHP
      $propertyType = $property->getType();
      if ($propertyType) {
        $tsType = $this->getTypeFromReflectionType($propertyType, $enums, $imports, $currentEntityName);
        $isNullable = $this->isNullable($propertyType);
        if ($isNullable) {
          $isOptional = true; // Si c'est nullable, marquer comme optionnel
        }
      }
    }

    // Gérer correctement la syntaxe pour les propriétés optionnelles
    if ($isOptional) {
      return "$propertyName?: $tsType | null";
    }

    // Sinon, générer la propriété normalement
    return "$propertyName: $tsType" . ($isNullable ? ' | null' : '');
  }

  private function getTypeFromReflectionType(
    ?\ReflectionType $phpType,
    array            &$enums = [],
    array            &$imports = [],
    string           $currentEntityName = ''
  ): string
  {
    if (!$phpType) {
      return 'any';
    }

    if ($phpType instanceof ReflectionNamedType) {
      return $this->convertNamedType($phpType, $enums, $imports, $currentEntityName);
    } elseif ($phpType instanceof ReflectionUnionType) {
      $types = [];
      foreach ($phpType->getTypes() as $type) {
        $types[] = $this->convertNamedType($type, $enums, $imports, $currentEntityName);
      }
      return implode(' | ', array_unique($types));
    } else {
      return 'any';
    }
  }

  private function convertNamedType(
    ReflectionNamedType $type,
    array               &$enums,
    array               &$imports,
    string              $currentEntityName
  ): string
  {
    $typeName = $type->getName();
    if ($type->isBuiltin()) {
      $typeMap = [
        'int' => 'number',
        'float' => 'number',
        'string' => 'string',
        'bool' => 'boolean',
        'array' => 'any[]',
        'mixed' => 'any',
        'void' => 'void',
        'object' => 'any',
        'null' => 'null',
        \DateTime::class => 'Date', // Mapping explicite pour DateTimeImmutable
        \DateTimeInterface::class => 'Date', // Mapping explicite pour DateTimeImmutable
        \DateTimeImmutable::class => 'Date', // Mapping explicite pour DateTimeImmutable
      ];

      return $typeMap[$typeName] ?? 'any';
    } else {
      // Gestion des Enums et des classes
      if (enum_exists($typeName)) {
        $enumName = $this->getEntityShortName($typeName);
        if ($enumName !== $currentEntityName) {
          $imports[] = $enumName;
        }
        $enums[$typeName] = $typeName; // Collecter l'Enum pour la génération
        return $enumName;
      } else {
        // Special types
        $typeMap = [
          \DateTime::class => 'Date', // Mapping explicite pour DateTimeImmutable
          \DateTimeInterface::class => 'Date', // Mapping explicite pour DateTimeImmutable
          \DateTimeImmutable::class => 'Date', // Mapping explicite pour DateTimeImmutable
        ];

        if (isset($typeMap[$typeName])) {
          return $typeMap[$typeName];
        }

        // Considérer comme une classe/entité
        $className = $this->getEntityShortName($typeName);
        if ($className !== $currentEntityName) {
          $imports[] = $className;
        }
        return $className;
      }
    }
  }

  private function convertDoctrineTypeToTsType(string $doctrineType): string
  {
    $typeMap = [
      'string' => 'string',
      'integer' => 'number',
      'boolean' => 'boolean',
      'float' => 'number',
      'datetime' => 'Date', // Mapping pour DateTime
      'datetime_immutable' => 'Date', // Mapping pour DateTimeImmutable
    ];

    return $typeMap[$doctrineType] ?? 'any';
  }

  private function isNullable(?\ReflectionType $phpType): bool
  {
    if (!$phpType) {
      return false;
    }

    if ($phpType instanceof ReflectionNamedType) {
      return $phpType->allowsNull();
    } elseif ($phpType instanceof ReflectionUnionType) {
      foreach ($phpType->getTypes() as $type) {
        if ($type->getName() === 'null') {
          return true;
        }
      }
    }

    return false;
  }

  private function generateEnum(string $enumClass, string $outputDir)
  {
    $reflectionEnum = new ReflectionEnum($enumClass);
    $enumName = $reflectionEnum->getShortName();

    $cases = $reflectionEnum->getCases();

    $tsEnum = "export enum $enumName {\n";
    foreach ($cases as $case) {
      $name = $case->getName();
      $value = $case->getBackingValue();

      if (is_string($value)) {
        $tsEnum .= "  $name = \"$value\",\n";
      } else {
        $tsEnum .= "  $name = $value,\n";
      }
    }
    $tsEnum .= "}\n";

    file_put_contents("$outputDir/$enumName.ts", $tsEnum);
  }

  private function methodNameToPropertyName(string $methodName): string
  {
    // Supprimer les préfixes 'get', 'set' ou 'is'
    if (str_starts_with($methodName, 'get')) {
      $name = substr($methodName, 3);
    } elseif (str_starts_with($methodName, 'set')) {
      $name = substr($methodName, 3);
    } elseif (str_starts_with($methodName, 'is')) {
      $name = substr($methodName, 2);
    } else {
      $name = $methodName;
    }

    // Convertir la première lettre en minuscule
    $name = lcfirst($name);

    return $name;
  }

  private function getEntityShortName(string $className): string
  {
    $parts = explode('\\', $className);
    return end($parts); // Récupère la dernière partie après le dernier '\'
  }

  private function normalizeGroupName(string $groupName): string
  {
    // Remplacer les espaces et tirets par des underscores
    $normalized = preg_replace('/[\s-]+/', '_', $groupName);

    // Supprimer les caractères non alphanumériques, sauf les underscores
    $normalized = preg_replace('/[^a-zA-Z0-9_]/', '', (string)$normalized);

    // S'assurer que le nom commence par une lettre ou un underscore
    if (!preg_match('/^[a-zA-Z_]/', (string)$normalized)) {
      $normalized = '_' . $normalized;
    }

    return ucfirst($normalized); // Mettre la première lettre en majuscule
  }

  //This value should be the directory that contains composer.json
  const appRoot = __DIR__ . "/../../";

  public function getClassesInNamespace($namespace)
  {
    $files = scandir($this->getNamespaceDirectory($namespace));

    $classes = array_map(fn($file) => $namespace . '\\' . str_replace('.php', '', $file), $files);

    return array_filter($classes, fn($possibleClass) => class_exists($possibleClass));
  }

  private function getDefinedNamespaces()
  {
    $composerJsonPath = self::appRoot . 'composer.json';
    $composerConfig = json_decode(file_get_contents($composerJsonPath));

    return (array)$composerConfig->autoload->{'psr-4'};
  }

  private function getNamespaceDirectory($namespace)
  {
    $composerNamespaces = $this->getDefinedNamespaces();

    $namespaceFragments = explode('\\', (string)$namespace);
    $undefinedNamespaceFragments = [];

    while ($namespaceFragments) {
      $possibleNamespace = implode('\\', $namespaceFragments) . '\\';

      if (array_key_exists($possibleNamespace, $composerNamespaces)) {
        return realpath(self::appRoot . $composerNamespaces[$possibleNamespace] . implode('/', $undefinedNamespaceFragments));
      }

      array_unshift($undefinedNamespaceFragments, array_pop($namespaceFragments));
    }

    return false;
  }

}
