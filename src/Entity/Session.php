<?php

namespace App\Entity;

use Doctrine\DBAL\Types\Types;
use App\Repository\SessionRepository;
use App\Service\PhpSessionUnserializer;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Cache\Marshaller\DefaultMarshaller;
use Symfony\Component\Cache\Marshaller\TagAwareMarshaller;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

#[ORM\Entity(repositoryClass: SessionRepository::class, readOnly: true)]
#[ORM\Table(name: 'sessions')]
#[ORM\Index(name: 'sess_lifetime_idx', columns: ['sess_lifetime'])]
class Session
{
  #[ORM\Id]
  #[ORM\Column(name: 'sess_id', type: Types::STRING, length: 128)]
  private string $id;

  #[ORM\Column(name: 'sess_data', type: Types::BINARY)]
  private mixed $data;

  #[ORM\Column(name: 'sess_lifetime', type: Types::INTEGER)]
  private int $lifetime;

  #[ORM\Column(name: 'sess_time', type: Types::INTEGER)]
  private int $time;

  private mixed $unserializedData = null;

  public function getId(): string
  {
    return $this->id;
  }

  public function getData(): mixed
  {
    return $this->data;
  }

  public function getDataString(): string
  {
    $data = $this->getData();
    if (is_resource($data)) {
      return stream_get_contents($data) ?: '';
    } elseif (is_scalar($data)) {
      return strval($data);
    }
    throw new \Exception('Session data is not stringable');
  }

  public function getLifetime(): \DateTime
  {
    return (new \DateTime())->setTimestamp($this->lifetime);
  }

  public function getTime(): \DateTime
  {
    return (new \DateTime())->setTimestamp($this->time);
  }

  public function getOngoing(): bool
  {
    return (new \DateTime()) < $this->getLifetime();
  }

  public function getUsername(): string
  {
    $un = $this->getUnserializedData();
    if (!isset($un['_sf2_attributes']['_security_main'])) {
      return 'anonymous';
    }

    /** @var UsernamePasswordToken $token */
    $token = $un['_sf2_attributes']['_security_main'];
    return $token->getUser()?->getUserIdentifier();
  }

  public function getUnserializedData(): mixed
  {
    if (!isset($this->unserializedData)) {
      $this->unserializedData = self::unserialize($this->getDataString());
    }

    return $this->unserializedData;
  }

  public function getUnserializedDataJson(): string
  {
    return json_encode($this->getUnserializedData(), JSON_PRETTY_PRINT) ?: '';
  }

  public static function unserialize(string $session_data): array
  {
    return (new PhpSessionUnserializer())->unserialize($session_data);
  }
}
