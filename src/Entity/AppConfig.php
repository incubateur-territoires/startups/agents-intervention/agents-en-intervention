<?php

// src/Entity/AppConfig.php
namespace App\Entity;

use App\Repository\AppConfigRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: AppConfigRepository::class)]
#[ORM\Table(name: "app_config")]
class AppConfig
{
  #[ORM\Id]
  #[ORM\Column(type: Types::STRING, unique: true)]
  private string $keyName;

  #[ORM\Column(type: Types::TEXT)]
  private string $value;

  #[ORM\Column(type: Types::STRING, length: 50)]
  private string $type;

  public function getKeyName(): string
  {
    return $this->keyName;
  }

  public function setKeyName(string $keyName): self
  {
    $this->keyName = $keyName;
    return $this;
  }

  public function getValue(): string
  {
    return $this->value;
  }

  public function setValue(string $value): self
  {
    $this->value = $value;
    return $this;
  }

  public function getType(): string
  {
    return $this->type;
  }

  public function setType(string $type): self
  {
    $this->type = $type;
    return $this;
  }

  /**
   * Retourne la valeur avec son type approprié.
   */
  public function getTypedValue(): mixed
  {
    return match ($this->type) {
      'int' => intval($this->value),
      'float' => floatval($this->value),
      'bool' => filter_var($this->value, FILTER_VALIDATE_BOOLEAN),
      'array' => json_decode($this->value, true, 512, JSON_THROW_ON_ERROR),
      default => $this->value, // Par défaut, retourne une chaîne
    };
  }

  /**
   * Définit la valeur et son type.
   */
  public function setTypedValue(mixed $value): self
  {
    if (is_array($value)) {
      $this->type = 'array';
      $this->value = json_encode($value, JSON_THROW_ON_ERROR);
    } elseif (is_int($value)) {
      $this->type = 'int';
      $this->value = (string)$value;
    } elseif (is_float($value)) {
      $this->type = 'float';
      $this->value = (string)$value;
    } elseif (is_bool($value)) {
      $this->type = 'bool';
      $this->value = $value ? 'true' : 'false';
    } elseif ($value === null || is_scalar($value) || (is_object($value) && method_exists($value, '__toString'))) {
      $this->type = 'string';
      $this->value = (string)$value;
    } else {
      // Valeur non prise en charge
      $this->type = 'string';
      $this->value = '';
    }

    return $this;
  }

}
