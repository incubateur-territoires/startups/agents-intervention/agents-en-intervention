<?php

namespace App\Entity;

use App\Repository\CalendarEventRepository;
use Doctrine\DBAL\Types\Types;
use ApiPlatform\Metadata\Post;
use App\Dto\IndisponibiliteAgent;
use App\State\IcsCalendarProvider;
use App\State\IndisponibiliteStateProcessor;
use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\IdGenerator\UlidGenerator;

#[
    ORM\Entity(repositoryClass: CalendarEventRepository::class),
    Post(
        uriTemplate: '/indisponibilite/agent',
        securityPostValidation: "is_granted('CREATE_INDISPONIBILITE_AGENT', object)",
        input: IndisponibiliteAgent::class,
        processor: IndisponibiliteStateProcessor::class,
    ),
]
class CalendarEvent
{
    #[
        ORM\Id(),
        ORM\Column(type: "ulid", unique: true),
        ORM\GeneratedValue(strategy: "CUSTOM"),
        ORM\CustomIdGenerator(class: UlidGenerator::class)
    ]
    private string $id;

    #[ORM\Column(type: Types::TEXT)]
    private string $calendarData;

    #[ORM\Column(type: Types::STRING)]
    private string $calendarId;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private DateTime $lastModified;

    #[ORM\Column(type: Types::STRING, length: 32)]
    private string $etag;

    #[ORM\Column(type: Types::INTEGER)]
    private int $size = 0;

    #[ORM\Column(type: Types::STRING, length: 8)]
    private string $componentType;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private DateTime $firstOccurence;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?DateTime $lastOccurence = null;

    public function getId(): string
    {
        return $this->id;
    }

    public function getCalendarData(): string
    {
        return $this->calendarData;
    }

    public function setCalendarData(string $calendarData): CalendarEvent
    {
        $this->calendarData = $calendarData;
        $this->setEtag(md5($calendarData));
        return $this;
    }

    /**
     * @return string
     */
    public function getCalendarId(): string
    {
        return $this->calendarId;
    }

    /**
     * @param string $calendarId
     * @return CalendarEvent
     */
    public function setCalendarId(string $calendarId): CalendarEvent
    {
        $this->calendarId = $calendarId;
        return $this;
    }

    public function getLastModified(): DateTime
    {
        return $this->lastModified;
    }

    public function setLastModified(DateTime $lastModified): CalendarEvent
    {
        $this->lastModified = $lastModified;
        return $this;
    }

    public function getEtag(): string
    {
        return $this->etag;
    }

    protected function setEtag(string $etag): CalendarEvent
    {
        $this->etag = $etag;
        return $this;
    }

    public function getSize(): int
    {
        return $this->size;
    }

    public function setSize(int $size): CalendarEvent
    {
        $this->size = $size;
        return $this;
    }

    public function getComponentType(): string
    {
        return $this->componentType;
    }

    public function setComponentType(string $componentType): CalendarEvent
    {
        $this->componentType = $componentType;
        return $this;
    }

    public function getFirstOccurence(): DateTime
    {
        return $this->firstOccurence;
    }

    public function setFirstOccurence(DateTime $firstOccurence): CalendarEvent
    {
        $this->firstOccurence = $firstOccurence;
        return $this;
    }

    public function getLastOccurence(): ?DateTime
    {
        return $this->lastOccurence;
    }

    public function setLastOccurence(?DateTime $lastOccurence): CalendarEvent
    {
        $this->lastOccurence = $lastOccurence;
        return $this;
    }
}
