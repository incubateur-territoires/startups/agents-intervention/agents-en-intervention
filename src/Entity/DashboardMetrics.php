<?php

namespace App\Entity;

class DashboardMetrics
{
    public function __construct(
        public int $status_todo = 0,
        public int $status_blocked = 0,
        public int $status_in_progress = 0,
        public int $status_not_assigned = 0,
        public int $status_finished = 0,
        public int $urgent = 0
    ) {
    }
}
