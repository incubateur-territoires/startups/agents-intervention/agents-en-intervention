<?php

declare(strict_types=1);

namespace App\Entity;

use App\Repository\RoadmapRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: RoadmapRepository::class)]
class Roadmap
{
  #[ORM\Id]
  #[ORM\GeneratedValue(strategy: 'AUTO')]
  #[ORM\Column]
  private ?int $id = null;

  #[ORM\Column(length: 255)]
  private string $title;

  #[ORM\Column(type: Types::TEXT)]
  #[Assert\NotBlank]
  #[Assert\Length(max: 1000)]
  private string $description;

  #[ORM\Column(type: Types::DATE_MUTABLE, nullable: true)]
  private ?\DateTimeInterface $expirationDate = null;

  #[ORM\Column(length: 255)]
  #[Assert\NotBlank]
  #[Assert\Length(max: 255)]
  private string $estimatedDate;

  #[Gedmo\SortablePosition]
  #[ORM\Column]
  #[ORM\OrderBy(['position' => 'ASC'])]
  private int $position = 0;

  #[ORM\Column]
  private bool $published = false;

  public function getId(): ?int
  {
    return $this->id;
  }

  public function getTitle(): string
  {
    return $this->title;
  }

  public function setTitle(string $title): static
  {
    $this->title = $title;

    return $this;
  }

  public function getDescription(): string
  {
    return $this->description;
  }

  public function setDescription(string $description): static
  {
    $this->description = $description;

    return $this;
  }

  public function getPosition(): int
  {
    return $this->position;
  }

  public function setPosition(int $position): void
  {
    $this->position = $position;
  }

  public function getExpirationDate(): ?\DateTimeInterface
  {
    return $this->expirationDate;
  }

  public function setExpirationDate(?\DateTimeInterface $expirationDate): self
  {
    $this->expirationDate = $expirationDate;

    return $this;
  }

  public function getEstimatedDate(): string
  {
    return $this->estimatedDate;
  }

  public function setEstimatedDate(string $estimatedDate): self
  {
    $this->estimatedDate = $estimatedDate;

    return $this;
  }

  public function isPublished(): bool
  {
    return $this->published;
  }

  public function setPublished(bool $published): self
  {
    $this->published = $published;

    return $this;
  }
}
