<?php

declare(strict_types=1);

namespace App\Entity;

use ApiPlatform\Metadata\Post;
use App\State\CommentProcessor;
use DateTimeImmutable;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * L'entité représentant un commentaire.
 */
#[
  ORM\Entity(),
  Post(
      denormalizationContext: ['groups' => [Comment::GROUP_COMMENT_POST]],
      processor: CommentProcessor::class
  ),
  ORM\HasLifecycleCallbacks()
]
class Comment implements ActionLoggableInterface
{
  const string GROUP_COMMENT_POST = 'comment:post';

    /**
     * @var int|null l'identifiant.
     */
    #[
        ORM\Column(type: Types::INTEGER),
        ORM\GeneratedValue(),
        ORM\Id()
    ]
    private ?int $id;

    /**
     * @var string le message.
     */
    #[
        Assert\NotBlank(),
        Groups([
            Comment::GROUP_COMMENT_POST,
            Intervention::GROUP_INTERVENTION_GET,
            Intervention::GROUP_INTERVENTION_GET_COLLECTION
        ]),
        ORM\Column(type: Types::TEXT)
    ]
    private string $message;

    /**
     * @var DateTimeImmutable la date de création.
     */
    #[
        Groups([
            Intervention::GROUP_INTERVENTION_GET,
            Intervention::GROUP_INTERVENTION_GET_COLLECTION
        ]),
        ORM\Column(),
    ]
    private DateTimeImmutable $createdAt;

    /**
     * @var User l'auteur.
     */
    #[
        Groups([
            Intervention::GROUP_INTERVENTION_GET,
            Intervention::GROUP_INTERVENTION_GET_COLLECTION
        ]),
        ORM\JoinColumn(nullable: false),
        ORM\ManyToOne(),
    ]
    private User $author;

    /**
     * @var Intervention l'intervention.
     */
    #[
        Assert\NotNull(),
        Groups([Comment::GROUP_COMMENT_POST]),
        ORM\JoinColumn(nullable: false, onDelete: 'cascade'),
        ORM\ManyToOne(inversedBy: 'comments'),
    ]
    private Intervention $intervention;




    /**
     * Le constructeur.
     * @param string $message le message.
     * @param Intervention $intervention l'intervention.
     */
    public function __construct(
        string $message,
        Intervention $intervention
    ) {
        $this->id = null;
        $this->message = $message;
        $this->intervention = $intervention;
    }

    /**
     * Renvoie l'identifiant.
     * @return int|null l'identifiant.
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * Renvoie le message.
     * @return string le message.
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * Renvoie la date de création.
     * @return DateTimeImmutable la date de création.
     */
    public function getCreatedAt(): DateTimeImmutable
    {
        return $this->createdAt;
    }

    /**
     * Renvoie l'auteur.
     * @return User l'auteur.
     */
    public function getAuthor(): User
    {
        return $this->author;
    }

    /**
     * Renvoie l'intervention.
     * @return Intervention l'intervention.
     */
    public function getIntervention(): Intervention
    {
        return $this->intervention;
    }

    /**
     * Change le message.
     * @param string $message le message.
     */
    public function setMessage(string $message): static
    {
        $this->message = $message;
        return $this;
    }

    /**
     * Change la date de création.
     * @param DateTimeImmutable $createdAt la date de création.
     */
    public function setCreatedAt(DateTimeImmutable $createdAt): static
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * Change l'auteur.
     * @param User $author l'auteur.
     */
    public function setAuthor(User $author): static
    {
        $this->author = $author;
        return $this;
    }

    /**
     * Change l'intervention.
     * @param Intervention $intervention l'intervention.
     */
    public function setIntervention(Intervention $intervention): static
    {
        $this->intervention = $intervention;
        return $this;
    }

    #[
        ORM\PrePersist,
        ORM\PreUpdate
    ]
    public function updatedTimestamps(): void
    {
        if (!isset($this->createdAt)) {
            $this->setCreatedAt(new DateTimeImmutable('now'));
        }
    }
}
