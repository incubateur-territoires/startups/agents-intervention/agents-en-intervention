<?php

declare(strict_types=1);

namespace App\Entity;

/**
 * L'énumération des statuts.
 */
enum Status: string
{
  /**
   * Correspond à une intervention créée par un demandeur
   */
  case ToValidate = 'to-validate';
  case Blocked = 'blocked';
  case Finished = 'finished';
  case InProgress = 'in-progress';
  case ToDo = 'to-do';
  /**
   * L'intervention n'existe pas en tant que telle,
   * c'est une configuration de récurrence.
   */
  case Recurring = 'recurring';

  /**
   * Statut initial de l'intervention avant de décider
   * quel état sera sélectionné en fonction du rôle de
   * l'utilisateur qui a créé l'intervention
   */
  case Draft  = 'draft';

  case Rejected = 'rejected';
}
