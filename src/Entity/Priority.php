<?php

declare(strict_types=1);

namespace App\Entity;

/**
 * L'énumération des priorités.
 */
enum Priority: string
{
    case Normal = 'normal';
    case Urgent = 'urgent';
}
