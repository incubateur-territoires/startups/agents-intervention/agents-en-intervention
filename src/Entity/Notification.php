<?php

namespace App\Entity;

use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use App\Repository\NotificationRepository;

#[
  ORM\Entity(repositoryClass: NotificationRepository::class),
  ORM\Table(name: "notifications"),
  ORM\Index(name: "idx_user", columns: ["user_id"]),
  ORM\Index(name: "idx_is_read", columns: ["is_read"]),
  ORM\Index(name: "idx_created_at", columns: ["created_at"])
]
class Notification
{
  /**
   * L'identifiant unique de la notification.
   */
  #[
    ORM\Id,
    ORM\GeneratedValue,
    ORM\Column(type: Types::INTEGER)
  ]
  private ?int $id = null;

  /**
   * L'utilisateur qui reçoit la notification.
   */
  #[ORM\ManyToOne(targetEntity: User::class)]
  #[ORM\JoinColumn(nullable: false, onDelete: "CASCADE")]
  private User $user;

  /**
   * L'intervention liée à la notification.
   */
  #[ORM\ManyToOne(targetEntity: Intervention::class)]
  #[ORM\JoinColumn(nullable: true, onDelete: "CASCADE")]
  private ?Intervention $intervention = null;

  /**
   * Le type de notification (ex : nouvelle demande, commentaire, etc.).
   */
  #[ORM\Column(type: Types::STRING, length: 500)]
  private string $title = '';

  /**
   * Le message facultatif de la notification.
   */
  #[ORM\Column(type: Types::TEXT, nullable: true)]
  private ?string $message = null;

  /**
   * Indique si la notification a été lue ou non.
   */
  #[ORM\Column(type: Types::BOOLEAN, options: ["default" => false])]
  private bool $isRead = false;

  /**
   * La date et l'heure de création de la notification.
   */
  #[ORM\Column(type: Types::DATETIME_MUTABLE)]
  private \DateTimeInterface $createdAt;

  /**
   * Constructeur : initialise la date de création à la date et l'heure actuelles.
   */
  public function __construct()
  {
    $this->createdAt = new \DateTimeImmutable();
  }

  public function getId(): ?int
  {
    return $this->id;
  }

  public function getUser(): User
  {
    return $this->user;
  }

  public function setUser(User $user): self
  {
    $this->user = $user;

    return $this;
  }

  public function getIntervention(): ?Intervention
  {
    return $this->intervention;
  }

  public function setIntervention(?Intervention $intervention): self
  {
    $this->intervention = $intervention;

    return $this;
  }

  public function getTitle(): string
  {
    return $this->title;
  }

  public function setTitle(string $title): self
  {
    $this->title = $title;

    return $this;
  }

  public function getMessage(): ?string
  {
    return $this->message;
  }

  public function setMessage(?string $message): self
  {
    $this->message = $message;

    return $this;
  }

  public function getIsRead(): ?bool
  {
    return $this->isRead;
  }

  public function setIsRead(bool $isRead): self
  {
    $this->isRead = $isRead;

    return $this;
  }

  public function getCreatedAt(): ?\DateTimeInterface
  {
    return $this->createdAt;
  }

  public function setCreatedAt(\DateTimeInterface $createdAt): self
  {
    $this->createdAt = $createdAt;

    return $this;
  }
}
