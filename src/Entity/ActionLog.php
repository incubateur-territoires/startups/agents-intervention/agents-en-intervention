<?php

namespace App\Entity;

use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Doctrine\UuidV7Generator;
use Ramsey\Uuid\UuidInterface;
use stdClass;

#[ORM\Entity]
#[ORM\Table(name: 'action_log')]
class ActionLog implements \Stringable
{
  #[ORM\Id]
  #[ORM\GeneratedValue(strategy: "CUSTOM")]
  #[ORM\CustomIdGenerator(class: UuidV7Generator::class)]
  #[ORM\Column(type: 'uuid', unique: true)]
  private UuidInterface $id;

  #[ORM\Column(type: Types::DATETIME_MUTABLE)]
  private \DateTimeInterface $timestamp;

  #[ORM\Column(type: Types::STRING, length: 255)]
  private string $type;

  #[ORM\Column(type: Types::STRING, length: 255)]
  private string $entityClass;

  #[ORM\Column(type: Types::INTEGER, nullable: true)]
  private ?int $entityId = null;

  #[ORM\Column(type: Types::TEXT, nullable: true)]
  private ?string $changes = null; // Détails des modifications (JSON)

  #[ORM\Column(type: Types::INTEGER, length: 255, nullable: true)]
  private ?int $userIdentifier = null;

  public static function new(
    string  $type,
    string  $entityClass,
    ?int    $entityId,
    ?string $changes,
    ?int $userIdentifier
  ): ActionLog
  {
    $log = new ActionLog();
    $log->setTimestamp(new \DateTimeImmutable());
    $log->setType($type);
    $log->setEntityClass($entityClass);
    $log->setEntityId($entityId);
    $log->setChanges($changes);
    $log->setUserIdentifier($userIdentifier);

    return $log;
  }

  public function getId(): UuidInterface
  {
    return $this->id;
  }

  public function getEntityId(): ?int
  {
    return $this->entityId;
  }

  public function setEntityId(?int $entityId): ActionLog
  {
    $this->entityId = $entityId;
    return $this;
  }

  public function getTimestamp(): \DateTimeInterface
  {
    return $this->timestamp;
  }

  public function setTimestamp(\DateTimeInterface $timestamp): ActionLog
  {
    $this->timestamp = $timestamp;
    return $this;
  }

  public function getType(): string
  {
    return $this->type;
  }

  public function setType(string $type): ActionLog
  {
    $this->type = $type;
    return $this;
  }

  public function getEntityClass(): string
  {
    return $this->entityClass;
  }

  public function setEntityClass(string $entityClass): ActionLog
  {
    $this->entityClass = $entityClass;
    return $this;
  }

  public function getChanges(): ?string
  {
    return $this->changes;
  }

  public function getChangesObject(): mixed
  {
    if (empty($this->changes)) {
      return null;
    }

    return json_decode($this->changes);
  }

  public function setChanges(?string $changes): ActionLog
  {
    $this->changes = $changes;
    return $this;
  }

  public function getUserIdentifier(): ?int
  {
    return $this->userIdentifier;
  }

  public function setUserIdentifier(?int $userIdentifier): ActionLog
  {
    $this->userIdentifier = $userIdentifier;
    return $this;
  }

  public function __toString(): string
  {
    return $this->type;
  }

  public function isConsistent(): bool
  {
    if (empty($this->changes)) {
      return false;
    }

    /** @var stdClass{'valid': bool} $changesObj */
    $changesObj = json_decode($this->changes);
    return $changesObj->valid;
  }
}
