<?php

declare(strict_types=1);

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\GetCollection;
use App\State\MetricsProvider;

#[ApiResource(
  operations: [
    new GetCollection(
      uriTemplate: '/metrics',
      provider: MetricsProvider::class
    ),
//    new Get(
//      uriTemplate: '/metrics/page-stat',
//      provider: MetricsPageStatProvider::class
//    )
  ]
)]
class Metrics
{
  public int $nbCollectivites;
  public int $nbCollectivitesActives;
  public int $nbUtilisateurs;
  public int $nbInterventions;
}
