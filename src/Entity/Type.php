<?php

declare(strict_types=1);

namespace App\Entity;

use ApiPlatform\Metadata\GetCollection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * L'entité représentant un type.
 */
#[
  GetCollection(
    paginationEnabled: false,
    normalizationContext: ['groups' => [Type::GROUP_TYPE_GET]]
  ),
  ORM\Entity(),
  ORM\UniqueConstraint(columns: ['name', 'category_id']),
  UniqueEntity(['name', 'category'])
]
class Type implements \Stringable
{
  const string GROUP_TYPE_GET = 'type:get';

  /**
   * @var int|null l'identifiant.
   */
  #[
    Groups([
      Intervention::GROUP_INTERVENTION_GET,
      Type::GROUP_TYPE_GET,
    ]),
    ORM\Column(nullable: false),
    ORM\GeneratedValue(),
    ORM\Id()
  ]
  private ?int $id = null;

  /**
   * @var string le nom.
   */
  #[
    Assert\NotBlank(),
    Groups([
      Intervention::GROUP_INTERVENTION_GET,
      Intervention::GROUP_INTERVENTION_GET_COLLECTION,
      Type::GROUP_TYPE_GET,
    ]),
    ORM\Column(type: Types::TEXT),
  ]
  private string $name;

  /**
   * @var Category la catégorie.
   */
  #[
    Assert\NotBlank(),
    Groups([
      Intervention::GROUP_INTERVENTION_GET,
      Intervention::GROUP_INTERVENTION_GET_COLLECTION,
      Type::GROUP_TYPE_GET,
    ]),
    ORM\JoinColumn(nullable: false),
    ORM\ManyToOne(),
  ]
  private Category $category;

  /**
   * @var string la photo.
   */
  #[
    Assert\NotBlank(),
    Groups([
      Intervention::GROUP_INTERVENTION_GET,
      Intervention::GROUP_INTERVENTION_GET_COLLECTION,
      Type::GROUP_TYPE_GET,
    ]),
    ORM\Column(type: Types::TEXT),
  ]
  private string $picture;

  /**
   * @var string|null la description.
   */
  #[
    Assert\NotBlank(),
    Groups([
      Intervention::GROUP_INTERVENTION_GET,
      Type::GROUP_TYPE_GET,
    ]),
    ORM\Column(type: Types::TEXT, nullable: true),
  ]
  private ?string $description = null;

  #[
    Groups([
      Type::GROUP_TYPE_GET,
    ]),
    ORM\Column(nullable: true)
  ]
  private ?int $position = null;

  /**
   * Renvoie l'identifiant.
   * @return int|null l'identifiant.
   */
  public function getId(): ?int
  {
    return $this->id;
  }

  /**
   * Renvoie le nom.
   * @return string le nom.
   */
  public function getName(): string
  {
    return $this->name;
  }

  /**
   * Renvoie la catégorie.
   * @return Category la catégorie.
   */
  public function getCategory(): Category
  {
    return $this->category;
  }

  /**
   * Renvoie si la photo.
   * @return string la photo.
   */
  public function getPicture(): string
  {
    return $this->picture;
  }

  /**
   * Renvoie la description.
   * @return string|null la description.
   */
  public function getDescription(): ?string
  {
    return $this->description;
  }

  /**
   * Change le nom.
   * @param string $name le nom.
   */
  public function setName(string $name): void
  {
    $this->name = $name;
  }

  /**
   * Change la catégorie.
   * @param Category $category la catégorie.
   */
  public function setCategory(Category $category): void
  {
    $this->category = $category;
  }

  /**
   * Change la photo.
   * @param string $picture la photo.
   */
  public function setPicture(string $picture): void
  {
    $this->picture = $picture;
  }

  /**
   * Change la description.
   * @param string|null $description la description.
   */
  public function setDescription(?string $description): void
  {
    $this->description = $description;
  }

  public function getPosition(): ?int
  {
    return $this->position;
  }

  public function setPosition(?int $position): static
  {
    $this->position = $position;

    return $this;
  }

  #[\Override]
  public function __toString(): string
  {
    return $this->getName();
  }
}
