<?php

declare(strict_types=1);

namespace App\Entity;

use ApiPlatform\Metadata\GetCollection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * L'entité représentant une category.
 */
#[
    GetCollection(),
    ORM\Entity(),
]
class Category implements \Stringable
{
    /**
     * @var int|null l'identifiant.
     */
    #[
        Groups([
            Type::GROUP_TYPE_GET,
            Intervention::GROUP_INTERVENTION_GET_COLLECTION,
            Intervention::GROUP_INTERVENTION_GET
        ]),
        ORM\Column(nullable: false),
        ORM\GeneratedValue(),
        ORM\Id()
    ]
    private ?int $id = null;

    /**
     * @var string le nom.
     */
    #[
        Assert\NotBlank(),
        Groups([
            Intervention::GROUP_INTERVENTION_GET,
            Intervention::GROUP_INTERVENTION_GET_COLLECTION,
            Type::GROUP_TYPE_GET,
        ]),
        ORM\Column(type: Types::TEXT, unique: true)
    ]
    private string $name;

    /**
     * @var string la photo.
     */
    #[
        Assert\NotNull(),
        Groups([
            Intervention::GROUP_INTERVENTION_GET,
            Intervention::GROUP_INTERVENTION_GET_COLLECTION,
            Type::GROUP_TYPE_GET,
        ]),
        ORM\Column(type: Types::TEXT, nullable: false)
    ]
    private string $picture;

    /**
     * @var string|null la description.
     */
    #[
        Groups([
            Intervention::GROUP_INTERVENTION_GET,
            Intervention::GROUP_INTERVENTION_GET_COLLECTION,
            Type::GROUP_TYPE_GET,
        ]),
        ORM\Column(type: Types::TEXT, nullable: true),
    ]
    private ?string $description = null;

    #[
        Groups([Type::GROUP_TYPE_GET]),
        ORM\Column(nullable: true)
    ]
    private ?int $position = null;

    /**
     * Renvoie l'identifiant.
     * @return int|null l'identifiant.
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * Renvoie le nom.
     * @return string le nom.
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * Renvoie si la photo.
     * @return string la photo.
     */
    public function getPicture(): string
    {
        return $this->picture;
    }

    /**
     * Renvoie la description.
     * @return string|null la description.
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * Change le nom.
     * @param string $name le nom.
     */
    public function setName(string $name): static
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Change la photo.
     * @param string $picture la photo.
     */
    public function setPicture(string $picture): static
    {
        $this->picture = $picture;
        return $this;
    }

    /**
     * Change la description.
     * @param string|null $description la description.
     */
    public function setDescription(?string $description): static
    {
        $this->description = $description;
        return $this;
    }

    public function getPosition(): ?int
    {
        return $this->position;
    }

    public function setPosition(?int $position): static
    {
        $this->position = $position;

        return $this;
    }

    #[\Override]
    public function __toString(): string
    {
        return $this->getName();
    }
}
