<?php

namespace App\Entity;

/**
 * Permet de signaler qu'une entité peut être loggée dans la piste d'audit.
 */
interface ActionLoggableInterface
{
  public function getId(): ?int;
}
