<?php

declare(strict_types=1);

namespace App\Entity;

/**
 * L'énumération des étiquettes pour les photos.
 */
enum PictureTag: string
{
    case After = 'after';
    case Before = 'before';
    case Avatar = 'avatar';
}
