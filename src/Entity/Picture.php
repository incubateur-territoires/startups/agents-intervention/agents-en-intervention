<?php

declare(strict_types=1);

namespace App\Entity;

use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Post;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * L'entité représentant une photo.
 */
#[
  Delete(),
  Post(
    inputFormats: ['multipart' => ['multipart/form-data']],
    denormalizationContext: ['groups' => [Picture::GROUP_POST]]
  ),
  ORM\Entity(),
  ORM\HasLifecycleCallbacks()
]
class Picture
{
  const string GROUP_VIEW = 'picture:view';
  const string GROUP_POST = 'picture:post';

  /**
   * @var int|null l'identifiant.
   */
  #[
    Groups([
      Intervention::GROUP_INTERVENTION_GET,
      Intervention::GROUP_INTERVENTION_GET_COLLECTION,
      User::GROUP_GET,
      User::GROUP_GET_COLLECTION,
      Picture::GROUP_VIEW
    ]),
    ORM\Column(type: Types::INTEGER),
    ORM\GeneratedValue(),
    ORM\Id()
  ]
  private ?int $id;

  /**
   * @var string le nom du fichier.
   */
  #[
    Assert\NotBlank(),
    Groups([
      Intervention::GROUP_INTERVENTION_GET,
      Intervention::GROUP_INTERVENTION_GET_COLLECTION,
      Picture::GROUP_POST,
      User::GROUP_GET,
      User::GROUP_GET_COLLECTION,
      Picture::GROUP_VIEW
    ]),
    ORM\Column(type: Types::TEXT, unique: true),
  ]
  private string $fileName;

  /**
   * @var string|null l'URL.
   */
  #[
    Groups([
      Intervention::GROUP_INTERVENTION_GET,
      Intervention::GROUP_INTERVENTION_GET_COLLECTION,
      User::GROUP_GET,
      User::GROUP_GET_COLLECTION,
      Picture::GROUP_VIEW
    ])
  ]
  private ?string $url;

  /**
   * Liste les URLs pour chaque taille.
   *
   * @var array<string, string> les urls pour chaque taille.
   */
  private array $urls = [];

  /**
   * @var \DateTimeImmutable la date de création.
   */
  #[ORM\Column()]
  private \DateTimeImmutable $createdAt;

  /**
   * @var PictureTag|null l'étiquette.
   */
  #[
    Groups([
      Intervention::GROUP_INTERVENTION_GET,
      Intervention::GROUP_INTERVENTION_GET_COLLECTION,
      Picture::GROUP_POST,
      User::GROUP_GET,
      User::GROUP_GET_COLLECTION,
      Picture::GROUP_VIEW
    ]),
    ORM\Column(nullable: true)
  ]
  private ?PictureTag $tag;

  /**
   * @var Intervention|null référence.
   */
  #[
    Groups([Intervention::GROUP_INTERVENTION_PATCH, Picture::GROUP_POST]),
    ORM\JoinColumn(nullable: true, onDelete: 'cascade'),
    ORM\ManyToOne(cascade: ['all'], inversedBy: 'pictures'),
  ]
  private ?Intervention $intervention;

  /**
   * Le constructeur.
   * @param string $fileName le nom du fichier.
   * @param PictureTag|null $tag l'étiquette.
   * @param string|null $url l'URL du fichier.
   * @param Intervention|null $intervention l'intervention.
   */
  public function __construct(
    string        $fileName,
    ?PictureTag   $tag = null,
    ?string       $url = null,
    ?Intervention $intervention = null
  )
  {
    $this->id = null;
    $this->fileName = $fileName;
    $this->url = $url;
    $this->tag = $tag;
    $this->intervention = $intervention;
  }


  // Accesseurs :

  /**
   * Renvoie l'identifiant.
   * @return int|null l'identifiant.
   */
  public function getId(): ?int
  {
    return $this->id;
  }

  /**
   * Renvoie le fichier.
   * @return string le fichier.
   */
  public function getFileName(): string
  {
    return $this->fileName;
  }

  /**
   * Renvoie l'URL.
   * @return string|null l'URL.
   */
  public function getUrl(): ?string
  {
    return $this->url;
  }

  /**
   * Renvoie l'intervention.
   * @return \DateTimeImmutable la date de création.
   */
  public function getCreatedAt(): \DateTimeImmutable
  {
    return $this->createdAt;
  }

  /**
   * Renvoie l'étiquette.
   * @return PictureTag|null l'étiquette.
   */
  public function getTag(): ?PictureTag
  {
    return $this->tag;
  }

  /**
   * Renvoie l'intervention.
   * @return Intervention|null l'intervention.
   */
  public function getIntervention(): ?Intervention
  {
    return $this->intervention;
  }

  #[
    ORM\PrePersist(),
    ORM\PreUpdate()
  ]
  public function updatedTimestamps(): void
  {
    if (!isset($this->createdAt)) {
      $this->setCreatedAt(new \DateTimeImmutable('now'));
    }
  }


  // Mutateurs :

  /**
   * Change le nom du fichier.
   * @param string $fileName le nom du fichier.
   */
  public function setFileName(string $fileName): void
  {
    $this->fileName = $fileName;
  }

  /**
   * Change l'URL.
   * @param string|null $url l'URL.
   */
  public function setUrl(?string $url): void
  {
    $this->url = $url;
  }

  /**
   * Change la date de création.
   * @param \DateTimeImmutable $createdAt la date de création.
   */
  public function setCreatedAt(\DateTimeImmutable $createdAt): void
  {
    $this->createdAt = $createdAt;
  }

  /**
   * Change une étiquette.
   * @param PictureTag $tag l'étiquette.
   */
  public function setTag(PictureTag $tag): void
  {
    $this->tag = $tag;
  }

  /**
   * Change l'intervention.
   * @param Intervention|null $intervention l'intervention.
   */
  public function setIntervention(?Intervention $intervention): static
  {
    $this->intervention = $intervention;
    return $this;
  }

  /**
   * @return array<string, string>
   */
  public function getUrls(): array
  {
    return $this->urls;
  }

  #[Groups([
    Picture::GROUP_VIEW,
    Intervention::GROUP_INTERVENTION_GET,
    Intervention::GROUP_INTERVENTION_GET_COLLECTION,
  ])]
  public function getSmall(): ?string
  {
    return $this->urls['small'] ?? $this->getUrl() ?? '';
  }

  #[Groups([
    Picture::GROUP_VIEW,
    Intervention::GROUP_INTERVENTION_GET,
    Intervention::GROUP_INTERVENTION_GET_COLLECTION,
  ])]
  public function getMedium(): ?string
  {
    return $this->urls['medium'] ?? $this->getUrl() ?? '';
  }

  #[Groups([
    Picture::GROUP_VIEW,
    Intervention::GROUP_INTERVENTION_GET,
    Intervention::GROUP_INTERVENTION_GET_COLLECTION,
  ])]
  public function getOriginal(): ?string
  {
    return $this->urls['original'] ?? $this->getUrl() ?? '';
  }

  /**
   * @param array<string, string> $urls
   * @return $this
   */
  public function setUrls(array $urls): static
  {
    $this->urls = $urls;
    return $this;
  }
}
