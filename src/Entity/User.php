<?php

declare(strict_types=1);

namespace App\Entity;

use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Metadata\ApiFilter;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Link;
use ApiPlatform\Metadata\Post;
use App\Filter\RoleFilter;
use App\Repository\UserRepository;
use App\State\UserProcessor;
use App\State\UserProvider;
use DateTimeImmutable;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * L'entité représentant un utilisateur.
 */
#[
  ApiFilter(
    SearchFilter::class,
    properties: [
      'active' => 'exact',
    ]
  ),
  ApiFilter(RoleFilter::class, properties: ['roles']),
  Delete(
    security: 'is_granted("ROLE_DELETE_USERS")'
  ),
  Get(
    normalizationContext: ['groups' => [User::GROUP_VIEW, User::GROUP_GET]],
    security: 'is_granted("VIEW", object)',
    provider: UserProvider::class
  ),
  GetCollection(
    normalizationContext: ['groups' => [User::GROUP_VIEW, User::GROUP_GET_COLLECTION]],
    security: 'is_granted("ROLE_ADMIN")'
  ),
  GetCollection(
    uriTemplate: '/employers/{employerId}/active-agents',
    uriVariables: [
      'employerId' => new Link(toProperty: 'employer', fromClass: User::class),
    ],
    normalizationContext: ['groups' => [User::GROUP_VIEW, User::GROUP_AGENT_GET_COLLECTION]],
    security: 'is_granted("ROLE_GET_ACTIVE_AGENTS")'
  ),
  GetCollection(
    uriTemplate: '/employers/{employerId}/users',
    uriVariables: [
      'employerId' => new Link(toProperty: 'employer', fromClass: User::class),
    ],
    normalizationContext: ['groups' => [User::GROUP_VIEW, User::GROUP_GET_COLLECTION]],
    security: 'is_granted("ROLE_GET_USERS")',
    filters: ['user.role_filter'],
  ),
  Post(
    normalizationContext: ['groups' => [User::GROUP_VIEW, User::GROUP_GET_COLLECTION, User::GROUP_GET]],
    denormalizationContext: ['groups' => [User::GROUP_POST]],
    securityPostDenormalize: 'is_granted("ROLE_EDIT_USERS") and is_granted("CREATE", object)',
    securityPostDenormalizeMessage: 'Sorry, but you cannot create this user.',
    processor: UserProcessor::class
  ),
  ORM\Entity(repositoryClass: UserRepository::class),
  ORM\HasLifecycleCallbacks(),
  ORM\Table(name: '`user`'),
  UniqueEntity(
    fields: ['login'],
    message: 'Le login de l\'utilisateur doit être unique',
    errorPath: 'login'
  )
]
class User implements UserInterface, PasswordAuthenticatedUserInterface, \Stringable, ActionLoggableInterface
{
  const string GROUP_GET = 'user:get';
  const string GROUP_GET_COLLECTION = 'user:get-collection';
  const string GROUP_VIEW = 'user:view';
  const string GROUP_AGENT_GET_COLLECTION = 'agent:get-collection';
  const string GROUP_POST = 'user:post';

  /**
   * @var int|null l'identifiant.
   */
  #[
    Groups([
      User::GROUP_AGENT_GET_COLLECTION,
      Employer::GROUP_POSTED,
      Intervention::GROUP_INTERVENTION_GET,
      Intervention::GROUP_INTERVENTION_GET_COLLECTION,
      User::GROUP_GET,
      User::GROUP_GET_COLLECTION,
      User::GROUP_VIEW,
      "user:put",
    ]),
    ORM\Column(),
    ORM\GeneratedValue(),
    ORM\Id(),
    ORM\SequenceGenerator(sequenceName: 'user_id_seq', initialValue: 10)
  ]
  private ?int $id = null;

  /**
   * @var string l'identifiant de connexion.
   */
  #[
    Assert\NotBlank(),
    Groups([
      Employer::GROUP_POST,
      Employer::GROUP_POSTED,
      User::GROUP_POST,
      User::GROUP_VIEW,
    ]),
    ORM\Column(type: Types::TEXT, unique: true)
  ]
  private string $login = '';

  /**
   * @var string le mot de passe.
   */
  #[
    Groups([
      Employer::GROUP_POST,
      'user:patch',
      User::GROUP_POST,
    ]),
    ORM\Column(type: Types::TEXT)
  ]
  private string $password;

  /**
   * @var string le prénom.
   */
  #[
    Assert\NotBlank,
    Groups([
      User::GROUP_AGENT_GET_COLLECTION,
      Employer::GROUP_POST,
      Employer::GROUP_POSTED,
      Intervention::GROUP_INTERVENTION_GET,
      Intervention::GROUP_INTERVENTION_GET_COLLECTION,
      User::GROUP_GET,
      User::GROUP_GET_COLLECTION,
      User::GROUP_POST,
      User::GROUP_VIEW,
    ]),
    ORM\Column(type: Types::TEXT)
  ]
  private string $firstname = "";

  /**
   * @var string le nom.
   */
  #[
    Assert\NotBlank(),
    Groups([
      User::GROUP_AGENT_GET_COLLECTION,
      Employer::GROUP_POST,
      Employer::GROUP_POSTED,
      Intervention::GROUP_INTERVENTION_GET,
      Intervention::GROUP_INTERVENTION_GET_COLLECTION,
      User::GROUP_GET,
      User::GROUP_GET_COLLECTION,
      User::GROUP_POST,
      User::GROUP_VIEW,
    ]),
    ORM\Column(type: Types::TEXT)
  ]
  private string $lastname = "";

  /**
   * @var string|null l'e-mail.
   */
  #[
    Groups([
      User::GROUP_AGENT_GET_COLLECTION,
      Employer::GROUP_POST,
      Employer::GROUP_POSTED,
      Intervention::GROUP_INTERVENTION_GET,
      Intervention::GROUP_INTERVENTION_GET_COLLECTION,
      User::GROUP_GET,
      User::GROUP_GET_COLLECTION,
      User::GROUP_POST,
      User::GROUP_VIEW,
    ]),
    ORM\Column(type: Types::TEXT, nullable: true)
  ]
  private ?string $email = null;

  /**
   * @var string|null le numéro de téléphone.
   */
  #[
    Groups([
      User::GROUP_AGENT_GET_COLLECTION,
      Employer::GROUP_POST,
      Employer::GROUP_POSTED,
      Intervention::GROUP_INTERVENTION_GET,
      Intervention::GROUP_INTERVENTION_GET_COLLECTION,
      User::GROUP_GET,
      User::GROUP_GET_COLLECTION,
      User::GROUP_POST,
      User::GROUP_VIEW,
    ]),
    ORM\Column(type: Types::TEXT, nullable: true)
  ]
  private ?string $phoneNumber = null;

  /**
   * @var DateTimeImmutable la date de création.
   */
  #[
    Groups([User::GROUP_VIEW]),
    ORM\Column()
  ]
  private DateTimeImmutable $createdAt;

  /**
   * @var Picture|null la photo.
   */
  #[
    Groups([
      Intervention::GROUP_INTERVENTION_GET,
      Intervention::GROUP_INTERVENTION_GET_COLLECTION,
      User::GROUP_GET,
      User::GROUP_GET_COLLECTION,
      User::GROUP_POST,
      User::GROUP_VIEW,
    ]),
    ORM\OneToOne(targetEntity: Picture::class, cascade: ['persist'], fetch: 'EAGER')
  ]
  private ?Picture $picture = null;

  /**
   * @var Employer l'employeur.
   */
  #[
    Groups([
      Employer::GROUP_POST,
      Employer::GROUP_POSTED,
      User::GROUP_VIEW,
      User::GROUP_POST,
    ]),
    ORM\JoinColumn(nullable: false),
    ORM\ManyToOne(cascade: ['persist'])
  ]
  private Employer $employer;

  /**
   * @var bool si le compte est actif.
   */
  #[
    Assert\NotNull(),
    Groups([User::GROUP_VIEW]),
    ORM\Column(
      options: [
        'default' => true,
      ]
    )
  ]
  private bool $active = true;

  /**
   * @var DateTimeImmutable|null la date de la dernière connexion.
   */
  #[
    Groups([User::GROUP_VIEW]),
    ORM\Column(
      nullable: true,
      options: [
        'default' => null,
      ]
    )
  ]
  private ?DateTimeImmutable $connectedAt = null;

  /**
   * @var string[] les rôles
   * @see \App\Validator\Constraints\UserRoleConstraintValidator
   */
  #[
    Groups([
      Employer::GROUP_POSTED,
      User::GROUP_POST,
      User::GROUP_VIEW,
    ]),
//    ORM\Column(enumType: Role::class)
    ORM\Column()
  ]
  private array $roles = [];

  #[ORM\Column(type: Types::TEXT, nullable: true)]
  private ?string $proConnectToken = null;

    /**
   * Le constructeur.
   * @param string $login l'identifiant de connexion.
   * @param string $password le mot de passe.
   * @param string $firstname le prénom.
   * @param string $lastname le nom.
   * @param Employer $employer l'employeur.
   * @param array<string> $roles les rôles.
   * @param string|null $email l'e-mail.
   * @param string|null $phoneNumber le numéro de téléphone.
   * @param Picture|null $picture la photo.
   * @param bool $active si le compte est actif.
   * @param DateTimeImmutable|null $connectedAt la date de la dernière connexion.
   */
  public static function new(
    string             $login,
    string             $password,
    string             $firstname,
    string             $lastname,
    Employer           $employer,
    array              $roles = [],
    ?string            $email = null,
    ?string            $phoneNumber = null,
    ?Picture           $picture = null,
    bool               $active = true,
    ?DateTimeImmutable $connectedAt = null
  ): self
  {
      $user = new User();
      $user->id = null;
      $user->setLogin($login);
      $user->setPassword($password);
      $user->setFirstname($firstname);
      $user->setLastname($lastname);
      $user->setEmployer($employer);
      $user->setRoles($roles);
      $user->setEmail($email);
      $user->setPhoneNumber($phoneNumber);
      $user->setPicture($picture);
      $user->setActive($active);
      $user->setConnectedAt($connectedAt);

      return $user;
  }

  /**
   * Renvoie l'identifiant.
   * @return int|null l'identifiant.
   */
  public function getId(): ?int
  {
    return $this->id;
  }

  /**
   * Renvoie l'identifiant de connexion.
   * @return string l'identifiant de connexion.
   */
  public function getLogin(): string
  {
    return $this->login;
  }

  /**
   * Renvoie le mot de passe.
   * @return string le mot de passe.
   */
  #[\Override]
  public function getPassword(): string
  {
    return $this->password;
  }

  /**
   * Renvoie le prénom.
   * @return string le prénom.
   */
  public function getFirstname(): string
  {
    return $this->firstname;
  }

  /**
   * Renvoie le nom.
   * @return string le nom.
   */
  public function getLastname(): string
  {
    return $this->lastname;
  }

  /**
   * Renvoie l'e-mail.
   * @return string|null l'e-mail.
   */
  public function getEmail(): ?string
  {
    return $this->email;
  }

  /**
   * Renvoie le numéro de téléphone.
   * @return string|null le numéro de téléphone.
   */
  public function getPhoneNumber(): ?string
  {
    return $this->phoneNumber;
  }

  /**
   * Renvoie la date de création.
   * @return DateTimeImmutable la date de création.
   */
  public function getCreatedAt(): DateTimeImmutable
  {
    return $this->createdAt;
  }

  /**
   * Renvoie si la photo.
   * @return Picture|null la photo.
   */
  public function getPicture(): ?Picture
  {
    return $this->picture;
  }

  /**
   * Renvoie l'employeur.
   * @return Employer l'employeur.
   */
  public function getEmployer(): Employer
  {
    return $this->employer;
  }

  /**
   * Renvoie si le compte est actif.
   * @return bool si le compte est actif.
   */
  public function isActive(): bool
  {
    return $this->active;
  }

  /**
   * Renvoie la date de la dernière connexion.
   * @return DateTimeImmutable|null la date de la dernière connexion.
   */
  public function getConnectedAt(): ?DateTimeImmutable
  {
    return $this->connectedAt;
  }


  // Mutateurs :

  /**
   * Change l'identifiant de connexion.
   * @param string $login l'identifiant de connexion.
   */
  public function setLogin(string $login): void
  {
    $this->login = $login;
  }

  /**
   * Change le mot de passe.
   * @param string $password le mot de passe.
   */
  public function setPassword(string $password): void
  {
    $this->password = $password;
  }

  /**
   * Change le prénom.
   * @param string $firstname le prénom.
   */
  public function setFirstname(string $firstname): void
  {
    $this->firstname = $firstname;
  }

  /**
   * Change le nom.
   * @param string $lastname le nom.
   */
  public function setLastname(string $lastname): void
  {
    $this->lastname = $lastname;
  }

  /**
   * Change l'e-mail.
   * @param string|null $email l'e-mail.
   */
  public function setEmail(?string $email): self
  {
    $this->email = $email;
    return $this;
  }

  /**
   * Change le numéro de téléphone.
   * @param string|null $phoneNumber le numéro de téléphone.
   */
  public function setPhoneNumber(?string $phoneNumber): void
  {
    $this->phoneNumber = $phoneNumber;
  }

  /**
   * Change la date de création.
   * @param DateTimeImmutable $createdAt la date de création.
   */
  public function setCreatedAt(DateTimeImmutable $createdAt): void
  {
    $this->createdAt = $createdAt;
  }

  /**
   * Change la photo.
   * @param Picture|null $picture la photo.
   */
  public function setPicture(?Picture $picture): void
  {
    $this->picture = $picture;
  }

  /**
   * Change l'employeur.
   * @param Employer $employer l'employeur.
   */
  public function setEmployer(Employer $employer): void
  {
    $this->employer = $employer;
    $employer->addUser($this);
  }

  /**
   * Change si le compte est actif.
   * @param bool $active si le compte est actif.
   */
  public function setActive(bool $active): void
  {
    $this->active = $active;
  }

  /**
   * Change la date de la dernière connexion.
   * @param DateTimeImmutable|null $connectedAt la date de la dernière connexion.
   */
  public function setConnectedAt(?DateTimeImmutable $connectedAt): void
  {
    $this->connectedAt = $connectedAt;
  }

  #[ORM\PrePersist()]
  public function updatedTimestamps(): void
  {
    if (!isset($this->createdAt)) {
      $this->setCreatedAt(new DateTimeImmutable('now'));
    }
  }

  #[\Override]
  public function getUserIdentifier(): string
  {
    $login = $this->getLogin();
    \assert(!empty($login));
    return $login;
  }

    /**
     * @return string[]
     */
  #[\Override]
  public function getRoles(): array
  {
    return $this->roles;
  }

  public function hasRole(Role $role): bool
  {
    return in_array($role->value, $this->roles);// || in_array($role, $this->roles);
  }

  /**
   * Change les rôles.
   * @param array<Role|string> $roles les rôles.
   */
  public function setRoles(array $roles): static
  {
    $this->roles = array_map(fn($role) => is_string($role) ? $role : $role->value, $roles);
    return $this;
  }

  #[Groups([
    User::GROUP_VIEW,
    Intervention::GROUP_INTERVENTION_GET,
  ])]
  public function getRole(): string
  {
    $role = '';

    if ($this->hasRole(Role::Agent)) {
      $role = Role::Agent->value;
    } elseif ($this->hasRole(Role::AdminEmployer)) {
      $role = Role::AdminEmployer->value;
    } elseif ($this->hasRole(Role::Elected)) {
      $role = Role::Elected->value;
    } elseif ($this->hasRole(Role::Director)) {
      $role = Role::Director->value;
    } elseif ($this->hasRole(Role::Admin)) {
      $role = Role::Admin->value;
    }
    return $role;
  }

  public function getFullName(): string {
    return $this->getFirstname() . ' ' . $this->getLastname();
  }

  #[\Override]
  public function eraseCredentials(): void
  {
    // Ne rien faire dans ce cas
  }

  #[ORM\PrePersist, ORM\PreUpdate]
  public function ensureRoleAndEmployerAreCompatibles(): void
  {
    if (isset($this->employer)) {
      if ($this->employer->getName() === 'ANCT' && !$this->hasRole(Role::Admin)) {
        throw new \Error('ANCT employees must have ROLE_ADMIN role');
      }
      if ($this->employer->getName() !== 'ANCT' && $this->hasRole(Role::Admin)) {
        throw new \Error('An admin must have ANCT employer');
      }
    }
  }

  #[\Override]
  public function __toString(): string
  {
    return $this->getUserIdentifier();
  }

  public function getProConnectToken(): ?string
  {
      return $this->proConnectToken;
  }

  public function setProConnectToken(?string $proConnectToken): static
  {
      $this->proConnectToken = $proConnectToken;

      return $this;
  }

  public function isComplete():bool
  {
    if ($this->hasRole(Role::AdminEmployer)) {
      return $this->getEmail() !== null && $this->getPhoneNumber() !== null;
    } else {
      return true;
    }
  }

  #[Groups([
    User::GROUP_VIEW,
  ])]
  public function isConnectWithProConnect(): bool {
    return $this->getProConnectToken() !== null;
}
}
