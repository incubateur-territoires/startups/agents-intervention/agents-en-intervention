<?php

declare(strict_types=1);

namespace App\Entity;

use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Put;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Event\PostPersistEventArgs;
use Doctrine\ORM\Event\PreRemoveEventArgs;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * L'entité représentant un employeur.
 */
#[
  Get(
    normalizationContext: ['groups' => [Employer::GROUP_VIEW, Employer::GROUP_GET]],
    security: 'is_granted("VIEW", object)'
  ),
  GetCollection(
    normalizationContext: ['groups' => [Employer::GROUP_VIEW, Employer::GROUP_GET]],
    security: 'is_granted("ROLE_ADMIN")'
  ),
  Put(
    uriTemplate: '/employers/{id}',
    requirements: ['id' => '\d+'],
    normalizationContext: ['groups' => [Employer::GROUP_PUT]],
    security: 'is_granted("EDIT", object)'
  ),
  ORM\Entity(),
  ORM\HasLifecycleCallbacks()
]
class Employer implements \Stringable, ActionLoggableInterface
{
  const string GROUP_VIEW = 'employer:view';
  const string GROUP_GET = 'employer:get';
  const string GROUP_POSTED = 'employer:posted';
  const string GROUP_PUT = 'employer:put';
  const string GROUP_POST = 'employer:post';

  /**
   * @var int|null l'identifiant.
   */
  #[
    Groups([
      Employer::GROUP_POSTED,
      Employer::GROUP_PUT,
      Employer::GROUP_VIEW,
      User::GROUP_GET_COLLECTION,
    ]),
    ORM\Column(),
    ORM\GeneratedValue(),
    ORM\Id()
  ]
  private ?int $id = null;

  /**
   * @var string le numéro SIREN.
   */
  #[
    Assert\NotBlank(),
    Assert\Regex(pattern: '/^[12].*$/', message: 'Le SIRET doit commencer par 1 ou 2 (pour une collectivité territoriale)'),
    Groups([
      Employer::GROUP_POSTED,
      Employer::GROUP_POST,
      Employer::GROUP_PUT,
      Employer::GROUP_VIEW,
    ]),
    ORM\Column(type: Types::TEXT, unique: true)
  ]
  private string $siren;

  /**
   * @var string le prénom.
   */
  #[
    Assert\NotBlank(),
    Groups([
      Employer::GROUP_POSTED,
      Employer::GROUP_POST,
      Employer::GROUP_PUT,
      Employer::GROUP_VIEW,
      User::GROUP_GET_COLLECTION,
    ]),
    ORM\Column(type: Types::TEXT)
  ]
  private string $name;

  /**
   * @var float la longitude.
   */
  #[
    Assert\NotNull(),
    Assert\Range(
      notInRangeMessage: 'employer.longitude.notInRange',
      min: -180,
      max: 180
    ),
    Groups([
      Employer::GROUP_POSTED,
      Employer::GROUP_POST,
      Employer::GROUP_PUT,
      Employer::GROUP_VIEW,
    ]),
    ORM\Column(type: Types::DECIMAL, precision: 9, scale: 6)
  ]
  private float $longitude;

  /**
   * @var float la latitude.
   */
  #[
    Assert\NotNull(),
    Assert\Range(
      notInRangeMessage: 'employer.latitude.notInRange',
      min: -90,
      max: 90
    ),
    Groups([
      Employer::GROUP_POSTED,
      Employer::GROUP_POST,
      Employer::GROUP_PUT,
      Employer::GROUP_VIEW,
    ]),
    ORM\Column(type: Types::DECIMAL, precision: 8, scale: 6)
  ]
  private float $latitude;

  /**
   * @var Collection<int, User> les utilisateurs.
   */
  #[
    ORM\OneToMany(
      targetEntity: User::class,
      mappedBy: 'employer',
      cascade: ['persist', 'remove']
    )
  ]
  private Collection $users;

  /**
   * @var string la timezone dans laquelle se situe la commune
   * Permet de générer correctement les récurrences
   */
  #[ORM\Column(length: 255, nullable: false, options: ['default' => 'Europe/Paris'])]
  private string $timezone;

  public function __construct()
  {
    $this->users = new ArrayCollection();
  }

  /**
   * @param string $siren le numéro SIREN.
   * @param string $name le prénom.
   * @param float $longitude la longitude.
   * @param float $latitude la latitude.
   */
  public static function new(
    string $siren,
    string $name,
    float  $longitude,
    float  $latitude,
    string $timezone = 'Europe/Paris'
  ): self
  {
    $employer = new Employer();
    $employer->id = null;
    $employer->siren = $siren;
    $employer->name = $name;
    $employer->longitude = $longitude;
    $employer->latitude = $latitude;
    $employer->timezone = $timezone;
    return $employer;
  }

  /**
   * Renvoie l'identifiant.
   * @return int|null l'identifiant.
   */
  public function getId(): ?int
  {
    return $this->id;
  }

  /**
   * Renvoie le numéro SIREN.
   * @return string le numéro SIREN.
   */
  public function getSiren(): string
  {
    return $this->siren;
  }

  /**
   * Renvoie le prénom.
   * @return string le prénom.
   */
  public function getName(): string
  {
    return $this->name;
  }

  /**
   * Renvoie la longitude.
   * @return float la longitude.
   */
  public function getLongitude(): float
  {
    return $this->longitude;
  }

  /**
   * Renvoie la latitude.
   * @return float la latitude.
   */
  public function getLatitude(): float
  {
    return $this->latitude;
  }

  /**
   * Renvoie les utilisateurs.
   * @return Collection<User> les utilisateurs.
   */
  public function getUsers(): Collection
  {
    return $this->users;
  }

  /**
   * Change le numéro SIREN.
   * @param string $siren le numéro SIREN.
   */
  public function setSiren(string $siren): void
  {
    $this->siren = $siren;
  }

  /**
   * Change le prénom.
   * @param string $name le prénom.
   */
  public function setName(string $name): void
  {
    $this->name = $name;
  }

  /**
   * Change la longitude.
   * @param float $longitude la longitude.
   */
  public function setLongitude(float $longitude): void
  {
    $this->longitude = $longitude;
  }

  /**
   * Change la latitude.
   * @param float $latitude la latitude.
   */
  public function setLatitude(float $latitude): void
  {
    $this->latitude = $latitude;
  }

  /**
   * Ajoute un utilisateur.
   * @param User $user un utilisateur.
   */
  public function addUser(User $user): void
  {
    if ($this->users->contains($user) === false) {
      $this->users->add($user);
      $user->setEmployer($this);
    }
  }

  /**
   * Enlève un utilisateur.
   * @param User $user un utilisateur.
   */
  public function removeUser(User $user): void
  {
    $this->users->removeElement($user);
  }

  public function getTimezone(): string
  {
    return $this->timezone;
  }

  public function setTimezone(string $timezone): static
  {
    $this->timezone = $timezone;

    return $this;
  }

  #[ORM\PostPersist]
  public function createLogicalSequence(PostPersistEventArgs $event): void
  {
    $entity = $event->getObject();

    // if this listener only applies to certain entity types,
    // add some code to check the entity type as early as possible
    if (!$entity instanceof self) {
      return;
    }
    $em = $event->getObjectManager();
    $id = $entity->getId();

    $em->getConnection()->executeQuery("INSERT INTO intervention_sequences (employer_id, seq)
      VALUES ($id, 0)");
    $em->getConnection()->executeQuery("INSERT INTO recurring_intervention_sequences (employer_id, seq)
      VALUES ($id, 0)");
  }

  #[ORM\PreRemove]
  public function removeLogicalSequences(PreRemoveEventArgs $event): void
  {
    $em = $event->getObjectManager();
    $id = $this->getId();

    $em->getConnection()->executeQuery("DELETE FROM intervention_sequences WHERE employer_id = $id");
    $em->getConnection()->executeQuery("DELETE FROM recurring_intervention_sequences WHERE employer_id = $id");
  }

  #[\Override]
  public function __toString(): string
  {
    return $this->getName();
  }
}
