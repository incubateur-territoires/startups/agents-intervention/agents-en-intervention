<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity()]
class RecurringInterventionSequences
{
  #[ORM\Id]
  #[ORM\OneToOne(cascade: ['persist', 'remove'])]
  #[ORM\JoinColumn(nullable: false)]
  private Employer $employer;

  #[ORM\Column(options: ['default' => 1])]
  private int $seq = 1;

  public function getEmployer(): Employer
  {
    return $this->employer;
  }

  public function setEmployer(Employer $employer): static
  {
    $this->employer = $employer;

    return $this;
  }

  public function getSeq(): int
  {
    return $this->seq;
  }

  public function setSeq(int $seq): static
  {
    $this->seq = $seq;

    return $this;
  }
}
