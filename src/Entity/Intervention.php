<?php

declare(strict_types=1);

namespace App\Entity;

use ApiPlatform\Doctrine\Orm\Filter\BooleanFilter;
use ApiPlatform\Doctrine\Orm\Filter\DateFilter;
use ApiPlatform\Doctrine\Orm\Filter\ExistsFilter;
use ApiPlatform\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Metadata\ApiFilter;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Link;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Post;
use App\Dto\InterventionRejectionInput;
use App\Event\InterventionType;
use App\Filter\InterventionParticipantFilter;
use App\Filter\InterventionSearchFilter;
use App\Model\Frequency;
use App\State\DeleteInterventionProcessor;
use App\State\EmployerInterventionProvider;
use App\State\InterventionPatchProcessor;
use App\State\InterventionProcessor;
use App\State\InterventionRejectionProcessor;
use App\State\InterventionRestartProcessor;
use DateTimeImmutable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Exception;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Event\PrePersistEventArgs;
use Doctrine\ORM\Mapping as ORM;
use LogicException;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Attribute\MaxDepth;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * L'entité représentant une intervention.
 */
#[
  ApiResource(paginationClientEnabled: true, paginationClientItemsPerPage: true, paginationEnabled: false),
  ApiFilter(OrderFilter::class, properties: ['logicId' => 'desc', 'title' => 'asc', 'startAt' => 'desc', 'createdAt' => 'desc', 'endedAt' => 'desc']),
  ApiFilter(SearchFilter::class, properties: [
    'priority' => 'exact',
    'status' => 'exact',
    'type.category' => 'exact',
    'author' => 'exact'
  ]),
  ApiFilter(InterventionSearchFilter::class),
  ApiFilter(InterventionParticipantFilter::class, properties: ['participants']),
  ApiFilter(ExistsFilter::class, properties: ['startAt', 'startedAt']),
  ApiFilter(BooleanFilter::class, properties: ['recurring', 'statusToBeValidate']),
  ApiFilter(DateFilter::class, properties: ['createdAt', 'startAt', 'startedAt']),
  Delete(
    security: 'is_granted("ROLE_DIRECTOR") and is_granted("DELETE", object)',
    processor: DeleteInterventionProcessor::class
  ),
  Get(
    normalizationContext: ['groups' => [Intervention::GROUP_INTERVENTION_VIEW, Intervention::GROUP_INTERVENTION_GET]],
    security: 'is_granted("VIEW", object)',
  ),
  GetCollection(
    uriTemplate: '/employers/{employerId}/interventions',
    uriVariables: [
      'employerId' => new Link(toProperty: 'employer', fromClass: Intervention::class),
    ],
    normalizationContext: ['groups' => [Intervention::GROUP_INTERVENTION_VIEW, Intervention::GROUP_INTERVENTION_GET_COLLECTION]],
    //security: 'is_granted("ROLE_DIRECTOR") or is_granted("ROLE_ELECTED")',
    // security is handle in EmployerInterventionsExtension
//    provider: EmployerInterventionProvider::class
  ),
  GetCollection(
    uriTemplate: '/users/{userId}/interventions',
    uriVariables: [
      'userId' => new Link(toProperty: 'participants', fromClass: Intervention::class),
    ],
    normalizationContext: ['groups' => [Intervention::GROUP_INTERVENTION_VIEW, Intervention::GROUP_INTERVENTION_GET_COLLECTION]],
    security: 'is_granted("ROLE_AGENT")',
  ),
  Patch(
    uriTemplate: '/interventions/{id}',
    inputFormats: ['json' => ['application/merge-patch+json']],
    requirements: ['id' => '\d+'],
    normalizationContext: ['groups' => [Intervention::GROUP_INTERVENTION_VIEW, Intervention::GROUP_INTERVENTION_GET, Picture::GROUP_VIEW]],
    denormalizationContext: ['groups' => [Intervention::GROUP_INTERVENTION_PATCH]],
    security: 'is_granted("EDIT", object)',
    processor: InterventionPatchProcessor::class
  ),
  Post(
    uriTemplate: '/interventions/{id}/reject',
    normalizationContext: ['groups' => [Intervention::GROUP_INTERVENTION_VIEW, Intervention::GROUP_INTERVENTION_GET, Picture::GROUP_VIEW]],
    input: InterventionRejectionInput::class,
    processor: InterventionRejectionProcessor::class
  ),
  Post(
    uriTemplate: '/interventions/{id}/restart',
    requirements: ['id' => '\d+'],
    status: 200,
    normalizationContext: ['groups' => [Intervention::GROUP_INTERVENTION_VIEW, Intervention::GROUP_INTERVENTION_GET, Picture::GROUP_VIEW]],
    security: 'is_granted("EDIT", object)',
    processor: InterventionRestartProcessor::class,
  ),
  Post(
    denormalizationContext: ['groups' => [Intervention::GROUP_INTERVENTION_POST]],
    processor: InterventionProcessor::class,
  ),
  ORM\Entity,
  ORM\HasLifecycleCallbacks
]
class Intervention implements ActionLoggableInterface
{
  const string GROUP_INTERVENTION_VIEW = 'intervention:view';
  const string GROUP_INTERVENTION_GET = 'intervention:get';
  const string GROUP_INTERVENTION_GET_COLLECTION = 'intervention:get-collection';
  const string GROUP_INTERVENTION_PATCH = 'intervention:patch';
  const string GROUP_INTERVENTION_POST = 'intervention:post';
  const string GROUP_EXPORT = 'intervention:export';

  /**
   * @var int|null l'identifiant.
   */
  #[
    Groups([Intervention::GROUP_INTERVENTION_VIEW]),
    ORM\Column(type: Types::INTEGER),
    ORM\GeneratedValue,
    ORM\Id
  ]
  private ?int $id;

  /**
   * @var int|null identifiant logique avec une numérotation pour chaque employer
   */
  #[
    ORM\Column,
    Groups([Intervention::GROUP_INTERVENTION_VIEW, Intervention::GROUP_EXPORT])
  ]
  private ?int $logicId = null;

  /**
   * @var DateTimeImmutable la date de création.
   */
  #[
    Groups([Intervention::GROUP_INTERVENTION_VIEW, Intervention::GROUP_EXPORT]),
    ORM\Column,
  ]
  private DateTimeImmutable $createdAt;

  /**
   * @var string|null la description
   */
  #[
    Groups([
      Intervention::GROUP_INTERVENTION_VIEW,
      Intervention::GROUP_INTERVENTION_PATCH,
      Intervention::GROUP_INTERVENTION_POST,
      Intervention::GROUP_EXPORT
    ]),
    ORM\Column(type: Types::TEXT, nullable: true)
  ]
  private ?string $description;

  /**
   * @var Status le statut.
   */
  #[
    Assert\NotNull,
    Groups([
      Intervention::GROUP_INTERVENTION_PATCH,
      Intervention::GROUP_INTERVENTION_POST,
      Intervention::GROUP_INTERVENTION_VIEW,
      Intervention::GROUP_EXPORT
    ]),
    ORM\Column(type: Types::STRING, enumType: Status::class)
  ]
  private Status $status;

  /**
   * @var Priority la priorité.
   */
  #[
    Assert\NotNull,
    Groups([
      Intervention::GROUP_INTERVENTION_PATCH,
      Intervention::GROUP_INTERVENTION_POST,
      Intervention::GROUP_INTERVENTION_VIEW,
      Intervention::GROUP_EXPORT
    ]),
    ORM\Column(type: Types::STRING, enumType: Priority::class)
  ]
  private Priority $priority;

  /**
   * @var Type le type.
   */
  #[
    Assert\NotNull,
    Groups([
      Intervention::GROUP_INTERVENTION_PATCH,
      Intervention::GROUP_INTERVENTION_POST,
      Intervention::GROUP_INTERVENTION_VIEW,
      Intervention::GROUP_EXPORT
    ]),
    ORM\JoinColumn(nullable: false),
    ORM\ManyToOne,
  ]
  private Type $type;

  /**
   * @var User l'auteur.
   */
  #[
    Groups([Intervention::GROUP_INTERVENTION_VIEW, Intervention::GROUP_EXPORT]),
    ORM\JoinColumn(nullable: false),
    ORM\ManyToOne,
    MaxDepth(1)
  ]
  private User $author;

  /**
   * @var Employer l'employeur.
   */
  #[
    Groups([
      Intervention::GROUP_INTERVENTION_VIEW,
      Intervention::GROUP_INTERVENTION_POST,
    ]),
    ORM\JoinColumn(nullable: false),
    ORM\ManyToOne,
    MaxDepth(1)
  ]
  private Employer $employer;

  /**
   * @var Location la localisation.
   */
  #[
    Assert\NotNull,
    Assert\Valid,
    Groups([
      Intervention::GROUP_INTERVENTION_PATCH,
      Intervention::GROUP_INTERVENTION_POST,
      Intervention::GROUP_INTERVENTION_VIEW,
      Intervention::GROUP_EXPORT
    ]),
    ORM\JoinColumn(nullable: false),
    ORM\ManyToOne(cascade: ['persist'])
  ]
  private Location $location;

  /**
   * @var DateTimeImmutable|null la date de fin.
   */
  #[
    Groups([
      Intervention::GROUP_INTERVENTION_VIEW,
      Intervention::GROUP_INTERVENTION_POST,
      Intervention::GROUP_INTERVENTION_PATCH,
      Intervention::GROUP_EXPORT
    ]),
    ORM\Column(nullable: true)
  ]
  private ?DateTimeImmutable $endedAt;

  /**
   * @var DateTimeImmutable|null la date de début de prise en charge effective de l'intervention.
   */
  #[
    Groups([
      Intervention::GROUP_INTERVENTION_VIEW,
      Intervention::GROUP_INTERVENTION_POST,
      Intervention::GROUP_INTERVENTION_PATCH,
      Intervention::GROUP_EXPORT
    ]),
    ORM\Column(nullable: true)
  ]
  private ?DateTimeImmutable $startedAt = null;

  /**
   * @var Collection<int, User> les intervenants.
   */
  #[
    Groups([
      Intervention::GROUP_INTERVENTION_PATCH,
      Intervention::GROUP_INTERVENTION_POST,
      Intervention::GROUP_INTERVENTION_VIEW
    ]),
    ORM\ManyToMany(targetEntity: User::class),
    MaxDepth(1)
  ]
  private Collection $participants;

  /**
   * @var Collection<int, Comment> les commentaires.
   */
  #[
    Groups([Intervention::GROUP_INTERVENTION_VIEW]),
    ORM\OneToMany(targetEntity: Comment::class, mappedBy: 'intervention', cascade: ['persist']),
    MaxDepth(1)
  ]
  private Collection $comments;

  /**
   * @var Collection<int, Picture> les photos.
   */
  #[
    Groups([
      Intervention::GROUP_INTERVENTION_POST,
      Intervention::GROUP_INTERVENTION_VIEW,
    ]),
    ORM\OneToMany(targetEntity: Picture::class, mappedBy: 'intervention', cascade: ['all'])
  ]
  private Collection $pictures;

  #[
    ORM\Column(nullable: true),
    Groups([
      Intervention::GROUP_INTERVENTION_PATCH,
      Intervention::GROUP_INTERVENTION_POST,
      Intervention::GROUP_INTERVENTION_VIEW,
      Intervention::GROUP_INTERVENTION_GET,
    ]),
  ]
  private ?DateTimeImmutable $startAt = null;

  #[
    ORM\Column(nullable: true),
    Groups([
      Intervention::GROUP_INTERVENTION_PATCH,
      Intervention::GROUP_INTERVENTION_POST,
      Intervention::GROUP_INTERVENTION_VIEW,
      Intervention::GROUP_INTERVENTION_GET,
    ]),
  ]
  private ?DateTimeImmutable $dueDate = null;

  #[ORM\ManyToOne(targetEntity: self::class, inversedBy: 'generatedInterventions')]
  private ?self $recurrenceReference = null;

  /**
   * @var Collection&iterable<Intervention> liste des interventions créées automatiquement liée à cette intervention récurrente
   */
  #[ORM\OneToMany(targetEntity: self::class, mappedBy: 'recurrenceReference')]
  private Collection $generatedInterventions;

  #[
    ORM\Column(nullable: true),
    Groups([
      Intervention::GROUP_INTERVENTION_PATCH,
      Intervention::GROUP_INTERVENTION_POST,
      Intervention::GROUP_INTERVENTION_VIEW,
      Intervention::GROUP_INTERVENTION_GET,
    ]),
  ]
  private ?DateTimeImmutable $endAt = null;

  #[
    ORM\Column(length: 20, options: ['default' => Frequency::ONE_TIME]),
    Groups([
      Intervention::GROUP_INTERVENTION_PATCH,
      Intervention::GROUP_INTERVENTION_POST,
      Intervention::GROUP_INTERVENTION_VIEW,
      Intervention::GROUP_INTERVENTION_GET,
    ]),
  ]
  private Frequency $frequency = Frequency::ONE_TIME;

  #[ORM\Column(length: 255, nullable: true)]
  private ?string $triggerAt = null;

  #[ORM\Column(type: Types::JSON, nullable: true)]
  private ?array $exceptions = [];

  #[
    ORM\Column(type: Types::INTEGER, options: ['default' => 1]),
    Groups([
      Intervention::GROUP_INTERVENTION_PATCH,
      Intervention::GROUP_INTERVENTION_POST,
      Intervention::GROUP_INTERVENTION_VIEW,
      Intervention::GROUP_INTERVENTION_GET,
    ]),
  ]
  private int $frequencyInterval = 1;

  #[
    ORM\Column(length: 255),
    Groups([
      Intervention::GROUP_INTERVENTION_PATCH,
      Intervention::GROUP_INTERVENTION_POST,
      Intervention::GROUP_INTERVENTION_VIEW,
      Intervention::GROUP_INTERVENTION_GET,
      Intervention::GROUP_EXPORT,
    ])
  ]
  private string $title;


  /**
   * Le constructeur.
   * @param string|null $description la description.
   * @param Priority $priority la priorité.
   * @param Type $type le type.
   * @param Location $location la localisation.
   * @param DateTimeImmutable|null $endedAt la date de fin.
   */
  public function __construct(
    ?string            $description,
    Priority           $priority,
    Type               $type,
    Location           $location,
    ?DateTimeImmutable $endedAt = null
  )
  {
    $this->id = null;
    $this->description = $description;
    $this->priority = $priority;
    $this->type = $type;
    $this->location = $location;
    $this->endedAt = $endedAt;
    $this->participants = new ArrayCollection();
    $this->comments = new ArrayCollection();
    $this->pictures = new ArrayCollection();
    $this->generatedInterventions = new ArrayCollection();
  }

  /**
   * Renvoie l'identifiant.
   * @return int|null l'identifiant.
   */
  public function getId(): ?int
  {
    return $this->id;
  }

  /**
   * Renvoie la date de création.
   * @return DateTimeImmutable la date de création.
   */
  public function getCreatedAt(): DateTimeImmutable
  {
    return $this->createdAt;
  }

  /**
   * Renvoie la description.
   * @return string|null la description.
   */
  public function getDescription(): ?string
  {
    return $this->description;
  }

  /**
   * Renvoie le statut.
   * @return Status le statut.
   */
  public function getStatus(): Status
  {
    return $this->status;
  }

  /**
   * Renvoie le statut sous forme de string
   */
  public function getStatusAsString(): string
  {
    return $this->status->value;
  }

  /**
   * Renvoie la priorité.
   * @return Priority la priorité.
   */
  public function getPriority(): Priority
  {
    return $this->priority;
  }

  /**
   * Renvoie le type.
   * @return Type le type.
   */
  public function getType(): Type
  {
    return $this->type;
  }

  /**
   * Renvoie l'auteur.
   * @return User l'auteur.
   */
  public function getAuthor(): User
  {
    return $this->author;
  }

  /**
   * Renvoie l'employeur.
   * @return Employer l'employeur.
   */
  public function getEmployer(): Employer
  {
    return $this->employer;
  }

  /**
   * Renvoie la localisation.
   * @return Location la localisation.
   */
  public function getLocation(): Location
  {
    return $this->location;
  }

  /**
   * Renvoie la date de fin.
   * @return DateTimeImmutable|null la date de fin.
   */
  public function getEndedAt(): ?DateTimeImmutable
  {
    return $this->endedAt;
  }

  /**
   * Renvoie les intervenants.
   * @return Collection<User> les intervenants.
   */
  public function getParticipants(): Collection
  {
    return $this->participants;
  }

  /**
   * Renvoie les commentaires.
   * @return Collection<Comment> les commentaires.
   */
  public function getComments(): Collection
  {
    return $this->comments;
  }

  /**
   * Renvoie les photos.
   * @return Collection<Picture> les photos.
   */
  public function getPictures(): Collection
  {
    return $this->pictures;
  }


  // Mutateurs :

  /**
   * Change la date de création.
   * @param DateTimeImmutable $createdAt la date de création.
   */
  public function setCreatedAt(DateTimeImmutable $createdAt): static
  {
    $this->createdAt = $createdAt->setTimezone(new \DateTimeZone('UTC'));
    return $this;
  }

  /**
   * Change la description.
   * @param string|null $description la description.
   */
  public function setDescription(?string $description): static
  {
    $this->description = $description;
    return $this;
  }

  /**
   * Change le statut.
   * @param Status|string $status le statut.
   * @return $this
   */
  public function setStatus(Status|string $status): static
  {
    if (is_string($status)) {
      $status = Status::from($status);
    }

    $this->status = $status;
    return $this;
  }

  /**
   * @param string $status
   * @return $this
   */
  public function setStatusAsString(string $status): static
  {
    $this->setStatus($status);
    return $this;
  }

  /**
   * Change la priorité.
   * @param Priority $priority la priorité.
   */
  public function setPriority(Priority $priority): static
  {
    $this->priority = $priority;
    return $this;
  }

  /**
   * Change le type.
   * @param Type $type le type.
   */
  public function setType(Type $type): static
  {
    $this->type = $type;
    return $this;
  }

  /**
   * Change l'auteur.
   * @param User $author l'auteur.
   */
  public function setAuthor(User $author): static
  {
    $this->author = $author;
    return $this;
  }

  /**
   * Change l'employeur.
   * @param Employer $employer l'employeur.
   */
  public function setEmployer(Employer $employer): static
  {
    $this->employer = $employer;
    return $this;
  }

  /**
   * Change la localisation.
   * @param Location $location la localisation.
   */
  public function setLocation(Location $location): static
  {
    $this->location = $location;
    return $this;
  }

  /**
   * Change la date à laquelle l'intervention a été marquée comme terminée
   * dans le cas d'une intervention simple.
   *
   * Change la date de fin de la récurrence dans le cas d'une intervention récurrente.
   *
   * @param DateTimeImmutable|null $endedAt la date de fin.
   */
  public function setEndedAt(?DateTimeImmutable $endedAt): static
  {
    $this->endedAt = $endedAt?->setTimezone(new \DateTimeZone('UTC'));
    return $this;
  }

  /**
   * Ajoute un participant.
   * @param User $participant un participant.
   */
  public function addParticipant(User $participant): void
  {
    if ($this->participants->contains($participant) === false) {
      $this->participants->add($participant);
    }
  }

  /**
   * Enlève un participant.
   * @param User $participant un participant.
   */
  public function removeParticipant(User $participant): void
  {
    $this->participants->removeElement($participant);
  }

  /**
   * Ajoute un commentaire.
   * @param Comment $comment un commentaire.
   */
  public function addComment(Comment $comment): void
  {
    if ($this->comments->contains($comment) === false) {
      $this->comments->add($comment);
    }
  }

  /**
   * Ajoute une liste de commentaires.
   * @param Collection<Comment> $comments une liste de commentaires.
   */
  public function setComments(Collection $comments): static
  {
    $this->comments = $comments;
    return $this;
  }

  /**
   * Enlève un commentaire.
   * @param Comment $comment un commentaire.
   */
  public function removeComment(Comment $comment): void
  {
    $this->comments->removeElement($comment);
  }

  /**
   * Ajoute une photo.
   * @param Picture $picture une photo.
   */
  public function addPicture(Picture $picture): void
  {
    if ($this->pictures->contains($picture) === false) {
      $picture->setIntervention($this);
      $this->pictures->add($picture);
    }
  }

  /**
   * Ajoute une liste de photos.
   * @param Collection<Picture> $pictures les photos.
   */
  public function setPictures(Collection $pictures): static
  {
    $this->pictures = $pictures;
    foreach ($this->pictures as $picture) {
      $picture->setIntervention($this);
    }
    return $this;
  }

  /**
   * Ajoute une liste de participants.
   * @param Collection<User> $participants les participants.
   */
  public function setParticipants(Collection $participants): static
  {
    $this->participants = $participants;
    return $this;
  }

  /**
   * Enlève une photo.
   * @param Picture $picture une photo.
   */
  public function removePicture(Picture $picture): void
  {
    $this->pictures->removeElement($picture);
  }

  public function getLogicId(): ?int
  {
    return $this->logicId;
  }

  public function setLogicId(int $logicId): static
  {
    $this->logicId = $logicId;

    return $this;
  }

  public function getStartAt(): ?DateTimeImmutable
  {
    return $this->startAt;
  }

  public function setStartAt(?DateTimeImmutable $startAt): static
  {
    $this->startAt = $startAt?->setTimezone(new \DateTimeZone('UTC'));

    return $this;
  }

  public function getDueDate(): ?DateTimeImmutable
  {
    return $this->dueDate;
  }

  public function setDueDate(?DateTimeImmutable $dueDate): static
  {
    $this->dueDate = $dueDate?->setTimezone(new \DateTimeZone('UTC'));

    return $this;
  }

  public function getRecurrenceReference(): ?self
  {
    return $this->recurrenceReference;
  }

  public function hasRecurrenceReference(): bool
  {
    return $this->recurrenceReference !== null;
  }


  public function setRecurrenceReference(?self $recurrenceReference): static
  {
    $this->recurrenceReference = $recurrenceReference;

    return $this;
  }

  /**
   * @return Collection<int, self>
   */
  public function getGeneratedInterventions(): Collection
  {
    return $this->generatedInterventions;
  }

  public function addGeneratedIntervention(self $generatedIntervention): static
  {
    if (!$this->generatedInterventions->contains($generatedIntervention)) {
      $this->generatedInterventions->add($generatedIntervention);
      $generatedIntervention->setRecurrenceReference($this);
    }

    return $this;
  }

  public function removeGeneratedIntervention(self $generatedIntervention): static
  {
    if ($this->generatedInterventions->removeElement($generatedIntervention) && $generatedIntervention->getRecurrenceReference() === $this) {
      $generatedIntervention->setRecurrenceReference(null);
    }

    return $this;
  }

  public function getEndAt(): ?DateTimeImmutable
  {
    return $this->endAt;
  }

  public function setEndAt(?DateTimeImmutable $endAt): static
  {
    $this->endAt = $endAt?->setTimezone(new \DateTimeZone('UTC'));

    return $this;
  }

  public function getFrequency(): Frequency
  {
    return $this->frequency;
  }

  public function setFrequency(Frequency $frequency): static
  {
    $this->frequency = $frequency;

    return $this;
  }

  public function getTriggerAt(): ?string
  {
    return $this->triggerAt;
  }

  public function setTriggerAt(?string $triggerAt): static
  {
    $this->triggerAt = $triggerAt;

    return $this;
  }

  public function getExceptions(): ?array
  {
    return $this->exceptions;
  }

  public function setExceptions(?array $exceptions): static
  {
    $this->exceptions = $exceptions;

    return $this;
  }

  public function getFrequencyInterval(): int
  {
    return $this->frequencyInterval;
  }

  public function setFrequencyInterval(int $frequencyInterval): static
  {
    $this->frequencyInterval = $frequencyInterval;

    return $this;
  }

  #[Groups([Intervention::GROUP_INTERVENTION_VIEW, Intervention::GROUP_INTERVENTION_GET_COLLECTION])]
  public function isRecurring(): bool
  {
    return $this->getFrequency() !== Frequency::ONE_TIME;
  }

  #[Groups([Intervention::GROUP_INTERVENTION_VIEW, Intervention::GROUP_INTERVENTION_GET_COLLECTION])]
  public function getCategory(): Category
  {
    return $this->getType()->getCategory();
  }

  public function getInterventionType(): InterventionType
  {
    $ret = InterventionType::Unknown;
    if ($this->getStatus() === Status::ToValidate) {
      $ret = InterventionType::Request;
    } elseif ($this->isRecurring()) {
      $ret = InterventionType::Recurring;
    } elseif ($this->getRecurrenceReference() === null) {
      // On ne prend pas en compte les interventions créées à partir de récurrences
      $ret = InterventionType::Simple;
    } elseif ($this->getRecurrenceReference() !== null) {
      $ret = InterventionType::Generated;
    }

    return $ret;
  }

  #[ORM\PrePersist, ORM\PreUpdate]
  public function ensureIntegrity(): void
  {
    if ($this->isRecurring()) {
      if ($this->getStatus() !== Status::Recurring) {
        throw new LogicException('Recurring intervention must have recurring status');
      }
      if ($this->getStartAt() === null) {
        throw new LogicException('Recurring intervention must have startAt datetime');
      }
      if ($this->getEndedAt() === null) {
        throw new LogicException('Recurring intervention must have endedAt datetime');
      }
      if ($this->getStartAt() > $this->getEndedAt()) {
        throw new LogicException('endedAt must be after startAt');
      }
    } else {
      if ($this->getStatus() === Status::Finished) {
        if ($this->getEndedAt() === null) {
          throw new LogicException('When intervention is finished, endedAt datetime must be provided');
        }
      } else {
        if ($this->getEndedAt() !== null) {
          throw new LogicException('one time intervention must not have endedAt datetime unless is finished');
        }
      }
    }
  }

  #[
    ORM\PrePersist,
    ORM\PreUpdate
  ]
  public function updatedTimestamps(): void
  {
    if (!isset($this->createdAt)) {
      $this->setCreatedAt(new DateTimeImmutable('now'));
    }
    if ($this->status === Status::Finished && !isset($this->endedAt)) {
      $this->setEndedAt(new DateTimeImmutable('now'));
    }
  }

  /**
   * Incrémente et affecte le logicId lors de la création de l'entité.
   *
   * @param PrePersistEventArgs $event
   * @return void
   * @throws Exception
   */
  #[ORM\PrePersist]
  public function logicalIncrement(PrePersistEventArgs $event): void
  {
    if ($this->logicId !== null) {
      return;
    }

    $employerId = $this->getEmployer()->getId();
    $em = $event->getObjectManager();

    // Start transaction
    $seq_table = $this->isRecurring() ? 'recurring_intervention_sequences' : 'intervention_sequences';
    $result = $em->getConnection()->executeQuery("UPDATE $seq_table
      SET seq = seq + 1
      WHERE employer_id = $employerId
      RETURNING seq");
    /** @var int|string $newId */
    $newId = $result->fetchOne();
    $result->free();
    if (!$newId) {
      throw new LogicException('Impossible de créer le logicId');
    }
    $this->setLogicId(intval($newId));
  }

  public function getTitle(): string
  {
    return $this->title;
  }

  public function setTitle(string $title): static
  {
    $this->title = $title;

    return $this;
  }

  public function isFrequencyDifferent(self $intervention): bool
  {
    return $intervention->getEndAt()?->getTimestamp() !== $this->getEndAt()?->getTimestamp()
      || $intervention->getEndedAt()?->getTimestamp() !== $this->getEndedAt()?->getTimestamp()
      || $intervention->getFrequency() !== $this->getFrequency()
      || $intervention->getFrequencyInterval() !== $this->getFrequencyInterval()
      || $intervention->getStartAt()?->getTimestamp() !== $this->getStartAt()?->getTimestamp();
  }

  public function isInformationDifferent(self $intervention): bool
  {
    return $intervention->getDescription() === $this->getDescription()
      || $intervention->getDueDate()?->getTimestamp() === $this->getDueDate()?->getTimestamp()
      || $intervention->getLocation() === $this->getLocation()
      || $intervention->getParticipants() === $this->getParticipants()
      || $intervention->getPriority() === $this->getPriority()
      || $intervention->getStatus() === $this->getStatus()
      || $intervention->getTitle() === $this->getTitle()
      || $intervention->getType() === $this->getType();
  }

  public function equalsTo(self $intervention): bool
  {
    return $intervention->getDescription() === $this->getDescription()
      && $intervention->getDueDate()?->getTimestamp() === $this->getDueDate()?->getTimestamp()
      && $intervention->getEndAt()?->getTimestamp() === $this->getEndAt()?->getTimestamp()
      && $intervention->getEndedAt()?->getTimestamp() === $this->getEndedAt()?->getTimestamp()
      && $intervention->getFrequency() === $this->getFrequency()
      && $intervention->getFrequencyInterval() === $this->getFrequencyInterval()
      && $intervention->getLocation() === $this->getLocation()
      && $intervention->getParticipants() === $this->getParticipants()
      && $intervention->getPriority() === $this->getPriority()
      && $intervention->getStartAt()?->getTimestamp() === $this->getStartAt()?->getTimestamp()
      && $intervention->getStatus() === $this->getStatus()
      && $intervention->getTitle() === $this->getTitle()
      && $intervention->getType() === $this->getType();
  }

  public function getStartedAt(): ?DateTimeImmutable
  {
    return $this->startedAt;
  }

  public function setStartedAt(?DateTimeImmutable $startedAt): self
  {
    $this->startedAt = $startedAt;
    return $this;
  }
}
