<?php

declare(strict_types=1);

namespace App\Entity;

/**
 * L'énumération des rôles.
 */
enum Role: string
{
    /** Rôle agent */
    case Agent = 'ROLE_AGENT';
    /** Rôle Administrateur pour une collectivité */
    case AdminEmployer = 'ROLE_ADMIN_EMPLOYER';
    /** Rôle Coordinateur */
    case Director = 'ROLE_DIRECTOR';
    /** Rôle Demandeur */
    case Elected = 'ROLE_ELECTED';
    /** Rôle administrateur ANCT */
    case Admin = 'ROLE_ADMIN';
}
