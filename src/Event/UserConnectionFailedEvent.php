<?php

declare(strict_types=1);

namespace App\Event;

class UserConnectionFailedEvent extends DomainEvent
{
  public function __construct(
    public readonly string $login,
  ) {
  }

  public static function getType(): string
  {
    return 'user_connection_failed';
  }
}
