<?php

declare(strict_types=1);

namespace App\Event;

use App\Entity\User;

class UserConnectedEvent extends DomainEvent
{
  public function __construct(
    public readonly User $user,
    public readonly ConnexionMode $mode
  ) {
  }

  public static function getType(): string
  {
    return 'user_connected';
  }
}
