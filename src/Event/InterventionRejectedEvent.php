<?php

namespace App\Event;

use App\Entity\Intervention;
use Symfony\Contracts\EventDispatcher\Event;

class InterventionRejectedEvent extends Event
{
  /**
   * @param Intervention $intervention
   */
  public function __construct(private readonly Intervention $intervention)
  {
  }

  /**
   * @return Intervention
   */
  public function getIntervention(): Intervention
  {
    return $this->intervention;
  }
}
