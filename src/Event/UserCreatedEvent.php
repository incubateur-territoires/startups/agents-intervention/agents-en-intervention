<?php

namespace App\Event;

use App\Entity\User;
use Symfony\Contracts\EventDispatcher\Event;

class UserCreatedEvent extends Event
{
  /**
   * @param User $user
   */
  public function __construct(public readonly User $user)
  {
  }
}
