<?php

namespace App\Event;

use App\Entity\Intervention;
use App\Entity\Status;
use Symfony\Contracts\EventDispatcher\Event;

/**
 * Événement métier déclenché lors de la création d'une intervention.
 */
class InterventionCreatedEvent extends Event
{
  /**
   * @param Intervention $intervention
   */
  public function __construct(public readonly Intervention $intervention)
  {
  }

  /**
   * @return Intervention
   */
  public function getIntervention(): Intervention
  {
    return $this->intervention;
  }

}
