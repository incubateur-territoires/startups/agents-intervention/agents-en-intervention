<?php

namespace App\Event;

enum ConnexionMode: string
{
  case Form = 'form';
  case ProConnect = 'proConnect';
}
