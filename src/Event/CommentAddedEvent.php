<?php

namespace App\Event;

use App\Entity\Comment;
use Symfony\Contracts\EventDispatcher\Event;

class CommentAddedEvent extends Event
{
  /**
   * @param Comment $comment
   */
  public function __construct(private readonly Comment $comment)
  {
  }

  /**
   * @return Comment
   */
  public function getComment(): Comment
  {
    return $this->comment;
  }

}
