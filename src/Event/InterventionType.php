<?php

namespace App\Event;

enum InterventionType
{
  /**
   * Demande d'intervention
   */
  case Request;

  /**
   * Intervention récurrente
   */
  case Recurring;

  /**
   * Intervention classique
   */
  case Simple;

  /**
   * Intervention générée
   */
  case Generated;

  /**
   * Le type n'a pas pu être déterminé
   * C'est une erreur de logique
   */
  case Unknown;
}
