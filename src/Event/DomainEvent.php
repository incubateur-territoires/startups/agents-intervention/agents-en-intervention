<?php

namespace App\Event;

abstract class DomainEvent
{
  abstract public static function getType(): string;
}
