<?php

namespace App\Event;

use App\Entity\Employer;
use Symfony\Contracts\EventDispatcher\Event;

/**
 * Événement métier déclenché lors de la création d'une collectivité.
 */
class EmployerCreatedEvent extends Event
{
  public function __construct(
    /**
     * La collectivité créée.
     */
    public readonly Employer $employer,

    /**
     * @var bool Indique si la collectivité a été créée par un administrateur.
     */
    public readonly bool $createdByAdmin,
  )
  {
  }
}
