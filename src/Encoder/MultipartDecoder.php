<?php

declare(strict_types=1);

namespace App\Encoder;

use App\Service\S3Service;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Serializer\Encoder\DecoderInterface;

final readonly class MultipartDecoder implements DecoderInterface
{
  public const string FORMAT = 'multipart';

  public function __construct(private RequestStack $requestStack, private S3Service $s3Service)
  {
  }

  #[\Override]
  public function supportsDecoding(string $format): bool
  {
    return self::FORMAT === $format;
  }

  #[\Override]
  public function decode(string $data, string $format, array $context = []): ?array
  {
    $request = $this->requestStack->getCurrentRequest();

    if (!$request) {
      return null;
    }

    /** @var UploadedFile|null $file */
    $file = $request->files->get('fileName');
    if (!$file) {
      throw new \LogicException('MultipartDecoder :: file must be send with fileName key');
    }

    $newFileName = $this->uploadFile(
      $file,
      md5($file->getClientOriginalName() . microtime())
    );

    $request->request->add(['fileName' => $newFileName]);
    /** @var string[] $all */
    $all = $request->request->all();
    return array_map(static function (string $element) {
        // Multipart form values will be encoded in JSON.
        $decoded = json_decode($element, true);

        return \is_array($decoded) ? $decoded : $element;
      }, $all) + $request->files->all();

  }

  private function uploadFile(UploadedFile $uploadedFile, string $interventionURI): string
  {
    $interventionURIItems = explode('/', $interventionURI);
    $interventionId = end($interventionURIItems);

    $newFileName = $this->generateNewFileName($interventionId, $uploadedFile->getClientOriginalExtension());
    $this->s3Service->putFile($uploadedFile, $newFileName);

    return $newFileName;
  }

  private function generateNewFileName(string $interventionId, string $fileExtension): string
  {
    $fileIndex = 0;

    do {
      $fileIndex++;
      $newFileName = 'interventions/' . $interventionId . '-' . $fileIndex . '.' . $fileExtension;
    } while ($this->s3Service->fileExist($newFileName) === true);

    return $newFileName;
  }
}
