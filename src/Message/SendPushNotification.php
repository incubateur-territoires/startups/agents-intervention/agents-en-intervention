<?php

namespace App\Message;

class SendPushNotification
{
  /**
   * @param string $event
   * @param array<string> $userIds
   * @param string $url
   * @param string $text
   * @param string $detail
   */
  public function __construct(
    public string $event,
    public array $userIds,
    public string $url,
    public string $text,
    public string $detail = ''
  ){}
}
