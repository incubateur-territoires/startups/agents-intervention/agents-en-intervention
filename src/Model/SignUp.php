<?php

namespace App\Model;

use Symfony\Component\Validator\Constraints as Assert;

class SignUp
{
    public function __construct(
        #[Assert\NotBlank]
        public string $firstname,
        #[Assert\NotBlank]
        public string $lastname,
        #[Assert\NotBlank]
        public string $role,
        #[Assert\NotBlank]
        public string $postalCode,
        #[Assert\NotBlank]
        public string $city,
        #[Assert\NotBlank]
        public string $siren,
        #[Assert\NotBlank]
        public string $email,
        #[Assert\NotBlank]
        public string $phone
    ) {
    }
}
