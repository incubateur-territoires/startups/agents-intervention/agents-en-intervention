<?php

namespace App\Model;

use App\Entity\Intervention;
use App\Entity\Status;
use DateMalformedIntervalStringException;
use DateMalformedPeriodStringException;
use DateMalformedStringException;
use DateTimeInterface;
use Exception;
use Psr\Log\LoggerInterface;
use Traversable;

class Recurrence implements \IteratorAggregate
{
  private \DateTimeImmutable $start;
  private \DateTimeImmutable $end;
  private readonly Frequency $frequency;
  private readonly int $step;

  public function __construct(
    private readonly Intervention  $recurringIntervention,
    ?DateTimeInterface             $start = null,
    ?DateTimeInterface             $end = null,
    /*
     * Je mets par défault la TZ de Paris, mais il faudra l'avoir par commune !
     */
    private readonly \DateTimeZone $tz = new \DateTimeZone('Europe/Paris'),
    private readonly \DateTimeImmutable $now = new \DateTimeImmutable('now', new \DateTimeZone('Europe/Paris')),
  )
  {
    if (!$start) {
      $start = new \DateTimeImmutable();
    }
    if (!$end) {
      $end = new \DateTimeImmutable();
    }

    // Les dates doivent être cohérentes avec les dates de la récurrence
    if ($this->recurringIntervention->getStartAt()) {
      $this->start = max(\DateTimeImmutable::createFromInterface($start), $this->recurringIntervention->getStartAt());
    } else {
      $this->start = \DateTimeImmutable::createFromInterface($start);
    }
    $this->start = $this->start->setTimezone($this->tz);

    if ($this->recurringIntervention->getEndedAt()) {
      $this->end = min(\DateTimeImmutable::createFromInterface($end), $this->recurringIntervention->getEndedAt());
    } else {
      $this->end = \DateTimeImmutable::createFromInterface($end);
    }
    $this->end = $this->end->setTimezone($this->tz);

    $this->frequency = $this->recurringIntervention->getFrequency();
    $this->step = $this->recurringIntervention->getFrequencyInterval();
  }

  /**
   * @return Traversable<string>
   * @throws DateMalformedIntervalStringException
   * @throws DateMalformedPeriodStringException
   * @throws DateMalformedStringException
   */
  #[\Override]
  public function getIterator(): Traversable
  {
    return $this->toInterventionIterator($this->internalGetIterator($this->start, $this->end));
  }

  /**
   * @param Traversable<string> $iterator
   * @return Traversable<Intervention>
   * @throws DateMalformedStringException
   */
  private function toInterventionIterator(Traversable $iterator): Traversable
  {
    $utc = new \DateTimeZone('UTC');
    foreach ($iterator as $it) {
      $startAt = new \DateTimeImmutable("$it " . $this->recurringIntervention->getStartAt()?->format('H:i:s'), $utc);
      $endAt = new \DateTimeImmutable("$it " . $this->recurringIntervention->getEndAt()?->format('H:i:s'), $utc);

      /**
       * Passage en TZ local à la commune
       */
      $startAt = $startAt->setTimezone($this->tz);
      $endAt = $endAt->setTimezone($this->tz);

      /**
       * Ce calcul permet de calculer le décalage de TZ
       * et de l'appliquer à la date de début et de fin de l'intervention.
       */
      $offsetStart = $this->tz->getOffset($this->now) - $this->tz->getOffset($startAt);
      $startAt = $startAt->modify("$offsetStart seconds");
      $offsetEnd = $this->tz->getOffset($this->now) - $this->tz->getOffset($endAt);
      $endAt = $endAt->modify("$offsetEnd seconds");

      yield (new Intervention(
        $this->recurringIntervention->getDescription(),
        $this->recurringIntervention->getPriority(),
        $this->recurringIntervention->getType(),
        $this->recurringIntervention->getLocation())
      )
        ->setTitle($this->recurringIntervention->getTitle())
        ->setStatus(Status::ToDo)
        ->setRecurrenceReference($this->recurringIntervention)
        ->setStartAt($startAt)
        ->setEndAt($endAt)
        ->setAuthor($this->recurringIntervention->getAuthor())
        ->setEmployer($this->recurringIntervention->getEmployer())
        ->setType($this->recurringIntervention->getType())
        ->setComments($this->recurringIntervention->getComments())
        ->setParticipants($this->recurringIntervention->getParticipants())
        ->setPictures($this->recurringIntervention->getPictures());
    }
  }

  /**
   * @param DateTimeInterface|null $filterStart
   * @param DateTimeInterface|null $filterEnd
   * @return Traversable<string>
   * @throws DateMalformedIntervalStringException
   * @throws DateMalformedPeriodStringException
   * @throws DateMalformedStringException
   */
  private function internalGetIterator(?DateTimeInterface $filterStart = null, ?DateTimeInterface $filterEnd = null): Traversable
  {
    $filter = $filterStart !== null && $filterEnd !== null;

    if ($this->frequency === Frequency::DAILY) {
      $frequency = sprintf('P%dD', $this->step);
      $interval = new \DateInterval($frequency);
      $period = new \DatePeriod($this->start, $interval, $this->end);

      foreach ($period as $it) {
        $n = $it->format('N');
        if ($filter && !$this->dateIn($it, $filterStart, $filterEnd)) {
          continue;
        }
        if ($n !== '6' && $n !== '7') {
          yield $it->format('Y-m-d');
        }
      }
    } elseif ($this->frequency === Frequency::WEEKLY) {
      $frequency = sprintf('P%dW', $this->step);
      $interval = new \DateInterval($frequency);
      $period = new \DatePeriod($this->start, $interval, $this->end);

      foreach ($period as $it) {
        if ($filter && !$this->dateIn($it, $filterStart, $filterEnd)) {
          continue;
        }
        yield $it->format('Y-m-d');
      }
    } elseif ($this->frequency === Frequency::MONTHLY) {
      // Combien-ème jour particulier (mardi par exemple) dans le mois
      $jourDansLeMois = intval($this->start->format('j'));
      $nbJourDansLeMois = intval(ceil($jourDansLeMois / 7));

      $current = clone $this->start;
      while ($current <= $this->end) {
        $computed = $current->modify($this->getLiteralDayOfWeek($nbJourDansLeMois) . ' ' . $this->start->format('l') . ' of this month');
        $computed = $computed->setTime(intval($current->format('G')), intval($current->format('i')), intval($current->format('s')), intval($current->format('u')));

        if ($filter && !$this->dateIn($computed, $filterStart, $filterEnd)) {
          $current = $current->modify("first day of +" . $this->step . " month");
          continue;
        }
        // verify that the "fifth tuesday" (exemple) is in the correct month => drop instead
        if ($current->format('Y-m') === $computed->format('Y-m')) {
          yield $computed->format('Y-m-d');
        }
        $current = $current->modify("first day of +" . $this->step . " month");
      }
    } elseif ($this->frequency === Frequency::YEARLY) {
      $frequency = sprintf('P%dY', $this->step);
      $interval = new \DateInterval($frequency);
      $period = new \DatePeriod($this->start, $interval, $this->end);

      foreach ($period as $it) {
        if ($filter && !$this->dateIn($it, $filterStart, $filterEnd)) {
          continue;
        }
        yield $it->format('Y-m-d');
      }
    }
  }

  /**
   * Return true if the given date is after or equal to start date
   * and strictly above end date.
   *
   * @param DateTimeInterface $dt
   * @param DateTimeInterface $start
   * @param DateTimeInterface $end
   * @return bool
   */
  private function dateIn(DateTimeInterface $dt, DateTimeInterface $start, DateTimeInterface $end): bool
  {
    return $dt >= $start && $dt < $end;
  }

  private function getLiteralDayOfWeek(int $n): string
  {
    return match ($n) {
      1 => 'first',
      2 => 'second',
      3 => 'third',
      4 => 'fourth',
      5 => 'fifth',
      6 => 'sixth',
      7 => 'seventh',
      default => throw new \LogicException("$n n'est pas dans la plage des jours autorisés")
    };
  }

}
