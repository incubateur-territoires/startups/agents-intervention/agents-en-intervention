<?php

namespace App\Model;

enum Frequency: string
{
  case ONE_TIME = 'onetime';
  case DAILY = 'daily';
  case WEEKLY = 'weekly';
  case MONTHLY = 'monthly';
  case YEARLY = 'yearly';
}
