<?php

namespace App\Twig;

use App\Service\AppConfigService;
use App\Service\TwigDataService;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class TwigExtension extends AbstractExtension
{
  public function __construct(
    private readonly AppConfigService $appConfigService,
    private readonly TwigDataService  $service
  )
  {
  }

  /**
   * @return TwigFunction[]
   */
  #[\Override]
  public function getFunctions(): array
  {
    return [
      new TwigFunction('getCommunes', callable: $this->service->getCommunes(...)),
      new TwigFunction('getActivesAgents', callable: $this->service->getActivesAgents(...)),
      new TwigFunction('getLogsForEntity', callable: $this->service->getLogsForEntity(...)),
      new TwigFunction('getHeaderLink', callable: fn() => $this->appConfigService->getConfigs(['headerLink', 'headerTitle', 'headerActivated'])),
    ];
  }
}
