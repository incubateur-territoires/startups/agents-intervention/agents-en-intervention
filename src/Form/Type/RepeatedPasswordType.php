<?php

namespace App\Form\Type;

use App\Validator\Constraints\PasswordComplexity;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RepeatedPasswordType extends AbstractType
{
  public function configureOptions(OptionsResolver $resolver): void
  {
    $resolver->setDefaults([
      'type' => PasswordType::class,
      'constraints' => [
        new PasswordComplexity(),
      ],
      'mapped' => false,
      'required' => true,
      'first_options' => [
        'label' => 'Mot de passe',
        'help' => 'Le mot de passe doit contenir au moins 10 caractères, une majuscule, une minuscule, un chiffre et un caractère spécial.',
        'hash_property_path' => 'password',
        'toggle' => true,
        'hidden_label' => 'Masquer',
        'visible_label' => 'Afficher',
        'attr' => [
          'class' => 'fr-input',
          'autocomplete' => 'new-password',
          'data-password-validation-target' => "input",
          'data-action' => "input->password-validation#validate",
        ],
        'row_attr' => [
          'class' => 'fr-input-group',
          'data-controller' => "password-validation",
        ],
        'label_attr' => [
          'class' => 'fr-label',
        ],
        'help_attr' => [
          'class' => 'fr-hint-text',
        ],
      ],
      'second_options' => [
        'label' => 'Confirmation',
        'toggle' => true,
        'required' => false,
        'hidden_label' => 'Masquer',
        'visible_label' => 'Afficher',
        'attr' => [
          'autocomplete' => 'new-password',
          'class' => 'fr-input'
        ],
        'row_attr' => [
          'class' => 'fr-input-group'
        ],
        'label_attr' => [
          'class' => 'fr-label',
        ],
      ],
    ]);
  }

  #[\Override]
  public function getParent(): string
  {
    return RepeatedType::class;
  }
}
