<?php

namespace App\Form;

use App\Entity\Employer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EmployerCreationType extends AbstractType
{
    #[\Override]
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        (new EmployerType())
            ->buildForm($builder, $options);

        $builder
//            ->setAttribute('class', '')
            ->add('user_lastname', TextType::class, [
                'label' => 'Nom',
                'row_attr' => [
                    'class' => 'fr-input-group'
                ],
                'label_attr' => [
                    'class' => 'fr-label',
                ],
                'attr' => [
                    'class' => 'form-control fr-input',
                ]
            ])
            ->add('user_firstname', TextType::class, [
                'label' => 'Prénom',
                'row_attr' => [
                    'class' => 'fr-input-group'
                ],
                'label_attr' => [
                    'class' => 'fr-label',
                ],
                'attr' => [
                    'class' => 'form-control fr-input',
                ]
            ])
            ->add('user_login', TextType::class, [
                'label' => 'Identifiant',
                'row_attr' => [
                    'class' => 'fr-input-group'
                ],
                'label_attr' => [
                    'class' => 'fr-label',
                ],
                'attr' => [
                    'class' => 'form-control fr-input',
                ]
            ])
            ->add('user_password', TextType::class, [
                'label' => 'Mot de passe',
                'row_attr' => [
                    'class' => 'fr-input-group'
                ],
                'label_attr' => [
                    'class' => 'fr-label',
                ],
                'attr' => [
                    'class' => 'form-control fr-input',
                ]
            ])
            ->add('user_email', EmailType::class, [
                'label' => 'Email',
                'row_attr' => [
                    'class' => 'fr-input-group'
                ],
                'label_attr' => [
                    'class' => 'fr-label',
                ],
                'attr' => [
                    'class' => 'form-control fr-input',
                ]
            ])
            ->add('user_phoneNumber', TextType::class, [
                'label' => 'Numéro de téléphone',
                'row_attr' => [
                    'class' => 'fr-input-group'
                ],
                'label_attr' => [
                    'class' => 'fr-label',
                ],
                'attr' => [
                    'class' => 'form-control fr-input',
                ]
            ])
        ;
    }

    #[\Override]
    public function configureOptions(OptionsResolver $resolver): void
    {
    }
}
