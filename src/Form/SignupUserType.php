<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class SignupUserType extends AbstractType
{
  public function buildForm(FormBuilderInterface $builder, array $options): void
  {
    $builder
      ->add('firstname', TextType::class, [
        'label' => 'Prénom',
        'constraints' => [
          new NotBlank([
            'message' => 'Veuillez saisir votre prénom',
          ]),
        ],
        'required' => true,
        'attr' => [
          'class' => 'form-control fr-input',
        ],
        'label_attr' => [
          'class' => 'fr-label',
        ],
        'row_attr' => [
          'class' => 'fr-input-group'
        ],
      ])
      ->add('lastname', TextType::class, [
        'label' => 'Nom',
        'constraints' => [
          new NotBlank([
            'message' => 'Veuillez saisir votre prénom',
          ]),
        ],
        'required' => true,
        'attr' => [
          'class' => 'form-control fr-input',
        ],
        'label_attr' => [
          'class' => 'fr-label',
        ],
        'row_attr' => [
          'class' => 'fr-input-group'
        ],
      ])
      ->add('email', EmailType::class, [
        'label' => 'E-mail',
        'required' => true,
        'attr' => [
          'class' => 'form-control fr-input',
        ],
        'help' => 'Cet e-mail sera utilisé pour vous connecter à votre compte',
        'help_attr' => [
          'class' => 'fr-hint-text',
        ],
        'label_attr' => [
          'class' => 'fr-label',
        ],
        'row_attr' => [
          'class' => 'fr-input-group'
        ],
      ])
      ->add('phoneNumber', TelType::class, [
        'label' => 'Numéro de téléphone',
        'required' => true,
        'attr' => [
          'class' => 'form-control fr-input',
        ],
        'label_attr' => [
          'class' => 'fr-label',
        ],
        'row_attr' => [
          'class' => 'fr-input-group'
        ],
      ])
      ->add('submit', SubmitType::class, [
        'label' => 'Suivant',
        'attr' => [
          'class' => 'fr-btn',
        ],
      ])
;
  }

  public function configureOptions(OptionsResolver $resolver): void
  {
    $resolver->setDefaults([
      'attr' => [
        'data-turbo' => 'false',
      ],
    ]);
  }
}
