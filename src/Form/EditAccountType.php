<?php

namespace App\Form;

use App\Entity\User;
use App\Form\Type\RepeatedPasswordType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ButtonType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;

class EditAccountType extends AbstractType
{
  #[\Override]
  public function buildForm(FormBuilderInterface $builder, array $options): void
  {
    $data = $options['data'];
    assert($data instanceof User);

    $connectWithProConnect = $data->getProConnectToken() !== null;

    $builder
      ->add('profilePicture', FileType::class, [
        'label' => 'Photo de profil (Fichier image)',
        'required' => false,
        'mapped' => false,

        'help' => 'Taille maximale : 2 Mo. Formats supportés : jpg, png.',
        'help_attr' => [
          'class' => 'fr-hint-text',
        ],

        // Contraintes sur le fichier uploadé
        'constraints' => [
          new File([
            'maxSize' => '2M',
            'mimeTypes' => [
              'image/jpeg',
              'image/png',
            ],
            'mimeTypesMessage' => 'Merci de télécharger une image valide (JPEG ou PNG).',
          ])
        ],

        'row_attr' => [
          'class' => 'fr-upload-group fr-input-group'
        ],
        'label_attr' => [
          'class' => 'fr-label',
        ],
        'attr' => [
          'class' => 'fr-upload',
        ]
      ])
      ->add('login', TextType::class, [
        'disabled' => $connectWithProConnect,
        'label' => 'Votre identifiant de connexion',
        'help' => $connectWithProConnect ? "Vous vous connectez à l'aide de ProConnect, il n'est pas possible de changer cet identifiant" : '',
        'help_attr' => [
          'class' => 'fr-hint-text',
        ],
        'row_attr' => [
          'class' => 'fr-input-group'
        ],
        'label_attr' => [
          'class' => 'fr-label',
        ],
        'attr' => [
          'class' => 'form-control fr-input',
          'autocomplete' => 'username',
        ]
      ]);

    if (!$connectWithProConnect) {
      $builder
        ->add('password', RepeatedPasswordType::class, [
          'required' => false
        ]);
    }

    $builder
      ->add('submit', SubmitType::class, [
        'row_attr' => [
          'class' => 'fr-input-group'
        ],
        'attr' => ['class' => 'fr-btn fr-btn--wide'],
        'label' => 'Enregistrer',
      ])
      ->add('cancel', ButtonType::class, [
        'row_attr' => [
          'class' => 'fr-input-group'
        ],
        'attr' => ['class' => 'fr-btn fr-btn--secondary fr-btn--wide', 'onclick' => 'javascript:history.back()'],
        'label' => 'Annuler',
      ]);
  }

  #[\Override]
  public function configureOptions(OptionsResolver $resolver): void
  {
    $resolver->setDefaults([
      'data_class' => User::class,
    ]);
  }
}
