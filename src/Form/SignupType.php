<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\Regex;

class SignupType extends AbstractType
{
  public function buildForm(FormBuilderInterface $builder, array $options): void
  {
    $builder
      ->add('siret', TextType::class, [
        'label' => 'SIRET de la collectivité*',
        'constraints' => [
          new Length([
            'min' => 14,
            'max' => 14,
            'exactMessage' => 'Le SIRET doit contenir exactement {{ limit }} chiffres.',
          ]),
          new Regex([
            'pattern' => '/^\d+$/',
            'message' => 'Le SIRET ne doit contenir que des chiffres.',
          ]),
          new Regex([
            'pattern' => '/^[12]/',
            'message' => 'Le SIRET doit commencer par 1 ou 2 (pour une collectivité territoriale).',
          ]),
        ],
        'required' => true,
        'attr' => [
          'class' => 'form-control fr-input',
          'placeholder' => 'Entrez le SIRET de votre collectivité',
        ],
        'help' => 'Pour retrouver le SIRET (14 chiffres) de votre collectivité, rendez-vous sur l’Annuaire des Entreprises.',
        'help_attr' => [
          'class' => 'fr-hint-text',
        ],
        'label_attr' => [
          'class' => 'fr-label',
        ],
      ])
      ->add('name', HiddenType::class)
      ->add('siren', HiddenType::class)
      ->add('longitude', HiddenType::class)
      ->add('latitude', HiddenType::class)
      ->add('submit', SubmitType::class, [
        'label' => 'Suivant',
        'attr' => [
          'class' => 'fr-btn',
          'disabled' => 'disabled',
        ],
      ]);
  }

  public function configureOptions(OptionsResolver $resolver): void
  {
    $resolver->setDefaults([
      // Configure your form options here
      'attr' => [
        'data-turbo' => 'false',
      ],
    ]);
  }
}
