<?php

namespace App\Form;

use App\Entity\User;
use App\Form\Type\RepeatedPasswordType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Regex;

class SignupCredentialType extends AbstractType
{
  public function buildForm(FormBuilderInterface $builder, array $options): void
  {
    $builder
      ->add('login', TextType::class, [
        'label' => 'Identifiant',
        'constraints' => [
          new NotBlank([
            'message' => 'Veuillez saisir un identifiant',
          ]),
          new Regex('/^[a-z0-9\.\-_]+$/', 'Votre identifiant ne doit contenir que des chiffres, des lettres minuscules ou les caractères spéciaux suivants “.-_@+” (cela peut être votre email)'),
        ],
        'required' => true,
        'attr' => [
          'autocomplete' => 'username',
          'autofocus' => true,
          'class' => 'form-control fr-input',
          'pattern' => '^[a-z0-9\.\-_@+]+$',
        ],
        'help' => 'Sans espace, uniquement chiffres, en lettres minuscules ou avec les caractères spéciaux suivants “.-_@+” (cela peut être votre email)',
        'help_attr' => [
          'class' => 'fr-hint-text',
        ],
        'label_attr' => [
          'class' => 'fr-label',
        ],
        'row_attr' => [
          'class' => 'fr-input-group'
        ],
      ])
      ->add('password', RepeatedPasswordType::class)
      ->add('agreeTerms', CheckboxType::class, [
        'label' => 'J’accepte les conditions générales d’utilisation',
        'required' => true,
        'mapped' => false,
        'row_attr' => [
          'class' => 'fr-checkbox-group',
        ],
        'label_attr' => [
          'class' => 'fr-label',
        ],
      ])
      ->add('firstname', HiddenType::class)
      ->add('lastname', HiddenType::class)
      ->add('email', HiddenType::class)
      ->add('phoneNumber', HiddenType::class)
      ->add('submit', SubmitType::class, [
        'label' => 'Créer mon compte',
        'attr' => [
          'class' => 'fr-btn',
        ],
      ]);
  }

  public function configureOptions(OptionsResolver $resolver): void
  {
    $resolver->setDefaults([
      'data_class' => User::class,
      'attr' => [
        'data-turbo' => 'false',
      ],
    ]);
  }
}
