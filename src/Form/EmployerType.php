<?php

namespace App\Form;

use App\Entity\Employer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TimezoneType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EmployerType extends AbstractType
{
  #[\Override]
  public function buildForm(FormBuilderInterface $builder, array $options): void
  {
    $builder
      ->add('name', TextType::class, [
        'label' => 'Nom',
        'row_attr' => [
          'class' => 'fr-input-group'
        ],
        'label_attr' => [
          'class' => 'fr-label',
        ],
        'attr' => [
          'class' => 'form-control fr-input',
        ]
      ])
      ->add('siren', TextType::class, [
        'label' => 'SIREN',
        'row_attr' => [
          'class' => 'fr-input-group'
        ],
        'label_attr' => [
          'class' => 'fr-label',
        ],
        'attr' => [
          'class' => 'form-control fr-input',
        ]
      ])
      ->add('longitude', NumberType::class, [
        'row_attr' => [
          'class' => 'fr-input-group'
        ],
        'label_attr' => [
          'class' => 'fr-label',
        ],
        'attr' => [
          'class' => 'form-control fr-input',
        ]])
      ->add('latitude', NumberType::class, [
        'row_attr' => [
          'class' => 'fr-input-group'
        ],
        'label_attr' => [
          'class' => 'fr-label',
        ],
        'attr' => [
          'class' => 'form-control fr-input',
        ]])
      ->add('timezone', TimezoneType::class, [
        'label' => 'Fuseau horaire',
        'preferred_choices' => ['Europe/Paris', 'America/Guadeloupe'],
        'row_attr' => [
          'class' => 'fr-input-group'
        ],
        'label_attr' => [
          'class' => 'fr-label',
        ],
        'attr' => [
          'class' => 'form-control fr-input',
        ]]);

    $builder->get('longitude')->addModelTransformer(new CallbackTransformer(
      fn($value) => $value,
      fn($value) => floatval($value)
    ));

    $builder->get('latitude')->addModelTransformer(new CallbackTransformer(
      fn($value) => $value,
      fn($value) => floatval($value)
    ));
  }

  #[\Override]
  public function configureOptions(OptionsResolver $resolver): void
  {
    $resolver->setDefaults([
      'data_class' => Employer::class,
    ]);
  }
}
