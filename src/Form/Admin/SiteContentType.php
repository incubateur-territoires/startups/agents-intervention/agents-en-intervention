<?php

namespace App\Form\Admin;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;

class SiteContentType extends AbstractType
{
  public function buildForm(FormBuilderInterface $builder, array $options): void
  {
    parent::buildForm($builder, $options);

    $builder
      ->add('headerLink', UrlType::class, [
        'label' => 'Lien du header',
        'required' => false,
        'row_attr' => [
          'class' => 'form-group form-widget',
        ],
        'label_attr' => [
          'class' => 'form-control-label',
        ],
        'attr' => [
          'class' => 'form-control',
          'placeholder' => 'https://example.anct.gouv.fr',
        ],
      ])
      ->add('headerTitle', TextType::class, [
        'label' => 'Titre du header',
        'required' => false,
        'row_attr' => [
          'class' => 'form-group form-widget',
        ],
        'label_attr' => [
          'class' => 'form-control-label',
        ],
        'attr' => [
          'class' => 'form-input form-control',
          'placeholder' => 'Réserver une démo',
        ],
      ])
      ->add('headerActivated', CheckboxType::class, [
        'label' => 'Activer le lien dans le header',
        'required' => false,
        'row_attr' => [
          'class' => 'form-group',
        ],
        'label_attr' => [
          'class' => 'form-control-label',
        ],
        'attr' => [
          'class' => 'form-check form-switch',
        ],
      ])
      ->add('submit', SubmitType::class, [
        'label' => 'Enregistrer',
        'row_attr' => [
          'class' => 'form-group',
        ],
        'attr' => [
          'class' => 'btn btn-primary',
        ],
      ])
    ;
  }
}
