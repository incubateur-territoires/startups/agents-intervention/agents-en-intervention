<?php

namespace App\Form\Admin;

use App\Entity\User;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ChangeAdminEmployerType extends AbstractType
{
  public function buildForm(FormBuilderInterface $builder, array $options): void
  {
    $employer = $options['employer'];

    // Sélectionner les utilisateurs qui appartiennent à l'employeur
    $builder
      ->add('newAdmin', EntityType::class, [
        'class' => User::class,
        'choice_label' => 'login',
        'query_builder' => fn($er) => $er->createQueryBuilder('u')
          ->where('u.employer = :employer and jsonb_exists(u.roles::jsonb , \'ROLE_ADMIN_EMPLOYER\') = false')
          ->setParameter('employer', $employer),
        'label' => 'Sélectionnez un nouvel administrateur pour cette collectivité',
        'required' => true,
        'row_attr' => [
          'class' => 'form-group form-widget',
        ],
        'label_attr' => [
          'class' => 'form-control-label required',
        ],
        'attr' => [
          'class' => 'form-select form-control',
        ],
      ]);
  }

  public function configureOptions(OptionsResolver $resolver): void
  {
    $resolver->setDefaults([
      'data_class' => null,  // Le formulaire n'est pas directement lié à une entité
      'employer' => null,    // Nous passons l'employeur au formulaire via les options
    ]);
  }
}
