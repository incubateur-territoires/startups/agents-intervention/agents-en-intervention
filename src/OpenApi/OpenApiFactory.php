<?php

namespace App\OpenApi;

use ApiPlatform\OpenApi\Factory\OpenApiFactoryInterface;
use ApiPlatform\OpenApi\OpenApi;
use ApiPlatform\OpenApi\Model\MediaType;
use ApiPlatform\OpenApi\Model\Operation;
use ApiPlatform\OpenApi\Model\Parameter;
use ApiPlatform\OpenApi\Model\PathItem;
use ApiPlatform\OpenApi\Model\RequestBody;
use ApiPlatform\OpenApi\Model\Response;
use ArrayObject;
use Symfony\Component\DependencyInjection\Attribute\AsDecorator;

#[AsDecorator('api_platform.openapi.factory')]
readonly class OpenApiFactory implements OpenApiFactoryInterface
{
    public function __construct(private OpenApiFactoryInterface $decorated)
    {
    }
    #[\Override]
    public function __invoke(array $context = []): OpenApi
    {
        $openApi = $this->decorated->__invoke($context);

        /**
         * Ajout de la documentation du endpoint permettant de récupérer les métriques
         * pour le dashboard des utilisateurs connectés (directeurs + élus)
         * Cette action est implémentée dans un Controller à par d'APIPlatform et ne bénéficie pas
         * de la documentation automatique.
         *
         * @see \App\Controller\DashboardMetrics
         */
        $schema = new ArrayObject([
            "type" => "object",
            "description" => "Metrics data for dashboard",
            "required" => ['status_todo', 'status_blocked', 'status_in_progress', 'status_not_assigned', 'status_finished', 'urgent'],
            "properties" => [
                'status_todo' => new ArrayObject(["readOnly" => true, 'description' => 'count of interventions todo', "type" => "integer"]),
                'status_blocked' => new ArrayObject(["readOnly" => true, 'description' => 'count of interventions blocked', "type" => "integer"]),
                'status_in_progress' => new ArrayObject(["readOnly" => true, 'description' => 'count of interventions in_progress', "type" => "integer"]),
                'status_not_assigned' => new ArrayObject(["readOnly" => true, 'description' => 'count of interventions not_assigned', "type" => "integer"]),
                'status_finished' => new ArrayObject(["readOnly" => true, 'description' => 'count of interventions finished', "type" => "integer"]),
                'urgent' => new ArrayObject(["readOnly" => true, 'description' => 'count of urgent interventions', "type" => "integer"])
            ]
        ]);
        $openApi->getComponents()->getSchemas()?->offsetSet('DashboardMetrics', $schema);

        $path = new PathItem(
            'Metrics dashboard',
            'michel michel',
            'michel michel michel',
            new Operation(
                'api_dashboard_metrics_get',
                ['Metrics'],
                [200 => new Response('DashboardMetrics', new ArrayObject([
                    "application/json" => new MediaType(new ArrayObject([
                        '$ref' => "#/components/schemas/DashboardMetrics"
                    ]))
                ]))],
                'summary michel',
                'Metrics for the private dashboard page',
                null,
                [
                    new Parameter(
                        'employerId',
                        'path',
                        'Employer identifier',
                        true,
                        false,
                        false,
                        ['type' => 'integer'],
                        "simple"
                    )
                ]
            )
        );

        $openApi->getPaths()->addPath('/employers/{employerId}/dashboard-metrics', $path);

        /**
         * Documentation du endpoint de demande de création de commune
         *
         * @see \App\Controller\SignUp
         */

        $schema = new ArrayObject([
            "type" => "object",
            "description" => "SignUp request",
            "required" => ['firstname', 'lastname', 'role', 'postalCode', 'city', 'siren', 'email', 'phone'],
            "properties" => [
                'firstname' => new ArrayObject(['description' => 'Firsname', "type" => "string"]),
                'lastname' => new ArrayObject(['description' => 'Lastanme', "type" => "string"]),
                'role' => new ArrayObject(['description' => 'Role in the city organisation', "type" => "string"]),
                'postalCode' => new ArrayObject(['description' => 'Postal code', "type" => "string"]),
                'city' => new ArrayObject(['description' => 'City', "type" => "string"]),
                'siren' => new ArrayObject(['description' => 'SIREN number', "type" => "string"]),
                'email' => new ArrayObject(['description' => 'Email', "type" => "string"]),
                'phone' => new ArrayObject(['description' => 'Phone number', "type" => "string"])
            ]
        ]);
        $openApi->getComponents()->getSchemas()?->offsetSet('SignUpRequest', $schema);

        $path = new PathItem(
            'Signup',
            'michel michel',
            'michel michel michel',
            null,
            null,
            new Operation(
                'api_sign_up',
                ['Signup'],
                [200 => new Response('"ok"')],
                'Signup endpoint for signup form',
                'Signup for public page',
                null,
                null,
                new RequestBody('SignUp object', new ArrayObject(['application/json' => [
                    'schema' => $schema,
                ]]), true)
            )
        );

        $openApi->getPaths()->addPath('/sign-up', $path);



        $path = new PathItem(
            'User',
            'michel michel',
            'michel michel michel',
            null,
            null,
            null,
            null,
            null,
            null,
            new Operation(
                'api_user_patch',
                ['User'],
                [200 => new Response('User resource patched', new ArrayObject([
                    "application/json" => new MediaType(new ArrayObject([
                        '$ref' => "#/components/schemas/User-user.view_user.get"
                    ]))
                ]))],
                'Patches a User resource.',
                'Patches a User resource.',
                null,
                [
                    new Parameter(
                        'id',
                        'path',
                        'User identifier',
                        true,
                        false,
                        false,
                        ['type' => 'integer'],
                        "simple"
                    )
                ],
                new RequestBody('The patched User resource', new ArrayObject(['application/json' => [
                    'schema' => new ArrayObject([
                        "type" => "object",
                        "description" => "The patched User resource",
                        "properties" => [
                            'login' => new ArrayObject(['description' => 'login', "type" => "string"]),
                            'password' => new ArrayObject(['description' => 'password', "type" => "string"]),
                            'firstname' => new ArrayObject(['description' => 'firstname', "type" => "string"]),
                            'lastname' => new ArrayObject(['description' => 'lastname', "type" => "string"]),
                            'picture' => new ArrayObject(['description' => 'picture', "type" => "string"]),
                            'role' => new ArrayObject(['description' => 'role', "type" => "string"]),
                        ]
                    ]),
                ]]), true)
            )
        );

        $openApi->getPaths()->addPath('/users-patch/{id}', $path);

        return $openApi;
    }
}
