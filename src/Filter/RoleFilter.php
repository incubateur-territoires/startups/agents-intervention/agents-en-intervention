<?php

namespace App\Filter;

use ApiPlatform\Doctrine\Orm\Filter\AbstractFilter;
use ApiPlatform\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use ApiPlatform\Metadata\Operation;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\PropertyInfo\Type;

final class RoleFilter extends AbstractFilter
{
  #[\Override]
  protected function filterProperty(
    string                      $property,
    mixed                       $value,
    QueryBuilder                $queryBuilder,
    QueryNameGeneratorInterface $queryNameGenerator,
    string                      $resourceClass,
    Operation|null              $operation = null,
    array                       $context = []
  ): void
  {
    // Otherwise filter is applied to order and page as well
    if (
      !$this->isPropertyEnabled($property, $resourceClass) ||
      !$this->isPropertyMapped($property, $resourceClass)
    ) {
      return;
    }
    $interventionAlias = $queryBuilder->getRootAliases()[0];

    if (is_array($value)) {
      $ors = [];
      foreach ($value as $val) {
        $ors[] = sprintf("jsonb_exists($interventionAlias.%s::jsonb , '%s') = true", $property, $val);
      }
      $queryBuilder->andWhere(implode(' OR ', $ors));
    } elseif (is_string($value)) {
      $queryBuilder
        ->andWhere(sprintf("jsonb_exists($interventionAlias.%s::jsonb , '%s') = true", $property, $value));
    } else {
      throw new \LogicException('RoleFilter :: $value not valid');
    }
  }

  // This function is only used to hook in documentation generators (supported by Swagger and Hydra)
  #[\Override]
  public function getDescription(string $resourceClass): array
  {
    if (!$this->properties) {
      return [];
    }
    $description = [];
    foreach ($this->properties as $property => $strategy) {
      $description[$property] = [
        'property' => $property,
        'type' => Type::BUILTIN_TYPE_STRING,
        'required' => false,
        'description' => 'Filter on a role',
        'openapi' => [
          'example' => 'Custom example that will be in the documentation and be the default value of the sandbox',
          'allowReserved' => false, // if true, query parameters will be not percent-encoded
          'allowEmptyValue' => true,
          'explode' => false, // to be true, the type must be Type::BUILTIN_TYPE_ARRAY, ?product=blue,green will be ?product=blue&product=green
        ],
      ];
      $description[$property . '[]'] = [
        'property' => $property,
        'type' => Type::BUILTIN_TYPE_ARRAY,
        'required' => false,
        'description' => 'Filter on a role',
        'openapi' => [
          'example' => 'Custom example that will be in the documentation and be the default value of the sandbox',
          'allowReserved' => false, // if true, query parameters will be not percent-encoded
          'allowEmptyValue' => true,
          'explode' => true, // to be true, the type must be Type::BUILTIN_TYPE_ARRAY, ?product=blue,green will be ?product=blue&product=green
        ],
      ];
    }
    return $description;
  }
}
