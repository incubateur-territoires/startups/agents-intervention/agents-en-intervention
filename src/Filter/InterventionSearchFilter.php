<?php

namespace App\Filter;

use ApiPlatform\Doctrine\Orm\Filter\AbstractFilter;
use ApiPlatform\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use ApiPlatform\Metadata\Operation;
use Doctrine\ORM\QueryBuilder;

final class InterventionSearchFilter extends AbstractFilter
{
  protected function filterProperty(string $property, mixed $value, QueryBuilder $queryBuilder, QueryNameGeneratorInterface $queryNameGenerator, string $resourceClass, ?Operation $operation = null, array $context = []): void
  {
    // Nous voulons un terme de recherche global, donc nous ignorons le nom de la propriété et utilisons toujours 'term'
    if ($property !== 'term' || !is_string($value)) {
      return;
    }

    // Appliquez des filtres pour plusieurs champs
    $rootAlias = $queryBuilder->getRootAliases()[0];
    $queryBuilder->leftJoin("$rootAlias.location", 'l');
    $queryBuilder->leftJoin("$rootAlias.author", 'a');
    $queryBuilder->leftJoin("$rootAlias.participants", 'p');

    $queryBuilder->andWhere(
      $queryBuilder->expr()->orX(
        $queryBuilder->expr()->like($queryBuilder->expr()->concat("' '", "$rootAlias.id"), ':term'),
        $queryBuilder->expr()->like("LOWER($rootAlias.title)", ':term'),
        $queryBuilder->expr()->like("LOWER(l.city)", ':term'),
        $queryBuilder->expr()->like("LOWER(l.postcode)", ':term'),
        $queryBuilder->expr()->like("LOWER(l.street)", ':term'),
        $queryBuilder->expr()->like("LOWER(a.firstname)", ':term'),
        $queryBuilder->expr()->like("LOWER(a.lastname)", ':term'),
        $queryBuilder->expr()->like("LOWER(p.firstname)", ':term'),
        $queryBuilder->expr()->like("LOWER(p.lastname)", ':term')
      )
    )
      ->setParameter('term', '%' . strtolower($value) . '%');
  }

  public function getDescription(string $resourceClass): array
  {
    return [
      'term' => [
        'type' => 'string',
        'required' => false,
        'description' => 'Search across multiple fields using a term',
      ],
    ];
  }
}
