<?php

namespace App\Filter;

use ApiPlatform\Doctrine\Orm\Filter\AbstractFilter;
use ApiPlatform\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use ApiPlatform\Metadata\Operation;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\PropertyInfo\Type;

/**
 * Ce filtre permet d'avoir un filtrage des interventions assignées à un agent
 * ou non-assignées (avec une condition OR).
 */
class InterventionParticipantFilter extends AbstractFilter
{

  /**
   * @inheritDoc
   */
  #[\Override]
  protected function filterProperty(string $property, mixed $value, QueryBuilder $queryBuilder, QueryNameGeneratorInterface $queryNameGenerator, string $resourceClass, Operation|null $operation = null, array $context = []): void
  {
    // Otherwise filter is applied to order and page as well
    if (
      !$this->isPropertyEnabled($property, $resourceClass) ||
      !$this->isPropertyMapped($property, $resourceClass, true)
    ) {
      return;
    }

    $alias = $queryBuilder->getRootAliases()[0];
    $orX = $queryBuilder->expr()->orX();

    if (!is_iterable($value)) {
      throw new \LogicException('InterventionParticipantFilter :: $value is not iterable');
    }

    foreach ($value as $condition) {
      if ($condition === 'null') {
        $orX->add(sprintf('%s.%s IS EMPTY', $alias, $property));
      } else {
        $parameterName = $queryNameGenerator->generateParameterName($property); // Generate a unique parameter name to avoid collisions with other filters
        $orX->add(sprintf(':%s MEMBER OF %s.%s', $parameterName, $alias, $property));
        $queryBuilder->setParameter($parameterName, intval($condition));
      }
    }

    $queryBuilder->andWhere($orX);
  }


  /**
   * @inheritDoc
   */
  #[\Override]
  public function getDescription(string $resourceClass): array
  {
    if (!$this->properties) {
      return [];
    }

    $description = [];
    foreach ($this->properties as $property => $strategy) {
      $description[$property . '[]'] = [
        'property' => $property,
        'type' => Type::BUILTIN_TYPE_ARRAY,
        'required' => false,
        'description' => 'Filter on participants',
        'is_collection' => true,
        'openapi' => [
          'example' => 'Custom example that will be in the documentation and be the default value of the sandbox',
          'allowReserved' => false,// if true, query parameters will be not percent-encoded
          'allowEmptyValue' => true,
          'explode' => false, // to be true, the type must be Type::BUILTIN_TYPE_ARRAY, ?product=blue,green will be ?product=blue&product=green
        ],
      ];
    }

    return $description;
  }

}

//SELECT o, type_a4, category_a5, author_a6, picture_a7, o_a1, location_a8, participants_a9, picture_a10, comments_a11, author_a12, picture_a13, pictures_a14 FROM App\Entity\Intervention o INNER JOIN o.employer o_a1 INNER JOIN o.type type_a4 INNER JOIN type_a4.category category_a5 INNER JOIN o.author author_a6 LEFT JOIN author_a6.picture picture_a7 INNER JOIN o.location location_a8 LEFT JOIN o.participants participants_a9 LEFT JOIN participants_a9.picture picture_a10 LEFT JOIN o.comments comments_a11 LEFT JOIN comments_a11.author author_a12 LEFT JOIN author_a12.picture picture_a13 LEFT JOIN o.pictures pictures_a14 WHERE o IN(SELECT o_a2 FROM App\Entity\Intervention o_a2 INNER JOIN o_a2.employer employer_a3 WHERE employer_a3.id = :id_p1 AND o_a2.employer = :employerId AND o_a2.status <> 'recurring' AND o_a2.status <> 'to-validate' AND (:participants_p2 MEMBER OF o_a2.participants OR o_a2.participants is null)) ORDER BY o.id ASC
