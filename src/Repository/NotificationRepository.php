<?php

namespace App\Repository;

use App\Entity\Notification;
use App\Entity\Notification as NotificationEntity;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class NotificationRepository extends ServiceEntityRepository
{
  public function __construct(
    ManagerRegistry $registry,
  ) {
    parent::__construct($registry, Notification::class);
  }

  /**
   * Récupère toutes les notifications (lues et non lues) d'un utilisateur, avec pagination.
   *
   * @param User $user L'utilisateur pour lequel récupérer les notifications
   * @param int $page Le numéro de page pour la pagination
   * @param int $limit Le nombre de notifications par page
   * @return array<Notification> Retourne un tableau de notifications paginées
   */
  public function getNotifications(User $user, int $page = 1, int $limit = 10): array
  {
    $offset = ($page - 1) * $limit;

    /** @var Notification[] */
    return $this->findBy(
      ['user' => $user],
      ['createdAt' => 'DESC'],
      $limit,
      $offset
    );
  }

  /**
   * @param User $user
   * @return int
   */
  public function countNotifications(User $user): int
  {
    return $this->count(['user' => $user]);
  }


  /**
   * Récupère les notifications non lues d'un utilisateur.
   *
   * @param User $user L'utilisateur pour lequel récupérer les notifications
   * @return array<Notification> Retourne un tableau de notifications non lues
   */
  public function getUnreadNotifications(User $user): array
  {
    /** @var Notification[] */
    return $this->findBy([
      'user' => $user,
      'isRead' => false
    ], ['createdAt' => 'DESC']);
  }
}
