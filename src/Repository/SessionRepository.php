<?php

namespace App\Repository;

use App\Entity\Session;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Session>
 */
class SessionRepository extends ServiceEntityRepository
{
  public function __construct(ManagerRegistry $registry)
  {
    parent::__construct($registry, Session::class);
  }

  public function deleteStale(): int
  {
    $qb = $this->createQueryBuilder('s');
    $qb->delete()
      ->where('s.lifetime < :time')
      ->setParameter('time', time());

    return $qb->getQuery()->execute();
  }

  /**
   * @param User $user
   * @return array<Session>
   */
  public function findByUser(User $user): array
  {
    $sessions = $this->findAll();

    return array_filter($sessions, fn(Session $session) => $session->getUsername() === $user->getUserIdentifier());
  }
}
