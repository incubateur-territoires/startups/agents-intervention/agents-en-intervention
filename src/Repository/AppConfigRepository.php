<?php

// src/Repository/AppConfigRepository.php
namespace App\Repository;

use App\Entity\AppConfig;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class AppConfigRepository extends ServiceEntityRepository
{
  public function __construct(ManagerRegistry $registry)
  {
    parent::__construct($registry, AppConfig::class);
  }

  public function findValue(string $key): ?string
  {
    /** @var AppConfig|null $config */
    $config = $this->find($key);
    return $config?->getValue();
  }
}
