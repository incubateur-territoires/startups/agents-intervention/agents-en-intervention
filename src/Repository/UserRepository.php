<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bridge\Doctrine\Security\User\UserLoaderInterface;

/**
 * Dépôt pour l'entité User.
 */
class UserRepository extends ServiceEntityRepository implements UserLoaderInterface
{
  /**
   * Le constructor.
   * @param ManagerRegistry $registry the registry manager.
   */
  public function __construct(ManagerRegistry $registry)
  {
    parent::__construct($registry, User::class);
  }

  /**
   * Renvoie l'utilisateur et ses rôles.
   * @param string $identifier l'identifiant.
   * @return User|null l'utilisateur et ses rôles.
   * @throws NonUniqueResultException
   */
  #[\Override]
  public function loadUserByIdentifier(string $identifier): ?User
  {
    $queryBuilder = $this->createQueryBuilder('user');
    $queryBuilder->where('user.login = :login')->setParameter('login', $identifier);
    /** @var User|null $user */
    $user = $queryBuilder->getQuery()->getOneOrNullResult();

    if ($user === null || $user->isActive() === false) {
      return null;
    }

    return $user;
  }

  /**
   * @param int $employerId
   * @return User[]
   */
  public function getActiveAgents(int $employerId): array
  {
    $queryBuilder = $this->getAgentsQueryBuilder($employerId);
    $queryBuilder->andWhere('u.active = true');

    /** @var User[] */
    return $queryBuilder->getQuery()->execute();
  }

  /**
   * @param int $employerId
   * @return User[]
   */
  public function getAgents(int $employerId): array
  {
    /** @var User[] */
    return $this->getAgentsQueryBuilder($employerId)->getQuery()->execute();
  }

  private function getAgentsQueryBuilder(int $employerId): QueryBuilder
  {
    $queryBuilder = $this->createQueryBuilder('u');
    $queryBuilder->andWhere('u.employer = :employerId');
    $queryBuilder->setParameter('employerId', $employerId);
    $queryBuilder->andWhere('u.active = true');

    if ($_ENV['APP_ENV'] !== 'test') {
      $queryBuilder->andWhere($queryBuilder->expr()->orX(
        "jsonb_exists(u.roles::jsonb , 'ROLE_AGENT') = true",
        "jsonb_exists(u.roles::jsonb , 'ROLE_ADMIN_EMPLOYER') = true",
        "jsonb_exists(u.roles::jsonb , 'ROLE_DIRECTOR') = true",
      ));
    }

    return $queryBuilder;
  }

  /**
   * Retourne l'administrateur de la commune.
   *
   * @param int $employerId
   * @return User|null
   */
  public function getAdminEmployer(int $employerId): ?User
  {
    $queryBuilder = $this->createQueryBuilder('u');
    $queryBuilder->andWhere('u.employer = :employerId');
    $queryBuilder->setParameter('employerId', $employerId);

    if ($_ENV['APP_ENV'] !== 'test') {
      $queryBuilder->andWhere(
        "jsonb_exists(u.roles::jsonb , 'ROLE_ADMIN_EMPLOYER') = true",
      );
    }

    /** @var User|null */
    return $queryBuilder->getQuery()->setMaxResults(1)->getOneOrNullResult();
  }

  public function getByEmail(mixed $email): ?User
  {
    /** @var User|null */
    return $this->createQueryBuilder('u')
      ->where('u.email = :email')
      ->setParameter('email', $email)
      ->getQuery()
      ->getOneOrNullResult();
  }

  public function saveProConnectToken(User $user, string $token): void
  {
    $user->setProConnectToken($token);
    $this->getEntityManager()->flush();
  }

}
