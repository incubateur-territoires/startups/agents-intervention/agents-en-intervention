<?php

namespace App\Repository;

use App\Entity\InterventionSequences;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<InterventionSequences>
 *
 * @method InterventionSequences|null find($id, $lockMode = null, $lockVersion = null)
 * @method InterventionSequences|null findOneBy(array $criteria, array $orderBy = null)
 * @method InterventionSequences[]    findAll()
 * @method InterventionSequences[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class InterventionSequencesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, InterventionSequences::class);
    }

//    /**
//     * @return InterventionSequences[] Returns an array of InterventionSequences objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('i')
//            ->andWhere('i.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('i.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?InterventionSequences
//    {
//        return $this->createQueryBuilder('i')
//            ->andWhere('i.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
