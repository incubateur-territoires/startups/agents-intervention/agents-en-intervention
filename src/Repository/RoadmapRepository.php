<?php

namespace App\Repository;

use App\Entity\Roadmap;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\Collection;
use Doctrine\Persistence\ManagerRegistry;

class RoadmapRepository extends ServiceEntityRepository
{
  public function __construct(
    ManagerRegistry $registry,
  ) {
    parent::__construct($registry, Roadmap::class);
  }

  /**
   * @return Collection<Roadmap>|array<Roadmap>
   */
  public function getHomepageRoadmap(): Collection|array {
    $qb = $this->createQueryBuilder('r')
      ->andWhere('r.published = true')
      ->andWhere('(r.expirationDate IS NULL OR r.expirationDate <= :now)')
      ->setParameter('now', new \DateTimeImmutable())
      ->orderBy('r.position', 'ASC');

    /** @var Collection<Roadmap> */
    return $qb->getQuery()->getResult();
  }
}
