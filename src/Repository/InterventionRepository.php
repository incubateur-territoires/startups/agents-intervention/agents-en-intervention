<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Employer;
use App\Entity\Intervention;
use App\Entity\Role;
use App\Entity\Status;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\Collection;
use Doctrine\Persistence\ManagerRegistry;

/**
 * Dépôt pour l'entité User.
 */
class InterventionRepository extends ServiceEntityRepository
{
  /**
   * Le constructor.
   * @param ManagerRegistry $registry the registry manager.
   */
  public function __construct(ManagerRegistry $registry)
  {
    parent::__construct($registry, Intervention::class);
  }

  /**
   * @param \DateTimeInterface $weekStart
   * @param \DateTimeInterface $weekEnd
   * @return Collection<Intervention>|array<Intervention>
   */
  public function getActiveRecurringIntervention(\DateTimeInterface $weekStart, \DateTimeInterface $weekEnd, ?int $interventionId = null): Collection|array
  {
    $qb = $this->createQueryBuilder('ri')
      ->andWhere('ri.frequency <> \'onetime\'')
      ->andWhere('ri.startAt < :st')
      ->andWhere('ri.endedAt > :et')
      ->setParameter('st', $weekEnd)
      ->setParameter('et', $weekStart);

    if ($interventionId) {
      $qb->andWhere('ri.id = :interventionId')
        ->setParameter('interventionId', $interventionId);
    }

    /** @var Collection<Intervention> */
    return $qb->getQuery()
      ->getResult();
  }

  /**
   * @param Employer $employer
   * @param array<Status> $statuses
   * @param int $hideGeneratedInterventionsAfterDays
   * @return Collection<Intervention>|array<Intervention>
   */
  public function getInterventionsForMap(Employer $employer, array $statuses, int $hideGeneratedInterventionsAfterDays = 1): Collection|array
  {
//    findBy(['employer' => $employer, 'status' => $statuses])
    $queryBuilder = $this->createQueryBuilder('ri')
      ->andWhere('ri.employer = :employer')
      ->andWhere('ri.status IN (:statuses)')
      ->setParameter('employer', $employer)
      ->setParameter('statuses', $statuses)
      ->andWhere('ri.status <> \'recurring\'')
      ->andWhere("(ri.recurrenceReference IS NULL OR (ri.recurrenceReference IS NOT NULL AND ri.startAt <= :afterDate))")
      ->setParameter('afterDate', new \DateTime("+$hideGeneratedInterventionsAfterDays days"));

    /** @var Collection<Intervention> */
    return $queryBuilder->getQuery()
      ->getResult();
  }

  /**
   * @param Employer $employer
   * @param User $user
   * @param string $sortBy
   * @param string $order
   * @return Collection<Intervention>|array<Intervention>
   */
  public function listToValidate(Employer $employer, User $user, string $sortBy, string $order): Collection|array
  {
    $queryBuilder = $this->createQueryBuilder('i')
      ->andWhere('i.employer = :employer')
      ->andWhere('i.status = :status')
      ->setParameter('employer', $employer)
      ->setParameter('status', Status::ToValidate);

    /*
    * Un agent n'a le droit de voir que les interventions auxquelles il est affecté
    */
    if ($user->hasRole(Role::Agent)) {
      $queryBuilder->andWhere(':userId MEMBER OF i.participants');
      $queryBuilder->setParameter('userId', $user->getId());
    }

    /*
     * Un demandeur ne peut accéder qu'aux interventions dont il est l'auteur
     */
    elseif ($user->hasRole(Role::Elected)) {
      $queryBuilder->andWhere(':userId = i.author');
      $queryBuilder->setParameter('userId', $user->getId());
    }

    $queryBuilder->orderBy('i.' . $sortBy, $order);

    /** @var Collection<Intervention> */
    return $queryBuilder->getQuery()->getResult();
  }
}
