<?php

namespace App\Repository;

use App\Entity\Employer;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class EmployerRepository extends ServiceEntityRepository
{

  public function __construct(ManagerRegistry $registry)
  {
    parent::__construct($registry, Employer::class);
  }

  /**
   * @return Employer|null
   */
  public function getAnct(): ?Employer
  {
    /** @var Employer|null */
    return $this->findOneBy(['name' => 'ANCT']);
  }

}
