<?php

declare(strict_types=1);

namespace App\Workflow;

use Symfony\Component\Workflow\Transition;
use Symfony\Component\Workflow\WorkflowInterface;

class TransitionGuesser
{
  public function guessTransition(WorkflowInterface $workflow, string $from, string $to): ?Transition
  {
    $foundTransition = null;

    foreach ($workflow->getDefinition()->getTransitions() as $transition) {
      if (\in_array($from, $transition->getFroms(), true) && \in_array($to, $transition->getTos(), true)) {
        if (null !== $foundTransition) {
          throw new \LogicException("This method doesn't manage multiple transitions from one state to another.");
        }

        $foundTransition = $transition;
      }
    }

    return $foundTransition;
  }

  public function guessTransitionFromSubject(WorkflowInterface $workflow, object $subject): ?Transition
  {
    $foundTransition = null;

    foreach ($workflow->getEnabledTransitions($subject) as $transition) {
      if (null !== $foundTransition) {
        throw new \LogicException("This method doesn't manage multiple transitions from one state to another.");
      }

      $foundTransition = $transition;
    }

    return $foundTransition;
  }
}
