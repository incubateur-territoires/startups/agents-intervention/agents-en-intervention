<?php

namespace App\EventListener;

use App\Entity\User;
use App\Event\ConnexionMode;
use App\Event\UserConnectedEvent;
use Doctrine\ORM\EntityManagerInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Psr\EventDispatcher\EventDispatcherInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\EventDispatcher\Attribute\AsEventListener;

#[AsEventListener(event: InteractiveLoginEvent::class, method: 'onLoginSuccess')]
readonly class LoginListener
{
  public function __construct(
    private EntityManagerInterface   $entityManager,
    private EventDispatcherInterface $eventDispatcher,
    private JWTTokenManagerInterface $tokenManager,
    private LoggerInterface          $logger,
  )
  {
  }

  public function onLoginSuccess(InteractiveLoginEvent $event): void
  {
    // Récupérer l'utilisateur connecté
    $user = $event->getAuthenticationToken()->getUser();
    \assert($user !== null);

    if ($user instanceof User) {
      $user->setConnectedAt(new \DateTimeImmutable());
      $this->entityManager->persist($user);
      $this->entityManager->flush();

      // Ajouter une information personnalisée dans la session
      $this->logger->info('Generate jwt for user', ['user' => $user->getId()]);
      $event->getRequest()->getSession()->set('jwt', $this->tokenManager->create($user));

      $this->eventDispatcher->dispatch(new UserConnectedEvent($user, ConnexionMode::Form));

    }


  }
}
