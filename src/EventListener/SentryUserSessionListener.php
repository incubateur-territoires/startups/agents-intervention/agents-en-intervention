<?php

namespace App\EventListener;

use App\Entity\User;
use Sentry\State\Scope;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\EventDispatcher\Attribute\AsEventListener;
use function Sentry\configureScope;

#[AsEventListener(event: 'kernel.request', method: 'onKernelRequest')]
readonly class SentryUserSessionListener
{
  public function __construct(private Security $security)
  {
  }

  public function onKernelRequest(): void
  {
    /** @var User|null $user */
    $user = $this->security->getUser();

    if ($user) {
      configureScope(function (Scope $scope) use ($user): void {
        $scope->setUser([
          'id' => $user->getId(),
        ]);
      });
    }
  }
}
