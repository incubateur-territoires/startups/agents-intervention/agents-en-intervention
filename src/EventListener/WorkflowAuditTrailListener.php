<?php

namespace App\EventListener;

use App\Entity\Intervention;
use App\Service\AuditTrailLogger;
use Psr\Log\LoggerInterface;
use Symfony\Component\Workflow\Attribute\AsGuardListener;
use Symfony\Component\Workflow\Attribute\AsTransitionListener;
use Symfony\Component\Workflow\Event\GuardEvent;
use Symfony\Component\Workflow\Event\TransitionEvent;

readonly class WorkflowAuditTrailListener
{
  public function __construct(
    private AuditTrailLogger $auditTrailLogger,
    private LoggerInterface  $logger,
  )
  {
  }

  #[AsTransitionListener(workflow: 'intervention_publishing')]
  public function onTransition(TransitionEvent $event): void
  {
    $transition = $event->getTransition();
    if ($transition === null) {
      $this->logger->error('No transition found in TransitionEvent');
      return;
    }

    $changes = $this->getChanges($event);

    /** @var Intervention $subject */
    $subject = $event->getSubject(); // L'entité gérée par le Workflow
    $this->auditTrailLogger->log('workflow_transition', $subject::class, $subject->getId(), $changes);
  }

  #[AsGuardListener(workflow: 'intervention_publishing', priority: -255)]
  public function onGuard(GuardEvent $event): void
  {
    $this->logger->debug('Workflow guard');
    if ($event->isBlocked()) {
      $this->logger->debug('Workflow guard blocked');

      $changes = $this->getChanges($event);

      // Stocker les raisons du blocage
      $blockedReasons = [];
      foreach ($event->getTransitionBlockerList() as $blocker) {
        $blockedReasons[] = $blocker->getMessage();
      }

      $changes['comment'] = 'Reasons : ' . implode(', ', $blockedReasons);

      /** @var Intervention $subject */
      $subject = $event->getSubject(); // L'entité gérée par le Workflow
      $this->auditTrailLogger->log('workflow_transition', $subject::class, $subject->getId(), $changes);
    }
  }

  /**
   * @param GuardEvent|TransitionEvent $event
   * @return array<string, string|null>
   */
  private function getChanges(GuardEvent|TransitionEvent $event): array {
    $transition = $event->getTransition();
    if ($transition === null) {
      return [];
    }

    $transitionName = $transition->getName(); // Nom de la transition
    $fromStates = implode(', ', $transition->getFroms()); // États précédents
    $toState = $transition->getTos()[0]; // État suivant

    return [
      'workflow' => 'intervention_workflow',
      'transition' => $transitionName,
      'from' => $fromStates,
      'to' => $toState,
      'comment' => null,
    ];
  }
}
