<?php

namespace App\EventListener\Domain;

use App\Entity\Notification;
use App\Entity\Role;
use App\Entity\User;
use App\Event\InterventionBlockedEvent;
use Symfony\Component\EventDispatcher\Attribute\AsEventListener;
use Symfony\Component\Messenger\Exception\ExceptionInterface;

#[AsEventListener]
readonly class InterventionBlockedListener extends AbstractNotificationListener
{

  /**
   * @param InterventionBlockedEvent $event
   * @return void
   * @throws ExceptionInterface
   */
  public function __invoke(InterventionBlockedEvent $event): void
{
  $this->logger->debug('InterventionBlockedListener::__invoke');

  /** @var User $user */
  $user = $this->security->getUser();
  $url = $this->generateInterventionUrl($event->getIntervention());

  $users = $this->getUsers($event->getIntervention(), [Role::Director, Role::AdminEmployer]);
  $users = $this->uniqueAndExceptUsers($users, true);
  $text = 'Intervention mise en attente : ' . $event->getIntervention()->getTitle() . ' par ' . $user->getFirstname();

  $notification = new Notification();
  $notification->setIntervention($event->getIntervention());
  $notification->setTitle($text);

  $this->notificationChannelManager->sendNotification(
    $notification,
    $event,
    $url,
    $users
  );
}
}
