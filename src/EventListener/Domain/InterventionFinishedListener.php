<?php

namespace App\EventListener\Domain;

use App\Entity\Notification;
use App\Entity\Role;
use App\Entity\User;
use App\Event\InterventionFinishedEvent;
use Symfony\Component\EventDispatcher\Attribute\AsEventListener;

#[AsEventListener]
readonly class InterventionFinishedListener extends AbstractNotificationListener
{
  public function __invoke(InterventionFinishedEvent $event): void
  {
    $this->logger->debug('InterventionFinishedListener::__invoke');

    /** @var User $user */
    $user = $this->security->getUser();
    $url = $this->generateInterventionUrl($event->getIntervention());

    $users = $this->getUsers($event->getIntervention(), [Role::Director, Role::AdminEmployer]);
    $users = $this->uniqueAndExceptUsers($users, true);
    $text = 'Intervention terminée : ' . $event->getIntervention()->getTitle() . ' par ' . $user->getFirstname();

    $notification = new Notification();
    $notification->setIntervention($event->getIntervention());
    $notification->setTitle($text);

    $this->notificationChannelManager->sendNotification(
      $notification,
      $event,
      $url,
      $users
    );

    // Notification de l'auteur si c'est un demandeur
    if ($event->getIntervention()->getAuthor()->hasRole(Role::Elected)) {
      $notification = new Notification();
      $notification->setIntervention($event->getIntervention());
      $notification->setTitle('L\'intervention ' . $event->getIntervention()->getTitle() . ' est terminée');

      $this->notificationChannelManager->sendNotification(
        $notification,
        $event,
        $url,
        [$event->getIntervention()->getAuthor()]
      );
    }
  }
}
