<?php

namespace App\EventListener\Domain;

use App\Entity\User;
use App\Event\UserConnectedEvent;
use App\Event\UserConnectionFailedEvent;
use App\Repository\UserRepository;
use App\Service\AuditTrailLogger;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\Attribute\AsEventListener;

readonly class UserEventsListener
{
  public function __construct(

    private AuditTrailLogger $auditTrailLogger,
    private LoggerInterface  $logger,
    private UserRepository    $userRepository,
  )
  {
  }

  #[AsEventListener(event: UserConnectedEvent::class)]
  public function onUserConnected(UserConnectedEvent $event): void
  {
    $user = $event->user;
    $this->logger->info('User connected', ['user' => $event->user->getId()]);
    $this->auditTrailLogger->log(UserConnectedEvent::getType(), User::class, $user->getId(), ['user_id' => $user->getId(), 'mode' => $event->mode], true);
  }

  #[AsEventListener(event: UserConnectionFailedEvent::class)]
  public function onUserConnectionFailed(UserConnectionFailedEvent $event): void
  {
    // On essaie de récupérer l'utilisateur
    /** @var User|null $user */
    $user = $this->userRepository->findOneBy(['login' => $event->login]);

    $this->logger->warning("Échec de connexion pour l'utilisateur : {$event->login}");
    $this->auditTrailLogger->log(UserConnectionFailedEvent::getType(), User::class, $user?->getId(), ['login' => $event->login], true);
  }
}
