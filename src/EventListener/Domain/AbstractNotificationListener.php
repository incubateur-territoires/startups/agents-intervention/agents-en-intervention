<?php

namespace App\EventListener\Domain;

use App\Entity\Intervention;
use App\Entity\Role;
use App\Entity\User;
use App\Service\NotificationChannelManager;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

abstract readonly class AbstractNotificationListener
{
  public function __construct(
    protected LoggerInterface $logger,
    protected NotificationChannelManager $notificationChannelManager,
    protected UrlGeneratorInterface $urlGenerator,
    protected Security $security
  )
  {
  }

  /**
   * Méthode pour générer une URL pour une intervention donnée.
   */
  protected function generateInterventionUrl(Intervention $intervention, bool $editMode = false): string
  {
    return $this->urlGenerator->generate('app_dashboard_interventions', ['employer' => $intervention->getAuthor()->getEmployer()->getId()]) . ($editMode ? '#intervention-edit-' : '#intervention-') . $intervention->getId();
  }

  /**
   * Récupère les utilisateurs demandés depuis l'employer
   * de l'auteur d'une intervention AINSI QUE l'auteur lui-même
   *
   * @param Intervention $intervention
   * @param Role[] $roles
   * @return User[]
   */
  protected function getUsers(Intervention $intervention, array $roles): array
  {

    /** @var User[] $_ */
    $_ = [$intervention->getAuthor()];
    foreach ($roles as $role) {
      $list = $intervention->getAuthor()->getEmployer()->getUsers()->filter(
        fn(mixed $u) => $u instanceof User && $u->hasRole($role)
      );

      foreach ($list as $u) {
        $_[] = $u;
      }
    }

    return $_;
  }

  /**
   * @param User[]|array $users
   * @return User[]
   */
  protected function uniqueAndExceptUsers(array $users, bool $exceptCurrentUser = false): array
  {
    $uniqueUsers = [];

    /** @var User|null $currentUser */
    $currentUser = $this->security->getUser();

    foreach ($users as $user) {
      if (!array_key_exists($user->getId() ?? '-1', $uniqueUsers) && (!$exceptCurrentUser || $user->getId() !== $currentUser?->getId())) {
        $uniqueUsers[$user->getId()] = $user;
      }
    }

    return array_values($uniqueUsers);
  }
}
