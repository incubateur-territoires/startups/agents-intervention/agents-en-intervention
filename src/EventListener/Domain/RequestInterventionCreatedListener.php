<?php

namespace App\EventListener\Domain;

use App\Entity\Notification;
use App\Entity\Role;
use App\Event\RequestInterventionCreatedEvent;
use Symfony\Component\EventDispatcher\Attribute\AsEventListener;

#[AsEventListener]
readonly class RequestInterventionCreatedListener extends AbstractNotificationListener
{
  public function __invoke(RequestInterventionCreatedEvent $event): void
  {
    $intervention = $event->getIntervention();

    $users = $this->getUsers($intervention, [Role::Director, Role::AdminEmployer]);

    // Utilise la méthode commune pour générer l'URL
    $url = $this->generateInterventionUrl($intervention, true);

    $notification = new Notification();
    $notification->setIntervention($intervention);
    $notification->setTitle('Nouvelle demande d\'intervention : ' . $intervention->getTitle());

    $this->notificationChannelManager->sendNotification(
      $notification,
      $event,
      $url,
      $users
    );
  }

}
