<?php

namespace App\EventListener\Domain;

use App\Entity\Notification;
use App\Entity\Role;
use App\Entity\User;
use App\Event\InterventionRejectedEvent;
use Symfony\Component\EventDispatcher\Attribute\AsEventListener;

#[AsEventListener]
readonly class InterventionRejectedListener extends AbstractNotificationListener
{
  public function __invoke(InterventionRejectedEvent $event): void
  {
    $this->logger->debug('InterventionRejectedListener::__invoke');

    /** @var User $user */
    $user = $this->security->getUser();
    $url = $this->generateInterventionUrl($event->getIntervention());

    $users = array_merge(
      [$event->getIntervention()->getAuthor()],
      $this->getUsers($event->getIntervention(), [Role::Director, Role::AdminEmployer]),
    );
    $users = $this->uniqueAndExceptUsers($users, true);
    $text = "Votre demande d'intervention a été refusée";
    $detail = "L'intervention " . $event->getIntervention()->getTitle() . " demandée le " . $event->getIntervention()->getCreatedAt()->format('d/m/Y') . " a été refusée";

    $notification = new Notification();
    $notification->setIntervention($event->getIntervention());
    $notification->setTitle($text);
    $notification->setMessage($detail);

    $this->logger->info('NotificationService::sendInterventionRejetee', ['intervention' => $event->getIntervention(), 'userIds' => array_map(fn($u) => $u->getId(), $users), 'text' => $text, 'detail' => $detail]);
    $this->notificationChannelManager->sendNotification(
      $notification,
      $event,
      $url,
      $users
    );
  }
}
