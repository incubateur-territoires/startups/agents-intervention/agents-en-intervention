<?php

namespace App\EventListener\Domain;

use App\Bridge\Mattermost\Message;
use App\Entity\User;
use App\Event\EmployerCreatedEvent;
use App\Repository\EmployerRepository;
use NotFloran\MjmlBundle\Renderer\RendererInterface;
use Symfony\Component\EventDispatcher\Attribute\AsEventListener;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Messenger\Exception\ExceptionInterface;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Twig\Environment;

#[AsEventListener]
readonly class EmployerCreatedListener
{
  public function __construct(
    private Environment           $twig,
    private EmployerRepository    $employerRepository,
    private MailerInterface       $mailer,
    private MessageBusInterface   $messageBus,
    private RendererInterface     $mjml,
    private UrlGeneratorInterface $urlGenerator
  )
  {
  }

  /**
   * @param EmployerCreatedEvent $event
   * @return void
   * @throws ExceptionInterface
   * @throws TransportExceptionInterface
   */
  public function __invoke(EmployerCreatedEvent $event): void
  {

    /**
     * Dans le cas d'une création autonome, on envoie une notification à l'équipe support.
     */
    if (!$event->createdByAdmin) {
      /** @var ?User $admin */
      $admin = $event->employer->getUsers()[0];
      if (!$admin) {
        return;
      }

      $anct = $this->employerRepository->getAnct();
      assert($anct !== null);

      $messageTpl = <<<MATTERMOST
:bell: :bell: :bell:

Nouvelle collectivité créée : [%s - %s](https://annuaire-entreprises.data.gouv.fr/entreprise/%s)
Créée par %s - %s

Voir [les informations de la collectivité](%s)
MATTERMOST;

      $message = sprintf(
        $messageTpl,
        $event->employer->getName(),
        $event->employer->getSiren(),
        $event->employer->getSiren(),
        $admin->getFullname(),
        $admin->getEmail(),
        $this->urlGenerator->generate(
          'app_employer_show',
          ['employer' => $anct->getId(), 'id' => $event->employer->getId()],
          UrlGeneratorInterface::ABSOLUTE_URL
        ),
      );

      // Envoi du message vers Mattermost
      $this->messageBus->dispatch(new Message($message));

      $mjmlBody = $this->twig->render(
        'emails/nouvelle-collectivite-mail-support.mjml.twig',
        ['anct' => $anct, 'user' => $admin, 'employer' => $event->employer]
      );
      $htmlBody = $this->mjml->render($mjmlBody);

      $tos = [];
      foreach ($anct->getUsers() as $user) {
        $email = $user->getEmail();
        if ($user->isActive() && $email !== null && $email !== '') {
          $tos[] = $email;
        }
      }

      if (!empty($tos)) {
        // Envoi du message par email
        $email = (new Email())
          ->to(...$tos)
          ->subject('La collectivité ' . $event->employer->getName() . ' s\'est inscrite sur Agents en Intervention')
          ->text('Veuillez utiliser un client email capable de visualiser le HTML')
          ->html($htmlBody);

        $this->mailer->send($email);
      }

      /**
       * Envoi d'un mail de bienvenue à l'administrateur de la collectivité.
       */

      if ($admin->getEmail() === null) {
        return;
      }

      $mjmlBody = $this->twig->render(
        'emails/nouvelle-collectivite-mail-admin.mjml.twig'
      );
      $htmlBody = $this->mjml->render($mjmlBody);

      $email = (new Email())
        ->to($admin->getEmail())
        ->subject('Bienvenue sur Agents en Intervention')
        ->text('Veuillez utiliser un client email capable de visualiser le HTML')
        ->html($htmlBody);
      $this->mailer->send($email);
    }
  }
}
