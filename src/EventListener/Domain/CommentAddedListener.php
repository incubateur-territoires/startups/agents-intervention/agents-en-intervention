<?php

namespace App\EventListener\Domain;

use App\Entity\Notification;
use App\Entity\Role;
use App\Entity\User;
use App\Event\CommentAddedEvent;
use Symfony\Component\EventDispatcher\Attribute\AsEventListener;

#[AsEventListener]
readonly class CommentAddedListener extends AbstractNotificationListener
{
  public function __invoke(CommentAddedEvent $event): void
  {
    $user = $event->getComment()->getAuthor();
    $comment = $event->getComment();

    // Utilise la méthode commune pour générer l'URL du commentaire
    $url = $this->generateInterventionUrl($comment->getIntervention());

    $notification = new Notification();
    $notification->setUser($user);
    $notification->setIntervention($comment->getIntervention());
    $notification->setTitle('Nouveau commentaire de ' . $user->getFirstname() . ' sur ' . $comment->getIntervention()->getTitle());
    $notification->setMessage($comment->getMessage());

    /** @var User[] $users */
    $users = array_merge(
      $this->getUsers($comment->getIntervention(), [Role::Director, Role::AdminEmployer]),
      $comment->getIntervention()->getParticipants()->toArray()
    );
    $users = $this->uniqueAndExceptUsers($users, true);

    // Envoi de la notification avec l'URL du commentaire
    $this->notificationChannelManager->sendNotification(
      $notification,
      $event,
      $url,
      $users
    );
  }
}
