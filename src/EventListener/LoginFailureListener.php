<?php

namespace App\EventListener;

use App\Event\UserConnectionFailedEvent;
use Psr\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\EventDispatcher\Attribute\AsEventListener;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\UserBadge;
use Symfony\Component\Security\Http\Event\LoginFailureEvent;
use Psr\Log\LoggerInterface;

readonly class LoginFailureListener
{
  public function __construct(
    private EventDispatcherInterface $eventDispatcher,
    private LoggerInterface          $logger,
  )
  {
  }

  #[AsEventListener(event: LoginFailureEvent::class, priority: 10)]
  public function onLoginFailure(LoginFailureEvent $event): void
  {
    $passport = $event->getPassport();
    if ($passport === null) {
      $this->logger->warning('Échec de connexion sans badge');
      return;
    }

    $login = $passport->getBadge(UserBadge::class)?->getUserIdentifier();

    if ($login) {
      $this->logger->warning("Échec de connexion pour l'utilisateur : $login");
      $this->eventDispatcher->dispatch(new UserConnectionFailedEvent($login));
    }
  }
}
