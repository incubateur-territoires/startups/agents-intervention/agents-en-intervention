<?php

namespace App\EventListener\Doctrine;

use App\Entity\ActionLoggableInterface;
use App\Service\AuditTrailLogger;
use Doctrine\Bundle\DoctrineBundle\Attribute\AsDoctrineListener;
use Doctrine\ORM\Event\OnFlushEventArgs;
use Doctrine\ORM\Events;
use Doctrine\Persistence\Mapping\MappingException;
use Psr\Log\LoggerInterface;
use ReflectionException;

#[AsDoctrineListener(event: Events::onFlush)]
readonly class DoctrineAuditTrailListener
{
  public function __construct(
    private AuditTrailLogger $auditTrailLogger,
    private LoggerInterface  $logger,
  )
  {
  }

  /**
   * @param OnFlushEventArgs $args
   * @return void
   * @throws MappingException
   * @throws ReflectionException
   */
  public function __invoke(OnFlushEventArgs $args): void
  {
    $uow = $args->getObjectManager()->getUnitOfWork();

    // Traiter les entités insérées
    foreach ($uow->getScheduledEntityInsertions() as $entity) {
      $this->logAction($args,'entity_create', $entity);
    }

    // Traiter les entités mises à jour
    foreach ($uow->getScheduledEntityUpdates() as $entity) {
      $this->logger->debug('entity_update', ['entity' => $entity]);
      $changeSet = $uow->getEntityChangeSet($entity);

      // Capture uniquement les noms des champs modifiés
//      $fieldsChanged = array_keys($changeSet);
      $this->logAction($args,'entity_update', $entity, $changeSet);
    }

    // Traiter les entités supprimées
    foreach ($uow->getScheduledEntityDeletions() as $entity) {
      $this->logAction($args,'entity_delete', $entity);
    }

    foreach ($uow->getScheduledCollectionDeletions() as $col) {
      foreach($col->getIterator() as $entity) {
        $this->logAction($args,'entity_delete', $entity);
      }
    }

    foreach ($uow->getScheduledCollectionUpdates() as $col) {
      foreach($col->getIterator() as $entity) {
        $this->logger->debug('entity_update', ['entity' => $entity]);
        $changeSet = $uow->getEntityChangeSet($entity);

        // Capture uniquement les noms des champs modifiés
        $fieldsChanged = array_keys($changeSet);
        $this->logAction($args,'entity_update', $entity, $fieldsChanged);
      }
    }
  }

  /**
   * @param OnFlushEventArgs $args
   * @param string $action
   * @param object $entity
   * @param array $changes
   * @return void
   * @throws MappingException
   * @throws ReflectionException
   */
  private function logAction(OnFlushEventArgs $args, string $action, object $entity, array $changes = []): void
  {
    // Ignorer les entités non traçables
    if (!$entity instanceof ActionLoggableInterface || !method_exists($entity, 'getId')) {
      $this->logger->debug('SKIP Logging action ' . $action . ' for entity ' . $entity::class . (method_exists($entity, 'getId') ? ' with ID ' . $entity->getId() : ''));
      return;
    }
    $this->logger->debug('Logging action ' . $action . ' for entity ' . $entity::class . ' with ID ' . $entity->getId());

    $_changes = ['entity' => $entity::class, 'changes' => $changes];

    $log = $this->auditTrailLogger->log(
      $action,
      $entity::class,
      $entity->getId(),
      $_changes
    );

    $om = $args->getObjectManager();
    $uow = $om->getUnitOfWork();
    $uow->computeChangeSet($om->getClassMetadata($log::class), $log);
  }
}

// FIXME : Comment récupérer l'identifiant des objects pas encore crées en base ?
