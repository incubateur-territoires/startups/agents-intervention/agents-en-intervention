<?php

// php
namespace App\EventListener\Doctrine;

use App\Entity\Intervention;
use App\Event\InterventionCreatedEvent;
use App\Event\InterventionType;
use App\Event\RecurringInterventionCreatedEvent;
use App\Event\RequestInterventionCreatedEvent;
use App\Event\SimpleInterventionCreatedEvent;
use App\Service\RecurrenceService;
use Doctrine\Bundle\DoctrineBundle\Attribute\AsEntityListener;
use Doctrine\ORM\Event\PostPersistEventArgs;
use Doctrine\ORM\Events;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

/**
 * Ce listener permet de dispatcher
 * automatiquement les événements métier
 * liés à la création d'une intervention
 * de type particulier et ainsi éviter à chaque
 * listener de faire le contrôle de type.
 */
#[AsEntityListener(event: Events::postPersist, entity: Intervention::class)]
readonly class InterventionCreatedListener
{
  public function __construct(
    private EventDispatcherInterface $eventDispatcher,
    private RecurrenceService        $recurrenceService
  )
  {
  }

  public function __invoke(Intervention $intervention): void
  {
    // Si ce n'est pas une intervention générée,
    // on émet l'événement général de création d'une intervention
    if (!$intervention->hasRecurrenceReference()) {
      $this->eventDispatcher->dispatch(new InterventionCreatedEvent($intervention));
    }

    /*
     * Ensuite, on envoie un événement spécifique par typologie
     * d'intervention pour les listener ayant besoin de cette
     * granularité.
     */
    switch ($intervention->getInterventionType()) {
      case InterventionType::Request:
        $this->eventDispatcher->dispatch(new RequestInterventionCreatedEvent($intervention));
        break;
      case InterventionType::Recurring:
        $this->recurrenceService->addGeneratedInterventionsFor($intervention);
        $this->eventDispatcher->dispatch(new RecurringInterventionCreatedEvent($intervention));
        break;
      case InterventionType::Simple:
        if (!$intervention->hasRecurrenceReference()) {
          $this->eventDispatcher->dispatch(new SimpleInterventionCreatedEvent($intervention));
        }
        break;
      case InterventionType::Generated:
        // On ne fait rien pour les interventions générées
        break;
      case InterventionType::Unknown:
        throw new \LogicException('Type d\'intervention inconnu');
    }
  }
}
