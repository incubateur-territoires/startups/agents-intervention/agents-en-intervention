<?php

namespace App\EventListener\Doctrine;

use App\Entity\Picture;
use App\Entity\PictureTag;
use App\Service\S3Service;
use Doctrine\Bundle\DoctrineBundle\Attribute\AsEntityListener;
use Doctrine\ORM\Events;

#[AsEntityListener(event: Events::postLoad, entity: Picture::class)]
readonly class PicturePostLoadListener
{
  public function __construct(
    private S3Service $s3
  )
  {
  }

  public function __invoke(Picture $picture): void
  {
    $picture->setUrl($this->s3->getSignedURL($picture->getFilename()));
    if ($picture->getTag() === PictureTag::Avatar) {
      $picture->setUrls([
        'small' => $this->s3->getSignedURL('users/small/' . pathinfo($picture->getFilename(), PATHINFO_FILENAME) . '.png'),
        'original' => $this->s3->getSignedURL('users/original/' . pathinfo($picture->getFilename(), PATHINFO_FILENAME) . '.png'),
        'medium' => $this->s3->getSignedURL('users/medium/' . pathinfo($picture->getFilename(), PATHINFO_FILENAME) . '.png'),
      ]);
    } else {
      $picture->setUrls([
        'small' => $this->s3->getSignedURL('interventions/small/' . pathinfo($picture->getFilename(), PATHINFO_FILENAME) . '.png'),
        'original' => $this->s3->getSignedURL('interventions/original/' . pathinfo($picture->getFilename(), PATHINFO_FILENAME) . '.png'),
        'medium' => $this->s3->getSignedURL('interventions/medium/' . pathinfo($picture->getFilename(), PATHINFO_FILENAME) . '.png'),
      ]);
    }
  }

}
