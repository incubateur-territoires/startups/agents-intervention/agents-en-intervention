<?php

namespace App\EventListener\Doctrine;

use App\Entity\Intervention;
use App\Service\RecurrenceService;
use Doctrine\Bundle\DoctrineBundle\Attribute\AsEntityListener;
use Doctrine\ORM\Event\PreRemoveEventArgs;
use Doctrine\ORM\Events;
use Exception;

#[AsEntityListener(event: Events::preRemove, method: 'preRemove', entity: Intervention::class)]
readonly class InterventionRemovedListener
{
  public function __construct(
    private RecurrenceService $recurrenceService
  )
  {
  }

  /**
   * @throws Exception
   */
  public function preRemove(Intervention $intervention, PreRemoveEventArgs $event): void
  {
    if ($intervention->isRecurring()) {
      $this->recurrenceService->removeGeneratedInterventionsFor($intervention);
    }
  }
}
