<?php

namespace App\EventListener\Doctrine;

use App\Entity\Picture;
use App\Entity\PictureTag;
use App\Service\S3Service;
use Doctrine\Bundle\DoctrineBundle\Attribute\AsEntityListener;
use Doctrine\ORM\Events;
use Imagine\Filter\Basic\Autorotate;
use Imagine\Gd\Imagine;
use Imagine\Image\Box;
use Imagine\Image\ManipulatorInterface;
use Psr\Log\LoggerInterface;

#[
  AsEntityListener(event: Events::postPersist, entity: Picture::class),
  AsEntityListener(event: Events::postUpdate, entity: Picture::class),
]
readonly class PicturePostCreateListener
{
  public const int SMALL_SIZE = 40;
  public const int MEDIUM_SIZE = 150;
  public const int SMALL_QUALITY = 50;
  public const int MEDIUM_QUALITY = 75;

  public function __construct(
    private LoggerInterface $logger,
    private S3Service       $s3,
  )
  {
  }

  public function __invoke(Picture $picture): void
  {
    if (getenv('APP_ENV') === 'test') {
      return;
    }

    /*
     * Création et upload des images optimisées
     */
    $file = $this->s3->getFile($picture->getFileName());

    $imagine = new Imagine();
    if (!is_string($file)) {
      return;
    }
    $this->logger->debug('PicturePostCreateListener :: file: ' . $file);
    $image = $imagine->load($file);
    (new Autorotate())->apply($image);

    $info = pathinfo($picture->getFileName());

    $prefix = $picture->getTag() === PictureTag::Avatar ? 'users' : 'interventions';

    $this->s3->putFileContent(
      $image->get('png', ['quality' => 100]),
      $prefix . '/original/' . $info['filename'] . '.png'
    );

    $size = new Box(PicturePostCreateListener::SMALL_SIZE, PicturePostCreateListener::SMALL_SIZE);

    $this->s3->putFileContent(
      $image->thumbnail($size, ManipulatorInterface::THUMBNAIL_OUTBOUND)
        ->get('png', ['quality' => PicturePostCreateListener::SMALL_QUALITY]),
      $prefix . '/small/' . $info['filename'] . '.png'
    );

    $size = new Box(self::MEDIUM_SIZE, PicturePostCreateListener::MEDIUM_SIZE);
    $this->s3->putFileContent(
      $image->thumbnail($size, ManipulatorInterface::THUMBNAIL_OUTBOUND)
        ->get('png', ['quality' => PicturePostCreateListener::MEDIUM_QUALITY]),
      $prefix . '/medium/' . $info['filename'] . '.png'
    );
  }
}
