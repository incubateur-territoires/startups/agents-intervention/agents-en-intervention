<?php

namespace App\EventListener\Doctrine;

use App\Entity\Comment;
use App\Event\CommentAddedEvent;
use Doctrine\Bundle\DoctrineBundle\Attribute\AsEntityListener;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

#[AsEntityListener(event: 'postPersist', entity: Comment::class)]
readonly class CommentListener
{
  public function __construct(private EventDispatcherInterface $eventDispatcher) {}

  public function __invoke(Comment $comment): void
  {
    $this->eventDispatcher->dispatch(new CommentAddedEvent($comment));
  }
}
