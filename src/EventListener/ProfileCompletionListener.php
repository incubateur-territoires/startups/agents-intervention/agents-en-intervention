<?php

namespace App\EventListener;

use App\Entity\User;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\EventDispatcher\Attribute\AsEventListener;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\Routing\RouterInterface;

#[AsEventListener(event: 'kernel.request', method: 'onKernelRequest')]
readonly class ProfileCompletionListener
{
  public function __construct(private Security $security, private RouterInterface $router)
  {
  }

  public function onKernelRequest(RequestEvent $event): void
  {

    /** @var User|null $user */
    $user = $this->security->getUser();

    // Vérifier si l'utilisateur existe et si le profil est incomplet
    if ($user && !$user->isComplete()) {
      $currentRoute = $event->getRequest()->attributes->get('_route');

      // Éviter les boucles de redirection si on est déjà sur la page de complétion
      if ($event->getRequest()->attributes->has('_route') && $currentRoute !== 'app_profile_completion') {
        $event->setResponse(new RedirectResponse($this->router->generate('app_profile_completion')));
      }
    }
  }
}
