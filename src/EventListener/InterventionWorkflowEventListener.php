<?php

namespace App\EventListener;

use App\Entity\Intervention;
use App\Event\InterventionBlockedEvent;
use App\Event\InterventionFinishedEvent;
use App\Event\InterventionRejectedEvent;
use App\Event\RequestInterventionCreatedEvent;
use App\Repository\InterventionRepository;
use Psr\EventDispatcher\EventDispatcherInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Workflow\Attribute\AsCompletedListener;
use Symfony\Component\Workflow\Event\CompletedEvent;

readonly class InterventionWorkflowEventListener
{
  public function __construct(
    private LoggerInterface          $logger,
    private EventDispatcherInterface $eventDispatcher,
    private InterventionRepository   $interventionRepository,
  )
  {
  }

  #[AsCompletedListener(workflow: 'intervention_publishing', transition: 'init_to_validate')]
  public function onRequestCreated(CompletedEvent $event): void
  {
    $this->logger->info('InterventionWorkflowEventListener::onRequestCreated');

    $intervention = $event->getSubject();
    \assert($intervention instanceof Intervention);

//    $this->notificationService->sendNouvelleDemande($intervention);
//    $this->eventDispatcher->dispatch(new RequestInterventionCreatedEvent($intervention), 'domain.RequestInterventionCreatedEvent');
    $this->eventDispatcher->dispatch(new RequestInterventionCreatedEvent($intervention));
  }

  #[AsCompletedListener(workflow: 'intervention_publishing', transition: 'pause')]
  public function onInterventionBlocked(CompletedEvent $event): void
  {
    $this->logger->info('InterventionWorkflowEventListener::onInterventionBlocked');

    $intervention = $event->getSubject();
    \assert($intervention instanceof Intervention);

    /** @var Intervention|null $doctrineIntervention */
    $doctrineIntervention = $this->interventionRepository->find($intervention->getId());
    if ($doctrineIntervention) {
//      $this->notificationService->sendInterventionBloquee($doctrineIntervention);
      $this->eventDispatcher->dispatch(new InterventionBlockedEvent($doctrineIntervention));
    }
  }

  #[AsCompletedListener(workflow: 'intervention_publishing', transition: 'finished')]
  public function onInterventionFinished(CompletedEvent $event): void
  {
    $this->logger->info('InterventionWorkflowEventListener::onInterventionFinished');

    $intervention = $event->getSubject();
    \assert($intervention instanceof Intervention);

    /** @var Intervention|null $doctrineIntervention */
    $doctrineIntervention = $this->interventionRepository->find($intervention->getId());
    if ($doctrineIntervention) {
//      $this->notificationService->sendInterventionTerminee($doctrineIntervention);
      $this->eventDispatcher->dispatch(new InterventionFinishedEvent($doctrineIntervention));
    }
  }

  #[AsCompletedListener(workflow: 'intervention_publishing', transition: 'to_one_time')]
  public function onInterventionChangeToOneTime(CompletedEvent $event): void
  {
    $this->logger->info('InterventionWorkflowEventListener::onInterventionChangeToOneTime, setting endedAt to null');

    $intervention = $event->getSubject();
    \assert($intervention instanceof Intervention);

    $intervention->setEndedAt(null);
  }

  #[AsCompletedListener(workflow: 'intervention_publishing', transition: 'reject')]
  public function onInterventionRejected(CompletedEvent $event): void
  {
    $this->logger->info('InterventionWorkflowEventListener::onInterventionRejected, setting endedAt to null');

    $intervention = $event->getSubject();
    \assert($intervention instanceof Intervention);

//    $this->notificationService->sendInterventionRejetee($intervention);
    $this->eventDispatcher->dispatch(new InterventionRejectedEvent($intervention));
  }
}
