<?php

namespace App\Validator;

use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\ConstraintViolationListInterface;

/**
 * Interface pour les validateurs d’événements de la piste d'audit
 */
abstract class AbstractEventValidator implements EventValidatorInterface
{
  public function __construct(protected ValidatorInterface $validator)
  {
  }

  /**
   * Retourne les contraintes spécifiques au validateur
   */
  abstract protected function getConstraints(): Collection;

  /**
   * Retourne le type d’événement supporté
   */
  abstract public function getEventType(): string;

  /**
   * Transforme les données validées en données finales (peut être surchargée)
   * @param array<string, mixed> $data
   * @return array<string, mixed>
   */
  protected function transformValidatedData(array $data): array
  {
    return $data;
  }

  /**
   * Vérifie si ce validateur supporte le type donné
   */
  public function supports(string $type): bool
  {
    return $type === $this->getEventType();
  }

  /**
   * Valide les données et retourne le résultat
   *
   * @param array<string, mixed> $data
   * @return array<string, mixed>
   */
  public function validate(array $data): array
  {
    $violations = $this->validator->validate($data, $this->getConstraints());

    if (count($violations) > 0) {
      return $this->handleValidationErrors($violations, $data);
    }

    return $this->generateEventData($this->transformValidatedData($data));
  }

  /**
   * Gère les erreurs de validation et retourne un tableau d’erreurs
   *
   * @param ConstraintViolationListInterface $violations
   * @param array<string, mixed> $rawData
   * @return array<string, mixed>
   */
  protected function handleValidationErrors(
    ConstraintViolationListInterface $violations,
    array                            $rawData
  ): array
  {
    $errors = [];
    foreach ($violations as $violation) {
      $errors[$violation->getPropertyPath()] = $violation->getMessage();
    }

    $validData = array_diff_key($rawData, $errors);

    return [
      'valid' => false,
      'errors' => $errors,
      'raw' => $rawData,
      'data' => $validData,
    ];
  }

  /**
   * Génère un tableau de données finales
   *
   * @param array<string, mixed> $validatedData
   * @return array<string, mixed>
   */
  protected function generateEventData(array $validatedData): array
  {
    return [
      'valid' => true,
      'data' => $validatedData,
    ];
  }
}
