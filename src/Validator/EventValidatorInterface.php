<?php

namespace App\Validator;

interface EventValidatorInterface
{
  public function supports(string $type): bool;

  /**
   * @param array<string, mixed> $data
   * @return array<string, mixed>
   */
  public function validate(array $data): array;

  public function getEventType(): string;
}
