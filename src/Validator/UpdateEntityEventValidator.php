<?php

namespace App\Validator;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Constraints\Collection;

class UpdateEntityEventValidator extends AbstractEventValidator
{
  protected function getConstraints(): Collection
  {
    return new Collection([
      'entity' => new Assert\NotBlank(),
      'changes' => new Assert\Type('array'),
//      'user' => new Assert\Optional(new Assert\Type('string')),
    ]);
  }

  public function getEventType(): string
  {
    return 'entity_update';
  }

  /**
   * Transforme les données validées en données finales
   * @param array<string, mixed> $data
   * @return array<string, mixed>
   */
  #[\Override]
  protected function transformValidatedData(array $data): array
  {
    if (isset($data['changes'])) {
      $data['changes'] = [
        'fields_changed' => array_keys($data['changes'])
      ];
    }

    return $data;
  }
}
