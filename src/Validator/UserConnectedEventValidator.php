<?php

namespace App\Validator;

use App\Event\ConnexionMode;
use App\Event\UserConnectedEvent;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Constraints as Assert;

class UserConnectedEventValidator extends AbstractEventValidator
{
  /**
   * @inheritDoc
   */
  protected function getConstraints(): Collection
  {
    return new Collection([
      'user_id' => [
        new Assert\Type('integer'),
        new Assert\Positive(),
      ],
      'mode' => [
        new Assert\Type(type: ConnexionMode::class),
      ]
    ]);
  }

  /**
   * @inheritDoc
   */
  public function getEventType(): string
  {
    return UserConnectedEvent::getType();
  }
}
