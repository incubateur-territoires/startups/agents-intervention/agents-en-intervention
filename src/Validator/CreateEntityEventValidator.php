<?php

namespace App\Validator;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Constraints\Collection;

class CreateEntityEventValidator extends AbstractEventValidator
{
  protected function getConstraints(): Collection
  {
    return new Collection([
      'entity' => new Assert\NotBlank(),
      'changes' => new Assert\Optional(new Assert\Type('array')),
//      'user' => new Assert\Optional(new Assert\Type('string')),
    ]);
  }

  public function getEventType(): string
  {
    return 'entity_create';
  }
}
