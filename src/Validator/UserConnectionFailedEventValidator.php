<?php

namespace App\Validator;

use App\Event\UserConnectionFailedEvent;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Constraints as Assert;

class UserConnectionFailedEventValidator extends AbstractEventValidator
{

  protected function getConstraints(): Collection
  {
    return new Collection([
      'login' => [
        new Assert\NotBlank(),
        new Assert\Type('string'),
      ],
    ]);
  }

  public function getEventType(): string
  {
    return UserConnectionFailedEvent::getType();
  }
}
