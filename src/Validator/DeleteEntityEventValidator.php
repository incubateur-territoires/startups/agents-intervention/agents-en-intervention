<?php

namespace App\Validator;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Constraints\Collection;

class DeleteEntityEventValidator extends AbstractEventValidator
{
  protected function getConstraints(): Collection
  {
    return new Collection([
      'entity' => new Assert\NotBlank(),
    ]);
  }

  public function getEventType(): string
  {
    return 'entity_delete';
  }
}
