<?php

namespace App\Validator;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Constraints\Collection;

class WorkflowDeniedEventValidator extends AbstractEventValidator
{
  protected function getConstraints(): Collection
  {
    return new Collection([
      'workflow' => new Assert\NotBlank(),
      'transition' => new Assert\NotBlank(),
      'from' => new Assert\Type('string'),
      'to' => new Assert\NotBlank(),
      'comment' => new Assert\Optional(new Assert\Type('string')),
    ]);
  }

  public function getEventType(): string
  {
    return 'workflow_denied';
  }
}
