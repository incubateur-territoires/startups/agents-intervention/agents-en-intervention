<?php

namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class UserRoleConstraintValidator extends ConstraintValidator
{
    #[\Override]
    public function validate(mixed $value, Constraint $constraint): void
    {
        if(!$constraint instanceof UserRoleConstraint) {
          throw new \LogicException('UserRoleConstraintValidator :: can process only UserRoleConstraint');
        }

        if (!is_array($value) || count($value) !== 1) {
            $this->context->buildViolation($constraint->message)->addViolation();
        }
    }
}
