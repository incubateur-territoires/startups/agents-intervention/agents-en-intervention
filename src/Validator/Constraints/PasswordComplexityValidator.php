<?php

namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class PasswordComplexityValidator extends ConstraintValidator
{
  public function validate(mixed $value, Constraint $constraint): void
  {
    if (is_string($value) === false || $constraint instanceof PasswordComplexity === false) {
      return;
    }

    if (strlen($value) < 10 ||
      !preg_match('/[A-Z]/', $value) ||
      !preg_match('/[a-z]/', $value) ||
      !preg_match('/\d/', $value) ||
      !preg_match('/[^A-Za-z0-9]/', $value)) {
      $this->context->buildViolation($constraint->message)
        ->addViolation();
    }
  }
}
