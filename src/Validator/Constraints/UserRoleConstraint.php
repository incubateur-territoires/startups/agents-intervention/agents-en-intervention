<?php

namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

#[\Attribute]
class UserRoleConstraint extends Constraint
{
    public string $message = 'The user must have one and only one role';
}
