<?php

declare(strict_types=1);

namespace App\Doctrine;

use ApiPlatform\Doctrine\Orm\Extension\QueryCollectionExtensionInterface;
use ApiPlatform\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use ApiPlatform\Metadata\Operation;
use App\Entity\User;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

final readonly class CurrentAgentInterventionsExtension implements QueryCollectionExtensionInterface
{
    public function __construct(private Security $security)
    {
    }

    #[\Override]
    public function applyToCollection(
        QueryBuilder $queryBuilder,
        QueryNameGeneratorInterface $queryNameGenerator,
        string $resourceClass,
        Operation|null $operation = null,
        array $context = []
    ): void {
        if ($operation?->getUriTemplate() === '/users/{userId}/interventions') {
            /** @var User $user */
            $user = $this->security->getUser();
            if ((int) $context['uri_variables']['userId'] !== $user->getId()) {
                throw new AccessDeniedException();
            }

            $this->addWhere($queryBuilder, $user->getId());
        }
    }

    private function addWhere(QueryBuilder $queryBuilder, int $userId): void
    {
        $interventionAlias = $queryBuilder->getRootAliases()[0];
        $queryBuilder->andWhere(':userId MEMBER OF ' . $interventionAlias . '.participants');
        $queryBuilder->setParameter('userId', $userId);
    }
}
