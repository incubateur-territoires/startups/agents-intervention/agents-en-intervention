<?php

namespace App\Doctrine;

use Doctrine\ORM\Query\AST\ASTException;
use Doctrine\ORM\Query\AST\Functions\FunctionNode;
use Doctrine\ORM\Query\AST\Node;
use Doctrine\ORM\Query\AST\Subselect;
use Doctrine\ORM\Query\Parser;
use Doctrine\ORM\Query\QueryException;
use Doctrine\ORM\Query\SqlWalker;
use Doctrine\ORM\Query\TokenType;
use Exception;

class JsonbExists extends FunctionNode
{
  public Subselect|Node|string $jsonbExpression;
  public Subselect|Node|string $key;

  /**
   * @throws QueryException
   */
  #[\Override]
  public function parse(Parser $parser): void
  {
    $parser->match(TokenType::T_IDENTIFIER);
    $parser->match(TokenType::T_OPEN_PARENTHESIS);
    $this->jsonbExpression = $parser->StringExpression();
    $parser->match(TokenType::T_INPUT_PARAMETER);
    $parser->match(TokenType::T_INPUT_PARAMETER);
    $parser->match(TokenType::T_COMMA);
    $this->key = $parser->StringExpression();
    $parser->match(TokenType::T_CLOSE_PARENTHESIS);
  }

  /**
   * @throws ASTException
   * @throws Exception
   */
  #[\Override]
  public function getSql(SqlWalker $sqlWalker): string
  {
//    dd($this->key, $this->jsonbExpression, isset($this->key->value) && is_string($this->key->value) && !is_string($this->jsonbExpression), isset($this->key->value) , is_string($this->key->value) ,!is_string($this->jsonbExpression));
    if (isset($this->key->value) && is_string($this->key->value) && !is_string($this->jsonbExpression)) {
      return 'jsonb_exists(' . $this->jsonbExpression->dispatch($sqlWalker) . '::jsonb, \'' . $this->key->value . '\')';
    }
    throw new Exception('JsonbExists :: generation failed');
  }
}
