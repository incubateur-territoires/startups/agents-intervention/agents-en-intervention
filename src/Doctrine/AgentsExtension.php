<?php

declare(strict_types=1);

namespace App\Doctrine;

use ApiPlatform\Doctrine\Orm\Extension\QueryCollectionExtensionInterface;
use ApiPlatform\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Operation;
use App\Entity\Role;
use App\Entity\User;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

final readonly class AgentsExtension implements QueryCollectionExtensionInterface
{
    public function __construct(private Security $security)
    {
    }

    #[\Override]
    public function applyToCollection(
        QueryBuilder $queryBuilder,
        QueryNameGeneratorInterface $queryNameGenerator,
        string $resourceClass,
        Operation|null $operation = null,
        array $context = []
    ): void {
        if ($operation instanceof GetCollection && $operation->getUriTemplate() === '/employers/{employerId}/users') {
            /** @var User $user */
            $user = $this->security->getUser();
            $employer = $user->getEmployer();

            if ($user->hasRole(Role::Admin)) {
                return;
            }

            if (intval($context['uri_variables']['employerId']) !== $employer->getId()) {
                throw new AccessDeniedException();
            }
        }
    }
}
