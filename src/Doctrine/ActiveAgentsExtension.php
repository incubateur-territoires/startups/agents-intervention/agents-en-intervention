<?php

declare(strict_types=1);

namespace App\Doctrine;

use ApiPlatform\Doctrine\Orm\Extension\QueryCollectionExtensionInterface;
use ApiPlatform\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Operation;
use App\Entity\Role;
use App\Entity\User;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

final readonly class ActiveAgentsExtension implements QueryCollectionExtensionInterface
{
  public function __construct(private Security $security)
  {
  }

  #[\Override]
  public function applyToCollection(
    QueryBuilder                $queryBuilder,
    QueryNameGeneratorInterface $queryNameGenerator,
    string                      $resourceClass,
    Operation|null              $operation = null,
    array                       $context = []
  ): void
  {
    if ($operation instanceof GetCollection && $operation->getUriTemplate() === '/employers/{employerId}/active-agents') {
      /** @var User $user */
      $user = $this->security->getUser();
      $employer = $user->getEmployer();
      $employerId = intval($context['uri_variables']['employerId']);
      if (!$user->hasRole(Role::Admin) && (int)$employerId !== $employer->getId()) {
        throw new AccessDeniedException();
      }

      $this->addWhere($queryBuilder, $employerId);
    }
  }

  private function addWhere(QueryBuilder $queryBuilder, int $employerId): void
  {
    $interventionAlias = $queryBuilder->getRootAliases()[0];
    $queryBuilder->andWhere($interventionAlias . '.employer = :employerId');
    $queryBuilder->setParameter('employerId', $employerId);
    $queryBuilder->andWhere($interventionAlias . '.active = true');

    if ($_ENV['APP_ENV'] !== 'test') {
      $queryBuilder->andWhere($queryBuilder->expr()->orX(
        "jsonb_exists($interventionAlias.roles::jsonb , 'ROLE_AGENT') = true",
        "jsonb_exists($interventionAlias.roles::jsonb , 'ROLE_ADMIN_EMPLOYER') = true",
        "jsonb_exists($interventionAlias.roles::jsonb , 'ROLE_DIRECTOR') = true",
      ));
    }

  }
}
