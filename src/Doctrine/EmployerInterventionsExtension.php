<?php

declare(strict_types=1);

namespace App\Doctrine;

use ApiPlatform\Doctrine\Orm\Extension\QueryCollectionExtensionInterface;
use ApiPlatform\Doctrine\Common\PropertyHelperTrait;
use ApiPlatform\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Operation;
use App\Entity\Role;
use App\Entity\User;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use ApiPlatform\Doctrine\Orm\PropertyHelperTrait as OrmPropertyHelperTrait;


final readonly class EmployerInterventionsExtension implements QueryCollectionExtensionInterface
{
  use PropertyHelperTrait;
  use OrmPropertyHelperTrait;

  public function __construct(
    private Security        $security,
    private ManagerRegistry $managerRegistry,
  )
  {
  }

  protected function getManagerRegistry(): ManagerRegistry
  {
    return $this->managerRegistry;
  }

  #[\Override]
  public function applyToCollection(
    QueryBuilder                $queryBuilder,
    QueryNameGeneratorInterface $queryNameGenerator,
    string                      $resourceClass,
    Operation|null              $operation = null,
    array                       $context = []
  ): void
  {

    /** @var User|null $user */
    $user = $this->security->getUser();

    if (is_null($user)) {
      throw new AccessDeniedException();
    }

    if ($operation instanceof GetCollection &&
      ($operation->getUriTemplate() === '/employers/{employerId}/interventions' || $operation->getRouteName() === 'export_interventions_csv')) {
      $employer = $user->getEmployer();

      $employerId = (int)$context['uri_variables']['employerId'];
      /*
       * Les utilisateurs des communes (non admin) n'ont pas accès aux interventions
       * des autres communes.
       */
      if (!$user->hasRole(Role::Admin) && $employerId !== $employer->getId()) {
        throw new AccessDeniedException();
      }

      $recurring = isset($context['filters']['recurring']) && $context['filters']['recurring'] === 'true';

      /*
       * Accès interdit aux interventions récurrentes aux non admins, coordinateurs, admin employeurs
       */
      if ($recurring && !($user->hasRole(Role::Admin) || $user->hasRole(Role::AdminEmployer) || $user->hasRole(Role::Director))) {
        throw new AccessDeniedException();
      }

      /*
       * Si faux, filtre les demandes avec le statut to-validate
       */
      $statusToBeValidate = isset($context['filters']['statusToBeValidate']) && $context['filters']['statusToBeValidate'] === 'true';

      $interventionAlias = $queryBuilder->getRootAliases()[0];
      /*
       * Cache les interventions issues de récurrence après un nombre de jours donnés
       */
      if (isset($context['filters']['hidegeneratedafter'])) {
        $hideGeneratedInterventionsAfterDays = intval($context['filters']['hidegeneratedafter']);
        $queryBuilder->andWhere("($interventionAlias.recurrenceReference IS NULL OR ($interventionAlias.recurrenceReference IS NOT NULL AND $interventionAlias.startAt <= :afterDate))");
        $queryBuilder->setParameter('afterDate', new \DateTime("+$hideGeneratedInterventionsAfterDays days"));
      }

      /*
       * Un agent n'a le droit de voir que les interventions auxquelles il est affecté
       */
      if ($user->hasRole(Role::Agent)) {
        $queryBuilder->andWhere(':userId MEMBER OF ' . $interventionAlias . '.participants');
        $queryBuilder->setParameter('userId', $user->getId());
      } /*
       * Un demandeur ne peut accéder qu'aux interventions dont il est l'auteur
       */
      elseif ($user->hasRole(Role::Elected)) {
        $queryBuilder->andWhere(':userId = ' . $interventionAlias . '.author');
        $queryBuilder->setParameter('userId', $user->getId());
        $statusToBeValidate = true;
      }

      $this->addWhere($queryBuilder, $employerId, $recurring, $statusToBeValidate);
    }
  }

  private function addWhere(QueryBuilder $queryBuilder, int $employerId, bool $recurring, bool $statusToBeValidate): void
  {
    $interventionAlias = $queryBuilder->getRootAliases()[0];
    $queryBuilder->andWhere($interventionAlias . '.employer = :employerId');
    $queryBuilder->setParameter('employerId', $employerId);

    if ($recurring) {
      $queryBuilder->andWhere($interventionAlias . '.status = \'recurring\'');
    } else {
      $queryBuilder->andWhere($interventionAlias . '.status <> \'recurring\'');
    }

    if (!$statusToBeValidate) {
      $queryBuilder
        ->andWhere("$interventionAlias.status <> 'to-validate'");
    }
  }
}
