<?php

namespace App\Dto;

use App\Entity\Status;

use Symfony\Component\Validator\Constraints as Assert;

readonly class InterventionRejectionInput
{
  public function __construct(#[Assert\NotBlank]
  public string $reason, #[Assert\NotNull]
  public string $details)
  {
  }
}
