<?php

namespace App\Dto;

use App\Model\Frequency;
use DateTime;
use Symfony\Component\Serializer\Attribute\Groups;

class IndisponibiliteAgent
{

  #[Groups('indisponibilite')]
  private string $title = 'Indisponibilité';

  #[Groups('indisponibilite')]
  private Frequency $frequency = Frequency::ONE_TIME;
  #[Groups('indisponibilite')]
  private int $frequencyInterval = 1;
  #[Groups('indisponibilite')]
  private DateTime $startDate;
  #[Groups('indisponibilite')]
  private DateTime $endDate;
  #[Groups('indisponibilite')]
  private DateTime|null $recEndDate = null;

  #[Groups('indisponibilite')]
  private string $participant;

  public function getRecEndDate(): ?DateTime
  {
    return $this->recEndDate;
  }

  public function setRecEndDate(?DateTime $recEndDate): IndisponibiliteAgent
  {
    $this->recEndDate = $recEndDate;
    return $this;
  }

  public function getTitle(): string
  {
    return $this->title;
  }

  public function setTitle(string $title): IndisponibiliteAgent
  {
    $this->title = $title;
    return $this;
  }

  public function getFrequency(): Frequency
  {
    return $this->frequency;
  }

  public function setFrequency(Frequency $frequency): IndisponibiliteAgent
  {
    $this->frequency = $frequency;
    return $this;
  }

  public function getFrequencyInterval(): int
  {
    return $this->frequencyInterval;
  }

  public function setFrequencyInterval(int $frequencyInterval): IndisponibiliteAgent
  {
    $this->frequencyInterval = $frequencyInterval;
    return $this;
  }

  public function getStartDate(): DateTime
  {
    return $this->startDate;
  }

  public function setStartDate(DateTime $startDate): IndisponibiliteAgent
  {
    $this->startDate = $startDate;
    return $this;
  }

  public function getEndDate(): DateTime
  {
    return $this->endDate;
  }

  public function setEndDate(DateTime $endDate): IndisponibiliteAgent
  {
    $this->endDate = $endDate;
    return $this;
  }

  /**
   * @return string
   */
  public function getParticipant(): string
  {
    return $this->participant;
  }

  public function setParticipant(string $participant): IndisponibiliteAgent
  {
    $this->participant = $participant;
    return $this;
  }
}
