<?php

namespace App\Dto;

readonly class ProConnectUserDetails
{
  public function __construct(
    public string  $email,
    public string  $siret,
    public ?string $given_name,
    public ?string $usual_name,
    public ?string $phone_number)
  {
  }
}
