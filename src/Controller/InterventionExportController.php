<?php

namespace App\Controller;

use ApiPlatform\Metadata\Exception\ResourceClassNotFoundException;
use ApiPlatform\Metadata\Resource\Factory\ResourceMetadataCollectionFactoryInterface;
use ApiPlatform\State\ProviderInterface;
use App\Entity\Employer;
use App\Entity\Intervention;
use App\Entity\User;
use App\Security\EmployerVoter;
use Exception;
use LogicException;
use PhpOffice\PhpSpreadsheet\Reader\Csv;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\Attribute\Autowire;
use Symfony\Component\HttpFoundation\HeaderUtils;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Contracts\Translation\TranslatorInterface;

class InterventionExportController extends AbstractController
{
  private const string CSV_TYPE = 'text/csv';
  private const string EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
  private const array SUPPORTED_TYPES = [self::CSV_TYPE, self::EXCEL_TYPE];

  public function __construct(
    #[Autowire('@api_platform.doctrine.orm.state.collection_provider')]
    readonly private ProviderInterface                          $provider,
    readonly private ResourceMetadataCollectionFactoryInterface $resourceMetadataFactory,
    readonly private TranslatorInterface                        $translator
  )
  {

  }

  /**
   * @throws ResourceClassNotFoundException
   */
  #[
    Route('/api/employers/{employer<\d+>}/interventions/export', name: 'export_interventions'),
    IsGranted(EmployerVoter::VIEW, subject: 'employer'),
  ]
  public function exportProductsCsv(Employer $employer, Request $request): Response
  {
    $employerId = $employer->getId();
    if (!$employerId) {
      throw $this->createNotFoundException();
    }

    $type = $request->getAcceptableContentTypes()[0];

    // Retourner une réponse HTTP 406 si le client n'accepte pas le CSV
    if (!in_array($type, self::SUPPORTED_TYPES)) {
      return new Response("Allowed Content-Types are : " . implode(', ', self::SUPPORTED_TYPES), Response::HTTP_NOT_ACCEPTABLE);
    }

    // Récupérer la collection de métadonnées pour l'entité `Intervention`
    $resourceMetadataCollection = $this->resourceMetadataFactory->create(Intervention::class);

    // Sélectionner une opération particulière, ici la première "get" collection
    $operation = $resourceMetadataCollection->getOperation('_api_/employers/{employerId}/interventions_get_collection');

    // Contexte pour spécifier les en-têtes CSV personnalisés
    $context = [
      'filters' => $request->query->all(),
      'request' => $request,
      'uri_variables' => ['employerId' => $employerId]
    ];

    // Appel du provider pour obtenir les données de produits formatées pour le CSV
    /** @var Intervention[] $interventions */
    $interventions = $this->provider->provide($operation, [], $context);

    $csvData = $this->convertToCsv($interventions);
    if ($type === self::CSV_TYPE) {
      // Convertir les données en CSV

      // Retourner le CSV en réponse HTTP
      return new Response(
        $csvData,
        Response::HTTP_OK,
        [
          'Content-Type' => self::CSV_TYPE,
          'Content-Disposition' => 'attachment; filename="interventions.csv"',
        ]
      );
    } else {

      $reader = new Csv();
      $reader->setDelimiter(',');
      $reader->setEnclosure('"');
      $reader->setSheetIndex(0);
      $reader->setEscapeCharacter('\\');
      $spreadsheet = $reader->loadSpreadsheetFromString($csvData);
      $writer = new Xlsx($spreadsheet);
      $writer->setOffice2003Compatibility(true);

      ob_start();
      $writer->save('php://output');
      $content = ob_get_clean();
      ob_end_clean();

      if (!$content) {
        throw new LogicException('Impossible de générer le fichier Excel');
      }

      $disposition = HeaderUtils::makeDisposition(
        HeaderUtils::DISPOSITION_ATTACHMENT,
        'interventions.xlsx'
      );

      $response = new Response($content);
      $response->setStatusCode(Response::HTTP_OK);
      $response->headers->set('Content-Disposition', $disposition);
      $response->headers->set('Content-Type', self::EXCEL_TYPE);

      return $response;
    }
  }

  /**
   * @param Intervention[] $data
   * @return string
   * @throws Exception
   */
  private function convertToCsv(array $data): string
  {
    $csvHeaders = [
      'ID',
      'Titre',
      'Localisation (adresse)',
      'Description',
      'Statut de l\'intervention',
      'Statut de priorité',
      'Catégorie',
      'Sous catégorie',
      'Créateur',
      'Date de création',
      'Date de début',
      'Date de fin',
      'Utilisateurs assignés',
    ];
    $dateFormat = 'Y-m-d H:i:s';

    // Utiliser une fonction PHP pour générer le CSV à partir du tableau de données
    $handle = fopen('php://memory', 'r+');

    if (!$handle) {
      throw new LogicException('Cannot open memory stream');
    }

    fputcsv($handle, $csvHeaders, ',', '"', '\\');
    foreach ($data as $intervention) {
      $row = [
        $intervention->getLogicId(),
        $intervention->getTitle(),
        $intervention->getLocation()->getFullAddress(),
        $intervention->getDescription(),
        $this->translator->trans($intervention->getStatus()->value),
        $intervention->getPriority()->value,
        $intervention->getCategory()->getName(),
        $intervention->getType()->getName(),
        $intervention->getAuthor()->getFullName(),
        $intervention->getCreatedAt()->format($dateFormat),
        $intervention->getStartedAt()?->format($dateFormat),
        $intervention->getEndedAt()?->format($dateFormat),
        implode(', ', array_map(fn($user) => $user instanceof User ? $user->getFullName() : null, $intervention->getParticipants()->toArray())),
      ];
      fputcsv($handle, $row, ',', '"', '\\');
    }
    rewind($handle);
    $csvContent = stream_get_contents($handle);
    fclose($handle);

    if ($csvContent === false) {
      throw new LogicException('Cannot read memory stream');
    }

    return $csvContent;
  }

}
