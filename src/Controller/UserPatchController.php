<?php

namespace App\Controller;

use App\Entity\Picture;
use App\Entity\Role;
use App\Entity\User;
use App\Validator\Constraints\PasswordComplexity;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Attribute\AsController;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Component\Serializer\Context\Normalizer\ObjectNormalizerContextBuilder;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

#[AsController]
class UserPatchController extends AbstractController
{
  public function __construct(
    private readonly EntityManagerInterface      $entityManager,
    private readonly SerializerInterface         $serializer,
    private readonly UserPasswordHasherInterface $passwordHasher, private readonly ValidatorInterface $validator,
  )
  {
  }

  /**
   * @throws NonUniqueResultException
   */
  #[
    Route(
      path: 'api/users-patch/{id}',
      name: 'app_users_patch',
      methods: ['PATCH'],
    ),
    IsGranted('EDIT', subject: 'user')
  ]
  public function __invoke(Request $request, User $user, int $id): Response
  {
    $req = $request->toArray();
    foreach ($req as $key => $value) {
      switch ($key) {
        case 'firstname':
          $user->setFirstname($value);
          break;
        case 'lastname':
          $user->setLastname($value);
          break;
        case 'email':
          $user->setEmail($value);
          break;
        case 'phoneNumber':
          $user->setPhoneNumber($value);
          break;
        case 'roles':
          if (is_array($value)) {
            /** @var array<Role|string> $_value */
            $_value = $value;
            $user->setRoles($_value);
          }
          break;

        case 'picture':
          if ($value === null) {
            // TODO delete existing picture db (+s3 with db event)
            $user->setPicture(null);
            break;
          }

          $arr = explode('/', (string) $value);
          $pictureId = intval(array_pop($arr));
          if ($user->getPicture() && $user->getPicture()->getId() !== $pictureId) {
            // TODO delete existing picture db (+s3 with db event)
          }
          $repository = $this->entityManager->getRepository(Picture::class);
          $picture = $repository->find($pictureId);
          if (!$picture) {
            throw $this->createNotFoundException('Picture not found');
          }
          $user->setPicture($picture);
          break;
        case 'password':
          if($value === '') {
            break;
          }

          if (!array_key_exists('password_confirmation', $req)) {
            throw new \Error('password_confirmation field is missing');
          }

          if ($value !== $req['password_confirmation']) {
            throw new \Error('password and password_confirmation have to be equals');
          }

          $violations = $this->validator->validate($value, [
            new PasswordComplexity(),
          ]);

          if (count($violations)) {
            throw new \LogicException($violations[0]?->getMessage() ?? 'Password is invalid');
          }

          $hashedPassword = $this->passwordHasher->hashPassword($user, $value);
          $user->setPassword($hashedPassword);
          break;
        case 'login':
          $user->setLogin($value);
          break;
        default:
          // Do nothing
          break;
      }
    }

    $this->entityManager->flush();

    $query = $this->entityManager->createQuery(
      'SELECT u, e, p
        FROM App\Entity\User u
        INNER JOIN u.employer e
        LEFT JOIN u.picture p
        WHERE u.id = :id'
    )->setParameter('id', $id);

    /** @var User|null $user */
    $user = $query->getOneOrNullResult();

    if (!$user) {
      throw new NotFoundHttpException();
    }

    $context = (new ObjectNormalizerContextBuilder())
      ->withGroups([User::GROUP_VIEW, User::GROUP_GET_COLLECTION])
      ->toArray();

    return new Response(
      $this->serializer->serialize($user, 'json', $context),
      Response::HTTP_OK,
      ['Content-Type' => 'application/json']
    );
  }
}
