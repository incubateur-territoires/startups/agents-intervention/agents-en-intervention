<?php

namespace App\Controller;

use App\Entity\User;
use App\Security\EmployerVoter;
use App\Service\MetricsService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Attribute\AsController;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[AsController]
class DashboardMetricsController extends AbstractController
{
  public function __construct(
    private readonly MetricsService $metricsService,
  )
  {
  }

  #[
    Route(
      path: '/employers/{employerId}/dashboard-metrics',
      name: 'api_dashboard_metrics_get',
      methods: ['GET'],
    ),
    IsGranted(EmployerVoter::VIEW, subject: 'employer')
  ]
  public function __invoke(int $employerId): JsonResponse
  {
    $user = $this->getUser();
    if (!$user instanceof User) {
      throw $this->createAccessDeniedException();
    }

    $metrics = $this->metricsService->getMetricsForDashboard($user, $employerId);

    return $this->json($metrics);
  }
}
