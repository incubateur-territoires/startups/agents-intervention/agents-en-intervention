<?php

namespace App\Controller;

use App\Entity\Employer;
use App\Entity\Role;
use App\Entity\Status;
use App\Entity\User;
use App\Repository\CategoryRepository;
use App\Repository\InterventionRepository;
use App\Repository\NotificationRepository;
use App\Repository\UserRepository;
use App\Security\EmployerVoter;
use App\Security\Voter\PermissionVoter;
use App\Service\MetricsService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\CurrentUser;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Contracts\Translation\TranslatorInterface;

#[Route('/{employer<\d+>}/')]
class DashboardController extends AbstractController
{
  #[
    Route('dashboard', name: 'app_dashboard'),
    IsGranted(EmployerVoter::VIEW, subject: 'employer'),
    IsGranted(PermissionVoter::PRIVATE_MENU_SHOW_DASHBOARD)
  ]
  public function index(
    InterventionRepository $interventionRepository,
    Employer               $employer,
    MetricsService         $metricsService,
    Request                $request,
    UserRepository         $userRepository
  ): Response
  {
    $user = $this->getUser();
    if (!$user instanceof User || is_null($employer->getId())) {
      throw $this->createAccessDeniedException();
    }

    $order = $request->query->get('order', 'desc');
    $sortBy = $request->query->get('sort_by', 'logicId');

    $interventions = $interventionRepository->listToValidate($employer, $user, $sortBy, $order);

    $metrics = $metricsService->getMetricsForDashboard($user, $employer->getId());
    return $this->render('dashboard/index.html.twig', [
      'agents' => $userRepository->getActiveAgents($employer->getId()),
      'metrics' => $metrics,
      'interventions' => $interventions,
      'order' => $order,
      'sortBy' => $sortBy,
    ]);
  }

  #[
    Route('planning', name: 'app_dashboard_planning'),
    IsGranted(EmployerVoter::VIEW, subject: 'employer'),
    IsGranted(PermissionVoter::PRIVATE_MENU_SHOW_PLANNING)
  ]
  public function planning(Employer $employer): Response
  {
    return $this->render('dashboard/planning.html.twig', []);
  }

  #[
    Route('interventions', name: 'app_dashboard_interventions'),
    IsGranted(EmployerVoter::VIEW, subject: 'employer')
  ]
  public function interventions(
    Employer            $employer,
    CategoryRepository  $categoryRepository,
    UserRepository      $userRepository,
    TranslatorInterface $translator,
  ): Response
  {
    $employerId = $employer->getId();
    if (is_null($employerId)) {
      throw $this->createAccessDeniedException();
    }

    $statuses = [
      Status::Blocked->value => $translator->trans(Status::Blocked->name),
      Status::Finished->value => $translator->trans(Status::Finished->name),
      Status::InProgress->value => $translator->trans(Status::InProgress->name),
      Status::ToDo->value => $translator->trans(Status::ToDo->name),
    ];
    if (
      $this->isGranted(Role::Admin) ||
      $this->isGranted(Role::AdminEmployer) ||
      $this->isGranted(Role::Director)) {
      $statuses[Status::Rejected->value] = $translator->trans(Status::Rejected->name);
    }

    $agents = $userRepository->getAgents($employerId);

    return $this->render('dashboard/interventions.html.twig', [
      'title' => 'Liste des interventions',
      'recurrence' => false,
      'agents' => $agents,
      'agentsActifs' => $userRepository->getActiveAgents($employerId),
      'authors' => $agents,
      'categories' => $categoryRepository->findAll(),
      'statuses' => $statuses
    ]);
  }

  #[
    Route('recurrences', name: 'app_dashboard_recurrences'),
    IsGranted(EmployerVoter::VIEW, subject: 'employer'),
    IsGranted(PermissionVoter::PRIVATE_MENU_SHOW_RECURRENCES),
  ]
  public function recurrences(
    Employer            $employer,
    CategoryRepository  $categoryRepository,
    UserRepository      $userRepository,
    TranslatorInterface $translator
  ): Response
  {
    $employerId = $employer->getId();
    if (is_null($employerId)) {
      throw $this->createAccessDeniedException();
    }

    $agents = $userRepository->getAgents($employerId);

    return $this->render('dashboard/interventions.html.twig', [
      'title' => 'Liste des recurrences',
      'recurrence' => true,
      'agents' => $agents,
      'agentsActifs' => $userRepository->getActiveAgents($employerId),
      'authors' => $agents,
      'categories' => $categoryRepository->findAll(),
      'statuses' => [Status::Recurring->value => $translator->trans(Status::Recurring->name),]
    ]);
  }

  #[
    Route('carte', name: 'app_dashboard_map'),
    IsGranted(EmployerVoter::VIEW, subject: 'employer'),
  ]
  public function map(
    Employer               $employer,
    InterventionRepository $interventionRepository
  ): Response
  {
    $user = $this->getUser();
    \assert($user instanceof User);

    $statuses = [
      Status::ToDo,
      Status::Blocked,
      Status::InProgress,
    ];
    if (!$user->hasRole(Role::Agent)) {
      $statuses[] = Status::ToValidate;
      $statuses[] = Status::Rejected;
    }

    $interventions = $interventionRepository->getInterventionsForMap($employer, $statuses);

    return $this->render('dashboard/map.html.twig', [
      'employer' => $employer,
      'interventions' => $interventions
    ]);
  }

  #[
    Route('equipe', name: 'app_dashboard_team'),
    IsGranted(EmployerVoter::EMPLOYER_LIST_USERS, subject: 'employer'),
    IsGranted(EmployerVoter::VIEW, subject: 'employer'),
    IsGranted(PermissionVoter::PRIVATE_MENU_SHOW_TEAM),
  ]
  public function team(Employer $employer): Response
  {
    return $this->render('dashboard/team.html.twig', [
      'users' => $employer->getUsers()
    ]);
  }

  #[
    Route('communes', name: 'app_dashboard_employers'),
    IsGranted(Role::Admin->value)
  ]
  public function employers(Employer $employer): Response
  {
    return $this->render('dashboard/employers.html.twig', [
    ]);
  }

  #[
    Route('communes/ajout', name: 'app_dashboard_add_employer'),
    IsGranted(Role::Admin->value)
  ]
  public function addEmployer(Employer $employer): Response
  {
    return $this->render('dashboard/add_employer.html.twig', [
    ]);
  }

  #[Route('notifications/{page}', name: 'app_dashboard_notifications', requirements: ['page' => '\d+'])]
  public function showNotifications(
    #[CurrentUser] User    $user,
    NotificationRepository $notificationRepository,
    int                    $page = 1
  ): Response
  {
    $limit = 50;
    $nbPages = intval(ceil($notificationRepository->countNotifications($user) / $limit));

    return $this->render('dashboard/show_notifications.html.twig', [
      'notifications' => $notificationRepository->getNotifications($user, $page, $limit),
      'page' => $page,
      'nbPages' => $nbPages
    ]);
  }
}
