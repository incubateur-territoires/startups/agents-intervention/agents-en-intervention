<?php

namespace App\Controller;

use App\Entity\Employer;
use App\Entity\Role;
use App\Entity\User;
use App\Event\EmployerCreatedEvent;
use App\Form\EmployerCreationType;
use App\Form\EmployerType;
use Doctrine\ORM\EntityManagerInterface;
use Psr\EventDispatcher\EventDispatcherInterface;
use Symfony\Bridge\Doctrine\Attribute\MapEntity;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Component\Validator\Validator\ValidatorInterface;

#[
  Route('/{employer}/employer'),
  IsGranted('ROLE_ADMIN')
]
final class EmployerController extends AbstractController
{
  #[Route(name: 'app_employer_index', methods: ['GET'])]
  public function index(EntityManagerInterface $entityManager): Response
  {
    $employers = $entityManager
      ->getRepository(Employer::class)
      ->findAll();

    return $this->render('employer/index.html.twig', [
      'employers' => $employers,
    ]);
  }

  #[Route('/new', name: 'app_employer_new', methods: ['GET', 'POST'])]
  public function new(
    Request                     $request,
    EntityManagerInterface      $entityManager,
    ValidatorInterface          $validator,
    UserPasswordHasherInterface $passwordHasher,
    EventDispatcherInterface    $eventDispatcher
  ): Response
  {
    $form = $this->createForm(EmployerCreationType::class);
    $form->handleRequest($request);

    if ($form->isSubmitted() && $form->isValid()) {
      /** @var array<string, string> $data */
      $data = $form->getData();

      $employer = new Employer();
      $employer->setName($data['name']);
      $employer->setSiren($data['siren']);
      $employer->setLatitude(floatval($data['latitude']));
      $employer->setLongitude(floatval($data['longitude']));
      $employer->setTimezone($data['timezone']);
      $validator->validate($employer);

      $user = new User();
      $user->setFirstname($data['user_firstname']);
      $user->setLastname($data['user_lastname']);
      $user->setLogin($data['user_login']);
      $user->setEmail($data['user_email']);
      $user->setPhoneNumber($data['user_phoneNumber']);
      $user->setCreatedAt(new \DateTimeImmutable());
      $user->setActive(true);
      $user->setRoles([Role::AdminEmployer]);
      $user->setEmployer($employer);

      $hashedPassword = $passwordHasher->hashPassword($user, $data['user_password']);
      $user->setPassword($hashedPassword);

      $entityManager->persist($employer);
      $entityManager->persist($user);
      $entityManager->flush();

      $eventDispatcher->dispatch(new EmployerCreatedEvent($employer, true));

      return $this->redirectToRoute('app_employer_index', ['employer' => $request->attributes->get('employer')], Response::HTTP_SEE_OTHER);
    }

    return $this->render('employer/new.html.twig', [
      'form' => $form,
    ]);
  }

  #[Route('/{id}', name: 'app_employer_show', methods: ['GET'])]
  public function show(#[MapEntity(mapping: ['id' => 'id'])] Employer $employer): Response
  {
    return $this->render('employer/show.html.twig', [
      'employer' => $employer,
    ]);
  }

  #[Route('/{id}/edit', name: 'app_employer_edit', methods: ['GET', 'POST'])]
  public function edit(
    Request                $request,
    #[MapEntity(mapping: ['id' => 'id'])]
    Employer               $employer,
    EntityManagerInterface $entityManager
  ): Response
  {
    $form = $this->createForm(EmployerType::class, $employer);
    $form->handleRequest($request);

    if ($form->isSubmitted() && $form->isValid()) {
      $entityManager->flush();

      return $this->redirectToRoute('app_employer_index', ['employer' => $request->attributes->get('employer')], Response::HTTP_SEE_OTHER);
    }

    return $this->render('employer/edit.html.twig', [
      'employer' => $employer,
      'form' => $form,
    ]);
  }

  #[Route('/{id}', name: 'app_employer_delete', methods: ['POST'])]
  public function delete(
    Request                $request,
    #[MapEntity(mapping: ['id' => 'id'])]
    Employer               $employer,
    EntityManagerInterface $entityManager
  ): Response
  {
    if ($this->isCsrfTokenValid('delete' . $employer->getId(), $request->getPayload()->getString('_token'))) {
      $entityManager->remove($employer);
      $entityManager->flush();
    }

    return $this->redirectToRoute('app_employer_index', ['employer' => $request->attributes->get('employer')], Response::HTTP_SEE_OTHER);
  }
}
