<?php

namespace App\Controller\Admin;

use App\Entity\ActionLog;
use App\Entity\Category;
use App\Entity\Employer;
use App\Entity\Intervention;
use App\Entity\Roadmap;
use App\Entity\Session;
use App\Entity\Type;
use App\Entity\User;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

class DashboardController extends AbstractDashboardController
{
  #[Route('/admin', name: 'easyadmin_dashboard')]
  #[\Override]
  public function index(): Response
  {
     return $this->render('admin/my-dashboard.html.twig');
  }

  #[\Override]
  public function configureDashboard(): Dashboard
  {
    return Dashboard::new()
      ->setTitle('Agents en intervention')
      ->disableDarkMode();
  }

  #[\Override]
  public function configureMenuItems(): iterable
  {
    yield MenuItem::linkToUrl('Retour au site', 'fas fa-left-long', $this->generateUrl('app_home'));
    yield MenuItem::linkToDashboard('Dashboard', 'fa fa-home');

    yield MenuItem::section('Gestion des données');
    yield MenuItem::linkToCrud('Catégories', 'fas fa-list', Category::class)
      ->setPermission('ROLE_SUPER_ADMIN');
    yield MenuItem::linkToCrud('Types', 'fas fa-list', Type::class)
      ->setPermission('ROLE_SUPER_ADMIN');
    yield MenuItem::linkToCrud('Collectivités', 'fas fa-building', Employer::class);
    yield MenuItem::linkToCrud('Interventions', 'fas fa-person-digging', Intervention::class);
    yield MenuItem::linkToCrud('User', 'fas fa-users', User::class);

    yield MenuItem::section('Contenus');
    yield MenuItem::linkToCrud('Roadmap', 'fas fa-timeline', Roadmap::class);
    yield MenuItem::linkToRoute('Paramétrage du site', 'fas fa-sliders', 'admin_content_site');

    yield MenuItem::section('Administration')
      ->setPermission('ROLE_SUPER_ADMIN');
    yield MenuItem::linkToCrud("Piste d'audit", 'fas fa-history', ActionLog::class);
    yield MenuItem::linkToCrud('Sessions', 'fas fa-clock', Session::class);
    yield MenuItem::linkToRoute('Vérification des séquences', 'fas fa-exclamation-triangle', 'admin_check_sequence_errors')
      ->setPermission('ROLE_SUPER_ADMIN');
    yield MenuItem::linkToRoute('Divers', 'fas fa-screwdriver-wrench', 'admin_misc')
      ->setPermission('ROLE_SUPER_ADMIN');
  }

  #[\Override]
  public function configureActions(): Actions
  {
    return parent::configureActions()
      ->add(Crud::PAGE_INDEX, Action::DETAIL)
      ->add(Crud::PAGE_EDIT, Action::INDEX);
  }
}
