<?php

declare(strict_types=1);

namespace App\Controller\Admin;

use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Context\AdminContext;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;

trait SortableTrait
{
  abstract protected function getEntityCount(): int;

  abstract protected function getEntityManager(): EntityManagerInterface;

  public function configureActions(Actions $actions): Actions
  {
    $entityCount = $this->getEntityCount();

    $moveTop = Action::new('moveTop', false, 'fa fa-arrow-up')
      ->setHtmlAttributes(['title' => 'Déplacer au début'])
      ->linkToCrudAction('moveTop')
      ->displayIf(fn ($entity) => $entity->getPosition() > 0);

    $moveUp = Action::new('moveUp', false, 'fa fa-sort-up')
      ->setHtmlAttributes(['title' => 'Monter'])
      ->linkToCrudAction('moveUp')
      ->displayIf(fn ($entity) => $entity->getPosition() > 0);

    $moveDown = Action::new('moveDown', false, 'fa fa-sort-down')
      ->setHtmlAttributes(['title' => 'Descendre'])
      ->linkToCrudAction('moveDown')
      ->displayIf(fn ($entity) => $entity->getPosition() < $entityCount - 1);

    $moveBottom = Action::new('moveBottom', false, 'fa fa-arrow-down')
      ->setHtmlAttributes(['title' => 'Déplacer à la fin'])
      ->linkToCrudAction('moveBottom')
      ->displayIf(fn ($entity) => $entity->getPosition() < $entityCount - 1);

    return $actions
      ->add(Crud::PAGE_INDEX, $moveBottom)
      ->add(Crud::PAGE_INDEX, $moveDown)
      ->add(Crud::PAGE_INDEX, $moveUp)
      ->add(Crud::PAGE_INDEX, $moveTop)
      ;
  }

  public function moveTop(AdminContext $context): Response
  {
    return $this->move($context, SortableDirection::Top);
  }

  public function moveUp(AdminContext $context): Response
  {
    return $this->move($context, SortableDirection::Up);
  }

  public function moveDown(AdminContext $context): Response
  {
    return $this->move($context, SortableDirection::Down);
  }

  public function moveBottom(AdminContext $context): Response
  {
    return $this->move($context, SortableDirection::Bottom);
  }

  private function move(AdminContext $context, SortableDirection $direction): Response
  {
    $object = $context->getEntity()->getInstance();
    $newPosition = match ($direction) {
      SortableDirection::Top => 0,
      SortableDirection::Up => $object->getPosition() - 1,
      SortableDirection::Down => $object->getPosition() + 1,
      SortableDirection::Bottom => -1,
    };

    $object->setPosition($newPosition);
    $this->getEntityManager()->flush();

    if (null === $referer = $context->getRequest()->headers->get('referer')) {
      return new RedirectResponse('/');
    }

    return new RedirectResponse($context->getRequest()->headers->get('referer'));
  }
}
