<?php

namespace App\Controller\Admin;

use App\Form\Admin\SiteContentType;
use App\Service\AppConfigService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

class ContentController extends AbstractController
{
  public function __construct(
    private readonly AppConfigService $appConfigService
  )
  {
  }

  #[Route(path: '/admin/site-content', name: 'admin_content_site')]
  public function siteContent(Request $request): Response
  {
    $values = $this->appConfigService->getConfigsValues(['headerLink', 'headerTitle', 'headerActivated']);
    $form = $this->createForm(SiteContentType::class, $values);

    $form->handleRequest($request);
    if ($form->isSubmitted() && $form->isValid()) {
      $data = $form->getData();

      if (!is_array($data)) {
        $data = [];
      }

      foreach ($form as $key => $field) {
        if (array_key_exists($key, $data)) {
          $value = $data[$key];

          // Sauvegarder la configuration
          $this->appConfigService->setConfig($key, $value);
          $this->redirectToRoute('admin_content_site');
        }
      }
      $this->addFlash('success', 'Les paramètres du site ont été mis à jour.');
    }

    return $this->render('admin/content/site.html.twig', [
      'form' => $form
    ]);
  }
}
