<?php

declare(strict_types=1);

namespace App\Controller\Admin;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[IsGranted('ROLE_SUPER_ADMIN')]
class MiscController extends AbstractController
{
  #[Route('/admin/misc', name: 'admin_misc')]
  public function index(): Response
  {
    return $this->render('admin/misc/index.html.twig', [
      'serverTimezone' => date_default_timezone_get(),
    ]);
  }
}
