<?php

namespace App\Controller\Admin;

use App\Entity\Type;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[IsGranted('ROLE_SUPER_ADMIN')]
class TypeCrudController extends AbstractCrudController
{
  #[\Override]
  public static function getEntityFqcn(): string
  {
    return Type::class;
  }

  #[\Override]
  public function configureCrud(Crud $crud): Crud
  {
    return $crud
      ->setPaginatorPageSize(50);
  }

  #[\Override]
  public function configureFields(string $pageName): iterable
  {
    return [
      IdField::new('id')->hideOnForm(),
      TextField::new('name'),
      AssociationField::new('category'),
      TextField::new('picture'),
      TextEditorField::new('description'),
      IntegerField::new('position'),
    ];
  }
}
