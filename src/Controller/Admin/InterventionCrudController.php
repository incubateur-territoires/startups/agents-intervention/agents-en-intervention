<?php

namespace App\Controller\Admin;

use App\Entity\Intervention;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Filters;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class InterventionCrudController extends AbstractCrudController
{
  #[\Override]
  public static function getEntityFqcn(): string
  {
    return Intervention::class;
  }

  #[\Override]
  public function configureActions(Actions $actions): Actions
  {
    return parent::configureActions($actions)
      ->disable(Action::NEW, Action::EDIT, Action::BATCH_DELETE, Action::DELETE);
  }

  #[\Override]
  public function configureFilters(Filters $filters): Filters
  {
    return $filters
      ->add('employer')
      ->add('status')
      ;
  }

  #[\Override]
  public function configureFields(string $pageName): iterable
  {
    return [
      IdField::new('id'),
      IdField::new('logicId', 'ID logique'),
      DateTimeField::new('createdAt')
        ->hideOnIndex(),
      TextField::new('title', 'Titre')
        ->hideOnIndex(),
      TextEditorField::new('description', 'Description'),
      ChoiceField::new('status'),
      ChoiceField::new('priority'),
      AssociationField::new('type'),
      AssociationField::new('author'),
      AssociationField::new('employer'),
      AssociationField::new('location')
        ->hideOnIndex(),
      DateTimeField::new('endedAt'),
      AssociationField::new('participants'),
      AssociationField::new('comments')
        ->hideOnIndex(),
      AssociationField::new('pictures')
        ->hideOnIndex(),
      DateTimeField::new('startAt')
        ->hideOnIndex(),
      DateTimeField::new('endAt')
        ->hideOnIndex(),
      DateTimeField::new('dueDate')
        ->hideOnIndex(),
      ChoiceField::new('frequency')
        ->hideOnIndex(),
      IntegerField::new('frequencyInterval')
        ->hideOnIndex(),
      TextField::new('triggerAt')
        ->hideOnIndex(),
//      ArrayField::new('exceptions'),

//      AssociationField::new('recurrenceReference'),
//      AssociationField::new('generatedInterventions'),
    ];
  }
}
