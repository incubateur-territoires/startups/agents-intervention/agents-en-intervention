<?php

namespace App\Controller\Admin;

use App\Entity\ActionLog;
use App\Entity\Employer;
use App\Entity\Intervention;
use App\Entity\Location;
use App\Entity\User;
use App\Event\UserConnectedEvent;
use App\Event\UserConnectionFailedEvent;
use App\Repository\UserRepository;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Filters;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\NumberField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Filter\ChoiceFilter;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;

class ActionLogCrudController extends AbstractCrudController
{
  public function __construct(
    private readonly AdminUrlGenerator $adminUrlGenerator,
    private readonly UserRepository    $userRepository
  )
  {
  }

  #[\Override]
  public function configureActions(Actions $actions): Actions
  {
    return parent::configureActions($actions)
      ->disable(Action::NEW, Action::EDIT, Action::DELETE, Action::BATCH_DELETE);
  }

  public static function getEntityFqcn(): string
  {
    return ActionLog::class;
  }

  #[\Override]
  public function configureFilters(Filters $filters): Filters
  {
    return $filters
      ->add('entityClass')
      ->add(ChoiceFilter::new('type')->setChoices([
        'Workflow Transition' => 'workflow_transition',
        'Création' => 'entity_create',
        'Mise à jour' => 'entity_update',
        'Suppression' => 'entity_delete',
        'Connexion réussie' => 'user_connected',
        'Connexion échouée' => 'user_connection_failed',
      ]))
//      ->add(
//        BooleanFilter::new('changes', 'Has Raw Data')
//          ->setFormTypeOption('mapped', false)
//          ->setApplyFilter(function ($queryBuilder, $value) {
//            $queryBuilder->andWhere('JSON_CONTAINS(changes, \'{"_raw":{}}\') = 1');
//          }))
      ;

  }

  #[\Override]
  public function configureCrud(Crud $crud): Crud
  {
    return parent::configureCrud($crud)
      ->setPageTitle(Crud::PAGE_INDEX, "Piste d'audit")
      ->setPageTitle(Crud::PAGE_DETAIL, fn(ActionLog $actionLog) => sprintf("Entrée d'audit <small>(#%s)</small>", $actionLog->getId()))
      ->setDefaultSort(['id' => 'DESC']);
  }

  #[\Override]
  public function configureFields(string $pageName): iterable
  {
    return [
      IdField::new('id')->hideOnForm()->hideOnIndex(),
      DateTimeField::new('timestamp', 'Date'),
      TextField::new('type', 'Type')->formatValue(function (string $value, ActionLog $entity) {
        if (!$entity->isConsistent()) {
          return sprintf('<span class="badge badge-danger">Inconsistent (%s)</span>', $value);
        }

        return match ($value) {
          'workflow_transition' => '<span class="badge badge-info">Workflow : transition réussie</span>',
          'workflow_denied' => '<span class="badge badge-danger">Workflow : transition interdite</span>',
          'entity_create' => '<span class="badge badge-info">Création</span>',
          'entity_update' => '<span class="badge badge-warning">Mise à jour</span>',
          'entity_delete' => '<span class="badge badge-warning">Suppression</span>',
          UserConnectedEvent::getType() => '<span class="badge badge-success">Connexion réussie</span>',
          UserConnectionFailedEvent::getType() => '<span class="badge badge-danger">Connexion échouée</span>',
          default => '<span class="badge badge-secondary">Autre</span>',
        };
      })->renderAsHtml(),
      TextField::new('entityClass', 'Entity Class')
        ->formatValue(function ($value) {
          $arr = explode('\\', $value);
          return '<span class="badge badge-outline">' . end($arr) . '</span>';
        }),
      NumberField::new('entityId', 'Entity ID')->formatValue(function ($value, $entity) {
        $targetEntityClass = $entity->getEntityClass();

        // Mapping entre les classes d'entités et les noms de CRUD controllers
        $crudControllers = [
          Intervention::class => InterventionCrudController::class,
          Employer::class => EmployerCrudController::class,
          Location::class => LocationCrudController::class,
          User::class => UserCrudController::class,
          // Ajoutez d'autres entités selon les besoins
        ];

        if ($value && array_key_exists($targetEntityClass, $crudControllers)) {
          $crudController = $crudControllers[$targetEntityClass];
          return sprintf('<a href="%s">%s</a>',
            $this->adminUrlGenerator->setController($crudController)->setEntityId($value)->setAction(Crud::PAGE_DETAIL)->generateUrl(),
            $value
          );
        } elseif ($value) {
          return sprintf('%s', $value);
        }

        return 'N/A';
      }),
      NumberField::new('userIdentifier', 'User')
        ->formatValue(function ($value) {
          if (!$value) {
            return 'N/A';
          }

          /** @var User|null $user */
          $user = $this->userRepository->find($value);
          return sprintf(
            '<a href="%s">%s (%s)</a>',
            $this->adminUrlGenerator->setController(UserCrudController::class)->setEntityId($value)->setAction(Crud::PAGE_DETAIL)->generateUrl(),
            $value,
            $user?->getUserIdentifier() ?? 'Unknown or removed'
          );
        }),
      TextareaField::new('changes', 'Details')
        ->formatValue($this->changesFormatValue(...))
        ->hideOnIndex(),
    ];
  }

  public function changesFormatValue(string $value, ActionLog $entity): string
  {
    /** @var array $decoded */
    $decoded = json_decode($value, true);
    $data = $decoded['data'] ?? [];

    if (!$data) {
      return 'Invalid data format';
    }

    return match ($entity->getType()) {
      'workflow_transition' => $this->formatWorkflowTransition($data),
      'workflow_denied' => $this->formatWorkflowTransitionDenied($data),
      'entity_update' => $this->formatEntityUpdate($data),
      default => json_encode($data, JSON_PRETTY_PRINT) ?: 'No data',
    };
  }

  /**
   * @param array<string, bool|float|int|string|null> $data
   * @return string
   */
  public function formatWorkflowTransition(array $data): string
  {
    return sprintf(
      'Workflow "%s": transitioned from "%s" to "%s" via "%s"%s',
      $data['workflow'] ?? 'Unknown',
      $data['from'] ?? 'Unknown',
      $data['to'] ?? 'Unknown',
      $data['transition'] ?? 'Unknown',
      isset($data['comment']) ? ' (Comment: ' . $data['comment'] . ')' : ''
    );
  }

  /**
   * @param array<string, bool|float|int|string|null> $data
   * @return string
   */
  public function formatWorkflowTransitionDenied(array $data): string
  {
    return sprintf(
      'Workflow "%s": try to make transition from "%s" to "%s" via "%s"%s',
      $data['workflow'] ?? 'Unknown',
      $data['from'] ?? 'Unknown',
      $data['to'] ?? 'Unknown',
      $data['transition'] ?? 'Unknown',
      isset($data['comment']) ? ' (Comment: ' . $data['comment'] . ')' : ''
    );
  }

  /**
   * @param array{'changes': array{'fields_changed': string[]}|null, 'entity': string|null} $data
   * @return string
   */
  public function formatEntityUpdate(array $data): string
  {
    if (isset($data['changes']['fields_changed'])) {
      return sprintf(
        '<p>Entité mise à jour : <span class="badge badge-primary">%s</span></p><p>Champs modifiés: %s</p>',
        $data['entity'] ?? 'Unknown',
        '<span class="badge badge-secondary">' . implode('</span>, <span class="badge badge-secondary">', $data['changes']['fields_changed']) . '</span>'
      );
    }

    return 'Entité mise à jour. Pas de détails disponibles.';
  }

}
