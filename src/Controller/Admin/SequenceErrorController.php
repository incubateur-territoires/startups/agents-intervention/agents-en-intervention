<?php

namespace App\Controller\Admin;

use App\Entity\Employer;
use App\Entity\InterventionSequences;
use App\Entity\RecurringInterventionSequences;
use App\Service\Admin\SequenceChecker;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[IsGranted('ROLE_SUPER_ADMIN')]
class SequenceErrorController extends AbstractController
{
  public function __construct(private readonly SequenceChecker $sequenceChecker)
  {
  }

  #[Route("/admin/check-sequence-errors", name: "admin_check_sequence_errors")]
  public function checkErrors(): Response
  {
    // Récupération des séquences manquantes
    $missingSequences = $this->sequenceChecker->findMissingSequences();

    return $this->render('admin/sequence_errors.html.twig', [
      'missingSequences' => $missingSequences,
    ]);
  }

  /**
   * @param int $employerId
   * @param string $sequenceType
   * @param EntityManagerInterface $entityManager
   * @param AdminUrlGenerator $adminUrlGenerator
   * @return Response
   */
  #[Route("/admin/add-sequence/{employerId}/{sequenceType}", name: "admin_add_sequence")]
  public function addSequence(
    int                    $employerId,
    string                 $sequenceType,
    EntityManagerInterface $entityManager,
    AdminUrlGenerator      $adminUrlGenerator
  ): Response
  {
    $employer = $entityManager->getRepository(Employer::class)->find($employerId);
    if (!$employer) {
      throw $this->createNotFoundException('Employer not found');
    }

    if ($sequenceType === 'Intervention') {
      // Ajouter une nouvelle séquence dans InterventionSequences
      $sequence = new InterventionSequences();
      $sequence->setEmployer($employer);
      $sequence->setSeq(0);
      $entityManager->persist($sequence);
      $entityManager->flush();
    } elseif ($sequenceType === 'Intervention récurrente') {
      // Ajouter une nouvelle séquence dans RecurringInterventionSequences
      $sequence = new RecurringInterventionSequences();
      $sequence->setEmployer($employer);
      $sequence->setSeq(0);
      $entityManager->persist($sequence);
      $entityManager->flush();
    }
    $this->addFlash('success', "Séquence \"$sequenceType\" créée pour " . $employer->getName());

    return $this->redirect($adminUrlGenerator->setRoute('admin_check_sequence_errors')->generateUrl());
  }

}
