<?php

namespace App\Controller\Admin;

use App\Entity\Session;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Attribute\AdminAction;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Option\EA;
use EasyCorp\Bundle\EasyAdminBundle\Context\AdminContext;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpFoundation\Response;

class SessionCrudController extends AbstractCrudController
{
  public function __construct(
    private readonly Security $security
  )
  {
  }

  public static function getEntityFqcn(): string
  {
    return Session::class;
  }

  #[\Override]
  public function configureCrud(Crud $crud): Crud
  {
    return parent::configureCrud($crud)
      ->showEntityActionsInlined();
  }

  #[\Override]
  public function configureActions(Actions $actions): Actions
  {
    $deleteStaleSessions = Action::new('delete_stale', 'Supprimer les sessions expirées')
      ->linkToCrudAction('deleteStale')
      ->setIcon('fa fa-trash')
      ->setCssClass('btn btn-danger')
      ->setHtmlAttributes([
        'title' => 'Supprimer les sessions expirées',
        'onclick' => 'return confirm("Êtes-vous sûr de vouloir supprimer les sessions expirées ?")'
      ])
      ->createAsGlobalAction();

    return parent::configureActions($actions)
      ->disable(Action::NEW, Action::EDIT)
      ->add(Crud::PAGE_INDEX, $deleteStaleSessions)
      ->update(Crud::PAGE_INDEX, Action::DELETE, fn(Action $action) => $action
        ->setIcon('fa fa-trash')
        ->displayIf(fn($entity) => $entity && !($entity->getOngoing() && $entity->getUsername() === $this->security->getUser()?->getUserIdentifier())));
  }

  #[\Override]
  public function configureFields(string $pageName): iterable
  {
    return [
      IdField::new('id')->setLabel('Session ID'),
      TextField::new('username')->setLabel('Utilisateur')->setVirtual(true)
      ->formatValue(
        fn($value, $entity) => $entity->getUsername() === 'anonymous' ? '<i>non connecté</i>' : $value
      ),
      TextareaField::new('unserializedDataJson')
        ->setLabel('Données de session')
        ->setVirtual(true)
        ->hideOnIndex()
        ->formatValue(
          fn($value) => '<pre>' . $value . '</pre>'
        ),
      BooleanField::new('ongoing')->setLabel('Session en cours')
        ->renderAsSwitch(false)
        ->setVirtual(true),
      DateTimeField::new('lifetime')->setLabel('Date d\'expiration'),
      DateTimeField::new('time')->setLabel('Dernière activité'),
    ];
  }

  #[AdminAction(routePath: '/deleteStale', routeName: 'delete_stale', methods: ['GET', 'POST'])]
  public function deleteStale(AdminContext $context, EntityManagerInterface $em): Response
  {
    $em->getRepository(Session::class)->deleteStale();

    $this->addFlash('success', 'Les sessions expirées ont été supprimées.');
    return $this->redirect($this->container->get(AdminUrlGenerator::class)->setController(self::class)->setAction(Action::INDEX)->unset(EA::ENTITY_ID)->generateUrl());
  }
}
