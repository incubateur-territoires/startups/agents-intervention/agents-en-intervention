<?php

declare(strict_types=1);

namespace App\Controller\Admin;

use App\Entity\Roadmap;
use App\Repository\RoadmapRepository;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Event\BeforeEntityPersistedEvent;
use EasyCorp\Bundle\EasyAdminBundle\Event\BeforeEntityUpdatedEvent;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use Symfony\Component\EventDispatcher\Attribute\AsEventListener;
use Symfony\Component\HtmlSanitizer\HtmlSanitizer;
use Symfony\Component\HtmlSanitizer\HtmlSanitizerConfig;

class RoadmapCrudController extends AbstractCrudController
{
  use SortableTrait;

  public function __construct(
    private EntityManagerInterface $entityManager,
    private RoadmapRepository $roadmapRepository,
  ) {
  }

  protected function getEntityManager(): EntityManagerInterface
  {
    return $this->entityManager;
  }

  protected function getEntityCount(): int
  {
    return $this->roadmapRepository->count();
  }

  #[\Override]
  public static function getEntityFqcn(): string
  {
    return Roadmap::class;
  }

  #[\Override]
  public function configureCrud(Crud $crud): Crud
  {
    $crud = parent::configureCrud($crud);

    return $crud
      ->setDefaultSort(['position' => 'ASC'])
      ->showEntityActionsInlined()
      ;
  }

  #[\Override]
  public function configureFields(string $pageName): iterable
  {
    return [
      IdField::new('id')
        ->hideOnForm(),
      TextField::new('title', 'Titre'),
      TextField::new('estimatedDate', 'Date estimée'),
      DateField::new('expirationDate', 'Date d\'expiration'),
      BooleanField::new('published', 'Publié'),
      TextEditorField::new('description')->hideOnIndex(),
    ];
  }

  #[AsEventListener(BeforeEntityPersistedEvent::class)]
  #[AsEventListener(BeforeEntityUpdatedEvent::class)]
  public function formatDescription(BeforeEntityPersistedEvent|BeforeEntityUpdatedEvent $event): void
  {
    $entity = $event->getEntityInstance();

    if (!$entity instanceof Roadmap) {
      return;
    }

    $description = $entity->getDescription();
    $config = (new HtmlSanitizerConfig())
      ->allowSafeElements()
    ;

    $sanitizer = new HtmlSanitizer($config);

    $entity->setDescription($sanitizer->sanitize($description));
  }
}
