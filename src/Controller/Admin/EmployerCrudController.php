<?php

namespace App\Controller\Admin;

use App\Entity\Employer;
use App\Entity\Role;
use App\Entity\User;
use App\Form\Admin\ChangeAdminEmployerType;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Context\AdminContext;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\FormField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\NumberField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TimezoneField;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class EmployerCrudController extends AbstractCrudController
{
  #[\Override]
  public static function getEntityFqcn(): string
  {
    return Employer::class;
  }

  #[\Override]
  public function configureActions(Actions $actions): Actions
  {
    $changeAdminAction = Action::new('create')
      ->setLabel(
        'Modifier admin'
      )
      ->linkToCrudAction('changeAdmin');

    return parent::configureActions($actions)
      ->add(Crud::PAGE_INDEX, $changeAdminAction)
      ->add(Crud::PAGE_DETAIL, $changeAdminAction)
      ;
  }

  #[\Override]
  public function configureCrud(Crud $crud): Crud
  {
    return parent::configureCrud($crud)
      ->setPageTitle(Crud::PAGE_INDEX, 'Collectivités')
      ->setEntityLabelInPlural('Collectivités')
      ->setEntityLabelInSingular('Collectivité');
  }

  #[\Override]
  public function configureFields(string $pageName): iterable
  {
    return [
      FormField::addPanel('Informations sur la commune')
        ->onlyWhenCreating(),

      IdField::new('id')->hideOnForm(),
      TextField::new('siren'),
      TextField::new('name'),
      NumberField::new('longitude'),
      NumberField::new('latitude'),
      TimezoneField::new('timezone', 'Timezone')->hideOnIndex(),

      AssociationField::new('users')->hideOnForm(),

      // Pour la création, il faut aussi renseigner
      // les informations sur l'administrateur
//            FormField::addPanel('Administrateur')->onlyWhenCreating(),
//
//            AssociationField::new('users')
//                ->setFormTypeOptions([
//                    'by_reference' => false
//                ])
//                ->onlyWhenCreating(),

//            TextField::new('user.login')->onlyWhenCreating()
//            ->setEmptyData()->setFieldFqcn(User::class),
//            TextField::new('user.firstname')->onlyWhenCreating(),
//            TextField::new('user.lastname')->onlyWhenCreating(),
//            EmailField::new('user.email')->onlyWhenCreating(),
//            TextField::new('user.phoneNumber')->onlyWhenCreating(),
    ];
  }

  public function changeAdmin(
    Request           $request,
    AdminUrlGenerator $adminUrlGenerator,
    UserRepository    $userRepository,
    AdminContext $adminContext,
    EntityManagerInterface $entityManager
  ): Response
  {
    $employer = $adminContext->getEntity()->getInstance();
    if (!$employer instanceof Employer && $employer->getId()) {
      throw new \LogicException('Entity is missing or not a Employer');
    }

    // Trouver l'utilisateur actuel avec le rôle AdminEmployer
    /** @var User|null $currentAdmin */
    $currentAdmin = $userRepository->getAdminEmployer($employer->getId());

    // Créer et traiter le formulaire
    $form = $this->createForm(ChangeAdminEmployerType::class, null, ['employer' => $employer]);
    $form->handleRequest($request);

    if ($form->isSubmitted() && $form->isValid()) {
      /** @var User|null $newAdmin */
      $newAdmin = $form->get('newAdmin')->getData();
      if (!$newAdmin) {
        throw new \LogicException('Le nouvel admin n\'est pas correct');
      }

      if ($currentAdmin) {
        // Retirer le rôle AdminEmployer au profit de Coordinateur de l'utilisateur actuel
        $currentAdmin->setRoles([Role::Director]);
      }

      // Assigner le rôle AdminEmployer au nouvel utilisateur
      $newAdmin->setRoles([Role::AdminEmployer]);

      // Sauvegarder les modifications
      $entityManager->flush();

      // Message de succès et redirection
      $this->addFlash('success', 'Le nouvel administrateur de la collectivité a été assigné avec succès.');
      return $this->redirect($adminUrlGenerator->setController(self::class)->generateUrl());  // Remplacez par la route adéquate pour revenir au dashboard
    }

    return $this->render('admin/change_admin_employer.html.twig', [
      'form' => $form->createView(),
      'employer' => $employer,
      'currentAdmin' => $currentAdmin,
    ]);
  }
}
