<?php

namespace App\Controller\Admin;

use App\Entity\User;
use App\Repository\SessionRepository;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Filters;
use EasyCorp\Bundle\EasyAdminBundle\Context\AdminContext;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\ArrayField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\EmailField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;

class UserCrudController extends AbstractCrudController
{
  #[\Override]
  public static function getEntityFqcn(): string
  {
    return User::class;
  }

  #[\Override]
  public function configureActions(Actions $actions): Actions
  {
    $seeSessions = Action::new('sessions', 'Voir les sessions')
      ->linkToCrudAction('sessions')
      ->setIcon('fa fa-eye')
      ->setCssClass('btn btn-info');

    return parent::configureActions($actions)
      ->disable(Action::NEW, Action::EDIT, Action::BATCH_DELETE)
      ->add(Crud::PAGE_DETAIL, $seeSessions);
  }

  #[\Override]
  public function configureCrud(Crud $crud): Crud
  {
    return parent::configureCrud($crud)
      ->showEntityActionsInlined();
  }

  #[\Override]
  public function configureFilters(Filters $filters): Filters
  {
    return parent::configureFilters($filters)
//      ->add(ChoiceFilter::new('roles', 'Rôles')->setChoices([
//        $this->translator->trans(Role::AdminEmployer->value) => Role::AdminEmployer,
//        $this->translator->trans(Role::Director->value) => Role::Director,
//        $this->translator->trans(Role::Agent->value) => Role::Agent,
//        $this->translator->trans(Role::Elected->value) => Role::Elected,
//        $this->translator->trans(Role::Admin->value) => Role::Admin,
//      ]))
      ->add('employer');
  }

  #[\Override]
  public function configureFields(string $pageName): iterable
  {
    return [
      IdField::new('id')->hideOnForm(),
      TextField::new('login'),
      TextField::new('firstname', 'Prénom'),
      TextField::new('lastname', 'Nom'),
      EmailField::new('email', 'Email'),
      TextField::new('phoneNumber', 'Numéro tél.')->onlyOnDetail(),
      ArrayField::new('roles'),
      AssociationField::new('employer', 'Collectivité'),
      BooleanField::new('active'),
      DateTimeField::new('createdAt', 'Création du compte')->onlyOnDetail()->setDisabled(true),
      DateTimeField::new('connectedAt', 'Dernière connexion')->onlyOnDetail()->setDisabled(true),
      TextField::new('proConnectToken', 'Pro Connect activé')
        ->onlyOnDetail()
        ->formatValue(fn ($value, $entity) => $entity->getProConnectToken() ? '<span class="badge badge-info">Oui</span>' : '<span class="badge badge badge-outline">Non</span>'),
    ];
  }

  public function sessions(AdminContext $context, SessionRepository $sessionRepository): Response
  {
    /** @var User $user */
    $user = $context->getEntity()->getInstance();
    $sessions = $sessionRepository->findByUser($user);
    return $this->render('admin/user/sessions.html.twig', [
      'user' => $user,
      'sessions' => $sessions,
    ]);
  }

  public function deleteSession(
    AdminContext           $context,
    AdminUrlGenerator      $adminUrlGenerator,
    EntityManagerInterface $em,
    SessionRepository      $sessionRepository,
  ): RedirectResponse
  {
    $session = $sessionRepository->find($context->getRequest()->get('sessionId'));
    if ($session) {
      $em->remove($session);
      $em->flush();
    }
    $this->addFlash('success', 'La session a été supprimée.');
    return $this->redirect($adminUrlGenerator->setController(self::class)->setAction('sessions')->generateUrl());
  }
}
