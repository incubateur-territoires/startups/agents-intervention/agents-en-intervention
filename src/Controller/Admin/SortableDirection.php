<?php

declare(strict_types=1);

namespace App\Controller\Admin;

enum SortableDirection
{
  case Top;
  case Up;
  case Down;
  case Bottom;
}
