<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Employer;
use App\Entity\Role;
use App\Entity\User;
use App\Event\EmployerCreatedEvent;
use App\Form\SignupCredentialType;
use App\Form\SignupType;
use App\Form\SignupUserType;
use App\Repository\EmployerRepository;
use App\Service\EntrepriseService;
use App\Service\TimezoneService;
use Doctrine\ORM\EntityManagerInterface;
use NotFloran\MjmlBundle\Renderer\RendererInterface;
use Psr\EventDispatcher\EventDispatcherInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class SignupController extends AbstractController
{
  public function __construct(private readonly EntrepriseService $entrepriseService)
  {
  }

  /**
   * @param EntrepriseService $entrepriseService
   * @param EmployerRepository $employerRepository
   * @param Request $request
   * @return Response
   * @throws ClientExceptionInterface
   * @throws RedirectionExceptionInterface
   * @throws ServerExceptionInterface
   * @throws TransportExceptionInterface
   */
  #[Route(path: 'inscription', name: 'app_signup')]
  public function signup(
    EntrepriseService  $entrepriseService,
    EmployerRepository $employerRepository,
    Request            $request
  ): Response
  {
    $form = $this->createForm(SignupType::class);

    $form->handleRequest($request);
    if ($form->isSubmitted() && $form->isValid()) {
      /** @var array{siren: string} $data */
      $data = $form->getData();

      if (!$entrepriseService->isCollectiviteTerritoriale($data['siren'])) {
        $form->get('siret')->addError(new FormError("Le SIRET renseigné ne correspond pas à une collectivité territoriale"));
      } elseif ($employerRepository->findOneBy(['siren' => $data['siren']])) {
        $form->get('siret')->addError(new FormError("Cette collectivité a déjà un compte de créé, nous vous invitons à vous connecter"));
      } else {
        $session = $request->getSession();
        $session->set('signup', $data);
        return $this->redirectToRoute('app_signup_user');
      }

    }

    return $this->render('security/signup.html.twig', [
      'error' => null,
      'form' => $form->createView()
    ]);
  }

  #[Route(path: 'inscription/user', name: 'app_signup_user')]
  public function signupUser(Request $request): Response
  {
    $session = $request->getSession();
    if (!$session->has('signup')) {
      return $this->redirectToRoute('app_signup');
    }

    /** @var iterable<string, mixed> $signup */
    $signup = $session->get('signup') ?? [];
    /** @var iterable<string, mixed> $signup_user */
    $signup_user = $session->get('signup_user') ?? [];

    $form = $this->createForm(SignupUserType::class, [
      ...$signup,
      ...$signup_user
    ]);
    $form->handleRequest($request);

    if ($form->isSubmitted() && $form->isValid()) {
      $data = $form->getData();
      $session->set('signup_user', $data);
      return $this->redirectToRoute('app_signup_credentials');
    }

    return $this->render('security/signup_user.html.twig', [
      'error' => null,
      'form' => $form->createView()
    ]);
  }

  #[Route(path: 'inscription/identifiant', name: 'app_signup_credentials')]
  public function signupUserCredentials(
    EntityManagerInterface   $em,
    EventDispatcherInterface $eventDispatcher,
    Request                  $request,
    Security                 $security,
    TimezoneService          $timezoneService
  ): Response
  {
    $session = $request->getSession();
    if (!$session->get('signup_user')) {
      return $this->redirectToRoute('app_signup_user');
    }

    /** @var array{firstname: string, lastname: string, email: string, phoneNumber: string} $userData */
    $userData = $session->get('signup_user');

    $user = new User();
    $user->setFirstname($userData['firstname']);
    $user->setLastname($userData['lastname']);
    $user->setEmail($userData['email']);
    $user->setPhoneNumber($userData['phoneNumber']);

    $form = $this->createForm(SignupCredentialType::class, $user);
    $form->handleRequest($request);
    if ($form->isSubmitted() && $form->isValid()) {

      /** @var array{name: string, siren: string, latitude: string, longitude: string} $employerData */
      $employerData = $session->get('signup');
      $employer = new Employer();
      $employer->setName($employerData['name']);
      $employer->setSiren($employerData['siren']);
      $employer->setLatitude(floatval($employerData['latitude']));
      $employer->setLongitude(floatval($employerData['longitude']));

      if ($employer->getLatitude() == 0) {
        $employer = $this->entrepriseService->getEmployerFromSiret($employer->getSiren());
      } else {
        $employer->setTimezone($timezoneService->getTimezoneFromCoordinates(
          $employer->getLatitude(),
          $employer->getLongitude()
        ));
      }

      /** @var User $user */
      $user = $form->getData();

      /*
       * Si c'est le premier utilisateur de l'employeur, on lui donne le rôle
       * d'administrateur de l'employeur.
       */
      if ($employer->getSiren() === '130026032') {
        // Cas spécial pour l'ANCT
        $user->setRoles([Role::Admin]);
      } elseif ($employer->getUsers()->isEmpty()) {
        $user->setRoles([Role::AdminEmployer]);
      }

      $user->setEmployer($employer);
      $em->persist($employer);
      $em->persist($user);
      $em->flush();

      $eventDispatcher->dispatch(new EmployerCreatedEvent($employer, false));
      $security->login($user, 'form_login');

      $session->remove('signup');
      $session->remove('signup_user');

      return $this->redirectToRoute('app_dashboard', ['employer' => $employer->getId()]);
    }

    return $this->render('security/signup_credentials.html.twig', [
      'error' => null,
      'form' => $form->createView()
    ]);
  }
}
