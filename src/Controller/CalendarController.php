<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\CalendarEvent;
use App\Entity\Employer;
use App\Entity\User;
use App\Repository\CalendarEventRepository;
use App\Repository\UserRepository;
use Sabre\VObject\Component\VCalendar;
use Sabre\VObject\Reader;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[Route('/calendar')]
class CalendarController extends AbstractController
{
  #[
    Route('/collectivite/{employer}/indisponibilite.ics', name: 'api_collectivite_indisponibilite'),
    IsGranted('LIST_INDISPONIBILITE_COLLECTIVITE', subject: 'employer')
  ]
  public function indisponibiliteIcs(
    CalendarEventRepository $calendarEventRepository,
    Employer                $employer,
    UserRepository          $userRepository
  ): Response
  {

    // Find users related to the Employer
    $users = $userRepository->findBy(['employer' => $employer->getId()]);

    $mergedCalendar = new VCalendar([
      'PRODID' => '-//ANCT//Agents en intervention//FR',
    ]);

    if (empty($users)) {
      // Build the list of calendar identifiers to search for
      $calendarIdentifiers = array_map(
        fn(User $user) => sprintf('%d/indisponibilite', $user->getId()),
        $users
      );

      // Query calendar events related to the above calendar identifiers
      /** @var CalendarEvent[] $calendarEvents */
      $calendarEvents = $calendarEventRepository->createQueryBuilder('e')
        ->andWhere('e.calendarId IN (:calendars)')
        ->setParameter('calendars', $calendarIdentifiers)
        ->getQuery()
        ->getResult();

      foreach ($calendarEvents as $event) {
        $vcal = Reader::read($event->getCalendarData());
        foreach ($vcal->select('VEVENT') as $vcal_event) {
          $mergedCalendar->add(clone $vcal_event);
        }
      }
    }

    return new Response($mergedCalendar->serialize(), Response::HTTP_OK, ['Content-Type' => 'text/calendar']);
  }

  #[
    Route('/agent/{agent}/indisponibilite.ics', name: 'api_agent_indisponibilite'),
    IsGranted('LIST_INDISPONIBILITE_AGENT', subject: 'agent')
  ]
  public function agentIndisponibiliteIcs(
    CalendarEventRepository $calendarEventRepository,
    User                    $agent,
  ): Response
  {
    $mergedCalendar = new VCalendar([
      'PRODID' => '-//ANCT//Agents en intervention//FR',
    ]);

    // Query calendar events related to the above calendar identifiers
    /** @var CalendarEvent[] $calendarEvents */
    $calendarEvents = $calendarEventRepository->createQueryBuilder('e')
      ->andWhere('e.calendarId = :calendar')
      ->setParameter('calendar', sprintf('%d/indisponibilite', $agent->getId()))
      ->getQuery()
      ->getResult();

    foreach ($calendarEvents as $event) {
      $vcal = Reader::read($event->getCalendarData());
      foreach ($vcal->select('VEVENT') as $vcal_event) {
        $mergedCalendar->add(clone $vcal_event);
      }
    }

    return new Response($mergedCalendar->serialize(), Response::HTTP_OK, ['Content-Type' => 'text/calendar']);
  }
}
