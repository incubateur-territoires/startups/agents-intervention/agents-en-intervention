<?php

namespace App\Controller;

use App\Entity\Picture;
use App\Entity\PictureTag;
use App\Entity\Role;
use App\Entity\User;
use App\Form\EditAccountType;
use App\Service\S3Service;
use Doctrine\ORM\EntityManagerInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\Attribute\Autowire;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\CurrentUser;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends AbstractController
{
  /**
   * Authentification des utilisateurs
   *
   * @param User|null $user
   * @param AuthenticationUtils $authenticationUtils
   * @param JWTTokenManagerInterface $tokenManager
   * @param LoggerInterface $logger
   * @param Request $request
   * @param string $appUrl
   * @return Response
   */
  #[Route(path: '/connexion', name: 'app_login')]
  public function login(
    #[CurrentUser] ?User                   $user,
    AuthenticationUtils                    $authenticationUtils,
    JWTTokenManagerInterface               $tokenManager,
    LoggerInterface                        $logger,
    Request                                $request,
    #[Autowire('%env(FRONT_URL)%')] string $appUrl
  ): Response
  {
    /**
     * Si l'utilisateur est déjà authentifié cela peut
     * signifier qu'il est arrivé soit par erreur, soit
     * parce que son token JWT est expiré.
     * On essaye de le régénérer et de poursuivre.
     */
    if ($user) {
      $logger->info('Generate jwt for user', ['user' => $user->getId()]);

      $request->getSession()->set('jwt', $tokenManager->create($user));
      $redirect = $request->query->get('redirect', $this->generateUrl('login_success'));

      if (!str_starts_with($redirect, $appUrl . '/')) {
        return $this->redirectToRoute('login_success');
      }

      return new RedirectResponse($redirect, Response::HTTP_MOVED_PERMANENTLY);
    }

    // get the login error if there is one
    $error = $authenticationUtils->getLastAuthenticationError();

    // last username entered by the user
    $lastUsername = $authenticationUtils->getLastUsername();

    return $this->render('security/login.html.twig', [
      'last_username' => $lastUsername,
      'error' => $error,
    ]);
  }

  #[Route(path: '/auth/sign-in', name: '_app_sign_in')]
  public function oldSignInRedirect(): Response
  {
    return new RedirectResponse($this->generateUrl('app_login'));
  }

  #[Route(path: '/connexion-reussie', name: 'login_success')]
  public function onLoginSuccess(#[CurrentUser] User $user): Response
  {
    if ($user->hasRole(Role::Agent)) {
      return new RedirectResponse($this->generateUrl('app_dashboard_map', ['employer' => $user->getEmployer()->getId()]));
    } else {
      return new RedirectResponse($this->generateUrl('app_dashboard', ['employer' => $user->getEmployer()->getId()]));
    }
  }

  #[Route(path: '/deconnexion', name: 'app_logout')]
  public function logout(): void
  {
    throw new \LogicException('This method can be blank - it will be intercepted by the logout key on your firewall.');
  }

  #[Route(path: 'mon-compte', name: 'app_my_account')]
  public function myAccount(): Response
  {
    return $this->render('security/my-account.html.twig');
  }

  #[Route(path: 'mon-compte/notifications', name: 'app_my_account_notifications')]
  public function myAccountNotifications(): Response
  {
    return $this->render('security/my-account-notifications.html.twig');
  }

  #[Route(path: 'mon-compte/edit', name: 'app_edit_account')]
  public function editAccount(
    Request                $request,
    EntityManagerInterface $entityManager,
    S3Service              $s3
  ): Response
  {
    $form = $this->createForm(EditAccountType::class, $this->getUser());
    $form->handleRequest($request);
    if ($form->isSubmitted() && $form->isValid()) {
      // $form->getData() holds the submitted values
      // but, the original `$task` variable has also been updated
      /** @var User $user */
      $user = $form->getData();

      $file = $form->get('profilePicture')->getData();
      if ($file && $file instanceof UploadedFile) {
        $fileName = "users/user-" . $user->getId() . '-' . hash("sha512", $file->getContent()) . '.' . $file->getClientOriginalExtension();
        $s3->putFile($file, $fileName);
        $picture = $user->getPicture();
        if ($picture === null) {
          $picture = new Picture($fileName);
          $user->setPicture($picture);
        } else {
          $picture->setFilename($fileName);
        }
        $picture->setTag(PictureTag::Avatar);
        $entityManager->persist($picture);
      }
      $entityManager->persist($user);
      $entityManager->flush();

      $this->addFlash(
        'success',
        'Votre compte a bien été mis à jour'
      );

      // ... perform some action, such as saving the task to the database

      return $this->redirectToRoute('app_my_account');
    }

    return $this->render('security/edit-my-account.html.twig', [
      'form' => $form
    ]);
  }

  #[Route(path: 'mon-compte/completer', name: 'app_profile_completion')]
  public function profileCompletion(
    EntityManagerInterface $entityManager,
    Request                $request,
  ): Response
  {
    /** @var User|null $user */
    $user = $this->getUser();

    if ($user === null) {
      return $this->redirectToRoute('app_login');
    }

    if ($user->isComplete()) {
      return $this->redirectToRoute('app_dashboard', ['employer' => $user->getEmployer()->getId()]);
    }

    $formBuilder = $this->createFormBuilder($user, [
      'data_class' => User::class
    ]);

    if ($user->hasRole(Role::AdminEmployer)) {
      if ($user->getPhoneNumber() === null) {
        $formBuilder->add('phoneNumber', TelType::class, [
          'label' => 'Numéro de téléphone',
          'required' => true, 'row_attr' => [
            'class' => 'fr-input-group'
          ],
          'label_attr' => [
            'class' => 'fr-label',
          ],
          'attr' => [
            'class' => 'form-control fr-input',
          ]]);
      }
      if ($user->getEmail() === null) {
        $formBuilder->add('email', EmailType::class, [
          'label' => 'E-mail',
          'required' => true, 'row_attr' => [
            'class' => 'fr-input-group'
          ],
          'label_attr' => [
            'class' => 'fr-label',
          ],
          'attr' => [
            'class' => 'form-control fr-input',
          ]]);
      }
    }

    $formBuilder->add('submit', SubmitType::class, [
      'label' => 'Enregistrer',
      'attr' => [
        'class' => 'fr-btn'
      ]
    ]);
    $form = $formBuilder->getForm();

    $form->handleRequest($request);
    if ($form->isSubmitted() && $form->isValid()) {

      $data = $form->getData();

      if (!$data instanceof User) {
        $form->addError(new FormError("Les données retournées par le formulaire ne sont pas valides"));
      } else {
        $entityManager->persist($data);
        $entityManager->flush();

        $this->addFlash(
          'success',
          'Votre compte a bien été mis à jour'
        );

        return $this->redirectToRoute('app_dashboard', ['employer' => $user->getEmployer()->getId()]);
      }
    }

    return $this->render('security/profile-completion.html.twig', [
      'error' => null,
      'form' => $form
    ]);
  }

  /**
   * Expose le fichier de sécurité
   * @return Response
   */
  #[Route(path: '.well-known/security.txt')]
  public function wellKnownSecurityTxt(): Response
  {
    $response = $this->render('security/security.txt.twig');
    $response->headers->set('Content-Type', 'text/plain');
    return $response;
  }
}
