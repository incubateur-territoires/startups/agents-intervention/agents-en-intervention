<?php

namespace App\Controller;

use App\Repository\RoadmapRepository;
use App\Service\MetricsService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

class HomeController extends AbstractController
{
    #[Route('/', name: 'app_home')]
    public function index(RoadmapRepository $repository): Response
    {
        return $this->render('public/index.html.twig', [
          'roadmap' => $repository->getHomepageRoadmap()
        ]);
    }

    #[Route('/mentions-legales', name: 'legal_mentions')]
    public function legal_mentions(): Response
    {
        return $this->render('public/legal-mentions.html.twig');
    }

    #[Route('/statistiques', name: 'stats')]
    public function stats(MetricsService $metricsService): Response
    {
        return $this->render('public/stats.html.twig', [
            'metrics' => $metricsService->getForMetricsPage()
        ]);
    }

    #[Route('/politique-de-confidentialite', name: 'privacy-policy')]
    public function privacy(): Response
    {
        return $this->render('public/privacy-policy.html.twig');
    }

    #[Route('/conditions-generales-d-utilisation', name: 'cgu')]
    public function cgu(): Response
    {
        return $this->render('public/cgu.html.twig');
    }

    #[Route('/accessibilité', name: 'accessibility')]
    public function accessibility(): Response
    {
        return $this->render('public/accessibility.html.twig');
    }
}
