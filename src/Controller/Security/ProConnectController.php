<?php

declare(strict_types=1);

namespace App\Controller\Security;

use KnpU\OAuth2ClientBundle\Client\ClientRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

#[Route('', name: 'app_security_pro_connect_')]
class ProConnectController extends AbstractController
{
    #[Route('/pro_connect/login', name: 'start')]
    public function start(ClientRegistry $clientRegistry): Response
    {
        return $clientRegistry
            ->getClient('pro_connect')
            ->redirect([], []);
    }

    /**
     * Callback route set in config/packages/knpu_oauth2_client.yaml
     * Intercepted by the ProConnectAuthenticator.
     */
    #[Route('/pro_connect/complete', name: 'callback')]
    public function callback(): never
    {
        throw new \LogicException('This controller should not be executed. Something went wrong in the configuration.');
    }
}
