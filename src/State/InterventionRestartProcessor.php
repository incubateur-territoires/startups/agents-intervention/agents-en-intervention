<?php

namespace App\State;

use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\ProcessorInterface;
use App\Entity\Intervention;
use App\Service\AuditTrailLogger;
use App\Workflow\TransitionGuesser;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\Attribute\Autowire;
use Symfony\Component\DependencyInjection\Attribute\Target;
use Symfony\Component\Workflow\WorkflowInterface;

readonly class InterventionRestartProcessor implements ProcessorInterface
{
  public function __construct(
    private AuditTrailLogger       $auditTrailLogger,
    private EntityManagerInterface $entityManager,
    #[Autowire('@api_platform.doctrine.orm.state.persist_processor')]
    private ProcessorInterface     $processor,
    private TransitionGuesser      $transitionGuesser,
    #[Target('intervention_publishing')]
    private WorkflowInterface      $workflow,
  )
  {
  }

  /**
   * @inheritDoc
   */
  public function process(mixed $data, Operation $operation, array $uriVariables = [], array $context = [])
  {
    if (!$data instanceof Intervention) {
      throw new \LogicException('Data must be an instance of Intervention');
    }

    /** @var ?Intervention $previousIntervention */
    $previousIntervention = array_key_exists('previous_data', $context) ? $context['previous_data'] : null;

    try {

      $intervention = $data;
      $transition = $this->transitionGuesser->guessTransitionFromSubject($this->workflow, $intervention);

      if ($transition === null) {
        throw new \LogicException('No transition found');
      }

      try {
        $this->workflow->apply($intervention, $transition->getName());
      } catch (\LogicException $e) {
        $changes = [
          'workflow' => 'intervention_denied',
          'transition' => $transition->getName(),
          'from' => $previousIntervention?->getStatusAsString() ?? 'unknown',
          'to' => $intervention->getStatus(),
          'comment' => $e->getMessage(),
        ];
        $this->auditTrailLogger->log('workflow_transition', $intervention::class, $intervention->getId(), $changes);
      }
    } catch (\LogicException $e) {
      $changes = [
        'workflow' => 'intervention_denied',
        'transition' => 'unknown',
        'from' => $previousIntervention?->getStatusAsString() ?? 'unknown',
        'to' => 'unknown',
        'comment' => $e->getMessage(),
      ];
      $this->auditTrailLogger->log('workflow_transition', $intervention::class, $intervention->getId(), $changes);
      throw $e;
    }

    $this->entityManager->persist($intervention);
    $this->entityManager->flush();

    /** @var Intervention */
    return $this->processor->process($intervention, $operation, $uriVariables, $context);
  }
}
