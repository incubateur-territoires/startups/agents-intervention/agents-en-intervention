<?php

declare(strict_types=1);

namespace App\State;

use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\ProcessorInterface;
use App\Entity\Intervention;
use App\Entity\Picture;
use App\Service\S3Service;
use Doctrine\Common\Collections\Collection;

/**
 * Le processor pour supprimer une intervention.
 */
class DeleteInterventionProcessor implements ProcessorInterface
{
    /**
     * @param ProcessorInterface $removeProcessor
     * @param S3Service $s3DeleteService le service pour la suppression.
     */
    public function __construct(private readonly ProcessorInterface $removeProcessor, private readonly S3Service $s3DeleteService)
    {
    }

    #[\Override]
    public function process(mixed $data, Operation $operation, array $uriVariables = [], array $context = []): void
    {
        if(!$data instanceof Intervention) {
          throw new \LogicException('DeleteInterventionProcessor :: can process only Intervention');
        }

        /** @var Collection<Picture> $pictures */
        $pictures = $data->getPictures();

        foreach ($pictures as $picture) {
            $this->s3DeleteService->deleteFile($picture->getFileName());
        }

        $this->removeProcessor->process($data, $operation, $uriVariables, $context);
    }
}
