<?php

namespace App\State;

use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\ProviderInterface;
use Symfony\Component\DependencyInjection\Attribute\Autowire;

readonly class InterventionProvider implements ProviderInterface
{
    public function __construct(
      #[Autowire('@api_platform.doctrine.orm.state.collection_provider')]
        private ProviderInterface $provider
    )
    {
    }

  public function provide(Operation $operation, array $uriVariables = [], array $context = []): object|array|null
    {
      dump($operation, $uriVariables, $context);
        // Retrieve the state from somewhere
      return $this->provider->provide($operation, $uriVariables, $context);
    }
}
