<?php

namespace App\State;

use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\ProcessorInterface;
use App\Dto\IndisponibiliteAgent;
use App\Entity\CalendarEvent;
use App\Entity\Role;
use App\Entity\User;
use App\Model\Frequency;
use Sabre\VObject\Component\VCalendar;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\DependencyInjection\Attribute\Autowire;
use Symfony\Component\HttpFoundation\Request;

class IndisponibiliteStateProcessor implements ProcessorInterface
{
  private const string YMD_T_HIS = 'Ymd\THis';

  public function __construct(
    #[Autowire('@api_platform.doctrine.orm.state.persist_processor')]
    private readonly ProcessorInterface $processor,
    private readonly Security           $security,
  )
  {
  }

  /**
   * @param mixed $data
   * @param Operation $operation
   * @param array<string, mixed> $uriVariables
   * @param array{request?: Request, previous_data?: mixed, resource_class?: string, original_data?: mixed} $context
   * @return CalendarEvent
   */
  public function process(mixed $data, Operation $operation, array $uriVariables = [], array $context = []): CalendarEvent
  {
    if (!$data instanceof IndisponibiliteAgent) {
      throw new \LogicException('Type de donnée incorrect');
    }

    $vcalendar = $this->generate($data);
    $event = new CalendarEvent();

    /** @var User $user */
    $user = $this->security->getUser();
    if ($user->hasRole(Role::Agent)) {
      $event->setCalendarId($user->getId() . '/indisponibilite');
    } else {
      $event->setCalendarId($data->getParticipant() . '/indisponibilite');
    }

    $event->setCalendarData($vcalendar->serialize());
    $event->setComponentType('VEVENT');
    $event->setFirstOccurence($data->getStartDate());
    $event->setLastOccurence($data->getRecEndDate() ?: $data->getEndDate());
    $event->setLastModified(new \DateTime());

    // This is the place for adding the calendar event to the CalDAV of the user

    /** @var CalendarEvent $event */
    $event = $this->processor->process($event, $operation, $uriVariables, $context);

    return $event;
  }

  /**
   * @param IndisponibiliteAgent $event
   * @return VCalendar
   */
  public function generate(IndisponibiliteAgent $event): VCalendar
  {
    $vevent = [
      'summary' => $event->getTitle(),
      'dtstart' => $event->getStartDate()->format(self::YMD_T_HIS),
      'dtend' => $event->getEndDate()->format(self::YMD_T_HIS),
    ];

    /*
     * UNTIL for an end date,
      INTERVAL for example "every 2 days",
      COUNT to stop recurring after x items,
      FREQ=DAILY to recur every day, and BYDAY to limit it to certain days,
      FREQ=WEEKLY to recur every week, BYDAY to expand this to multiple weekdays in every week and WKST to specify on which day the week starts,
      FREQ=MONTHLY to recur every month, BYMONTHDAY to expand this to certain days in a month, BYDAY to expand it to certain weekdays occurring in a month, and BYSETPOS to limit the last two expansions,
      FREQ=YEARLY to recur every year, BYMONTH to expand that to certain months in a year, and BYDAY and BYWEEKDAY to expand the BYMONTH rule even further.
     */

    if ($event->getFrequency() != Frequency::ONE_TIME) {
      $vevent['RRULE'] = 'FREQ=' . $event->getFrequency()->name . ';INTERVAL=' . $event->getFrequencyInterval();

      if ($event->getRecEndDate() !== null) {
        $vevent['RRULE'] .= ';UNTIL=' . $event->getRecEndDate()->format(self::YMD_T_HIS);
      }
    }

    return new VCalendar([
      'PRODID' => '-//ANCT//Agents en intervention//FR',
      'VEVENT' => $vevent
    ]);
  }
}
