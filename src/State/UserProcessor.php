<?php

namespace App\State;

use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\ProcessorInterface;
use App\Entity\User;
use App\Event\UserCreatedEvent;
use Psr\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

readonly class UserProcessor implements ProcessorInterface
{
  /**
   * Le constructeur.
   * @param ProcessorInterface $processor
   * @param UserPasswordHasherInterface $passwordHasher
   * @param EventDispatcherInterface $eventDispatcher
   */
  public function __construct(
    private ProcessorInterface          $processor,
    private UserPasswordHasherInterface $passwordHasher,
    private EventDispatcherInterface    $eventDispatcher,
  )
  {
  }

  #[\Override]
  public function process(mixed $data, Operation $operation, array $uriVariables = [], array $context = []): User
  {
    if (!$data instanceof User) {
      throw new \LogicException('UserProcessor can process only User class');
    }

    if (!$data->getId()) {
      $hashedPassword = $this->passwordHasher->hashPassword($data, $data->getPassword());
      $data->setPassword($hashedPassword);
      $data->eraseCredentials();
    }

    /** @var User $user */
    $user = $this->processor->process($data, $operation, $uriVariables, $context);

    $this->eventDispatcher->dispatch(new UserCreatedEvent($user));

    return $user;
  }
}
