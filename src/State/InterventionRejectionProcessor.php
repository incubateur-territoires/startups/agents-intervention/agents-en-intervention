<?php

namespace App\State;

use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\ProcessorInterface;
use App\Dto\InterventionRejectionInput;
use App\Entity\Comment;
use App\Entity\Intervention;
use App\Entity\Status;
use App\Entity\User;
use App\Repository\InterventionRepository;
use App\Service\AuditTrailLogger;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\DependencyInjection\Attribute\Target;
use Symfony\Component\Workflow\WorkflowInterface;

readonly class InterventionRejectionProcessor implements ProcessorInterface
{
  public function __construct(
    private AuditTrailLogger       $auditTrailLogger,
    private InterventionRepository $interventionRepository,
    private ProcessorInterface     $processor,
    private Security               $security,
    #[Target('intervention_publishing')]
    private WorkflowInterface      $workflow,
  )
  {
  }

  #[\Override]
  public function process(mixed $data, Operation $operation, array $uriVariables = [], array $context = []): Intervention
  {
    \assert($data instanceof InterventionRejectionInput);

    /** @var Intervention $intervention */
    $intervention = $this->interventionRepository->find($uriVariables['id']);

    if ($this->workflow->can($intervention, 'reject')) {
      $this->workflow->apply($intervention, 'reject');
    } else {
      $changes = [
        'workflow' => 'intervention_workflow',
        'transition' => 'reject',
        'from' => implode(',', array_keys($this->workflow->getMarking($intervention)->getPlaces())),
        'to' => Status::Rejected->value,
        'comment' => null,
      ];
      $this->auditTrailLogger->log('workflow_denied', $intervention::class, $intervention->getId(), $changes, flush: true);
      throw new \LogicException("You can't reject");
    }

    /** @var ?User $user */
    $user = $this->security->getUser();
    \assert($user !== null);

    /*
     * Ajout d'un commentaire qui permet de garder une trace de la raison du rejet
     */
    $footer = $data->reason === 'Besoin de plus d\'informations' ? "\nContactez votre service technique ou créez une nouvelle demande avec ces informations.\nBien cordialement," : '';
    $intervention->addComment((new Comment("Demande d'intervention refusée.\n $data->reason\n$data->details\n" . $footer, $intervention))->setAuthor($user));

    /** @var Intervention */
    return $this->processor->process($intervention, $operation, $uriVariables, array_merge($context, ['comment' => $footer]));
  }
}
