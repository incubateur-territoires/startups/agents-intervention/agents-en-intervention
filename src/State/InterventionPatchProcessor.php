<?php

namespace App\State;

use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\ProcessorInterface;
use App\Entity\Intervention;
use App\Entity\Status;
use App\Model\Frequency;
use App\Service\AuditTrailLogger;
use App\Service\RecurrenceService;
use App\Service\S3Service;
use App\Workflow\TransitionGuesser;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\Attribute\Target;
use Symfony\Component\Workflow\WorkflowInterface;

readonly class InterventionPatchProcessor implements ProcessorInterface
{
  /**
   * Le constructeur.
   * @param AuditTrailLogger $auditTrailLogger
   * @param ProcessorInterface $processor
   * @param S3Service $s3GetUrlService
   * @param RecurrenceService $recurrenceService
   * @param LoggerInterface $logger
   * @param WorkflowInterface $workflow
   * @param TransitionGuesser $transitionGuesser
   */
  public function __construct(
    private AuditTrailLogger    $auditTrailLogger,
    private ProcessorInterface  $processor,
    private S3Service           $s3GetUrlService,
    private RecurrenceService   $recurrenceService,
    private LoggerInterface     $logger,
    #[Target('intervention_publishing')]
    private WorkflowInterface   $workflow,
    private TransitionGuesser   $transitionGuesser,
  )
  {
  }

  #[\Override]
  public function process(mixed $data, Operation $operation, array $uriVariables = [], array $context = []): Intervention
  {
    /** @var Intervention $intervention */
    $intervention = $data;

    // Set intervention to $picture because before - on upload - the intervention does not exists
    foreach ($intervention->getPictures() as $picture) {
      $picture->setIntervention($intervention);
    }

    /** @var ?Intervention $previousIntervention */
    $previousIntervention = array_key_exists('previous_data', $context) ? $context['previous_data'] : null;

    \assert($previousIntervention !== null);

    // Gestion du workflow des demandes d'interventions qui deviennent interventions OU récurrence une fois validées (éditées par un directeur / coordinateur)
    // Seuls les directeurs/coordinateurs peuvent mettre à jour une intervention, c'est pourquoi il n'y a pas de vérification du rôle
    // Le status doit être calculé en fonction des nouvelles données d'entrées car cela peut être soit une récurrence soit une intervention ponctuelle
    $recurrenceStatusChanged = $intervention->isRecurring() !== $previousIntervention->isRecurring();
    if ($intervention->getStatus() === Status::ToValidate || $recurrenceStatusChanged) {
      $intervention->setStatus($intervention->getFrequency() === Frequency::ONE_TIME ? Status::ToDo : Status::Recurring);
    }

    if ($intervention->getStatus() !== $previousIntervention->getStatus()) {
      /**
       * Ici l'appel demande à changer le statut de l'intervention
       */
      $transition = $this->transitionGuesser->guessTransition($this->workflow, $previousIntervention->getStatusAsString(), $intervention->getStatusAsString());
      if ($transition && $this->workflow->can($previousIntervention, $transition->getName())) {
        $this->workflow->apply($previousIntervention, $transition->getName());
      } else {
        $changes = [
          'workflow' => 'intervention_denied',
          'transition' => $transition?->getName() ?? 'unknown',
          'from' => $previousIntervention->getStatusAsString(),
          'to' => $intervention->getStatusAsString(),
          'comment' => null,
        ];
        $this->auditTrailLogger->log('workflow_transition', $intervention::class, $intervention->getId(), $changes, flush: true);
        throw new \LogicException('Intervention ' . $intervention->getId() . ' can not change to ' . $intervention->getStatusAsString());
      }
    }

    if ($intervention->getStatus() === Status::Finished && !$intervention->getEndedAt()) {
      $intervention->setEndedAt(new \DateTimeImmutable());
    }
    /**
     * Bien positionner la date de début effective de l'intervention
     * lorsqu'elle passe dans l'état startedAt
     */
    if ($intervention->getStatus() === Status::InProgress && !$intervention->getStartedAt()) {
      $intervention->setStartedAt(new \DateTimeImmutable());
    }

    /** @var Intervention $intervention */
    $intervention = $this->processor->process($data, $operation, $uriVariables, $context);

    // TODO ? déporter ce code dans le listener de workflow ?
    if (!$intervention->equalsTo($previousIntervention)) {
      $this->logger->info('Intervention patch processing : intervention changed');

      $isFrequencyDifferent = $intervention->isFrequencyDifferent($previousIntervention);
      $isInformationDifferent = $intervention->isInformationDifferent($previousIntervention);
      $this->logger->info('Intervention patch processing : isFrequencyDifferent: ' . $isFrequencyDifferent);
      $this->logger->info('Intervention patch processing : isInformationDifferent: ' . $isInformationDifferent);

      // Si ce sont uniquement les informations hors fréquence qui sont modifiées, on met à jour les
      // interventions déjà générées (permet de garder les éventuelles modifications d'horaires, commentaires ou photos)
      if (!$isFrequencyDifferent && $isInformationDifferent) {
        // modify
        $this->logger->info('Intervention patch processing : only informations has changed, modify future interventions');
        $this->recurrenceService->modifyGeneratedInterventionsFor($intervention);
      } elseif ($isFrequencyDifferent) {
        // remove + add
        $this->logger->info('Intervention patch processing : frequency has changed, regenerate future interventions');
        $this->recurrenceService->removeGeneratedInterventionsFor($intervention, false);
        $this->recurrenceService->addGeneratedInterventionsFor($intervention);
      }
    }

    foreach ($intervention->getPictures() as $picture) {
      if ($this->s3GetUrlService->fileExist($picture->getFileName()) === false) {
        $intervention->removePicture($picture);
      }
    }

    return $intervention;
  }
}
