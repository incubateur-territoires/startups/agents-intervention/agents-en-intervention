<?php

declare(strict_types=1);

namespace App\State;

use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\ProviderInterface;
use App\Entity\User;
use Psr\Log\LoggerInterface;

final readonly class UserProvider implements ProviderInterface
{
  /**
   * Le constructeur.
   * @param ProviderInterface $itemProvider
   * @param LoggerInterface $logger
   */
    public function __construct(
        private ProviderInterface $itemProvider,
      private LoggerInterface $logger
    ) {
    }

    #[\Override]
    public function provide(Operation $operation, array $uriVariables = [], array $context = []): object|null
    {
        /**
         * @var User|null $user l'utilisateur.
         */
        $user = $this->itemProvider->provide($operation, $uriVariables, $context);

        if (!$user) {
          $this->logger->error('User not found by provider :: ', ['uriVariables' => $uriVariables]);
          return null;
        }

        return $user;
    }
}
