<?php

namespace App\State;

use ApiPlatform\Metadata\Operation;
use ApiPlatform\Metadata\Post;
use ApiPlatform\State\ProcessorInterface;
use App\Entity\Intervention;
use App\Entity\Role;
use App\Entity\User;
use App\Model\Frequency;
use Symfony\Bundle\SecurityBundle\Security;
use App\Entity\Status;
use Symfony\Component\DependencyInjection\Attribute\Target;
use Symfony\Component\Workflow\WorkflowInterface;

readonly class InterventionProcessor implements ProcessorInterface
{
  /**
   * Le constructeur.
   * @param ProcessorInterface $processor
   * @param Security $security
   * @param WorkflowInterface $workflow
   */
  public function __construct(
    private ProcessorInterface  $processor,
    private Security            $security,
    #[Target('intervention_publishing')]
    private WorkflowInterface   $workflow,
  )
  {
  }

  #[\Override]
  public function process(mixed $data, Operation $operation, array $uriVariables = [], array $context = []): Intervention
  {
    \assert($operation instanceof Post, new \LogicException('InterventionProcessor can be used only for Post operation'));
    \assert($data instanceof Intervention, new \LogicException('InterventionProcessor can be used only Intervention data'));

    /** @var User $user */
    $user = $this->security->getUser();

    $intervention = $data;
    $intervention->setStatus(Status::Draft);
    $intervention->setAuthor($user);

    if (!$this->security->isGranted(Role::Admin->value)) {
      $intervention->setEmployer($user->getEmployer());
    }

    if (Frequency::ONE_TIME === $intervention->getFrequency()) {
      $intervention->setEndedAt(null);
    }

    // Set intervention to $picture because before - on upload - the intervention does not exists
    foreach ($intervention->getPictures() as $picture) {
      $picture->setIntervention($intervention);
    }

    $transitions = $this->workflow->getEnabledTransitions($intervention);
    if (empty($transitions)) {
      throw new \LogicException('InterventionProcessor::process no transition are available for this intervention');
    }

    try {
      $this->workflow->apply($intervention, $transitions[0]->getName());
    } catch (\LogicException $e) {
      throw $e;
    }

    /** @var Intervention $ret */
    $ret = $this->processor->process($intervention, $operation, $uriVariables, $context);

    return $ret;
  }
}
