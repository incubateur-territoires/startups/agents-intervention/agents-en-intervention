<?php

namespace App\State;

use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\ProviderInterface;
use App\Entity\Employer;
use App\Repository\EmployerRepository;
use DateMalformedStringException;
use Doctrine\ORM\EntityManagerInterface;

readonly class MetricsProvider implements ProviderInterface
{
  public function __construct(
    private EntityManagerInterface $em,
    private EmployerRepository $employerRepository
  )
  {
  }

  /**
   * @param Operation $operation
   * @param array $uriVariables
   * @param array $context
   * @return array{'name': string, 'siren': string, 'nbInterventions': int, 'nbInterventions30Jours': int}[]
   * @throws DateMalformedStringException
   */
  #[\Override]
  public function provide(Operation $operation, array $uriVariables = [], array $context = []): array
  {
    /** @var Employer[] $employers */
    $employers = $this->employerRepository->findAll();
    $interventionsParEmployer = $this->interventionsParEmployer();
    // Interventions créées ces 30 derniers jours
    $interventionsParEmployer30Jours = $this->interventionsParEmployer30Jours();

    return array_map(
      fn(Employer $employer) => [
        'name' => $employer->getName(),
        'siren' => $employer->getSiren(),
        'nbInterventions' => array_key_exists($employer->getId() ?? -1, $interventionsParEmployer) ? $interventionsParEmployer[$employer->getId()] : 0,
        'nbInterventions30Jours' => array_key_exists($employer->getId() ?? -1, $interventionsParEmployer30Jours) ? $interventionsParEmployer30Jours[$employer->getId()] : 0,
      ],
      $employers
    );
  }

  /**
   * @return array<int, int>
   */
  private function interventionsParEmployer(): array {
    $ret = [];
    $dql = 'SELECT emp.id, count(intervention.id) as count FROM App\Entity\Intervention as intervention JOIN intervention.employer as emp GROUP BY emp.id';

    $query = $this->em->createQuery($dql);
    $results = $query->getResult();

    foreach($results as $v) {
      $ret[$v['id']] = $v['count'];
    }

    return $ret;
  }

  /**
   * @return array<int, int>
   * @throws DateMalformedStringException
   */
  public function interventionsParEmployer30Jours(): array {
    $ret = [];
    $date = new \DateTime();
    $date->modify('-30 days');

    $dql = 'SELECT emp.id, count(intervention.id) as count FROM App\Entity\Intervention as intervention JOIN intervention.employer as emp WHERE intervention.createdAt > :date GROUP BY emp.id ';

    $query = $this->em->createQuery($dql);
    $query->setParameter(':date', $date);
    $results = $query->getResult();

    foreach($results as $v) {
      $ret[$v['id']] = $v['count'];
    }

    return $ret;
  }
}
