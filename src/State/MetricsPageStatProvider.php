<?php

namespace App\State;

use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\ProviderInterface;
use App\Entity\Metrics;
use Doctrine\DBAL\ArrayParameterType;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;

readonly class MetricsPageStatProvider implements ProviderInterface
{
  const to_exclude = ['ANCT', 'ANSM', 'SILAB', 'Arcie-en-Val / GIP RECIA', 'SIDEC du Jura'];

  public function __construct(
    private EntityManagerInterface $em,
  )
  {
  }

  /**
   * @throws NonUniqueResultException
   * @throws NoResultException
   */
  #[\Override]
  public function provide(Operation $operation, array $uriVariables = [], array $context = []): object|array|null
  {
    $metrics = new Metrics();
    $metrics->nbCollectivites = $this->nbCollectivites();
    $metrics->nbCollectivitesActives = $this->nbCollectivitesActives();
    $metrics->nbUtilisateurs = $this->nbUtilisateurs();
    $metrics->nbInterventions = $this->nbInterventions();

    return $metrics;
  }

  private function nbCollectivitesActives(): int
  {
    $date = new \DateTime();
    $date->modify('-30 days');

    $dql = 'SELECT DISTINCT e.id FROM App\Entity\Intervention i JOIN i.employer e WHERE i.createdAt >= :date GROUP BY e.id, i.id HAVING count(i.id) <=3';
    $query = $this->em->createQuery($dql);
    $query->setParameter(':date', $date);
    return count($query->getResult());
  }

  /**
   * @throws NonUniqueResultException
   * @throws NoResultException
   */
  private function nbCollectivites(): int {
    $dql = 'SELECT count(e.id) FROM App\Entity\Employer e WHERE e.name NOT IN (:names)';
    $query = $this->em->createQuery($dql);
    $query->setParameter(':names', self::to_exclude, ArrayParameterType::STRING);
    return intval($query->getSingleScalarResult());
  }

  private function nbUtilisateurs(): int
  {
    $dql = 'SELECT count(u.id) FROM App\Entity\User u JOIN u.employer e  WHERE e.name NOT IN (:names)';
    $query = $this->em->createQuery($dql);
    $query->setParameter(':names', self::to_exclude, ArrayParameterType::STRING);
    return intval($query->getSingleScalarResult());
  }

  /**
   * @throws NonUniqueResultException
   * @throws NoResultException
   */
  private function nbInterventions(): int {
    $dql = 'SELECT count(i.id) FROM App\Entity\Intervention i JOIN i.employer e  WHERE e.name NOT IN (:names)';
    $query = $this->em->createQuery($dql);
    $query->setParameter(':names', self::to_exclude, ArrayParameterType::STRING);
    return intval($query->getSingleScalarResult());
  }
}
