<?php

namespace App\State;

use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\ProcessorInterface;
use App\Entity\Comment;
use App\Entity\User;
use Symfony\Bundle\SecurityBundle\Security;

readonly class CommentProcessor implements ProcessorInterface
{
  /**
   * @param ProcessorInterface $processor
   * @param Security $security
   */
  public function __construct(
    private ProcessorInterface  $processor,
    private Security            $security,
//    private NotificationService $notificationService
  )
  {
  }

  #[\Override]
  public function process(mixed $data, Operation $operation, array $uriVariables = [], array $context = []): Comment
  {
    /** @var Comment $comment */
    $comment = $data;
    /** @var User $user */
    $user = $this->security->getUser();
    $comment->setAuthor($user);

    /** @var Comment $comment */
    $comment = $this->processor->process($data, $operation, $uriVariables, $context);

//    $this->notificationService->sendNouveauCommentaire($comment);
    return $comment;
  }
}
