<?php

namespace App\Logger;

use App\Entity\User;
use Monolog\Attribute\AsMonologProcessor;
use Monolog\LogRecord;
use Monolog\Processor\ProcessorInterface;
use Symfony\Bundle\SecurityBundle\Security;

#[AsMonologProcessor]
class UserProcessor implements ProcessorInterface
{
  public function __construct(
    private readonly Security $security
  )
  {
  }

  // method is called for each log record; optimize it to not hurt performance
  #[\Override]
  public function __invoke(LogRecord $record): LogRecord
  {
    /** @var User|null $user */
    $user = $this->security->getUser();

    $id = $user ? 'userId:' . $user->getId() : 'anonymous';
    $record->extra['user_id'] = $id;
    if ($user) {
      $record->extra['employer_id'] = $user->getEmployer()->getId();
    }

    return $record;
  }
}
