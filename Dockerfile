ARG PHP_VERSION=8.4.3

FROM php:$PHP_VERSION-fpm

# Install nginx as a reverse proxy to target node and php-fpm
# it uses supervisord to manage concurrent programs
RUN mv "$PHP_INI_DIR/php.ini-production" "$PHP_INI_DIR/php.ini" \
    && echo 'apc.enable_cli = 1' >> "$PHP_INI_DIR/php.ini" \
    && apt update \
    # Install GD extension (https://hub.docker.com/_/php)
    && apt install -y \
          apt-utils \
       		libfreetype-dev \
          libjpeg-dev \
       		libjpeg62-turbo-dev \
    libgd-dev libavif-dev libwebp-dev \
       		libpng-dev \
          libpq-dev \
          libicu-dev \
          nginx \
          supervisor \
       	&& docker-php-ext-configure gd --with-freetype --with-jpeg --with-webp --with-webp --enable-gd \
       	&& docker-php-ext-install -j$(nproc) gd \
    # Install other mandatory extensions
    && docker-php-ext-install pdo pdo_pgsql opcache intl \
    && pecl install apcu excimer && docker-php-ext-enable apcu excimer \
    # Install NodeJS & npm \
    && curl -fsSL https://deb.nodesource.com/setup_23.x -o nodesource_setup.sh \
    && su -c "bash nodesource_setup.sh" \
    && rm -f nodesource_setup.sh \
    && apt install -y nodejs \
    && rm -rf /var/lib/apt/lists/*

WORKDIR /app

COPY . /app

# Install mjml to compile email templates
# [WORKAROUND] Symfony expects an `.env` file despite we don't want to pollute with local environment variables
# So creating an empty file so we can fully rely on environment variables injected with the deployment
RUN npm install mjml --legacy-peer-deps \
    && touch /app/.env \
    && rm -rf /app/var/cache \
    && mkdir -p /app/var/cache/prod  \
    && chown -R www-data:www-data /app/var/cache
#    && setfacl -R -m u:www-data:rwX /app/var \
#    && setfacl -dR -m u:www-data:rwX /app/var \
#    && su -c "chmod ugo+rwx -R /app/var"

COPY nginx.conf /etc/nginx/sites-available/default
COPY supervisord.conf /etc/supervisor/conf.d/supervisord.conf

# We do not rely on a dynamic port ($PORT) to avoid overlapping the fixed ones of our runtimes (3000 and 9000)
EXPOSE 80

CMD ["/usr/bin/supervisord", "-c", "/etc/supervisor/conf.d/supervisord.conf"]
