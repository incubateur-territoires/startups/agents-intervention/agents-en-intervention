# Agents en intervention

[![Minimum PHP version](https://img.shields.io/badge/php-%3E%3D8.3-%23777BB4?logo=php&style=flat)](https://www.php.net/)
[![pipeline status](https://gitlab.com/incubateur-territoires/startups/agents-intervention/api/badges/main/pipeline.svg)](https://gitlab.com/incubateur-territoires/startups/agents-intervention/api/-/commits/main)
[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=agents-en-intervention_app&metric=alert_status)](https://sonarcloud.io/summary/new_code?id=agents-en-intervention_app)
[![Symfony](https://img.shields.io/badge/symfony-%23000000.svg?style=for-the-badge&logo=symfony&logoColor=white)](https://symfony.com/)
[![Système de Design de l’État](https://img.shields.io/badge/-Syst%C3%A8me%20de%20Design%20de%20l%E2%80%99%C3%89tat-000091?style=for-the-badge)](https://www.systeme-de-design.gouv.fr/)

Agents en intervention est une startup d'État portée par l'Incubateur des Territoires de l'[Agence Nationale de la Cohésion des Territoires](https://agence-cohesion-territoires.gouv.fr/) et [beta.gouv.fr](https://beta.gouv.fr).

Le site https://agentsenintervention.anct.gouv.fr/ est une solution numérique gratuite conçue avec et pour les collectivités, afin de faciliter la coordination des interventions techniques sur le terrain.
Sur le site, vous pourrez retrouver les fonctionnalités disponibles et les évolutions à venir du service.

## Statistiques

[![Matomo](https://a11ybadges.com/badge?logo=matomo)](https://matomo.incubateur.anct.gouv.fr/)

Les statistiques de l'utilisation du service sont disponibles sur [la page statistique du service](https://agentsenintervention.anct.gouv.fr/statistiques) et sur [Données et Territoires](https://catalogue-indicateurs.donnees.incubateur.anct.gouv.fr/indicateurs/incubateur?territoires=p:fr).

## Licence

Ce projet est sous licence [MIT](./LICENSE).

## Documentation

La documentation se trouve dans le dossier `./doc/`.

- [Guide de démarrage](./docs/getting-started.md)
- [Document d'architecture technique](./docs/architecture-technique.md)
- [Intégration continue](./docs/continous-integration.md)
