# Guide de démarrage

## Installation

Télécharger le projet :

```shellsession
user@host ~$ cd [CHEMIN_OU_METTRE_LE_PROJET] # Exemple : ~/projets/
user@host projets$ git clone https://gitlab.com/incubateur-territoires/startups/agents-intervention/api.git
```

### Ajouter les variables d'environnement

Un certain nombre de variables d'environnement doivent être définies dans le fichier `.env.local`.
Rapprochez-vous d'un membre de l'équipe pour qu'il vous les communique.

```
APP_ENV=dev
APP_SECRET
DATABASE_URL
#CORS_ALLOW_ORIGIN
#CORS_ALLOW_ORIGIN
S3_ENDPOINT
S3_ACCESS_KEY
S3_SECRET_KEY
S3_REGION
S3_NAME
SCW_ACCESS_KEY
SCW_SECRET_KEY
SCW_DEFAULT_ORGANIZATION_ID
SCW_DEFAULT_PROJECT_ID
SENTRY_DSN
FRONT_URL
WONDERPUSH_ACCESS_TOKEN
WONDERPUSH_APPLICATION_ID
WONDERPUSH_NOUVELLE_DEMANDE
WONDERPUSH_INTERVENTION_BLOQUEE
WONDERPUSH_INTERVENTION_TERMINEE
WONDERPUSH_INTERVENTION_TERMINE_ELU
WONDERPUSH_INTERVENTION_REFUSEE
WONDERPUSH_NOUVEAU_COMMENTAIRE
ZAPIER_ENDPOINT
JWT_SECRET_KEY_BASE64
JWT_PUBLIC_KEY_BASE64
JWT_PASSPHRASE
```

### Installer les dépendances PHP

Définir la configuration de la base de données pour API Platform (voir `./.env`)
et installer les dépendances PHP :

```shell
docker compose run --rm api composer install -o [--no-dev]
```

L'option `--no-dev` est pour l'environnement de production.

### Installer les dépendances Javascript

```shell
yarn install
```

### Créer les clés de sécurité pour le jeton d'authentification

Il est nécessaire d'être authentifié pour utiliser les routes de l'API.

Lors de la connexion d'un utilisateur, un token JWT est généré puis stocké en session afin d'être injecté dans le javascript et utilisé pour les appels à l'API.

Les jetons sont générés à partir de clés de sécurité qu'il faut créer avec la commande :

```shell
docker compose run --rm api bin/console lexik:jwt:generate-keypair
```

### Créer la base de données

```shellsession
user@host api$ docker compose run --rm api ./bin/console doctrine:database:create [-e test]
user@host api$ docker compose run --rm api ./bin/console make:migration [-e test]
user@host api$ docker compose run --rm api ./bin/console doctrine:migrations:migrate [--no-interaction] [-e test]
```

L'option "-e test" est pour l'environnement de test qui utilise Sqlite (et donc des migrations propres à cette base).

### Ajouter des données aléatoires (fixtures)

Après avoir créé la base de données, il est possible d'ajouter des données aléatoires via :

```shell
docker compose run --rm api bin/console hautelook:fixtures:load
```

## Utilisation

Une fois l'installation terminée, il est possible de démarrer les conteneurs avec :

```shell
docker compose up -d
```

L'api sera disponible dans le navigateur via : http://localhost:3002/

Pour arrêter les conteneurs :

```shell
docker compose down
```

## Développement

Pour ajouter des commandes/alias au conteneur via un fichier `./.ashrc`,
on peut copier le fichier exemple `./.ashrc.dist` :

```shell
cp ./.ashrc.dist ./.ashrc
```

Et ensuite, le monter dans le conteneur grâce au fichier `./docker-compose.override.yml`
à créer :

```yaml
services:
  api:
    volumes:
      - ./.ashrc:/root/.ashrc
```

Les fichiers `./.ashrc` et `./docker-compose.override.yml` sont ignorés par git.

### Commandes/alias par défaut

> Toutes les commandes ci-dessous ne sont pas 100% fonctionnelles à ce jour (modification de l'architecture récente)

Le fichier `./.ashrc.dist` contient les éléments suivants :

- `migrate` : pour créer les migrations;
- `migratetest` : pour créer les migrations de test;
- `fixture` : pour ajouter les fixtures à la base de données;
- `psalm` : pour exécuter Psalm (analyse statique);
- `phpunit` : pour exécuter PHPUnit (test);
- `infection` : pour exécuter Infection (mutation de code);
- `phpdoc` : pour exécuter PHPDoc (documentation de code);
- `phpcbf` : pour indenter le code à la norme PSR-12;
- `phpcs` : pour vérifier l'indentation du code à la norme PSR-12;
- `ci` : pour exécuter les outils d'intégration continue.
