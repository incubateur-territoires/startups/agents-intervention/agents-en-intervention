# Cycle de vie d'une intervention

Le cycle de vie d'une intervention (son statut) est géré par un workflow Symfony.

![présentation du cycle d'une intervention'](./intervention_workflow.png)

## Mettre à jour ce schéma

```shell
php bin/console workflow:dump intervention_publishing | dot -Tpng -o docs/intervention_workflow.png
```
