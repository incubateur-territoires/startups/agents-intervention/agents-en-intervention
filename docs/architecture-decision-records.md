# Décisions d'architecture

En préambule, cette application existe depuis 2 ans environ, elle a déjà été refondue depuis une architecture Symfony / API Platform / NuxtJS (VueJS) vers Symfony / API Platform / NextJS (React).
La première refonte a eu lieu en août 2023.
Le choix de garder une pile distinct entre la partie serveur et cliente a été fait au regard de l'équipe en place au moment de la refonte : le développeur backend était spécialisé PHP et le développeur qui a initié la refonte souhaitait aller vers une pile Node.

Ensuite, l'équipe a été réduite à un (nouveau) développeur durant 1 an dès septembre 2023.

## Décision septembre 2024 : refonte pour limiter les serveurs

**Qui décide ?**

Sylvain LE GLEAU, développeur unique sur l'application depuis septembre 2023.

**Quel est le contexte ?**

L'application a gagné en fonctionnalité et en complexité depuis sa refonte précédente.

L'architecture proposée a été respectée.

Cependant, des problèmes de regressions, de lourdeurs de maintenance et de lenteurs dans l'implémentation de certaines fonctionnalités se font ressentir.

De plus, un problème de livraison sur l'environnement de pré-production a mis en risque une livraison importante début septembre 2024 : l'hébergeur Scaleway limitait (ce n'est plus de cas depuis début octobre mais le choix avait déjà été fait et les travaux bien avancés) à 1Go la taille de l'image des containers déployables.
L'application se déployait mais, de façon quasi-systématique, ne fonctionnait pas ensuite.
Avec Oscar qui gère l'ops dans l'incubateur, nous avons mené des travaux d'urgence pour diminuer la taille de l'image afin de pouvoir faire la livraison et nous donner du temps pour régler le problème de façon plus définitive.

Ces deux constats m'ont décidé à mener un test de rationalisation de la stack technique.

=> voiçi la description de l'incident chez l'hébergeur https://console.scaleway.com/support/tickets/1460366

Note : l'incident semble être plutôt résolu en passant sur la sandbox v2 de l'hébergeur, mais le constat de la taille trop importante de l'image Docker reste valable.

**Quelles sont les options que nous avons identifiées ?**

Les deux options sont de basculer vers une seule stack technique.

1. Basculer vers Symfony
2. Basculer vers NextJS / RSS

**Quels sont les avantages et inconvénients pour chaque option ?**

1. (-) Il faut rapatrier les pages du site, le code React associé et adapter le système de navigation au monde Symfony / Stimulus (+) la majeure partie du code métier est déjà écrit et stable ; réduction de l'outillage nécessaire pour compiler et faire tourner Javascript
2. (-) Il faut réécrire l'ensemble du code métier, d'accès aux données dans la base et API dans le monde NextJS (+) unification de la stack entière avec Typescript, moins de problèmes de typages lors des appels API

Dans les deux cas, l'architecture de l'application se simplifie considérablement.
L'outillage est limité à un framework.

**Quelle décision est prise et selon quels critères ?**

La décision prise est le choix 1., bascule vers Symfony.
Les critères retenus :

- le code métier ne change pas
- les logiques et les éléments de sécurité des données peuvent être réutilisés facilement
- certaines pages nécessitant plusieurs appels API peuvent être remplacées par des pages classiques (html / twig), certaines pages avec formulaires sont plus simples à gérer de même

**Quelles sont les conséquences de cette décision ?**

La migration de cette stack a pris moins de deux semaines de travaux, permis de résoudre des bugs latents et permis d'accélérer certains développements prévus au backlog.
Le déploiement ne contient plus qu'une technologie et donc un serveur au lieu des deux précédemment.
L'impact sur les utilisateurs est limité, car presque tout a pu être repris à l'identique.

## Décision octobre 2024 : suppression de la dépendance à MUI

**Par Sylvain Le Gléau**

Pour alléger le poids de l'application, j'ai décidé de supprimer la dépendance à MUI.
J'ai décidé de recenter l'application sur les composants de base du DSFR et de la librairie react-dsfr et de les personnaliser pour qu'ils soient plus légers.

## Décision décembre 2024 : mise en place d'un worker pour traiter les tâches asynchrones

**Par Sylvain Le Gléau**

Pour améliorer la réactivité de l'application, j'ai décidé de mettre en place un worker pour traiter les tâches asynchrones.
L'implémentation est celle de Symfony Messenger.

Les événements suivants sont traités de façon asynchrone :

- l'envoi de mail
- messages vers Mattermost
- messages vers Zapier
- envoi des notifications
