# performances

## Taille des bundles Encore

Au 17/10/2024 :

```

Entrypoint app [big] 17.9 MiB (1.34 MiB) = 7 assets 668 auxiliary assets
Entrypoint home [big] 15.8 MiB (6.22 KiB) = 8 assets 6 auxiliary assets
Entrypoint dashboard [big] 17.9 MiB (1.34 MiB) = 7 assets 668 auxiliary assets
Entrypoint map [big] 14.9 MiB (6.22 KiB) = 6 assets 6 auxiliary assets
webpack compiled with 1 error

```

Au 18/10/2024 :

```

Entrypoint app [big] 17.1 MiB (1.34 MiB) = 7 assets 668 auxiliary assets
Entrypoint home [big] 15 MiB (6.22 KiB) = 8 assets 6 auxiliary assets
Entrypoint dashboard [big] 17.2 MiB (1.34 MiB) = 8 assets 668 auxiliary assets
Entrypoint map [big] 14.1 MiB (6.22 KiB) = 6 assets 6 auxiliary assets

```

Au 22/10/2024 :

dev:

```
Entrypoint app [big] 14.4 MiB (1.34 MiB) = 8 assets 668 auxiliary assets
Entrypoint home [big] 11.9 MiB (6.22 KiB) = 8 assets 6 auxiliary assets
Entrypoint dashboard [big] 3.07 MiB (1.33 MiB) = runtime.js 39.3 KiB vendors-node_modules_codegouvfr_react-dsfr_dsfr_dsfr_css-node_modules_codegouvfr_react-dsfr_d-f06dfb.css 2.97 MiB dashboard.css 58.2 KiB dashboard.js 3.08 KiB 662 auxiliary assets
Entrypoint map 86.2 KiB (3.34 KiB) = runtime.js 39.3 KiB vendors-node_modules_leaflet_dist_leaflet_css.css 43.1 KiB map.css 1.34 KiB map.js 2.47 KiB 3 auxiliary assets
webpack compiled successfully
```

prod:

```
Entrypoint app [big] 2.22 MiB (2.88 KiB) = 7 assets 3 auxiliary assets
Entrypoint home [big] 1.31 MiB (2.88 KiB) = 6 assets 3 auxiliary assets
Entrypoint dashboard [big] 1010 KiB = runtime.d5ec9b77.js 4.37 KiB 883.87f4ddbb.css 991 KiB dashboard.47332c30.css 17.4 KiB dashboard.3c770617.js 144 bytes
Entrypoint map 15.4 KiB = runtime.d5ec9b77.js 4.37 KiB map.b8c072c9.css 11 KiB map.2c1bd59d.js 110 bytes
```
