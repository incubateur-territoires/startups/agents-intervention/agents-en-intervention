# Dossier technique

> Ce dossier a pour but de présenter l’architecture technique du SI. Il n’est par conséquent ni un dossier
> d’installation, ni un dossier d’exploitation ou un dossier de spécifications fonctionnelles.

**Nom du projet :** Agents en intervention

**Dépôt de code :** https://gitlab.com/incubateur-territoires/startups/agents-intervention/agents-en-intervention

**Hébergeur :** Scaleway, Paris

**Décision d’homologation :** !!<date>!!

**France Relance :** ❌

## Suivi du document

> Le suivi de ce document est assuré par le versionnage Git.

## Fiche de contrôle

> Cette fiche a pour vocation de lister l’ensemble des acteurs du projet ainsi que leur rôle dans la rédaction de ce
> dossier.

| Organisme                  | Nom               | Rôle            | Activité  |
| -------------------------- | ----------------- | --------------- | --------- |
| Agents en intervention     | Sylvain Le Gléau  | Lead tech       | Rédaction |
| Agents en intervention     | Alexandre Joubert | Product Manager | Relecture |
| Incubateur des territoires | Nicolas Ritouet   | CTO             | Relecture |
| Incubateur des territoires | Florian Busi      | Consultant SSI  | Relecture |

## Description du projet

Favoriser un espace public mieux entretenu et sécurisé en outillant les équipes techniques des collectivités pour
coordonner leurs actions de façon plus confortable et efficace

Améliorer la coordination des services techniques en leur proposant une vue partagée de leur activité et une gestion
facilitée de l’intervention de son signalement jusqu’à sa résolution.

Il est open source, bien que toutes les instances soient gérées par l'équipe.

Plus d'infos sur la fiche beta : https://beta.gouv.fr/startups/agents.en.intervention.html

## Architecture

### Stack technique

Le projet est un monolithe PHP / Symfony avec une base PostgreSQL pour les données métier et un S3 pour les fichiers /
images. L'infrastructure est entièrement gérée par Scaleway en PaaS et automatisée avec Terraform.

Le projet contient une grande partie de Javascript/Typescript pour avoir une partie connectée plus dynamique et le HTML
est généré côté serveur (utilisée au maximum lorsque les actions sont simples), mais une API est aussi mise à
disposition de
la partie JS. Coté CSS / composants, c'est principalement le Design System de l'état (DSFR) qui est utilisé.

Ces choix reflètent un désir de simplicité avant tout, afin de rester agiles et se concentrer sur la valeur métier.

### Matrice des flux

#### Applications métier sur Scalingo

| Source           | Destination       | Protocole | Port | Localisation | Interne/URL Externe |
| ---------------- | ----------------- | --------- | ---- | ------------ | ------------------- |
| Navigateur       | App PHP           | HTTPS     | 443  | Paris        | Externe             |
| Clients API JSON | App PHP           | HTTPS     | 443  | Paris        | Externe             |
| App PHP          | Postgres Scaleway | TCP       | 5432 | Paris        | Interne             |
| App PHP          | S3 Scaleway       | TCP       | 443  | Paris        | Interne             |

#### Tooling (error monitoring, APM)

| Source  | Destination | Protocole | Port | Localisation  | Interne/URL Externe     |
| ------- | ----------- | --------- | ---- | ------------- | ----------------------- |
| App PHP | Sentry      | HTTPS     | 443  | Tours, France | sentry.anct.cloud-ed.fr |

#### Services externes

| Source          | Destination    | Protocole | Port | Localisation  | Interne/URL Externe               |
| --------------- | -------------- | --------- | ---- | ------------- | --------------------------------- |
| App PHP         | WonderPush     | HTTP      | 443  | Paris, France | \*.wonderpush.com                 |
| App PHP         | API adresse    | HTTPS     | 443  | Paris, France | api-adresse.data.gouv.fr          |
| App PHP         | Matomo         | HTTPS     | 443  | France        | matomo.incubateur.anct.gouv.fr    |
| App PHP + Front | API Entreprise | HTTPS     | 443  | France        | recherche-entreprises.api.gouv.fr |
| App PHP         | API Nominatim  | HTTPS     | 443  | France        | nominatim.openstreetmap.org       |

#### Fournisseurs d'identité

| Source     | Destination | Protocole | Port | Localisation  | Interne/URL Externe |
| ---------- | ----------- | --------- | ---- | ------------- | ------------------- |
| Navigateur | Local       | HTTPS     | 443  | Paris, France | n/a                 |
| Navigateur | ProConnect  | HTTPS     | 443  | Paris, France | ?                   |

### Inventaire des dépendances

| Nom de l’applicatif | Service                   | Version   | Commentaires                                  |
| ------------------- | ------------------------- | --------- | --------------------------------------------- |
| Serveur web         | PHP                       | PHP 8.3   | Voir ci-dessous pour le détail des librairies |
| Serveur web         | Symfony                   | Symfony 7 | Voir ci-dessous pour le détail des librairies |
| BDD métier          | PostgreSQL                | `15`      | Stockage des données                          |
| Object storage      | Object storage - Scaleway | N/A       | Stockage des images et fichiers               |

La liste des librairies Ruby est disponible dans :

- [composer.json](/composer.json) pour la liste des dépendances directes et la description de la fonctionnalité de
  chacune des dépendances
- [composer.lock](/composer.lock) pour la liste complète des gems utilisées directement et indirectement (dépendances
  indirectes), et leurs versions précises

La liste des librairies JS utilisée est disponible dans :

- [package.json](/package.json) pour la liste des dépendances directes
- [yarn.lock](/yarn.lock) pour la liste complète des librairies JS utilisées directement et indirectement (dépendances
  indirectes), et leurs versions précises

### Schéma de l’architecture

Notre application environnement de production est architecturé de cette manière :

```mermaid
flowchart TD
    %% Domaine de prod
    agents-en-intervention.anct.gouv.fr

    %% Domaines de demo
    preprod.agents-en-intervention.anct.gouv.fr

    %% Domaines de staging
    dev.agents-en-intervention.anct.gouv.fr

    %% App Scaleway
    agents-intervention-prod
    agents-intervention-preprod
    agents-intervention-dev

    %% Relations domaine -> app
    agents-en-intervention.anct.gouv.fr --> agents-intervention-prod
    preprod.agents-en-intervention.anct.gouv.fr --> agents-intervention-preprod
    dev.agents-en-intervention.anct.gouv.fr --> agents-intervention-dev
```

#### Architecture interne à Scalingo

```mermaid
C4Container
    Person(user, "Utilisateur⋅ice", "Agent / usager")

    Container_Boundary(scaleway, "Scaleway") {
        Container(web_app, "PHP", "Application web + API JSON")
        ContainerDb(postgres, "PostgreSQL", "Données métier + sessions")
        ContainerDb(s3, "S3", "Fichiers")
    }

    Rel(user, web_app, "HTTPS")
    Rel(web_app, postgres, "TCP")
    Rel(web_app, s3, "TCP")

    UpdateRelStyle(web_app, redis, $offsetY="20", $offsetX="150")
```

#### Échanges entre l'app et les services externes

```mermaid
C4Container
    System(web_app, "PHP", "Application web + API JSON")

    System_Ext(sentry, "Sentry", "Error monitoring")

    System_Ext(wonderpush, "WonderPush", "Notifications web")

    System_Ext(api_entreprise, "API Entreprise", "Recherche de collectivités depuis un SIRET")
    System_Ext(api_nominatim, "API Nominatim", "Recherche de coordonnées géographiques depuis un nom de commune  (fallback API Entreprise)")

    Rel(web_app, sentry, "HTTPS")
    Rel(web_app, wonderpush, "HTTPS")
    Rel(web_app, api_entreprise, "HTTPS")
    Rel(web_app, api_nominatim, "HTTPS")
```

### Gestion DNS

C'est **Scaleway (scaleway.com)** qui fournit nos noms de domaine et la gestion DNS.

Nous y gérons les domaines suivants :

- `agents-en-intervention.anct.gouv.fr` : domaine de production
- `preprod.agents-en-intervention.anct.gouv.fr` : domaine de qualification et de démo (pré-production)
- `dev.agents-en-intervention.anct.gouv.fr` : domaine de staging (développement)

### Schéma des données

Lancer `bin/console doctrine:diagram` pour obtenir un PNG de l'état actuel des tables Postgres. Les fichiers entités
`src/Entity/*` donnent aussi une description des tables. Attention, cette commande nécessite d'avoir un serveur plantUML
lancé (https://jawira.github.io/doctrine-diagram-bundle/installing.html#installing-you-own-plantuml-server).

Ce fichier n'est pas versionné.

## Exigences générales

### Accès aux serveurs et sécurité des échanges

Les serveurs (applicatif et base de données) sont gérés par Scaleway. Scaleway fournit un système de rôle qui permet de
gérer finement les droits des utilisateurs. Les développeurs ne peuvent, par exemple, pas modifier directement
l'application par l'interface, mais uniquement par Terraform.

Nous avons actuellement 3 apps Scaleway :

- agents-intervention-prod
- agents-intervention-preprod
- agents-intervention-dev

**Scaleway propose du 2FA par TOTP, mais aucun mécanisme ne force les collaborateurs à l'activer. Nous avons donc dans
notre checklist d'onboarding un point précisant qu'il faut impérativement activer le 2FA.**

#### Détection de fuite de secrets

Nous avons activé la fonctionnalité "Secret scanning" de GitLab sur notre dépôt. Ce système envoie des alertes si des
secrets sont détectés dans un commit.

### Authentification, contrôle d’accès, habilitations et profils

L'application a 5 types d'utilisateurs :

- demandeur⋅euse
- agent⋅e
- coordinateur⋅trice
- admin collectivité
- super admin ANCT

Les sessions sont déconnectées automatiquement au bout d'une journée.

#### Les demandeurs

Les demandeurs ont accès aux fonctionnalités :

- déclaration d'une demande d'intervention
- visualisation et suivi de ses demandes

#### Les agents

Les agents ont accès à diverses fonctionnalités touchants à :

- création d'interventions
- gestion d'intervention
- visualisation des statistiques
- visualisation de son planning
- visualisation de sa carte d'interventions

La connexion à un profil agent est faite par email + mot de passe. Les mots de passes sont stockés salés et chiffrés
(en utilisant les préconisations de la dernière version de Symfony). Une connexion via ProConnect est en étude et
devrait arriver en fin d'année.

#### Coordinateur⋅trice

Ce rôle permet de gérer toutes les données de la collectivité à l'exception de l'équipe :

- les fonctionnalités de l'agent (mais avec la vue sur toutes les interventions)
- visualisation des statistiques
- visualisation, acceptation, refus des demandes

#### Admin collectivité

Ce rôle est unique pour une collectivité, il correspond au premier compte créé en même temps que la collectivité. Ce
compte a les mêmes droits que les coordinateurs plus :

- gestion de la collectivité
- gestion de l'équipe de la collectivité

#### Les super admins ANCT

Ce sont des administrateurs transverses à l'application.
Ils peuvent se positionner sur la collectivité qu'ils souhaitent en ayant les mêmes droits que les Admins collectivité.
Ils disposent en plus d'un accès à une interface de gestion dédiée pour certaines opérations.

### Traçabilité des erreurs et des actions utilisateurs

#### Logs textuels

Les logs textuels sont écrits dans le système de log de Scaleway. Cela comprend :

- Les logs des commandes de la production (e.g. lancer la console)
- les changements de variables d'environnements

Les logs applicatifs (générés par Symfony) contiennent, pour chaque requête HTTP :

- timestamp
- path HTTP
- méthode HTTP
- format (HTML, JSON)
- controller + action
- id de l'utilisateur connecté

La consultation des logs textuels ne se fait que lors d'investigations de bugs.
Leur usage est donc uniquement ponctuel et leur consultation est manuelle.
Nous n'avons pas de système d'analyse de logs.

#### Traçabilité applicative / auditing

À mettre en place selon les préconisations de l'équipe transverse.

#### Monitoring d'erreur

Nous utilisons Sentry afin d'être informé⋅es sur les nouvelles erreurs, et le volume des erreurs existantes.
Nous sommes alerté⋅es en cas de nouvelles erreurs ou volume inhabituel, en direct par email.
Nous utilisons l'instance Sentry de l'incubateur ANCT (https://sentry.anct.cloud-ed.fr/).

### Politique de mise à jour des applicatifs

Voici les cas dans lesquels nous mettons à jour une librairie spécifique :

- une version plus récente corrige une faille de sécurité (nous utilisons Dependabot pour être prévenu⋅es)
- une version plus récente permet de répondre à un besoin technique ou fonctionnel
- une montée de version est requises par une librairie correspondant aux critères ci-dessus (autrement dit, nous devons
  mettre à jour de manière indirecte)
- régulièrement, nous mettons à jour les packages vers leur dernier "patch level", afin d'être proactif sur les fixes de
  sécurité et de bugs

[//]: #
[//]: # 'La décision a été prise le 24 avril 2023, voir log de décision'
[//]: # 'ici : [2023-04-24-politique-maj-gems.md](/docs/decisions/2023-04-24-politique-maj-gems.md)'
[//]: # "Afin d'être prévenus lors de la publication d'une CVE, nous utilisons Dependabot sur notre dépôt GitHub."
[//]: # 'Une alerte e-mail est envoyée aux devs qui watchent le dépôt (et nous faisons en sorte de le watch à travers'
[//]: # "notre procédure d'onboarding)."

### Détection des vulnérabilités

Nous utilisons l'outil mis en place dans notre CI pour les suivre.

### Intégrité

Des backups de nos bases de données Postgres et Redis sont faîtes automatiquement par Scaleway.
Ces backups sont créés quotidiennement.

[//]: # 'Nous les testons régulièrement en les téléchargeant et en les chargeant dans notre environnement local.'
[//]: # "Scalingo nous offre également la fonctionnalité de Point-in-time Recovery. Nous profitons également d'un système"
[//]: # "de cluster avec 2 nodes, qui permet un failover automatique en cas de plantage d'une instance Postgres."

### Confidentialité

**L'application est concue pour demander le minimum d'informations personnelles aux utilisateurs. La politique de
confidentialité et des conditions générales d'utilisations définissent les responsabilités de chacun concernant les
données maniuplées.**

Parmi les données que nous manipulons, les plus critiques sont :

- la description et les commentaires sur une intervention, ce sont des champs texte libre où les utilisateurs peuvent
  saisir des informations de contexte sur un RDV
- les fichiers photos joints qui peuvent divulguer des informations sur les accès à un bâtiment notamment

### Bonnes pratiques de sécurité au sein de l'équipe

Lors de l'accueil d'un nouveau membre de l'équipe, on le forme à plusieurs bonnes pratiques de sécurité :

- Utiliser des outils sécurisés pour conserver ses mots de passe et s'échanger des informations confidentielles (
  Bitwarden, PGP, ...)
- Prendre l’habitude de verrouiller son écran dès qu’on est dans un lieu public (coworking, bureaux à Ourq, etc).

[//]: #
[//]: # '#### Suppression automatique de données anciennes'
[//]: #
[//]: # 'Voici les suppressions automatiques mises en place :'
[//]: #
[//]: # '- Suppression des RDVs de plus de 2 ans'
[//]: # "- Suppression des plages d'ouverture de plus de 1 an"
[//]: # "- Suppression des logs PaperTrail (auditing) de plus de 1 an contenant des données personnelles autres que l'identité de"
[//]: # "  la personne dont on journalise l'action."
[//]: # "- Anonymisation des logs PaperTrail(auditing) de plus de 1 an ne contenant pas données personnelles autre que l'identité"
[//]: # "  de la personne dont on journalise l'action"
[//]: # '- Suppression des logs PaperTrail (auditing) de plus de 5 ans'
[//]: # '- Suppression des usagers inactifs pendant au moins 2 ans (pas de rdv dans les 2 dernières années, et compte créé depuis'
[//]: # '  plus de 2 ans)'
[//]: # '- Suppression des agents inactifs pendant au moins 2 ans (pas de rdv dans les 2 dernières années, et compte créé depuis'
[//]: # '  plus de 2 ans, pas de connexion depuis 2 ans)'
