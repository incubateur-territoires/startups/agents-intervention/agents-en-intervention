# Intégration continue

## GitLab

### Branche par défaut

La branche par défaut est la branche `dev`.

### Branches spéciales

1. Branche: `main`

- branche contenant le code en production

2. Branche: `dev`

- branche contenant les développements en cours pour recette technique

3. Branches: `release/*`

- branches contenant les développements qui ont été validés pour une mise en production et qui doivent être validés de façon définitive

## Tests

Pour exécuter les tests :

```shellsession
user@host api$ docker compose run --rm api ./vendor/bin/phpunit -c ./ci/phpunit.xml
```

La commande va générer des fichiers dans `./ci/phpunit/`.
Le fichier `./ci/phpunit/html/index.html` montre la couverture de code
et `./ci/phpunit/testdox.html` affiche une liste détaillée des tests qui passent / échouent.

[//]: # 'Pour exécuter les tests de mutation,'
[//]: # 'il faut au préalable effectuer la migration de la base de données de test, puis :'
[//]: #
[//]: # '```shellsession'
[//]: # 'user@host api$ docker compose run --rm api ./tools/infection -c./ci/infection.json'
[//]: # '```'
[//]: # 'Le rapport HTML sera généré dans `./ci/infection/`.'

## Analyse statique

Pour faire une analyse statique :

```shellsession
user@host api$ ./vendor/bin/phpstan
```

Pour mettre à jour le fichier `phpstan-baseline.neon`, il faut utiliser la commande :

```shell
vendor/bin/phpstan analyse --level 9 --configuration phpstan.neon src/ --generate-baseline
```

## Formatage du code

```shell
$ yarn lint
```

## Documentation PHP

> À remettre en route

Pour générer la documentation PHP :

```shellsession
user@host api$ docker compose run --rm api ./tools/phpDocumentor --config ./ci/phpdoc.xml
```

Pour consulter la documentation HTML, il faut ouvrir le fichier `./ci/phpdoc/index.html`.

## Standard

Les fichiers PHP de ce projet suivent la norme [PSR-12](https://www.php-fig.org/psr/psr-12/).
Il est possible d'indenter le code avec :

```shellsession
user@host api$ docker compose run --rm api ./tools/phpcbf --standard=PSR12 --extensions=php --ignore=./src/Kernel.php,./tests/bootstrap.php -p ./src/ ./tests/
```

## Services du serveur

Les services démarrés par le serveur sont pilotés par `supervisord`.

Liste des services :

- `php-fpm` : serveur PHP
- `nginx` : serveur web
- `messenger-consume` : consommateur de messages
