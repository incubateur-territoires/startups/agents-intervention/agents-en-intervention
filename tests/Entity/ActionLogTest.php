<?php

namespace App\Tests\Entity;

use App\Entity\User;
use App\Entity\ActionLog;
use PHPUnit\Framework\TestCase;

class ActionLogTest extends TestCase
{
  public function testNewActionLog(): void
  {
    $type = 'update';
    $entityClass = User::class;
    $entityId = 1;
    $changes = json_encode(['field' => 'value']);
    $userIdentifier = 123;

    $actionLog = ActionLog::new($type, $entityClass, $entityId, $changes, $userIdentifier);

    $this->assertInstanceOf(ActionLog::class, $actionLog);
    $this->assertEquals($type, $actionLog->getType());
    $this->assertEquals($entityClass, $actionLog->getEntityClass());
    $this->assertEquals($entityId, $actionLog->getEntityId());
    $this->assertEquals($changes, $actionLog->getChanges());
    $this->assertEquals($userIdentifier, $actionLog->getUserIdentifier());
    $this->assertInstanceOf(\DateTimeInterface::class, $actionLog->getTimestamp());
  }

  public function testGetChangesObject(): void
  {
    $changes = json_encode(['field' => 'value']);
    $actionLog = ActionLog::new('update', User::class, 1, $changes, 123);

    $changesObject = $actionLog->getChangesObject();
    $this->assertIsObject($changesObject);
    $this->assertEquals('value', $changesObject->field);
  }

  public function testIsConsistent(): void
  {
    $validChanges = json_encode(['valid' => true]);
    $invalidChanges = json_encode(['valid' => false]);

    $validActionLog = ActionLog::new('update', User::class, 1, $validChanges, 123);
    $invalidActionLog = ActionLog::new('update', User::class, 1, $invalidChanges, 123);
    $emptyActionLog = ActionLog::new('update', User::class, 1, null, 123);

    $this->assertTrue($validActionLog->isConsistent());
    $this->assertFalse($invalidActionLog->isConsistent());
    $this->assertFalse($emptyActionLog->isConsistent());
  }
}
