<?php

declare(strict_types=1);

namespace App\Tests\Entity;

use App\Entity\Category;
use App\Entity\Type;
use PHPUnit\Framework\Attributes as PA;
use PHPUnit\Framework\TestCase;

/**
 * Test l'entité Type.
 */
#[
  PA\CoversClass(Type::class),
  PA\UsesClass(Category::class),
  PA\Group('entities'),
  PA\Group('entities_type'),
  PA\Group('type')
]
final class TypeTest extends TestCase
{
  // Méthodes :
  /**
   * Renvoie un substitut de l'entité Category.
   * @return Type un substitut de l'entité Category.
   */
  private function getMockForCategory(): Category
  {
    return $this->getMockBuilder(Category::class)
      ->disableOriginalConstructor()
      ->getMock();
  }


  /**
   * Test que l'identifiant
   * soit initialisé à null.
   */
  public function testCanInitialiseIdentifierToNull(): void
  {
    $type = new Type();
    $type->setName('type-name');
    $type->setCategory($this->getMockForCategory());
    $type->setPicture('type-picture');

    self::assertNull($type->getId());
  }


  /**
   * Test que le nom soit accessible.
   */
  public function testCanGetAndSetName(): void
  {
    $type = new Type();
    $type->setName('type-name');
    $type->setCategory($this->getMockForCategory());
    $type->setPicture('type-picture');

    self::assertSame('type-name', $type->getName());

    $type->setName('new-name');

    self::assertSame('new-name', $type->getName());
  }


  /**
   * Test que la categorie soit accessible.
   */
  public function testCanGetAndSetCategory(): void
  {
    $category = $this->getMockForCategory();
    $type = new Type();
    $type->setName('type-name');
    $type->setCategory($category);
    $type->setPicture('type-picture');
    $type->setDescription(null);

    self::assertSame($category, $type->getCategory());

    $otherCategory = $this->getMockForCategory();
    $type->setCategory($otherCategory);

    self::assertSame($otherCategory, $type->getCategory());
  }


  /**
   * Test que la photo soit accessible.
   */
  public function testCanGetAndSetPicture(): void
  {
    $type = new Type();
    $type->setName('type-name');
    $type->setCategory($this->getMockForCategory());
    $type->setPicture('type-picture');
    $type->setDescription('type-description');

    self::assertSame('type-picture', $type->getPicture());

    $type->setPicture('new-picture');

    self::assertSame('new-picture', $type->getPicture());
  }


  /**
   * Test que la description soit accessible.
   */
  public function testCanGetAndSetDescription(): void
  {
    $type = new Type();
    $type->setName('type-name');
    $type->setCategory($this->getMockForCategory());
    $type->setPicture('type-picture');

    self::assertNull($type->getDescription());

    $type->setDescription('new-description');

    self::assertSame('new-description', $type->getDescription());
  }

  /**
   * Test que la description puisse être nulle.
   */
  public function testCanGetAndSetANullDescription(): void
  {
    $type = new Type();
    $type->setName('type-name');
    $type->setCategory($this->getMockForCategory());
    $type->setPicture('type-picture');
    $type->setDescription('type-description');

    self::assertSame('type-description', $type->getDescription());

    $type->setDescription(null);

    self::assertNull($type->getDescription());
  }
}
