<?php

declare(strict_types=1);

namespace App\Tests\Entity;

use App\Entity\Picture;
use App\Entity\PictureTag;
use PHPUnit\Framework\Attributes as PA;
use PHPUnit\Framework\TestCase;

/**
 * Test l'entité Picture.
 */
#[
    PA\CoversClass(Picture::class),
    PA\UsesClass(PictureTag::class),
    PA\Group('entities'),
    PA\Group('entities_picture'),
    PA\Group('picture')
]
final class PictureTest extends TestCase
{
  // Traits :
    use InterventionMock;

  // Méthodes :

    /**
     * Test que l'identifiant
     * soit initialisé à null.
     */
    public function testCanInitialiseIdentifierToNull(): void
    {
        $picture = new Picture('picture-file-name');

        self::assertNull($picture->getId());
    }

    /**
     * Test que l'étiquette
     * soit initialisée à null.
     */
    public function testCanInitialiseTagToNull(): void
    {
        $picture = new Picture('picture-file-name', null);

        self::assertNull($picture->getTag());
    }

    /**
     * Test que l'URL
     * soit initialisé à null.
     */
    public function testCanInitialiseUrlToNull(): void
    {
        $picture = new Picture('picture-file-name', url: null);

        self::assertNull($picture->getURL());
    }

    /**
     * Test que l'intervention
     * soit initialisée à null.
     */
    public function testCanInitialiseInterventionToNull(): void
    {
        $picture = new Picture('picture-file-name', intervention: null);

        self::assertNull($picture->getIntervention());
    }


    /**
     * Test que le nom du fichier soit accessible.
     */
    public function testCanGetAndSetFileName(): void
    {
        $picture = new Picture('picture-file-name');

        self::assertSame('picture-file-name', $picture->getFileName());

        $picture->setFileName('new-file');

        self::assertSame('new-file', $picture->getFileName());
    }


    /**
     * Test que l'URL soit accessible.
     */
    public function testCanGetAndSetUrl(): void
    {
        $picture = new Picture('picture-file-name');

        self::assertNull($picture->getURL());

        $picture->setURL('new-URL');

        self::assertSame('new-URL', $picture->getURL());
    }

    /**
     * Test que l'URL puisse être nulle.
     */
    #[PA\TestDox('Can get and set a null URL')]
    public function testCanGetAndSetANullURL(): void
    {
        $picture = new Picture('picture-file-name', url: 'picture-URL');

        self::assertSame('picture-URL', $picture->getURL());

        $picture->setURL(null);

        self::assertNull($picture->getURL());
    }


    /**
     * Test que la date de création soit accessible.
     */
    public function testCanGetAndSetCreatedAt(): void
    {
        $picture = new Picture('picture-file-name');

        $picture->setCreatedAt(new \DateTimeImmutable('2023-02-01 00:00:00'));

        self::assertSame(
            '2023-02-01 00:00:00',
            $picture->getCreatedAt()->format('Y-m-d H:i:s')
        );
    }


    /**
     * Test que l'étiquette soit accessible.
     */
    public function testCanGetAndSetTag(): void
    {
        $picture = new Picture('picture-file-name', tag: PictureTag::Before);

        self::assertSame(PictureTag::Before, $picture->getTag());

        $picture->setTag(PictureTag::After);

        self::assertSame(PictureTag::After, $picture->getTag());
    }


    /**
     * Test que l'intervention soit accessible.
     */
    public function testCanGetAndSetIntervention(): void
    {
        $intervention = $this->getMockForIntervention();
        $picture = new Picture('picture-file-name', intervention: $intervention);

        self::assertSame($intervention, $picture->getIntervention());

        $otherIntervention = $this->getMockForIntervention();
        $picture->setIntervention($otherIntervention);

        self::assertSame($otherIntervention, $picture->getIntervention());
    }

    /**
     * Test que l'intervention puisse être nulle.
     */
    public function testCanGetAndSetANullIntervention(): void
    {
        $intervention = $this->getMockForIntervention();
        $picture = new Picture('picture-file-name', intervention: $intervention);

        self::assertSame($intervention, $picture->getIntervention());

        $picture->setIntervention(null);

        self::assertNull($picture->getIntervention());
    }

  /**
   * Test que les URLs soient initialisées à un tableau vide.
   */
  public function testCanInitialiseUrlsToEmptyArray(): void
  {
    $picture = new Picture('picture-file-name');

    self::assertSame([], $picture->getUrls());
  }

  /**
   * Test que les URLs puissent être définies et récupérées.
   */
  public function testCanGetAndSetUrls(): void
  {
    $urls = ['url1', 'url2'];
    $picture = new Picture('picture-file-name');
    $picture->setUrls($urls);

    self::assertSame($urls, $picture->getUrls());

    $newUrls = ['url3', 'url4'];
    $picture->setUrls($newUrls);

    self::assertSame($newUrls, $picture->getUrls());
  }

  /**
   * Test que les URLs puissent être nulles.
   */
  public function testCanGetAndSetNullUrls(): void
  {
    $picture = new Picture('picture-file-name');
    $picture->setUrls([]);

    self::assertCount(0, $picture->getUrls());

    $picture->setUrls(['url1', 'url2']);

    self::assertSame(['url1', 'url2'], $picture->getUrls());

    $picture->setUrls([]);

    self::assertCount(0, $picture->getUrls());
  }

  /**
   * Test que la méthode getSmall renvoie la bonne URL.
   */
  public function testCanGetSmall(): void
  {
    $urls = ['small' => 'small-url'];
    $picture = new Picture('picture-file-name');
    $picture->setUrls($urls);

    self::assertSame('small-url', $picture->getSmall());
  }

  /**
   * Test que la méthode getMedium renvoie la bonne URL.
   */
  public function testCanGetMedium(): void
  {
    $urls = ['medium' => 'medium-url'];
    $picture = new Picture('picture-file-name');
    $picture->setUrls($urls);

    self::assertSame('medium-url', $picture->getMedium());
  }

  /**
   * Test que la méthode getOriginal renvoie la bonne URL.
   */
  public function testCanGetOriginal(): void
  {
    $urls = ['original' => 'original-url'];
    $picture = new Picture('picture-file-name');
    $picture->setUrls($urls);

    self::assertSame('original-url', $picture->getOriginal());
  }

  /**
   * Test que la méthode getSmall renvoie l'URL par défaut si la taille n'est pas renseignée.
   */
  public function testGetSmallReturnsDefaultUrlIfNotSet(): void
  {
    $picture = new Picture('picture-file-name', url: 'default-url');

    self::assertSame('default-url', $picture->getSmall());
  }

  /**
   * Test que la méthode getMedium renvoie l'URL par défaut si la taille n'est pas renseignée.
   */
  public function testGetMediumReturnsDefaultUrlIfNotSet(): void
  {
    $picture = new Picture('picture-file-name', url: 'default-url');

    self::assertSame('default-url', $picture->getMedium());
  }

  /**
   * Test que la méthode getOriginal renvoie l'URL par défaut si la taille n'est pas renseignée.
   */
  public function testGetOriginalReturnsDefaultUrlIfNotSet(): void
  {
    $picture = new Picture('picture-file-name', url: 'default-url');

    self::assertSame('default-url', $picture->getOriginal());
  }

  /**
   * Test que la méthode getSmall renvoie une chaîne vide si aucune URL n'est définie.
   */
  public function testGetSmallReturnsEmptyStringIfNoUrlSet(): void
  {
    $picture = new Picture('picture-file-name');

    self::assertSame('', $picture->getSmall());
  }

  /**
   * Test que la méthode getMedium renvoie une chaîne vide si aucune URL n'est définie.
   */
  public function testGetMediumReturnsEmptyStringIfNoUrlSet(): void
  {
    $picture = new Picture('picture-file-name');

    self::assertSame('', $picture->getMedium());
  }

  /**
   * Test que la méthode getOriginal renvoie une chaîne vide si aucune URL n'est définie.
   */
  public function testGetOriginalReturnsEmptyStringIfNoUrlSet(): void
  {
    $picture = new Picture('picture-file-name');

    self::assertSame('', $picture->getOriginal());
  }
}
