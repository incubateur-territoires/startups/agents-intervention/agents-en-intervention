<?php

declare(strict_types=1);

namespace App\Tests\Entity;

use App\Entity\Employer;
use App\Entity\Picture;
use App\Entity\Role;
use App\Entity\User;
use PHPUnit\Framework\Attributes as PA;
use PHPUnit\Framework\TestCase;

/**
 * Test l'entité User.
 */
#[
    PA\CoversClass(User::class),
    PA\UsesClass(Employer::class),
    PA\Group('entities'),
    PA\Group('entities_user'),
    PA\Group('user')
]
final class UserTest extends TestCase
{
    // Méthodes :
    /**
     * Renvoie un substitut de l'entité Employer.
     * @return Employer un substitut de l'entité Employer.
     */
    private function getMockForEmployer(): Employer
    {
        return $this->getMockBuilder(Employer::class)
            ->disableOriginalConstructor()
            ->getMock();
    }

    /**
     * Renvoie un substitut de l'entité Picture.
     * @return Picture un substitut de l'entité Picture.
     */
    private function getMockForPicture(): Picture
    {
        return $this->getMockBuilder(Picture::class)
            ->disableOriginalConstructor()
            ->getMock();
    }


    /**
     * Test que l'identifiant
     * soit initialisé à null.
     */
    public function testCanInitialiseIdentifierToNull(): void
    {
        $user = User::new(
            'user-login',
            'user-password',
            'user-firstname',
            'user-lastname',
            $this->getMockForEmployer()
        );

        self::assertNull($user->getId());
    }

    /**
     * Test que les rôles
     * soit initialisés avec un tableau vide.
     */
    public function testCanInitialiseRolesToAnEmptyArray(): void
    {
        $user = User::new(
            'user-login',
            'user-password',
            'user-firstname',
            'user-lastname',
            $this->getMockForEmployer()
        );

        self::assertSame([], $user->getRoles());
    }

    /**
     * Test que l'e-mail
     * soit initialisé à null.
     */
    public function testCanInitialiseEmailToNull(): void
    {
        $user = User::new(
            'user-login',
            'user-password',
            'user-firstname',
            'user-lastname',
            $this->getMockForEmployer()
        );

        self::assertNull($user->getEmail());
    }

    /**
     * Test que le numéro de téléphone
     * soit initialisé à null.
     */
    public function testCanInitialisePhoneNumberToNull(): void
    {
        $user = User::new(
            'user-login',
            'user-password',
            'user-firstname',
            'user-lastname',
            $this->getMockForEmployer()
        );

        self::assertNull($user->getPhoneNumber());
    }

    /**
     * Test que la photo
     * soit initialisée à null.
     */
    public function testCanInitialisePictureToNull(): void
    {
        $user = User::new(
            'user-login',
            'user-password',
            'user-firstname',
            'user-lastname',
            $this->getMockForEmployer()
        );

        self::assertNull($user->getPicture());
    }

    /**
     * Test que le compte
     * soit initialisé à actif.
     */
    public function testCanInitialiseActiveToNull(): void
    {
        $user = User::new(
            'user-login',
            'user-password',
            'user-firstname',
            'user-lastname',
            $this->getMockForEmployer()
        );

        self::assertTrue($user->isActive());
    }

    /**
     * Test que la date de connexion
     * soit initialisée à null.
     */
    public function testCanInitialiseConnectedAtToNull(): void
    {
        $user = User::new(
            'user-login',
            'user-password',
            'user-firstname',
            'user-lastname',
            $this->getMockForEmployer()
        );

        self::assertNull($user->getConnectedAt());
    }


    /**
     * Test que l'identifiant de connexion soit accessible.
     */
    public function testCanGetAndSetLogin(): void
    {
        $user = User::new(
            'user-login',
            'user-password',
            'user-firstname',
            'user-lastname',
            $this->getMockForEmployer()
        );

        self::assertSame('user-login', $user->getLogin());

        $user->setLogin('new-login');

        self::assertSame('new-login', $user->getLogin());
    }


    /**
     * Test que le mot de passe soit accessible.
     */
    public function testCanGetAndPassword(): void
    {
        $user = User::new(
            'user-login',
            'user-password',
            'user-firstname',
            'user-lastname',
            $this->getMockForEmployer()
        );

        self::assertSame('user-password', $user->getPassword());

        $user->setPassword('new-password');

        self::assertSame('new-password', $user->getPassword());
    }


    /**
     * Test que le prénom soit accessible.
     */
    public function testCanGetAndSetFirstname(): void
    {
        $user = User::new(
            'user-login',
            'user-password',
            'user-firstname',
            'user-lastname',
            $this->getMockForEmployer()
        );

        self::assertSame('user-firstname', $user->getFirstname());

        $user->setFirstname('new-firstname');

        self::assertSame('new-firstname', $user->getFirstname());
    }


    /**
     * Test que le nom soit accessible.
     */
    public function testCanGetAndSetLastname(): void
    {
        $user = User::new(
            'user-login',
            'user-password',
            'user-firstname',
            'user-lastname',
            $this->getMockForEmployer()
        );

        self::assertSame('user-lastname', $user->getLastname());

        $user->setLastname('new-lastname');

        self::assertSame('new-lastname', $user->getLastname());
    }


    /**
     * Test que l'employeur soit accessible.
     */
    public function testCanGetAndSetEmployeur(): void
    {
        $employer = $this->getMockForEmployer();
        $user = User::new(
            'user-login',
            'user-password',
            'user-firstname',
            'user-lastname',
            $employer,
        );

        self::assertSame($employer, $user->getEmployer());

        $otherEmployer = $this->getMockForEmployer();
        $user->setEmployer($otherEmployer);

        self::assertSame($otherEmployer, $user->getEmployer());
    }


    /**
     * Test que les rôles soient accessible.
     */
    public function testCanGetAndSetRoles(): void
    {
        $user = User::new(
            'user-login',
            'user-password',
            'user-firstname',
            'user-lastname',
            $this->getMockForEmployer()
        );

        self::assertEmpty($user->getRoles());

        $user->setRoles(['ROLE_TEST']);

        self::assertSame(['ROLE_TEST'], $user->getRoles());
    }

    /**
     * Test que les rôles soient accessible.
     */
    public function testHasARole(): void
    {
        $user = User::new(
            'user-login',
            'user-password',
            'user-firstname',
            'user-lastname',
            $this->getMockForEmployer(),
            [Role::Agent->value]
        );

        self::assertTrue($user->hasRole(Role::Agent));
    }


    /**
     * Test que l'e-mail soit accessible.
     */
    public function testCanGetAndSetEmail(): void
    {
        $user = User::new(
            'user-login',
            'user-password',
            'user-firstname',
            'user-lastname',
            $this->getMockForEmployer()
        );

        self::assertNull($user->getEmail());

        $user->setEmail('new-email');

        self::assertSame('new-email', $user->getEmail());
    }

    /**
     * Test que l'e-mail puisse être nul.
     */
    public function testCanGetAndSetANullEmail(): void
    {
        $user = User::new(
            'user-login',
            'user-password',
            'user-firstname',
            'user-lastname',
            $this->getMockForEmployer(),
            email: 'user-email'
        );

        self::assertSame('user-email', $user->getEmail());

        $user->setEmail(null);

        self::assertNull($user->getEmail());
    }


    /**
     * Test que le numéro de téléphone soit accessible.
     */
    public function testCanGetAndSetPhoneNumber(): void
    {
        $user = User::new(
            'user-login',
            'user-password',
            'user-firstname',
            'user-lastname',
            $this->getMockForEmployer()
        );

        self::assertNull($user->getPhoneNumber());

        $user->setPhoneNumber('new-phoneNumber');

        self::assertSame('new-phoneNumber', $user->getPhoneNumber());
    }

    /**
     * Test que le numéro de téléphone puisse être nul.
     */
    public function testCanGetAndSetANullPhoneNumber(): void
    {
        $user = User::new(
            'user-login',
            'user-password',
            'user-firstname',
            'user-lastname',
            $this->getMockForEmployer(),
            phoneNumber: 'user-phoneNumber'
        );

        self::assertSame('user-phoneNumber', $user->getPhoneNumber());

        $user->setPhoneNumber(null);

        self::assertNull($user->getPhoneNumber());
    }


    /**
     * Test que la date de création soit accessible.
     */
    public function testCanGetAndSetCreatedAt(): void
    {
        $user = User::new(
            'user-login',
            'user-password',
            'user-firstname',
            'user-lastname',
            $this->getMockForEmployer()
        );

        $user->setCreatedAt(new \DateTimeImmutable('2023-01-01 00:00:00'));

        self::assertSame('2023-01-01 00:00:00', $user->getCreatedAt()->format('Y-m-d H:i:s'));
    }


    /**
     * Test que la photo soit accessible.
     */
    public function testCanGetAndSetPicture(): void
    {
        $user = User::new(
            'user-login',
            'user-password',
            'user-firstname',
            'user-lastname',
            $this->getMockForEmployer()
        );

        self::assertNull($user->getPicture());

        $picture = $this->getMockForPicture();
        $user->setPicture($picture);

        self::assertSame($picture, $user->getPicture());
    }

    /**
     * Test que la photo puisse être nul.
     */
    public function testCanGetAndSetANullPicture(): void
    {
        $picture = $this->getMockForPicture();
        $user = User::new(
            'user-login',
            'user-password',
            'user-firstname',
            'user-lastname',
            $this->getMockForEmployer(),
            picture: $picture
        );

        self::assertSame($picture, $user->getPicture());

        $user->setPicture(null);

        self::assertNull($user->getPicture());
    }


    /**
     * Test que le compte puisse être activé.
     */
    public function testCanBeActived(): void
    {
        $user = User::new(
            'user-login',
            'user-password',
            'user-firstname',
            'user-lastname',
            $this->getMockForEmployer(),
            active: false
        );

        self::assertFalse($user->isActive());

        $user->setActive(true);

        self::assertTrue($user->isActive());
    }

    /**
     * Test que le compte puisse être désactivé.
     */
    public function testCanBeDeactived(): void
    {
        $user = User::new(
            'user-login',
            'user-password',
            'user-firstname',
            'user-lastname',
            $this->getMockForEmployer()
        );

        self::assertTrue($user->isActive());

        $user->setActive(false);

        self::assertFalse($user->isActive());
    }


    /**
     * Test que la date de la dernière connexion soit accessible.
     */
    public function testCanGetAndSetConnectedAt(): void
    {
        $user = User::new(
            'user-login',
            'user-password',
            'user-firstname',
            'user-lastname',
            $this->getMockForEmployer()
        );

        self::assertNull($user->getConnectedAt());

        $user->setConnectedAt(new \DateTimeImmutable('2023-02-01 00:00:00'));

        self::assertSame('2023-02-01 00:00:00', $user->getConnectedAt()->format('Y-m-d H:i:s'));
    }

    /**
     * Test que la date de la dernière connexion puisse être nulle.
     */
    public function testCanGetAndSetANullConnectedAt(): void
    {
        $user = User::new(
            'user-login',
            'user-password',
            'user-firstname',
            'user-lastname',
            $this->getMockForEmployer(),
            connectedAt: new \DateTimeImmutable('2023-01-01 00:00:00')
        );

        self::assertSame('2023-01-01 00:00:00', $user->getConnectedAt()->format('Y-m-d H:i:s'));

        $user->setConnectedAt(null);

        self::assertNull($user->getConnectedAt());
    }


    /**
     * Test que l'identifiant utilisateur soit accessible.
     */
    public function testCanGetUserIdentifier(): void
    {
        $user = User::new(
            'user-login',
            'user-password',
            'user-firstname',
            'user-lastname',
            $this->getMockForEmployer()
        );

        self::assertSame('user-login', $user->getUserIdentifier());
    }


    /**
     * Test que l'on puisse effacer le mot de passe en clair.
     */
    public function testCanEraseCredentials(): void
    {
        $user = User::new(
            'user-login',
            'user-password',
            'user-firstname',
            'user-lastname',
            $this->getMockForEmployer()
        );

        self::assertNull($user->eraseCredentials());
    }

  /**
   * Test que l'on puisse récupérer le nom complet.
   */
  public function testCanGetFullName(): void
  {
    $user = User::new(
      'user-login',
      'user-password',
      'user-firstname',
      'user-lastname',
      $this->getMockForEmployer()
    );

    self::assertEquals("user-firstname user-lastname", $user->getFullName());
  }


  /**
   * Test de la méthode magique __toString.
   */
  public function testToString(): void
  {
    $user = User::new(
      'user-login',
      'user-password',
      'user-firstname',
      'user-lastname',
      $this->getMockForEmployer()
    );

    self::assertEquals("user-login", $user->__toString());
    self::assertEquals("user-login", (string) $user);
  }
}
