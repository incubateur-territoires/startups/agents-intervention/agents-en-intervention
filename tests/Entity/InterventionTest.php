<?php

declare(strict_types=1);

namespace App\Tests\Entity;

use App\Entity\Comment;
use App\Entity\Employer;
use App\Entity\Intervention;
use App\Entity\Location;
use App\Entity\Picture;
use App\Entity\Priority;
use App\Entity\Status;
use App\Entity\Type;
use App\Entity\User;
use App\Event\InterventionType;
use App\Model\Frequency;
use PHPUnit\Framework\Attributes as PA;
use PHPUnit\Framework\TestCase;

/**
 * Test l'entité Intervention.
 */
#[
    PA\CoversClass(Intervention::class),
    PA\Group('entities'),
    PA\Group('entities_intervention'),
    PA\Group('intervention')
]
final class InterventionTest extends TestCase
{
    /**
     * Renvoie un substitut de l'entité Type.
     * @return Type un substitut de l'entité Type.
     */
    private function getMockForType(): Type
    {
        return $this->getMockBuilder(Type::class)
            ->disableOriginalConstructor()
            ->getMock()
        ;
    }

    /**
     * Renvoie un substitut de l'entité User.
     * @return User un substitut de l'entité User.
     */
    private function getMockForAuthor(): User
    {
        return $this->getMockBuilder(User::class)
            ->disableOriginalConstructor()
            ->getMock()
        ;
    }

    /**
     * Renvoie un substitut de l'entité Employer.
     * @return Employer un substitut de l'entité Employer.
     */
    private function getMockForEmployer(): Employer
    {
        return $this->getMockBuilder(Employer::class)
            ->disableOriginalConstructor()
            ->getMock()
        ;
    }

    /**
     * Renvoie un substitut de l'entité Location.
     * @return Location un substitut de l'entité Location.
     */
    private function getMockForLocation(): Location
    {
        return $this->getMockBuilder(Location::class)
            ->disableOriginalConstructor()
            ->getMock()
        ;
    }

    /**
     * Renvoie un substitut de l'entité User.
     * @return User un substitut de l'entité User.
     */
    private function getMockForParticipant(): User
    {
        return $this->getMockForAuthor();
    }

    /**
     * Renvoie un substitut de l'entité Comment.
     * @return Comment un substitut de l'entité Comment.
     */
    private function getMockForComment(): Comment
    {
        return $this->getMockBuilder(Comment::class)
            ->disableOriginalConstructor()
            ->getMock()
        ;
    }

  /**
   * Renvoie un substitut de l'entité Picture.
   * @return Picture un substitut de l'entité Picture.
   */
    private function getMockForPicture(): Picture
    {
        return $this->getMockBuilder(Picture::class)
            ->disableOriginalConstructor()
            ->getMock()
        ;
    }


    /**
     * Test que l'identifiant
     * soit initialisé à null.
     */
    public function testCanInitialiseIdentifierToNull(): void
    {
        $intervention = new Intervention(
            'intervention-description',
            Priority::Normal,
            $this->getMockForType(),
            $this->getMockForLocation()
        );

        self::assertNull($intervention->getId());
    }


    /**
     * Test que la date de création soit accessible.
     */
    public function testCanGetAndSetCreatedAt(): void
    {
        $intervention = new Intervention(
            'intervention-description',
            Priority::Normal,
            $this->getMockForType(),
            $this->getMockForLocation()
        );

        $intervention->setCreatedAt(new \DateTimeImmutable('2023-01-01 00:00:00'));

        self::assertSame(
            '2023-01-01 00:00:00',
            $intervention->getCreatedAt()->format('Y-m-d H:i:s')
        );
    }


    /**
     * Test que la description soit accessible.
     */
    public function testCanGetAndSetDescription(): void
    {
        $intervention = new Intervention(
            null,
            Priority::Normal,
            $this->getMockForType(),
            $this->getMockForLocation()
        );

        self::assertNull($intervention->getDescription());

        $intervention->setDescription('new-description');

        self::assertSame('new-description', $intervention->getDescription());
    }

    /**
     * Test que la description puisse être nulle.
     */
    public function testCanGetAndSetANullDescription(): void
    {
        $intervention = new Intervention(
            'intervention-description',
            Priority::Normal,
            $this->getMockForType(),
            $this->getMockForLocation()
        );

        self::assertSame('intervention-description', $intervention->getDescription());

        $intervention->setDescription(null);

        self::assertNull($intervention->getDescription());
    }


    /**
     * Test que le statut soit accessible.
     */
    public function testCanGetAndSetStatus(): void
    {
        $intervention = new Intervention(
            'intervention-description',
            Priority::Normal,
            $this->getMockForType(),
            $this->getMockForLocation()
        );

        $intervention->setStatus(Status::ToDo);

        self::assertSame(Status::ToDo, $intervention->getStatus());
    }


    /**
     * Test que la priorité soit accessible.
     */
    public function testCanGetAndSetPriority(): void
    {
        $intervention = new Intervention(
            'intervention-description',
            Priority::Normal,
            $this->getMockForType(),
            $this->getMockForLocation()
        );

        self::assertSame(Priority::Normal, $intervention->getPriority());

        $intervention->setPriority(Priority::Urgent);

        self::assertSame(Priority::Urgent, $intervention->getPriority());
    }


    /**
     * Test que le type soit accessible.
     */
    public function testCanGetAndSetType(): void
    {
        $type = $this->getMockForType();
        $intervention = new Intervention(
            'intervention-description',
            Priority::Normal,
            $type,
            $this->getMockForLocation()
        );

        self::assertSame($type, $intervention->getType());

        $otherType = $this->getMockForType();
        $intervention->setType($otherType);

        self::assertSame($otherType, $intervention->getType());
    }


    /**
     * Test que l'auteur soit accessible.
     */
    public function testCanGetAndSetAuthor(): void
    {
        $intervention = new Intervention(
            'intervention-description',
            Priority::Normal,
            $this->getMockForType(),
            $this->getMockForLocation()
        );

        $author = $this->getMockForAuthor();
        $intervention->setAuthor($author);

        self::assertSame($author, $intervention->getAuthor());
    }


    /**
     * Test que l'auteur soit accessible.
     */
    public function testCanGetAndSetEmployer(): void
    {
        $intervention = new Intervention(
            'intervention-description',
            Priority::Normal,
            $this->getMockForType(),
            $this->getMockForLocation()
        );

        $employer = $this->getMockForEmployer();
        $intervention->setEmployer($employer);

        self::assertSame($employer, $intervention->getEmployer());
    }


    /**
     * Test que la localisation soit accessible.
     */
    public function testCanGetAndSetLocation(): void
    {
        $location = $this->getMockForLocation();
        $intervention = new Intervention(
            'intervention-description',
            Priority::Normal,
            $this->getMockForType(),
            $location
        );

        self::assertSame($location, $intervention->getLocation());

        $otherLocation = $this->getMockForLocation();
        $intervention->setLocation($otherLocation);

        self::assertSame($otherLocation, $intervention->getLocation());
    }


    /**
     * Test que l'on puisse ajouter un participant.
     * @return Intervention l'intervention.
     */
    public function testCanAddAParticipant(): Intervention
    {
        $intervention = new Intervention(
            'intervention-description',
            Priority::Normal,
            $this->getMockForType(),
            $this->getMockForLocation()
        );

        self::assertEmpty($intervention->getParticipants());

        $participant = $this->getMockForParticipant();
        $intervention->addParticipant($participant);

        self::assertCount(1, $intervention->getParticipants());

        return $intervention;
    }

    /**
     * Test que l'on puisse retirer un participant.
     * @param Intervention $intervention l'intervention.
     */
    #[PA\Depends('testCanAddAParticipant')]
    public function testCanRemoveAParticipant(Intervention $intervention): void
    {
        $participants = $intervention->getParticipants();

        $intervention->removeParticipant($participants[0]);

        self::assertEmpty($intervention->getParticipants());
    }

    /**
     * Test que l'on ne puisse pas ajouter
     * le même participant deux fois.
     */
    public function testCanNotAddTheSameParticipantTwice(): void
    {
        $intervention = new Intervention(
            'intervention-description',
            Priority::Normal,
            $this->getMockForType(),
            $this->getMockForLocation()
        );

        self::assertEmpty($intervention->getParticipants());

        $participant = $this->getMockForParticipant();
        $intervention->addParticipant($participant);

        self::assertCount(1, $intervention->getParticipants());

        $intervention->addParticipant($participant);

        self::assertCount(1, $intervention->getParticipants());
    }


    /**
     * Test que l'on puisse ajouter un commentaire.
     * @return Intervention l'intervention.
     */
    public function testCanAddAComment(): Intervention
    {
        $intervention = new Intervention(
            'intervention-description',
            Priority::Normal,
            $this->getMockForType(),
            $this->getMockForLocation()
        );

        self::assertEmpty($intervention->getComments());

        $comment = $this->getMockForComment();
        $intervention->addComment($comment);

        self::assertCount(1, $intervention->getComments());

        return $intervention;
    }

    /**
     * Test que l'on puisse retirer un commentaire.
     * @param Intervention $intervention l'intervention.
     */
    #[PA\Depends('testCanAddAComment')]
    public function testCanRemoveAComment(Intervention $intervention): void
    {
        $comments = $intervention->getComments();

        $intervention->removeComment($comments[0]);

        self::assertEmpty($intervention->getComments());
    }

    /**
     * Test que l'on ne puisse pas ajouter
     * le même commentaire deux fois.
     */
    public function testCanNotAddTheSameCommentTwice(): void
    {
        $intervention = new Intervention(
            'intervention-description',
            Priority::Normal,
            $this->getMockForType(),
            $this->getMockForLocation()
        );

        self::assertEmpty($intervention->getComments());

        $comment = $this->getMockForComment();
        $intervention->addComment($comment);

        self::assertCount(1, $intervention->getComments());

        $intervention->addComment($comment);

        self::assertCount(1, $intervention->getComments());
    }


    /**
     * Test que l'on puisse ajouter une photo.
     * @return Intervention l'intervention.
     */
    public function testCanAddAPicture(): Intervention
    {
        $intervention = new Intervention(
            'intervention-description',
            Priority::Normal,
            $this->getMockForType(),
            $this->getMockForLocation()
        );

        self::assertEmpty($intervention->getPictures());

        $picture = $this->getMockForPicture();
        $intervention->addPicture($picture);

        self::assertCount(1, $intervention->getPictures());

        return $intervention;
    }

    /**
     * Test que l'on puisse retirer un commentaire.
     * @param Intervention $intervention l'intervention.
     */
    #[PA\Depends('testCanAddAPicture')]
    public function testCanRemoveAPicture(Intervention $intervention): void
    {
        $pictures = $intervention->getPictures();

        $intervention->removePicture($pictures[0]);

        self::assertEmpty($intervention->getPictures());
    }

    /**
     * Test que l'on ne puisse pas ajouter
     * le même commentaire deux fois.
     */
    public function testCanNotAddTheSamePictureTwice(): void
    {
        $intervention = new Intervention(
            'intervention-description',
            Priority::Normal,
            $this->getMockForType(),
            $this->getMockForLocation()
        );

        self::assertEmpty($intervention->getPictures());

        $picture = $this->getMockForPicture();
        $intervention->addPicture($picture);

        self::assertCount(1, $intervention->getPictures());

        $intervention->addPicture($picture);

        self::assertCount(1, $intervention->getPictures());
    }

    public function testInterventionEnsureIntegrityRecurringStatus() {

      $intervention = new Intervention(
        'intervention-description',
        Priority::Normal,
        $this->getMockForType(),
        $this->getMockForLocation()
      );

      $intervention->setStatus(Status::ToDo);
      $intervention->setFrequency(Frequency::DAILY);

      $this->expectException(\LogicException::class);
      $this->expectExceptionMessage('Recurring intervention must have recurring status');
      $intervention->ensureIntegrity();
    }

    public function testInterventionEnsureIntegrityRecurringStartAtMandatory() {

      $intervention = new Intervention(
        'intervention-description',
        Priority::Normal,
        $this->getMockForType(),
        $this->getMockForLocation()
      );

      $intervention->setStatus(Status::Recurring);
      $intervention->setFrequency(Frequency::MONTHLY);

      $this->expectException(\LogicException::class);
      $this->expectExceptionMessage('Recurring intervention must have startAt datetime');
      $intervention->ensureIntegrity();
    }

    public function testInterventionEnsureIntegrityRecurringEndedAtMandatory() {

      $intervention = new Intervention(
        'intervention-description',
        Priority::Normal,
        $this->getMockForType(),
        $this->getMockForLocation()
      );

      $intervention->setStatus(Status::Recurring);
      $intervention->setFrequency(Frequency::YEARLY);
      $intervention->setStartAt(new \DateTimeImmutable('+1 year'));

      $this->expectException(\LogicException::class);
      $this->expectExceptionMessage('Recurring intervention must have endedAt datetime');
      $intervention->ensureIntegrity();
    }

    public function testInterventionEnsureIntegrityRecurringEndedAtAfterStartAt() {
      $intervention = new Intervention(
        'intervention-description',
        Priority::Normal,
        $this->getMockForType(),
        $this->getMockForLocation()
      );

      $intervention->setStatus(Status::Recurring);
      $intervention->setFrequency(Frequency::MONTHLY);
      $intervention->setStartAt(new \DateTimeImmutable('+1 month'));
      $intervention->setEndedAt(new \DateTimeImmutable());

      $this->expectException(\LogicException::class);
      $this->expectExceptionMessage('endedAt must be after startAt');
      $intervention->ensureIntegrity();
    }

    public function testInterventionEnsureIntegrityOneTimeNotFinishedHaveNoEndedAt() {

      $intervention = new Intervention(
        'intervention-description',
        Priority::Normal,
        $this->getMockForType(),
        $this->getMockForLocation()
      );

      $intervention->setStatus(Status::ToDo);
      $intervention->setFrequency(Frequency::ONE_TIME);
      $intervention->setStartAt(new \DateTimeImmutable('-1 month'));
      $intervention->setEndedAt(new \DateTimeImmutable());

      $this->expectException(\LogicException::class);
      $this->expectExceptionMessage('one time intervention must not have endedAt datetime unless is finished');
      $intervention->ensureIntegrity();
    }

    public function testInterventionEnsureIntegrityOneTimeFinishedHaveEndedAt() {

      $intervention = new Intervention(
        'intervention-description',
        Priority::Normal,
        $this->getMockForType(),
        $this->getMockForLocation()
      );

      $intervention->setStatus(Status::Finished);
      $intervention->setFrequency(Frequency::ONE_TIME);
      $intervention->setStartAt(new \DateTimeImmutable('-1 month'));

      $this->expectException(\LogicException::class);
      $this->expectExceptionMessage('When intervention is finished, endedAt datetime must be provided');
      $intervention->ensureIntegrity();
    }

    public function testInterventionEnsureIntegrityOneTimeFinishedHaveEndedAtAfterStartAt(): never {
      $this->markTestSkipped('[métier] le contrôle est suspendu');

      $intervention = new Intervention(
        'intervention-description',
        Priority::Normal,
        $this->getMockForType(),
        $this->getMockForLocation()
      );

      $intervention->setStatus(Status::Finished);
      $intervention->setFrequency(Frequency::ONE_TIME);
      $intervention->setStartAt(new \DateTimeImmutable('+1 month'));
      $intervention->setEndedAt(new \DateTimeImmutable());

      $this->expectException(\LogicException::class);
      $this->expectExceptionMessage('endedAt datetime must be after startAt');
      $intervention->ensureIntegrity();
    }

  public function testGetInterventionTypeToValidate(): void
  {
    $intervention = $this->getMockBuilder(Intervention::class)
      ->disableOriginalConstructor()
      ->onlyMethods(['getStatus', 'isRecurring', 'getRecurrenceReference'])
      ->getMock();

    $intervention->method('getStatus')->willReturn(Status::ToValidate);
    $intervention->method('isRecurring')->willReturn(false);
    $intervention->method('getRecurrenceReference')->willReturn(null);

    $this->assertEquals(InterventionType::Request, $intervention->getInterventionType());
  }

  public function testGetInterventionTypeRecurring(): void
  {
    $intervention = $this->getMockBuilder(Intervention::class)
      ->disableOriginalConstructor()
      ->onlyMethods(['getStatus', 'isRecurring', 'getRecurrenceReference'])
      ->getMock();

    $intervention->method('getStatus')->willReturn(Status::Recurring);
    $intervention->method('isRecurring')->willReturn(true);
    $intervention->method('getRecurrenceReference')->willReturn(null);

    $this->assertEquals(InterventionType::Recurring, $intervention->getInterventionType());
  }

  public function testGetInterventionTypeSimple(): void
  {
    $intervention = $this->getMockBuilder(Intervention::class)
      ->disableOriginalConstructor()
      ->onlyMethods(['getStatus', 'isRecurring', 'getRecurrenceReference'])
      ->getMock();

    $intervention->method('getStatus')->willReturn(Status::ToDo);
    $intervention->method('isRecurring')->willReturn(false);
    $intervention->method('getRecurrenceReference')->willReturn(null);

    $this->assertEquals(InterventionType::Simple, $intervention->getInterventionType());
  }

  public function testGetInterventionTypeGenerated(): void
  {
    $intervention = $this->getMockBuilder(Intervention::class)
      ->disableOriginalConstructor()
      ->onlyMethods(['getStatus', 'isRecurring', 'getRecurrenceReference'])
      ->getMock();

    $parent_intervention = $this->getMockBuilder(Intervention::class)
      ->disableOriginalConstructor()
      ->onlyMethods(['getStatus', 'isRecurring', 'getRecurrenceReference'])
      ->getMock();

    $parent_intervention->method('getStatus')->willReturn(Status::Recurring);
    $parent_intervention->method('isRecurring')->willReturn(true);
    $parent_intervention->method('getRecurrenceReference')->willReturn(null);

    $intervention->method('getStatus')->willReturn(Status::ToDo);
    $intervention->method('isRecurring')->willReturn(false);
    $intervention->method('getRecurrenceReference')->willReturn($parent_intervention);

    $this->assertEquals(InterventionType::Generated, $intervention->getInterventionType());
  }
}
