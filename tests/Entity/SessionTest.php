<?php

namespace App\Tests\Entity;

use App\Entity\Session;
use PHPUnit\Framework\TestCase;
use Spatie\Snapshots\MatchesSnapshots;

class SessionTest extends TestCase
{
  use MatchesSnapshots;

  public function testGetId(): void
  {
    $session = new Session();
    $reflection = new \ReflectionClass($session);
    $property = $reflection->getProperty('id');
    $property->setAccessible(true);
    $property->setValue($session, 'test_id');

    $this->assertEquals('test_id', $session->getId());
  }

  public function testGetData(): void
  {
    $session = new Session();
    $reflection = new \ReflectionClass($session);
    $property = $reflection->getProperty('data');
    $property->setAccessible(true);
    $property->setValue($session, 'test_data');

    $this->assertEquals('test_data', $session->getData());
  }

  public function testGetLifetime(): void
  {
    $session = new Session();
    $reflection = new \ReflectionClass($session);
    $property = $reflection->getProperty('lifetime');
    $property->setAccessible(true);
    $property->setValue($session, time() + 3600);

    $this->assertInstanceOf(\DateTime::class, $session->getLifetime());
  }

  public function testGetTime(): void
  {
    $session = new Session();
    $reflection = new \ReflectionClass($session);
    $property = $reflection->getProperty('time');
    $property->setAccessible(true);
    $property->setValue($session, time());

    $this->assertInstanceOf(\DateTime::class, $session->getTime());
  }

  public function testGetOngoing(): void
  {
    $session = new Session();
    $reflection = new \ReflectionClass($session);
    $property = $reflection->getProperty('lifetime');
    $property->setAccessible(true);
    $property->setValue($session, time() + 3600);

    $this->assertTrue($session->getOngoing());
  }

  public function testGetUsername(): void
  {
    $this->markTestSkipped('buggy test');
    $session = new Session();

    $sessionData = <<<SESS_DATA
_sf2_attributes|a:2:{s:3:"jwt";s:530:"eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJpYXQiOjE3MzQ1Mjg3MDAsImV4cCI6MTczNDU2NDcwMCwicm9sZXMiOlsiUk9MRV9BRE1JTiJdLCJ1c2VybmFtZSI6InN5bHZhaW4ubGVnbGVhdS5wcmVzdGF0YWlyZUBhbmN0LmdvdXYuZnIifQ.a2iDFnZXS4nHA7GXm0-IoS5446e0zfjyAEnmqIqCXYyhO_nKWSOW0JoBv9TRdj5Gfj-y8E2pNDFfu4Mt3xg2RFlaSC9pj3Uz-e9hFYcaMuJJOpMGwKephSLVov9P07lvhWPqG39gHb0vxovYYaLrhjgZ7--elGEkUKdA2JIkXjZRAg-l6U9EeHLY0Vz-fqQBfaxmQbSW8d-U1YLXFZDqtNlF1XeJ2RWQD4aeeCFet3d8wJp-Htjwi1wQMBY0EA_DkeF9W21F7R9s5Xkfq0L0J4ayokaU1z58DY-YM3ETaXV5tQ61rIgpMjscZ7WMkqAJLNoySm5Wo-gzPtbA7qDEqg";s:14:"_security_main";s:1712:"O:75:"Symfony\\Component\\Security\\Http\\Authenticator\\Token\\PostAuthenticationToken":2:{i:0;s:4:"main";i:1;a:5:{i:0;O:15:"App\\Entity\\User":14:{s:19:"0x00App\\Entity\\User0x00id";i:184;s:22:"0x00App\\Entity\\User0x00login";s:40:"sylvain.legleau.prestataire@anct.gouv.fr";s:25:"0x00App\\Entity\\User0x00password";s:1:"-";s:26:"0x00App\\Entity\\User0x00firstname";s:7:"Sylvain";s:25:"0x00App\\Entity\\User0x00lastname";s:8:"LE GLEAU";s:22:"0x00App\\Entity\\User0x00email";s:40:"sylvain.legleau.prestataire@anct.gouv.fr";s:28:"0x00App\\Entity\\User0x00phoneNumber";N;s:26:"0x00App\\Entity\\User0x00createdAt";O:17:"DateTimeImmutable":3:{s:4:"date";s:26:"2024-12-02 12:18:50.000000";s:13:"timezone_type";i:3;s:8:"timezone";s:3:"UTC";}s:24:"0x00App\\Entity\\User0x00picture";N;s:25:"0x00App\\Entity\\User0x00employer";O:20:"0x00App\\Entity\\Employer":7:{s:23:"0x00App\\Entity\\Employer0x00id";i:54;s:26:"0x00App\\Entity\\Employer0x00siren";s:9:"130026032";s:25:"0x00App\\Entity\\Employer0x00name";s:4:"ANCT";s:30:"0x00App\\Entity\\Employer0x00longitude";d:3.223203;s:29:"0x00App\\Entity\\Employer0x00latitude";d:44.324594;s:26:"0x00App\\Entity\\Employer0x00users";O:33:"Doctrine\\ORM\\PersistentCollection":2:{s:13:"0x00*0x00collection";O:43:"Doctrine\\Common\\Collections\\ArrayCollection":1:{s:53:"0x00Doctrine\\Common\\Collections\\ArrayCollection0x00elements";a:0:{}}s:14:"0x00*0x00initialized";b:0;}s:29:"0x00App\\Entity\\Employer0x00timezone";s:12:"Europe/Paris";}s:23:"0x00App\\Entity\\User0x00active";b:1;s:28:"0x00App\\Entity\\User0x00connectedAt";O:17:"DateTimeImmutable":3:{s:4:"date";s:26:"2024-12-18 13:31:40.000000";s:13:"timezone_type";i:3;s:8:"timezone";s:3:"UTC";}s:22:"0x00App\\Entity\\User0x00roles";a:1:{i:0;s:10:"ROLE_ADMIN";}s:32:"0x00App\\Entity\\User0x00proConnectToken";s:43:"Jm5_ENmA45aS9OKYMea0XPy5sWdKkhYmdBmuBXUzRts";}i:1;b:1;i:2;N;i:3;a:0:{}i:4;a:1:{i:0;s:10:"ROLE_ADMIN";}}}";}_sf2_meta|a:3:{s:1:"u";i:1734528722;s:1:"c";i:1734528700;s:1:"l";i:0;}
SESS_DATA;
    $reflection = new \ReflectionClass($session);
    $property = $reflection->getProperty('data');
    $property->setAccessible(true);

    $stream = fopen('php://memory', 'r+');
    fwrite($stream, (string) $sessionData);
    rewind($stream);
    $property->setValue($session, $stream);

    $this->assertEquals('user', $session->getUsername());
  }

  public function testGetUsernameForAnonymous(): void
  {
    $session = new Session();
    $reflection = new \ReflectionClass($session);
    $property = $reflection->getProperty('data');
    $property->setAccessible(true);

    $sessionData = '_sf2_attributes|a:1:{s:26:"_security.main.target_path";s:35:"https://localhost:8000/56/dashboard";}_sf2_meta|a:3:{s:1:"u";i:1734685473;s:1:"c";i:1734685473;s:1:"l";i:0;}';
    $stream = fopen('php://memory', 'r+');
    fwrite($stream, $sessionData);
    rewind($stream);
    $property->setValue($session, $stream);

    $this->assertEquals('anonymous', $session->getUsername());
  }

  public function testGetUnserializedData(): void
  {
    $session = new Session();
    $reflection = new \ReflectionClass($session);
    $property = $reflection->getProperty('data');
    $property->setAccessible(true);

    $sessionData = '_sf2_attributes|a:1:{s:26:"_security.main.target_path";s:35:"https://localhost:8000/56/dashboard";}_sf2_meta|a:3:{s:1:"u";i:1734685473;s:1:"c";i:1734685473;s:1:"l";i:0;}';
    $stream = fopen('php://memory', 'r+');
    fwrite($stream, $sessionData);
    rewind($stream);
    $property->setValue($session, $stream);

    $this->assertMatchesJsonSnapshot($session->getUnserializedData());
  }
}
