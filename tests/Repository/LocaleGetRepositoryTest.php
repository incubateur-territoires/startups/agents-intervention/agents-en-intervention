<?php

declare(strict_types=1);

namespace App\Tests\Repository;

use App\Entity\Employer;
use App\Entity\Picture;
use App\Entity\PictureTag;
use App\Entity\Role;
use App\Entity\User;
use App\Logger\UserProcessor;
use App\Repository\UserRepository;
use PHPUnit\Framework\Attributes as PA;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Zenstruck\Foundry\Test\Factories;
use Zenstruck\Foundry\Test\ResetDatabase;

/**
 * Tests the locale GET repository.
 */
#[
  PA\CoversClass(UserRepository::class),
  PA\UsesClass(Employer::class),
  PA\UsesClass(Role::class),
  PA\UsesClass(Picture::class),
  PA\UsesClass(User::class),
  PA\UsesClass(UserProcessor::class),
  PA\Group('repositories'),
  PA\Group('repositories_user'),
  PA\Group('user')
]
final class LocaleGetRepositoryTest extends WebTestCase
{
  use ResetDatabase, Factories;

  private function addUser(): void
  {
    $employer = Employer::new('employer-siren', 'employer-name', 45, 54);
    $user = User::new(
      'user-login',
      'user-password',
      'user-firstname',
      'user-lastname',
      $employer,
      [Role::Director],
      'user-email',
      'user-phone'
    );

    $manager = self::getContainer()
      ->get('doctrine')
      ->getManager();
    $manager->persist($employer);
    $manager->persist($user);
    $manager->flush();
  }

  /**
   * Test que l'on puisse charger
   * un utilisateur avec son identifiant.
   */
  public function testCanLoadAUserWithItsIdentifier(): void
  {
    $this->addUser();

    /**
     * @var UserRepository $localeRepository le dépôt de l'utilisateur.
     */
    $localeRepository = static::getContainer()->get(UserRepository::class);
    $foundUser = $localeRepository->loadUserByIdentifier('user-login');

    $roles = $foundUser->getRoles();

    self::assertCount(1, $roles);
    self::assertContains(Role::Director->value, $roles);
  }

  /**
   * Test que l'on ne puisse pas charger
   * un utilisateur inconnu.
   */
  public function testCanLoadAUserWithAnUnknownIdentifier(): void
  {
    /**
     * @var UserRepository $localeRepository le dépôt de l'utilisateur.
     */
    $localeRepository = static::getContainer()->get(UserRepository::class);
    $notFoundUser = $localeRepository->loadUserByIdentifier('not-a-user-login');

    self::assertNull($notFoundUser);
  }

  /**
   * Ajoute un utilisateur désactivé.
   */
  private function addADeactivatedUser(): void
  {
    $container = self::getContainer();
    $picture = new Picture(__DIR__ . '/../../../fixtures/test.jpg', PictureTag::After);
    $employer = Employer::new('employer-siren', 'employer-name', 45, 54);
    $user = User::new(
      'user-login',
      'user-password',
      'user-firstname',
      'user-lastname',
      $employer,
      [Role::Director],
      'user-email',
      'user-phone',
      $picture,
      false
    );
    $user->setPassword($container->get('security.user_password_hasher')->hashPassword($user, 'user-password'));

    $manager = $container->get('doctrine')->getManager();
    $manager->persist($employer);
    $manager->persist($picture);
    $manager->persist($user);
    $manager->flush();
  }

  /**
   * Test que l'on ne puisse pas s'authentifier
   * avec un compte désactivé.
   */
  public function testCanNotLoadADeactivatedUser(): void
  {
    $this->addADeactivatedUser();

    /**
     * @var UserRepository $localeRepository le dépôt de l'utilisateur.
     */
    $localeRepository = static::getContainer()->get(UserRepository::class);
    $notFoundUser = $localeRepository->loadUserByIdentifier('user-login');

    self::assertNull($notFoundUser);
  }
}
