<?php

namespace App\Tests\Encoder;

use App\Encoder\MultipartDecoder;
use App\Service\S3Service;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

class MultipartDecoderTest extends TestCase
{
  private MultipartDecoder $multipartDecoder;
  private RequestStack $requestStack;
  private S3Service $s3Service;

  protected function setUp(): void
  {
    $this->requestStack = new RequestStack();
    $this->s3Service = $this->createMock(S3Service::class);
    $this->multipartDecoder = new MultipartDecoder($this->requestStack, $this->s3Service);
  }

  public function testSupportsDecoding(): void
  {
    $this->assertTrue($this->multipartDecoder->supportsDecoding('multipart'));
    $this->assertFalse($this->multipartDecoder->supportsDecoding('json'));
  }

  public function testDecodeWithoutRequest(): void
  {
    $this->assertNull($this->multipartDecoder->decode('', 'multipart'));
  }

  public function testDecodeWithoutFile(): void
  {
    $request = new Request();
    $this->requestStack->push($request);

    $this->expectException(\LogicException::class);
    $this->expectExceptionMessage('MultipartDecoder :: file must be send with fileName key');

    $this->multipartDecoder->decode('', 'multipart');
  }

  public function testDecodeWithFile(): void
  {
    $file = $this->createMock(UploadedFile::class);
    $file->expects($this->once())
      ->method('getClientOriginalName')
      ->willReturn('originalName.jpg');
    $file->expects($this->once())
      ->method('getClientOriginalExtension')
      ->willReturn('jpg');

    $request = new Request([], [], [], [], ['fileName' => $file]);
    $this->requestStack->push($request);

    $this->s3Service->expects($this->once())
      ->method('putFile')
      ->with($file, $this->anything());

    $this->s3Service->expects($this->once())
      ->method('fileExist')
      ->willReturn(false);

    $result = $this->multipartDecoder->decode('', 'multipart');

    $this->assertIsArray($result);
    $this->assertArrayHasKey('fileName', $result);
  }
}
