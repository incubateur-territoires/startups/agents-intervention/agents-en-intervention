<?php

namespace App\Tests\EventListener;

use App\Entity\User;
use App\Event\UserConnectedEvent;
use App\EventListener\LoginListener;
use Doctrine\ORM\EntityManagerInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use PHPUnit\Framework\TestCase;
use Psr\EventDispatcher\EventDispatcherInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\Storage\MockArraySessionStorage;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;

class LoginListenerTest extends TestCase
{

  private $entityManager;
  private $eventDispatcher;
  private $tokenManager;
  private $loginListener;

  protected function setUp(): void
  {
    $this->entityManager = $this->createMock(EntityManagerInterface::class);
    $this->eventDispatcher = $this->createMock(EventDispatcherInterface::class);
    $this->logger = $this->createMock(LoggerInterface::class);
    $this->tokenManager = $this->createMock(JWTTokenManagerInterface::class);

    $this->loginListener = new LoginListener(
      $this->entityManager,
      $this->eventDispatcher,
      $this->tokenManager,
      $this->logger,
    );
  }

  public function testOnLoginSuccess(): void
  {
    $user = new User();
    $token = new UsernamePasswordToken($user, 'main', ['ROLE_USER']);
    $request = new Request();
    $session = new Session(new MockArraySessionStorage());
    $request->setSession($session);
    $event = new InteractiveLoginEvent($request, $token);

    $this->entityManager->expects($this->once())
      ->method('persist')
      ->with($user);

    $this->entityManager->expects($this->once())
      ->method('flush');

    $this->eventDispatcher->expects($this->once())
      ->method('dispatch')
      ->with($this->isInstanceOf(UserConnectedEvent::class));

    $this->tokenManager->expects($this->once())
      ->method('create')
      ->with($user)
      ->willReturn('jwt-token');

    $this->loginListener->onLoginSuccess($event);

    $this->assertEquals('jwt-token', $session->get('jwt'));
    $this->assertInstanceOf(\DateTimeImmutable::class, $user->getConnectedAt());
  }

}
