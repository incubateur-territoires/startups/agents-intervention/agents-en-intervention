<?php

namespace App\Tests\EventListener\Doctrine;

use App\Entity\ActionLoggableInterface;
use App\Entity\Location;
use App\Entity\Roadmap;
use App\EventListener\Doctrine\DoctrineAuditTrailListener;
use App\Service\AuditTrailLogger;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Event\OnFlushEventArgs;
use Doctrine\ORM\UnitOfWork;
use PHPUnit\Framework\TestCase;
use App\Entity\ActionLog;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;

class DoctrineAuditTrailListenerTest extends TestCase
{
  private $entityManager;
  private $auditTrailLogger;
  private $listener;
  private $logger;

  protected function setUp(): void
  {
    $this->entityManager = $this->createMock(EntityManagerInterface::class);
    $this->auditTrailLogger = $this->createMock(AuditTrailLogger::class);
    $this->logger = $this->createMock(LoggerInterface::class);
    $this->listener = new DoctrineAuditTrailListener($this->auditTrailLogger, $this->logger);
  }

  public function testPostPersist(): void
  {
    $location = new Location('street', 'city', 'state', 'zip', 42, 24);

    $uow = $this->createMock(UnitOfWork::class);
    $uow->method('getScheduledEntityInsertions')->willReturn([$location]);
    $om = $this->createMock(EntityManagerInterface::class);
    $om->method('getUnitOfWork')->willReturn($uow);
    $eventArgs = $this->createMock(OnFlushEventArgs::class);
    $eventArgs->method('getObjectManager')->willReturn($om);

    $this->auditTrailLogger
      ->expects($this->once())
      ->method('log')
      ->with('entity_create', Location::class, null, [
        'entity' => Location::class,
        'changes' => []
      ])
      /*->willReturnCallback(function ($type, $entityClass, $entity, $changes) {
        return ActionLog::new(
          type: $type,
          entityClass: $entityClass,
          entityId: null,
          changes: '[]',
          userIdentifier: 1
        );
      })*/;

    $this->listener->__invoke($eventArgs);

    // Vérifiez que l'ActionLog a été créé correctement
//    $logs = $this->entityManager->getRepository(ActionLog::class)->findAll();
//    $this->assertCount(1, $logs);
//    $log = $logs[0];
//    $this->assertEquals('create', $log->getType());
//    $this->assertEquals(Location::class, $log->getEntityClass());
//    $this->assertEquals(1, $log->getEntityId());
  }

  public function testPostUpdate(): void
  {
    $location = new Location('street', 'city', 'state', 'zip', 42, 24);
    $reflection = new \ReflectionClass($location);
    $property = $reflection->getProperty('id');
    $property->setAccessible(true);
    $property->setValue($location, 42);

    $uow = $this->createMock(UnitOfWork::class);
    $uow->method('getScheduledEntityUpdates')->willReturn([$location]);
    $om = $this->createMock(EntityManagerInterface::class);
    $om->method('getUnitOfWork')->willReturn($uow);
    $eventArgs = $this->createMock(OnFlushEventArgs::class);
    $eventArgs->method('getObjectManager')->willReturn($om);

    $this->auditTrailLogger
      ->expects($this->once())
      ->method('log')
      ->with('entity_update', Location::class, 42, [
        'entity' => Location::class,
        'changes' => []
      ]);

    $this->listener->__invoke($eventArgs);
  }


  public function testPostUpdateCollection(): void
  {
    $location = new Location('street', 'city', 'state', 'zip', 42, 24);
    $reflection = new \ReflectionClass($location);
    $property = $reflection->getProperty('id');
    $property->setAccessible(true);
    $property->setValue($location, 42);
    $location2 = new Location('street', 'city', 'state', 'zip', 42, 24);
    $reflection = new \ReflectionClass($location2);
    $property = $reflection->getProperty('id');
    $property->setAccessible(true);
    $property->setValue($location2, 43);

    $collection = new ArrayCollection([$location, $location2]);
    $uow = $this->createMock(UnitOfWork::class);
    $uow->method('getScheduledCollectionUpdates')->willReturn([$collection]);
    $om = $this->createMock(EntityManagerInterface::class);
    $om->method('getUnitOfWork')->willReturn($uow);
    $eventArgs = $this->createMock(OnFlushEventArgs::class);
    $eventArgs->method('getObjectManager')->willReturn($om);

    $this->auditTrailLogger
      ->expects($this->exactly(2))
      ->method('log')
    ;

    $this->listener->__invoke($eventArgs);
  }

  public function testPostRemove(): void
  {
    $location = new Location('street', 'city', 'state', 'zip', 42, 24);
    $reflection = new \ReflectionClass($location);
    $property = $reflection->getProperty('id');
    $property->setAccessible(true);
    $property->setValue($location, 42);

    $uow = $this->createMock(UnitOfWork::class);
    $uow->method('getScheduledEntityDeletions')->willReturn([$location]);
    $om = $this->createMock(EntityManagerInterface::class);
    $om->method('getUnitOfWork')->willReturn($uow);
    $eventArgs = $this->createMock(OnFlushEventArgs::class);
    $eventArgs->method('getObjectManager')->willReturn($om);

    $this->auditTrailLogger
      ->expects($this->once())
      ->method('log')
      ->with('entity_delete', Location::class, 42, [
        'entity' => Location::class,
        'changes' => []
      ]);

    $this->listener->__invoke($eventArgs);
  }


  public function testPostRemoveCollection(): void
  {
    $location = new Location('street', 'city', 'state', 'zip', 42, 24);
    $reflection = new \ReflectionClass($location);
    $property = $reflection->getProperty('id');
    $property->setAccessible(true);
    $property->setValue($location, 42);
    $location2 = new Location('street', 'city', 'state', 'zip', 42, 24);
    $reflection = new \ReflectionClass($location2);
    $property = $reflection->getProperty('id');
    $property->setAccessible(true);
    $property->setValue($location2, 43);

    $collection = new ArrayCollection([$location, $location2]);
    $uow = $this->createMock(UnitOfWork::class);
    $uow->method('getScheduledCollectionDeletions')->willReturn([$collection]);
    $om = $this->createMock(EntityManagerInterface::class);
    $om->method('getUnitOfWork')->willReturn($uow);
    $eventArgs = $this->createMock(OnFlushEventArgs::class);
    $eventArgs->method('getObjectManager')->willReturn($om);

    $this->auditTrailLogger
      ->expects($this->exactly(2))
      ->method('log')
    ;

    $this->listener->__invoke($eventArgs);
  }

  public function testPostPersistNonActionLoggableInterface(): void
  {
    $roadmap = new Roadmap();

    $uow = $this->createMock(UnitOfWork::class);
    $uow->method('getScheduledEntityInsertions')->willReturn([$roadmap]);
    $om = $this->createMock(EntityManagerInterface::class);
    $om->method('getUnitOfWork')->willReturn($uow);
    $eventArgs = $this->createMock(OnFlushEventArgs::class);
    $eventArgs->method('getObjectManager')->willReturn($om);

    $this->auditTrailLogger
      ->expects($this->never())
      ->method('log');

    $this->listener->__invoke($eventArgs);
  }
}
