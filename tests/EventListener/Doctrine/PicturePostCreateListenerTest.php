<?php

namespace App\Tests\EventListener\Doctrine;

use App\Entity\Picture;
use App\Entity\PictureTag;
use App\EventListener\Doctrine\PicturePostCreateListener;
use App\Service\S3Service;
use Imagine\Exception\RuntimeException;
use Imagine\Gd\Imagine;
use Imagine\Image\Box;
use Imagine\Image\ImageInterface;
use Imagine\Image\ManipulatorInterface;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;

class PicturePostCreateListenerTest extends TestCase
{
  private S3Service $s3Service;
  private PicturePostCreateListener $listener;

  protected function setUp(): void
  {
    $this->s3Service = $this->createMock(S3Service::class);
    $this->imagine = $this->createMock(Imagine::class);
    $this->logger = $this->createMock(LoggerInterface::class);
    $this->listener = new PicturePostCreateListener($this->logger, $this->s3Service);
  }

  public function testInvokeInTestEnvironment(): void
  {
    putenv('APP_ENV=test');

    $picture = $this->createMock(Picture::class);
    $this->s3Service->expects($this->never())->method('getFile');

    $this->listener->__invoke($picture);

    putenv('APP_ENV=test');
  }

  public function testInvokeWithInvalidFile(): void
  {
    putenv('APP_ENV=prod');

    $picture = $this->createMock(Picture::class);
    $picture->method('getFileName')->willReturn('invalid-file.png');

    $this->s3Service->method('getFile')->willReturn('file-content');

    $this->expectException(RuntimeException::class);
    $this->listener->__invoke($picture);

    $this->s3Service->expects($this->never())->method('putFileContent');

    putenv('APP_ENV=test');
  }

  public function testInvokeWithValidFile(): void
  {
    putenv('APP_ENV=prod');

    $picture = $this->createMock(Picture::class);
    $picture->method('getFileName')->willReturn('test.png');
    $picture->method('getTag')->willReturn(PictureTag::Avatar);

//    dd(realpath(__DIR__ . '/../../../fixtures/test.jpg'));
    $fileContent = file_get_contents(__DIR__ . '/../../../fixtures/test.jpg');
    $this->s3Service->method('getFile')->willReturn($fileContent);

    $image = $this->createMock(ImageInterface::class);
    $this->imagine->method('load')->willReturn($image);

//    $image->expects($this->exactly(2))
//      ->method('thumbnail')
//      ->willReturnSelf();
//
//    $image->expects($this->exactly(3))
//      ->method('get')
//      ->willReturn('image-content');

    $this->s3Service->expects($this->exactly(3))
      ->method('putFileContent');
//      ->withConsecutive(
//        ['image-content', 'users/original/test.png'],
//        ['image-content', 'users/small/test.png'],
//        ['image-content', 'users/medium/test.png']
//      );
    $this->listener->__invoke($picture);

    putenv('APP_ENV=test');
  }
}
