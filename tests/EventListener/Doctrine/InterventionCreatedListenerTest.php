<?php

namespace App\Tests\EventListener\Doctrine;

use App\Entity\Intervention;
use App\Event\InterventionCreatedEvent;
use App\Event\InterventionType;
use App\Event\RecurringInterventionCreatedEvent;
use App\Event\RequestInterventionCreatedEvent;
use App\Event\SimpleInterventionCreatedEvent;
use App\EventListener\Doctrine\InterventionCreatedListener;
use App\Service\RecurrenceService;
use PHPUnit\Framework\TestCase;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

class InterventionCreatedListenerTest extends TestCase
{
  private EventDispatcherInterface $eventDispatcher;
  private RecurrenceService $recurrenceService;
  private InterventionCreatedListener $listener;

  protected function setUp(): void
  {
    $this->eventDispatcher = $this->createMock(EventDispatcherInterface::class);
    $this->recurrenceService = $this->createMock(RecurrenceService::class);
    $this->listener = new InterventionCreatedListener($this->eventDispatcher, $this->recurrenceService);
  }

  public function testDispatchesInterventionCreatedEvent(): void
  {
    $intervention = $this->createMock(Intervention::class);
    $intervention->method('hasRecurrenceReference')->willReturn(false);
    $intervention->method('getInterventionType')->willReturn(InterventionType::Request);

    $calls = [];
    $this->eventDispatcher->method('dispatch')
      ->willReturnCallback(function ($arg) use (&$calls) {
        $calls[] = $arg;
        return $arg;
      });

    $this->listener->__invoke($intervention);

    $this->assertCount(2, $calls);
    $this->assertInstanceOf(InterventionCreatedEvent::class, $calls[0]);
    $this->assertInstanceOf(RequestInterventionCreatedEvent::class, $calls[1]);
  }

  public function testDispatchesRequestInterventionCreatedEvent(): void
  {
    $intervention = $this->createMock(Intervention::class);
    $intervention->method('hasRecurrenceReference')->willReturn(false);
    $intervention->method('getInterventionType')->willReturn(InterventionType::Request);

    $this->eventDispatcher->method('dispatch')
      ->willReturnCallback(function ($arg) use (&$calls) {
        $calls[] = $arg;
        return $arg;
      });

    $this->listener->__invoke($intervention);

    $this->assertCount(2, $calls);
    $this->assertInstanceOf(InterventionCreatedEvent::class, $calls[0]);
    $this->assertInstanceOf(RequestInterventionCreatedEvent::class, $calls[1]);
  }

  public function testDispatchesRecurringInterventionCreatedEvent(): void
  {
    $intervention = $this->createMock(Intervention::class);
    $intervention->method('getInterventionType')->willReturn(InterventionType::Recurring);

    $this->recurrenceService->expects($this->once())
      ->method('addGeneratedInterventionsFor')
      ->with($intervention);

    $this->eventDispatcher->method('dispatch')
      ->willReturnCallback(function ($arg) use (&$calls) {
        $calls[] = $arg;
        return $arg;
      });

    $this->listener->__invoke($intervention);

    $this->assertCount(2, $calls);
    $this->assertInstanceOf(InterventionCreatedEvent::class, $calls[0]);
    $this->assertInstanceOf(RecurringInterventionCreatedEvent::class, $calls[1]);
  }

  public function testDispatchesSimpleInterventionCreatedEvent(): void
  {
    $intervention = $this->createMock(Intervention::class);
    $intervention->method('hasRecurrenceReference')->willReturn(false);
    $intervention->method('getInterventionType')->willReturn(InterventionType::Simple);

    $this->eventDispatcher->method('dispatch')
      ->willReturnCallback(function ($arg) use (&$calls) {
        $calls[] = $arg;
        return $arg;
      });

    $this->listener->__invoke($intervention);

    $this->assertCount(2, $calls);
    $this->assertInstanceOf(InterventionCreatedEvent::class, $calls[0]);
    $this->assertInstanceOf(SimpleInterventionCreatedEvent::class, $calls[1]);
  }

  public function testThrowsLogicExceptionForUnknownType(): void
  {
    $this->expectException(\LogicException::class);

    $intervention = $this->createMock(Intervention::class);
    $intervention->method('getInterventionType')->willReturn(InterventionType::Unknown);

    $this->listener->__invoke($intervention);
  }
}
