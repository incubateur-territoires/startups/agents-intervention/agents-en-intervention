<?php

namespace App\Tests\EventListener\Doctrine;

use App\Entity\Picture;
use App\Entity\PictureTag;
use App\EventListener\Doctrine\PicturePostLoadListener;
use App\Service\S3Service;
use PHPUnit\Framework\TestCase;

class PicturePostLoadListenerTest extends TestCase
{
  public function testInvoke(): void
  {
    $s3Service = $this->createMock(S3Service::class);
    $s3Service->method('getSignedURL')
      ->willReturn('https://example.com/signed-url');

    $picture = new Picture('test.jpg');

    $listener = new PicturePostLoadListener($s3Service);
    $listener($picture);

    $this->assertEquals('https://example.com/signed-url', $picture->getUrl());
  }

  public function testInvokeWithAvatarTag(): void
  {
    $s3Service = $this->createMock(S3Service::class);
    $s3Service->method('getSignedURL')
      ->willReturnCallback(fn($path) => 'https://example.com/' . $path);

    $picture = new Picture('avatar.jpg');
    $picture->setTag(PictureTag::Avatar);

    $listener = new PicturePostLoadListener($s3Service);
    $listener($picture);

    $this->assertEquals([
      'small' => 'https://example.com/users/small/avatar.png',
      'original' => 'https://example.com/users/original/avatar.png',
      'medium' => 'https://example.com/users/medium/avatar.png',
    ], $picture->getUrls());
  }

  public function testInvokeWithNonAvatarTag(): void
  {
    $s3Service = $this->createMock(S3Service::class);
    $s3Service->method('getSignedURL')
      ->willReturnCallback(fn($path) => 'https://example.com/' . $path);

    $picture = new Picture('intervention.jpg');
    $picture->setTag(PictureTag::After);

    $listener = new PicturePostLoadListener($s3Service);
    $listener($picture);

    $this->assertEquals([
      'small' => 'https://example.com/interventions/small/intervention.png',
      'original' => 'https://example.com/interventions/original/intervention.png',
      'medium' => 'https://example.com/interventions/medium/intervention.png',
    ], $picture->getUrls());
  }

}
