<?php

namespace App\Tests\EventListener\Domain;

use App\Bridge\Mattermost\Message;
use App\Entity\Employer;
use App\Entity\User;
use App\Event\EmployerCreatedEvent;
use App\EventListener\Domain\EmployerCreatedListener;
use App\Repository\EmployerRepository;
use App\Tests\Api\PermissionsTestCase;
use NotFloran\MjmlBundle\Renderer\RendererInterface;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\Mime\Email;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Twig\Environment;

class EmployerCreatedListenerTest extends PermissionsTestCase
{
  private $twig;
  private $employerRepository;
  private $mailer;
  private $messageBus;
  private $mjml;
  private $urlGenerator;

  private EmployerCreatedListener $listener;

  #[\Override]
  public function setUp(): void
  {
    parent::setUp();

    $this->twig = $this->createMock(Environment::class);
    $this->employerRepository = $this->createMock(EmployerRepository::class);
    $this->mailer = $this->createMock(MailerInterface::class);
    $this->messageBus = $this->createMock(MessageBusInterface::class);
    $this->mjml = $this->createMock(RendererInterface::class);
    $this->urlGenerator = $this->createMock(UrlGeneratorInterface::class);
    $this->listener = new EmployerCreatedListener(
      $this->twig,
      $this->employerRepository,
      $this->mailer,
      $this->messageBus,
      $this->mjml,
      $this->urlGenerator
    );
  }

  public function testOnEmployerCreatedByUser(): void
  {
    $employer = Employer::new('26574E7685', 'ANCT', 42, 4, 'America/Guadeloupe');
    $user = User::new('test', 'test', 'test', 'test', $employer, ['ROLE_ADMIN_EMPLOYER'], 'test@agentsenintervention.anct.gouv.fr', '0987654321', null, true);
    $employer->addUser($user);
    $event = new EmployerCreatedEvent($employer, false);

    $invokedCount = $this->exactly(2);
    $this->twig->expects($invokedCount)
      ->method('render')
      ->willReturnCallback(function (...$parameters) use ($invokedCount, $employer, $user, $event) {
        if ($invokedCount->numberOfInvocations() === 1) {
          $this->assertSame([
              'emails/nouvelle-collectivite-mail-support.mjml.twig',
              ['anct' => self::$employer_anct, 'user' => $user, 'employer' => $event->employer]]
            , $parameters);
          return 'emails/nouvelle-collectivite-mail-support.mjml.twig';
        }
        if ($invokedCount->numberOfInvocations() === 2) {
          $this->assertSame(['emails/nouvelle-collectivite-mail-admin.mjml.twig', []], $parameters);
          return 'emails/nouvelle-collectivite-mail-admin.mjml.twig';
        }

        return null;
      });

    $this->employerRepository->expects($this->once())
      ->method('getAnct')
      ->willReturn(self::$employer_anct);

    $invokedCount = $this->exactly(2);
    $this->mailer->expects($invokedCount)
      ->method('send')
      ->willReturnCallback(function (...$parameters) use ($invokedCount) {
        if ($invokedCount->numberOfInvocations() === 1) {
          $this->assertInstanceOf(Email::class, $parameters[0]);
          $this->assertSame('La collectivité ANCT s\'est inscrite sur Agents en Intervention', $parameters[0]->getSubject());
          $this->assertEquals([new Address('emailactive@email.com'), new Address('email@email.com')], $parameters[0]->getTo());
          return 'sent';
        }
        if ($invokedCount->numberOfInvocations() === 2) {
          $this->assertInstanceOf(Email::class, $parameters[0]);
          $this->assertSame('Bienvenue sur Agents en Intervention', $parameters[0]->getSubject());
          $this->assertEquals([new Address('test@agentsenintervention.anct.gouv.fr')], $parameters[0]->getTo());
          return 'sent';
        }

        return null;
      });

    $this->messageBus->expects($this->once())
      ->method('dispatch')
      ->willReturn(new Envelope(new Message('test', 'test')));

    $invokedCount = $this->exactly(2);
    $this->mjml->expects($invokedCount)
      ->method('render')
      ->willReturnCallback(function (...$parameters) use ($invokedCount) {
        if ($invokedCount->numberOfInvocations() === 1) {
          $this->assertSame(['emails/nouvelle-collectivite-mail-support.mjml.twig'], $parameters);
          return 'rendered';
        }
        if ($invokedCount->numberOfInvocations() === 2) {
          $this->assertSame(['emails/nouvelle-collectivite-mail-admin.mjml.twig'], $parameters);
          return 'rendered';
        }

        return null;
      });

    $this->urlGenerator->expects($this->once())
      ->method('generate')
      ->willReturn('test');

    $this->listener->__invoke($event);
  }

  public function testOnEmployerCreatedBybAdmin(): void
  {
    $employer = Employer::new('26574E7685', 'My Title', 42, 4, 'America/Guadeloupe');
    $event = new EmployerCreatedEvent($employer, true);

    $this->twig->expects($this->never())
      ->method('render');
    $this->employerRepository->expects($this->never())
      ->method('getAnct');
    $this->mailer->expects($this->never())
      ->method('send');
    $this->messageBus->expects($this->never())
      ->method('dispatch');
    $this->mjml->expects($this->never())
      ->method('render');
    $this->urlGenerator->expects($this->never())
      ->method('generate');

    $this->listener->__invoke($event);
  }


  public function testOnEmployerCreatedByUserWithEmployerWithoutUser(): void
  {
    $employer = Employer::new('26574E7685', 'ANCT', 42, 4, 'America/Guadeloupe');
    $event = new EmployerCreatedEvent($employer, false);

    $this->twig->expects($this->never())
      ->method('render');
    $this->employerRepository->expects($this->never())
      ->method('getAnct');
    $this->mailer->expects($this->never())
      ->method('send');
    $this->messageBus->expects($this->never())
      ->method('dispatch');
    $this->mjml->expects($this->never())
      ->method('render');
    $this->urlGenerator->expects($this->never())
      ->method('generate');

    $this->listener->__invoke($event);
  }

  protected function getMethod(): string
  {
    return 'POST';
  }
}
