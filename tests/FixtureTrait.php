<?php

namespace App\Tests;

use App\Entity\Type;
use Doctrine\ORM\EntityManagerInterface;
use Nelmio\Alice\Loader\NativeLoader;
use PHPUnit\Framework\Attributes\Before;

trait FixtureTrait
{
  #[Before]
  protected function beforeFixtures(): void
  {
    $kernel = static::createKernel();
    $kernel->boot();
    $doctrine = $kernel->getContainer()->get('doctrine');

    $ret = $doctrine->getManager()->getRepository(Type::class)->findAll();
    if (count($ret) === 0) {
      static::_addFixtures($doctrine->getManager());
    }

    $kernel->shutdown();
  }

  /**
   * Ajoute les fixtures.
   * @param EntityManagerInterface $entityManager le gestionnaire d'entité.
   */
  private function addFixtures(EntityManagerInterface $entityManager): void
  {
    self::_addFixtures($entityManager);
  }

  private static function _addFixtures(EntityManagerInterface $entityManager): void
  {
    $loader = new NativeLoader();
    $categorySet = $loader->loadFile(__DIR__ . '/../fixtures/category.yaml');

    foreach ($categorySet->getObjects() as $category) {
      $entityManager->persist($category);
    }

    $locationSet = $loader->loadFile(__DIR__ . '/../fixtures/location.yaml');

    foreach ($locationSet->getObjects() as $location) {
      $entityManager->persist($location);
    }

    $typeSet = $loader->loadFile(
      __DIR__ . '/../fixtures/type.yaml',
      $categorySet->getParameters(),
      $categorySet->getObjects()
    );

    foreach ($typeSet->getObjects() as $type) {
      $entityManager->persist($type);
    }


    $employerSet = $loader->loadFile(__DIR__ . '/../fixtures/employer.yaml');

    foreach ($employerSet->getObjects() as $employer) {
      $entityManager->persist($employer);
    }

    $userSet = $loader->loadFile(
      __DIR__ . '/../fixtures/user.yaml',
      $employerSet->getParameters(),
      $employerSet->getObjects(),
    );

    foreach ($userSet->getObjects() as $user) {
      $entityManager->persist($user);
    }
    $entityManager->flush();

    $interventionSet = $loader->loadFile(
      __DIR__ . '/../fixtures/intervention.yaml',
      array_merge(
        $categorySet->getParameters(),
        $typeSet->getParameters(),
        $userSet->getParameters(),
        $employerSet->getParameters(),
        $locationSet->getParameters()
      ),
      array_merge(
        $categorySet->getObjects(),
        $typeSet->getObjects(),
        $userSet->getObjects(),
        $employerSet->getObjects(),
        $locationSet->getObjects()
      )
    );
  }

}
