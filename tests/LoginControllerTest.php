<?php

namespace App\Tests;

use App\Entity\Employer;
use App\Entity\Role;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class LoginControllerTest extends WebTestCase
{
  private KernelBrowser $client;

  #[\Override]
  protected function setUp(): void
  {
    $this->client = static::createClient();
    $container = static::getContainer();
    $em = $container->get('doctrine.orm.entity_manager');
    $userRepository = $em->getRepository(User::class);

    // Remove any existing users from the test database
    foreach ($userRepository->findAll() as $user) {
      $em->remove($user);
    }

    $em->flush();

    // Create a User fixture
    /** @var UserPasswordHasherInterface $passwordHasher */
    $passwordHasher = $container->get('security.user_password_hasher');


    $employer = Employer::new('siren', 'commune', 4, 42);
    $user = (User::new(
      'michel',
      'michel',
      'michel',
      'michel',
      $employer,
      [Role::Director->value]
    ))->setEmail('email@example.com');
    $user->setPassword($passwordHasher->hashPassword($user, 'password'));

    $em->persist($employer);
    $em->persist($user);
    $em->flush();
  }

  public function testLogin(): void
  {
    // Denied - Can't login with invalid email address.
    $this->client->request(Request::METHOD_GET, '/connexion');
    self::assertResponseIsSuccessful();

    $this->client->submitForm('Se connecter', [
      '_username' => 'doesNotExist',
      '_password' => 'password',
    ]);

    self::assertResponseRedirects('/connexion');
    $this->client->followRedirect();

    // Ensure we do not reveal if the user exists or not.
    self::assertSelectorTextContains('.fr-alert--error p', 'Identifiants invalides.');

    // Denied - Can't login with invalid password.
    $this->client->request(Request::METHOD_GET, '/connexion');
    self::assertResponseIsSuccessful();

    $this->client->submitForm('Se connecter', [
      '_username' => 'michel',
      '_password' => 'bad-password',
    ]);

    self::assertResponseRedirects('/connexion');
    $this->client->followRedirect();

    // Ensure we do not reveal the user exists but the password is wrong.
    self::assertSelectorTextContains('.fr-alert--error p', 'Identifiants invalides.');
    
    // Success - Login with valid credentials is allowed.
    $this->client->submitForm('Se connecter', [
      '_username' => 'michel',
      '_password' => 'password',
    ]);

    self::assertResponseRedirects('http://localhost/connexion-reussie');
    $this->client->followRedirect();
    self::assertResponseRedirects();
    $this->client->followRedirect();

    self::assertSelectorNotExists('.fr-alert--error p');
    self::assertResponseIsSuccessful();
  }
}
