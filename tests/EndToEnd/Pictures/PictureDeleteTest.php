<?php

declare(strict_types=1);

namespace App\Tests\EndToEnd\Pictures;

use App\Entity\Category;
use App\Entity\Employer;
use App\Entity\Intervention;
use App\Entity\Location;
use App\Entity\Picture;
use App\Entity\PictureTag;
use App\Entity\Priority;
use App\Entity\Role;
use App\Entity\Status;
use App\Entity\Type;
use App\Entity\User;
use App\Repository\UserRepository;
use App\Tests\Api\AuthenticationTestCase;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\Attributes as PA;
use Zenstruck\Foundry\Test\Factories;
use Zenstruck\Foundry\Test\ResetDatabase;

/**
 * Test DELETE /pictures/{id}.
 */
#[
  PA\CoversClass(Picture::class),
  PA\UsesClass(Category::class),
  PA\UsesClass(Employer::class),
  PA\UsesClass(Intervention::class),
  PA\UsesClass(Location::class),
  PA\UsesClass(Priority::class),
  PA\UsesClass(Role::class),
  PA\UsesClass(Status::class),
  PA\UsesClass(Type::class),
  PA\UsesClass(User::class),
  PA\UsesClass(UserRepository::class),
  PA\Group('e2e'),
  PA\Group('e2e_picture'),
  PA\Group('e2e_picture_delete'),
  PA\Group('picture'),
  PA\TestDox('Picture')
]
final class PictureDeleteTest extends AuthenticationTestCase
{

  use ResetDatabase, Factories;
  /**
   * Ajoute une photo.
   * @param EntityManagerInterface $entityManager le gestionnaire d'entité.
   */
  private function addPicture(EntityManagerInterface $entityManager): void
  {
    $status = Status::ToDo;
    $priority = Priority::Normal;

    $category = new Category();
    $category->setName('category-name');
    $category->setPicture('category-picture');

    $type = new Type();
    $type->setName('type-name');
    $type->setCategory($category);
    $type->setPicture('type-picture');

    $employer = Employer::new('employer-siren', 'employer-name', 4.4, 6.6);

    $user = User::new(
      'user-login',
      'user-password',
      'user-firstname',
      'user-lastname',
      $employer,
      [Role::Director]
    );
    $user->setEmployer($employer);

    $location = new Location(null, null, null, null, 1.1, 2.2);

    $intervention = new Intervention(
      'intervention-description',
      $priority,
      $type,
      $location
    );
    $intervention->setStatus($status);
    $intervention->setAuthor($user);
    $intervention->setEmployer($employer);
    $intervention->setTitle('Intervention');
    $intervention->setLogicId(42);

    $picture = new Picture('picture-file', PictureTag::Before, null, $intervention);
    $intervention->addPicture($picture);

    $entityManager->persist($category);
    $entityManager->persist($type);
    $entityManager->persist($user);
    $entityManager->persist($employer);
    $entityManager->persist($location);
    $entityManager->persist($intervention);
    $entityManager->persist($picture);
    $entityManager->flush();
  }

  /**
   * Test qu'une photo puisse être supprimée.
   */
  public function testIsDeletedFromTheDatabaseWithDelete(): never
  {
    $this->markTestSkipped();
    $client = static::createClient();
    $entityManager = static::$kernel->getContainer()
      ->get('doctrine')
      ->getManager();

    $this->addPicture($entityManager);

    $client->request('DELETE', '/pictures/1');

    $client->request('DELETE', '/pictures/1', [
      'headers' => [
        'Content-Type' => 'application/json',
        'authorization' => 'Bearer ' . $this->getJWT(),
      ],
    ]);

    $picture = $entityManager->find(Picture::class, 1);

    self::assertNull($picture, 'The picture has not been deleted.');
  }
}
