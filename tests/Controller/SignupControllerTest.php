<?php

declare(strict_types=1);

namespace App\Tests\Controller;

use App\Repository\EmployerRepository;
use App\Service\EntrepriseService;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;

class SignupControllerTest extends WebTestCase
{
  private KernelBrowser $client;

  protected function setUp(): void
  {
    $this->markTestSkipped('Le mock des services ne fonctionne pas');
    $this->client = self::createClient();

    // Mocking the services
    $entrepriseServiceMock = $this->createMock(EntrepriseService::class);
    $employerRepositoryMock = $this->createMock(EmployerRepository::class);

    // Configure the mocks
    $entrepriseServiceMock->method('isCollectiviteTerritoriale')
      ->willReturn(true);
    $employerRepositoryMock->method('findOneBy')
      ->willReturn(null);

    // Replace the real services with mocks
    $this->client->getContainer()->set(EntrepriseService::class, $entrepriseServiceMock);
    $this->client->getContainer()->set(EmployerRepository::class, $employerRepositoryMock);
  }

  public function testSignup(): void
  {
    $crawler = $this->client->request(Request::METHOD_GET, '/inscription');

    // Vérifie que la page de l'inscription est accessible
    $this->assertResponseIsSuccessful();

    // Soumet le formulaire avec des données valides
    $form = $crawler->selectButton('Suivant')->form([
      'signup[siren]' => '112901318',
      'signup[siret]' => '11290131800015',
      'signup[latitude]' => '48.8566',
      'signup[longitude]' => '2.3522'
    ]);
    $this->client->submit($form);

    // Vérifie la redirection vers la page suivante
    $this->assertResponseRedirects('/inscription/user');
    $this->client->followRedirect();

    // Vérifie que la page de l'inscription utilisateur est accessible
    $this->assertResponseIsSuccessful();

    // Vérifie que les données du formulaire précédent sont bien enregistrées en session
//    $this->assertEquals([], $request->getSession()->get('signup'));
    // Vérifie que les données du formulaire précédent sont bien enregistrées en session
  }

  public function testSignupUser(): void
  {
    $this->markTestSkipped('This test has not been implemented yet.');
    $session = self::getContainer()->get('session');
    $session->set('signup', ['siren' => '123456789']);
    $session->save();

    $crawler = $this->client->request(Request::METHOD_GET, '/inscription/user');

    // Vérifie que la page de l'inscription utilisateur est accessible
    $this->assertResponseIsSuccessful();

    // Soumet le formulaire avec des données valides
    $form = $crawler->selectButton('Submit')->form([
      'signup_user[firstname]' => 'John',
      'signup_user[lastname]' => 'Doe',
      'signup_user[email]' => 'john.doe@example.com',
      'signup_user[phoneNumber]' => '0123456789',
    ]);
    $this->client->submit($form);

    // Vérifie la redirection vers la page suivante
    $this->assertResponseRedirects('/inscription/identifiant');
    $this->client->followRedirect();

    // Vérifie que la page de l'inscription des identifiants est accessible
    $this->assertResponseIsSuccessful();
  }

  public function testSignupUserCredentials(): void
  {
    $this->markTestSkipped('This test has not been implemented yet.');
    $session = $this->client->getContainer()->get('session');
    $session->set('signup', [
      'siren' => '123456789',
      'name' => 'Test Employer',
      'latitude' => '48.8566',
      'longitude' => '2.3522'
    ]);
    $session->set('signup_user', [
      'firstname' => 'John',
      'lastname' => 'Doe',
      'email' => 'john.doe@example.com',
      'phoneNumber' => '0123456789'
    ]);
    $session->save();

    $crawler = $client->request(Request::METHOD_GET, '/inscription/identifiant');

    // Vérifie que la page de l'inscription des identifiants est accessible
    $this->assertResponseIsSuccessful();

    // Soumet le formulaire avec des données valides
    $form = $crawler->selectButton('Submit')->form([
      'signup_credentials[password]' => 'password123',
      'signup_credentials[confirm_password]' => 'password123',
    ]);
    $client->submit($form);

    // Vérifie la redirection vers le tableau de bord
    $this->assertResponseRedirects('/dashboard');
    $client->followRedirect();

    // Vérifie que la page du tableau de bord est accessible
    $this->assertResponseIsSuccessful();
  }

}
