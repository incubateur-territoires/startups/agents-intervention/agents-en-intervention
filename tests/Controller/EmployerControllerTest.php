<?php

namespace App\Tests\Controller;

use App\Entity\User;
use App\Event\EmployerCreatedEvent;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Employer;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Zenstruck\Foundry\Test\Factories;
use Zenstruck\Foundry\Test\ResetDatabase;

final class EmployerControllerTest extends WebTestCase
{
  use ResetDatabase, Factories;

  private KernelBrowser $client;
  private EntityManagerInterface $manager;
  private EntityRepository $repository;
  private string $path = '/1/employer';
  private User $user;

  #[\Override]
  protected function setUp(): void
  {
    parent::setUp();
    $this->client = static::createClient();
    $this->manager = static::getContainer()->get('doctrine')->getManager();
    $this->repository = $this->manager->getRepository(Employer::class);

//    foreach ($this->repository->findAll() as $object) {
//      $this->manager->remove($object);
//    }
//
//    $this->manager->flush();
    $employer = Employer::new('anct', 'ANCT', 4.4, 6.6);
    $this->user = User::new('test', 'test', 'test', 'test', $employer, ['ROLE_ADMIN'], 'email@email.com', '0987654321', null, false);
    $this->manager->persist($employer);
    $this->manager->persist($this->user);
    $this->manager->flush();
  }

  public function testIndex(): void
  {

    $user = self::getContainer()->get('doctrine')->getRepository(User::class)->find(1);
//    dd(self::getContainer()->get('doctrine')->getRepository(User::class)->findAll());
    $this->client->loginUser($user);

    $this->client->followRedirects();
    $crawler = $this->client->request(Request::METHOD_GET, $this->path);

    self::assertResponseStatusCodeSame(200);
    self::assertPageTitleContains('Communes');

    // Use the $crawler to perform additional assertions e.g.
    // self::assertSame('Some text on the page', $crawler->filter('.p')->first());
  }

  public function testNew(): void
  {
    $this->client->loginUser($this->user);
    $this->client->request(Request::METHOD_GET, sprintf('%s/new', $this->path));

    self::assertResponseStatusCodeSame(200);

    $this->client->submitForm('Créer', [
      'employer_creation[siren]' => 'Testing',
      'employer_creation[name]' => 'Testing',
      'employer_creation[longitude]' => 42,
      'employer_creation[latitude]' => 42,
      'employer_creation[timezone]' => 'Europe/Paris',

      'employer_creation[user_firstname]' => 'Firstname',
      'employer_creation[user_lastname]' => 'Lastname',
      'employer_creation[user_login]' => 'login',
      'employer_creation[user_password]' => 'fake_password',
      'employer_creation[user_email]' => 'mon@email.fr',
      'employer_creation[user_phoneNumber]' => '0987654321',
    ]);

    self::assertResponseRedirects($this->path);

    self::assertSame(2, $this->repository->count([]));
    self::assertSame(2, $this->manager->getRepository(User::class)->count([]));

    // Doit dispatcher un événement EmployerCreatedEvent
  }

  public function testShow(): void
  {
    $fixture = Employer::new(
      '26574E7685',
      'My Title',
      42,
      4,
      'America/Guadeloupe'
    );

    $this->manager->persist($fixture);
    $this->manager->flush();

    $this->client->loginUser($this->user);
    $this->client->request(Request::METHOD_GET, sprintf('%s/%s', $this->path, $fixture->getId()));

    self::assertResponseStatusCodeSame(200);
    self::assertPageTitleContains('Commune de My Title');

    // Use assertions to check that the properties are properly displayed.
    self::assertAnySelectorTextContains('td', 'America/Guadeloupe');
  }

  public function testEdit(): void
  {
    $fixture = Employer::new(
      '26574E7685',
      'My Title',
      42,
      4,
      'America/Guadeloupe'
    );

    $this->manager->persist($fixture);
    $this->manager->flush();

    $this->client->loginUser($this->user);
    $this->client->request(Request::METHOD_GET, sprintf('%s/%s/edit', $this->path, $fixture->getId()));

    $this->client->submitForm('Mettre à jour', [
      'employer[siren]' => '12345678900001',
      'employer[name]' => 'Something New',
      'employer[longitude]' => 43,
      'employer[latitude]' => 5,
      'employer[timezone]' => 'Europe/Paris',
    ]);

    self::assertResponseRedirects('/1/employer');

    $fixture = $this->repository->findAll();

    self::assertSame('12345678900001', $fixture[1]->getSiren());
    self::assertSame('Something New', $fixture[1]->getName());
    self::assertSame(43.0, $fixture[1]->getLongitude());
    self::assertSame(5.0, $fixture[1]->getLatitude());
    self::assertSame('Europe/Paris', $fixture[1]->getTimezone());
  }

  public function testEditShowSiretDoitCommencerPar1ou2Erreur(): void
  {
    $fixture = Employer::new(
      '26574E7685',
      'My Title',
      42,
      4,
      'America/Guadeloupe'
    );

    $this->manager->persist($fixture);
    $this->manager->flush();

    $this->client->loginUser($this->user);
    $this->client->request(Request::METHOD_GET, sprintf('%s/%s/edit', $this->path, $fixture->getId()));

    $this->client->submitForm('Mettre à jour', [
      'employer[siren]' => 'Something New',
      'employer[name]' => 'Something New',
      'employer[longitude]' => 43,
      'employer[latitude]' => 5,
      'employer[timezone]' => 'Europe/Paris',
    ]);

//    self::assertRespo('/1/employer/edit');
    self::assertAnySelectorTextContains('[class="fr-error-text"]','Le SIRET doit commencer par 1 ou 2 (pour une collectivité territoriale)');

    $fixture = $this->repository->findAll();

    self::assertSame('26574E7685', $fixture[1]->getSiren());
    self::assertSame('My Title', $fixture[1]->getName());
    self::assertSame(42.0, $fixture[1]->getLongitude());
    self::assertSame(4.0, $fixture[1]->getLatitude());
    self::assertSame('America/Guadeloupe', $fixture[1]->getTimezone());
  }

  public function testRemove(): void
  {
    $fixture = Employer::new(
      '26574E7685',
      'My Title',
      42,
      4,
      'America/Guadeloupe'
    );

    $this->manager->persist($fixture);
    $this->manager->flush();

    $this->client->loginUser($this->user);
    $this->client->request(Request::METHOD_GET, sprintf('%s/%s', $this->path, $fixture->getId()));
    $this->client->submitForm('Supprimer');

    self::assertResponseRedirects('/1/employer');
    self::assertSame(1, $this->repository->count([]));
  }
}
