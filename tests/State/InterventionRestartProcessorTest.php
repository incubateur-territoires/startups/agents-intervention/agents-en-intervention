<?php

namespace App\Tests\State;

use App\Entity\Category;
use App\Entity\Intervention;
use App\Entity\Location;
use App\Entity\Priority;
use App\Entity\Type;
use App\Service\AuditTrailLogger;
use App\State\InterventionRestartProcessor;
use App\Tests\FixtureTrait;
use App\Workflow\TransitionGuesser;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Workflow\Transition;
use Symfony\Component\Workflow\WorkflowInterface;
use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\ProcessorInterface;
use Zenstruck\Foundry\Test\Factories;
use Zenstruck\Foundry\Test\ResetDatabase;

class InterventionRestartProcessorTest extends KernelTestCase
{
  use ResetDatabase, Factories, FixtureTrait;

  private EntityManagerInterface $entityManager;
  private ProcessorInterface $processor;
  private TransitionGuesser $transitionGuesser;
  private WorkflowInterface $workflow;
  private InterventionRestartProcessor $interventionRestartProcessor;

  protected function setUp(): void
  {
    $this->auditTrailLogger = $this->createMock(AuditTrailLogger::class);
    $this->entityManager = $this->createMock(EntityManagerInterface::class);
    $this->processor = $this->createMock(ProcessorInterface::class);
    $this->transitionGuesser = $this->createMock(TransitionGuesser::class);
    $this->workflow = $this->createMock(WorkflowInterface::class);

    $this->interventionRestartProcessor = new InterventionRestartProcessor(
      $this->auditTrailLogger,
      $this->entityManager,
      $this->processor,
      $this->transitionGuesser,
      $this->workflow
    );
  }

  public function testProcessWithInvalidData()
  {
    $this->expectException(\LogicException::class);
    $this->expectExceptionMessage('Data must be an instance of Intervention');

    $this->interventionRestartProcessor->process(new \stdClass(), $this->createMock(Operation::class));
  }

  public function testProcessWithNoTransitionFound()
  {
    $intervention = $this->createIntervention();
    $this->transitionGuesser
      ->method('guessTransitionFromSubject')
      ->willReturn(null);

    $this->expectException(\LogicException::class);
    $this->expectExceptionMessage('No transition found');

    $this->interventionRestartProcessor->process($intervention, $this->createMock(Operation::class));
  }

  public function testProcessWithValidData()
  {
    $intervention = $this->createIntervention();
    $transition = $this->createMock(Transition::class);
    $transition->method('getName')->willReturn('transition_name');

    $this->transitionGuesser
      ->method('guessTransitionFromSubject')
      ->willReturn($transition);

    $this->workflow
      ->expects($this->once())
      ->method('apply')
      ->with($intervention, 'transition_name');

    $this->entityManager
      ->expects($this->once())
      ->method('persist')
      ->with($intervention);

    $this->entityManager
      ->expects($this->once())
      ->method('flush');

    $this->processor
      ->method('process')
      ->willReturn($intervention);

    $result = $this->interventionRestartProcessor->process($intervention, $this->createMock(Operation::class));

    $this->assertSame($intervention, $result);
  }

  private function createIntervention(): Intervention
  {

    $category = new Category();
    $category->setName('category_name');
    $type = new Type();
    $type->setName('type_name');
    $type->setCategory($category);
    $location = new Location(
      'street',
      'city',
      'zip_code',
      'country',
      1.0,
      1.0
    );
    $intervention = new Intervention(
      'description',
      Priority::Normal,
      $type,
      $location
    );

    return $intervention;
  }
}
