<?php

namespace App\Tests\Validator;

use App\Event\ConnexionMode;
use App\Event\UserConnectedEvent;
use App\Validator\UserConnectedEventValidator;
use PHPUnit\Framework\Attributes\DataProvider;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class UserConnectedEventValidatorTest extends KernelTestCase
{
  private ValidatorInterface $validator;
  private UserConnectedEventValidator $eventValidator;

  protected function setUp(): void
  {
    self::bootKernel();

    // Récupération du service de validation depuis le conteneur
    $this->validator = static::getContainer()->get(ValidatorInterface::class);
    $this->eventValidator = new UserConnectedEventValidator($this->validator);
  }

  public function testValidEvent(): void
  {
    // Création d'un événement valide
    $eventData = [
      'user_id' => 42,
      'mode' => ConnexionMode::Form
    ];

    // Validation des données
    $result = $this->eventValidator->validate(
      $eventData
    );

    // Vérification qu'il n'y a pas d'erreurs de validation
    $this->assertTrue($result['valid']);
    $this->assertEquals($result['data'], $eventData);
  }

  #[DataProvider('invalidEventDataProvider')]
  public function testInvalidEvent(array $eventData, string $expectedViolationMessage): void
  {
    $result = $this->eventValidator->validate(
      $eventData
    );
//dd($result);
    // Vérification qu'il n'y a pas d'erreurs de validation
    $this->assertFalse($result['valid']);
//    $this->assertEquals($result['errors'], ["[user_id]" => "Cette valeur doit être strictement positive."]);
//    $this->assertCount(1, $violations);
//    $this->assertEquals($expectedViolationMessage, $violations[0]->getMessage());
  }

  public static function invalidEventDataProvider(): array
  {
    return [
      'user_id négatif' => [
        [
          'user_id' => -1,
          'mode' => ConnexionMode::Form
        ],
        'Cette valeur doit être strictement positive.'
      ],
      'user_id non numérique' => [
        [
          'user_id' => 'abc',
          'mode' => ConnexionMode::Form
        ],
        'Cette valeur doit être de type integer.'
      ],
      'mode invalide' => [
        [
          'user_id' => 42,
          'mode' => 'MODE_INVALIDE'
        ],
        sprintf('Cette valeur doit être de type %s.', ConnexionMode::class)
      ],
      'user_id manquant' => [
        [
          'mode' => ConnexionMode::Form
        ],
        'Cette valeur ne doit pas être vide.'
      ],
    ];
  }

  public function testGetEventType(): void
  {
    $this->assertSame(
      UserConnectedEvent::getType(),
      $this->eventValidator->getEventType()
    );
  }
}
