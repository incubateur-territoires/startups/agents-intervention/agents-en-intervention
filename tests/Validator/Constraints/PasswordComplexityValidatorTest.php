<?php

namespace App\Tests\Validator\Constraints;

use App\Validator\Constraints\PasswordComplexity;
use App\Validator\Constraints\PasswordComplexityValidator;
use Symfony\Component\Validator\Test\ConstraintValidatorTestCase;

class PasswordComplexityValidatorTest extends ConstraintValidatorTestCase
{
  protected function createValidator(): PasswordComplexityValidator
  {
    return new PasswordComplexityValidator();
  }

  public function testNullIsValid(): void
  {
    $this->validator->validate(null, new PasswordComplexity());

    $this->assertNoViolation();
  }

  public function testValidPassword(): void
  {
    $this->validator->validate('Valid1Password!', new PasswordComplexity());

    $this->assertNoViolation();
  }

  public function testInvalidPassword(): void
  {
    $constraint = new PasswordComplexity();
    $this->validator->validate('invalid', $constraint);

    $this->buildViolation($constraint->message)
      ->assertRaised();
  }

  public function testPasswordWithoutUppercase(): void
  {
    $constraint = new PasswordComplexity();
    $this->validator->validate('valid1password!', $constraint);

    $this->buildViolation($constraint->message)
      ->assertRaised();
  }

  public function testPasswordWithoutLowercase(): void
  {
    $constraint = new PasswordComplexity();
    $this->validator->validate('VALID1PASSWORD!', $constraint);

    $this->buildViolation($constraint->message)
      ->assertRaised();
  }

  public function testPasswordWithoutDigit(): void
  {
    $constraint = new PasswordComplexity();
    $this->validator->validate('ValidPassword!', $constraint);

    $this->buildViolation($constraint->message)
      ->assertRaised();
  }

  public function testPasswordWithoutSpecialCharacter(): void
  {
    $constraint = new PasswordComplexity();
    $this->validator->validate('Valid1Password', $constraint);

    $this->buildViolation($constraint->message)
      ->assertRaised();
  }
}
