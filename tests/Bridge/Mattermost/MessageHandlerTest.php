<?php

namespace App\Tests\Bridge\Mattermost;

use App\Bridge\Mattermost\Message;
use App\Bridge\Mattermost\MessageHandler;
use PHPUnit\Framework\TestCase;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

class MessageHandlerTest extends TestCase
{
  public function testInvokeWithValidWebhookUrl(): void
  {
    $httpClient = $this->createMock(HttpClientInterface::class);
    $response = $this->createMock(ResponseInterface::class);

    $httpClient->expects($this->once())
      ->method('request')
      ->with(
        'POST',
        'http://example.com/webhook',
        [
          'headers' => ['Content-Type' => 'application/json'],
          'body' => json_encode(['text' => 'Test message', 'channel' => 'test-channel']),
        ]
      )
      ->willReturn($response);

    $messageHandler = new MessageHandler(
      $httpClient,
      'http://example.com/webhook',
      'test-channel'
    );

    $message = new Message('Test message');
    $messageHandler($message);
  }

  public function testInvokeWithoutWebhookUrl(): void
  {
    $httpClient = $this->createMock(HttpClientInterface::class);

    $httpClient->expects($this->never())
      ->method('request');

    $messageHandler = new MessageHandler($httpClient, null, 'test-channel');

    $message = new Message('Test message');
    $messageHandler($message);
  }
}
