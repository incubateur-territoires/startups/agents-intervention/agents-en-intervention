<?php

namespace App\Tests\Api;

use App\Entity\Employer;
use App\Entity\Role;

class PermissionCheck
{
  public function __construct(
    public Employer $employer,
    public Role     $role,
    public int      $statusCode,
    public string   $url,
    public ?array   $data = null
  )
  {
  }
}
