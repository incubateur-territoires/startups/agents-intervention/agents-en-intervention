<?php

declare(strict_types=1);

namespace App\Tests\Api\Locations;

use App\Encoder\MultipartDecoder;
use App\Entity\Category;
use App\Entity\Employer;
use App\Entity\Intervention;
use App\Entity\Location;
use App\Entity\Role;
use App\Entity\Type;
use App\Entity\User;
use App\Logger\UserProcessor;
use App\Repository\UserRepository;
use App\Security\DeleteInterventionVoter;
use App\Security\EmployerVoter;
use App\Security\InterventionVoter;
use App\Security\UserVoter;
use App\Tests\Api\AuthenticationTestCase;
use App\Tests\FixtureTrait;
use PHPUnit\Framework\Attributes as PA;
use Zenstruck\Foundry\Test\Factories;
use Zenstruck\Foundry\Test\ResetDatabase;

/**
 * Test GET /api/locations.
 */
#[
  PA\CoversClass(Location::class),
  PA\UsesClass(Employer::class),
  PA\UsesClass(Role::class),
  PA\UsesClass(User::class),
  PA\UsesClass(UserRepository::class),
  PA\UsesClass(MultipartDecoder::class),
  PA\UsesClass(Category::class),
  PA\UsesClass(Intervention::class),
  PA\UsesClass(Type::class),
  PA\UsesClass(UserProcessor::class),
  PA\UsesClass(DeleteInterventionVoter::class),
  PA\UsesClass(EmployerVoter::class),
  PA\UsesClass(InterventionVoter::class),
  PA\UsesClass(UserVoter::class),
  PA\Group('api'),
  PA\Group('api_locations'),
  PA\Group('api_locations_get_collection'),
  PA\Group('location')
]
final class LocationGetCollectionTest extends AuthenticationTestCase
{
  use ResetDatabase, Factories, FixtureTrait;

    /**
     * Test que la route nécessite d'être authentifié.
     */
    public function testNeedsAuthentication(): void
    {
        $client = static::createClient();

        $apiResponse = $client->request('GET', '/api/locations');

        self::assertSame(401, $apiResponse->getStatusCode(), 'GET "/api/locations" succeeded.');
    }

    /**
     * Test qu'une collection de localisations puissent être renvoyée.
     */
    public function testCanGetALocationCollection(): void
    {
        $client = static::createClient();

        $apiResponse = $client->request('GET', '/api/locations', ['auth_bearer' => $this->getJWT()]);

        self::assertSame(200, $apiResponse->getStatusCode(), 'GET "/api/locations" failed.');
        self::assertJson($apiResponse->getContent());

        $collection = json_decode($apiResponse->getContent(), false);

        $hydraMember = 'hydra:member';
        self::assertIsArray($collection->$hydraMember);
        $locations = $collection->$hydraMember;
        self::assertGreaterThanOrEqual(30, $locations);

        foreach ($locations as $location) {
            $this->assertLocationIsComplete($location);
        }
    }

    /**
     * Les assertions de la localisation.
     * @param object $location la localisation.
     */
    private function assertLocationIsComplete(object $location): void
    {
        self::assertGreaterThanOrEqual(5, count((array) $location), 'Incorrect count of location data has been returned.');

        self::assertIsInt($location->id);
        self::assertIsFloat($location->longitude);
        self::assertIsFloat($location->latitude);

        $atId = '@id';
        self::assertSame('/api/locations/' . $location->id, $location->$atId);
        $atType = '@type';
        self::assertSame('Location', $location->$atType);
    }
}
