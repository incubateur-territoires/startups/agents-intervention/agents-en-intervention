<?php

namespace App\Tests\Api\Users;

use App\Entity\Role;
use App\Tests\Api\PermissionCheck;
use App\Tests\Api\PermissionsTestCase;

class UserDeleteTest extends PermissionsTestCase
{
  #[\Override]
  protected function getMethod(): string
  {
    return 'DELETE';
  }

  public function testPermissions(): void {

    $this->checkPermissions([
      new PermissionCheck(self::$employer_anct, Role::Admin, 204, '/api/users/' . static::$user->getId()),
      new PermissionCheck(self::$employer_anct, Role::Admin, 204, '/api/users/' . static::$user2->getId()),
      new PermissionCheck(self::$employer_1, Role::AdminEmployer, 204, '/api/users/' . static::$user3->getId()),
      new PermissionCheck(self::$employer_1, Role::Director, 403, '/api/users/' . static::$user4->getId()),
      new PermissionCheck(self::$employer_1, Role::Agent, 403, '/api/users/' . static::$user4->getId()),
      new PermissionCheck(self::$employer_1, Role::Elected, 403, '/api/users/' . static::$user4->getId()),
    ]);
  }

}
