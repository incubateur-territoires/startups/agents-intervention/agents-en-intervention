<?php

declare(strict_types=1);

namespace App\Tests\Api\Users;

use App\Encoder\MultipartDecoder;
use App\Entity\Employer;
use App\Entity\Role;
use App\Entity\User;
use App\Logger\UserProcessor;
use App\Repository\UserRepository;
use App\Security\DeleteInterventionVoter;
use App\Security\EmployerVoter;
use App\Security\InterventionVoter;
use App\Security\UserVoter;
use App\State\UserProvider;
use App\Tests\Api\AuthenticationTestCase;
use PHPUnit\Framework\Attributes as PA;
use Zenstruck\Foundry\Test\ResetDatabase;

/**
 * Test GET /api/users/{id}.
 */
#[
  PA\CoversClass(User::class),
  PA\UsesClass(Employer::class),
  PA\UsesClass(Role::class),
  PA\UsesClass(UserRepository::class),
  PA\UsesClass(MultipartDecoder::class),
  PA\UsesClass(UserProcessor::class),
  PA\UsesClass(DeleteInterventionVoter::class),
  PA\UsesClass(EmployerVoter::class),
  PA\UsesClass(InterventionVoter::class),
  PA\UsesClass(UserVoter::class),
  PA\UsesClass(UserProvider::class),
  PA\Group('api'),
  PA\Group('api_users'),
  PA\Group('api_users_get'),
  PA\Group('user')
]
final class UserGetTest extends AuthenticationTestCase
{
  use ResetDatabase;

  /**
   * Test que la route nécessite d'être authentifié.
   */
  public function testNeedsAuthentication(): void
  {
    $client = static::createClient();

    $apiResponse = $client->request('GET', '/api/users/1');

    self::assertSame(401, $apiResponse->getStatusCode(), 'GET "/api/users/1" succeeded.');
  }

  /**
   * Ajoute un employeur.
   * @return Employer l'employeur.
   */
  private function getEmployer(): Employer
  {
    return Employer::new('employer-siren', 'employer-name', 45, 54);
  }

  /**
   * Ajoute un utilisateur.
   * @param Employer $employer l'employeur.
   * @return User l'utilisateur.
   */
  private function getUser(Employer $employer): User
  {
    return User::new(
      'user-login',
      'user-password',
      'user-firstname',
      'user-lastname',
      $employer,
      [Role::Director],
      'user-email',
      'user-phoneNumber',
      null,
      true,
      connectedAt: new \DateTimeImmutable('2023-01-02T00:00:00+00:00')
    );
  }

  /**
   * Test qu'un utilisateur puisse être renvoyé.
   */
  public function testCanGetAUser(): void
  {
    $client = static::createClient();

    $entityManager = static::$kernel
      ->getContainer()
      ->get('doctrine')
      ->getManager();
    $employer = $this->getEmployer();
    $newUser = $this->getUser($employer);
    $entityManager->persist($newUser);
    $entityManager->flush();

    $apiResponse = $client->request('GET', '/api/users/'.$newUser->getId(), ['auth_bearer' => $this->getJWT(employer: $employer)]);

    self::assertSame(200, $apiResponse->getStatusCode(), 'GET "/api/users/1" failed.');
    self::assertJson($apiResponse->getContent());

    $user = json_decode($apiResponse->getContent(), false);

    self::assertCount(17, (array)$user, 'Too much data has been returned.');

    self::assertSame($newUser->getId(), $user->id);
    self::assertSame('user-login', $user->login);
    self::assertObjectNotHasProperty('password', $user);
    self::assertSame('user-firstname', $user->firstname);
    self::assertSame('user-lastname', $user->lastname);
    self::assertSame('user-email', $user->email);
    self::assertSame('user-phoneNumber', $user->phoneNumber);
    self::assertSame('/api/employers/'.$employer->getId(), $user->employer);
    self::assertTrue($user->active);

    $atId = '@id';
    self::assertSame('/api/users/'.$newUser->getId(), $user->$atId);
    $atType = '@type';
    self::assertSame('User', $user->$atType);
  }

  /**
   * Test qu'un utilisateur sans courriel puisse être renvoyé.
   */
  public function testCanGetAUserWithoutEmail(): void
  {
    $client = static::createClient();

    $entityManager = static::$kernel
      ->getContainer()
      ->get('doctrine')
      ->getManager();
    $employer = $this->getEmployer();
    $entityManager->persist($employer);
    $newUser= $this->getUser($employer);
    $entityManager->persist($newUser);
    $entityManager->flush();

    $apiResponse = $client->request('GET', '/api/users/'.$newUser->getId(), ['auth_bearer' => $this->getJWT(employer: $employer)]);

    self::assertSame(200, $apiResponse->getStatusCode(), 'GET "/api/users/1" failed.');
    self::assertJson($apiResponse->getContent());

    $user = json_decode($apiResponse->getContent(), false);

    self::assertCount(17, (array)$user, 'Too much data has been returned.');

    self::assertSame($newUser->getId(), $user->id);
    self::assertSame('user-login', $user->login);
    self::assertSame('user-firstname', $user->firstname);
    self::assertSame('user-lastname', $user->lastname);
    self::assertSame('user-phoneNumber', $user->phoneNumber);
    self::assertSame('/api/employers/'.$employer->getId(), $user->employer);
    self::assertTrue($user->active);

    $atId = '@id';
    self::assertSame('/api/users/'.$newUser->getId(), $user->$atId);
    $atType = '@type';
    self::assertSame('User', $user->$atType);
  }

  /**
   * Test qu'un utilisateur sans numéro de téléphone puisse être renvoyé.
   */
  public function testCanGetAUserWithoutPhoneNumber(): void
  {
    $client = static::createClient();


    $entityManager = static::$kernel
      ->getContainer()
      ->get('doctrine')
      ->getManager();
    $employer = $this->getEmployer();
    $newUser = $this->getUser($employer);
    $newUser->setPhoneNumber(null);
    $entityManager->persist($newUser);
    $entityManager->flush();

    $apiResponse = $client->request('GET', '/api/users/'.$newUser->getId(), ['auth_bearer' => $this->getJWT(employer: $employer)]);

    self::assertSame(200, $apiResponse->getStatusCode(), 'GET "/api/users/1" failed.');
    self::assertJson($apiResponse->getContent());

    $user = json_decode($apiResponse->getContent(), false);

    self::assertCount(17, (array)$user, 'Too much data has been returned.');

    self::assertSame($newUser->getId(), $user->id);
    self::assertSame('user-login', $user->login);
    self::assertSame('user-firstname', $user->firstname);
    self::assertSame('user-lastname', $user->lastname);
    self::assertSame('user-email', $user->email);
    self::assertSame('/api/employers/'.$employer->getId(), $user->employer);
    self::assertTrue($user->active);

    $atId = '@id';
    self::assertSame('/api/users/'.$newUser->getId(), $user->$atId);
    $atType = '@type';
    self::assertSame('User', $user->$atType);
  }

  /**
   * Test qu'un utilisateur sans photo puisse être renvoyé.
   */
  public function testCanGetAUserWithoutPicture(): void
  {
    $client = static::createClient();

    $entityManager = static::$kernel
      ->getContainer()
      ->get('doctrine')
      ->getManager();
    $employer = $this->getEmployer();
    $newUser = $this->getUser($employer);
    $newUser->setPicture(null);
    $entityManager->persist($newUser);
    $entityManager->flush();

    $apiResponse = $client->request('GET', '/api/users/'.$newUser->getId(), ['auth_bearer' => $this->getJWT(employer: $employer)]);

    self::assertSame(200, $apiResponse->getStatusCode(), 'GET "/api/users/1" failed.');
    self::assertJson($apiResponse->getContent());

    $user = json_decode($apiResponse->getContent(), false);

    self::assertCount(17, (array)$user, 'Too much data has been returned.');

    self::assertSame($newUser->getId(), $user->id);
    self::assertSame('user-login', $user->login);
    self::assertSame('user-firstname', $user->firstname);
    self::assertSame('user-lastname', $user->lastname);
    self::assertSame('user-email', $user->email);
    self::assertSame('user-phoneNumber', $user->phoneNumber);
    self::assertSame('/api/employers/'.$employer->getId(), $user->employer);
    self::assertTrue($user->active);

    $atId = '@id';
    self::assertSame('/api/users/'.$newUser->getId(), $user->$atId);
    $atType = '@type';
    self::assertSame('User', $user->$atType);
  }

  /**
   * Test qu'un utilisateur sans date de connexion
   * puisse être renvoyé.
   */
  public function testCanGetAUserWithoutConnectedAt(): void
  {
    $client = static::createClient();


    $entityManager = static::$kernel
      ->getContainer()
      ->get('doctrine')
      ->getManager();
    $employer = $this->getEmployer();
    $newUser = $this->getUser($employer);
    $newUser->setConnectedAt(null);
    $entityManager->persist($newUser);
    $entityManager->flush();

    $apiResponse = $client->request('GET', '/api/users/'.$newUser->getId(), ['auth_bearer' => $this->getJWT(employer: $employer)]);

    self::assertSame(200, $apiResponse->getStatusCode(), 'GET "/api/users/1" failed.');
    self::assertJson($apiResponse->getContent());

    $user = json_decode($apiResponse->getContent(), false);

    self::assertCount(17, (array)$user, 'Too much data has been returned.');

    self::assertSame($newUser->getId(), $user->id);
    self::assertSame('user-login', $user->login);
    self::assertSame('user-firstname', $user->firstname);
    self::assertSame('user-lastname', $user->lastname);
    self::assertSame('user-email', $user->email);
    self::assertSame('user-phoneNumber', $user->phoneNumber);
    self::assertSame('/api/employers/'.$employer->getId(), $user->employer);
    self::assertTrue($user->active);

    $atId = '@id';
    self::assertSame('/api/users/'.$newUser->getId(), $user->$atId);
    $atType = '@type';
    self::assertSame('User', $user->$atType);
  }

  /**
   * Test qu'un utilisateur inactif puisse être renvoyé.
   */
  public function testCanGetAnInactiveUser(): void
  {
    $client = static::createClient();

    $entityManager = static::$kernel
      ->getContainer()
      ->get('doctrine')
      ->getManager();
    $employer = $this->getEmployer();
    $newUser = $this->getUser($employer);
    $newUser->setActive(false);
    $entityManager->persist($employer);
    $entityManager->persist($newUser);
    $entityManager->flush();

    $apiResponse = $client->request('GET', '/api/users/'.$newUser->getId(), ['auth_bearer' => $this->getJWT(employer: $employer)]);
    self::assertSame(200, $apiResponse->getStatusCode(), 'GET "/api/users/1" failed.');
    self::assertJson($apiResponse->getContent());

    $user = json_decode($apiResponse->getContent(), false);

    self::assertCount(17, (array)$user, 'Too much data has been returned.');

    self::assertSame($newUser->getId(), $user->id);
    self::assertSame('user-login', $user->login);
    self::assertSame('user-firstname', $user->firstname);
    self::assertSame('user-lastname', $user->lastname);
    self::assertSame('user-email', $user->email);
    self::assertSame('user-phoneNumber', $user->phoneNumber);
    self::assertSame('/api/employers/'.$employer->getId(), $user->employer);
    self::assertFalse($user->active);

    $atId = '@id';
    self::assertSame('/api/users/'.$newUser->getId(), $user->$atId);
    $atType = '@type';
    self::assertSame('User', $user->$atType);
  }
}
