<?php

namespace App\Tests\Api\Users;

use App\Entity\Picture;
use App\Entity\Role;
use App\Entity\User;
use App\Repository\UserRepository;
use App\Tests\Api\PermissionCheck;
use App\Tests\Api\PermissionsTestCase;

class UserPatchTest extends PermissionsTestCase
{
  #[\Override]
  protected function getMethod(): string
  {
    return 'PATCH';
  }

  public function testPermissionPatchUserAsAdminForAnct()
  {
    $p = new PermissionCheck(
      self::$employer_anct,
      Role::Admin,
      200,
      '/api/users-patch/' . static::$user->getId(),
      [
        'login' => 'l0g1n',
        'email' => 'dummy@anct',
        'firstname' => 'Michel',
        'lastname' => 'Dupont',
        'phoneNumber' => '0987654321',
        'employer' => '/api/employers/' . self::$employer_anct->getId(),
        'roles' => [Role::Admin]
      ]
    );

    $apiResponse = $this->checkPermissionObject($p);

    $content = $apiResponse->getContent();
    self::assertJson($content);

    $jsonResponse = json_decode($apiResponse->getContent(), false);

    self::assertCount(14, (array)$jsonResponse);
    self::assertSame(1, $jsonResponse->id);
    self::assertSame('Michel', $jsonResponse->firstname);
    self::assertSame('Dupont', $jsonResponse->lastname);
    self::assertSame('0987654321', $jsonResponse->phoneNumber);
    self::assertSame('dummy@anct', $jsonResponse->email);
    self::assertSame(1, $jsonResponse->employer->id);
  }

  public function testPatchUserAsAdminForAnct_Partial()
  {
    $p = new PermissionCheck(
      self::$employer_anct,
      Role::Admin,
      200,
      '/api/users-patch/' . static::$user->getId()
    );

    $apiResponse = $this->checkPermissionObject($p, [
      'email' => 'partial@anct',
    ]);

    $content = $apiResponse->getContent();

    self::assertJson($content);

    $jsonResponse = json_decode($apiResponse->getContent(), false);

    self::assertCount(14, (array)$jsonResponse);

    self::assertSame(1, $jsonResponse->id);
    self::assertSame('test', $jsonResponse->firstname);
    self::assertSame('test', $jsonResponse->lastname);
    self::assertSame('0987654321', $jsonResponse->phoneNumber);
    self::assertSame('partial@anct', $jsonResponse->email);
    self::assertSame(1, $jsonResponse->employer->id);
  }

  public function testPermissions()
  {

    $apiResponses = $this->checkPermissions([
      new PermissionCheck(
        static::$employer_1,
        Role::AdminEmployer,
        200,
        '/api/users-patch/' . static::$user4->getId(),
        [
          'email' => 'validate@anct',
        ]),
      new PermissionCheck(
        static::$employer_1,
        Role::AdminEmployer,
        403,
        '/api/users-patch/' . static::$user2->getId(),
        [
          'email' => 'validate@anct',
        ]),
      new PermissionCheck(
        static::$employer_2,
        Role::AdminEmployer,
        200,
        '/api/users-patch/' . static::$user2->getId(),
        [
          'email' => 'validate@anct',
        ]),
      new PermissionCheck(
        static::$employer_2,
        Role::AdminEmployer,
        403,
        '/api/users-patch/' . static::$user4->getId(),
        [
          'email' => 'validate@anct',
        ]),
      new PermissionCheck(
        static::$employer_1,
        Role::Agent,
        403,
        '/api/users-patch/' . static::$user4->getId(),
        [
          'email' => 'validate@anct',
        ]),
      new PermissionCheck(
        static::$employer_1,
        Role::Elected,
        403,
        '/api/users-patch/' . static::$user4->getId(),
        [
          'email' => 'validate@anct',
        ]),
    ]);

    $content = $apiResponses[0]->getContent();
    self::assertJson($content);

    $jsonResponse = json_decode($apiResponses[0]->getContent(), false);
    self::assertSame('validate@anct', $jsonResponse->email);
  }

  public function testPatchUserAddPicture()
  {
    $doctrine = static::getContainer()->get('doctrine');
    $em = $doctrine->getManager();

    $pictureId = 1; // Assuming a picture with ID 1 exists for testing
    $p = new PermissionCheck(
      self::$employer_anct,
      Role::Admin,
      200,
      '/api/users-patch/' . static::$user->getId(),
      [
        'picture' => '/api/pictures/' . $pictureId,
      ]
    );

    $apiResponse = $this->checkPermissionObject($p);

    $content = $apiResponse->getContent();
    self::assertJson($content);

    $jsonResponse = json_decode($apiResponse->getContent(), false);
    self::assertSame($pictureId, $jsonResponse->picture->id);
  }

  public function testPatchUserUpdatePicture()
  {
    $newPictureId = 1; // Assuming a new picture with ID 2 exists for testing
    $p = new PermissionCheck(
      self::$employer_anct,
      Role::Admin,
      200,
      '/api/users-patch/' . static::$userWithPicture->getId(),
      [
        'picture' => '/api/pictures/' . $newPictureId,
      ]
    );

    $apiResponse = $this->checkPermissionObject($p);

    $content = $apiResponse->getContent();
    self::assertJson($content);

    $jsonResponse = json_decode($apiResponse->getContent(), false);
    self::assertSame($newPictureId, $jsonResponse->picture->id);
  }

  public function testPatchUserRemovePicture()
  {
    $p = new PermissionCheck(
      self::$employer_anct,
      Role::Admin,
      200,
      '/api/users-patch/' . static::$userWithPicture->getId(),
      [
        'picture' => null,
      ]
    );

    $apiResponse = $this->checkPermissionObject($p);

    $content = $apiResponse->getContent();
    self::assertJson($content);

    $jsonResponse = json_decode($apiResponse->getContent(), false);
    self::assertNull($jsonResponse->picture);
  }

  public function testPatchUserModifyButPassword()
  {
    $p = new PermissionCheck(
      self::$employer_anct,
      Role::Admin,
      200,
      '/api/users-patch/' . static::$userWithPicture->getId(),
      [
        'picture' => '/api/pictures/1',
        'password' => '',
        'password_confirmation' => '',
      ]
    );

    $apiResponse = $this->checkPermissionObject($p);

    $content = $apiResponse->getContent();
    self::assertJson($content);

    $jsonResponse = json_decode($apiResponse->getContent(), false);
    self::assertNotNull($jsonResponse->picture);

    $container = static::getContainer();
    $em = $container->get('doctrine.orm.entity_manager');
    $userRepository = $em->getRepository(User::class);
    $user = $userRepository->find(static::$userWithPicture->getId());

    // Le mot de passe n'est pas modifié
    self::assertEquals($user->getPassword(), 'test');
  }
}
