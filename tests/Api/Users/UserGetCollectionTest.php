<?php

declare(strict_types=1);

namespace App\tests\Api\Users;

use App\Encoder\MultipartDecoder;
use App\Entity\Employer;
use App\Entity\Role;
use App\Entity\User;
use App\Logger\UserProcessor;
use App\Repository\UserRepository;
use App\Security\DeleteInterventionVoter;
use App\Security\EmployerVoter;
use App\Security\InterventionVoter;
use App\Security\UserVoter;
use App\State\UserProvider;
use App\Tests\Api\PermissionCheck;
use App\Tests\Api\PermissionsTestCase;
use PHPUnit\Framework\Attributes as PA;

/**
 * Test GET /users/{id}.
 */
#[
  PA\CoversClass(User::class),
  PA\UsesClass(Employer::class),
  PA\UsesClass(Role::class),
  PA\UsesClass(UserRepository::class),
  PA\UsesClass(MultipartDecoder::class),
  PA\UsesClass(UserProcessor::class),
  PA\UsesClass(DeleteInterventionVoter::class),
  PA\UsesClass(EmployerVoter::class),
  PA\UsesClass(InterventionVoter::class),
  PA\UsesClass(UserVoter::class),
  PA\UsesClass(UserProvider::class),
  PA\Group('api'),
  PA\Group('api_users'),
  PA\Group('api_users_get'),
  PA\Group('user')
]
final class UserGetCollectionTest extends PermissionsTestCase
{
  #[\Override]
  protected function getMethod(): string
  {
    return 'GET';
  }

  public function testNeedsAuthentication(): void
  {
    $client = static::createClient();
    $apiResponse = $client->request('GET', '/api/users');
    self::assertSame(401, $apiResponse->getStatusCode(), 'GET "/api/users" succeeded.');
  }

  public function testPermissionsOnUsersList(): void {
    $this->checkPermissions([
      new PermissionCheck(
        self::$employer_anct,
        Role::Admin,
        200,
        '/api/users'
      ),
      new PermissionCheck(
        self::$employer_1,
        Role::AdminEmployer,
        403,
        '/api/users'
      ),
      new PermissionCheck(
        self::$employer_1,
        Role::Director,
        403,
        '/api/users'
      ),
      new PermissionCheck(
        self::$employer_1,
        Role::Agent,
        403,
        '/api/users'
      ),
      new PermissionCheck(
        self::$employer_1,
        Role::Elected,
        403,
        '/api/users'
      ),
    ]);
  }

  public function testPermissionsOnEmployer1UsersList(): void {
    $this->checkPermissions([
      new PermissionCheck(
        self::$employer_anct,
        Role::Admin,
        200,
        '/api/employers/'.self::$employer_1->getId().'/users'
      ),
      new PermissionCheck(
        self::$employer_1,
        Role::AdminEmployer,
        200,
        '/api/employers/'.self::$employer_1->getId().'/users'
      ),
      new PermissionCheck(
        self::$employer_2,
        Role::AdminEmployer,
        403,
        '/api/employers/'.self::$employer_1->getId().'/users'
      ),
      new PermissionCheck(
        self::$employer_1,
        Role::Director,
        200,
        '/api/employers/'.self::$employer_1->getId().'/users'
      ),
      new PermissionCheck(
        self::$employer_1,
        Role::Agent,
        200,
        '/api/employers/'.self::$employer_1->getId().'/users'
      ),
      new PermissionCheck(
        self::$employer_1,
        Role::Elected,
        200,
        '/api/employers/'.self::$employer_1->getId().'/users'
      ),
    ]);
  }

  public function testPermissionsOnEmployer1UsersActivesList(): void {
    $this->checkPermissions([
      new PermissionCheck(
        self::$employer_anct,
        Role::Admin,
        200,
        '/api/employers/'.self::$employer_1->getId().'/active-agents'
      ),
      new PermissionCheck(
        self::$employer_1,
        Role::AdminEmployer,
        200,
        '/api/employers/'.self::$employer_1->getId().'/active-agents'
      ),
      new PermissionCheck(
        self::$employer_2,
        Role::AdminEmployer,
        403,
        '/api/employers/'.self::$employer_1->getId().'/active-agents'
      ),
      new PermissionCheck(
        self::$employer_2,
        Role::Director,
        403,
        '/api/employers/'.self::$employer_1->getId().'/active-agents'
      ),
      new PermissionCheck(
        self::$employer_1,
        Role::Director,
        200,
        '/api/employers/'.self::$employer_1->getId().'/active-agents'
      ),
      new PermissionCheck(
        self::$employer_1,
        Role::Agent,
        200,
        '/api/employers/'.self::$employer_1->getId().'/active-agents'
      ),
      new PermissionCheck(
        self::$employer_1,
        Role::Elected,
        200,
        '/api/employers/'.self::$employer_1->getId().'/active-agents'
      ),
    ]);
  }

  public function testContent(): void {
    $apiResponse = $this->checkPermissionObject(new PermissionCheck(
      self::$employer_anct,
      Role::Admin,
      200,
      '/api/users'
    ));

    $jsonResponse = json_decode($apiResponse->getContent(), false);
    self::assertCount(9, $jsonResponse->{'hydra:member'});

    $user = $jsonResponse->{'hydra:member'}[0];
    self::assertIsObject($user);
    $compare = new \stdClass();
    $compare->{'@id'} = '/api/users/1';
    $compare->{'@type'} = 'User';
    $compare->id = 1;
    $compare->login = 'test';
    $compare->firstname = 'test';
    $compare->lastname = 'test';
    $compare->email = 'email@email.com';
    $compare->phoneNumber = '0987654321';
    $compare->createdAt = '2024-08-08T10:10:20+00:00';
    $compare->picture = null;
    $compare->employer = self::$employer_1;
    $compare->active = false;
    $compare->connectedAt = null;
    $compare->roles = ['ROLE_ADMIN'];
    $compare->role = '';

    self::assertEquals($user->id, $compare->id);
    self::assertEquals($user->login, $compare->login);
    self::assertEquals($user->firstname, $compare->firstname);
    self::assertEquals($user->lastname, $compare->lastname);
    self::assertEquals($user->email, $compare->email);
    self::assertEquals($user->phoneNumber, $compare->phoneNumber);
    self::assertEquals($user->active, $compare->active);
    self::assertEquals($user->roles, $compare->roles);
  }

  public function testActivesEmployer1Content(): void {
    $apiResponse = $this->checkPermissionObject(new PermissionCheck(
      self::$employer_1,
      Role::Director,
      200,
      '/api/employers/' . self::$employer_1->getId() . '/active-agents'
    ));

    $jsonResponse = json_decode($apiResponse->getContent(), false);
    self::assertCount(1, $jsonResponse->{'hydra:member'});

    $user = $jsonResponse->{'hydra:member'}[0];
    self::assertIsObject($user);
    $compare = new \stdClass();
    $compare->{'@id'} = '/api/users/1';
    $compare->{'@type'} = 'User';
    $compare->id = 9;
//    $compare->login = 'test';
    $compare->firstname = 'firstname';
    $compare->lastname = 'lastname';
    $compare->email = 'email@yopmail.com';
//    $compare->email = null;
    $compare->phoneNumber = '0987654321';
//    $compare->phoneNumber = null;
    $compare->createdAt = '2024-08-08T10:10:20+00:00';
    $compare->picture = null;
    $compare->employer = self::$employer_1;
    $compare->active = true;
    $compare->connectedAt = null;
    $compare->roles = ['ROLE_DIRECTOR'];
    $compare->role = '';

    self::assertEquals($user->id, $compare->id);
//    self::assertEquals($user->login, $compare->login);
    self::assertEquals($user->firstname, $compare->firstname);
    self::assertEquals($user->lastname, $compare->lastname);
    self::assertEquals($user->email, $compare->email);
    self::assertEquals($user->phoneNumber, $compare->phoneNumber);
    self::assertEquals($user->active, $compare->active);
    self::assertEquals($user->roles, $compare->roles);
  }

  /**
   * Test qu'un utilisateur puisse être renvoyé.
   */
//  public function testCanGetAUser(): void
//  {
//    $client = static::createClient();
//
//    $entityManager = static::$kernel
//      ->getContainer()
//      ->get('doctrine')
//      ->getManager();
//    $employer = $this->getEmployer();
//    $newUser = $this->getUser($employer);
//    $entityManager->persist($newUser);
//    $entityManager->flush();
//
//    $apiResponse = $client->request('GET', '/users/'.$newUser->getId(), ['auth_bearer' => $this->getJWT(employer: $employer)]);
//
//    self::assertSame(200, $apiResponse->getStatusCode(), 'GET "/users/1" failed.');
//    self::assertJson($apiResponse->getContent());
//
//    $user = json_decode($apiResponse->getContent(), false);
//
//    self::assertCount(16, (array)$user, 'Too much data has been returned.');
//
//    self::assertSame($newUser->getId(), $user->id);
//    self::assertSame('user-login', $user->login);
//    self::assertObjectNotHasProperty('password', $user);
//    self::assertSame('user-firstname', $user->firstname);
//    self::assertSame('user-lastname', $user->lastname);
//    self::assertSame('user-email', $user->email);
//    self::assertSame('user-phoneNumber', $user->phoneNumber);
//    self::assertSame('/api/employers/'.$employer->getId(), $user->employer);
//    self::assertTrue($user->active);
//
//    $atId = '@id';
//    self::assertSame('/users/'.$newUser->getId(), $user->$atId);
//    $atType = '@type';
//    self::assertSame('User', $user->$atType);
//  }
//
//  /**
//   * Test qu'un utilisateur sans courriel puisse être renvoyé.
//   */
//  public function testCanGetAUserWithoutEmail(): void
//  {
//    $client = static::createClient();
//
//    $entityManager = static::$kernel
//      ->getContainer()
//      ->get('doctrine')
//      ->getManager();
//    $employer = $this->getEmployer();
//    $entityManager->persist($employer);
//    $newUser= $this->getUser($employer);
//    $entityManager->persist($newUser);
//    $entityManager->flush();
//
//    $apiResponse = $client->request('GET', '/users/'.$newUser->getId(), ['auth_bearer' => $this->getJWT(employer: $employer)]);
//
//    self::assertSame(200, $apiResponse->getStatusCode(), 'GET "/users/1" failed.');
//    self::assertJson($apiResponse->getContent());
//
//    $user = json_decode($apiResponse->getContent(), false);
//
//    self::assertCount(16, (array)$user, 'Too much data has been returned.');
//
//    self::assertSame($newUser->getId(), $user->id);
//    self::assertSame('user-login', $user->login);
//    self::assertSame('user-firstname', $user->firstname);
//    self::assertSame('user-lastname', $user->lastname);
//    self::assertSame('user-phoneNumber', $user->phoneNumber);
//    self::assertSame('/api/employers/'.$employer->getId(), $user->employer);
//    self::assertTrue($user->active);
//
//    $atId = '@id';
//    self::assertSame('/users/'.$newUser->getId(), $user->$atId);
//    $atType = '@type';
//    self::assertSame('User', $user->$atType);
//  }
//
//  /**
//   * Test qu'un utilisateur sans numéro de téléphone puisse être renvoyé.
//   */
//  public function testCanGetAUserWithoutPhoneNumber(): void
//  {
//    $client = static::createClient();
//
//
//    $entityManager = static::$kernel
//      ->getContainer()
//      ->get('doctrine')
//      ->getManager();
//    $employer = $this->getEmployer();
//    $newUser = $this->getUser($employer);
//    $newUser->setPhoneNumber(null);
//    $entityManager->persist($newUser);
//    $entityManager->flush();
//
//    $apiResponse = $client->request('GET', '/users/'.$newUser->getId(), ['auth_bearer' => $this->getJWT(employer: $employer)]);
//
//    self::assertSame(200, $apiResponse->getStatusCode(), 'GET "/users/1" failed.');
//    self::assertJson($apiResponse->getContent());
//
//    $user = json_decode($apiResponse->getContent(), false);
//
//    self::assertCount(16, (array)$user, 'Too much data has been returned.');
//
//    self::assertSame($newUser->getId(), $user->id);
//    self::assertSame('user-login', $user->login);
//    self::assertSame('user-firstname', $user->firstname);
//    self::assertSame('user-lastname', $user->lastname);
//    self::assertSame('user-email', $user->email);
//    self::assertSame('/api/employers/'.$employer->getId(), $user->employer);
//    self::assertTrue($user->active);
//
//    $atId = '@id';
//    self::assertSame('/users/'.$newUser->getId(), $user->$atId);
//    $atType = '@type';
//    self::assertSame('User', $user->$atType);
//  }
//
//  /**
//   * Test qu'un utilisateur sans photo puisse être renvoyé.
//   */
//  public function testCanGetAUserWithoutPicture(): void
//  {
//    $client = static::createClient();
//
//    $entityManager = static::$kernel
//      ->getContainer()
//      ->get('doctrine')
//      ->getManager();
//    $employer = $this->getEmployer();
//    $newUser = $this->getUser($employer);
//    $newUser->setPicture(null);
//    $entityManager->persist($newUser);
//    $entityManager->flush();
//
//    $apiResponse = $client->request('GET', '/users/'.$newUser->getId(), ['auth_bearer' => $this->getJWT(employer: $employer)]);
//
//    self::assertSame(200, $apiResponse->getStatusCode(), 'GET "/users/1" failed.');
//    self::assertJson($apiResponse->getContent());
//
//    $user = json_decode($apiResponse->getContent(), false);
//
//    self::assertCount(16, (array)$user, 'Too much data has been returned.');
//
//    self::assertSame($newUser->getId(), $user->id);
//    self::assertSame('user-login', $user->login);
//    self::assertSame('user-firstname', $user->firstname);
//    self::assertSame('user-lastname', $user->lastname);
//    self::assertSame('user-email', $user->email);
//    self::assertSame('user-phoneNumber', $user->phoneNumber);
//    self::assertSame('/api/employers/'.$employer->getId(), $user->employer);
//    self::assertTrue($user->active);
//
//    $atId = '@id';
//    self::assertSame('/users/'.$newUser->getId(), $user->$atId);
//    $atType = '@type';
//    self::assertSame('User', $user->$atType);
//  }
//
//  /**
//   * Test qu'un utilisateur sans date de connexion
//   * puisse être renvoyé.
//   */
//  public function testCanGetAUserWithoutConnectedAt(): void
//  {
//    $client = static::createClient();
//
//
//    $entityManager = static::$kernel
//      ->getContainer()
//      ->get('doctrine')
//      ->getManager();
//    $employer = $this->getEmployer();
//    $newUser = $this->getUser($employer);
//    $newUser->setConnectedAt(null);
//    $entityManager->persist($newUser);
//    $entityManager->flush();
//
//    $apiResponse = $client->request('GET', '/users/'.$newUser->getId(), ['auth_bearer' => $this->getJWT(employer: $employer)]);
//
//    self::assertSame(200, $apiResponse->getStatusCode(), 'GET "/users/1" failed.');
//    self::assertJson($apiResponse->getContent());
//
//    $user = json_decode($apiResponse->getContent(), false);
//
//    self::assertCount(16, (array)$user, 'Too much data has been returned.');
//
//    self::assertSame($newUser->getId(), $user->id);
//    self::assertSame('user-login', $user->login);
//    self::assertSame('user-firstname', $user->firstname);
//    self::assertSame('user-lastname', $user->lastname);
//    self::assertSame('user-email', $user->email);
//    self::assertSame('user-phoneNumber', $user->phoneNumber);
//    self::assertSame('/api/employers/'.$employer->getId(), $user->employer);
//    self::assertTrue($user->active);
//
//    $atId = '@id';
//    self::assertSame('/users/'.$newUser->getId(), $user->$atId);
//    $atType = '@type';
//    self::assertSame('User', $user->$atType);
//  }
//
//  /**
//   * Test qu'un utilisateur inactif puisse être renvoyé.
//   */
//  public function testCanGetAnInactiveUser(): void
//  {
//    $client = static::createClient();
//
//    $entityManager = static::$kernel
//      ->getContainer()
//      ->get('doctrine')
//      ->getManager();
//    $employer = $this->getEmployer();
//    $newUser = $this->getUser($employer);
//    $newUser->setActive(false);
//    $entityManager->persist($employer);
//    $entityManager->persist($newUser);
//    $entityManager->flush();
//
//    $apiResponse = $client->request('GET', '/users/'.$newUser->getId(), ['auth_bearer' => $this->getJWT(employer: $employer)]);
//    self::assertSame(200, $apiResponse->getStatusCode(), 'GET "/users/1" failed.');
//    self::assertJson($apiResponse->getContent());
//
//    $user = json_decode($apiResponse->getContent(), false);
//
//    self::assertCount(16, (array)$user, 'Too much data has been returned.');
//
//    self::assertSame($newUser->getId(), $user->id);
//    self::assertSame('user-login', $user->login);
//    self::assertSame('user-firstname', $user->firstname);
//    self::assertSame('user-lastname', $user->lastname);
//    self::assertSame('user-email', $user->email);
//    self::assertSame('user-phoneNumber', $user->phoneNumber);
//    self::assertSame('/api/employers/'.$employer->getId(), $user->employer);
//    self::assertFalse($user->active);
//
//    $atId = '@id';
//    self::assertSame('/users/'.$newUser->getId(), $user->$atId);
//    $atType = '@type';
//    self::assertSame('User', $user->$atType);
//  }
}
