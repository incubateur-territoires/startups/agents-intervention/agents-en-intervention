<?php

namespace App\Tests\Api\Users;

use App\Entity\Employer;
use App\Entity\Role;
use App\Tests\Api\AuthenticationTestCase;

class UserPostTest extends AuthenticationTestCase
{
  private static Employer $employer_anct;
  private static Employer $employer_1;
  private static Employer $employer_2;

  #[\Override]
  public function setUp(): void
  {
    $doctrine = static::getContainer()->get('doctrine');
    $em = $doctrine->getManager();

    static::$employer_anct = Employer::new('anct', 'ANCT', 4.4, 6.6);
    static::$employer_1 = Employer::new('siren-1', 'employer-1', 4.4, 6.6);
    static::$employer_2 = Employer::new('siren-2', 'employer-2', 4.4, 6.6);
    $em->persist(static::$employer_anct);
    $em->persist(static::$employer_1);
    $em->persist(static::$employer_2);
    $em->flush();
  }

  public function testCreateUserAsAdminForAnct()
  {
    $client = static::createClient();

    $apiResponse = $client->request(
      'POST',
      '/api/users',
      [
        'headers' => [
          'Content-Type' => 'application/json',
          'authorization' => 'Bearer ' . $this->getJWT(Role::Admin, 'adminadmin', 'admin', static::$employer_anct)
        ],
        'json' => [
          'login' => 'l0g1n',
          'password' => 'p4ssw0rd',
          'firstname' => 'Michel',
          'lastname' => 'Dupont',
          'phoneNumber' => '0987654321',
          'employer' => '/api/employers/1',
          'roles' => ['ROLE_ADMIN']
        ]
      ]
    );

    self::assertSame(201, $apiResponse->getStatusCode(), 'PUT to "/api/users" failed.');
    $content = $apiResponse->getContent();
    self::assertJson($content);

    $jsonResponse = json_decode($apiResponse->getContent(), false);

    self::assertCount(17, (array)$jsonResponse);

    $atId = '@id';
    self::assertSame('/api/users/2', $jsonResponse->$atId);
    self::assertSame(2, $jsonResponse->id);
    self::assertSame('Michel', $jsonResponse->firstname);
    self::assertSame('Dupont', $jsonResponse->lastname);
    self::assertSame('0987654321', $jsonResponse->phoneNumber);
    self::assertSame(1, $jsonResponse->employer->id);
  }

  public function testCreateUserAsAdminForEmployer1()
  {
    $this->expectExceptionMessage('An admin must have ANCT employer');
    $client = static::createClient();

    $apiResponse = $client->request(
      'POST',
      '/api/users',
      [
        'headers' => [
          'Content-Type' => 'application/json',
          'authorization' => 'Bearer ' . $this->getJWT(Role::Admin, 'adminadmin', 'admin', static::$employer_anct)
        ],
        'json' => [
          'login' => 'l0g1n',
          'password' => 'p4ssw0rd',
          'firstname' => 'Michel',
          'lastname' => 'Dupont',
          'phoneNumber' => '0987654321',
          'employer' => '/api/employers/' . self::$employer_1->getId(),
          'roles' => ['ROLE_ADMIN']
        ]
      ]
    );

    self::assertSame(500, $apiResponse->getStatusCode(), 'PUT to "/api/users" failed.');
    $content = $apiResponse->getContent();
    self::assertJson($content);

//    $jsonResponse = json_decode($apiResponse->getContent(), false);
//
//    self::assertCount(16, (array)$jsonResponse);
//
//    $atId = '@id';
//    self::assertSame('/api/users/2', $jsonResponse->$atId);
//    self::assertSame(2, $jsonResponse->id);
//    self::assertSame('Michel', $jsonResponse->firstname);
//    self::assertSame('Dupont', $jsonResponse->lastname);
//    self::assertSame('0987654321', $jsonResponse->phoneNumber);
//    self::assertSame(self::$employer_anct->getId(), $jsonResponse->employer->id);
  }

  public function testCreateUserAsAdminEmployerForAnct()
  {
    $client = static::createClient();

    $apiResponse = $client->request(
      'POST',
      '/api/users',
      [
        'headers' => [
          'Content-Type' => 'application/json',
          'authorization' => 'Bearer ' . $this->getJWT(Role::AdminEmployer, 'admin_employer', 'pwd', static::$employer_1)
        ],
        'json' => [
          'login' => 'l0g1n',
          'password' => 'p4ssw0rd',
          'firstname' => 'Michel',
          'lastname' => 'Dupont',
          'phoneNumber' => '0987654321',
          'employer' => '/api/employers/' . self::$employer_anct->getId(),
          'roles' => ['ROLE_DIRECTOR']
        ]
      ]
    );

    self::assertSame(403, $apiResponse->getStatusCode(), 'PUT to "/api/users" failed.');
  }

  public function testCreateUserAsAdminEmployerForEmployer1()
  {
    $client = static::createClient();

    $apiResponse = $client->request(
      'POST',
      '/api/users',
      [
        'headers' => [
          'Content-Type' => 'application/json',
          'authorization' => 'Bearer ' . $this->getJWT(Role::AdminEmployer, 'admin_employer', 'admin', static::$employer_1)
        ],
        'json' => [
          'login' => 'l0g1n',
          'password' => 'p4ssw0rd',
          'firstname' => 'Michel',
          'lastname' => 'Dupont',
          'phoneNumber' => '0987654321',
          'employer' => '/api/employers/' . self::$employer_1->getId()
        ]
      ]
    );

    self::assertSame(201, $apiResponse->getStatusCode(), 'PUT to "/api/users" failed.');
    $content = $apiResponse->getContent();
    self::assertJson($content);

    $jsonResponse = json_decode($apiResponse->getContent(), false);

    self::assertCount(17, (array)$jsonResponse);

    $atId = '@id';
    self::assertSame('/api/users/2', $jsonResponse->$atId);
    self::assertSame(2, $jsonResponse->id);
    self::assertSame('Michel', $jsonResponse->firstname);
    self::assertSame('Dupont', $jsonResponse->lastname);
    self::assertSame('0987654321', $jsonResponse->phoneNumber);
    self::assertSame(self::$employer_1->getId(), $jsonResponse->employer->id);
  }

  public function testCreateUserAsDirectorForEmployer1()
  {
    $client = static::createClient();

    $apiResponse = $client->request(
      'POST',
      '/api/users',
      [
        'headers' => [
          'Content-Type' => 'application/json',
          'authorization' => 'Bearer ' . $this->getJWT(Role::Director, 'director', 'admin', static::$employer_1)
        ],
        'json' => [
          'login' => 'l0g1n',
          'password' => 'p4ssw0rd',
          'firstname' => 'Michel',
          'lastname' => 'Dupont',
          'phoneNumber' => '0987654321',
          'employer' => '/api/employers/' . self::$employer_1->getId()
        ]
      ]
    );

    self::assertSame(403, $apiResponse->getStatusCode(), 'PUT to "/api/users" failed.');
  }

  public function testCreateUserAsAgentForEmployer1()
  {
    $client = static::createClient();

    $apiResponse = $client->request(
      'POST',
      '/api/users',
      [
        'headers' => [
          'Content-Type' => 'application/json',
          'authorization' => 'Bearer ' . $this->getJWT(Role::Agent, 'agent', 'admin', static::$employer_1)
        ],
        'json' => [
          'login' => 'l0g1n',
          'password' => 'p4ssw0rd',
          'firstname' => 'Michel',
          'lastname' => 'Dupont',
          'phoneNumber' => '0987654321',
          'employer' => '/api/employers/' . self::$employer_1->getId()
        ]
      ]
    );

    self::assertSame(403, $apiResponse->getStatusCode(), 'PUT to "/api/users" failed.');
  }

  public function testCreateUserAsElectedForEmployer1()
  {
    $client = static::createClient();

    $apiResponse = $client->request(
      'POST',
      '/api/users',
      [
        'headers' => [
          'Content-Type' => 'application/json',
          'authorization' => 'Bearer ' . $this->getJWT(Role::Elected, 'elected', 'admin', static::$employer_1)
        ],
        'json' => [
          'login' => 'l0g1n',
          'password' => 'p4ssw0rd',
          'firstname' => 'Michel',
          'lastname' => 'Dupont',
          'phoneNumber' => '0987654321',
          'employer' => '/api/employers/' . self::$employer_1->getId()
        ]
      ]
    );

    self::assertSame(403, $apiResponse->getStatusCode(), 'PUT to "/api/users" failed.');
  }

  public function testCreateUserAsAdminEmployerForEmployer2()
  {
    $client = static::createClient();

    $apiResponse = $client->request(
      'POST',
      '/api/users',
      [
        'headers' => [
          'Content-Type' => 'application/json',
          'authorization' => 'Bearer ' . $this->getJWT(Role::AdminEmployer, 'admin_employer', 'admin', static::$employer_1)
        ],
        'json' => [
          'login' => 'l0g1n',
          'password' => 'p4ssw0rd',
          'firstname' => 'Michel',
          'lastname' => 'Dupont',
          'phoneNumber' => '0987654321',
          'employer' => '/api/employers/' . self::$employer_2->getId()
        ]
      ]
    );

    self::assertSame(403, $apiResponse->getStatusCode(), 'PUT to "/api/users" failed.');
  }
}
