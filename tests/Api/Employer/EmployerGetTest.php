<?php

namespace App\tests\Api\Employer;

use App\Encoder\MultipartDecoder;
use App\Entity\Employer;
use App\Entity\Role;
use App\Entity\User;
use App\Repository\UserRepository;
use App\Security\DeleteInterventionVoter;
use App\Security\EmployerVoter;
use App\Security\InterventionVoter;
use App\Security\UserVoter;
use App\Serializer\UploadedFileDenormalizer;
use App\Tests\Api\PermissionsTestCase;
use PHPUnit\Framework\Attributes as PA;

#[
  PA\CoversClass(Employer::class),
  PA\UsesClass(EmployerVoter::class),
  PA\UsesClass(DeleteInterventionVoter::class),
  PA\UsesClass(InterventionVoter::class),
  PA\UsesClass(MultipartDecoder::class),
  PA\UsesClass(UploadedFileDenormalizer::class),
  PA\UsesClass(User::class),
  PA\UsesClass(UserRepository::class),
  PA\UsesClass(UserVoter::class),
  PA\Group('api'),
  PA\Group('api_admin'),
  PA\Group('api_admin_employers'),
  PA\Group('api_admin_employers_post'),
  PA\Group('employer'),
  PA\TestDox('Administrator')
]
class EmployerGetTest extends PermissionsTestCase
{

  #[\Override]
  protected function getMethod(): string
  {
    return 'GET';
  }

  public function testCanGetAnctEmployerAsAdmin(): void
  {
    $apiResponse = $this->checkPermission('/api/employers/' . static::$employer_anct->getId(), Role::Admin, static::$employer_anct, 200);

    $employer = json_decode($apiResponse->getContent(), false);

    self::assertCount(8, (array)$employer);

    $atId = '@id';
    self::assertSame('/api/employers/1', $employer->$atId);
    self::assertSame(1, $employer->id);
    self::assertSame('anct', $employer->siren);
    self::assertSame('ANCT', $employer->name);
    self::assertSame(4.4, $employer->longitude);
    self::assertSame(6.6, $employer->latitude);
  }

  public function testCanGetEmployer1AsAdmin(): void
  {
    $apiResponse = $this->checkPermission('/api/employers/' . static::$employer_1->getId(), Role::Admin, static::$employer_anct, 200);

    $employer = json_decode($apiResponse->getContent(), false);

    self::assertCount(8, (array)$employer);

    $atId = '@id';
    self::assertSame('/api/employers/2', $employer->$atId);
    self::assertSame(2, $employer->id);
    self::assertSame('siren-1', $employer->siren);
  }

  public function testCanGetEmployer2AsAdmin(): void
  {
    $apiResponse = $this->checkPermission('/api/employers/' . static::$employer_2->getId(), Role::Admin, static::$employer_anct, 200);

    $employer = json_decode($apiResponse->getContent(), false);

    self::assertCount(8, (array)$employer);

    $atId = '@id';
    self::assertSame('/api/employers/3', $employer->$atId);
    self::assertSame(3, $employer->id);
    self::assertSame('siren-2', $employer->siren);
  }

  /**
   * Test qu'un employeur et un directeur
   * puissent être enregistrés.
   */
  public function testCannotGetAnctAsAdminEmployer(): void
  {
    $this->checkPermission('/api/employers/' . static::$employer_anct->getId(), Role::AdminEmployer, static::$employer_1, 403);
  }

  public function testCannotGetAnctAsDirector(): void
  {
    $this->checkPermission('/api/employers/' . static::$employer_anct->getId(), Role::Director, static::$employer_1, 403);
  }

  public function testCanGetAnEmployer1AsDirector1(): void
  {
    $this->checkPermission('/api/employers/' . static::$employer_1->getId(), Role::Director, static::$employer_1, 200);
  }

  public function testCanGetAnEmployer1AsAgent1(): void
  {
    $apiResponse = $this->checkPermission('/api/employers/' . static::$employer_1->getId(), Role::Agent, static::$employer_1, 200);
    $employer = json_decode($apiResponse->getContent(), false);

    $atId = '@id';
    self::assertSame('/api/employers/2', $employer->$atId);
    self::assertSame(2, $employer->id);
    self::assertSame('siren-1', $employer->siren);
  }

  public function testCanGetAnEmployer1AsElected1(): void
  {
    $this->checkPermission('/api/employers/' . static::$employer_1->getId(), Role::Elected, static::$employer_1, 200);
  }

  public function testCanGetAnEmployer2AsDirector1(): void
  {
    $this->checkPermission('/api/employers/' . static::$employer_2->getId(), Role::Director, static::$employer_1, 403);
  }

  public function testCanGetAnEmployer2AsAgent1(): void
  {
    $this->checkPermission('/api/employers/' . static::$employer_2->getId(), Role::Agent, static::$employer_1, 403);
  }

  public function testCanGetAnEmployer2AsElected1(): void
  {
    $this->checkPermission('/api/employers/' . static::$employer_2->getId(), Role::Elected, static::$employer_1, 403);
  }

}
