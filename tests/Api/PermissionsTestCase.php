<?php

namespace App\Tests\Api;

use App\Entity\Employer;
use App\Entity\Picture;
use App\Entity\PictureTag;
use App\Entity\Role;
use App\Entity\User;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;
use Zenstruck\Foundry\Test\Factories;
use Zenstruck\Foundry\Test\ResetDatabase;

abstract class PermissionsTestCase extends AuthenticationTestCase
{
  use ResetDatabase, Factories;

  protected static Employer $employer_anct;
  protected static Employer $employer_1;
  protected static Employer $employer_2;
  protected static User $user;
  protected static User $userActive;
  protected static User $userInactive;
  protected static User $user2;
  protected static User $user3;
  protected static User $user4;
  protected static User $user5;
  protected static Picture $picture1;
  protected static Picture $picture2;
  protected static User $userWithPicture;

  #[\Override]
  public function setUp(): void
  {
    $doctrine = static::getContainer()->get('doctrine');
    $em = $doctrine->getManager();

    static::$employer_anct = Employer::new('anct', 'ANCT', 4.4, 6.6);
    static::$employer_1 = Employer::new('siren-1', 'employer-1', 4.4, 6.6);
    static::$employer_2 = Employer::new('siren-2', 'employer-2', 4.4, 6.6);
    $em->persist(static::$employer_anct);
    $em->persist(static::$employer_1);
    $em->persist(static::$employer_2);

    static::$user = User::new('test', 'test', 'test', 'test', self::$employer_anct, ['ROLE_ADMIN'], 'email@email.com', '0987654321', null, false);
    static::$userActive = User::new('test_active', 'test', 'test', 'test', self::$employer_anct, ['ROLE_ADMIN'], 'emailactive@email.com', '0987654321');
    static::$userInactive = User::new('test_inactive', 'test', 'test', 'test', self::$employer_anct, ['ROLE_ADMIN'], 'emailinactive@email.com', '0987654321', null, false);
    static::createUser(static::$user);
    static::createUser(static::$userActive);
    static::createUser(static::$userInactive);

    // ça ne devrait pas être possible de supprimer cet utilisateur
    static::$user2 = User::new('test2', 'test', 'test', 'test', self::$employer_2, ['ROLE_ADMIN_EMPLOYER'], 'email@email.com', '0987654321', null, false);
    static::createUser(static::$user2);
    static::$user3 = User::new('test3', 'test', 'test', 'test', self::$employer_anct, ['ROLE_ADMIN'], 'email@email.com', '0987654321', null, true);
    static::createUser(static::$user3);
    static::$user4 = User::new('test4', 'test', 'test', 'test', self::$employer_1, ['ROLE_DIRECTOR'], 'email@email.com', '0987654321', null, false);
    static::createUser(static::$user4);
    static::$user5 = User::new('test5', 'test', 'test', 'test', self::$employer_1, ['ROLE_DIRECTOR'], 'email@email.com', '0987654321', null, false);
    static::createUser(static::$user5);

    $em->persist(static::$user);
    $em->persist(static::$userActive);
    $em->persist(static::$user2);
    $em->persist(static::$user3);
    $em->persist(static::$user4);
    $em->persist(static::$user5);

    static::$picture1 = new Picture('test-picture1', PictureTag::Avatar);
    static::$picture2 = new Picture('test-picture2', PictureTag::Avatar);

    $em->persist(static::$picture1);
    $em->persist(static::$picture2);

    static::$userWithPicture = User::new('test6', 'test', 'test', 'test', self::$employer_1, ['ROLE_DIRECTOR'], 'email@email.com', '0987654321', static::$picture2, false);
    static::createUser(static::$userWithPicture);
    $em->persist(static::$userWithPicture);


    $em->flush();
  }

  protected static function createUser(User $user): User
  {
    $em = static::getContainer()->get('doctrine')->getManager();
    $em->persist($user);
    $em->flush();
    return $user;
  }

  /**
   * @param string $url
   * @param Role $role
   * @param Employer $employer
   * @param int $statusCode
   * @return ResponseInterface
   *
   * @throws ClientExceptionInterface
   * @throws RedirectionExceptionInterface
   * @throws ServerExceptionInterface
   * @throws TransportExceptionInterface
   */
  protected function checkPermission(string $url, Role $role, Employer $employer, int $statusCode): ResponseInterface
  {
    $permissionCheck = new PermissionCheck(
      $employer,
      $role,
      $statusCode,
      $url
    );
    return $this->checkPermissionObject($permissionCheck);
  }

  /**
   * @param PermissionCheck $check
   * @param mixed|null $content
   * @return ResponseInterface
   * @throws ClientExceptionInterface
   * @throws RedirectionExceptionInterface
   * @throws ServerExceptionInterface
   * @throws TransportExceptionInterface
   * @todo rename to checkPermission
   */
  public function checkPermissionObject(PermissionCheck $check, mixed $content = null): ResponseInterface
  {
    $client = static::createClient();

    $apiResponse = $client->request(
      $this->getMethod(),
      $check->url,
      [
        'headers' => [
          'Content-Type' => $this->getMethod() === 'PATCH' ? 'application/merge-patch+json' : 'application/json',
          'authorization' => 'Bearer ' . $this->getJWT($check->role, $check->role->value . uniqid(), $check->role->value, $check->employer)
        ],
        'json' => $content ?? $check->data ?? $this->getContent()
      ]
    );

    if ($check->statusCode !== $apiResponse->getStatusCode()) {
      dump($apiResponse);
    }
//    dd($apiResponse);
//dump($check->statusCode, $apiResponse->getStatusCode(), $this->getMethod() . ' to "' . $check->url . '" failed.');
    self::assertSame($check->statusCode, $apiResponse->getStatusCode(), $this->getMethod() . ' to "' . $check->url . '" failed with role ' . $check->role->name . ' and employer ' . $check->employer->getName() . '.');
    if ($check->statusCode === 200 || $check->statusCode === 201) self::assertJson($apiResponse->getContent());
    if ($check->statusCode === 204) self::assertEmpty($apiResponse->getContent());

    return $apiResponse;
  }

  /**
   * @param PermissionCheck[] $checks
   * @return ResponseInterface[]
   */
  protected function checkPermissions(array $checks): array
  {
    return array_map($this->checkPermissionObject(...), $checks);
  }

  abstract protected function getMethod(): string;

  protected function getContent(): ?array
  {
    return null;
  }
}
