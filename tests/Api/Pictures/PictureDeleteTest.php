<?php

declare(strict_types=1);

namespace App\Tests\Api\Pictures;

use App\Encoder\MultipartDecoder;
use App\Entity\Category;
use App\Entity\Employer;
use App\Entity\Intervention;
use App\Entity\Location;
use App\Entity\Picture;
use App\Entity\PictureTag;
use App\Entity\Priority;
use App\Entity\Role;
use App\Entity\Status;
use App\Entity\Type;
use App\Entity\User;
use App\Logger\UserProcessor;
use App\Repository\UserRepository;
use App\Security\DeleteInterventionVoter;
use App\Security\EmployerVoter;
use App\Security\InterventionVoter;
use App\Security\UserVoter;
use App\Tests\Api\AuthenticationTestCase;
use PHPUnit\Framework\Attributes as PA;
use Zenstruck\Foundry\Test\Factories;
use Zenstruck\Foundry\Test\ResetDatabase;

/**
 * Test DELETE /api/pictures/{id}.
 */
#[
  PA\CoversClass(Picture::class),
  PA\UsesClass(Category::class),
  PA\UsesClass(Employer::class),
  PA\UsesClass(Intervention::class),
  PA\UsesClass(Location::class),
  PA\UsesClass(Priority::class),
  PA\UsesClass(Role::class),
  PA\UsesClass(Status::class),
  PA\UsesClass(Type::class),
  PA\UsesClass(User::class),
  PA\UsesClass(UserRepository::class),
  PA\UsesClass(MultipartDecoder::class),
  PA\UsesClass(DeleteInterventionVoter::class),
  PA\UsesClass(EmployerVoter::class),
  PA\UsesClass(InterventionVoter::class),
  PA\UsesClass(UserVoter::class),
  PA\UsesClass(UserProcessor::class),
  PA\Group('api'),
  PA\Group('api_pictures'),
  PA\Group('api_pictures_delete'),
  PA\Group('picture')
]
final class PictureDeleteTest extends AuthenticationTestCase
{
  use ResetDatabase, Factories;

  /**
   * Test que la route nécessite d'être authentifié.
   */
  public function testNeedsAuthentication(): void
  {
    $client = static::createClient();
    $apiResponse = $client->request('DELETE', '/api/pictures/1');

    self::assertSame(401, $apiResponse->getStatusCode(), 'DELETE "/api/pictures/1" succeeded.');
  }

  /**
   * Ajoute une photo.
   */
  private function addPicture(): void
  {
    $picture = new Picture('picture-file', PictureTag::After);
    $category = new Category();
    $category->setName('category-name');
    $category->setPicture('category-picture');

    $type = new Type();
    $type->setName('type-name');
    $type->setCategory($category);
    $type->setPicture('type-picture');
    $type->setDescription('description');

    $employer = Employer::new('employer-siren', 'employer-name', 45, 54);

    $user = User::new(
      'user-login',
      'user-password',
      'user-firstname',
      'user-lastname',
      $employer,
      [Role::Director],
      null,
      null,
      $picture,
      true,
      new \DateTimeImmutable('2023-01-01T00:00:00+00:00')
    );

    $location = new Location(null, null, null, null, 1.1, 2.2);

    $intervention = new Intervention(
      'intervention-description',
      Priority::Normal,
      $type,
      $location
    );
    $intervention->setTitle('Intervention');
    $intervention->setAuthor($user);
    $intervention->addPicture($picture);
    $intervention->setEmployer($employer);
    $intervention->setLogicId(42);
    $intervention->setStatus(Status::ToDo);

    $entityManager = static::$kernel
      ->getContainer()
      ->get('doctrine')
      ->getManager();
//    dd($entityManager->getConnection()->executeQuery('select * from employer')->fetchAll());
    $entityManager->persist($intervention);
    $entityManager->persist($category);
    $entityManager->persist($type);
    $entityManager->persist($user);
    $entityManager->persist($employer);
    $entityManager->persist($location);
    $entityManager->persist($picture);
    $entityManager->flush();
  }

  /**
   * Test qu'une photo puisse être supprimée.
   */
  public function testCanDeleteAPicture(): void
  {
    $client = static::createClient();
    $this->addPicture();

    $client->request('DELETE', '/api/pictures/1', ['auth_bearer' => $this->getJWT()]);
    $apiResponse = $client->getResponse();

    self::assertSame(204, $apiResponse->getStatusCode(), 'DELETE "/api/pictures/1" failed.');
    self::assertEmpty($apiResponse->getContent());
  }
}
