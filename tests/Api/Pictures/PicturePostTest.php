<?php

declare(strict_types=1);

namespace App\Tests\Api\Pictures;

use App\Encoder\MultipartDecoder;
use App\Entity\Category;
use App\Entity\Employer;
use App\Entity\Picture;
use App\Entity\Intervention;
use App\Entity\Location;
use App\Entity\PictureTag;
use App\Entity\Priority;
use App\Entity\Role;
use App\Entity\Status;
use App\Entity\Type;
use App\Entity\User;
use App\Logger\UserProcessor;
use App\Repository\UserRepository;
use App\Security\DeleteInterventionVoter;
use App\Security\EmployerVoter;
use App\Security\InterventionVoter;
use App\Security\UserVoter;
use App\Serializer\UploadedFileDenormalizer;
use App\Service\S3ServiceTest;
use App\Tests\Api\AuthenticationTestCase;
use App\Tests\FixtureTrait;
use PHPUnit\Framework\Attributes as PA;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Zenstruck\Foundry\Test\Factories;
use Zenstruck\Foundry\Test\ResetDatabase;

/**
 * Test POST /api/pictures.
 */
#[
  PA\CoversClass(Picture::class),
  PA\UsesClass(Category::class),
  PA\UsesClass(Employer::class),
  PA\UsesClass(Intervention::class),
  PA\UsesClass(Location::class),
  PA\UsesClass(Priority::class),
  PA\UsesClass(Role::class),
  PA\UsesClass(Status::class),
  PA\UsesClass(Type::class),
  PA\UsesClass(User::class),
  PA\UsesClass(UserRepository::class),
  PA\UsesClass(MultipartDecoder::class),
  PA\UsesClass(UserProcessor::class),
  PA\UsesClass(DeleteInterventionVoter::class),
  PA\UsesClass(EmployerVoter::class),
  PA\UsesClass(InterventionVoter::class),
  PA\UsesClass(UserVoter::class),
  PA\UsesClass(UploadedFileDenormalizer::class),
  PA\UsesClass(S3ServiceTest::class),
  PA\Group('api'),
  PA\Group('api_pictures'),
  PA\Group('api_pictures_post'),
  PA\Group('picture')
]
final class PicturePostTest extends AuthenticationTestCase
{
  use ResetDatabase, Factories, FixtureTrait;

  /**
   * Test que la route nécessite d'être authentifié.
   */
  public function testNeedsAuthentication(): void
  {
    $client = static::createClient();

    $apiResponse = $client->request('POST', '/api/pictures', [
      'headers' => [
        'Content-Type' => 'multipart/form-data',
      ],
    ]);

    self::assertSame(401, $apiResponse->getStatusCode(), 'POST "/api/pictures" succeeded.');
  }

  /**
   * Renvoie les données de la photo.
   * @return array Les données de la photo.
   */
  private function pictureToPost(): array
  {
    $uploadedFile = new UploadedFile(
      __DIR__.'/../../../fixtures/test.jpg',
      'test.jpg'
    );

    return [
      'fileName' => $uploadedFile,
      'tag' => PictureTag::Before,
      'intervention' => '/interventions/1'
    ];
  }

  /**
   * Test que des photos puissent être enregistrées.
   */
  public function testCanPostPictures(): void
  {
    $client = static::createClient();;

    $pic = $this->pictureToPost();
    $apiResponse = $client->request('POST', '/api/pictures', [
      'headers' => [
        'content-type' => 'multipart/form-data',
        'authorization' => 'Bearer ' . $this->getJWT(),
      ],
      'json' => $pic,
      'extra' => [
        'files' => [
          'fileName' => $pic['fileName']
        ]
      ]
    ]);

    self::assertSame(201, $apiResponse->getStatusCode(), 'POST to "/api/pictures" failed.');
    self::assertJson($apiResponse->getContent());

    $jsonResponse = json_decode($apiResponse->getContent(), false);

    self::assertSame(1, $jsonResponse->id);
    $atId = '@id';
    self::assertSame('/api/pictures/1', $jsonResponse->$atId);
  }
  /**
   * Test que des photos puissent être enregistrées.
   */
  public function testCanNotPostPicturesWithInvalidMediaType(): never
  {
    $this->markTestSkipped('test is not accurate');
    $client = static::createClient();

    $picturePost = $this->pictureToPost();
    $picturePost['fileName'] = 'picture-file';
    $apiResponse = $client->request('POST', '/api/pictures', [
      'headers' => [
        'content-type' => 'multipart/form-data',
        'authorization' => 'Bearer ' . $this->getJWT(),
      ],
      'json' => $picturePost,
//      'extra' => [
//        'files' => [
//          'fileName' => $picturePost['fileName']
//        ]
//      ]
    ]);

    self::assertSame(500, $apiResponse->getStatusCode(), 'POST to "/api/pictures" failed.');
    self::assertJson($apiResponse->getContent());

    $jsonResponse = json_decode((string) $apiResponse->getContent(), false);

    self::assertSame(1, $jsonResponse->id);
    $atId = '@id';
    self::assertSame('/api/pictures/1', $jsonResponse->$atId);
  }

  /**
   * Test que l'on ne puisse pas ajouter
   * une photo avec une intervention inconnue.
   */
  public function testCanPostAPictureWithANonExistantIntervention(): void
  {
    $client = static::createClient();

    $pictureDatas = $this->pictureToPost();
    $pictureDatas['intervention'] = '/interventions/99999';

    $apiResponse = $client->request('POST', '/api/pictures', [
      'headers' => [
        'content-type' => 'multipart/form-data',
        'authorization' => 'Bearer ' . $this->getJWT(),
      ],
      'json' => $pictureDatas,
      'extra' => [
        'files' => [
          'fileName' => $pictureDatas['fileName']
        ]
      ]
    ]);

    self::assertSame(201, $apiResponse->getStatusCode(), 'POST to "/api/pictures" did not failed.');
    $content = json_decode($apiResponse->getContent(), false);
    self::assertNull($content->intervention);
  }
}
