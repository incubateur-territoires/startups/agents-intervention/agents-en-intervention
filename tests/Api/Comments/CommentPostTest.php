<?php

declare(strict_types=1);

namespace App\Tests\Api\Comments;

use App\Encoder\MultipartDecoder;
use App\Entity\Category;
use App\Entity\Comment;
use App\Entity\Employer;
use App\Entity\Intervention;
use App\Entity\Location;
use App\Entity\Priority;
use App\Entity\Role;
use App\Entity\Status;
use App\Entity\Type;
use App\Entity\User;
use App\Logger\UserProcessor;
use App\Repository\UserRepository;
use App\Security\DeleteInterventionVoter;
use App\Security\EmployerVoter;
use App\Security\InterventionVoter;
use App\Security\UserVoter;
use App\Serializer\UploadedFileDenormalizer;
use App\State\CommentProcessor;
use App\Tests\Api\AuthenticationTestCase;
use Doctrine\ORM\EntityManagerInterface;
use Nelmio\Alice\Loader\NativeLoader;
use PHPUnit\Framework\Attributes as PA;
use Zenstruck\Foundry\Test\Factories;
use Zenstruck\Foundry\Test\ResetDatabase;

/**
 * Test POST /api/comments.
 */
#[
  PA\CoversClass(Comment::class),
  PA\UsesClass(Category::class),
  PA\UsesClass(Employer::class),
  PA\UsesClass(Intervention::class),
  PA\UsesClass(Location::class),
  PA\UsesClass(Priority::class),
  PA\UsesClass(Role::class),
  PA\UsesClass(Status::class),
  PA\UsesClass(Type::class),
  PA\UsesClass(User::class),
  PA\UsesClass(UserRepository::class),
  PA\UsesClass(MultipartDecoder::class),
  PA\UsesClass(UserProcessor::class),
  PA\UsesClass(DeleteInterventionVoter::class),
  PA\UsesClass(EmployerVoter::class),
  PA\UsesClass(InterventionVoter::class),
  PA\UsesClass(UserVoter::class),
  PA\UsesClass(UploadedFileDenormalizer::class),
  PA\UsesClass(CommentProcessor::class),
  PA\Group('api'),
  PA\Group('api_comments'),
  PA\Group('api_comments_post'),
  PA\Group('comment')
]
final class CommentPostTest extends AuthenticationTestCase
{
  use ResetDatabase, Factories;

  /**
   * Test que la route nécessite d'être authentifié.
   */
  public function testNeedsAuthentication(): never
  {
    $this->markTestSkipped();
    $client = static::createClient();

    $apiResponse = $client->request('POST', '/api/comments', [
      'headers' => [
        'Content-Type' => 'application/json',
      ],
    ]);
dd($apiResponse);
    self::assertSame(401, $apiResponse->getStatusCode(), 'POST "/api/comments" succeeded.');
  }

  /**
   * Ajoute les fixtures.
   * @param EntityManagerInterface $entityManager le gestionnaire d'entité.
   */
  private function addFixtures(EntityManagerInterface $entityManager): void
  {
    $loader = new NativeLoader();
    $categorySet = $loader->loadFile(__DIR__ . '/../../../fixtures/category.yaml');

    foreach ($categorySet->getObjects() as $category) {
      $entityManager->persist($category);
    }

    $locationSet = $loader->loadFile(__DIR__ . '/../../../fixtures/location.yaml');

    foreach ($locationSet->getObjects() as $location) {
      $entityManager->persist($location);
    }

    $typeSet = $loader->loadFile(__DIR__ . '/../../../fixtures/type.yaml', $categorySet->getParameters(), $categorySet->getObjects());

    foreach ($typeSet->getObjects() as $type) {
      $entityManager->persist($type);
    }

    $employerSet = $loader->loadFile(__DIR__ . '/../../../fixtures/employer.yaml');

    foreach ($employerSet->getObjects() as $employer) {
      $entityManager->persist($employer);
    }

    $userSet = $loader->loadFile(
      __DIR__ . '/../../../fixtures/user.yaml',
      $employerSet->getParameters(),
      $employerSet->getObjects()
    );

    foreach ($userSet->getObjects() as $user) {
      $entityManager->persist($user);
    }

    $interventionSet = $loader->loadFile(
      __DIR__ . '/../../../fixtures/intervention.yaml',
      array_merge(
        $categorySet->getParameters(),
        $typeSet->getParameters(),
        $userSet->getParameters(),
        $employerSet->getParameters(),
        $locationSet->getParameters()
      ),
      array_merge(
        $categorySet->getObjects(),
        $typeSet->getObjects(),
        $userSet->getObjects(),
        $employerSet->getObjects(),
        $locationSet->getObjects()
      )
    );

    foreach ($interventionSet->getObjects() as $intervention) {
      $entityManager->persist($intervention);
    }
    $entityManager->flush();
  }

  /**
   * Renvoie les données du commentaire.
   * @return array Les données du commentaire.
   */
  private function commentToPost(): array
  {
    return [
      'message' => 'comment-message',
      'intervention' => 'api/interventions/1',
    ];
  }

  /**
   * Test que des commentaires puissent être enregistrés.
   */
  public function testCanPostComments(): void
  {
    $client = static::createClient();

    $doctrine = static::$kernel->getContainer()->get('doctrine');
    $this->addFixtures($doctrine->getManager());

    $apiResponse = $client->request('POST', '/api/comments', [
      'headers' => [
        'Content-Type' => 'application/json',
        'authorization' => 'Bearer ' . $this->getJWT(Role::Director),
      ],
      'json' => $this->commentToPost(),
    ]);

    self::assertSame(201, $apiResponse->getStatusCode(), 'POST to "/api/comments" failed.');
    self::assertJson($apiResponse->getContent());
    $jsonResponse = json_decode((string) $apiResponse->getContent(), false);

    self::assertSame(1, $jsonResponse->id);
    $atId = '@id';
    self::assertSame('/api/comments/1', $jsonResponse->$atId);
  }

  /**
   * Test que l'on ne puisse pas ajouter
   * un commentaire avec une intervention inconnue.
   */
  public function testCanNotPostACommentWithANonExistantIntervention(): void
  {
    $client = static::createClient();

    $doctrine = static::$kernel->getContainer()->get('doctrine');

    $this->addFixtures($doctrine->getManager());

    $commentDatas = $this->commentToPost();
    $commentDatas['intervention'] = '/interventions/9000999';

    $apiResponse = $client->request('POST', '/api/comments', [
      'headers' => [
        'CONTENT_TYPE' => 'application/json',
        'authorization' => 'Bearer ' . $this->getJWT(),
      ],
      'json' => $commentDatas,
    ]);

    self::assertSame(400, $apiResponse->getStatusCode(), 'POST to "/api/comments" did not failed.');
  }
}
