<?php

declare(strict_types=1);

namespace App\Tests\Api\Interventions;

use App\Entity\Category;
use App\Entity\Comment;
use App\Entity\Employer;
use App\Entity\Intervention;
use App\Entity\Location;
use App\Entity\Picture;
use App\Entity\PictureTag;
use App\Entity\Priority;
use App\Entity\Role;
use App\Entity\Status;
use App\Entity\Type;
use App\Entity\User;
use App\Repository\UserRepository;
use App\Tests\Api\AuthenticationTestCase;
use PHPUnit\Framework\Attributes as PA;
use Zenstruck\Foundry\Test\Factories;
use Zenstruck\Foundry\Test\ResetDatabase;

/**
 * Test GET /api/interventions/{id}.
 */
#[
  PA\CoversClass(Intervention::class),
  PA\UsesClass(Category::class),
  PA\UsesClass(Comment::class),
  PA\UsesClass(Employer::class),
  PA\UsesClass(Location::class),
  PA\UsesClass(Priority::class),
  PA\UsesClass(Picture::class),
  PA\UsesClass(PictureTag::class),
  PA\UsesClass(Role::class),
  PA\UsesClass(Status::class),
  PA\UsesClass(Type::class),
  PA\UsesClass(User::class),
  PA\UsesClass(UserRepository::class),
  PA\Group('api'),
  PA\Group('api_interventions'),
  PA\Group('api_interventions_get'),
  PA\Group('intervention')
]
final class InterventionGetTest extends AuthenticationTestCase
{
  use ResetDatabase, Factories;

  /**
   * Test que la route nécessite d'être authentifié.
   */
  public function testNeedsAuthentication(): void
  {
    $client = static::createClient();

    $apiResponse = $client->request('GET', '/api/interventions/1');

    self::assertSame(401, $apiResponse->getStatusCode(), 'GET "/api/interventions/1" succeeded.');
  }

  /**
   * Ajoute une intervention.
   */
  private function addIntervention(): Intervention
  {
    $category = new Category();
    $category->setName('category-name');
    $category->setPicture('category-picture');

    $type = new Type();
    $type->setName('type-name');
    $type->setCategory($category);
    $type->setPicture('type-picture');

    $employer = Employer::new('employer-siren', 'employer-name', 45, 54);
    $reflection = new \ReflectionClass($employer);
    $property = $reflection->getProperty('id');
    $property->setAccessible(true);
    $property->setValue($employer, 1);

    $picture_user = new Picture('user-picture-file', PictureTag::Avatar);
    $user = User::new(
      'user-login',
      'password',
      'user-firstname',
      'user-lastname',
      $employer,
      [Role::Director->value],
      'email@email.email',
      '0987654321',
      $picture_user,
      true
    );
    $user->setPassword(self::getContainer()->get('security.user_password_hasher')->hashPassword($user, 'password'));

    $location = new Location(null, null, null, null, 1.1, 2.2);

    $intervention = new Intervention(
      'intervention-description',
      Priority::Normal,
      $type,
      $location
    );
    $intervention->setTitle('Intervention');
    $intervention->setEmployer($employer);
    $intervention->addParticipant($user);
    $intervention->setLogicId(42);
    $intervention->setAuthor($user);
    $intervention->setStatus(Status::ToDo);
    $intervention->setType($type);

    $comment = new Comment('comment-message', $intervention);
    $comment->setAuthor($user);
    $intervention->addComment($comment);

    $picture = new Picture('picture-file', PictureTag::Before, null, $intervention);
    $intervention->addPicture($picture);

    $entityManager = static::$kernel
      ->getContainer()
      ->get('doctrine')
      ->getManager();
    $entityManager->persist($category);
    $entityManager->persist($type);
    $entityManager->persist($picture_user);
    $entityManager->persist($user);
    $entityManager->persist($employer);
    $entityManager->persist($location);
    $entityManager->persist($intervention);
    $entityManager->persist($comment);
    $entityManager->persist($picture);
    $entityManager->flush();

    return $intervention;
  }

  /**
   * Test qu'une intervention puisse être renvoyée.
   */
  public function testCanGetAnIntervention(): void
  {
    $client = static::createClient();

    $interventionAdded = $this->addIntervention();
    $apiResponse = $client->request('GET', '/api/interventions/'.$interventionAdded->getId(), ['auth_bearer' => $this->getJWT(login: 'user-login')]);

    self::assertSame(200, $apiResponse->getStatusCode(), 'GET "/api/interventions/1" failed.');
    self::assertJson($apiResponse->getContent());

    $intervention = json_decode($apiResponse->getContent(), false);

    self::assertCount(26, (array)$intervention, 'Too much data has been returned.');

    self::assertSame($interventionAdded->getId(), $intervention->id);
//    self::assertSame('2023-01-01T00:00:00+01:00', $intervention->createdAt);

    $this->assertStatusIsTodo($intervention->status);
    $this->assertPriorityIsComplete($intervention->priority);
//    $this->assertCategoryIsComplete($intervention->category);
    $this->assertTypeIsComplete($intervention->type);
    $this->assertAuthorIsComplete($intervention->author);
    $this->assertLocationIsComplete($intervention->location);

    self::assertIsArray($intervention->participants);
    self::assertArrayHasKey(0, $intervention->participants);

    foreach ($intervention->participants as $participant) {
      $this->assertParticipantIsComplete($participant);
    }

    self::assertIsArray($intervention->comments);
    self::assertArrayHasKey(0, $intervention->comments);

    foreach ($intervention->comments as $comment) {
      $this->assertCommentIsComplete($comment);
    }

    self::assertIsArray($intervention->pictures);
    self::assertArrayHasKey(0, $intervention->pictures);

    foreach ($intervention->pictures as $picture) {
      $this->assertPictureIsComplete($picture);
    }
  }

  /**
   * Les assertions du statut.
   * @param string $status le statut.
   */
  private function assertStatusIsTodo(string $status): void
  {
    self::assertSame(Status::ToDo->value, $status);
  }

  /**
   * Les assertions de la priorité.
   * @param string $priority la priorité.
   */
  private function assertPriorityIsComplete(string $priority): void
  {
    self::assertSame(Priority::Normal->value, $priority);
  }

  /**
   * Les assertions de la catégorie.
   * @param object $category la catégorie.
   */
  private function assertCategoryIsComplete(object $category): void
  {
    self::assertCount(4, (array)$category, 'Incorrect count of catergory data has been returned.');

    self::assertSame(1, $category->id);
    self::assertSame('category-name', $category->name);

    $atId = '@id';
    self::assertSame('/categories/1', $category->$atId);
    $atType = '@type';
    self::assertSame('Category', $category->$atType);
  }

  /**
   * Les assertions du type.
   * @param object $type du type.
   */
  private function assertTypeIsComplete(object $type): void
  {
    self::assertCount(7, (array)$type, 'Incorrect count of type data has been returned.');

    self::assertIsInt($type->id);
    self::assertSame('type-name', $type->name);

    $atId = '@id';
    self::assertSame('/api/types/'.$type->id, $type->$atId);
    $atType = '@type';
    self::assertSame('Type', $type->$atType);
  }

  /**
   * Les assertions de l'auteur.
   * @param object $author l'auteur.
   */
  private function assertAuthorIsComplete(object $author): void
  {
    self::assertCount(9, (array)$author, 'Incorrect count of user data has been returned.');

//    self::assertSame(1, $author->id);
    self::assertSame('user-firstname', $author->firstname);
    self::assertSame('user-lastname', $author->lastname);
    self::assertSame('user-picture-file', $author->picture->fileName);

//    $atId = '@id';
//    self::assertSame('/users/1', $author->$atId);
    $atType = '@type';
    self::assertSame('User', $author->$atType);
  }

  /**
   * Les assertions de la localisation.
   * @param object $location la localisation.
   */
  private function assertLocationIsComplete(object $location): void
  {
    self::assertCount(10, (array)$location, 'Incorrect count of location data has been returned.');

    self::assertSame(1.1, $location->longitude);
    self::assertSame(2.2, $location->latitude);

    $atType = '@type';
    self::assertSame('Location', $location->$atType);
  }

  /**
   * Les assertions de l'intervenant.
   * @param object $participant l'intervenant.
   */
  private function assertParticipantIsComplete(object $participant): void
  {
    self::assertCount(9, (array)$participant, 'Incorrect count of user data has been returned.');
    self::assertSame('user-firstname', $participant->firstname);
    self::assertSame('user-lastname', $participant->lastname);
    self::assertSame('user-picture-file', $participant->picture->fileName);

    $atType = '@type';
    self::assertSame('User', $participant->$atType);
  }

  /**
   * Les assertions du commentaire.
   * @param object $comment le commentaire.
   */
  private function assertCommentIsComplete(object $comment): void
  {
    self::assertCount(5, (array)$comment, 'Incorrect count of comment data has been returned.');
    self::assertSame('comment-message', $comment->message);

    $this->assertAuthorIsComplete($comment->author);

    $atType = '@type';
    self::assertSame('Comment', $comment->$atType);
  }

  /**
   * Les assertions de la photo.
   * @param object $picture la photo.
   */
  private function assertPictureIsComplete(object $picture): void
  {
    self::assertCount(9, (array)$picture, 'Incorrect count of picture data has been returned.');

    self::assertSame('picture-file', $picture->fileName);

    self::assertSame(PictureTag::Before->value, $picture->tag);

    $atType = '@type';
    self::assertSame('Picture', $picture->$atType);
  }
}
