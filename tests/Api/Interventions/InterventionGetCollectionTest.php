<?php

declare(strict_types=1);

namespace App\Tests\Api\Interventions;

use App\Encoder\MultipartDecoder;
use App\Entity\Category;
use App\Entity\Comment;
use App\Entity\Employer;
use App\Entity\Intervention;
use App\Entity\Location;
use App\Entity\Picture;
use App\Entity\PictureTag;
use App\Entity\Priority;
use App\Entity\Role;
use App\Entity\Status;
use App\Entity\Type;
use App\Entity\User;
use App\Filter\InterventionParticipantFilter;
use App\Model\Frequency;
use App\Repository\UserRepository;
use App\Security\DeleteInterventionVoter;
use App\Security\EmployerVoter;
use App\Security\InterventionVoter;
use App\Security\UserVoter;
use App\State\UserProcessor;
use App\Tests\Api\AuthenticationTestCase;
use PHPUnit\Framework\Attributes as PA;
use Spatie\Snapshots\MatchesSnapshots;
use Zenstruck\Foundry\Test\Factories;
use Zenstruck\Foundry\Test\ResetDatabase;

/**
 * Test GET /interventions.
 */
#[
  PA\CoversClass(Intervention::class),
  PA\UsesClass(Category::class),
  PA\UsesClass(Comment::class),
  PA\UsesClass(Employer::class),
  PA\UsesClass(Location::class),
  PA\UsesClass(Priority::class),
  PA\UsesClass(Picture::class),
  PA\UsesClass(PictureTag::class),
  PA\UsesClass(Role::class),
  PA\UsesClass(Status::class),
  PA\UsesClass(Type::class),
  PA\UsesClass(User::class),
  PA\UsesClass(UserRepository::class),
  PA\UsesClass(MultipartDecoder::class),
  PA\UsesClass(InterventionParticipantFilter::class),
  PA\UsesClass(UserProcessor::class),
  PA\UsesClass(DeleteInterventionVoter::class),
  PA\UsesClass(EmployerVoter::class),
  PA\UsesClass(InterventionVoter::class),
  PA\UsesClass(UserVoter::class),
  PA\Group('api'),
  PA\Group('api_interventions'),
  PA\Group('api_interventions_get_collection'),
  PA\Group('intervention')
]
final class InterventionGetCollectionTest extends AuthenticationTestCase
{
  use ResetDatabase, Factories, MatchesSnapshots;

  /**
   * Test que la route nécessite d'être authentifié.
   */
  public function testNeedsAuthentication(): void
  {
    $client = static::createClient();

    $apiResponse = $client->request('GET', '/api/employers/1/interventions');

    self::assertSame(401, $apiResponse->getStatusCode(), 'GET "/interventions" succeeded.');
  }

  /**
   * Ajoute une intervention.
   */
  private function addInterventions(): void
  {
    $category = new Category();
    $category->setName('category-name');
    $category->setPicture('category-picture');

    $type = new Type();
    $type->setName('type-name');
    $type->setCategory($category);
    $type->setPicture('type-picture');

    $entityManager = static::$kernel
      ->getContainer()
      ->get('doctrine')
      ->getManager();
    $existingEmployer = $entityManager->getRepository(Employer::class)->find(1);
    $employer = $existingEmployer ?? Employer::new('employer-siren', 'employer-name', 45, 54);
    $reflection = new \ReflectionClass($employer);
    $property = $reflection->getProperty('id');
    $property->setAccessible(true);
    $property->setValue($employer, 1);

    $user = User::new(
      'user-login',
      'user-password',
      'user-firstname',
      'user-lastname',
      $employer,
      [Role::Director],
      'email@email.email',
      '0987654321',
    );
    $user->setPassword(self::getContainer()->get('security.user_password_hasher')->hashPassword($user, $user->getLogin()));

    $location = new Location(null, null, null, null, 1.1, 2.2);

    $intervention = new Intervention(
      'intervention-description',
      Priority::Normal,
      $type,
      $location
    );
    $intervention->setCreatedAt(new \DateTimeImmutable('2024-01-01'));
    $intervention->setTitle('Intervention');
    $intervention->setEmployer($employer);
    $intervention->addParticipant($user);
    $intervention->setLogicId(42);
    $intervention->setAuthor($user);
    $intervention->setFrequencyInterval(1);
    $intervention->setFrequency(Frequency::ONE_TIME);
    $intervention->setStatus(Status::ToDo);

    $comment = new Comment('comment-message', $intervention);
    $comment->setAuthor($user);
    $intervention->addComment($comment);

    $picture = new Picture('picture-file', PictureTag::Before, null, $intervention);
    $intervention->addPicture($picture);

    $intervention2 = new Intervention(
      'intervention2-description',
      Priority::Normal,
      $type,
      $location
    );
    $intervention2->setCreatedAt(new \DateTimeImmutable('2024-02-01'));
    $intervention2->setTitle('Intervention 2');
    $intervention2->setEmployer($employer);
    $intervention2->addParticipant($user);
    $intervention2->addComment($comment);
    $intervention2->addPicture($picture);
    $intervention2->setLogicId(43);
    $intervention2->setAuthor($user);
    $intervention2->setFrequency(Frequency::ONE_TIME);
    $intervention2->setFrequencyInterval(1);
    $intervention2->setStatus(Status::ToDo);

    $entityManager->persist($category);
    $entityManager->persist($type);
    $entityManager->persist($user);
    $entityManager->persist($employer);
    $entityManager->persist($location);
    $entityManager->persist($intervention);
    $entityManager->persist($intervention2);
    $entityManager->persist($comment);
    $entityManager->persist($picture);
    $entityManager->flush();
  }

  /**
   * Test qu'une collection d'interventions puissent être renvoyée.
   */
  public function testCanGetAnInterventionCollection(): void
  {
    $client = static::createClient();

    $this->addInterventions();

    $user = static::$kernel
      ->getContainer()
      ->get('doctrine')
      ->getManager()
      ->getRepository(User::class)
      ->find(1);
    $apiResponse = $client->request('GET', '/api/employers/1/interventions', ['auth_bearer' => $this->getJWT(login: $user->getLogin(), password: $user->getLogin())]);

    self::assertSame(200, $apiResponse->getStatusCode(), 'GET "/interventions" failed.');
    self::assertJson($apiResponse->getContent());

    $collection = json_decode($apiResponse->getContent(), false);
    $hydraMember = 'hydra:member';
    self::assertIsArray($collection->$hydraMember);
    $interventions = $collection->$hydraMember;

    self::assertCount(2, $interventions);

    foreach ($interventions as $intervention) {
      $this->assertInterventionIsComplete($intervention);
    }
  }

  /**
   * Les assertions de l'intervention.
   * @param object $intervention l'intervention.
   */
  private function assertInterventionIsComplete(object $intervention): void
  {
    self::assertCount(25, (array)$intervention, 'Incorrect count of intervention data has been returned.');

    self::assertIsInt($intervention->id);
    self::assertIsString($intervention->createdAt);

    $this->assertLocationIsComplete($intervention->location);
    $this->assertPriorityIsComplete($intervention->priority);
    $this->assertStatusIsComplete($intervention->status);

    self::assertIsArray($intervention->participants);

    foreach ($intervention->participants as $participant) {
      $this->assertParticipantIsComplete($participant);
    }

    self::assertIsArray($intervention->pictures);

    foreach ($intervention->pictures as $picture) {
      $this->assertPictureIsComplete($picture);
    }

    $atId = '@id';
    self::assertSame('/api/interventions/' . $intervention->id, $intervention->$atId);
    $atType = '@type';
    self::assertSame('Intervention', $intervention->$atType);
  }

  /**
   * Les assertions de la catégorie.
   * @param object $category la catégorie.
   */
  private function assertCategoryIsComplete(object $category): void
  {
    self::assertCount(3, (array)$category, 'Incorrect count of catergory data has been returned.');

    self::assertIsString($category->name);

    $atId = '@id';
    self::assertIsString($category->$atId);
    $atType = '@type';
    self::assertSame('Category', $category->$atType);
  }

  /**
   * Les assertions de la localisation.
   * @param object $location la localisation.
   */
  private function assertLocationIsComplete(object $location): void
  {
    self::assertCount(10, (array)$location, 'Incorrect count of location data has been returned.');

    self::assertIsFloat($location->longitude);
    self::assertIsFloat($location->latitude);

    $atId = '@id';
    self::assertIsString($location->$atId);
    $atType = '@type';
    self::assertSame('Location', $location->$atType);
  }

  /**
   * Les assertions de la priorité.
   * @param string $priority la priorité.
   */
  private function assertPriorityIsComplete(string $priority): void
  {
    self::assertSame(Priority::Normal->value, $priority, 'Incorrect count of priority data has been returned.');
  }

  /**
   * Les assertions du statut.
   * @param string $status le statut.
   */
  private function assertStatusIsComplete(string $status): void
  {
    self::assertSame(Status::ToDo->value, $status, 'Incorrect count of status data has been returned.');
  }

  /**
   * Les assertions de l'intervenant.
   * @param object $participant l'intervenant.
   */
  private function assertParticipantIsComplete(object $participant): void
  {
    self::assertCount(8, (array)$participant, 'Incorrect count of user data has been returned.');

    self::assertIsString($participant->firstname);
    self::assertIsString($participant->lastname);
    self::assertNull($participant->picture);

    $atId = '@id';
    self::assertIsString($participant->$atId);
    $atType = '@type';
    self::assertSame('User', $participant->$atType);
  }

  /**
   * Les assertions de la photo.
   * @param object $picture la photo.
   */
  private function assertPictureIsComplete(object $picture): void
  {
    self::assertCount(9, (array)$picture, 'Incorrect count of picture data has been returned.');

    self::assertIsString($picture->fileName);

    $atId = '@id';
    self::assertIsString($picture->$atId);
    $atType = '@type';
    self::assertSame('Picture', $picture->$atType);
  }

  /**
   * Test qu'une collection d'interventions puissent être renvoyée.
   */
  public function testCanGetAnInterventionExportCollection(): void
  {
    $client = static::createClient();

    $this->addInterventions();

    $user = static::$kernel
      ->getContainer()
      ->get('doctrine')
      ->getManager()
      ->getRepository(User::class)
      ->find(1);
    $apiResponse = $client->request('GET', '/api/employers/' . $user->getEmployer()->getId() . '/interventions/export', ['auth_bearer' => $this->getJWT(login: $user->getLogin(), password: $user->getLogin()), 'headers' => ['Accept' => 'text/csv']]);

    self::assertSame(200, $apiResponse->getStatusCode(), 'GET "/interventions" failed.');
//    self::assert($apiResponse->getContent());

    $csv = $apiResponse->getContent();
    self::assertIsString($csv);
    self::assertEquals(3, substr_count($csv, "\n"));
    $this->assertMatchesTextSnapshot($csv);
  }


  /**
   * Test qu'une collection d'interventions puissent être renvoyée
   * au format XLSX
   */
  public function testCanGetAnInterventionExportXLSXCollection(): void
  {
    $client = static::createClient();

    $this->addInterventions();

    $user = static::$kernel
      ->getContainer()
      ->get('doctrine')
      ->getManager()
      ->getRepository(User::class)
      ->find(1);
    $apiResponse = $client->request(
      'GET',
      '/api/employers/1/interventions/export',
      [
        'auth_bearer' => $this->getJWT(login: $user->getLogin(), password: $user->getLogin()),
        'headers' => ['Accept' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet']
      ]
    );

    self::assertSame(200, $apiResponse->getStatusCode(), 'GET "/interventions" failed.');

    $xlsx = $apiResponse->getContent();
    self::assertIsString($xlsx);
  }
}
