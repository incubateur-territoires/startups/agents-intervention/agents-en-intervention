<?php

declare(strict_types=1);

namespace App\Tests\Api\Interventions;

use App\Encoder\MultipartDecoder;
use App\Entity\Category;
use App\Entity\Comment;
use App\Entity\Employer;
use App\Entity\Intervention;
use App\Entity\Location;
use App\Entity\Priority;
use App\Entity\Role;
use App\Entity\Status;
use App\Entity\Type;
use App\Entity\User;
use App\Logger\UserProcessor;
use App\Repository\UserRepository;
use App\Security\InterventionVoter;
use App\Security\UserVoter;
use App\Serializer\UploadedFileDenormalizer;
use App\State\InterventionProcessor;
use App\State\UserProvider;
use App\Tests\Api\PermissionCheck;
use App\Tests\Api\PermissionsTestCase;
use PHPUnit\Framework\Attributes as PA;
use Zenstruck\Foundry\Test\Factories;
use Zenstruck\Foundry\Test\ResetDatabase;

/**
 * Test POST /api/interventions.
 */
#[
  PA\CoversClass(Intervention::class),
  PA\UsesClass(Category::class),
  PA\UsesClass(Employer::class),
  PA\UsesClass(Location::class),
  PA\UsesClass(Priority::class),
  PA\UsesClass(Role::class),
  PA\UsesClass(Status::class),
  PA\UsesClass(Type::class),
  PA\UsesClass(User::class),
  PA\UsesClass(UserRepository::class),
  PA\UsesClass(MultipartDecoder::class),
  PA\UsesClass(UserProcessor::class),
  PA\UsesClass(InterventionVoter::class),
  PA\UsesClass(UserVoter::class),
  PA\UsesClass(UploadedFileDenormalizer::class),
  PA\UsesClass(InterventionProcessor::class),
  PA\UsesClass(UserProvider::class),
  PA\Group('api'),
  PA\Group('api_interventions'),
  PA\Group('api_interventions_post'),
  PA\Group('intervention')
]
final class InterventionPostTest extends PermissionsTestCase
{
  use ResetDatabase, Factories;

  #[\Override]
  public function setUp(): void
  {
    parent::setUp();

    $entityManager = static::getContainer()->get('doctrine')->getManager();
    $cat = new Category();
    $cat->setName('cat-n');
    $cat->setDescription('cat-d');
    $cat->setPicture('cat.png');
    $entityManager->persist($cat);

    $type = new Type();
    $type->setName('type-n');
    $type->setDescription('type-d');
    $type->setPicture('type.png');
    $type->setCategory($cat);
    $entityManager->persist($type);

    $employer = Employer::new('SireN', 'emp-name', 42, 4);
    $entityManager->persist($employer);
    $location = new Location('street', 'rest', '09876', 'city', 42, 4);
    $entityManager->persist($location);

    $intervention = new Intervention(
      'intervention-description',
      Priority::Normal,
      $type,
      $location
    );
    $intervention->setTitle('Intervention');
    $intervention->setEmployer($employer);
    $intervention->addParticipant(static::$user);
    $intervention->setLogicId(42);
    $intervention->setAuthor(static::$user);
    $intervention->setStatus(Status::ToDo);

    $comment = new Comment('comment-message', $intervention);
    $comment->setAuthor(static::$user);
    $intervention->addComment($comment);

    $entityManager->persist($intervention);
    $entityManager->persist($comment);
    $entityManager->flush();
  }

  /**
   * Test que la route nécessite d'être authentifié.
   */
  public function testNeedsAuthentication(): void
  {
    $client = static::createClient();

    $apiResponse = $client->request('POST', '/api/interventions', [
      'headers' => [
        'Content-Type' => 'application/json',
      ],
    ]);

    self::assertSame(401, $apiResponse->getStatusCode(), 'POST "/api/interventions" succeeded.');
  }

  /**
   * Renvoie les données de l'intervention.
   * @return array Les données de l'intervention.
   */
  private function interventionToPost(): array
  {
    $createdAt = new \DateTimeImmutable('2023-01-01T00:00:00+00:00');

    return [
      'title' => 'intervention-title',
      'createdAt' => $createdAt->format(\DateTimeInterface::ATOM),
      'description' => 'intervention-description',
      'location' => [
        'street' => 'location-street',
        'rest' => 'location-rest',
        'postcode' => '75000',
        'city' => 'location-city',
        'longitude' => '1.234567',
        'latitude' => '1.23456',
      ],
      'participants' => ['api/users/2', 'api/users/3'],
      'priority' => Priority::Normal,
      'type' => 'api/types/1',
      'status' => Status::ToDo,
    ];
  }

  /**
   * Test qu'une intervention puisse être enregistrée.
   */
  public function testCanPostAnIntervention(): void
  {
    $check = new PermissionCheck(static::$employer_1, Role::Director, 201, '/api/interventions', $this->interventionToPost());
    $apiResponse = $this->checkPermissionObject($check);

    self::assertSame(201, $apiResponse->getStatusCode(), 'POST to "/api/interventions" failed.');
    self::assertJson($apiResponse->getContent());

    $jsonResponse = json_decode($apiResponse->getContent(), false);

    self::assertIsInt($jsonResponse->id);
  }

  /**
   * Test qu'une intervention puisse être enregistrée
   * avec une description vide.
   */
  public function testCanPostAnInterventionWithABlankDescription(): void
  {
    $client = static::createClient();

    $interventionDatas = $this->interventionToPost();
    unset($interventionDatas['description']);

    $apiResponse = $client->request('POST', '/api/interventions', [
      'headers' => [
        'Content-Type' => 'application/json',
        'authorization' => 'Bearer ' . $this->getJWT(),
      ],
      'json' => $this->interventionToPost(),
    ]);

    self::assertSame(201, $apiResponse->getStatusCode(), 'POST to "/api/interventions" failed.');
    self::assertJson($apiResponse->getContent());

    $jsonResponse = json_decode($apiResponse->getContent(), false);

    self::assertIsInt($jsonResponse->id);
  }

  /**
   * Test que l'on ne puisse pas ajouter
   * une intervention avec un statut inconnu.
   */
  public function testCanNotPostAnInterventionWithANonExistantStatus(): void
  {
    $client = static::createClient();

    $interventionDatas = $this->interventionToPost();
    $interventionDatas['status'] = 'non-existant-status';

    $apiResponse = $client->request('POST', '/api/interventions', [
      'headers' => [
        'Content-Type' => 'application/json',
        'authorization' => 'Bearer ' . $this->getJWT(),
      ],
      'json' => $interventionDatas,
    ]);

    self::assertSame(400, $apiResponse->getStatusCode(), 'POST to "/api/interventions" did not failed.');
  }

  /**
   * Test que l'on ne puisse pas ajouter
   * une intervention avec un employeur inconnu.
   */
  public function testCanNotPostAnInterventionWithANonExistantEmployer(): void
  {
    $client = static::createClient();

    $interventionDatas = $this->interventionToPost();
    $interventionDatas['employer'] = '/employers/999';

    $apiResponse = $client->request('POST', '/api/interventions', [
      'headers' => [
        'Content-Type' => 'application/json',
        'authorization' => 'Bearer ' . $this->getJWT(),
      ],
      'json' => $interventionDatas,
    ]);

    self::assertSame(400, $apiResponse->getStatusCode(), 'POST to "/api/interventions" did not failed.');
  }

  /**
   * Test que l'on ne puisse pas ajouter
   * une intervention avec une priorité inconnue.
   */
  public function testCanNotPostAnInterventionWithANonExistantPriority(): void
  {
    $client = static::createClient();

    $interventionDatas = $this->interventionToPost();
    $interventionDatas['priority'] = 'unknown-priority';

    $apiResponse = $client->request('POST', '/api/interventions', [
      'headers' => [
        'Content-Type' => 'application/json',
        'authorization' => 'Bearer ' . $this->getJWT(),
      ],
      'json' => $interventionDatas,
    ]);

    self::assertSame(400, $apiResponse->getStatusCode(), 'POST to "/api/interventions" did not failed.');
  }

  /**
   * Test que l'on ne puisse pas ajouter
   * une intervention avec un type inconnu.
   */
  public function testCanNotPostAnInterventionWithANonExistantType(): void
  {
    $client = static::createClient();

    $interventionDatas = $this->interventionToPost();
    $interventionDatas['type'] = '/types/999';

    $apiResponse = $client->request('POST', '/api/interventions', [
      'headers' => [
        'Content-Type' => 'application/json',
        'authorization' => 'Bearer ' . $this->getJWT(),
      ],
      'json' => $interventionDatas,
    ]);

    self::assertSame(400, $apiResponse->getStatusCode(), 'POST to "/api/interventions" did not failed.');
  }

  /**
   * Test que l'on ne puisse pas ajouter
   * une intervention avec une rue
   * de plus de 100 caractères.
   */
  public function testCanNotPostAnInterventionWithAnOver100CharactersStreet(): never
  {
    $this->markTestSkipped();
    $client = static::createClient();

    $interventionDatas = $this->interventionToPost();
    $interventionDatas['location']['street'] = '';

    for ($caracterCount = 1; $caracterCount < 102; $caracterCount++) {
      $interventionDatas['location']['street'] .= 'a';
    }

    $apiResponse = $client->request('POST', '/api/interventions', [
      'headers' => [
        'Content-Type' => 'application/json',
        'authorization' => 'Bearer ' . $this->getJWT(),
      ],
      'json' => $interventionDatas,
    ]);

    self::assertSame(422, $apiResponse->getStatusCode(), 'POST to "/api/interventions" did not failed.');
    self::assertJson($apiResponse->getContent(false));

    $jsonResponse = json_decode((string) $apiResponse->getContent(false), false);

    self::assertIsArray($jsonResponse->violations);
    self::assertCount(1, $jsonResponse->violations);
    self::assertArrayHasKey(0, $jsonResponse->violations);
    self::assertSame('location.street', $jsonResponse->violations[0]->propertyPath);
    self::assertSame('Le nom de la rue ne doit pas être plus long que 100 caractères.', $jsonResponse->violations[0]->message);
  }

  /**
   * Test que l'on ne puisse pas ajouter
   * une intervention avec un complément d'adresse
   * de plus de 500 caractères.
   */
  public function testCanNotPostAnInterventionWithAnOver500CharactersRest(): void
  {
    $client = static::createClient();

    $interventionDatas = $this->interventionToPost();
    $interventionDatas['location']['rest'] = '';

    for ($caracterCount = 1; $caracterCount < 502; $caracterCount++) {
      $interventionDatas['location']['rest'] .= 'a';
    }

    $apiResponse = $client->request('POST', '/api/interventions', [
      'headers' => [
        'Content-Type' => 'application/json',
        'authorization' => 'Bearer ' . $this->getJWT(),
      ],
      'json' => $interventionDatas,
    ]);

    self::assertSame(422, $apiResponse->getStatusCode(), 'POST to "/api/interventions" did not failed.');
    self::assertJson($apiResponse->getContent(false));

    $jsonResponse = json_decode($apiResponse->getContent(false), false);

    self::assertIsArray($jsonResponse->violations);
    self::assertCount(1, $jsonResponse->violations);
    self::assertArrayHasKey(0, $jsonResponse->violations);
    self::assertSame('location.rest', $jsonResponse->violations[0]->propertyPath);
    self::assertSame('Le complément d\'adresse ne doit pas être plus long que 500 caractères.', $jsonResponse->violations[0]->message);
  }

  /**
   * Renvoie des codes postaux invalides.
   * @return string[] des codes postaux invalides.
   */
  public static function getInvalidPostcodes(): array
  {
    return [
      'letters only' => ['aaaaa'],
      'alphanumeric characters' => ['aa111'],
      'too many digits' => ['111111'],
      'not enough digits' => ['1111'],
    ];
  }

  /**
   * Test que l'on ne puisse pas ajouter
   * une intervention avec un code postal invalide.
   * @param string $invalidPostcode un code postal invalide.
   */
  #[PA\DataProvider('getInvalidPostcodes')]
  public function testCanNotPostAnInvalidPostcode(string $invalidPostcode): never
  {
    $this->markTestSkipped('postcode assertion is disabled');
    $client = static::createClient();

    $interventionDatas = $this->interventionToPost();
    $interventionDatas['location']['postcode'] = $invalidPostcode;

    $apiResponse = $client->request('POST', '/api/interventions', [
      'headers' => [
        'Content-Type' => 'application/json',
        'authorization' => 'Bearer ' . $this->getJWT(),
      ],
      'json' => $interventionDatas,
    ]);

    self::assertSame(422, $apiResponse->getStatusCode(), 'POST to "/api/interventions" did not failed.');
    self::assertJson($apiResponse->getContent(false));

    $jsonResponse = json_decode((string) $apiResponse->getContent(false), false);

    self::assertIsArray($jsonResponse->violations);
    self::assertCount(1, $jsonResponse->violations);
    self::assertArrayHasKey(0, $jsonResponse->violations);
    self::assertSame('location.postcode', $jsonResponse->violations[0]->propertyPath);
    self::assertSame('Le code postal doit être constitué de 5 chiffres.', $jsonResponse->violations[0]->message);
  }

  /**
   * Test que l'on ne puisse pas ajouter
   * une intervention avec une ville
   * de plus de 163 caractères.
   */
  public function testCanNotPostAnInterventionWithAnOver163CharactersCity(): void
  {
    $client = static::createClient();

    $interventionDatas = $this->interventionToPost();
    $interventionDatas['location']['city'] = '';

    for ($caracterCount = 1; $caracterCount < 165; $caracterCount++) {
      $interventionDatas['location']['city'] .= 'a';
    }

    $apiResponse = $client->request('POST', '/api/interventions', [
      'headers' => [
        'Content-Type' => 'application/json',
        'authorization' => 'Bearer ' . $this->getJWT(),
      ],
      'json' => $interventionDatas,
    ]);

    self::assertSame(422, $apiResponse->getStatusCode(), 'POST to "/api/interventions" did not failed.');
    self::assertJson($apiResponse->getContent(false));

    $jsonResponse = json_decode($apiResponse->getContent(false), false);

    self::assertIsArray($jsonResponse->violations);
    self::assertCount(1, $jsonResponse->violations);
    self::assertArrayHasKey(0, $jsonResponse->violations);
    self::assertSame('location.city', $jsonResponse->violations[0]->propertyPath);
    self::assertSame('Le nom de la ville ne doit pas être plus long que 163 caractères.', $jsonResponse->violations[0]->message);
  }

  /**
   * Renvoie des longitudes hors limites.
   * @return string[] des longitudes hors limites.
   */
  public static function getNotInRangeLongitudes(): array
  {
    return [
      'too high' => ['180.1'],
      'too low' => ['-180.1'],
    ];
  }

  /**
   * Test que l'on ne puisse pas ajouter
   * une intervention avec une longitude hors limites.
   * @param string $notInRangeLongitude une longitude hors limites.
   */
  #[PA\DataProvider('getNotInRangeLongitudes')]
  public function testCanNotPostAnInvalidLongitude(string $notInRangeLongitude): void
  {
    $client = static::createClient();

    $interventionDatas = $this->interventionToPost();
    $interventionDatas['location']['longitude'] = $notInRangeLongitude;

    $apiResponse = $client->request('POST', '/api/interventions', [
      'headers' => [
        'Content-Type' => 'application/json',
        'authorization' => 'Bearer ' . $this->getJWT(),
      ],
      'json' => $interventionDatas,
    ]);

    self::assertSame(422, $apiResponse->getStatusCode(), 'POST to "/api/interventions" did not failed.');
    self::assertJson($apiResponse->getContent(false));

    $jsonResponse = json_decode($apiResponse->getContent(false), false);

    self::assertIsArray($jsonResponse->violations);
    self::assertCount(1, $jsonResponse->violations);
    self::assertArrayHasKey(0, $jsonResponse->violations);
    self::assertSame('location.longitude', $jsonResponse->violations[0]->propertyPath);
    self::assertSame('La longitude doit être comprise entre -180 et 180.', $jsonResponse->violations[0]->message);
  }

  /**
   * Renvoie des latitudes hors limites.
   * @return string[] des latitudes hors limites.
   */
  public static function getNotInRangeLatitudes(): array
  {
    return [
      'too high' => ['90.1'],
      'too low' => ['-90.1'],
    ];
  }

  /**
   * Test que l'on ne puisse pas ajouter
   * une intervention avec une latitude hors limites.
   * @param string $notInRangeLatitude une latitude hors limites.
   */
  #[PA\DataProvider('getNotInRangeLatitudes')]
  public function testCanNotPostAnInvalidLatitude(string $notInRangeLatitude): void
  {
    $client = static::createClient();

    $interventionDatas = $this->interventionToPost();
    $interventionDatas['location']['latitude'] = $notInRangeLatitude;

    $apiResponse = $client->request('POST', '/api/interventions', [
      'headers' => [
        'Content-Type' => 'application/json',
        'authorization' => 'Bearer ' . $this->getJWT(),
      ],
      'json' => $interventionDatas,
    ]);

    self::assertSame(422, $apiResponse->getStatusCode(), 'POST to "/api/interventions" did not failed.');
    self::assertJson($apiResponse->getContent(false));

    $jsonResponse = json_decode($apiResponse->getContent(false), false);

    self::assertIsArray($jsonResponse->violations);
    self::assertCount(1, $jsonResponse->violations);
    self::assertArrayHasKey(0, $jsonResponse->violations);
    self::assertSame('location.latitude', $jsonResponse->violations[0]->propertyPath);
    self::assertSame('La latitude doit être comprise entre -90 et 90.', $jsonResponse->violations[0]->message);
  }

  #[\Override]
  protected function getMethod(): string
  {
    return 'POST';
  }
}
