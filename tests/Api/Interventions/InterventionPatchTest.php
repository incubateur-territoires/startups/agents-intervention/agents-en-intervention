<?php

declare(strict_types=1);

namespace App\Tests\Api\Interventions;

use App\Entity\Category;
use App\Entity\Comment;
use App\Entity\Employer;
use App\Entity\Intervention;
use App\Entity\Location;
use App\Entity\Priority;
use App\Entity\Role;
use App\Entity\Status;
use App\Entity\Type;
use App\Entity\User;
use App\Repository\UserRepository;
use App\Tests\Api\AuthenticationTestCase;
use App\Tests\Api\PermissionCheck;
use App\Tests\Api\PermissionsTestCase;
use App\Tests\FixtureTrait;
use Doctrine\ORM\EntityManagerInterface;
use Nelmio\Alice\Throwable\LoadingThrowable;
use PHPUnit\Framework\Attributes as PA;
use Zenstruck\Foundry\Test\Factories;
use Zenstruck\Foundry\Test\ResetDatabase;

/**
 * Test PATCH /api/interventions/1.
 */
#[
  PA\CoversClass(Intervention::class),
  PA\UsesClass(Category::class),
  PA\UsesClass(Comment::class),
  PA\UsesClass(Employer::class),
  PA\UsesClass(Location::class),
  PA\UsesClass(Priority::class),
  PA\UsesClass(Role::class),
  PA\UsesClass(Status::class),
  PA\UsesClass(Type::class),
  PA\UsesClass(User::class),
  PA\UsesClass(UserRepository::class),
  PA\Group('api'),
  PA\Group('api_interventions'),
  PA\Group('api_interventions_patch'),
  PA\Group('intervention')
]
final class InterventionPatchTest extends PermissionsTestCase
{
  use ResetDatabase, Factories, FixtureTrait;

  /**
   * Test que la route nécessite d'être authentifié.
   */
  public function testNeedsAuthentication(): void
  {
    $client = static::createClient();

    $apiResponse = $client->request('PATCH', '/api/interventions/1');

    self::assertSame(401, $apiResponse->getStatusCode(), 'PATCH "/api/interventions/1" succeeded.');
  }

  /**
   * Ajoute une intervention.
   * @param EntityManagerInterface $entityManager le gestionnaire d'entité.
   */
  private function addIntervention(EntityManagerInterface $entityManager): Intervention
  {
    $cat = new Category();
    $cat->setName('cat-n');
    $cat->setDescription('cat-d');
    $cat->setPicture('cat.png');
    $entityManager->persist($cat);

    $type = new Type();
    $type->setName('type-n');
    $type->setDescription('type-d');
    $type->setPicture('type.png');
    $type->setCategory($cat);
    $entityManager->persist($type);

    $user = $entityManager->find(User::class, 1);
    $employer = Employer::new('SireN', 'emp-name', 42, 4);
    $entityManager->persist($employer);
    $location = new Location('street', 'rest', '09876', 'city', 42, 4);
    $entityManager->persist($location);

    $intervention = new Intervention(
      'intervention-description',
      Priority::Normal,
      $type,
      $location
    );
    $intervention->setTitle('Intervention');
    $intervention->setEmployer($employer);
    $intervention->addParticipant($user);
    $intervention->setLogicId(42);
    $intervention->setAuthor($user);
    $intervention->setStatus(Status::ToDo);

    $comment = new Comment('comment-message', $intervention);
    $comment->setAuthor($user);
    $intervention->addComment($comment);

    $entityManager->persist($intervention);
    $entityManager->persist($comment);
    $entityManager->flush();

    return $intervention;
  }

  /**
   * Renvoie les données de l'intervention.
   * @return array Les données de l'intervention.
   */
  private function interventionToPatch(): array
  {
    $doctrine = self::getContainer()->get('doctrine');

    $type = new Type();
    $type->setName('type-2');
    $type->setDescription('type-2');
    $type->setPicture('type-2.png');
    $cat = (new Category())->setName('cat-n2')->setDescription('cat-n2')->setPicture('cat-n2.png');
    $type->setCategory($cat);
    $doctrine->getManager()->persist($type);
    $doctrine->getManager()->persist($cat);
    $doctrine->getManager()->flush();

    return [
      'category' => 'api/categories/' . $cat->getId(),
      'description' => 'intervention-description-2',
      'location' => [
        'street' => 'location-street-2',
        'rest' => 'location-rest-2',
        'postcode' => '75002',
        'city' => 'location-city-2',
        'longitude' => '2.345678',
        'latitude' => '2.34567',
      ],
      'participants' => ['api/users/4', 'api/users/5'],
      'priority' => Priority::Urgent,
      'type' => 'api/types/' . $type->getId(),
      'status' => Status::ToDo,
    ];
  }

  /**
   * Test qu'une intervention puisse être complètement mise à jour.
   */
  public function testCanPatchAFullIntervention(): void
  {
    $doctrine = static::$kernel->getContainer()->get('doctrine');
//    $this->addFixtures($doctrine->getManager());
    $i = $this->addIntervention($doctrine->getManager());

    $check = new PermissionCheck(self::$employer_anct, Role::Admin, 200, '/api/interventions/' . $i->getId());
    $apiResponse = $this->checkPermissionObject($check, $this->interventionToPatch());

    self::assertSame(200, $apiResponse->getStatusCode(), 'PATCH to "/api/interventions/1" failed.');
    self::assertNotEmpty($apiResponse->getContent(), 'The response must not have a body.');
  }

  /**
   * Test que l'on puisse mettre à jour seulement
   * l'auteur d'une intervention.
   */
  public function testCanNotPatchOnlyTheAuthorOfAnIntervention(): void
  {
    $doctrine = static::$kernel->getContainer()->get('doctrine');
    $i = $this->addIntervention($doctrine->getManager());
    $interventionDatas = ['author' => '/users/2'];

    $user = self::$user;
    $check = new PermissionCheck($user->getEmployer(), Role::Admin, 200, '/api/interventions/' . $i->getId());
    $apiResponse = $this->checkPermissionObject($check, $interventionDatas);

    self::assertSame(200, $apiResponse->getStatusCode(), 'PATCH to "/api/interventions/1" failed.');
    self::assertNotEmpty($apiResponse->getContent(), 'The response must not have a body.');
    $content = json_decode($apiResponse->getContent(), false);
    self::assertNotSame(2, $content->author->id);
  }

  /**
   * Test que l'on puisse mettre à jour seulement
   * la description d'une intervention.
   */
  public function testCanPatchOnlyTheDescriptionOfAnIntervention(): void
  {
    $doctrine = static::$kernel->getContainer()->get('doctrine');
    $i = $this->addIntervention($doctrine->getManager());
    $interventionDatas = ['description' => 'intervention-description-2'];

    $check = new PermissionCheck(self::$employer_anct, Role::Admin, 200, '/api/interventions/' . $i->getId());
    $apiResponse = $this->checkPermissionObject($check, $interventionDatas);

    self::assertSame(200, $apiResponse->getStatusCode(), 'PATCH to "/api/interventions/1" failed.');
    self::assertNotEmpty($apiResponse->getContent(), 'The response must not have a body.');
    $content = json_decode($apiResponse->getContent(), false);
    self::assertSame('intervention-description-2', $content->description);
  }

  /**
   * Test que l'on puisse mettre à jour seulement
   * la description d'une intervention.
   */
  public function testCanPatchOnlyTheDescriptionOfAnInterventionWithBlank(): void
  {
    $doctrine = static::$kernel->getContainer()->get('doctrine');
    $i = $this->addIntervention($doctrine->getManager());
    $interventionDatas = ['description' => null];

    $check = new PermissionCheck(self::$employer_anct, Role::Admin, 200, '/api/interventions/' . $i->getId());
    $apiResponse = $this->checkPermissionObject($check, $interventionDatas);

    self::assertSame(200, $apiResponse->getStatusCode(), 'PATCH to "/api/interventions/1" failed.');
    self::assertNotEmpty($apiResponse->getContent(), 'The response must not have a body.');

    $content = json_decode($apiResponse->getContent(), false);
    self::assertNull($content->description);
  }

  /**
   * Test que l'on puisse mettre à jour seulement
   * la localisation d'une intervention.
   */
  public function testCanPatchOnlyTheLocationOfAnIntervention(): void
  {
    $doctrine = static::$kernel->getContainer()->get('doctrine');
    $i = $this->addIntervention($doctrine->getManager());

    $interventionDatas = [
      'location' => [
        'street' => 'location-street-2',
        'rest' => 'location-rest-2',
        'postcode' => '75002',
        'city' => 'location-city-2',
        'longitude' => '2.345678',
        'latitude' => '2.34567',
      ],
    ];

    $check = new PermissionCheck(self::$employer_anct, Role::Admin, 200, '/api/interventions/' . $i->getId());
    $apiResponse = $this->checkPermissionObject($check, $interventionDatas);

    self::assertSame(200, $apiResponse->getStatusCode(), 'PATCH to "/api/interventions/1" failed.');
    self::assertNotEmpty($apiResponse->getContent(), 'The response must not have a body.');
    $content = json_decode($apiResponse->getContent(), false);
    self::assertSame('location-street-2', $content->location->street);
  }

  /**
   * Test que l'on puisse mettre à jour seulement
   * les participants d'une intervention.
   */
  public function testCanPatchOnlyTheParticipantsOfAnIntervention(): void
  {
    $doctrine = static::$kernel->getContainer()->get('doctrine');
    $i = $this->addIntervention($doctrine->getManager());
    $interventionDatas = [
      'participants' => ['api/users/4', 'api/users/5'],
    ];

    $check = new PermissionCheck(self::$employer_anct, Role::Admin, 200, '/api/interventions/' . $i->getId());
    $apiResponse = $this->checkPermissionObject($check, $interventionDatas);

    self::assertSame(200, $apiResponse->getStatusCode(), 'PATCH to "/api/interventions/1" failed.');
    self::assertNotEmpty($apiResponse->getContent(), 'The response must not have a body.');
    $content = json_decode($apiResponse->getContent(), false);
    self::assertCount(2, $content->participants);
  }

  /**
   * Test que l'on puisse mettre à jour seulement
   * la priorité d'une intervention.
   */
  public function testCanPatchOnlyThePriorityOfAnIntervention(): void
  {
    $doctrine = static::$kernel->getContainer()->get('doctrine');
    $i = $this->addIntervention($doctrine->getManager());
    $interventionDatas = ['priority' => Priority::Urgent];

    $check = new PermissionCheck(self::$employer_anct, Role::Admin, 200, '/api/interventions/' . $i->getId());
    $apiResponse = $this->checkPermissionObject($check, $interventionDatas);

    self::assertSame(200, $apiResponse->getStatusCode(), 'PATCH to "/api/interventions/1" failed.');
    self::assertNotEmpty($apiResponse->getContent(), 'The response must not have a body.');
    $content = json_decode($apiResponse->getContent(), false);
    self::assertSame(Priority::Urgent->value, $content->priority);
  }

  /**
   * Test que l'on puisse mettre à jour seulement
   * le type d'une intervention.
   */
  public function testCanPatchOnlyTheTypeOfAnIntervention(): void
  {
    $doctrine = static::$kernel->getContainer()->get('doctrine');
    $i = $this->addIntervention($doctrine->getManager());

    $type = new Type();
    $type->setName('type-2');
    $type->setDescription('type-2');
    $type->setPicture('type-2.png');
    $cat = (new Category())->setName('cat-n2')->setDescription('cat-n2')->setPicture('cat-n2.png');
    $type->setCategory($cat);
    $doctrine->getManager()->persist($type);
    $doctrine->getManager()->persist($cat);
    $doctrine->getManager()->flush();

    $interventionDatas = ['type' => 'api/types/' . $type->getId()];

    $check = new PermissionCheck(self::$employer_anct, Role::Admin, 200, '/api/interventions/' . $i->getId());
    $apiResponse = $this->checkPermissionObject($check, $interventionDatas);


    self::assertSame(200, $apiResponse->getStatusCode(), 'PATCH to "/api/interventions/1" failed.');
    self::assertNotEmpty($apiResponse->getContent(), 'The response must not have a body.');
    $content = json_decode($apiResponse->getContent(), false);
    self::assertSame($type->getId(), $content->type->id);
  }

  /**
   * Test que l'on puisse mettre à jour seulement
   * le statut d'une intervention.
   */
  public function testCanPatchOnlyTheStatusOfAnIntervention(): void
  {
    $doctrine = static::$kernel->getContainer()->get('doctrine');
    $i = $this->addIntervention($doctrine->getManager());
    $interventionDatas = ['status' => Status::InProgress];

    $check = new PermissionCheck(self::$employer_anct, Role::Admin, 200, '/api/interventions/' . $i->getId());
    $apiResponse = $this->checkPermissionObject($check, $interventionDatas);

    self::assertSame(200, $apiResponse->getStatusCode(), 'PATCH to "/api/interventions/1" failed.');
    self::assertNotEmpty($apiResponse->getContent(), 'The response must not have a body.');
    $content = json_decode($apiResponse->getContent(), false);
    self::assertSame(Status::InProgress->value, $content->status);
  }

  /**
   * Test que l'on ne puisse pas ajouter
   * une intervention avec un statut inconnu.
   */
  public function testCanNotPatchAnInterventionWithANonExistantStatus(): void
  {
    $doctrine = static::$kernel->getContainer()->get('doctrine');
    $i = $this->addIntervention($doctrine->getManager());

    $interventionDatas = $this->interventionToPatch();
    $interventionDatas['status'] = 'non-existant-status';

    $check = new PermissionCheck(self::$employer_anct, Role::Admin, 400, '/api/interventions/' . $i->getId());
    $apiResponse = $this->checkPermissionObject($check, $interventionDatas);

    self::assertSame(400, $apiResponse->getStatusCode(), 'PATCH to "/api/interventions/1" did not failed.');
  }

  /**
   * Test que l'on ne puisse pas ajouter
   * une intervention avec une priorité inconnue.
   */
  public function testCanNotPatchAnInterventionWithANonExistantPriority(): void
  {
    $doctrine = static::$kernel->getContainer()->get('doctrine');
    $i = $this->addIntervention($doctrine->getManager());
    $interventionDatas = $this->interventionToPatch();
    $interventionDatas['priority'] = 'non-existant-priority';

    $check = new PermissionCheck(self::$employer_anct, Role::Admin, 400, '/api/interventions/' . $i->getId());
    $apiResponse = $this->checkPermissionObject($check, $interventionDatas);

    self::assertSame(400, $apiResponse->getStatusCode(), 'PATCH to "/api/interventions/1" did not failed.');
  }

  /**
   * Test que l'on ne puisse pas ajouter
   * une intervention avec un type inconnu.
   */
  public function testCanNotPatchAnInterventionWithANonExistantType(): void
  {
    $doctrine = static::$kernel->getContainer()->get('doctrine');
    $i = $this->addIntervention($doctrine->getManager());

    $interventionDatas = $this->interventionToPatch();
    $interventionDatas['type'] = '/types/999';

    $check = new PermissionCheck(self::$employer_anct, Role::Admin, 400, '/api/interventions/' . $i->getId());
    $apiResponse = $this->checkPermissionObject($check, $interventionDatas);

    self::assertSame(400, $apiResponse->getStatusCode(), 'PATCH to "/api/interventions/1" did not failed.');
  }

  /**
   * Test que l'on ne puisse pas ajouter
   * une intervention avec une rue
   * de plus de 100 caractères.
   */
  public function testCanNotPatchAnInterventionWithAnOver100CharactersStreet(): void
  {
    $doctrine = static::$kernel->getContainer()->get('doctrine');
    $i = $this->addIntervention($doctrine->getManager());

    $interventionDatas = $this->interventionToPatch();
    $interventionDatas['location']['street'] = '';

    for ($caracterCount = 1; $caracterCount < 102; $caracterCount++) {
      $interventionDatas['location']['street'] .= 'a';
    }

    $check = new PermissionCheck(self::$employer_anct, Role::Admin, 422, '/api/interventions/' . $i->getId());
    $apiResponse = $this->checkPermissionObject($check, $interventionDatas);

    self::assertSame(422, $apiResponse->getStatusCode(), 'PATCH to "/api/interventions/1" did not failed.');
    self::assertJson($apiResponse->getContent(false));

    $jsonResponse = json_decode($apiResponse->getContent(false), false);

    self::assertIsArray($jsonResponse->violations);
    self::assertCount(1, $jsonResponse->violations);
    self::assertArrayHasKey(0, $jsonResponse->violations);
    self::assertSame('location.street', $jsonResponse->violations[0]->propertyPath);
    self::assertSame('Le nom de la rue ne doit pas être plus long que 100 caractères.', $jsonResponse->violations[0]->message);
  }

  /**
   * Test que l'on ne puisse pas ajouter
   * une intervention avec un complément d'adresse
   * de plus de 500 caractères.
   * @throws LoadingThrowable
   */
  public function testCanNotPatchAnInterventionWithAnOver500CharactersRest(): void
  {
    $doctrine = static::$kernel->getContainer()->get('doctrine');
    $i = $this->addIntervention($doctrine->getManager());

    $interventionDatas = $this->interventionToPatch();
    $interventionDatas['location']['rest'] = '';

    for ($caracterCount = 1; $caracterCount < 502; $caracterCount++) {
      $interventionDatas['location']['rest'] .= 'a';
    }

    $check = new PermissionCheck(self::$employer_anct, Role::Admin, 422, '/api/interventions/' . $i->getId());
    $apiResponse = $this->checkPermissionObject($check, $interventionDatas);

    self::assertSame(422, $apiResponse->getStatusCode(), 'PATCH to "/api/interventions/1" did not failed.');
    self::assertJson($apiResponse->getContent(false));

    $jsonResponse = json_decode($apiResponse->getContent(false), false);

    self::assertIsArray($jsonResponse->violations);
    self::assertCount(1, $jsonResponse->violations);
    self::assertArrayHasKey(0, $jsonResponse->violations);
    self::assertSame('location.rest', $jsonResponse->violations[0]->propertyPath);
    self::assertSame('Le complément d\'adresse ne doit pas être plus long que 500 caractères.', $jsonResponse->violations[0]->message);
  }

  /**
   * Renvoie des codes patchaux invalides.
   * @return string[] des codes patchaux invalides.
   */
  public static function getInvalidPostcodes(): array
  {
    return [
      'letters only' => ['aaaaa'],
      'alphanumeric characters' => ['aa111'],
      'too many digits' => ['111111'],
      'not enough digits' => ['1111'],
    ];
  }

  /**
   * Test que l'on ne puisse pas ajouter
   * une intervention avec un code postal invalide.
   * @param string $invalidPostcode un code postal invalide.
   */
  #[PA\DataProvider('getInvalidPostcodes')]
  public function testCanNotPatchAnInvalidPostcode(string $invalidPostcode): never
  {
    $this->markTestSkipped('assertion commented in source code & not acccurate (Corse)');
    $client = static::createClient();

    $doctrine = static::$kernel->getContainer()->get('doctrine');

    $this->addIntervention($doctrine->getManager());

    $interventionDatas = $this->interventionToPatch();
    $interventionDatas['location']['postcode'] = $invalidPostcode;

    $user = $doctrine->getManager()->find(User::class, 1);
    $apiResponse = $client->request('PATCH', '/api/interventions/1', [
      'headers' => [
        'Content-Type' => 'application/merge-patch+json',
        'authorization' => 'Bearer ' . $this->getJWT(login: $user->getLogin(), password: $user->getLogin()),
      ],
      'json' => $interventionDatas,
    ]);

    self::assertSame(422, $apiResponse->getStatusCode(), 'PATCH to "/api/interventions/1" did not failed.');
    self::assertJson($apiResponse->getContent(false));

    $jsonResponse = json_decode((string) $apiResponse->getContent(false), false);

    self::assertIsArray($jsonResponse->violations);
    self::assertCount(1, $jsonResponse->violations);
    self::assertArrayHasKey(0, $jsonResponse->violations);
    self::assertSame('location.postcode', $jsonResponse->violations[0]->propertyPath);
    self::assertSame('Le code postal doit être constitué de 5 chiffres.', $jsonResponse->violations[0]->message);
  }

  /**
   * Test que l'on ne puisse pas ajouter
   * une intervention avec une ville
   * de plus de 163 caractères.
   */
  public function testCanNotPatchAnInterventionWithAnOver163CharactersCity(): void
  {
    $doctrine = static::$kernel->getContainer()->get('doctrine');
    $i = $this->addIntervention($doctrine->getManager());

    $interventionDatas = $this->interventionToPatch();
    $interventionDatas['location']['city'] = '';

    for ($caracterCount = 1; $caracterCount < 165; $caracterCount++) {
      $interventionDatas['location']['city'] .= 'a';
    }

    $check = new PermissionCheck(self::$employer_anct, Role::Admin, 422, '/api/interventions/' . $i->getId());
    $apiResponse = $this->checkPermissionObject($check, $interventionDatas);

    self::assertSame(422, $apiResponse->getStatusCode(), 'PATCH to "/api/interventions/1" did not failed.');
    self::assertJson($apiResponse->getContent(false));

    $jsonResponse = json_decode($apiResponse->getContent(false), false);

    self::assertIsArray($jsonResponse->violations);
    self::assertCount(1, $jsonResponse->violations);
    self::assertArrayHasKey(0, $jsonResponse->violations);
    self::assertSame('location.city', $jsonResponse->violations[0]->propertyPath);
    self::assertSame('Le nom de la ville ne doit pas être plus long que 163 caractères.', $jsonResponse->violations[0]->message);
  }

  /**
   * Renvoie des longitudes hors limites.
   * @return string[] des longitudes hors limites.
   */
  public static function getNotInRangeLongitudes(): array
  {
    return [
      'too high' => ['180.1'],
      'too low' => ['-180.1'],
    ];
  }

  /**
   * Test que l'on ne puisse pas ajouter
   * une intervention avec une longitude hors limites.
   * @param string $notInRangeLongitude une longitude hors limites.
   */
  #[PA\DataProvider('getNotInRangeLongitudes')]
  public function testCanNotPatchAnInvalidLongitude(string $notInRangeLongitude): void
  {
    $doctrine = static::$kernel->getContainer()->get('doctrine');
    $i = $this->addIntervention($doctrine->getManager());

    $interventionDatas = $this->interventionToPatch();
    $interventionDatas['location']['longitude'] = $notInRangeLongitude;

    $check = new PermissionCheck(self::$employer_anct, Role::Admin, 422, '/api/interventions/' . $i->getId());
    $apiResponse = $this->checkPermissionObject($check, $interventionDatas);

    self::assertSame(422, $apiResponse->getStatusCode(), 'PATCH to "/api/interventions/1" did not failed.');
    self::assertJson($apiResponse->getContent(false));

    $jsonResponse = json_decode($apiResponse->getContent(false), false);

    self::assertIsArray($jsonResponse->violations);
    self::assertCount(1, $jsonResponse->violations);
    self::assertArrayHasKey(0, $jsonResponse->violations);
    self::assertSame('location.longitude', $jsonResponse->violations[0]->propertyPath);
    self::assertSame('La longitude doit être comprise entre -180 et 180.', $jsonResponse->violations[0]->message);
  }

  /**
   * Renvoie des latitudes hors limites.
   * @return string[] des latitudes hors limites.
   */
  public static function getNotInRangeLatitudes(): array
  {
    return [
      'too high' => ['90.1'],
      'too low' => ['-90.1'],
    ];
  }

  /**
   * Test que l'on ne puisse pas ajouter
   * une intervention avec une latitude hors limites.
   * @param string $notInRangeLatitude une latitude hors limites.
   */
  #[PA\DataProvider('getNotInRangeLatitudes')]
  public function testCanNotPatchAnInvalidLatitude(string $notInRangeLatitude): void
  {
    $doctrine = static::$kernel->getContainer()->get('doctrine');
    $i = $this->addIntervention($doctrine->getManager());

    $interventionDatas = $this->interventionToPatch();
    $interventionDatas['location']['latitude'] = $notInRangeLatitude;

    $check = new PermissionCheck(self::$employer_anct, Role::Admin, 422, '/api/interventions/' . $i->getId());
    $apiResponse = $this->checkPermissionObject($check, $interventionDatas);

    self::assertSame(422, $apiResponse->getStatusCode(), 'PATCH to "/api/interventions/1" did not failed.');
    self::assertJson($apiResponse->getContent(false));

    $jsonResponse = json_decode($apiResponse->getContent(false), false);

    self::assertIsArray($jsonResponse->violations);
    self::assertCount(1, $jsonResponse->violations);
    self::assertArrayHasKey(0, $jsonResponse->violations);
    self::assertSame('location.latitude', $jsonResponse->violations[0]->propertyPath);
    self::assertSame('La latitude doit être comprise entre -90 et 90.', $jsonResponse->violations[0]->message);
  }

  #[\Override]
  protected function getMethod(): string
  {
    return 'PATCH';
  }
}
