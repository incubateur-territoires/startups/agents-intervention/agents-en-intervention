<?php

declare(strict_types=1);

namespace App\Tests\Api\Interventions;

use App\Entity\Category;
use App\Entity\Comment;
use App\Entity\Employer;
use App\Entity\Intervention;
use App\Entity\Location;
use App\Entity\Priority;
use App\Entity\Role;
use App\Entity\Status;
use App\Entity\Type;
use App\Entity\User;
use App\Repository\UserRepository;
use App\Tests\Api\AuthenticationTestCase;
use App\Tests\Api\PermissionCheck;
use App\Tests\Api\PermissionsTestCase;
use App\Tests\FixtureTrait;
use Doctrine\ORM\EntityManagerInterface;
use Nelmio\Alice\Throwable\LoadingThrowable;
use PHPUnit\Framework\Attributes as PA;
use Zenstruck\Foundry\Test\Factories;
use Zenstruck\Foundry\Test\ResetDatabase;

/**
 * Test POST /api/interventions/1/restart.
 */
#[
  PA\CoversClass(Intervention::class),
  PA\UsesClass(Category::class),
  PA\UsesClass(Comment::class),
  PA\UsesClass(Employer::class),
  PA\UsesClass(Location::class),
  PA\UsesClass(Priority::class),
  PA\UsesClass(Role::class),
  PA\UsesClass(Status::class),
  PA\UsesClass(Type::class),
  PA\UsesClass(User::class),
  PA\UsesClass(UserRepository::class),
  PA\Group('api'),
  PA\Group('api_interventions'),
  PA\Group('api_interventions_patch'),
  PA\Group('intervention')
]
final class InterventionRestartTest extends PermissionsTestCase
{
  use ResetDatabase, Factories, FixtureTrait;

  /**
   * Test que la route nécessite d'être authentifié.
   */
  public function testNeedsAuthentication(): void
  {
    $client = static::createClient();

    $apiResponse = $client->request('POST', '/api/interventions/1/restart', ['headers' => ['Content-Type' => 'application/json']]);

    self::assertSame(401, $apiResponse->getStatusCode(), 'POST "/api/interventions/1/restart" succeeded.');
  }

  /**
   * Ajoute une intervention.
   * @param EntityManagerInterface $entityManager le gestionnaire d'entité.
   */
  private function addIntervention(EntityManagerInterface $entityManager, ?\DateTimeImmutable $startedAt = null): Intervention
  {
    $cat = new Category();
    $cat->setName('cat-n');
    $cat->setDescription('cat-d');
    $cat->setPicture('cat.png');
    $entityManager->persist($cat);

    $type = new Type();
    $type->setName('type-n');
    $type->setDescription('type-d');
    $type->setPicture('type.png');
    $type->setCategory($cat);
    $entityManager->persist($type);

    $user = $entityManager->find(User::class, 1);
    $employer = Employer::new('SireN', 'emp-name', 42, 4);
    $entityManager->persist($employer);
    $location = new Location('street', 'rest', '09876', 'city', 42, 4);
    $entityManager->persist($location);

    $intervention = new Intervention(
      'intervention-description',
      Priority::Normal,
      $type,
      $location
    );
    $intervention->setTitle('Intervention');
    $intervention->setEmployer($employer);
    $intervention->addParticipant($user);
    $intervention->setLogicId(42);
    $intervention->setAuthor($user);
    $intervention->setStatus(Status::Blocked);
    $intervention->setStartedAt($startedAt);

    $comment = new Comment('comment-message', $intervention);
    $comment->setAuthor($user);
    $intervention->addComment($comment);

    $entityManager->persist($intervention);
    $entityManager->persist($comment);
    $entityManager->flush();

    return $intervention;
  }

  public function testCanRestartAnNonStartedIntervention(): void
  {
    $doctrine = static::$kernel->getContainer()->get('doctrine');
    $i = $this->addIntervention($doctrine->getManager());
    $interventionDatas = [];

    $check = new PermissionCheck(self::$employer_anct, Role::Admin, 200, '/api/interventions/' . $i->getId(). '/restart');
    $apiResponse = $this->checkPermissionObject($check, $interventionDatas);

    self::assertSame(200, $apiResponse->getStatusCode(), 'PATCH to "/api/interventions/1/restart" failed.');
    self::assertNotEmpty($apiResponse->getContent(), 'The response must not have a body.');
    $content = json_decode($apiResponse->getContent(), false);
    self::assertSame(Status::ToDo->value, $content->status);
  }

  public function testCanRestartAnAlreadyStartedIntervention(): void
  {
    $doctrine = static::$kernel->getContainer()->get('doctrine');
    $i = $this->addIntervention($doctrine->getManager(), new \DateTimeImmutable());
    $interventionDatas = [];

    $check = new PermissionCheck(self::$employer_anct, Role::Admin, 200, '/api/interventions/' . $i->getId(). '/restart');
    $apiResponse = $this->checkPermissionObject($check, $interventionDatas);

    self::assertSame(200, $apiResponse->getStatusCode(), 'PATCH to "/api/interventions/1/restart" failed.');
    self::assertNotEmpty($apiResponse->getContent(), 'The response must not have a body.');
    $content = json_decode($apiResponse->getContent(), false);
    self::assertSame(Status::InProgress->value, $content->status);
  }

  #[\Override]
  protected function getMethod(): string
  {
    return 'POST';
  }
}
