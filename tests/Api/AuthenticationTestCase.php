<?php

declare(strict_types=1);

namespace App\Tests\Api;

use ApiPlatform\Symfony\Bundle\Test\ApiTestCase;
use App\Entity\Employer;
use App\Entity\Role;
use App\Entity\User;
use Zenstruck\Foundry\Test\Factories;
use Zenstruck\Foundry\Test\ResetDatabase;

abstract class AuthenticationTestCase extends ApiTestCase
{
  use ResetDatabase, Factories;

  /**
   * Ajoute un utilisateur.
   */
  protected function addUserIfNotExists(Role $role = Role::Director, ?string $_login = 'login', ?string $_password = 'password', ?Employer $existingEmployer = null): User
  {
    $login = $_login ?? 'login';
    $password = $_password ?? 'password';
    $container = self::getContainer();
    $doctrine = $container->get('doctrine');
    $manager = $container->get('doctrine')->getManager();
    $user = $doctrine->getRepository(User::class)->findOneBy(['login' => $login]);
    if($user) {

      if($existingEmployer !== null && $existingEmployer->getId() !== $user->getEmployer()->getId()) {
        $user->setEmployer($existingEmployer);
        $manager->flush();
      }

      return $user;
    }

    if($existingEmployer) {
      $employer = $this->addEmployerIfNotExists($existingEmployer->getSiren(), $existingEmployer->getName());
    }  else {
      $employer = $this->addEmployerIfNotExists($role === Role::Admin ? 'anct' : 'siren'/* . rand(10, 999)*/, $role === Role::Admin ? 'ANCT' : 'name');
    }

    $user = User::new(
      $login,
      $password,
      'firstname',
      'lastname',
      $employer,
      [$role->value],
      'email@yopmail.com',
      '0987654321',
    );
    $user->setEmployer($employer);
    $user->setPassword($container->get('security.user_password_hasher')->hashPassword($user, $password));

    $manager->persist($user);
    $manager->flush();

    return $user;
  }

  /**
   * Renvoie le JWT.
   * @return string le JWT.
   */
  protected function getJWT(Role $role = Role::Director, ?string $login = null, ?string $password = null, ?Employer $employer = null): string
  {
    $user = $this->addUserIfNotExists($role, $login, $password, $employer);

    return $this->authenticate($user);

  }

  public function authenticate(User $user): string
  {
//    $client = static::createClient();
//
//    $client->request('POST', '/authentication', [
//      'headers' => ['Content-Type' => 'application/json'],
//      'json' => [
//        'login' => $login,
//        'password' => $password,
//      ],
//    ]);
//    $apiResponse = $client->getResponse()->getContent();
//    $token = json_decode($apiResponse, false);

    $JWTManager = $this->getContainer()->get('lexik_jwt_authentication.jwt_manager');
    return $JWTManager->create($user);
  }

  /**
   * @param string $siren
   * @param string $name
   * @return Employer
   */
  protected function addEmployerIfNotExists(string $siren, string $name): Employer {

    $container = self::getContainer();
    $doctrine = $container->get('doctrine');
    $manager = $container->get('doctrine')->getManager();

    $employer = $doctrine->getRepository(Employer::class)->findOneBy(['siren' => $siren]);
    if(!is_null($employer)) {
      return $employer;
    }

    $employer = Employer::new($siren, $name, 4.4, 6.6);

    $manager->persist($employer);
    $manager->flush();

    return $employer;
  }
}
