<?php

declare(strict_types=1);

namespace App\Tests\Api\Director;

use App\Encoder\MultipartDecoder;
use App\Entity\Category;
use App\Entity\Employer;
use App\Entity\Intervention;
use App\Entity\Location;
use App\Entity\Priority;
use App\Entity\Role;
use App\Entity\Status;
use App\Entity\Type;
use App\Entity\User;
use App\Logger\UserProcessor;
use App\Repository\UserRepository;
use App\Security\EmployerVoter;
use App\Security\InterventionVoter;
use App\Security\UserVoter;
use App\Serializer\UploadedFileDenormalizer;
use App\State\InterventionProcessor;
use App\Tests\Api\AuthenticationTestCase;
use App\Tests\Api\PermissionCheck;
use App\Tests\Api\PermissionsTestCase;
use App\Tests\FixtureTrait;
use PHPUnit\Framework\Attributes as PA;
use Zenstruck\Foundry\Test\Factories;
use Zenstruck\Foundry\Test\ResetDatabase;

/**
 * Test POST /api/interventions.
 */
#[
  PA\CoversClass(Intervention::class),
  PA\UsesClass(Category::class),
  PA\UsesClass(Employer::class),
  PA\UsesClass(EmployerVoter::class),
  PA\UsesClass(InterventionVoter::class),
  PA\UsesClass(Location::class),
  PA\UsesClass(Priority::class),
  PA\UsesClass(Role::class),
  PA\UsesClass(Status::class),
  PA\UsesClass(Type::class),
  PA\UsesClass(User::class),
  PA\UsesClass(UserRepository::class),
  PA\UsesClass(UserVoter::class),
  PA\UsesClass(MultipartDecoder::class),
  PA\UsesClass(UserProcessor::class),
  PA\UsesClass(UploadedFileDenormalizer::class),
  PA\UsesClass(InterventionProcessor::class),
  PA\Group('api'),
  PA\Group('api_director'),
  PA\Group('api_director_interventions'),
  PA\Group('api_director_interventions_post'),
  PA\Group('intervention'),
  PA\TestDox('Director')
]
final class InterventionPostTest extends PermissionsTestCase
{
  use Factories, FixtureTrait;

  /**
   * Test que la route nécessite d'être authentifié.
   */
  public function testNeedsAuthenticationToPost(): void
  {
    $client = static::createClient();

    $apiResponse = $client->request('POST', '/api/interventions', [
      'headers' => [
        'Content-Type' => 'application/json',
      ],
    ]);
//dd($apiResponse);
    self::assertSame(401, $apiResponse->getStatusCode(), 'POST "/api/interventions" succeeded.');
  }

  /**
   * Renvoie les données de l'intervention.
   * @return array Les données de l'intervention.
   */
  private function interventionToPost(): array
  {
    return [
      'title' => 'Intervention title',
      'description' => 'intervention-description',
      'location' => [
        'street' => 'location-street',
        'rest' => 'location-rest',
        'postcode' => '75000',
        'city' => 'location-city',
        'longitude' => "1.234567",
        'latitude' => "1.23456"
      ],
      'participants' => [],
      'priority' => Priority::Normal,
      'type' => 'api/types/1',
      'status' => Status::ToDo,
      'employer' => 'api/employers/5',
      "frequency" => "onetime",
      "frequencyInterval" => 1
    ];
  }

  /**
   * Test qu'une intervention puisse être enregistrée.
   */
  public function testCanPostAnIntervention(): void
  {
    $check = new PermissionCheck(self::$employer_2, Role::Director, 201, '/api/interventions', $this->interventionToPost());
    $apiResponse = $this->checkPermissionObject($check);

    self::assertSame(201, $apiResponse->getStatusCode(), 'POST to "/api/interventions" failed.');
    self::assertJson($apiResponse->getContent());

    $jsonResponse = json_decode($apiResponse->getContent(), false);

    self::assertIsInt($jsonResponse->id);
  }

  #[\Override]
  protected function getMethod(): string
  {
    return 'POST';
  }
}
