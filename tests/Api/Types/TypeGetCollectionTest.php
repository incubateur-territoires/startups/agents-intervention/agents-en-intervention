<?php

declare(strict_types=1);

namespace App\Tests\Api\Types;

use App\Doctrine\ActiveAgentsExtension;
use App\Doctrine\AgentsExtension;
use App\Doctrine\CurrentAgentInterventionsExtension;
use App\Doctrine\EmployerInterventionsExtension;
use App\Encoder\MultipartDecoder;
use App\Entity\Category;
use App\Entity\Employer;
use App\Entity\Intervention;
use App\Entity\Location;
use App\Entity\Role;
use App\Entity\Type;
use App\Entity\User;
use App\Logger\UserProcessor;
use App\Repository\UserRepository;
use App\Tests\Api\AuthenticationTestCase;
use App\Tests\FixtureTrait;
use PHPUnit\Framework\Attributes as PA;
use Zenstruck\Foundry\Test\Factories;
use Zenstruck\Foundry\Test\ResetDatabase;

/**
 * Test GET /api/types.
 */
#[
  PA\CoversClass(Type::class),
  PA\UsesClass(Category::class),
  PA\UsesClass(Employer::class),
  PA\UsesClass(Role::class),
  PA\UsesClass(User::class),
  PA\UsesClass(UserRepository::class),
  PA\UsesClass(ActiveAgentsExtension::class),
  PA\UsesClass(AgentsExtension::class),
  PA\UsesClass(CurrentAgentInterventionsExtension::class),
  PA\UsesClass(EmployerInterventionsExtension::class),
  PA\UsesClass(MultipartDecoder::class),
  PA\UsesClass(Intervention::class),
  PA\UsesClass(Location::class),
  PA\UsesClass(UserProcessor::class),
  PA\Group('api'),
  PA\Group('api_types'),
  PA\Group('api_types_get_collection'),
  PA\Group('type')
]
final class TypeGetCollectionTest extends AuthenticationTestCase
{
  use ResetDatabase, Factories, FixtureTrait;

  /**
   * Test que la route nécessite d'être authentifié.
   */
  public function testNeedsAuthentication(): void
  {
    $client = static::createClient();

    $apiResponse = $client->request('GET', '/api/types');

    self::assertSame(401, $apiResponse->getStatusCode(), 'GET "/api/types" succeeded.');
  }

  /**
   * Test qu'une collection de types puissent être renvoyée.
   */
  public function testCanGetATypeCollection(): void
  {
    $client = static::createClient();

    $apiResponse = $client->request('GET', '/api/types', ['auth_bearer' => $this->getJWT()]);

    self::assertSame(200, $apiResponse->getStatusCode(), 'GET "/api/types" failed.');
    self::assertJson($apiResponse->getContent());

    $collection = json_decode($apiResponse->getContent(), false);

    $hydraMember = 'hydra:member';
    self::assertIsArray($collection->$hydraMember);
    $types = $collection->$hydraMember;

    foreach ($types as $type) {
      $this->assertTypeIsComplete($type);
    }
  }

  /**
   * Les assertions du type.
   * @param object $type le type.
   */
  private function assertTypeIsComplete(object $type): void
  {
    self::assertGreaterThanOrEqual(5, count((array)$type), 'Incorrect count of type data has been returned.');

    self::assertIsInt($type->id);
    self::assertIsString($type->name);
    self::assertIsObject($type->category);

    $atId = '@id';
    self::assertSame('/api/types/' . $type->id, $type->$atId);
    $atType = '@type';
    self::assertSame('Type', $type->$atType);
  }
}
