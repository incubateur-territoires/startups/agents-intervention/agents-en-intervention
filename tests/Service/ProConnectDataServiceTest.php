<?php

namespace App\Tests\Service;

use App\Dto\ProConnectUserDetails;
use Doctrine\Common\Collections\ArrayCollection;
use App\Entity\Employer;
use App\Entity\Role;
use App\Entity\User;
use App\Repository\EmployerRepository;
use App\Repository\UserRepository;
use App\Service\EntrepriseService;
use App\Service\ProConnectDataService;
use App\Service\TimezoneService;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\TestCase;
use Psr\EventDispatcher\EventDispatcherInterface;

class ProConnectDataServiceTest extends TestCase
{
  private $employerRepository;
  private $userRepository;
  private $entityManager;
  private $eventDispatcher;
  private $proConnectDataService;
  private $entrepriseService;

  protected function setUp(): void
  {
    $this->employerRepository = $this->createMock(EmployerRepository::class);
    $this->userRepository = $this->createMock(UserRepository::class);
    $this->entityManager = $this->createMock(EntityManagerInterface::class);
    $this->eventDispatcher = $this->createMock(EventDispatcherInterface::class);
    $this->entrepriseService = $this->createMock(EntrepriseService::class);

    $this->proConnectDataService = new ProConnectDataService(
      $this->employerRepository,
      $this->entityManager,
      $this->entrepriseService,
      $this->eventDispatcher,
      $this->userRepository
    );
  }

  public function testCreateUser(): void
  {
    $userDetails = new ProConnectUserDetails(
      'test@example.com',
      '8376453A8476',
      'John',
      'Doe',
      '1234567890',
    );

    $user = $this->proConnectDataService->createUser($userDetails);

    $this->assertInstanceOf(User::class, $user);
    $this->assertEquals($userDetails->email, $user->getEmail());
    $this->assertEquals($userDetails->given_name, $user->getFirstName());
    $this->assertEquals($userDetails->usual_name, $user->getLastName());
    $this->assertEquals($userDetails->phone_number, $user->getPhoneNumber());
  }

  public function testFindOrCreateUser(): void
  {
    $userDetails = new ProConnectUserDetails(
      'test@example.com',
      '8376453A8476',
      'John',
      'Doe',
      '1234567890',
    );

    $employer = $this->createMock(Employer::class);
    $employer->method('getUsers')->willReturn(new ArrayCollection());

    $this->userRepository->method('getByEmail')->willReturn(null);

    $user = $this->proConnectDataService->findOrCreateUser($userDetails, $employer);

    $this->assertInstanceOf(User::class, $user);
    $this->assertEquals($userDetails->email, $user->getEmail());
    $this->assertEquals([Role::AdminEmployer->value], $user->getRoles());
  }

  public function testFindOrCreateEmployer(): void
  {
    $userDetails = new ProConnectUserDetails(
      'test@example.com',
      '12345678901234',
      'John',
      'Doe',
      '1234567890',
    );

    $this->employerRepository->method('findOneBy')->willReturn(null);

    $employer = $this->createMock(Employer::class);
    $this->entrepriseService->method('getEmployerFromSiret')->willReturn($employer);

    $result = $this->proConnectDataService->findOrCreateEmployer($userDetails);

    $this->assertInstanceOf(Employer::class, $result);
  }

  public function testSync(): void
  {
    $userDetails = new ProConnectUserDetails(
      'test@example.com',
      '12345678901234',
      'John',
      'Doe',
      '1234567890',
    );


    $employer = $this->createMock(Employer::class);
    $this->employerRepository->method('findOneBy')->willReturn($employer);

    $user = $this->createMock(User::class);
    $this->userRepository->method('getByEmail')->willReturn($user);

    $result = $this->proConnectDataService->sync($userDetails);

    $this->assertInstanceOf(User::class, $result);
  }
}
