<?php

namespace App\Tests\Service;

use App\Entity\User;
use App\Entity\ActionLog;
use App\Service\AuditTrailLogger;
use App\Service\EventProcessor;
use App\Validator\UpdateEntityEventValidator;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\Validator\ConstraintViolationInterface;
use Symfony\Component\Validator\ConstraintViolationList;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class AuditTrailLoggerTest extends TestCase
{
  private $entityManager;
  private $logger;
  private $auditTrailLogger;
  private $eventProcessor;
  private $security;
  private $validator;

  protected function setUp(): void
  {
    $this->entityManager = $this->createMock(EntityManagerInterface::class);
    $this->validator = $this->createMock(ValidatorInterface::class);
    $this->eventProcessor = new EventProcessor([new UpdateEntityEventValidator($this->validator)]);
    $this->logger = $this->createMock(LoggerInterface::class);
    $this->security = $this->createMock(Security::class);
    $this->auditTrailLogger = new AuditTrailLogger($this->entityManager, $this->eventProcessor, $this->logger, $this->security);
  }

  public function testLog(): void
  {
    $type = 'entity_update';
    $entityClass = User::class;
    $entityId = 1;
    $changes = ['field' => 'value'];

    $this->entityManager
      ->expects($this->once())
      ->method('persist')
      ->with($this->isInstanceOf(ActionLog::class));

    $this->entityManager
      ->expects($this->never())
      ->method('flush');

    $log = $this->auditTrailLogger->log($type, $entityClass, $entityId, $changes);

    $this->assertInstanceOf(ActionLog::class, $log);
    $this->assertEquals($type, $log->getType());
    $this->assertEquals($entityClass, $log->getEntityClass());
    $this->assertEquals($entityId, $log->getEntityId());
    $this->assertEquals('{"valid":true,"data":{"field":"value"}}', $log->getChanges());
  }

  public function testLogWithFlush(): void
  {
    $type = 'entity_update';
    $entityClass = User::class;
    $entityId = 1;
    $changes = ['field' => 'value'];

    $this->entityManager
      ->expects($this->once())
      ->method('persist')
      ->with($this->isInstanceOf(ActionLog::class));

    $this->entityManager
      ->expects($this->once())
      ->method('flush');

    $log = $this->auditTrailLogger->log($type, $entityClass, $entityId, $changes, flush: true);

    $this->assertInstanceOf(ActionLog::class, $log);
    $this->assertEquals($type, $log->getType());
    $this->assertEquals($entityClass, $log->getEntityClass());
    $this->assertEquals($entityId, $log->getEntityId());
    $this->assertEquals('{"valid":true,"data":{"field":"value"}}', $log->getChanges());
  }


  public function testLogValidationFailed(): void
  {
    $type = 'entity_update';
    $entityClass = User::class;
    $entityId = 1;
    $changes = ['field' => 'value'];

    $this->entityManager
      ->expects($this->once())
      ->method('persist')
      ->with($this->isInstanceOf(ActionLog::class));

    $this->entityManager
      ->expects($this->never())
      ->method('flush');

    $this->logger
      ->expects($this->once())
      ->method('error')
      ->with('Invalid event data: {"field":"This value is not valid"}');

    $violation = $this->createConfiguredStub(ConstraintViolationInterface::class, [
      'getPropertyPath' => 'field',
      'getMessage' => 'This value is not valid',
    ]);
    $this->validator
      ->expects($this->once())
      ->method('validate')
      ->willReturn(new ConstraintViolationList([$violation]));

    $log = $this->auditTrailLogger->log($type, $entityClass, $entityId, $changes);

    $this->assertInstanceOf(ActionLog::class, $log);
    $this->assertEquals($type, $log->getType());
    $this->assertEquals($entityClass, $log->getEntityClass());
    $this->assertEquals($entityId, $log->getEntityId());
    $this->assertEquals('{"valid":false,"errors":{"field":"This value is not valid"},"raw":{"field":"value"},"data":[]}', $log->getChanges());
  }
}
