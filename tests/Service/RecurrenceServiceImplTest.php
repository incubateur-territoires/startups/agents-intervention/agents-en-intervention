<?php

namespace App\tests\Service;

use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use App\Entity\Category;
use App\Entity\Employer;
use App\Entity\Intervention;
use App\Entity\Location;
use App\Entity\Priority;
use App\Entity\Status;
use App\Entity\Type;
use App\Entity\User;
use App\Model\Frequency;
use App\Service\RecurrenceServiceImpl;
use Exception;
use Mockery;
use PHPUnit\Framework\TestCase;

class RecurrenceServiceImplTest extends TestCase
{
  #[\Override]
  public function tearDown(): void
  {
    Mockery::close();
  }


  public function testInit()
  {
    $em = Mockery::mock(EntityManagerInterface::class);
    $logger = Mockery::mock(LoggerInterface::class);

    $logger->shouldReceive('debug');

    self::assertIsObject(new RecurrenceServiceImpl($em, $logger));
  }

  public function testRemove()
  {
    $em = Mockery::mock(EntityManagerInterface::class);
    $logger = Mockery::mock(LoggerInterface::class);

    $logger->shouldReceive('debug')->times(4);
    $logger->shouldReceive('info')->andReturn(function ($args) {
      echo $args;
    })->twice();

    $intervention = $this->getIntervention();
    $i1 = $this->getIntervention($intervention);
    $i1->setStartAt($i1->getStartAt()->modify('-1 day'));
    $intervention->addGeneratedIntervention($i1);
    $i2 = $this->getIntervention($intervention);
    $intervention->addGeneratedIntervention($i2);
    $i3 = $this->getIntervention($intervention);
    $i3->setStartAt($i3->getStartAt()->modify('+1 day'));
    $intervention->addGeneratedIntervention($i3);
    $i4 = $this->getIntervention($intervention);
    $i4->setStartAt($i4->getStartAt()->modify('+2 day'));
    $intervention->addGeneratedIntervention($i4);

//    $em->shouldReceive('remove')->with($i1)->once();
//    $em->shouldReceive('remove')->with($i2)->once();
    $em->shouldReceive('remove')->with($i3)->once();
    $em->shouldReceive('remove')->with($i4)->once();
    $em->shouldReceive('remove')->with($intervention)->never();

    $em->shouldReceive('flush')->once();

    $service = new RecurrenceServiceImpl($em, $logger);
    $service->removeGeneratedInterventionsFor($intervention);

    self::assertNull($i1->getRecurrenceReference());
    self::assertNull($i2->getRecurrenceReference());
    self::assertNotNull($i3->getRecurrenceReference());
    self::assertNotNull($i4->getRecurrenceReference());
  }

  public function testRemoveWithDontRemoveAllReferences()
  {
    $em = Mockery::mock(EntityManagerInterface::class);
    $logger = Mockery::mock(LoggerInterface::class);

    $logger->shouldReceive('debug')->times(2);
    $logger->shouldReceive('info')->andReturn(function ($args) {
      echo $args;
    })->twice();

    $intervention = $this->getIntervention();
    $i1 = $this->getIntervention($intervention);
    $i1->setStartAt($i1->getStartAt()->modify('-1 day'));
    $intervention->addGeneratedIntervention($i1);
    $i2 = $this->getIntervention($intervention);
    $intervention->addGeneratedIntervention($i2);
    $i3 = $this->getIntervention($intervention);
    $i3->setStartAt($i3->getStartAt()->modify('+1 day'));
    $intervention->addGeneratedIntervention($i3);
    $i4 = $this->getIntervention($intervention);
    $i4->setStartAt($i4->getStartAt()->modify('+2 day'));
    $intervention->addGeneratedIntervention($i4);

//    $em->shouldReceive('remove')->with($i1)->once();
//    $em->shouldReceive('remove')->with($i2)->once();
    $em->shouldReceive('remove')->with($i3)->once();
    $em->shouldReceive('remove')->with($i4)->once();
    $em->shouldReceive('remove')->with($intervention)->never();

    $em->shouldReceive('flush')->once();

    $service = new RecurrenceServiceImpl($em, $logger);
    $service->removeGeneratedInterventionsFor($intervention, false);

    self::assertNotNull($i1->getRecurrenceReference());
    self::assertNotNull($i2->getRecurrenceReference());
    self::assertNotNull($i3->getRecurrenceReference());
    self::assertNotNull($i4->getRecurrenceReference());
  }

  public function testModify()
  {
    $em = Mockery::mock(EntityManagerInterface::class);
    $logger = Mockery::mock(LoggerInterface::class);

    $logger->shouldReceive('debug')->times(7);
    $logger->shouldReceive('info')->never();

    $intervention = $this->getIntervention();
    $i1 = $this->getIntervention($intervention);
    $i1->setStartAt($i1->getStartAt()->modify('-1 day'));
    $intervention->addGeneratedIntervention($i1);
    $i2 = $this->getIntervention($intervention);
    $intervention->addGeneratedIntervention($i2);
    $i3 = $this->getIntervention($intervention);
    $i3->setStartAt($i3->getStartAt()->modify('+1 day'));
    $intervention->addGeneratedIntervention($i3);
    $i4 = $this->getIntervention($intervention);
    $i4->setStartAt($i4->getStartAt()->modify('+2 day'));
    $intervention->addGeneratedIntervention($i4);

//    $em->shouldReceive('remove')->with($i1)->once();
//    $em->shouldReceive('remove')->with($i2)->once();
    $em->shouldReceive('persist')->with($i3)->once();
    $em->shouldReceive('persist')->with($i4)->once();

    $em->shouldReceive('flush')->once();

    $intervention->setTitle('mich');
    $service = new RecurrenceServiceImpl($em, $logger);
    $service->modifyGeneratedInterventionsFor($intervention);

    self::assertEquals('michel', $i1->getTitle());
    self::assertEquals('michel', $i2->getTitle());
    self::assertEquals('mich', $i3->getTitle());
    self::assertEquals('mich', $i4->getTitle());
  }

  /**
   * @throws Exception
   */
  public function testAdd()
  {
    $em = Mockery::mock(EntityManagerInterface::class);
    $logger = Mockery::mock(LoggerInterface::class);

    $logger->shouldReceive('debug')->times(260);
    $logger->shouldReceive('info')->never();

    $tz = new \DateTimeZone('UTC');
    $intervention = $this->getIntervention();
//    $intervention->setStartAt(new \DateTimeImmutable('2025-03-25T14:00:00', $tz));
//    $intervention->setEndedAt(new \DateTimeImmutable('2025-04-05T14:00:00', $tz));
//    $intervention->setEndAt(new \DateTimeImmutable('2025-03-25T16:00:00', $tz));
    $intervention->setStartAt(new \DateTimeImmutable('2024-06-05T14:00:00', $tz));
    $intervention->setEndedAt(new \DateTimeImmutable('2025-06-05T14:00:00', $tz));
    $intervention->setEndAt(new \DateTimeImmutable('2024-06-05T16:00:00', $tz));

    $em->shouldReceive('persist')->times(259);
    $em->shouldReceive('flush')->once();

    $service = new RecurrenceServiceImpl($em, $logger);
    $service->addGeneratedInterventionsFor($intervention, '2024-06-07T14:00:00');

    self::assertCount(259, $intervention->getGeneratedInterventions());
  }

  private function getIntervention(Intervention|null $ref = null)
  {
    $category = new Category();
    $category->setPicture('klnkn');
    $category->setDescription('qsfgqsfd');
    $category->setName('category');

    $type = new Type();
    $type->setName('jndkvjbsf');
    $type->setDescription('descr');
    $type->setPicture('picture');
    $type->setCategory($category);

    $location = new Location(
      'street',
      'rest',
      'postcode',
      'city',
      42,
      4
    );

    $employer = Employer::new(
      'siren',
      'name',
      43,
      5
    );

    $user = User::new(
      'login',
      'password',
      'firstname',
      'lastname',
      $employer,
    );

    $tz = new \DateTimeZone('Europe/Paris');
    $intervention = new Intervention(
      'descr',
      Priority::Normal,
      $type,
      $location,
    );
    $intervention->setTitle('michel');
    $intervention->setStatus(Status::Recurring);
    $intervention->setAuthor($user);
    $intervention->setEmployer($employer);
    $intervention->setFrequency(Frequency::DAILY);
    $intervention->setFrequencyInterval(1);
    $intervention->setStartAt(new \DateTimeImmutable('now', $tz));
    $intervention->setEndedAt(new \DateTimeImmutable('+1 year', $tz));
    $intervention->setEndAt(new \DateTimeImmutable('+2 hours', $tz));
    $intervention->setRecurrenceReference($ref);

    return $intervention;
  }
}
