<?php

namespace App\Tests\Service;

use App\Service\TimezoneServiceImpl;
use LogicException;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

class TimezoneServiceImplTest extends TestCase
{
  private HttpClientInterface $httpClient;
  private LoggerInterface $logger;
  private TimezoneServiceImpl $timezoneService;

  protected function setUp(): void
  {
    $this->httpClient = $this->createMock(HttpClientInterface::class);
    $this->logger = $this->createMock(LoggerInterface::class);
    $this->timezoneService = new TimezoneServiceImpl($this->httpClient, $this->logger);
  }

  public function testGetTimezoneFromCoordinates(): void
  {
    $latitude = 48.8566;
    $longitude = 2.3522;
    $response = $this->createMock(ResponseInterface::class);
    $response->method('toArray')->willReturn([
      'zoneName' => 'Europe/Paris',
    ]);

    $this->httpClient->method('request')->willReturn($response);

    $timezone = $this->timezoneService->getTimezoneFromCoordinates($latitude, $longitude);

    $this->assertEquals('Europe/Paris', $timezone);
  }

  public function testGetTimezoneFromCoordinatesThrowsExceptionOnError(): void
  {
    $latitude = 48.8566;
    $longitude = 2.3522;
    $this->httpClient->method('request')->willThrowException(new \Exception('Error'));

    $this->logger->expects($this->once())
      ->method('error')
      ->with('Erreur lors de la récupération du fuseau horaire', $this->arrayHasKey('exception'));

    $this->expectException(LogicException::class);
    $this->expectExceptionMessage('Erreur ');

    $this->timezoneService->getTimezoneFromCoordinates($latitude, $longitude);
  }
}
