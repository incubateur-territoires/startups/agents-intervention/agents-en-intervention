<?php

namespace App\Tests\Service;

use App\Service\PhpSessionUnserializer;
use PHPUnit\Framework\TestCase;
use Spatie\Snapshots\MatchesSnapshots;

class PhpSessionUnserializerTest extends TestCase
{
  use MatchesSnapshots;

  public function testUnserialize(): void
  {
    $sessionData = <<<SESS_DATA
_sf2_attributes|a:2:{s:3:"jwt";s:530:"eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJpYXQiOjE3MzQ1Mjg3MDAsImV4cCI6MTczNDU2NDcwMCwicm9sZXMiOlsiUk9MRV9BRE1JTiJdLCJ1c2VybmFtZSI6InN5bHZhaW4ubGVnbGVhdS5wcmVzdGF0YWlyZUBhbmN0LmdvdXYuZnIifQ.a2iDFnZXS4nHA7GXm0-IoS5446e0zfjyAEnmqIqCXYyhO_nKWSOW0JoBv9TRdj5Gfj-y8E2pNDFfu4Mt3xg2RFlaSC9pj3Uz-e9hFYcaMuJJOpMGwKephSLVov9P07lvhWPqG39gHb0vxovYYaLrhjgZ7--elGEkUKdA2JIkXjZRAg-l6U9EeHLY0Vz-fqQBfaxmQbSW8d-U1YLXFZDqtNlF1XeJ2RWQD4aeeCFet3d8wJp-Htjwi1wQMBY0EA_DkeF9W21F7R9s5Xkfq0L0J4ayokaU1z58DY-YM3ETaXV5tQ61rIgpMjscZ7WMkqAJLNoySm5Wo-gzPtbA7qDEqg";s:14:"_security_main";s:1712:"O:75:"Symfony\\Component\\Security\\Http\\Authenticator\\Token\\PostAuthenticationToken":2:{i:0;s:4:"main";i:1;a:5:{i:0;O:15:"App\\Entity\\User":14:{s:19:"\000App\\Entity\\User\000id";i:184;s:22:"\000App\\Entity\\User\000login";s:40:"sylvain.legleau.prestataire@anct.gouv.fr";s:25:"\000App\\Entity\\User\000password";s:1:"-";s:26:"\000App\\Entity\\User\000firstname";s:7:"Sylvain";s:25:"\000App\\Entity\\User\000lastname";s:8:"LE GLEAU";s:22:"\000App\\Entity\\User\000email";s:40:"sylvain.legleau.prestataire@anct.gouv.fr";s:28:"\000App\\Entity\\User\000phoneNumber";N;s:26:"\000App\\Entity\\User\000createdAt";O:17:"DateTimeImmutable":3:{s:4:"date";s:26:"2024-12-02 12:18:50.000000";s:13:"timezone_type";i:3;s:8:"timezone";s:3:"UTC";}s:24:"\000App\\Entity\\User\000picture";N;s:25:"\000App\\Entity\\User\000employer";O:20:"\000App\\Entity\\Employer":7:{s:23:"\000App\\Entity\\Employer\000id";i:54;s:26:"\000App\\Entity\\Employer\000siren";s:9:"130026032";s:25:"\000App\\Entity\\Employer\000name";s:4:"ANCT";s:30:"\000App\\Entity\\Employer\000longitude";d:3.223203;s:29:"\000App\\Entity\\Employer\000latitude";d:44.324594;s:26:"\000App\\Entity\\Employer\000users";O:33:"Doctrine\\ORM\\PersistentCollection":2:{s:13:"\000*\000collection";O:43:"Doctrine\\Common\\Collections\\ArrayCollection":1:{s:53:"\000Doctrine\\Common\\Collections\\ArrayCollection\000elements";a:0:{}}s:14:"\000*\000initialized";b:0;}s:29:"\000App\\Entity\\Employer\000timezone";s:12:"Europe/Paris";}s:23:"\000App\\Entity\\User\000active";b:1;s:28:"\000App\\Entity\\User\000connectedAt";O:17:"DateTimeImmutable":3:{s:4:"date";s:26:"2024-12-18 13:31:40.000000";s:13:"timezone_type";i:3;s:8:"timezone";s:3:"UTC";}s:22:"\000App\\Entity\\User\000roles";a:1:{i:0;s:10:"ROLE_ADMIN";}s:32:"\000App\\Entity\\User\000proConnectToken";s:43:"Jm5_ENmA45aS9OKYMea0XPy5sWdKkhYmdBmuBXUzRts";}i:1;b:1;i:2;N;i:3;a:0:{}i:4;a:1:{i:0;s:10:"ROLE_ADMIN";}}}";}_sf2_meta|a:3:{s:1:"u";i:1734528722;s:1:"c";i:1734528700;s:1:"l";i:0;}
SESS_DATA;
    $michel = (new PhpSessionUnserializer())->unserialize($sessionData);

    self::assertMatchesSnapshot($michel);
    self::assertNotFalse($michel);

  }

  public function testUnserializeBugInProduction(): void {
    $sessionData = <<<SESS_DATA
O:68:"Symfony\Component\Security\Core\Authentication\Token\RememberMeToken":3:{i:0;N;i:1;s:4:"main";i:2;a:5:{i:0;O:15:"App\Entity\User":14:{s:19:"\x00App\Entity\User\x00id";i:863;s:22:"\x00App\Entity\User\x00login";s:14:"François59123";s:25:"\x00App\Entity\User\x00password";s:60:"$2y$13\$aKpJU9jCxLRRUM5VcxrTdetAZERTapADvf/WsGmWSiS8qVz6fuhNi";s:26:"\x00App\Entity\User\x00firstname";s:9:"François";s:25:"\x00App\Entity\User\x00lastname";s:7:"JANSSEN";s:22:"\x00App\Entity\User\x00email";N;s:28:"\x00App\Entity\User\x00phoneNumber";N;s:26:"\x00App\Entity\User\x00createdAt";O:17:"DateTimeImmutable":3:{s:4:"date";s:26:"2024-07-04 15:13:26.000000";s:13:"timezone_type";i:3;s:8:"timezone";s:3:"UTC";}s:24:"\x00App\Entity\User\x00picture";N;s:25:"\x00App\Entity\User\x00employer";O:19:"App\Entity\Employer":1:{s:23:"\x00App\Entity\Employer\x00id";i:117;}s:23:"\x00App\Entity\User\x00active";b:1;s:28:"\x00App\Entity\User\x00connectedAt";O:17:"DateTimeImmutable":3:{s:4:"date";s:26:"2025-02-03 09:24:06.000000";s:13:"timezone_type";i:3;s:8:"timezone";s:3:"UTC";}s:22:"\x00App\Entity\User\x00roles";a:1:{i:0;s:13:"ROLE_DIRECTOR";}s:32:"\x00App\Entity\User\x00proConnectToken";N;}i:1;b:1;i:2;N;i:3;a:0:{}i:4;a:1:{i:0;s:13:"ROLE_DIRECTOR";}}}
SESS_DATA;
    $michel = (new PhpSessionUnserializer())->unserializeValue('dummy', $sessionData);

    self::assertMatchesSnapshot($michel);
    self::assertNotFalse($michel);
  }

  public function testUnserializeBugInProductionWithDoctrineProxy(): void {
    $sessionData = <<<SESS_DATA
O:68:"Symfony\Component\Security\Core\Authentication\Token\RememberMeToken":3:{i:0;N;i:1;s:4:"main";i:2;a:5:{i:0;O:15:"App\Entity\User":14:{s:19:"\x00App\Entity\User\x00id";i:863;s:22:"\x00App\Entity\User\x00login";s:14:"François59123";s:25:"\x00App\Entity\User\x00password";s:60:"$2y$13\$aKpJU9jCxLRRUM5VcxrTdetAZERTapADvf/WsGmWSiS8qVz6fuhNi";s:26:"\x00App\Entity\User\x00firstname";s:9:"François";s:25:"\x00App\Entity\User\x00lastname";s:7:"JANSSEN";s:22:"\x00App\Entity\User\x00email";N;s:28:"\x00App\Entity\User\x00phoneNumber";N;s:26:"\x00App\Entity\User\x00createdAt";O:17:"DateTimeImmutable":3:{s:4:"date";s:26:"2024-07-04 15:13:26.000000";s:13:"timezone_type";i:3;s:8:"timezone";s:3:"UTC";}s:24:"\x00App\Entity\User\x00picture";N;s:25:"\x00App\Entity\User\x00employer";O:34:"Proxies\__CG__\App\Entity\Employer":1:{s:23:"\x00App\Entity\Employer\x00id";i:117;}s:23:"\x00App\Entity\User\x00active";b:1;s:28:"\x00App\Entity\User\x00connectedAt";O:17:"DateTimeImmutable":3:{s:4:"date";s:26:"2025-02-03 09:24:06.000000";s:13:"timezone_type";i:3;s:8:"timezone";s:3:"UTC";}s:22:"\x00App\Entity\User\x00roles";a:1:{i:0;s:13:"ROLE_DIRECTOR";}s:32:"\x00App\Entity\User\x00proConnectToken";N;}i:1;b:1;i:2;N;i:3;a:0:{}i:4;a:1:{i:0;s:13:"ROLE_DIRECTOR";}}}
SESS_DATA;
    $michel = (new PhpSessionUnserializer())->unserializeValue('dummy', $sessionData);

    self::assertMatchesSnapshot($michel);
    self::assertNotFalse($michel);
  }
}
