<?php

namespace App\tests\Dto;

use App\Dto\InterventionRejectionInput;
use PHPUnit\Framework\TestCase;

class InterventionRejectionInputTest extends TestCase
{
  public function testInitialization(): void {
    $input = new InterventionRejectionInput("reason", "details");
    self::assertSame($input->reason, 'reason');
    self::assertSame($input->details, 'details');
  }

  public function testInitializationWithEmptyDetails(): void {
    $input = new InterventionRejectionInput("reason", "");
    self::assertSame($input->reason, 'reason');
    self::assertSame($input->details, '');
  }

  public function testInitializationWithNullDetails(): void {
    $this->expectException(\TypeError::class);
    $input = new InterventionRejectionInput("reason", null);
  }
}
