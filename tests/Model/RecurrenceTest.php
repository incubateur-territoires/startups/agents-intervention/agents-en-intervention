<?php

declare(strict_types=1);

namespace App\Tests\Model;

use App\Entity\Category;
use App\Entity\Employer;
use App\Entity\Intervention;
use App\Entity\Location;
use App\Entity\Priority;
use App\Entity\Status;
use App\Entity\Type;
use App\Entity\User;
use App\Model\Frequency;
use App\Model\Recurrence;
use Exception;
use PHPUnit\Framework\Attributes as PA;
use PHPUnit\Framework\TestCase;

#[
  PA\CoversClass(Recurrence::class),
]
final class RecurrenceTest extends TestCase
{
  /**
   * @throws Exception
   */
  public function testRecurrenceMonthly()
  {
    $tz = new \DateTimeZone('Europe/Paris');
    $intervention = $this->getIntervention();

    $intervention->setFrequency(Frequency::MONTHLY);
    $intervention->setFrequencyInterval(1);

    $rec = new Recurrence($intervention, new \DateTimeImmutable('2024-06-05T14:00:00', $tz), new \DateTimeImmutable('2025-06-05T14:00:00', $tz));

    $it = $rec->getIterator();
    self::assertIsIterable($it);
    self::assertCount(13, iterator_to_array($it));
  }

  /**
   * @throws Exception
   */
  public function testRecurrenceMonthlyWithImmediateStartDate()
  {
    $tz = new \DateTimeZone('Europe/Paris');
    $intervention = $this->getIntervention();

    $intervention->setStartAt(new \DateTimeImmutable('2024-06-12T14:00:00', $tz));
    $intervention->setEndedAt($intervention->getStartAt()->modify('+2 month'));
    $intervention->setFrequency(Frequency::MONTHLY);
    $intervention->setFrequencyInterval(1);

    $dt = new \DateTimeImmutable('2024-06-12T0:00:00', $tz);
    $rec = new Recurrence($intervention, $dt, $dt->modify('+2 month'));

    $it = $rec->getIterator();
    self::assertIsIterable($it);
    self::assertCount(2, iterator_to_array($it));
  }

  /**
   * @throws Exception
   */
  public function testRecurrenceWeekly()
  {
    $tz = new \DateTimeZone('Europe/Paris');
    $intervention = $this->getIntervention();

    $intervention->setFrequency(Frequency::WEEKLY);
    $intervention->setFrequencyInterval(2);

    $rec = new Recurrence($intervention, new \DateTimeImmutable('2024-06-05T14:00:00', $tz), new \DateTimeImmutable('2025-06-05T14:00:00', $tz));

    $it = $rec->getIterator();
    self::assertIsIterable($it);
    self::assertCount(27, iterator_to_array($it));
  }

  /**
   * @throws Exception
   */
  public function testRecurrenceWeeklyOnSaturday()
  {
    $tz = new \DateTimeZone('Europe/Paris');
    $intervention = $this->getIntervention();

    $intervention->setFrequency(Frequency::WEEKLY);
    $intervention->setFrequencyInterval(2);

    $rec = new Recurrence($intervention, new \DateTimeImmutable('2024-06-08T14:00:00', $tz), new \DateTimeImmutable('2025-06-08T14:00:00', $tz));

    $it = $rec->getIterator();
    self::assertIsIterable($it);
    self::assertCount(26, iterator_to_array($it));
  }

  /**
   * @throws Exception
   */
  public function testRecurrenceWeeklyOnSunday()
  {
    $tz = new \DateTimeZone('Europe/Paris');
    $intervention = $this->getIntervention();

    $intervention->setFrequency(Frequency::WEEKLY);
    $intervention->setFrequencyInterval(2);

    $rec = new Recurrence($intervention, new \DateTimeImmutable('2024-06-09T14:00:00', $tz), new \DateTimeImmutable('2025-06-09T14:00:00', $tz));

    $it = $rec->getIterator();
    self::assertIsIterable($it);
    self::assertCount(26, iterator_to_array($it));
  }

  /**
   * @throws Exception
   */
  public function testRecurrenceDaily()
  {
    $tz = new \DateTimeZone('Europe/Paris');
    $intervention = $this->getIntervention();

    $rec = new Recurrence($intervention, new \DateTimeImmutable('2024-06-05T14:00:00', $tz), new \DateTimeImmutable('2025-06-05T14:00:00', $tz));

    $it = $rec->getIterator();
    self::assertIsIterable($it);
    self::assertCount(261, iterator_to_array($it));
  }

  /**
   * @throws Exception
   */
  public function testRecurrenceYearly()
  {
    $tz = new \DateTimeZone('Europe/Paris');
    $intervention = $this->getIntervention();
    $intervention->setFrequency(Frequency::YEARLY);
    $intervention->setFrequencyInterval(1);
    $intervention->setEndedAt(new \DateTimeImmutable('2030-06-30'));
    $rec = new Recurrence(
      $intervention,
      new \DateTimeImmutable('2024-06-05T14:00:00'),
      new \DateTimeImmutable('2028-06-05T14:00:00'),
      $tz,
      new \DateTimeImmutable('2024-06-05T10:00:00', $tz)
    );

    $it = $rec->getIterator();
    self::assertIsIterable($it);
    /** @var Intervention[] $arr */
    $arr = iterator_to_array($it);
    self::assertCount(4, $arr);

    /*
     * $intervention->setStartAt(new \DateTimeImmutable('2024-06-05T14:00:00', $tz));
     * $intervention->setEndedAt(new \DateTimeImmutable('2025-06-05T14:00:00', $tz));
     * $intervention->setEndAt(new \DateTimeImmutable('2024-06-05T16:00:00', $tz));
     */
    self::assertEquals('2024-06-05T12:00:00+00:00', $arr[0]->getStartAt()->format('c'));
    self::assertEquals('2025-06-05T12:00:00+00:00', $arr[1]->getStartAt()->format('c'));
    self::assertEquals('2026-06-05T12:00:00+00:00', $arr[2]->getStartAt()->format('c'));
    self::assertEquals('2027-06-05T12:00:00+00:00', $arr[3]->getStartAt()->format('c'));
  }

  private function getIntervention()
  {
    $category = new Category();
    $category->setPicture('klnkn');
    $category->setDescription('qsfgqsfd');
    $category->setName('category');

    $type = new Type();
    $type->setName('jndkvjbsf');
    $type->setDescription('descr');
    $type->setPicture('picture');
    $type->setCategory($category);

    $location = new Location(
      'street',
      'rest',
      'postcode',
      'city',
      42,
      4
    );

    $employer = Employer::new(
      'siren',
      'name',
      43,
      5
    );

    $user = User::new(
      'login',
      'password',
      'firstname',
      'lastname',
      $employer,
    );

    $tz = new \DateTimeZone('Europe/Paris');
    $intervention = new Intervention(
      'descr',
      Priority::Normal,
      $type,
      $location,
    );
    $intervention->setTitle('michel');
    $intervention->setStatus(Status::Recurring);
    $intervention->setAuthor($user);
    $intervention->setEmployer($employer);
    $intervention->setFrequency(Frequency::DAILY);
    $intervention->setFrequencyInterval(1);
    $intervention->setStartAt(new \DateTimeImmutable('2024-06-05T14:00:00', $tz));
    $intervention->setEndedAt(new \DateTimeImmutable('2025-06-05T14:00:00', $tz));
    $intervention->setEndAt(new \DateTimeImmutable('2024-06-05T16:00:00', $tz));

    return $intervention;
  }

  /**
   * @throws Exception
   */
  public function testRecurrenceMonthlyOn2ndSaturday()
  {
    $tz = new \DateTimeZone('Europe/Paris');
    $i = $this->getIntervention();
    $i->setStartAt(new \DateTimeImmutable('2024-06-15T08:30:00', $tz));
    $i->setEndAt(new \DateTimeImmutable('2024-06-15T10:00:00', $tz));
    $i->setEndedAt(new \DateTimeImmutable('2025-06-15T08:30:00', $tz));
    $i->setFrequency(Frequency::MONTHLY);
    $i->setFrequencyInterval(1);

    $rec = new Recurrence($i, $i->getStartAt(), $i->getEndedAt(),
      $tz,
      new \DateTimeImmutable('2024-06-05T10:00:00', $tz));

    $it = $rec->getIterator();
    self::assertIsIterable($it);
    /** @var Intervention[] $arr */
    $arr = iterator_to_array($it);
    self::assertCount(12, $arr);

    self::assertEquals('2024-06-15T06:30:00+00:00', $arr[0]->getStartAt()->format('c'));
    self::assertEquals('2024-07-20T06:30:00+00:00', $arr[1]->getStartAt()->format('c'));
    self::assertEquals('2024-08-17T06:30:00+00:00', $arr[2]->getStartAt()->format('c'));
    self::assertEquals('2024-09-21T06:30:00+00:00', $arr[3]->getStartAt()->format('c'));
    self::assertEquals('2024-10-19T06:30:00+00:00', $arr[4]->getStartAt()->format('c'));
    self::assertEquals('2024-11-16T07:30:00+00:00', $arr[5]->getStartAt()->format('c'));
    self::assertEquals('2024-12-21T07:30:00+00:00', $arr[6]->getStartAt()->format('c'));
    self::assertEquals('2025-01-18T07:30:00+00:00', $arr[7]->getStartAt()->format('c'));
    self::assertEquals('2025-02-15T07:30:00+00:00', $arr[8]->getStartAt()->format('c'));
    self::assertEquals('2025-03-15T07:30:00+00:00', $arr[9]->getStartAt()->format('c'));
    self::assertEquals('2025-04-19T06:30:00+00:00', $arr[10]->getStartAt()->format('c'));
    self::assertEquals('2025-05-17T06:30:00+00:00', $arr[11]->getStartAt()->format('c'));
  }


  /**
   * @throws Exception
   */
  public function testRecurrenceMonthlyOn2ndSaturdayForGuadeloupe()
  {
    $tz = new \DateTimeZone('UTC');
    $i = $this->getIntervention();
    $i->setStartAt(new \DateTimeImmutable('2024-06-15T08:30:00', $tz));
    $i->setEndAt(new \DateTimeImmutable('2024-06-15T10:00:00', $tz));
    $i->setEndedAt(new \DateTimeImmutable('2025-06-15T08:30:00', $tz));
    $i->setFrequency(Frequency::MONTHLY);
    $i->setFrequencyInterval(1);

    $rec = new Recurrence(
      $i,
      $i->getStartAt(),
      $i->getEndedAt(),
      new \DateTimeZone('America/Guadeloupe'),
      new \DateTimeImmutable('2024-06-05T10:00:00', new \DateTimeZone('America/Guadeloupe'))
    );

    $it = $rec->getIterator();
    self::assertIsIterable($it);
    /** @var Intervention[] $arr */
    $arr = iterator_to_array($it);
    self::assertCount(12, $arr);

    self::assertEquals('2024-06-15T08:30:00+00:00', $arr[0]->getStartAt()?->format('c'));
    self::assertEquals('2024-07-20T08:30:00+00:00', $arr[1]->getStartAt()?->format('c'));
    self::assertEquals('2024-08-17T08:30:00+00:00', $arr[2]->getStartAt()?->format('c'));
    self::assertEquals('2024-09-21T08:30:00+00:00', $arr[3]->getStartAt()?->format('c'));
    self::assertEquals('2024-10-19T08:30:00+00:00', $arr[4]->getStartAt()?->format('c'));
    self::assertEquals('2024-11-16T08:30:00+00:00', $arr[5]->getStartAt()?->format('c'));
    self::assertEquals('2024-12-21T08:30:00+00:00', $arr[6]->getStartAt()?->format('c'));
    self::assertEquals('2025-01-18T08:30:00+00:00', $arr[7]->getStartAt()?->format('c'));
    self::assertEquals('2025-02-15T08:30:00+00:00', $arr[8]->getStartAt()?->format('c'));
    self::assertEquals('2025-03-15T08:30:00+00:00', $arr[9]->getStartAt()?->format('c'));
    self::assertEquals('2025-04-19T08:30:00+00:00', $arr[10]->getStartAt()?->format('c'));
    self::assertEquals('2025-05-17T08:30:00+00:00', $arr[11]->getStartAt()?->format('c'));
  }
}
