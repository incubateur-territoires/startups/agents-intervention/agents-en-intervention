module.exports = {
  // extends: ['next', 'turbo', 'prettier', 'plugin:jsx-a11y/strict'],
  // publgins: ['jsx-a11y', 'import', 'testing-library'],
  // ignorePatterns: ['vendor/*', 'public/bundles/*', 'public/build/*'],
  // languageOptions: {
  //   ecmaVersion: 12,
  // },
  ignores: ['vendor/*', 'public/bundles/*', 'public/build/*'],
  rules: {
    'interface-name': 'off',
    'no-console': 'off',
    'no-implicit-dependencies': 'off',
    'no-submodule-imports': 'off',
    'no-trailing-spaces': 'error',
    'react/jsx-key': 'off',
    // When hunting dead code it's useful to use the following:
    // ---
    // 'no-unused-vars': 'error',
    // 'import/no-unused-modules': [1, { unusedExports: true }],
  },
  // overrides: [
  //   // Only uses Testing Library lint rules in test files
  //   {
  //     files: ['**/__tests__/**/*.[jt]s?(x)', '**/?(*.)+(spec|test).[jt]s?(x)'],
  //     extends: ['plugin:testing-library/react'],
  //   },
  // ],
};
