<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20250214111116 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'create calendar event schema';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE TABLE calendar_event (id UUID NOT NULL, calendar_data TEXT NOT NULL, calendar_id VARCHAR(255) NOT NULL, last_modified TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, etag VARCHAR(32) NOT NULL, size INT NOT NULL, component_type VARCHAR(8) NOT NULL, first_occurence TIMESTAMP(0) WITHOUT TIME ZONE, last_occurence TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('COMMENT ON COLUMN calendar_event.id IS \'(DC2Type:ulid)\'');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('DROP TABLE calendar_event');
    }
}
