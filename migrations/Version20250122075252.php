<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20250122075252 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'création de la table app_config';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE TABLE app_config (key_name VARCHAR(255) NOT NULL, value TEXT NOT NULL, type VARCHAR(50) NOT NULL, PRIMARY KEY(key_name))');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('DROP TABLE app_config');
    }
}
