<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240220145912 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Ajout de la colonne title sur intervention';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE intervention ADD title VARCHAR(255)');
        $this->addSql("UPDATE intervention SET title='Intervention'");
        $this->addSql('ALTER TABLE intervention ALTER COLUMN title SET NOT NULL');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE intervention DROP title');
    }
}
