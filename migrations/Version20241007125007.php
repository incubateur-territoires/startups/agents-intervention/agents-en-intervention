<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20241007125007 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Ajout de la roadmap en base de donnée';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE SEQUENCE roadmap_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE roadmap (id INT NOT NULL, title VARCHAR(255) NOT NULL, description TEXT NOT NULL, expiration_date DATE DEFAULT NULL, estimated_date VARCHAR(255) NOT NULL, position INT NOT NULL, published BOOLEAN NOT NULL, PRIMARY KEY(id))');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('DROP SEQUENCE roadmap_id_seq CASCADE');
        $this->addSql('DROP TABLE roadmap');
    }
}
