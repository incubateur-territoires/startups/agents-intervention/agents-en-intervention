<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231122071450 extends AbstractMigration
{
  public function getDescription(): string
  {
    return '';
  }

  public function up(Schema $schema): void
  {
    // this up() migration is auto-generated, please modify it to your needs
    $this->addSql('ALTER TABLE intervention ADD logic_id INT');

    $this->addSql('update intervention i set logic_id=j.NouvelID
        from (SELECT ii.id as id, ROW_NUMBER() OVER (PARTITION BY employer_id ORDER BY id) AS NouvelID
                    FROM intervention ii order by ii.employer_id, ii.id) j where i.id=j.id');

    $this->addSql('CREATE TABLE intervention_sequences (employer_id INT NOT NULL, seq INT NOT NULL DEFAULT 1, PRIMARY KEY(employer_id))');
    $this->addSql('ALTER TABLE intervention_sequences ADD CONSTRAINT FK_F802D1B541CD9E7A FOREIGN KEY (employer_id) REFERENCES employer (id) NOT DEFERRABLE INITIALLY IMMEDIATE');

    // Initialize the table with existing data
    $this->addSql('INSERT INTO intervention_sequences SELECT employer_id, max(logic_id) from intervention group by employer_id');

    $this->addSql('ALTER TABLE intervention ALTER COLUMN logic_id SET NOT NULL');
  }

  public function down(Schema $schema): void
  {
    // this down() migration is auto-generated, please modify it to your needs
    $this->addSql('ALTER TABLE intervention_sequences DROP CONSTRAINT FK_F802D1B541CD9E7A');
    $this->addSql('DROP TABLE intervention_sequences');
    $this->addSql('ALTER TABLE intervention DROP logic_id');
  }
}
