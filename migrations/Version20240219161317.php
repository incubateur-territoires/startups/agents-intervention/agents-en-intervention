<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240219161317 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Création des interventions récurrentes';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE TABLE recurring_intervention_sequences (employer_id INT NOT NULL, seq INT DEFAULT 1 NOT NULL, PRIMARY KEY(employer_id))');
        $this->addSql('ALTER TABLE recurring_intervention_sequences ADD CONSTRAINT FK_415E4EDE41CD9E7A FOREIGN KEY (employer_id) REFERENCES employer (id) NOT DEFERRABLE INITIALLY IMMEDIATE');

      // Initialize the table with existing data
      $this->addSql('INSERT INTO recurring_intervention_sequences SELECT employer_id, 0 from intervention group by employer_id');

        $this->addSql('ALTER TABLE intervention ADD recurrence_reference_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE intervention ADD start_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL');
        $this->addSql('ALTER TABLE intervention ADD due_date TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL');
        $this->addSql('ALTER TABLE intervention ADD end_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL');
        $this->addSql('ALTER TABLE intervention ADD frequency VARCHAR(20) DEFAULT \'onetime\' NOT NULL');
        $this->addSql('ALTER TABLE intervention ADD trigger_at VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE intervention ADD exceptions JSON DEFAULT NULL');
        $this->addSql('ALTER TABLE intervention ADD frequency_interval INT NOT NULL DEFAULT 1');
        $this->addSql('COMMENT ON COLUMN intervention.start_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN intervention.due_date IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN intervention.end_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('ALTER TABLE intervention ADD CONSTRAINT FK_D11814AB2A55EF9E FOREIGN KEY (recurrence_reference_id) REFERENCES intervention (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_D11814AB2A55EF9E ON intervention (recurrence_reference_id)');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE recurring_intervention_sequences DROP CONSTRAINT FK_415E4EDE41CD9E7A');
        $this->addSql('DROP TABLE recurring_intervention_sequences');
        $this->addSql('ALTER TABLE intervention DROP CONSTRAINT FK_D11814AB2A55EF9E');
        $this->addSql('DROP INDEX IDX_D11814AB2A55EF9E');
        $this->addSql('ALTER TABLE intervention DROP recurrence_reference_id');
        $this->addSql('ALTER TABLE intervention DROP start_at');
        $this->addSql('ALTER TABLE intervention DROP due_date');
        $this->addSql('ALTER TABLE intervention DROP end_at');
        $this->addSql('ALTER TABLE intervention DROP frequency');
        $this->addSql('ALTER TABLE intervention DROP trigger_at');
        $this->addSql('ALTER TABLE intervention DROP exceptions');
        $this->addSql('ALTER TABLE intervention DROP frequency_interval');
    }
}
