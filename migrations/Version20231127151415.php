<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231127151415 extends AbstractMigration
{
  public function getDescription(): string
  {
    return '';
  }

  public function up(Schema $schema): void
  {
    // this up() migration is auto-generated, please modify it to your needs
    $this->addSql('ALTER SEQUENCE user_id_seq INCREMENT BY 1');
    $this->addSql('ALTER TABLE category ADD position INT DEFAULT NULL');
    $this->addSql('ALTER TABLE type ADD position INT DEFAULT NULL');
  }

  public function down(Schema $schema): void
  {
    // this down() migration is auto-generated, please modify it to your needs
    $this->addSql('ALTER SEQUENCE user_id_seq INCREMENT BY 1');
    $this->addSql('ALTER TABLE category DROP position');
    $this->addSql('ALTER TABLE type DROP position');
  }
}
