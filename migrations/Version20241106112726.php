<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20241106112726 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Ajout du champ startedAt pour le début effectif d\'une intervention';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE intervention ADD started_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL');
        $this->addSql('COMMENT ON COLUMN intervention.started_at IS \'(DC2Type:datetime_immutable)\'');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE intervention DROP started_at');
    }
}
