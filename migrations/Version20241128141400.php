<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20241128141400 extends AbstractMigration
{
  public function getDescription(): string
  {
    return "Création d'une piste d'audit";
  }

  public function up(Schema $schema): void
  {
    $this->addSql('CREATE TABLE action_log (id UUID NOT NULL, timestamp TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, type VARCHAR(255) NOT NULL, entity_class VARCHAR(255) NOT NULL, entity_id INT DEFAULT NULL, changes TEXT DEFAULT NULL, user_identifier INT DEFAULT NULL, PRIMARY KEY(id))');
    $this->addSql('COMMENT ON COLUMN action_log.id IS \'(DC2Type:uuid)\'');
  }

  public function down(Schema $schema): void
  {
    $this->addSql('DROP TABLE action_log');
  }
}
