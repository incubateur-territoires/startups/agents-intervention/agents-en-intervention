<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230830073621 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE intervention ADD pictures JSON DEFAULT NULL');
        $this->addSql('ALTER TABLE picture DROP CONSTRAINT fk_16db4f898eae3863');
        $this->addSql('DROP INDEX idx_16db4f898eae3863');
        $this->addSql('ALTER TABLE picture DROP intervention_id');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE intervention DROP pictures');
        $this->addSql('ALTER TABLE picture ADD intervention_id BIGINT NOT NULL');
        $this->addSql('ALTER TABLE picture ADD CONSTRAINT fk_16db4f898eae3863 FOREIGN KEY (intervention_id) REFERENCES intervention (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX idx_16db4f898eae3863 ON picture (intervention_id)');
    }
}
