<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20241007161038 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'enregistrement des notifications en base pour pouvoir les afficher dans le site directement';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE SEQUENCE notifications_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE notifications (id INT NOT NULL, user_id INT NOT NULL, intervention_id INT DEFAULT NULL, title VARCHAR(500) NOT NULL, message TEXT DEFAULT NULL, is_read BOOLEAN DEFAULT false NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_6000B0D38EAE3863 ON notifications (intervention_id)');
        $this->addSql('CREATE INDEX idx_user ON notifications (user_id)');
        $this->addSql('CREATE INDEX idx_is_read ON notifications (is_read)');
        $this->addSql('CREATE INDEX idx_created_at ON notifications (created_at)');
        $this->addSql('ALTER TABLE notifications ADD CONSTRAINT FK_6000B0D3A76ED395 FOREIGN KEY (user_id) REFERENCES "user" (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE notifications ADD CONSTRAINT FK_6000B0D38EAE3863 FOREIGN KEY (intervention_id) REFERENCES intervention (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('DROP SEQUENCE notifications_id_seq CASCADE');
        $this->addSql('ALTER TABLE notifications DROP CONSTRAINT FK_6000B0D3A76ED395');
        $this->addSql('ALTER TABLE notifications DROP CONSTRAINT FK_6000B0D38EAE3863');
        $this->addSql('DROP TABLE notifications');
    }
}
