<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230911090510 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE comment ALTER id TYPE INT');
        $this->addSql('ALTER TABLE comment ALTER intervention_id TYPE INT');
        $this->addSql('ALTER TABLE intervention ALTER id TYPE INT');
        $this->addSql('ALTER TABLE intervention ALTER location_id TYPE INT');
        $this->addSql('ALTER TABLE intervention_user ALTER intervention_id TYPE INT');
        $this->addSql('ALTER TABLE location ALTER id TYPE INT');
        $this->addSql('ALTER TABLE picture ALTER id TYPE INT');
        $this->addSql('ALTER TABLE "user" ALTER picture_id TYPE INT');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE intervention ALTER id TYPE BIGINT');
        $this->addSql('ALTER TABLE intervention ALTER location_id TYPE BIGINT');
        $this->addSql('ALTER TABLE comment ALTER id TYPE BIGINT');
        $this->addSql('ALTER TABLE comment ALTER intervention_id TYPE BIGINT');
        $this->addSql('ALTER TABLE location ALTER id TYPE BIGINT');
        $this->addSql('ALTER TABLE intervention_user ALTER intervention_id TYPE BIGINT');
        $this->addSql('ALTER TABLE picture ALTER id TYPE BIGINT');
        $this->addSql('ALTER TABLE "user" ALTER picture_id TYPE BIGINT');
    }
}
