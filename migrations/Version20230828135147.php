<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230828135147 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE category_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE comment_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE employer_id_seq INCREMENT BY 1 MINVALUE 2 START 2');
        $this->addSql('CREATE SEQUENCE intervention_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE location_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE picture_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE type_id_seq INCREMENT BY 1 MINVALUE 10 START 10');
        $this->addSql('CREATE SEQUENCE "user_id_seq" INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE category (id INT NOT NULL, name TEXT NOT NULL, description TEXT DEFAULT NULL, picture TEXT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_64C19C15E237E06 ON category (name)');
        $this->addSql('CREATE TABLE comment (id BIGINT NOT NULL, author_id INT NOT NULL, intervention_id BIGINT NOT NULL, message TEXT NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_9474526CF675F31B ON comment (author_id)');
        $this->addSql('CREATE INDEX IDX_9474526C8EAE3863 ON comment (intervention_id)');
        $this->addSql('COMMENT ON COLUMN comment.created_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('CREATE TABLE employer (id INT NOT NULL, siren TEXT NOT NULL, name TEXT NOT NULL, longitude NUMERIC(9, 6) NOT NULL, latitude NUMERIC(8, 6) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_DE4CF066DB8BBA08 ON employer (siren)');
        $this->addSql('CREATE TABLE intervention (id BIGINT NOT NULL, type_id INT NOT NULL, author_id INT NOT NULL, employer_id INT NOT NULL, location_id BIGINT NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, description TEXT DEFAULT NULL, status VARCHAR(255) NOT NULL, priority VARCHAR(255) NOT NULL, ended_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_D11814ABC54C8C93 ON intervention (type_id)');
        $this->addSql('CREATE INDEX IDX_D11814ABF675F31B ON intervention (author_id)');
        $this->addSql('CREATE INDEX IDX_D11814AB41CD9E7A ON intervention (employer_id)');
        $this->addSql('CREATE INDEX IDX_D11814AB64D218E ON intervention (location_id)');
        $this->addSql('COMMENT ON COLUMN intervention.created_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN intervention.ended_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('CREATE TABLE intervention_user (intervention_id BIGINT NOT NULL, user_id INT NOT NULL, PRIMARY KEY(intervention_id, user_id))');
        $this->addSql('CREATE INDEX IDX_822CCE8B8EAE3863 ON intervention_user (intervention_id)');
        $this->addSql('CREATE INDEX IDX_822CCE8BA76ED395 ON intervention_user (user_id)');
        $this->addSql('CREATE TABLE location (id BIGINT NOT NULL, street TEXT DEFAULT NULL, rest TEXT DEFAULT NULL, postcode TEXT DEFAULT NULL, city TEXT DEFAULT NULL, longitude NUMERIC(9, 6) NOT NULL, latitude NUMERIC(8, 6) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE picture (id BIGINT NOT NULL, intervention_id BIGINT NOT NULL, file_name TEXT NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, tags JSON NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_16DB4F89D7DF1668 ON picture (file_name)');
        $this->addSql('CREATE INDEX IDX_16DB4F898EAE3863 ON picture (intervention_id)');
        $this->addSql('COMMENT ON COLUMN picture.created_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('CREATE TABLE type (id INT NOT NULL, category_id INT NOT NULL, name TEXT NOT NULL, description TEXT DEFAULT NULL, picture TEXT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_8CDE572912469DE2 ON type (category_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8CDE57295E237E0612469DE2 ON type (name, category_id)');
        $this->addSql('CREATE TABLE "user" (id INT NOT NULL, employer_id INT NOT NULL, login TEXT NOT NULL, password TEXT NOT NULL, firstname TEXT NOT NULL, lastname TEXT NOT NULL, email TEXT DEFAULT NULL, phone_number TEXT DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, picture TEXT DEFAULT NULL, active BOOLEAN DEFAULT true NOT NULL, connected_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, roles JSON NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8D93D649AA08CB10 ON "user" (login)');
        $this->addSql('CREATE INDEX IDX_8D93D64941CD9E7A ON "user" (employer_id)');
        $this->addSql('COMMENT ON COLUMN "user".created_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN "user".connected_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('ALTER TABLE comment ADD CONSTRAINT FK_9474526CF675F31B FOREIGN KEY (author_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE comment ADD CONSTRAINT FK_9474526C8EAE3863 FOREIGN KEY (intervention_id) REFERENCES intervention (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE intervention ADD CONSTRAINT FK_D11814ABC54C8C93 FOREIGN KEY (type_id) REFERENCES type (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE intervention ADD CONSTRAINT FK_D11814ABF675F31B FOREIGN KEY (author_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE intervention ADD CONSTRAINT FK_D11814AB41CD9E7A FOREIGN KEY (employer_id) REFERENCES employer (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE intervention ADD CONSTRAINT FK_D11814AB64D218E FOREIGN KEY (location_id) REFERENCES location (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE intervention_user ADD CONSTRAINT FK_822CCE8B8EAE3863 FOREIGN KEY (intervention_id) REFERENCES intervention (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE intervention_user ADD CONSTRAINT FK_822CCE8BA76ED395 FOREIGN KEY (user_id) REFERENCES "user" (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE picture ADD CONSTRAINT FK_16DB4F898EAE3863 FOREIGN KEY (intervention_id) REFERENCES intervention (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE type ADD CONSTRAINT FK_8CDE572912469DE2 FOREIGN KEY (category_id) REFERENCES category (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE "user" ADD CONSTRAINT FK_8D93D64941CD9E7A FOREIGN KEY (employer_id) REFERENCES employer (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP SEQUENCE category_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE comment_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE employer_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE intervention_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE location_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE picture_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE type_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE "user_id_seq" CASCADE');
        $this->addSql('ALTER TABLE comment DROP CONSTRAINT FK_9474526CF675F31B');
        $this->addSql('ALTER TABLE comment DROP CONSTRAINT FK_9474526C8EAE3863');
        $this->addSql('ALTER TABLE intervention DROP CONSTRAINT FK_D11814ABC54C8C93');
        $this->addSql('ALTER TABLE intervention DROP CONSTRAINT FK_D11814ABF675F31B');
        $this->addSql('ALTER TABLE intervention DROP CONSTRAINT FK_D11814AB41CD9E7A');
        $this->addSql('ALTER TABLE intervention DROP CONSTRAINT FK_D11814AB64D218E');
        $this->addSql('ALTER TABLE intervention_user DROP CONSTRAINT FK_822CCE8B8EAE3863');
        $this->addSql('ALTER TABLE intervention_user DROP CONSTRAINT FK_822CCE8BA76ED395');
        $this->addSql('ALTER TABLE picture DROP CONSTRAINT FK_16DB4F898EAE3863');
        $this->addSql('ALTER TABLE type DROP CONSTRAINT FK_8CDE572912469DE2');
        $this->addSql('ALTER TABLE "user" DROP CONSTRAINT FK_8D93D64941CD9E7A');
        $this->addSql('DROP TABLE category');
        $this->addSql('DROP TABLE comment');
        $this->addSql('DROP TABLE employer');
        $this->addSql('DROP TABLE intervention');
        $this->addSql('DROP TABLE intervention_user');
        $this->addSql('DROP TABLE location');
        $this->addSql('DROP TABLE picture');
        $this->addSql('DROP TABLE type');
        $this->addSql('DROP TABLE "user"');
    }
}
