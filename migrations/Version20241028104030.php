<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20241028104030 extends AbstractMigration
{
    public function getDescription(): string
    {
      return 'Ajoute la timezone de la commune en base pour pouvoir générer correctement les récurrences';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE employer ADD timezone VARCHAR(255) DEFAULT \'Europe/Paris\' NOT NULL');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE employer DROP timezone');
    }
}
